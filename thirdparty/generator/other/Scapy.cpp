/* Soubor: Scapy.cpp
 * Autor: Pavel Svanda
 * Email: svandap@gmail.com
 * Datum vytvoreni: 9.5.2013
 * Popis: definice tridy Scapy, ktera slouzi jako klient k programu scapy
 */

#include "Scapy.h"

/* Scapy::Scapy
 * Popis: kostruktor
 * Parametry: -
 * Navratova hodnota: -
 */
Scapy::Scapy() {
}

/* Scapy::~Scapy
 * Popis: destruktor
 * Parametry: -
 * Navratova hodnota: -
 */
Scapy::~Scapy() {
}

/* Scapy::loadData
 * Popis: funkce provede nacteni dat z programu scapy, kterym je vygenerovana hlavicka
 * Parametry: char * str - obsahuje retezec, ktery specifikuje obsah generovany Scapy
 *	      char * data - misto, kam se ulozi vygenerovana data
 * Navratova hodnota: delka dat
 */
int Scapy::loadData(const char * str, char * data) {
	char command[SCAPY_MAX_COMMAND_LENGTH];
	sprintf(command, "%s%s%s", SCAPY_COMMAND_1, str, SCAPY_COMMAND_2);
	pid_t status;
	int PipeFD[2];
	if(pipe(PipeFD) != 0) {
		return 0;
	}

	status = fork();
	if(status == -1) {
		return 0;
	}

	if(status == 0) {
		//potomek
		close(PipeFD[STDIN_FILENO]);
		dup2(PipeFD[STDOUT_FILENO], STDOUT_FILENO);
		FILE * ret = freopen("/dev/null", "w", stderr);
		int status = system(command);
		close(PipeFD[STDOUT_FILENO]);
		exit(0);
	} else {
		//puvodni proces
		close(PipeFD[STDOUT_FILENO]);
		int stringLength = 5000;
		char * string = (char *) malloc(sizeof(char) * stringLength);
		int length = 0;
		char ch = 0;
		while(ch != EOF) {
			if(length == stringLength) {
				stringLength *= 2;
				string = (char *)realloc((void *)string, stringLength);
			}
			int res = read(PipeFD[STDIN_FILENO], &ch, 1);
			if(res != 1) { break; }
			string[length++] = ch;
		}
		string[length++] = 0;

		length = Scapy::parseData(string, data);
		
		free(string);
		close(PipeFD[STDIN_FILENO]);		
		return length;
	}
	return 0;
}

/* Scapy::parseData
 * Popis: funkce provede ziskani datoveho obsahu ze stupniho retezce
 * Parametry: char * str - obsahuje retezec, ktery obsahuje pozadovana data
 *	      char * data - misto, kam se ulozi ziskana data
 * Navratova hodnota: delka dat
 */
int Scapy::parseData(char * str, char * data) {
	bool saved    = false;
	int stringPos = 0;
	int outputPos = 0;
	int rowPos = 0;
	int preRow = 0;
	int wsCount = 0;
	char ch = str[stringPos++];
	while(ch != 0) {
		if(saved) {
			if(ch != ' ') {
				if(rowPos > 31) {
					while(ch != '\n') {
						ch = str[stringPos++];
					}
					rowPos = 0;
					preRow = 0;
					wsCount = 0;
				} else {
					if((ch >= '0' && ch <= '9') || (ch >= 'a' && ch <= 'f') || (ch >= 'A' && ch <= 'F')) {
						if(preRow < 4) {
							preRow++;
						} else {
							data[outputPos++] = ch;
							rowPos++;
							wsCount = 0;
						}
					}
				}
				if(ch == '>' && str[stringPos] == '>' && str[stringPos + 1] == '>') {
					break;
				}
			} else {
				wsCount++;
				if(rowPos > 0 && rowPos < 31 && wsCount > 3) {
					break;
				}
			}
		} else {
			if(str[stringPos++] == '>' && str[stringPos++] == '>' && str[stringPos++] == '>') {
				stringPos += 5;
				saved = true;
			}
		}
		ch = str[stringPos++];
	}
	data[outputPos++] = 0;
	return 0;
}

