/* Soubor: Configuration.h
 * Autor: Pavel Svanda
 * Email: svandap@gmail.com
 * Datum vytvoreni: 10.11.2012
 * Popis: deklarace tridy Configuration, ktera slouzi k ukladani uzivatelskych nastaveni
 */
#ifndef CONFIGURATION_H
#define CONFIGURATION_H

#define CONFIGURATION_PATH "config/configuration.xml"

#include <string.h>
#include "Alias.h"


class Configuration {
	public:
		Alias alias;

		Configuration();
	       ~Configuration();

		void saveConfiguration();
};
#endif
