/* Soubor: Alias.cpp
 * Autor: Pavel Svanda
 * Email: svandap@gmail.com
 * Datum vytvoreni: 9.11.2012
 * Popis: definice tridy Alias, ktera slouzi k ukladani aliasu adresaru
 */

#include "Alias.h"

/* Alias::Alias
 * Popis: kostruktor
 * Parametry: -
 * Navratova hodnota: -
 */
Alias::Alias() {
}

/* Alias::~Alias
 * Popis: destruktor
 * Parametry: -
 * Navratova hodnota: -
 */
Alias::~Alias() {
}

/* Alias::setAlias
 * Popis: funkce provede nastaveni noveho aliasu
 * Parametry: alias - jmeno noveho aliasu
 *	      path  - adresa, kterou alias bude zastupovat
 * Navratova hodnota: -
 */
void Alias::setAlias(char * alias, char * path) {
	string aliasS(alias);
	string pathS(path);
	if(this -> aliasList.find(aliasS) != this -> aliasList.end()) {
		this -> aliasListIt= this -> aliasList.find(aliasS);
		this -> aliasList.erase(this -> aliasListIt); 
	}
	this -> aliasList.insert ( pair<string,string>(aliasS,pathS) );
}

/* Alias::delAlias
 * Popis: odstrani alias
 * Parametry: alias - jmeno mazaneho aliasu
 * Navratova hodnota: informace, zdali se odstraneni podarilo
 */
bool Alias::delAlias(char * alias) {
	string aliasS(alias);
	if(this -> aliasList.find(aliasS) != this -> aliasList.end()) {
		this -> aliasListIt= this -> aliasList.find(aliasS);
		this -> aliasList.erase(this -> aliasListIt); 
		return true;
	}
	return false;
}

/* Alias::getPaht
 * Popis: funkce vrati cestu pro zadany alias
 * Parametry: alias - jmeno aliasu
 *	      path  - adresa, kam se ulozi odpovidajici cesta	
 * Navratova hodnota: informace, zdali se nacteni podarilo
 */
bool Alias::getPath(char * alias, char * path) {
	string aliasS(alias);
	if(this -> aliasList.find(aliasS) != this -> aliasList.end()) {
		this -> aliasListIt= this -> aliasList.find(aliasS);
		strcpy(path,(char *)this -> aliasListIt -> second.c_str());
		return true;		
	}
	return false;
}

/* Alias::listAlias
 * Popis: funkce provede vypsani vsech aliasu
 * Parametry: -
 * Navratova hodnota: informace, zdali je seznam prazdny nebo ne
 */
bool Alias::listAlias() {
	bool anyAlias = false;
	for ( this -> aliasListIt = this -> aliasList.begin(); this -> aliasListIt != this -> aliasList.end(); this -> aliasListIt++ ) {
		anyAlias = true;
		printf("\t%15s - ", (*aliasListIt).first.c_str());
		printf("%s", (*aliasListIt).second.c_str());
		printf("\r\n");
	}
	return anyAlias;
}

/* Alias::saveAlias
 * Popis: funkce provede ulozeni aliasu do xml souboru - provede se vygenerovani odpovidajici casti, ktera se vrati
 * Parametry: -
 * Navratova hodnota: element v xml souboru, ktery se pote ulozi na disk
 */
TiXmlElement * Alias::saveAlias() {
	TiXmlElement * aliasElement = new TiXmlElement( "alias");
 
	char pom [MAX_LENGTH_ALIAS_AND_PATH];
	int i = 0;
	for ( this -> aliasListIt = this -> aliasList.begin(); this -> aliasListIt != this -> aliasList.end(); this -> aliasListIt++ ) {
		sprintf(pom, "aliasItem%d", i++);
		TiXmlElement * aliasItemElement = new TiXmlElement( pom );
		TiXmlElement * aliasNameElement = new TiXmlElement( "aliasName" );
		aliasNameElement -> LinkEndChild(new TiXmlText((char *)this -> aliasListIt -> first.c_str()));
		aliasItemElement -> LinkEndChild( aliasNameElement );

		TiXmlElement * aliasPathElement = new TiXmlElement( "aliasPath" );
		aliasPathElement -> LinkEndChild(new TiXmlText((char *)this -> aliasListIt -> second.c_str()));
		aliasItemElement -> LinkEndChild( aliasPathElement );

		aliasElement -> LinkEndChild( aliasItemElement );
	}
	aliasElement -> SetAttribute("value", i );

	return aliasElement;
}

/* Alias::loadAlias
 * Popis: funkce provede nacteni aliasu z xml souboru
 * Parametry: element - korenovy element, ze ktereho se nacitaji jednotlive aliasy
 * Navratova hodnota: -
 */
void Alias::loadAlias(TiXmlElement * element) {
	TiXmlHandle hAliasElement(element);

	int countAlias;
	element -> QueryIntAttribute("value", &countAlias );
	char pom [MAX_LENGTH_ALIAS_AND_PATH];
	for(int i = 0; i < countAlias; i++) {
		sprintf(pom, "aliasItem%d", i);
		TiXmlElement * aliasItem = hAliasElement.FirstChild( pom ).Element();	
		if(aliasItem) {
				TiXmlHandle hAliasItem(aliasItem);	
				TiXmlElement * aliasName = hAliasItem.FirstChild( "aliasName" ).Element();
				TiXmlElement * aliasPath = hAliasItem.FirstChild( "aliasPath" ).Element();
				if(aliasName && aliasPath) {
					if(aliasName -> GetText() != NULL && aliasPath -> GetText() != NULL) {
						this -> setAlias((char *)aliasName -> GetText(), (char *)aliasPath -> GetText());		
					}
				}			
		}
	}
}
