/* Soubor: CommandHistory.h
 * Autor: Pavel Svanda
 * Email: svandap@gmail.com
 * Datum vytvoreni: 6.3.2013
 * Popis: deklarace tridy CommandHistory, ktera slouzi k ukladani historie prikazu behem spusteni programu
 */
#ifndef COMMAND_HISTORY_H
#define COMMAND_HISTORY_H

#include <string.h>
#include <string>
#include <map>

using namespace std;

typedef map<int, string> CommandHistoryList;
typedef	map<int, string>::iterator CommandHistoryListIt;

class CommandHistory {
	private:
		CommandHistoryList   commandHistoryList;
		CommandHistoryListIt commandHistoryListIt;	

		/*cislo posledniho nacteneho prikazu*/
		int actualCommand;
		/*cislo posledniho vlozeneho prikazu*/
		int lastCommand;

	public:
		CommandHistory();
	       ~CommandHistory();

		void addCommand(char * command);
		bool previousCommand(char * command);
		bool nextCommand(char * command);
};
#endif
