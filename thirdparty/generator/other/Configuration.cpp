/* Soubor: Configuration.cpp
 * Autor: Pavel Svanda
 * Email: svandap@gmail.com
 * Datum vytvoreni: 10.11.2012
 * Popis: definice tridy Configuration, ktera slouzi k ukladani uzivatelskych nastaveni
 */

#include "Configuration.h"

/* Configuration::Configuration
 * Popis: kostruktor
 * Parametry: -
 * Navratova hodnota: -
 */
Configuration::Configuration() {
	TiXmlDocument loadFile( CONFIGURATION_PATH );
	bool loadOkay = loadFile.LoadFile();
	if(loadOkay) {
		TiXmlHandle hLoadFile(&loadFile);
		TiXmlElement * configurationElement = hLoadFile.FirstChild( "configuration" ).Element();
		TiXmlHandle hConfigurationElement(configurationElement);
		//nactu aliasy	
		TiXmlElement * aliasElement = hConfigurationElement.FirstChild( "alias" ).Element();
		if(aliasElement) {
			this -> alias.loadAlias(aliasElement);	
		}
	}
}

/* Configuration::~Configuration
 * Popis: destruktor
 * Parametry: -
 * Navratova hodnota: -
 */
Configuration::~Configuration() {
	this -> saveConfiguration();
}

/* Configuration::saveConfiguration()
 * Popis: funkce provede ulozeni configurace do souboru
 * Parametry: -
 * Navratova hodnota: -
 */
void Configuration::saveConfiguration() {
	TiXmlDocument saveFile;
	TiXmlDeclaration * decl = new TiXmlDeclaration( "1.0", "", "" );
	saveFile.LinkEndChild( decl );
	
	//polozky nastaveni	
	TiXmlElement * configurationElement = new TiXmlElement( "configuration" );
	saveFile.LinkEndChild( configurationElement ); 

	//ulozim aliasy
	TiXmlElement * aliasElement = this -> alias.saveAlias();
	configurationElement -> LinkEndChild( aliasElement ); 	

	saveFile.SaveFile( CONFIGURATION_PATH );
}
