/* Soubor: CommandHistory.cpp
 * Autor: Pavel Svanda
 * Email: svandap@gmail.com
 * Datum vytvoreni: 6.3.2013
 * Popis: definice tridy CommandHistory, ktera slouzi k ukladani historie prikazu
 */

#include "CommandHistory.h"

/* CommandHistory::CommandHistory
 * Popis: kostruktor
 * Parametry: -
 * Navratova hodnota: -
 */
CommandHistory::CommandHistory() {
	this -> actualCommand = 0;
	this -> lastCommand = 0;
}

/* CommandHistory::~CommandHistory
 * Popis: destruktor
 * Parametry: -
 * Navratova hodnota: -
 */
CommandHistory::~CommandHistory() {
}

/* CommandHistory::addCommand
 * Popis: funkce prida prikaz do historie prikazu
 * Parametry: char * command - pridavany prikaz
 * Navratova hodnota: -	
 */
void CommandHistory::addCommand(char * command) {
	if(strlen(command) > 0) {
		string commandS(command);
		this -> commandHistoryList.insert ( pair<int,string>(this -> lastCommand,commandS) );
		this -> actualCommand = this -> lastCommand = this -> lastCommand + 1;
	}
}

/* CommandHistory::previousCommand(char * command)
 * Popis: funkce nacte predchozi prikaz
 * Parametry: char * command - misto, kam se ulozi nacteny prikaz
 * Navratova hodnota: informace, zdali se podarilo prikaz nacist
 *                    false: neni predchozi prikaz
 *		      true : v ostatnich pripadech
 */
bool CommandHistory::previousCommand(char * command) {
	if(this -> actualCommand == 0) {
		return false;
	}
	this -> actualCommand--;
	this -> commandHistoryListIt= this -> commandHistoryList.find(this -> actualCommand);
	strcpy(command,(char *)this -> commandHistoryListIt -> second.c_str());
	return true;
}

/* CommandHistory::nextCommand(char * command)
 * Popis: funkce nacte nasledujici prikaz
 * Parametry: char * command - misto, kam se ulozi nacteny prikaz
 * Navratova hodnota: informace, zdali se podarilo prikaz nacist
 *                    false: neni predchozi prikaz
 *		      true : v ostatnich pripadech
 */
bool CommandHistory::nextCommand(char * command) {
	if(this -> actualCommand == this -> lastCommand) {
		strcpy(command, "");
		return false;
	}
	this -> actualCommand++;
	this -> commandHistoryListIt= this -> commandHistoryList.find(this -> actualCommand);
	strcpy(command,(char *)this -> commandHistoryListIt -> second.c_str());
	return true;
}
