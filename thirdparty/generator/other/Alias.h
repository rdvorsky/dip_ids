/* Soubor: Alias.h
 * Autor: Pavel Svanda
 * Email: svandap@gmail.com
 * Datum vytvoreni: 9.11.2012
 * Popis: deklarace tridy Alias, ktera slouzi k ukladani aliasu adresaru
 */
#ifndef ALIAS_H
#define ALIAS_H

#include <string.h>
#include <string>
#include <map>

#ifdef TIXML_USE_STL
	#include <iostream>
	#include <sstream>
	using namespace std;
#else
	#include <stdio.h>
#endif

#if defined( WIN32 ) && defined( TUNE )
	#include <crtdbg.h>
	_CrtMemState startMemState;
	_CrtMemState endMemState;
#endif

#include "../tinyXml/tinyxml.h"


#define MAX_LENGTH_ALIAS_AND_PATH	1000

using namespace std;

typedef map<string, string> AliasList;
typedef	map<string, string>::iterator AliasListIt;


class Alias {
	private:
		AliasList   aliasList;
		AliasListIt aliasListIt;	
	public:
		Alias();
	       ~Alias();

		void setAlias(char * alias, char * path);
		bool delAlias(char * alias);
		bool getPath(char * alias, char * path);
		bool listAlias();

		TiXmlElement * saveAlias();
		void loadAlias(TiXmlElement * element);
};
#endif
