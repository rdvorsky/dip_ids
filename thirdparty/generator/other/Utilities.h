/* Soubor: Utilities.h
 * Autor: Pavel Svanda
 * Email: svandap@gmail.com
 * Datum vytvoreni: 11.2.2013
 * Popis: deklarace tridy Utilities, ktera obsahuje pomocne funkce, ktere se mohou opakovat
 */

#ifndef UTILITIES_H
#define UTILITIES_H

#define LITTLE_ENDIAN_ORDER true
#define BIG_ENDIAN_ORDER false

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <endian.h>

class Utilities {
	public:
		static void  memcpy8(void * destination, const void * source, int length, bool littleEndian);		
		static void memcpy16(void * destination, const void * source, int length, bool littleEndian);
		static void memcpy24(void * destination, const void * source, int length, bool littleEndian);		
		static void memcpy32(void * destination, const void * source, int length, bool littleEndian);
		static void memcpy64(void * destination, const void * source, int length, bool littleEndian);
		static void copyOUI(uint8_t * oui, char * string, int length);
		static void hexDump (const char *desc, void *addr, int len);
		static void hexStr (const char *desc, void *addr, int len);
		static int strToHex (uint8_t * dst, char * src);
		static int readCommandFromFile(FILE ** input, char ** buf);
};

#endif
