/* Soubor: Utilities.cpp
 * Autor: Pavel Svanda
 * Email: svandap@gmail.com
 * Datum vytvoreni: 11.2.2013
 * Popis: definice tridy Utilities, ktera obsahuje pomocne funkce, ktere se mohou opakovat
 */
#include "Utilities.h"

/* Utilities::memcpy8
 * Popis: funkce provede nakopirovani hodnoty source do pameti definovane pomoci destination s ohledem na endianitu
 * Parametry: void * destination - ukazatel do pameti, kam se budou kopirovat data
 *            void * source - kopirovana hodnota
 *	      int length - delka kopirovanych dat	
 *            bool littleEndian - informace, zdali ma byt ve vystupnich datech (pokud jsou zasilana na rozhrani) v poradi little endian (RadioTap, IEEE) nebo big endian
 * Navratova hodnota: -
 */
void Utilities::memcpy8(void * destination, const void * source, int length, bool littleEndian) {
	uint8_t * isource = (uint8_t*) source;
	uint8_t * idestination = (uint8_t *)destination;
	uint8_t _source;
	for(int i = 0; i < length; i++) {
		if(i != 0) {
			idestination++;
			isource++;
		}
		if(littleEndian) {
			//pokud na vystupu maji byt data v little endian, tak v pripade little endianu nedelam nic, v big endianu musim prevest na little
			//v pripade uint8_t nedelam nic v obou pripadech
			_source = *isource;
		} else {
			//pokud na vystupu maji byt data v big endianu, tak v pripade little endianu musim prevest data na big endian a v pripade big endianu nedelam nic	
			//v pripade uint8_t nedelam nic v obou pripadech
			_source = *isource;
		}
		memcpy(idestination, &_source, sizeof(uint8_t));
	}
}

/* Utilities::memcpy16
 * Popis: funkce provede nakopirovani hodnoty source do pameti definovane pomoci destination s ohledem na endianitu
 * Parametry: void * destination - ukazatel do pameti, kam se budou kopirovat data
 *            void * source - kopirovana hodnota
 *	      int length - delka kopirovanych dat
 *            bool littleEndian - informace, zdali ma byt ve vystupnich datech (pokud jsou zasilana na rozhrani) v poradi little endian (RadioTap, IEEE) nebo big endian
 * Navratova hodnota: -
 */		
void Utilities::memcpy16(void * destination, const void * source, int length, bool littleEndian) {
	uint16_t * isource = (uint16_t*) source;
	uint8_t * idestination = (uint8_t *)destination;
	uint16_t _source;
	for(int i = 0; i < length; i++) {
		if(i != 0) {
			idestination++;
			isource++;	
		}	
		if(littleEndian) {
			//na little endianu nedelam nic
			if(__BYTE_ORDER == __LITTLE_ENDIAN) {
				_source = *isource;
			} else {
			//na big endianu musim prevest na little endian
				_source = __bswap_16(*isource);
			}
		} else {
			//na little endianu musim prevest na big endian
			if(__BYTE_ORDER == __LITTLE_ENDIAN) {
				_source = __bswap_16(*isource);
			} else {
			//na big endianu nedelam nic
				_source = *isource;
			}
		}
		memcpy(idestination, &_source, sizeof(uint16_t));
	}
}

/* Utilities::memcpy24
 * Popis: funkce provede nakopirovani hodnoty source do pameti definovane pomoci destination s ohledem na endianitu
 * Parametry: void * destination - ukazatel do pameti, kam se budou kopirovat data
 *            void * source - kopirovana hodnota
 *	      int length - delka kopirovanych dat
 *            bool littleEndian - informace, zdali ma byt ve vystupnich datech (pokud jsou zasilana na rozhrani) v poradi little endian (RadioTap, IEEE) nebo big endian
 * Navratova hodnota: -
 */
void Utilities::memcpy24(void * destination, const void * source, int length, bool littleEndian) {
	uint32_t * isource = (uint32_t*) source;
	uint8_t * idestination = (uint8_t *)destination;
	uint32_t _source;
	for(int i = 0; i < length; i++) {
		if(i != 0) {
			idestination++;
			isource++;	
		}	
		if(littleEndian) {
			//na little endianu nedelam nic
			if(__BYTE_ORDER == __LITTLE_ENDIAN) {
				_source = *isource;
			} else {
			//na big endianu musim prevest na little endian
				_source = __bswap_32(*isource);
			}
		} else {
			//na little endianu musim prevest na big endian
			if(__BYTE_ORDER == __LITTLE_ENDIAN) {
				_source = __bswap_32(*isource);
			} else {
			//na big endianu nedelam nic
				_source = *isource;
			}
		}
		_source = _source >> 8;
		memcpy(idestination, &_source, sizeof(uint8_t) * 3);
	}
}

/* Utilities::memcpy32
 * Popis: funkce provede nakopirovani hodnoty source do pameti definovane pomoci destination s ohledem na endianitu
 * Parametry: void * destination - ukazatel do pameti, kam se budou kopirovat data
 *            void * source - kopirovana hodnota
 *	      int length - delka kopirovanych dat
 *            bool littleEndian - informace, zdali ma byt ve vystupnich datech (pokud jsou zasilana na rozhrani) v poradi little endian (RadioTap, IEEE) nebo big endian
 * Navratova hodnota: -
 */
void Utilities::memcpy32(void * destination, const void * source, int length, bool littleEndian) {
	uint32_t * isource = (uint32_t*) source;
	uint8_t * idestination = (uint8_t *)destination;
	uint32_t _source;
	for(int i = 0; i < length; i++) {
		if(i != 0) {
			idestination++;
			isource++;	
		}	
		if(littleEndian) {
			//na little endianu nedelam nic
			if(__BYTE_ORDER == __LITTLE_ENDIAN) {
				_source = *isource;
			} else {
			//na big endianu musim prevest na little endian
				_source = __bswap_32(*isource);
			}
		} else {
			//na little endianu musim prevest na big endian
			if(__BYTE_ORDER == __LITTLE_ENDIAN) {
				_source = __bswap_32(*isource);
			} else {
			//na big endianu nedelam nic
				_source = *isource;
			}
		}
		memcpy(idestination, &_source, sizeof(uint32_t));
	}
}

/* Utilities::memcpy64
 * Popis: funkce provede nakopirovani hodnoty source do pameti definovane pomoci destination s ohledem na endianitu
 * Parametry: void * destination - ukazatel do pameti, kam se budou kopirovat data
 *            void * source - kopirovana hodnota
 *	      int length - delka kopirovanych dat
 *            bool littleEndian - informace, zdali ma byt ve vystupnich datech (pokud jsou zasilana na rozhrani) v poradi little endian (RadioTap, IEEE) nebo big endian
 * Navratova hodnota: -
 */
void Utilities::memcpy64(void * destination, const void * source, int length, bool littleEndian) {
	uint64_t * isource = (uint64_t*) source;
	uint8_t * idestination = (uint8_t *)destination;
	uint64_t _source;
	for(int i = 0; i < length; i++) {
		if(i != 0) {
			idestination++;
			isource++;	
		}	
		if(littleEndian) {
			//na little endianu nedelam nic
			if(__BYTE_ORDER == __LITTLE_ENDIAN) {
				_source = *isource;
			} else {
			//na big endianu musim prevest na little endian
				_source = __bswap_64(*isource);
			}
		} else {
			//na little endianu musim prevest na big endian
			if(__BYTE_ORDER == __LITTLE_ENDIAN) {
				_source = __bswap_64(*isource);
			} else {
			//na big endianu nedelam nic
				_source = *isource;
			}
		}
		memcpy(idestination, &_source, sizeof(uint64_t));
	}
}

/* Utilities::copyOUI
 * Popis: funkce provede nakopirovani OUI z retezce
 * Parametry: oui - misto, kde se bude ukladat oui retezec
 *            string - retezec, ze ktereho se nacita oui
 *	      lengt - delka nacitaneho oui retezce
 * Navratova hodnota: -
 */
void Utilities::copyOUI(uint8_t * oui, char * string, int length) {
	char pom [3];
	pom[2] = 0;
	for(int i = 0; i < length; i++) {
		pom[0] = string[0];
		pom[1] = string[1];
		oui[i] = strtoll((char *)pom, NULL, 16);
		string = &string[3];
	}
}

/* Utilities::hexDump
 * Popis: funkce provede vypsani dat v hexa
 * Poznamka: funkce pochazi z baliku aircrack
 * Parametry: desc - vypisovana zprava pred vlasnim vypisem dat
 *            addr - adresa zacatku dat
 *	      len  - delka vypisovanych dat
 * Navratova hodnota: -
 */
void Utilities::hexDump (const char *desc, void *addr, int len) {
    int i;
    int offset = 0;
    unsigned char buff[20];
    unsigned char *pc = (unsigned char*)addr;

    // Output description if given.
    if (desc != NULL)
        printf ("%s:\r\n", desc);

    // Process every byte in the data.
    for (i = 0; i < len; i++) {
        // Multiple of 16 means new line (with line offset).

        if ((i % 16) == 0) {
            // Just don't print ASCII for the zeroth line.
            if (i != 0)
                printf ("  %s\r\n", buff);

            // Output the offset.
            printf ("  %04x ", i);
	    offset = 0;
        }
	
	if((i % 8) == 0) {
		printf(" ");
		buff[(i % 16) + offset] = ' ';
		offset++;
	}

        // Now the hex code for the specific character.
        printf (" %02x", pc[i]);

	// And store a printable ASCII character for later.
	if ((pc[i] < 0x20) || (pc[i] > 0x7e))
	    buff[(i % 16) + offset] = '.';
	else
	    buff[(i % 16) + offset] = pc[i];

        buff[((i % 16) + 1) + offset] = '\0';
    }

    // Pad out last line if not exactly 16 characters.
    while ((i % 16) != 0) {
        printf ("   ");
	if((i % 8) == 0)
		printf(" ");
        i++;
    }

    // And print the final ASCII bit.
    printf ("  %s\r\n", buff);
}

/* Utilities::hexStr
 * Popis: funkce provede vypsani dat v hexa
 * Parametry: desc - vypisovana zprava pred vlasnim vypisem dat
 *            addr - adresa zacatku dat
 *	      len  - delka vypisovanych dat
 * Navratova hodnota: -
 */
void Utilities::hexStr (const char *desc, void *addr, int len) {
	printf("0x");
	unsigned char * _addr = (unsigned char *) addr;
	for(int i = 0; i < len; i++) {
		printf("%02x", _addr[i]);
    	}
	printf("\r\n");
}

/* Utilities::strToHex
 * Popis: funkce provede prvod retezce do hexa
 * Parametry: desc - misto, kam se ulozi vysledek
 *	      src  - vstupni retezec
 * Navratova hodnota: delka dat
 */
int Utilities::strToHex (uint8_t * dst, char * src) {
	int k = 0;
	uint8_t v1, v2;
	for(unsigned int i = 0; i < strlen(src); i += 2) {
		//cislo
	    	if(src[i] >= 0x30 && src[i] <= 0x39) { v1 = src[i] - 0x30; }
		//pismeno //prevedu na mala pismena
		else {	v1 = src[i] | 0x20;  v1 = ( v1 - 0x41 ) + 0xa;  }

		//cislo
	    	if(src[i+1] >= 0x30 && src[i+1] <= 0x39) { v2 = src[i+1] - 0x30; }
		//pismeno //prevedu na mala pismena
		else {	v2 = src[i+1] | 0x20;  v2 = ( v2 - 0x41 ) + 0xa;  }	
	    	dst[k] =  ((v1 << 4) & 0xf0) | (v2 & 0x0f);
	    	k++;	
	}
	return k;
}

/* Utilities::readCommandFromFile
 * Popis: funkce provede nacteni prikazu ze souboru, ktery je nasledne zpracovan pomoci yyparse
 * Parametry: input - soubor, ze ktereho se cte
 *	      buf   - misto, kam se ulozi prave nacteny vstup
 * Navratova hodnota: delka nactenych dat
 */
int Utilities::readCommandFromFile(FILE ** input, char ** buf) {
	if(*buf != NULL) { free(*buf); }
	static bool endRead = false;
	if(endRead) { endRead = false; return 0; }
	int CUR_MAX = 4095;
	*buf = (char *) malloc(sizeof(char) * CUR_MAX);
	int length = 0;
	char ch = 'a';
	int op = 0;
	while ( ((ch != '\n') || (op != 0) )  && (ch != EOF)) {
	    if(length == ( CUR_MAX - 4) ) {
	      CUR_MAX *= 2;
	      *buf = (char *) realloc((void *)*buf, CUR_MAX);
	    }
	    ch = getc(*input);
	    if(ch == '{') { op++; } 
	    if(ch == '}') { op--; }
	    if(ch == EOF) {endRead = true;}
	    else          { (*buf)[length] = ch; }
	    length++;
	}
	(*buf)[length++] = '\0';
	(*buf)[length++] = '\0';

	if(op) {
		printf("nespravne zadany vstup!\n");
		length = 2;
	}

	if(length == 2 || endRead) { 
		free(*buf);
		length = 0;
		endRead = false;
	}

	return length;
}
