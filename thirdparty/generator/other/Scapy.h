/* Soubor: Scapy.h
 * Autor: Pavel Svanda
 * Email: svandap@gmail.com
 * Datum vytvoreni: 9.5.2013
 * Popis: deklarace tridy Scapy, ktera slouzi jako klient k programu scapy
 */
#ifndef SCAPY_H
#define SCAPY_H

#include <string.h>
#include <string>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include "../lang/Lang.h"

#define SCAPY_COMMAND_1			"echo 'hexdump("
#define SCAPY_COMMAND_2			")' | scapy"
#define SCAPY_MAX_COMMAND_LENGTH	5000
#define SCAPY_MAX_DATA_LENGTH		5000

class Scapy {
	private:
	
	public:
		Scapy();
	       ~Scapy();
		
		static int loadData(const char * str, char * data);
	private:
		static int parseData(char * str, char * data);
};
#endif
