/* Soubor: KeyLoader.h
 * Autor: Pavel Svanda
 * Email: svandap@gmail.com
 * Datum vytvoreni: 24.11.2012
 * Popis: deklarace tridy keyLoader, ktera slouzi k nacitani klicu pomoci wpa_supplicant
 */

#ifndef KEY_LOADER_H
#define KEY_LOADER_H

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <netdb.h> 
#include <ctype.h>
#include <time.h>
#include <sys/wait.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/wait.h>
#include <pthread.h>
#include "../lang/Lang.h"

#define ERROR_LOG_LENGTH 		200

#define KL_KEY_LENGTH			2*80
#define KL_PTK_KEY			"WPA: PTK - hexdump(len=64):"
#define KL_GTK_KEY			"WPA: Group Key - hexdump(len=16):"
#define KL_GTK_KEY_ALT			"WPA: decrypted EAPOL-Key key data - hexdump(len=48):"
#define KL_IWLIST_KEY			"]:"
#define KL_IWLIST_NOT_KEY		"]: off"
#define KL_BAD_CONFIG			"Failed to read or parse configuration"
#define KL_BAD_INTERFACE		"Failed to add interface"

#define KL_WPA_SUPPLICANT		"wpa_supplicant -dd -i %s -c %s -K -Dnl80211"
#define KL_IWLIST			"iwlist %s key"
#define KL_PS				"ps -a"
#define KL_KILL				"kill -15 "
#define KL_LENGTH_WPA_SUPPLICANT 	50
#define KL_LENGTH_IWLIST 	 	20
#define KL_WPA_SUPPLICANT_PROC		"wpa_supplicant"
#define KL_WPA_SUPPLICANT_DEFUNCT 	"<defunct>"

#define KL_LENGTH_INTERFACE	200
#define KL_LENGTH_CONFIG	300
#define KL_LENGTH_PROC_NAME	200
#define KL_LENGT_KEY		240

typedef struct {
	uint8_t * ptkKey;
	uint8_t * gtkKey;
	int *     idDaemon;
	int *     idSupplicant;
	char *    interface;
	char *    config;
        pthread_mutex_t * keyDatabaseMutex;
} KeyDaemonData;

typedef struct {
	int * PipeFD;
	char * command;
} KeyDaemonWpaSupplicantData;

class KeyLoader {
	private:
		char interface[KL_LENGTH_INTERFACE];
		char config[KL_LENGTH_CONFIG];
		char procName[KL_LENGTH_PROC_NAME];

		uint8_t ptkKey [KL_LENGT_KEY];
		uint8_t gtkKey [KL_LENGT_KEY];
	
		char errorLog [ERROR_LOG_LENGTH+1];

		pthread_t       daemonThread;
		KeyDaemonData   keyDaemonData;
		pthread_mutex_t keyDatabaseMutex;
		int 		idDaemon;
		int 		idSupplicant;
		bool 		isRun;

		void getProcessID(char * string, char * procID);
	public:
		KeyLoader();
	       ~KeyLoader();

		bool startLoadKey(char * interface, char * config, char * procName);
		bool loadGTK(char * interface);
		void showKeys();
		int getGTK(uint8_t * gtk);
		int getPTK(uint8_t * ptk);
		char * getErrorLog();
		void killLastProc();
};

#endif
