/* Soubor: KeyLoadr.cpp
 * Autor: Pavel Svanda
 * Email: svandap@gmail.com
 * Datum vytvoreni: 24.11.2012
 * Popis: definici tridy keyLoader, ktera slouzi k nacitani klicu pomoci wpa_supplicant
 */
#include "KeyLoader.h"

/* KeyLoader::KeyLoader
 * Popis: konstruktor
 * Parametry: -
 * Navratova hodnota: -
 */
KeyLoader::KeyLoader() {	
	this -> isRun = false;
	this -> ptkKey[0] = 0;
	this -> gtkKey[0] = 0;
	pthread_mutex_init(&this -> keyDatabaseMutex, NULL);
}

/* KeyLoader::~KeyLoader
 * Popis: destruktor
 * Parametry: -
 * Navratova hodnota: -
 */
KeyLoader::~KeyLoader(){
	pthread_mutex_destroy(&this -> keyDatabaseMutex);
	if(this -> isRun) {
		pthread_cancel(this -> daemonThread);
		this -> killLastProc();
	}
}

/* saveKey
 * Popis: funkce provede prekopirovani klice ze vstupniho retezce do pole
 * Parametry: char * string - retezec obsahujici klic
 *            uint8_t * key - misto, kam se bude kopirovat klic
 * Navratova hodnota: -
 */
void saveKey(char * string, uint8_t * key) {
	bool saved    = false;
	int stringPos = 0;
	int keyPos    = 0;
	char ch = string[stringPos++];
	while(ch != 0 && ch != '\n' && ch != '\r') {
		if(saved) {
			if(ch != ' ' && keyPos < KL_KEY_LENGTH) {
				key[keyPos++] = (uint8_t) ch;
			}
		} else {
			if(ch == ')') {
				stringPos++;
				saved = true;
			}
		}
		ch = string[stringPos++];
	}
	key[keyPos++] = 0;
}

/* saveKeyGTKalt
 * Popis: funkce provede prekopirovani klice ze vstupniho retezce do pole
 * Parametry: char * string - retezec obsahujici klic
 *            uint8_t * key - misto, kam se bude kopirovat klic
 * Navratova hodnota: -
 */
void saveKeyGTKalt(char * string, uint8_t * key) {
	bool saved    = false;
	int stringPos = 0;
	int keyPos    = 0;
	char ch = string[stringPos++];
	while(ch != 0 && ch != '\n' && ch != '\r') {
		if(saved) {
			if(ch != ' ' && keyPos < KL_KEY_LENGTH) {
				key[keyPos++] = (uint8_t) ch;
			}
		} else {
			if(ch == ')') {
				stringPos++;
				saved = true;
			}
		}
		ch = string[stringPos++];
	}
	key[keyPos++] = 0;

	for(int i = 0; i < 2*16; i++) {
		key[i] = key[i+2*30];
	}
	key[2*16] = 0;
}

/* saveIWlistKey
 * Popis: funkce provede prekopirovani klice z vystupu IWlistu
 * Parametry: char * string - retezec obsahujici klic
 *            uint8_t * key - misto, kam se bude kopirovat klic
 * Navratova hodnota: -
 */
void saveIWlistKey(char * string, uint8_t * key) {
	bool saved    = false;
	int stringPos = 0;
	int keyPos    = 0;
	char ch = string[stringPos++];
	while(ch != 0 && ch != '\n' && ch != '\r') {
		if(saved) {
			if(ch == '(') {
				break;
			}
			if(ch != ' ' &&  ch != '-' && keyPos < KL_KEY_LENGTH) {
				key[keyPos++] = (uint8_t) ch;
			}
		} else {
			if(ch == ':') {
				stringPos++;
				saved = true;
			}
		}
		ch = string[stringPos++];
	}
	key[keyPos++] = 0;
}

/* procDaemonThread
 * Popis: vlakno demona, ktery se stara o prubezne nacitani klicu
 * Parametry: void * data - ukazatel na data, ktera jsou preda daemonu
 * Navratova hodnota: -
 */
void * procDaemonThread(void * data) {
	KeyDaemonData * keyDaemonData = (KeyDaemonData *) data;

	char command[KL_LENGTH_WPA_SUPPLICANT + KL_LENGTH_INTERFACE + KL_LENGTH_CONFIG];
	sprintf(command, KL_WPA_SUPPLICANT, keyDaemonData -> interface, keyDaemonData -> config);
	pid_t status;
	int PipeFD[2];
	if(pipe(PipeFD) != 0) {
		pthread_exit(NULL);
	}

	status = fork();
	if(status == -1) {
		pthread_exit(NULL);
	}

	if(status == 0) {
		//potomek
		close(PipeFD[STDIN_FILENO]);
		dup2(PipeFD[STDOUT_FILENO], fileno(stdout));
		dup2(PipeFD[STDOUT_FILENO], fileno(stderr));
		int status = system(command);
		exit(0);
	} else {
		//puvodni proces
		close(PipeFD[STDOUT_FILENO]);

		int stringLength = 5000;
		char * string = (char *) malloc(sizeof(char) * stringLength);
		int length = 0;
		char ch = 1;

		while(ch != EOF) {
			while((ch != '\n') && (ch != EOF)) {
				if(length == stringLength) {
					stringLength *= 2;
					string = (char *)realloc((void *)string, stringLength);
				}
				int res = read(PipeFD[STDIN_FILENO], &ch, 1);
				string[length++] = ch;
			}
			string[length++] = 0;
			length = 0;
			ch = 'a';
			if(strncmp(string, KL_PTK_KEY, strlen(KL_PTK_KEY)) == 0 ) {
				printf("\r%s\r\n>> ", LANG_KEY_PTK_LOADED); fflush(stdout);
				pthread_mutex_lock(keyDaemonData -> keyDatabaseMutex);
					saveKey(string, keyDaemonData -> ptkKey);
				pthread_mutex_unlock(keyDaemonData -> keyDatabaseMutex);	
			} else
			if(strncmp(string, KL_GTK_KEY, strlen(KL_GTK_KEY)) == 0 ) {
				printf("\r%s\r\n>> ", LANG_KEY_GTK_LOADED); fflush(stdout);
				pthread_mutex_lock(keyDaemonData -> keyDatabaseMutex);
					saveKey(string, keyDaemonData -> gtkKey);
				pthread_mutex_unlock(keyDaemonData -> keyDatabaseMutex);				
			} else
			if(strncmp(string, KL_GTK_KEY_ALT, strlen(KL_GTK_KEY_ALT)) == 0 ) {
				printf("\r%s\r\n>> ", LANG_KEY_GTK_LOADED); fflush(stdout);
				pthread_mutex_lock(keyDaemonData -> keyDatabaseMutex);
					saveKeyGTKalt(string, keyDaemonData -> gtkKey);
				pthread_mutex_unlock(keyDaemonData -> keyDatabaseMutex);				
			} else
			if(strncmp(string, KL_BAD_CONFIG, strlen(KL_BAD_CONFIG)) == 0 ) {
				printf("\r%s\r\n>>", string); fflush(stdout);
			} else
			if(strncmp(string, KL_BAD_INTERFACE, strlen(KL_BAD_INTERFACE)) == 0 ) {
				printf("\r%s\r\n>>", string); fflush(stdout);
			} else {
				//printf("%s\r\n", string); fflush(stdout);
			}
		}
		free(string);
		close(PipeFD[STDIN_FILENO]);		
	}
	pthread_exit(NULL);
}

/* KeyLoader::startLoadKey
 * Popis: funkce provede spusteni demona, ktery se stara o nacitani klicu, v okamziku, kdy se zmeni klic, provede jeho ulozeni do databaze
 * Parametry: -
 * Navratova hodnota: informace, zdali podarilo spustit demona
 */
bool KeyLoader::startLoadKey(char * interface, char * config, char * procName) {
	this -> killLastProc();
	//vytvorim vlakno, ktere se stara o nacitani klicu
	if(this -> isRun) {
		pthread_cancel(this -> daemonThread);
	}
	this -> isRun = true;
	strcpy(this -> interface, interface);
	strcpy(this -> config, config);
	strcpy(this -> procName, procName);
	//musim zrusit predchoziho daemona a nastartovat noveho
	this -> keyDaemonData.ptkKey =            this -> ptkKey;
	this -> keyDaemonData.gtkKey =            this -> gtkKey;
	this -> keyDaemonData.idDaemon =         &this -> idDaemon;
	this -> keyDaemonData.idSupplicant =     &this -> idSupplicant;
	this -> keyDaemonData.interface=          this -> interface;
	this -> keyDaemonData.config=	          this -> config;
	this -> keyDaemonData.keyDatabaseMutex = &this -> keyDatabaseMutex;

	int rc = pthread_create(&this->daemonThread, NULL, procDaemonThread, (void *)&this -> keyDaemonData);
	if(rc != 0 ) {
		sprintf(this -> errorLog, "%s", LANG_KEY_NOT_CREATE_DAEMON);
		return false;
	}
	
	return true;	
}

/* KeyLoader::loadGTK
 * Popis: funkce provede nacteni klice z iw listu
 * Parametry: char * interface - rozrhrani, ktere je pripojeno k siti
 * Navratova hodnota: informace, zdali se podarilo nacteni
 */
bool KeyLoader::loadGTK(char * interface) {
	strcpy(this -> interface, interface);

	char command[KL_LENGTH_IWLIST + KL_LENGTH_INTERFACE];
	sprintf(command, KL_IWLIST, this -> interface);
	pid_t status;
	int PipeFD[2];
	if(pipe(PipeFD) != 0) {
		return false;
	}

	status = fork();
	if(status == -1) {
		return false;
	}

	if(status == 0) {
		//potomek
		close(PipeFD[STDIN_FILENO]);
		dup2(PipeFD[STDOUT_FILENO], fileno(stdout));
		dup2(PipeFD[STDOUT_FILENO], fileno(stderr));
		int status = system(command);
		close(PipeFD[STDOUT_FILENO]);
		exit(0);
	} else {
		//puvodni proces
		close(PipeFD[STDOUT_FILENO]);

		int stringLength = 5000;
		char * string = (char *) malloc(sizeof(char) * stringLength);
		int length = 0;
		char ch;
		bool endOfRead = false;
		char * match = 0;
		while(!endOfRead) {
			while((ch != '\n') && (ch != EOF)) {
				if(length == stringLength) {
					stringLength *= 2;
					string = (char *)realloc((void *)string, stringLength);
				}
				int res = read(PipeFD[STDIN_FILENO], &ch, 1);
				if(res != 1) { endOfRead = true; break; }
				string[length++] = ch;
			}
			string[length++] = 0;
			length = 0;
			ch = 'a';
			match = strstr(string, KL_IWLIST_KEY);
			if(match != NULL) {
				match = strstr(string, KL_IWLIST_NOT_KEY);
				if(match == NULL) {
					printf("\r%s\r\n>> ", LANG_KEY_GTK_LOADED); fflush(stdout);
					pthread_mutex_lock(&this -> keyDatabaseMutex);
						saveIWlistKey(string, this -> gtkKey);
					pthread_mutex_unlock(&this -> keyDatabaseMutex);	
				}
			} else
			if(strncmp(string, KL_BAD_CONFIG, strlen(KL_BAD_CONFIG)) == 0 ) {
				printf("\r%s\r\n>>", string); fflush(stdout);
			} else
			if(strncmp(string, KL_BAD_INTERFACE, strlen(KL_BAD_INTERFACE)) == 0 ) {
				printf("\r%s\r\n>>", string); fflush(stdout);
			} else {
				//printf("%s\r\n", string); fflush(stdout);
			}
		}
		free(string);
		close(PipeFD[STDIN_FILENO]);		
	}
	
	return true;	
}


/* KeyLoader::getProcessID
 * Popis: funkce nacte id procesu ze zadaneho retezce
 * Parametry: char * string - retezec, ze ktereho je cteno id procesu
 *            char * procID - misto, kam se ulozi nacteny proces ID
 * Navratova hodnota: -
 */
void KeyLoader::getProcessID(char * string, char * procID) {
	char ch;
	int pos = 0;
	int pos2= 0;
	do {
		ch = string[pos++];
		if(ch != ' ') {
			if(ch >= '0' && ch <= '9') {
				procID[pos2++] = ch;
			} else { 
				break;
			}
		}
	} while(ch != '\n' && ch != 0 );
	procID[pos2] = 0;
}

/* KeyLoader::killLastProc
 * Popis: funkce provede zaslani signalu na zruseni predchozich procesu
 * Parametry: -
 * Navratova hodnota: -
 */
void KeyLoader::killLastProc() {
	char command[200];
	sprintf(command, KL_PS);
	pid_t status;
	int PipeFD[2];
	if(pipe(PipeFD) != 0) {
		return;
	}

	status = fork();
	if(status == -1) {
		return;
	}

	if(status == 0) {
		//potomek
		close(PipeFD[STDIN_FILENO]);
		dup2(PipeFD[STDOUT_FILENO], STDOUT_FILENO);
		int status = system(command);
		close(PipeFD[STDOUT_FILENO]);
		exit(0);
	} else {
		//puvodni proces
		close(PipeFD[STDOUT_FILENO]);
		int stringLength = 5000;
		char * string = (char *) malloc(sizeof(char) * stringLength);
		int length = 0;
		char ch = 0;
		bool endOfRead = false;
		char * match = 0;
		char procID[50];
		while(!endOfRead) {
			while((ch != '\n') && (ch != EOF)) {
				if(length == stringLength) {
					stringLength *= 2;
					string = (char *)realloc((void *)string, stringLength);
				}
				int res = read(PipeFD[STDIN_FILENO], &ch, 1);
				if(res != 1) { endOfRead = true; break; }
				string[length++] = ch;
			}
			string[length++] = 0;
			length = 0;
			ch = 'a';
			match = strstr(string, KL_WPA_SUPPLICANT_DEFUNCT);
			if(match != NULL)
				continue;
			match = strstr(string, KL_WPA_SUPPLICANT_PROC);			
			if(match != NULL) {
				this -> getProcessID(string, procID);
				sprintf(command, "%s %s", KL_KILL, procID);
				int res = system(command);
			} 
		}
		free(string);
		close(PipeFD[STDIN_FILENO]);		
	}
}

/* KeyLoader::showKeys
 * Popis: funkce provede zobrazeni klicu, ktere jsou ulozne v databazi
 * Parametry: -
 * Navratova hodnota: -
 */
void KeyLoader::showKeys() {
	pthread_mutex_lock(&this -> keyDatabaseMutex);
		printf("PTK key: 0x%s\r\n", this -> ptkKey);
		printf("GTK key: 0x%s\r\n", this -> gtkKey);
	pthread_mutex_unlock(&this -> keyDatabaseMutex);
} 

/* KeyLoader::
 * Popis:
 * Parametry: -
 * Navratova hodnota: -
 */
int KeyLoader::getGTK(uint8_t * gtk) {
	pthread_mutex_lock(&this -> keyDatabaseMutex);
		sprintf((char *)gtk, "0x%s", this -> gtkKey);
	pthread_mutex_unlock(&this -> keyDatabaseMutex);
	return strlen((char *)this -> gtkKey) + 2;
}

/* KeyLoader::
 * Popis: destruktor
 * Parametry: -
 * Navratova hodnota: -
 */
int KeyLoader::getPTK(uint8_t * ptk) {
	pthread_mutex_lock(&this -> keyDatabaseMutex);
		sprintf((char *)ptk, "0x%s", this -> ptkKey);
	pthread_mutex_unlock(&this -> keyDatabaseMutex);
	return strlen((char *)this -> ptkKey) + 2;
}

/* KeyLoader::getErrorLog
 * Popis: funkce vraci retezec s chybou
 * Parametry: -
 * Navratova hodnota: ukazatel do pameti na retezec s chybou
 */
char * KeyLoader::getErrorLog() {
	return this -> errorLog;
}
