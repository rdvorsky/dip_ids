/* Soubor: Payload.h
 * Autor: Pavel Svanda
 * Email: svandap@gmail.com
 * Datum vytvoreni: 17.11.2012
 * Popis: deklarace tridy Payload, ktera obsahuje prenasena data uvnitr wifi ramce
 */
#ifndef PAYLOAD_H
#define PAYLOAD_H

#ifdef TIXML_USE_STL
	#include <iostream>
	#include <sstream>
	using namespace std;
#else
	#include <stdio.h>
#endif

#if defined( WIN32 ) && defined( TUNE )
	#include <crtdbg.h>
	_CrtMemState startMemState;
	_CrtMemState endMemState;
#endif

#include "../tinyXml/tinyxml.h"

#include <stdint.h>
#include "../crypto/crypto.h"
#include "../frame/Crc32.h"
#include "../other/Utilities.h"
#ifndef LANG_IS_LOAD
	#include "../lang/Lang.h"
#endif

#define D_CIPHER 		   0
#define D_KEY_INDEX 		   0

#define CIPHER_PLAIN_TEXT	   0
#define CIPHER_WEP_40		   1
#define CIPHER_WEP_104		   2
#define CIPHER_TKIP		   3
#define CIPHER_CCMP		   4

#define CIPHER_STRING_PLAIN_TEXT   "plain text"
#define CIPHER_STRING_WEP_40	   "wep40"
#define CIPHER_STRING_WEP_104	   "wep104"
#define CIPHER_STRING_TKIP	   "tkip"
#define CIPHER_STRING_CCMP	   "ccmp"
#define CIPHER_STRING_NONE	   "nezname sifrovani"
	
#define CIPHER_WEP_40_IV_LENGTH	   3//24 - 24 bitu = 3 byty
#define CIPHER_WEP_104_IV_LENGTH   3//24
#define CIPHER_TKIP_IV_1_LENGTH	   2//24 bitu
#define CIPHER_TKIP_IV_2_LENGTH    4//32 bitu
#define CIPHER_CCMP_IV_LENGTH	   8//64
#define CIPHER_CCMP_NONCE	   6

#define CIPHER_WEP_40_KEY_LENGTH   5//40 - 40 bitu = 5 bytu
#define CIPHER_WEP_104_KEY_LENGTH 13//104 - 104 bitu = 13 bytu
#define CIPHER_TKIP_KEY_LENGTH    16
#define CIPHER_CCMP_KEY_LENGTH 	  13
#define CIPHER_PTK_LENGTH	  64

#define LENGTH_PAYLOAD_DATA		2000
#define LENGTH_IV			 100 
#define LENGTH_ICV			 100
#define LENGTH_KEY			 200 //slouzi pro pole, do ktereho si docasne ulozim klic
#define LENGTH_TKIP_MIC			   8

class Payload {
	private:
		uint32_t lengthData;
		uint8_t data[2*LENGTH_PAYLOAD_DATA];
		int cipher;
		uint32_t lengthIV;
		uint8_t iv[LENGTH_IV];
		uint32_t lengthKey;
		uint8_t key[LENGTH_KEY];
		uint8_t keyIndex;
	public:
		Payload();
	       ~Payload();
		
		void setData(uint8_t * data);
		uint32_t getData(uint8_t * data);
		void delData();

		uint32_t getLengthData();

		void setCipher(int cipher);
		int getCipher();
		void delCipher();

		void setIV(uint8_t * iv);
		void setIVNumber(uint64_t iv);
		uint32_t getIV(uint8_t * iv);
		void delIV();

		void setKey(uint8_t * key);
		uint32_t getKey(uint8_t * key);
		void delKey();

		void setKeyIndex(uint8_t keyIndex);
		uint8_t getKeyIndex();
		void delKeyIndex();

		void delPayload();

		uint32_t generatePayload(uint8_t * payload, int lengthIeee, int lengthLLCheader, uint8_t * llcHeader);
		void printPayload();
		void loadPayload(TiXmlElement * element);
		TiXmlElement * savePayload();

		void getCipherStr(char * str); 

	private:
		uint32_t getPaddingData(uint8_t * data, int lengthLLCheader, uint8_t * llcHeader);
		uint32_t getLengthIVNonceEncrypty();
		uint32_t getLengthKEYencrypty();
		uint32_t generateIVNonce(uint8_t * iv);
		uint32_t generateKEY(uint8_t * key);
		uint32_t generateKEYindex(uint8_t * keyIndex);
		uint32_t generateWepEncryptData(uint8_t * data, int lengthLLCheader, uint8_t * llcHeader);
		uint32_t generateTKIPEncryptData(uint8_t * data, int lengthIeee, int lengthLLCheader, uint8_t * llcHeader);
		uint32_t generateCCMPEncryptData(uint8_t * data, int lengthIeee, int lengthLLCheader, uint8_t * llcHeader);
};

#endif
