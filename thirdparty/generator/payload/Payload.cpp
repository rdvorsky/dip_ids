/* Soubor: Payload.cpp
 * Autor: Pavel Svanda
 * Email: svandap@gmail.com
 * Datum vytvoreni: 17.11.2012
 * Popis: definice tridy Payload, ktera obsahuje prenasena data uvnitr wifi ramce
 */

#include "Payload.h"

/* Payload::Payload
 * Popis: konstruktor
 * Parametry: -
 * Navratova hodnota: -
 */
Payload::Payload() {
	this -> delPayload();	
}

/* Payload::Payload
 * Popis: destruktor
 * Parametry: -
 * Navratova hodnota: -
 */
Payload::~Payload() {

}

/* Payload::setData
 * Popis: funkce provede zkopirovani dat do vnitrni promemnne Payloadu
 * Parametry: data - ukazatel na zacatek dat
 *            lengthData - delka dat
 * Navratova hodnota: -
 */
void Payload::setData(uint8_t * data) {
	int lengthData = strlen((char *) data);
 	if(lengthData > 2*LENGTH_PAYLOAD_DATA) {
		return;
	}
	Utilities::memcpy8(this -> data, &data[2], lengthData, LITTLE_ENDIAN_ORDER);
	this -> lengthData = lengthData - 2;
}

/* Payload::getData
 * Popis: funkce provede zkopirovani vnitrni promenne Payloadu na adresu ukazatele
 * Parametry: data - ukazatel, kam se budou kopirovat data
 * Navratova hodnota: delka dat
 */
uint32_t Payload::getData(uint8_t * data) {
	Utilities::memcpy8(data, "0x", 2, LITTLE_ENDIAN_ORDER);
	Utilities::memcpy8(data + 2, this -> data, this -> lengthData, LITTLE_ENDIAN_ORDER);
	data[this -> lengthData+2] = 0;
	return this -> lengthData+2;
}

void Payload::delData() {
	this -> data[0] = 0;
	this -> lengthData = 0;
}

/* Payload::getLengthPayload
 * Popis: funkce navrati delku dat Payloadu
 * Parametry: -
 * Navratova hodnota: delka dat
 */
uint32_t Payload::getLengthData() {
	return this -> lengthData;
}

/* settry a gettry
 */
void Payload::setCipher(int cipher) {
	this -> cipher = cipher;
}

int Payload::getCipher() {
	return this -> cipher;
}

void Payload::delCipher() {
	this -> cipher = D_CIPHER;
}

void Payload::setIV(uint8_t * iv) {
	int lengthIV = strlen((char *) iv);
 	if(lengthIV > LENGTH_IV) {
		return;
	}
	Utilities::memcpy8(this -> iv, &iv[2], lengthIV, LITTLE_ENDIAN_ORDER);
	this -> lengthIV = lengthIV - 2;
}

void Payload::setIVNumber(uint64_t iv) {
	this -> lengthIV = sprintf((char *)this -> iv, "%012llx", iv);
}

uint32_t Payload::getIV(uint8_t * iv) {
	Utilities::memcpy8(iv, this -> iv, this -> lengthIV, LITTLE_ENDIAN_ORDER);
	iv[this -> lengthIV] = 0;
	return this -> lengthIV;
}

void Payload::delIV() {
	this -> iv[0] = 0;
	this -> lengthIV = 0;
}

void Payload::setKey(uint8_t * key) {
	int lengthKey = strlen((char *) key);
 	if(lengthKey > LENGTH_KEY) {
		return;
	}
	Utilities::memcpy8(this -> key, &key[2], lengthKey, LITTLE_ENDIAN_ORDER);
	this -> lengthKey = lengthKey-2;
}

uint32_t Payload::getKey(uint8_t * key) {
	Utilities::memcpy8(key, "0x", 2, LITTLE_ENDIAN_ORDER);
	Utilities::memcpy8(key+2, this -> key, this -> lengthKey, LITTLE_ENDIAN_ORDER);
	key[this -> lengthKey+2] = 0;
	return this -> lengthKey+2;
}

void Payload::delKey() {
	this -> key[0] = 0;
	this -> lengthKey = 0;
}

void Payload::setKeyIndex(uint8_t keyIndex) {
	this -> keyIndex = keyIndex;
}

uint8_t Payload::getKeyIndex() {
	return this -> keyIndex;
}

void Payload::delKeyIndex() {
	this -> keyIndex = D_KEY_INDEX;
}

void Payload::delPayload() {
	this -> delData();
	this -> delCipher();
	this -> delIV();
	this -> delKey();
	this -> delKeyIndex();
}

/* Payload::getPaddingData
 * Popis: funkce provede zarovani dat na sudy pocet niblu a provede jejich prevedeni z textoveho retezce do hexa
 * Parametry: data - ukazatel do pameti, kam se data ulozi
 * Navratova hodnota: delka dat
 */
uint32_t Payload::getPaddingData(uint8_t * data, int lengthLLCheader, uint8_t * llcHeader) {
	int length = 0;
	uint8_t p;
	uint8_t pom[3];
	pom[2] = 0;

	if(lengthLLCheader) {
		Utilities::memcpy8(data, llcHeader, lengthLLCheader, LITTLE_ENDIAN_ORDER);
		data += (sizeof(uint8_t) * lengthLLCheader);
		length  += (sizeof(uint8_t) * lengthLLCheader);
	}

	int countCycle = this -> lengthData;
	if(this -> lengthData % 2 == 1) countCycle -= 1;
	for(int i = 0; i < countCycle; i = i + 2) {
		pom[0] = this -> data[i];
		pom[1] = this -> data[i+1];
		p = strtoll((char *)pom, NULL, 16);
		Utilities::memcpy8(data, &p, 1, LITTLE_ENDIAN_ORDER);
		data += sizeof(uint8_t);
		length  += sizeof(uint8_t);
	}
	if(this -> lengthData % 2 == 1) {
		pom[0] = this -> data[this->lengthData-1];
		pom[1] = '0';
		p = strtoll((char *)pom, NULL, 16);
		Utilities::memcpy8(data, &p, 1, LITTLE_ENDIAN_ORDER);
		data += sizeof(uint8_t);
		length  += sizeof(uint8_t);
	}
	return length;
}

/* Payload::getLengthIVNonceEncrypty
 * Popis: funkce provede navraceni delky sifrovacich inicializacniho vektoru
 * Parametry: -
 * Navratova hodnota: delka vektoru
 */
uint32_t Payload::getLengthIVNonceEncrypty() {
	uint32_t length = 0;
	switch(this -> cipher) {
		case CIPHER_WEP_40 : {
			length = CIPHER_WEP_40_IV_LENGTH;
		}; break;
		case CIPHER_WEP_104 : {
			length = CIPHER_WEP_104_IV_LENGTH;
		}; break;
		case CIPHER_TKIP : {
			length = CIPHER_TKIP_IV_1_LENGTH + CIPHER_TKIP_IV_2_LENGTH;	
		}; break;
		case CIPHER_CCMP : {
			length = CIPHER_CCMP_NONCE;
		}; break;
	}

	return length;
}

/* Payload::getLengthKEYencrypty
 * Popis: funkce provede navraceni delky sifrovacich klicu
 * Parametry: -
 * Navratova hodnota: delka klice
 */
uint32_t Payload::getLengthKEYencrypty() {
	uint32_t length = 0;
	switch(this -> cipher) {
		case CIPHER_WEP_40 : {
			length = CIPHER_WEP_40_KEY_LENGTH;
		}; break;
		case CIPHER_WEP_104 : {
			length = CIPHER_WEP_104_KEY_LENGTH;
		}; break;
		case CIPHER_TKIP : {
			length = CIPHER_PTK_LENGTH;
		}; break;
		case CIPHER_CCMP : {
			length = CIPHER_PTK_LENGTH;
		}; break;
	}

	return length;
}

/* Payload::generateIVNonce
 * Popis: funkce provede vygenerovani inicializacniho vektoru
 * Parametry: uint8_t * iv - misto, kam se ulozi iv
 * Navratova hodnota: delka generovanych dat
 */
uint32_t Payload::generateIVNonce(uint8_t * iv) {
	int length = 0;
	int lengthIV = this -> getLengthIVNonceEncrypty();
	uint8_t p;
	uint8_t pom[3];
	pom[2] = 0;
	int start = 0;

	int _lengthIV = 0;
	uint8_t _iv[LENGTH_IV];

	Utilities::memcpy8(_iv, this -> iv, this -> lengthIV, LITTLE_ENDIAN_ORDER);
	_lengthIV = this -> lengthIV;

	if(_lengthIV < lengthIV*2) {
		if((_lengthIV+1) < lengthIV*2) {
			pom[0] = '0'; pom[1] = '0';
			p = strtoll((char *)pom, NULL, 16);
			int l = 2*lengthIV - _lengthIV - (_lengthIV%2);
			for(int i = 0; i < l; i=i+2) {
				Utilities::memcpy8(iv, &p, 1, LITTLE_ENDIAN_ORDER);
				iv += sizeof(uint8_t);
				length  += sizeof(uint8_t);
			}	
		}
		if(_lengthIV % 2 == 1) {
			pom[0] = '0';
			pom[1] = _iv[0];
			start = 1;
			p = strtoll((char *)pom, NULL, 16);
			Utilities::memcpy8(iv, &p, 1, LITTLE_ENDIAN_ORDER);
			iv += sizeof(uint8_t);
			length  += sizeof(uint8_t);
		}
	}
	int l = lengthIV - length;
	for(int i = start; i < l*2; i=i+2) {
		pom[0] = _iv[i];
		pom[1] = _iv[i+1];
		p = strtoll((char *)pom, NULL, 16);
		Utilities::memcpy8(iv, &p, 1, LITTLE_ENDIAN_ORDER);
		iv += sizeof(uint8_t);
		length  += sizeof(uint8_t);
	}
	return length;
}

/* Payload::generateKEY
 * Popis: funkce provede vygenerovani klice
 * Parametry: uint8_t * key - misto, kam se ulozi klic
 * Navratova hodnota: delka generovanych dat
 */
uint32_t Payload::generateKEY(uint8_t * key) {
	int length = 0;
	unsigned int lengthKey = this -> getLengthKEYencrypty();
	uint8_t p;
	uint8_t pom[3];
	pom[2] = 0;
	int start = 0;
	if(this -> lengthKey < lengthKey*2) {
		if((this -> lengthKey+1) < lengthKey*2) {
			pom[0] = '0'; pom[1] = '0';
			p = strtoll((char *)pom, NULL, 16);
			int l = 2*lengthKey - this -> lengthKey - (this -> lengthKey%2);
			for(int i = 0; i < l; i=i+2) {
				Utilities::memcpy8(key, &p, 1, LITTLE_ENDIAN_ORDER);
				key += sizeof(uint8_t);
				length  += sizeof(uint8_t);
			}	
		}
		if(this -> lengthKey % 2 == 1) {
			pom[0] = '0';
			pom[1] = this -> key[0];
			start = 1;
			p = strtoll((char *)pom, NULL, 16);
			Utilities::memcpy8(key, &p, 1, LITTLE_ENDIAN_ORDER);
			key += sizeof(uint8_t);
			length  += sizeof(uint8_t);
		}
	}
	int l = lengthKey - length;
	for(int i = start; i < l*2; i=i+2) {
		pom[0] = this -> key[i];
		pom[1] = this -> key[i+1];
		p = strtoll((char *)pom, NULL, 16);
		Utilities::memcpy8(key, &p, 1, LITTLE_ENDIAN_ORDER);
		key += sizeof(uint8_t);
		length  += sizeof(uint8_t);
	}
	return length;
}

/* Payload::generateKEYindex
 * Popis: funkce provede vygenerovani key indexu
 * Parametry: uint8_t * keyIndex - misto, kam se ulozi key index
 * Navratova hodnota: delka generovanych dat
 */
uint32_t Payload::generateKEYindex(uint8_t * keyIndex) {
	uint32_t lengthData = 0;
	uint8_t ki = this -> keyIndex << 6;
	Utilities::memcpy8(keyIndex, &ki, 1, LITTLE_ENDIAN_ORDER);
	keyIndex += sizeof(uint8_t);
	lengthData += sizeof(uint8_t);
	return lengthData;
}

/* Payload::generateWEPEncryptyData
 * Popis: funkce provede vygenerovani a zasifrovani dat pomoci metody WEP, v zavislosti na typu dat (broadcat, multicast) provadi vyber delek klicu
 * Parametry: uint8_t * data - misto, kam se budou ukladat data
 *            int lengthIeee - delka IEEE hlavicky, ktera predchazi datum
 *            int lengthLLCHeader - delka llc hlavicky
 *            uint8_t * llcHeader - llc hlavicka
 * Navratova hodnota: delka vygenerovanych dat
 */
uint32_t Payload::generateWepEncryptData(uint8_t * data, int lengthLLCheader, uint8_t * llcHeader) {
	uint8_t pKey[LENGTH_KEY];
	uint8_t key[LENGTH_KEY];
	uint32_t lengthKey = this -> generateKEY(pKey);
	uint32_t lengthIV = this -> getLengthIVNonceEncrypty();
	Utilities::memcpy8(key, data - lengthIV - sizeof(uint8_t), lengthIV, LITTLE_ENDIAN_ORDER);
	Utilities::memcpy8(key+lengthIV, pKey, lengthKey, LITTLE_ENDIAN_ORDER);

    	uint32_t length = this -> getPaddingData(data, lengthLLCheader, llcHeader);

	//zabezpecime data
	Crc32 checkSum;		
	checkSum.addData(data , length);
	uint32_t crc = checkSum.getCrc32();
	Utilities::memcpy32(data + length, &crc, 1, LITTLE_ENDIAN_ORDER);
	length += sizeof(uint32_t);

	//encrypt_wep
	encrypt_wep( data, length, key, lengthIV + lengthKey);

    	return length;
}

/* Payload::generateTKIPEncryptyData
 * Popis: funkce provede vygenerovani a zasifrovani dat pomoci metody TKIP, v zavislosti na typu dat (broadcat, multicast) provadi vyber delek klicu
 * Parametry: uint8_t * data - misto, kam se budou ukladat data
 *            int lengthIeee - delka IEEE hlavicky, ktera predchazi datum
 *            int lengthLLCHeader - delka llc hlavicky
 *            uint8_t * llcHeader - llc hlavicka
 * Navratova hodnota: delka vygenerovanych dat
 */
/*
 * funkce nejdrive nakopiruje data do ramce. Pote vypocita MIC, ktery rovnou prida na konec dat. Po vypoctu MIC nakopiruje na 
 * vhodne misto IV index klice a rozsirujici IV. Nakonec provede zasifrovani.
 */
uint32_t Payload::generateTKIPEncryptData(uint8_t * data, int lengthIeee, int lengthLLCheader, uint8_t * llcHeader) {
	uint32_t length;
	uint8_t needIVlength;
	uint8_t actIVlength = 0;
	uint8_t ptk[CIPHER_PTK_LENGTH];
	uint8_t iv[CIPHER_TKIP_IV_1_LENGTH + CIPHER_TKIP_IV_2_LENGTH + 2];
	uint8_t lengthIV = 0;
	uint8_t buff[LENGTH_PAYLOAD_DATA + LENGTH_TKIP_MIC];
	this -> generateKEY(ptk);
	//int keyLength = this -> getLengthKEYencrypty();
	//nakopiruji data
	length = this -> getPaddingData(data, lengthLLCheader, llcHeader);

	//vypoctu a pridam mic
	calc_tkip_mic(data - lengthIeee, lengthIeee + length, ptk, data + length);
	length += LENGTH_TKIP_MIC;

	//musim vytvorit misto pro IV
	//potrebna mezera pro IV
	needIVlength = this -> getLengthIVNonceEncrypty() + sizeof(uint8_t) + 1;
	Utilities::memcpy8(buff, data, length, LITTLE_ENDIAN_ORDER);
	Utilities::memcpy8(data + needIVlength , buff, length, LITTLE_ENDIAN_ORDER);

	//pridam iv
	//actIVlength  += this -> generateIVNonce(data+actIVlength);
	lengthIV = this -> generateIVNonce(iv);
	Utilities::memcpy8(data + actIVlength, &iv[4], CIPHER_TKIP_IV_1_LENGTH, LITTLE_ENDIAN_ORDER);
	actIVlength += CIPHER_TKIP_IV_1_LENGTH;
	//WEPSeed
	Utilities::memcpy8(data + actIVlength, data + actIVlength - 1, 1, LITTLE_ENDIAN_ORDER);
	data[actIVlength-1] = (data[actIVlength-2] | 0x20 ) & 0x7F;
	actIVlength  += 1;
	//keyindex
	actIVlength  += this -> generateKEYindex(data+actIVlength);
	data[actIVlength-1] = 0x20 | data[actIVlength-1];
	//pridam 2 iv
	//actIVlength  += this -> generateIVNonce(data+actIVlength);
	Utilities::memcpy8(data + actIVlength, &iv[0], CIPHER_TKIP_IV_2_LENGTH, LITTLE_ENDIAN_ORDER);
	actIVlength += CIPHER_TKIP_IV_2_LENGTH;

	uint8_t pomIV[CIPHER_TKIP_IV_2_LENGTH];
	Utilities::memcpy8(pomIV, &data[actIVlength - CIPHER_TKIP_IV_2_LENGTH], CIPHER_TKIP_IV_2_LENGTH, LITTLE_ENDIAN_ORDER);
	for(int i = CIPHER_TKIP_IV_2_LENGTH - 1; i >= 0 ; i--) {
		data[actIVlength - i - 1] = pomIV[i];
	}
	length += actIVlength;

	//provedu zasifrovani pomoci tkip ... 
	//pradavam ukazatel na zacatek ieee hlavicky - data je ukazatel na zacatek kde maji byt data
	//celkovou delku ieee hlavicka + data + iv + ...
	//klic

	//misto kde zacina klic
	int startKey = 32;

	//pokud je to broadcast, tak je to gtk klic a ten zacina jinde
	//pokud prvni adresa je broadcastova, tak zmenime vychozi pozici klice
	uint8_t * addr = data - lengthIeee;
	int i = 4;
	if((addr[i] == 0xff && addr[i+1] == 0xff && addr[i+2] == 0xff && addr[i+3] == 0xff && addr[i+4] == 0xff && addr[i+5] == 0xff) ||//broadcast
	   (addr[i] == 0x01 && addr[i+1] == 0x00 && addr[i+2] == 0x5e && (addr[i+4] & 0x80 ) == 0)) {					//multicast
		startKey = 48;
	}

	encrypt_tkip( data - lengthIeee, lengthIeee + length, &ptk[startKey] );
	
	return length;
}

/* Payload::generateCCMPEncryptyData
 * Popis: funkce provede vygenerovani a zasifrovani dat pomoci metody CCMP, v zavislosti na typu dat (broadcat, multicast) provadi vyber delek klicu
 * Parametry: uint8_t * data - misto, kam se budou ukladat data
 *            int lengthIeee - delka IEEE hlavicky, ktera predchazi datum
 *            int lengthLLCHeader - delka llc hlavicky
 *            uint8_t * llcHeader - llc hlavicka
 * Navratova hodnota: delka vygenerovanych dat
 */
uint32_t Payload::generateCCMPEncryptData(uint8_t * data, int lengthIeee, int lengthLLCheader, uint8_t * llcHeader) {
	uint32_t length = 0;
	uint32_t lengthNonce = 0;
	uint8_t  lengthEncrypty;
	uint8_t nonce[LENGTH_IV];
	uint8_t encryptyData[LENGTH_PAYLOAD_DATA + 200];
	uint8_t ptk[CIPHER_PTK_LENGTH];

	//misto kde zacina klic
	int startKey = 32;

	//pokud je to broadcast, tak je to gtk klic a ten zacina jinde
	//pokud prvni adresa je broadcastova, tak zmenime vychozi pozici klice
	uint8_t * addr = data - lengthIeee;
	int i = 4;
	if(addr[i++] == 0xff && addr[i++] == 0xff && addr[i++] == 0xff && addr[i++] == 0xff && addr[i++] == 0xff && addr[i++] == 0xff) {
		startKey = 48;
	}

	this -> generateKEY(ptk);

	//nakopiruji data
	length += this -> getPaddingData(data, lengthLLCheader, llcHeader);
	//ziskam nonce
	lengthNonce += this -> generateIVNonce(nonce);

	//provedu zasifrovani vcetne vypoctu mic
	lengthEncrypty = encrypt_ccmp(encryptyData, data - lengthIeee, lengthIeee + length, lengthIeee, this -> keyIndex, nonce, &ptk[startKey]);

	//provedu prekopirovani zasifrovanych dat vcetne Ieee hlavicky
	data -= lengthIeee;
	Utilities::memcpy8(data, encryptyData, lengthEncrypty, LITTLE_ENDIAN_ORDER);

	return lengthEncrypty - lengthIeee;
}

/* Payload::generatePayload
 * Popis: funkce provede vygenerovani a zasifrovani payloadu
 * Parametry: uint8_t * payload - misto, kam se ulozi vygenerovany payload
 *            int lengthIeee - delka IEEE hlavicky, ktera jiz je v payloadu (nektere sifrovaci metody ji vyzaduji)
 *            int lengthLLCheader - delka LLC hlavicky
 *            uint8_t * llcHeader - llcHlavicka, ktera se predradi generovanym datum, pokud je delka 0, tak hlavicka neni pritomna
 * Navratova hodnota: delka vygenerovanych dat
 */
uint32_t Payload::generatePayload(uint8_t * payload, int lengthIeee, int lengthLLCheader, uint8_t * llcHeader) {
	int length = 0;

	switch(this -> cipher) {
		case CIPHER_WEP_40 : {
			length  += this -> generateIVNonce(payload+length);
			length  += this -> generateKEYindex(payload+length);
			length  += this -> generateWepEncryptData(payload+length, lengthLLCheader, llcHeader);
		}; break;
		case CIPHER_WEP_104 : {
			length  += this -> generateIVNonce(payload+length);
			length  += this -> generateKEYindex(payload+length);
			length  += this -> generateWepEncryptData(payload+length, lengthLLCheader, llcHeader);
		}; break;
		case CIPHER_TKIP : {
			length  += this -> generateTKIPEncryptData(payload, lengthIeee+length, lengthLLCheader, llcHeader);
		}; break;
		case CIPHER_CCMP : {
			length  += this -> generateCCMPEncryptData(payload+length, lengthIeee+length, lengthLLCheader, llcHeader);
		}; break;
		case CIPHER_PLAIN_TEXT :
		default : { //defaultne nesifruji ... zasilam data jako plaintext
			length += this -> getPaddingData(payload, lengthLLCheader, llcHeader);
		}
	}

	return length;
}

/* Payload::printPayload
 * Popis: funkce provede vypsani obsahu payloadu
 * Parametry: -
 * Navratova hodnota: -
 */
void Payload::printPayload() {
	char str[200];
	this -> getCipherStr(str);
	printf("\t%s\t\t%d - %s\r\n", LANG_PAYLOAD_CIPHER ,this -> cipher, str);
	printf("\t%s\t\t0x%s\r\n", LANG_PAYLOAD_KEY, this -> key);
	printf("\t%s\t0x%02d\r\n", LANG_PAYLOAD_KEY_INDEX, this -> keyIndex);
	printf("\t%s\t\t0x%s\r\n", LANG_PAYLOAD_VECTOR, this -> iv);
	printf("\t%s\t\t0x%s\r\n", LANG_PAYLOAD_DATA, this -> data);
}

/* Payload::getCipherStr
 * Popis: funkce navrati textovy retezec s informaci o pouzite sifrovaci metode
 * Parametry: char * str - misto, kam se ulozi textovy retezec
 * Navratova hodnota: -
 */
void Payload::getCipherStr(char * str) {
	switch(this -> cipher) {
		case CIPHER_PLAIN_TEXT : {
			strcpy(str, CIPHER_STRING_PLAIN_TEXT);
		}; break;
		case CIPHER_WEP_40 : {
			strcpy(str, CIPHER_STRING_WEP_40);
		}; break;
		case CIPHER_WEP_104 : {
			strcpy(str, CIPHER_STRING_WEP_104);
		}; break;
		case CIPHER_TKIP : {
			strcpy(str, CIPHER_STRING_TKIP);
		}; break;
		case CIPHER_CCMP : {
			strcpy(str, CIPHER_STRING_CCMP);
		}; break;
		default : {
			strcpy(str, CIPHER_STRING_NONE);
		};
	}
}

/* Payload::loadPayload
 * Popis: funkce provede nacteni payloadu ze struktur, ve kterych jsou ulozeny
 * Parametry: TiXmlElement * element - ukazatel na koren struktur
 * Navratova hodnota: -
 */
void Payload::loadPayload(TiXmlElement * element) {
	int pom;

	TiXmlHandle hpayloadElement(element);
	
	TiXmlElement * cipherElement = hpayloadElement.FirstChild( "cipher" ).Element();
	if(cipherElement) { cipherElement -> QueryIntAttribute("value", &pom); this -> cipher = (uint32_t) pom; }

	TiXmlElement * dataElement = hpayloadElement.FirstChild( "data" ).Element();
	if(dataElement) {
		if(dataElement -> GetText() != NULL) {
			strncpy((char *)this -> data, dataElement -> GetText(), LENGTH_PAYLOAD_DATA);
			this -> lengthData = strlen((char *)this -> data);
		}
	}

	TiXmlElement * vectorElement = hpayloadElement.FirstChild( "vector" ).Element();
	if(vectorElement) {
		if(vectorElement -> GetText() != NULL) {
			strncpy((char *)this -> iv, vectorElement -> GetText(), LENGTH_IV);
			this -> lengthIV = strlen((char *)this -> iv);
		}
	}

	TiXmlElement * keyElement = hpayloadElement.FirstChild( "key" ).Element();
	if(keyElement) {
		if(keyElement -> GetText() != NULL) {
			strncpy((char *)this -> key, keyElement -> GetText(), LENGTH_KEY);
			this -> lengthKey = strlen((char *)this -> key);
		}
	}

	TiXmlElement * keyIndexElement = hpayloadElement.FirstChild( "keyIndex" ).Element();
	if(keyIndexElement) { keyIndexElement -> QueryIntAttribute("value", &pom); this -> keyIndex = (uint8_t) pom; }
}

/* Payload::savePayload
 * Popis: funkce provede vygenerovani struktur, ze kterych je nasledne generovan soubor s ulozenymi daty
 * Parametry: -
 * Navratova hodnota: ukazatel na koren vygenerovanych struktur
 */
TiXmlElement * Payload::savePayload() {
	TiXmlElement * payloadElement = new TiXmlElement( "payload" );

	TiXmlElement * cipherElement = new TiXmlElement( "cipher" );
	cipherElement -> SetAttribute("value", this -> cipher );
	payloadElement -> LinkEndChild( cipherElement ); 

	TiXmlElement * dataElement = new TiXmlElement( "data" );
	dataElement -> LinkEndChild(new TiXmlText((char *)this -> data));
	payloadElement -> LinkEndChild( dataElement );

	TiXmlElement * vectorElement = new TiXmlElement( "vector" );
	vectorElement -> LinkEndChild(new TiXmlText((char *)this -> iv));
	payloadElement -> LinkEndChild( vectorElement );

	TiXmlElement * keyElement = new TiXmlElement( "key" );
	keyElement -> LinkEndChild(new TiXmlText((char *)this -> key));
	payloadElement -> LinkEndChild( keyElement );

	TiXmlElement * keyIndexElement = new TiXmlElement( "keyIndex" );
	keyIndexElement -> SetAttribute("value", this -> keyIndex );
	payloadElement -> LinkEndChild( keyIndexElement ); 

	return payloadElement;
}
