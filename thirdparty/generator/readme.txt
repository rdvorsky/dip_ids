Potrebne programy a knihovny:
	K prekladu je zapotrebi prekladce g++. Neni-li na pocitaci pritomen, lze jej doinstalovat pomoci prikazu
		apt-get install g++

	Program pouziva knihovnu libpcap. Pokud neni na pocitaci nainstalovana, lze ji nainstalovat pomoci nasledujiciho prikazu
		apt-get install libpcap0.8-dev

	Program pouziva knihovnu openssl. Pokud neni na pocitaci nainstalovana, lze ji nainstalovat pomoci nasledujiciho prikazu
		apt-get install libssl-dev

	Program dale vyuziva programy flex a yacc k vygenerovani gramatiky. Pokud nejsou nainstalovany v pocitaci, lze je doinstalovat pomoci
		apt-get install byacc flex bison

	Program vyuziva aplikaci scapy, ktera je volana pomoci odpovidajici funkce. Chcete-li tutu funkci vyuzivat, je zapotrebi provest instalaci tohoto programu, pokud se na pocitaci nenachazi.
		apt-get install python-scapy

Preklad:
	Preklad se provede pomoci prikazu "make"

Spusteni:
	Spusteni lze provest pomoci prikazu "./wifipacket <interface>", kde <interface> je nazev sitoveho rozhrani v monitorovacim modu.
	
	Spusteni programu lze take provest pomoci prikazu "make run", kde se predpoklada nazev rozhrani <interface> jako "mon0".
