/* Soubor: RadioTapHeader.h
 * Autor: Pavel Svanda
 * Email: svandap@gmail.com
 * Datum vytvoreni: 25.6.2012
 * Popis: deklarace tridy RadioTapHeader, ktera slouzi jako reprezentace RadioTap hlavicky
 */
#ifndef RADIOTAPHEADER_H
#define RADIOTAPHEADER_H

#include "Header.h"

//jednotlive bity present pole
#define TSFT				 0
#define FLAGS				 1
#define RATE				 2
#define CHANNEL				 3
#define FHSS				 4
#define ANTENNA_SIGNAL   		 5
#define ANTENNA_NOISE 			 6
#define LOCK_QUALITY			 7
#define TX_ATTENUATION			 8
#define DB_TX_ATTENUATION		 9
#define DBM_TX_POWER			10
#define ANTENNA 			11
#define DB_ANTENNA_SIGNAL		12
#define DB_ANTENNA_NOISE		13
#define RX_FLAGS			14
#define RTS_RETRIES			16
#define DATA_RETRIES			17
//#define NOTES				18
#define MCS				19
//#define RESET_TO_RADIOTAP_NAMESPACE	29
#define VENDOR_NAMESPACE		30
#define EXT				31

//flags
#define SENT_RECEIVE_DURING_CFP		 0
#define SENT_RECEIVE_WITH_SHORT_PREAMBLE 1
#define SENT_RECEIVE_WITH_WEP_ENCRYPTION 2
#define SENT_RECEIVE_WITH_FRAGMENTATION	 3
#define FRAME_INCLUDES_FCS		 4
#define FRAME_HAS_PADDING_802_11	 5
#define FRAME_FAILED_FCS_CHECK		 6
#define FRAME_USED_SHORT_GUARD_INTERVAL  7

#define V_SENT_RECEIVE_DURING_CFP	   0x01
#define V_SENT_RECEIVE_WITH_SHORT_PREAMBLE 0x02
#define V_SENT_RECEIVE_WITH_WEP_ENCRYPTION 0x04
#define V_SENT_RECEIVE_WITH_FRAGMENTATION  0x08
#define V_FRAME_INCLUDES_FCS		   0x10
#define V_FRAME_HAS_PADDING_802_11	   0x20
#define V_FRAME_FAILED_FCS_CHECK	   0x40
#define V_FRAME_USED_SHORT_GUARD_INTERVAL  0x80

//channel flags
#define TURBO_CHANNEL			 4
#define CCK_CHANNEL			 5
#define OFDM_CHANNEL			 6
#define GHZ2_SPECTRUM_CHANNEL		 7
#define GHZ5_SPECTRUM_CHANNEL		 8
#define ONLY_PASSIVE_SCAN_ALLOWED	 9
#define DYNAMIC_CCK_OFDM_CHANNEL	10
#define GFSK_CHANNEL		        11

#define V_TURBO_CHANNEL			 0x0010
#define V_CCK_CHANNEL			 0x0020
#define V_OFDM_CHANNEL			 0x0040
#define V_GHZ2_SPECTRUM_CHANNEL		 0x0080
#define V_GHZ5_SPECTRUM_CHANNEL		 0x0100
#define V_ONLY_PASSIVE_SCAN_ALLOWED	 0x0200
#define V_DYNAMIC_CCK_OFDM_CHANNEL	 0x0400
#define V_GFSK_CHANNEL		         0x0800

//RX flags
#define RX_RESERVED			 0
#define PLCP_CRC_CHECK_FAILED            1

#define V_RX_RESERVED			 0x0001
#define V_PLCP_CRC_CHECK_FAILED          0x0002

//MCS flags
#define	BANDWIDTH			0
#define BANDWIDTH_MCS_FLAGS		3
#define MCS_INDEX_KNOWN			1
#define GUARD_INTERVAL			2
#define HT_FORMAT			3
#define FEC_TYPE			4
#define MCS_RESERVED			5

#define	V_BANDWIDTH			0x01
#define V_BANDWIDTH_MCS_FLAGS		0x03
#define V_MCS_INDEX_KNOWN		0x02
#define V_GUARD_INTERVAL		0x04
#define V_HT_FORMAT			0x08
#define V_FEC_TYPE			0x10
#define V_MCS_RESERVED			0xe0

//Defaultni hodnoty
#define D_VERSION 			0
#define D_PAD				0
#define D_LEN				8
#define D_PRESENT			0
#define D_TSFT_MACTIME			0
#define D_FLAGS				0
#define D_ANTENNA			0
#define D_ANTENNA_NOISE			0
#define D_ANTENNA_SIGNAL		0
#define D_CHANNEL_FREQUENCY		0
#define D_CHANNEL_FLAGS			0
#define D_FHSS_HOP_SET			0
#define D_FHSS_HOP_PATTERN		0
#define D_LOCK_QUALITY			0	
#define D_MCS_KNOWN			0
#define D_MCS_FLAGS			0
#define D_MCS_MCS			0
#define D_RX_FLAGS			0
#define D_RATE				0
#define D_TX_ATTENUATION		0
#define D_VENDOR_NAMESPACE_OUI_1	0
#define D_VENDOR_NAMESPACE_OUI_2	0
#define D_VENDOR_NAMESPACE_OUI_3	0
#define D_VENDOR_NAMESPACE_SUB_NAMESPACE 0
#define D_VENDOR_NAMESPACE_SKIP_LENGTH  0
#define D_DB_TX_ATTENUATION		0
#define D_DB_ANTENNA_NOISE		0
#define D_DB_ANTENNA_SIGNAL		0
#define D_DBM_TX_POWER			0
#define D_RTS_RETRIES			0
#define D_DATA_RETRIES			0
#define D_NOTES				0
#define D_RESET_TO_RADIO_NAMESPACE	0

#define INT_8				1
#define INT_16				2
#define INT_32				4
#define INT_64				8

#define LENGTH_HEADER	     2000

class RadioTapHeader : public Header {
	private:
		//povinne polozky
		uint8_t version;
		uint8_t pad;
		uint16_t len;
		uint32_t present;
		//rozsirujici polozky
		uint64_t TSFTmactime;
		uint8_t flags;
		uint8_t antenna;
		int8_t antennaNoise;
		int8_t antennaSignal;
		uint16_t channelFrequency;
		uint16_t channelFlags;
		uint8_t FHSShopSet;
		uint8_t FHSShopPattern;		
		uint16_t lockQuality;
		uint8_t MCSknown;
		uint8_t MCSflags;
		uint8_t MCSmcs;
		uint16_t RXflags;
		uint8_t rate;		
		uint16_t TXattenuation;
		uint8_t vendorNamespaceOUI [4];
		uint8_t vendorNamespaceSubNamespace;
		uint16_t vendorNamespaceSkipLength;
		uint16_t dbTXattenuation;
		uint8_t dbAntennaNoise;
		uint8_t dbAntennaSignal;
		int8_t dbmTXpower;
		uint8_t RTSretries;
		uint8_t dataRetries;
		//Notes
		//Reset to Radiotap Namespace

		uint32_t insertPadding(uint8_t ** data, uint32_t lengthData, uint8_t neededPadding);

	public:
		RadioTapHeader();
	       ~RadioTapHeader();

		//GETry a SETry
		//povinne polozky
		//version
		void setVersion(uint8_t version);
		uint8_t getVersion();
		void delVersion();
		//pad
		void setPad(uint8_t pad);
		uint8_t getPad();
		void delPad();
		//len
		//void setLen(uint16_t len);
		uint16_t getLen();
		void delLen();
		//present
		void setPresent(uint32_t present);
		uint32_t getPresent();
		void delPresent();
		//nastaveni jednotlivych bitu pole present
		void setBitPresent(uint8_t numberBit, uint8_t valueBit);
		uint8_t getBitPresent(uint8_t numberBit);

		//rozsirujici polozky
		void setTSFTmactime(uint64_t TSFTmactime);
		uint64_t getTSFTmactime();
		void delTSFTmactime();

		void setFlags(uint8_t flags);
		void setBitFlags(uint8_t numberBit, uint8_t valueBit);
		uint8_t getFlags();
		uint8_t getBitFlags(uint8_t numberBit);
		void delFlags();

		void setFlagsKeyword(Keyword keywordInfo);

		void setRate(uint8_t rate);
		uint8_t getRate();
		void delRate();

		void setChannelFrequency(uint16_t channelFrequency);
		uint16_t getChannelFrequency();
		void delChannelFrequency();

		void setChannelFlags(uint16_t channelFlags);
		void setBitChannelFlags(uint8_t numberBit, uint8_t valueBit);
		uint16_t getChannelFlags();
		uint8_t getBitChannelFlags(uint8_t numberBit);
		void delChannelFlags();

		void setChannelFlagsKeyword(Keyword keywordInfo);

		void delChannel();

		void setFHSShopSet(uint8_t FHSShopSet);
		uint8_t getFHSShopSet();
		void delFHSShopSet();

		void setFHSShopPattern(uint8_t FHSShopPattern);		
		uint8_t getFHSShopPattern();
		void delFHSShopPattern();

		void delFHSShop();

		void setAntennaSignal(int8_t antennaSignal);
		int8_t getAntennaSignal();
		void delAntennaSignal();

		void setAntennaNoise(uint8_t antennaNoise);
		int8_t getAntennaNoise();
		void delAntennaNoise();

		void setLockQuality(uint16_t lockQuality);
		uint16_t getLockQuality();
		void delLockQuality();

		void setTXattenuation(uint16_t TXattenuation);
		uint16_t getTXattenuation();
		void delTXattenuation();

		void setDbTXattenuation(uint16_t dbTXattenuation);
		uint16_t getDbTXattenuation();
		void delDbTXattenuation();

		void setDbmTXpower(int8_t dbmTXpower);
		int8_t getDbmTXpower();
		void delDbmTXpower();

		void setAntenna(uint8_t antenna);
		uint8_t getAntenna();
		void delAntenna();

		void setDbAntennaSignal(uint8_t dbAntennaSignal);
		uint8_t getDbAntennaSignal();
		void delDbAntennaSignal();

		void setDbAntennaNoise(uint8_t dbAntennaNoise);
		uint8_t getDbAntennaNoise();
		void delDbAntennaNoise();

		void setRXflags(uint16_t RXflags);
		void setBitRXflags(uint8_t numberBit, uint8_t valueBit);
		uint16_t getRXflags();
		uint8_t getBitRXflags(uint8_t numberBit);
		void delRXflags();

		void setRXflagsKeyword(Keyword keywordInfo);

		void setRTSretries(uint8_t RTSretries);
		uint8_t getRTSretries();
		void delRTSretries();

		void setDataRetries(uint8_t dataRetries);
		uint8_t getDataRetries();
		void delDataRetries();

		void setMCSknown(uint8_t MCSknown);
		void setBitMCSknown(uint8_t numberBit, uint8_t valueBit);
		uint8_t getMCSknown();
		uint8_t getBitMCSknown(int numberBit);	
		void delMCSknown();
		
		void setMCSflags(uint8_t MCSflags);
		void setBitMCSFlags(uint8_t numberBit, uint8_t valueBit);
		uint8_t getMCSflags();
		uint8_t getBitMCSflags(int numberBit);
		void delMCSflags();

		void setMCSflagsKeyword(Keyword keywordInfo);

		void setMCSmcs(uint8_t MCSmcs);
		uint8_t getMCSmcs();
		void delMCSmcs();

		void delMCS();

		void setVendorNamespaceOUI(uint8_t * vendorNamespaceOUI);
		uint8_t  * getVendorNamespaceOUI();
		void delVendorNamespaceOUI();

		void setVendorNamespaceSubNamespace(uint8_t vendorNamespaceSubNamespace);		
		uint8_t getVendorNamespaceSubNamespace();
		void delVendorNamespaceSubNamespace();

		void setVendorNamespaceSkipLength(uint16_t vendorNamespaceSkipLength);
		uint16_t getVendorNamespaceSkipLength();
		void delVendorNamespaceSkipLength();

		void delVendorNamespace();

		//dalsi funkce
		virtual uint32_t generateHeader(uint8_t * data);
		virtual void printHeader();
		virtual void deleteHeader();
		virtual TiXmlElement * saveHeader();
		virtual void loadHeader(TiXmlElement * element);

};

#endif
