/* Soubor: IeeeHeader.h
 * Autor: Pavel Svanda
 * Email: svandap@gmail.com
 * Datum vytvoreni: 3.7.2012
 * Popis: deklarace tridy IeeeHeader, ktera slouzi jako reprezentace Ieee hlavicky
 */
#ifndef IEEEHEADER_H
#define IEEEHEADER_H

#include "Header.h"

//jednotlive bity frame control
#define FC_PROTOCOL1		 	 0
#define FC_PROTOCOL2			 1
#define FC_TYPE1			 2
#define FC_TYPE2	 		 3
#define FC_SUB_TYPE1 		         4
#define FC_SUB_TYPE2		         5
#define FC_SUB_TYPE3		         6
#define FC_SUB_TYPE4		         7
#define FC_TO_DS		   	 8
#define FC_FROM_DS			 9
#define FC_MORE_FRAG			10
#define FC_RETRY			11
#define FC_PWR_MGMT			12
#define FC_MORE_DATA			13
#define FC_PROTECTED_FRAME		14
#define FC_ORDER			15

//Typy ramcu
#define T_MANAGEMENT_FRAME		0
#define T_CONTROL_FRAME			1
#define T_DATA_FRAME			2
//Subtypy ramcu
#define ST_DATA				0
#define ST_ASSOCIATION_REQUEST	        0
#define ST_REASSOCIATION_REQUEST	2
#define ST_REASSOCIATION_RESPONSE       3
#define ST_PROBE_REQUEST		4
#define ST_PROBE_RESPONSE	        5
#define ST_BEACON			8
#define ST_ATIM				9
#define ST_DISASSOCIATION	       10
#define ST_AUTHETICATION	       11
#define ST_DEAUTHETICATION	       12
#define ST_PS_POLL		       10
#define ST_RTS			       11
#define ST_CTS			       12
#define ST_ACK			       13
#define ST_QOS_DATA			8

//Defaultni hodnoty
#define D_FRAME_CONTROL			  0
#define D_PROTOCOL			  0
#define D_TYPE				  0
#define D_SUB_TYPE			  0
#define D_TO_DS				  0
#define D_FROM_DS			  0
#define D_MORE_FRAG			  0
#define D_RETRY				  0
#define D_PWR_MGMT			  0
#define D_MORE_DATA			  0
#define D_PROTECTED_FRAME		  0
#define D_ORDER				  0
#define D_DURATION			  0
#define D_ADDR1				  0
#define D_ADDR2				  0
#define D_ADDR3				  0
#define D_ADDR4				  0
#define D_ADDR1S			  "00:00:00:00:00:00"
#define D_ADDR2S			  "00:00:00:00:00:00"
#define D_ADDR3S			  "00:00:00:00:00:00"
#define D_ADDR4S			  "00:00:00:00:00:00"
#define D_AID				  0
#define D_SEQ_CTL			  0
#define D_TIMESTAMP			  0
#define D_BEACON_INTERVAL		  0
#define D_CAPABILITY_INFO		  0
#define D_SSID				  ""
#define D_FH_PARAMETER_SET_DWELL_TIME     0		
#define D_FH_PARAMETER_SET_HOP_SET	  0
#define D_FH_PARAMETER_SET_HOP_PATTERN	  0
#define D_FH_PARAMETER_SET_HOP_INDEX	  0
#define D_DS_PARAMETER_SET		  0
#define D_IBSS_PARAMETER_SET		  0
#define D_TIM_DTIM_COUNT		  0
#define D_TIM_DTIM_PERIOD		  0
#define D_TIM_BITMAP_CONTROL		  0
#define D_TIM_PARTICIAL_VIRTUAL_BITMAP	  "0"
#define D_LOCAL_POWER_CONSTRAINT	  0
#define D_COUNTRY_STRING		  ""
#define D_FIRST_CHANNEL_NUMBER		  0
#define D_NUMBER_OF_CHANNEL		  0
#define D_MAX_TRANSMIT_POWER		  0
#define D_CHANNEL_SWITCH_MODE		  0
#define D_NEW_CHANNEL_NUMBER		  0
#define D_CHANNEL_SWITCH_COUNT		  0
#define D_QUIET_COUNT			  0
#define D_QUIET_PERIOD	   		  0
#define D_QUIET_DURATION  		  0 
#define D_QUIET_OFFSET			  0 
#define D_TRANSMIT_POWER		  0
#define D_LINK_MARGIN			  0
#define D_ERP				  0
#define D_SUPPORTED_RATE		  0
#define D_EXTENDED_SUPPORTED_RATE	  0
#define D_REASON_CODE			  0
#define D_LISTEN_INTERVAL		  0
#define D_CFP_COUNT			  0
#define D_CFP_PERIOD			  0
#define D_CFP_MAX_DURATION		  0
#define D_CFP_REMAINING			  0
#define D_RSN_VERSION			  1
#define D_GROUP_CIPHER_SUITE_OUI_0	  0x00
#define D_GROUP_CIPHER_SUITE_OUI_1	  0x0F
#define D_GROUP_CIPHER_SUITE_OUI_2	  0xAC
#define D_GROUP_CIPHER_SUITE_TYPE	  0
#define D_AUTHENTICATION_SUITE_OUI_0	  0x00
#define D_AUTHENTICATION_SUITE_OUI_1	  0x0F
#define D_AUTHENTICATION_SUITE_OUI_2	  0xAC
#define D_AUTHENTICATION_SUITE_TYPE	  0
#define D_RSN_CAPABILITIES 		  0
#define D_STATUS_CODE	 		  0
#define D_ASSOCIATION_ID		  0
#define D_AUTHENTICATION_ALGORITHM_NUMBER 0
#define D_AUTHENTICATION_SEQUENCE_NUMBER  0
#define D_CHALLENGE_TEXT		  "0"
#define D_QOS				  0

//cisla bitu copability info
#define CI_ESS				0
#define CI_IBSS				1
#define CI_CF_POLLABLE			2
#define CI_CF_POLL_REQUEST		3
#define CI_PRIVACY			4
#define CI_SHORT_PREAMBULE		5
#define CI_PBCC				6
#define CI_CHANNEL_AGILITY		7
#define CI_SHORT_SLOT_TIME	       10
#define CI_DSSS_OFDM		       13

//odpovidajici hexa hodnota
#define V_CI_ESS		     0x0001
#define V_CI_IBSS		     0x0002
#define V_CI_CF_POLLABLE	     0x0004
#define V_CI_CF_POLL_REQUEST	     0x0008 	
#define V_CI_PRIVACY		     0x0010
#define V_CI_SHORT_PREAMBULE	     0x0020
#define V_CI_PBCC		     0x0040	
#define V_CI_CHANNEL_AGILITY	     0x0080	
#define V_CI_SHORT_SLOT_TIME	     0x0400
#define V_CI_DSSS_OFDM		     0x2000

//cisla bitu erp
#define ERP_NON_ERP_PRESENT		0
#define ERP_USE_PROTECTION		1
#define ERP_BARKER_PREAMBLE		2

#define V_ERP_NON_ERP_PRESENT		0x1
#define V_ERP_USE_PROTECTION		0x2
#define V_ERP_BARKER_PREAMBLE		0x4

//typy sifrovani
#define V_GROUP_CIPHER_SUITE		0x0
#define V_WEP_40			0x1
#define V_WEP_104			0x5
#define V_TKIP				0x2
#define V_CCMP				0x4
#define V_PRESHARED_MASTER_KEY		0x1
#define V_PRE_SHARED_KEY		0x2

//texty
#define UNKNOWN				"unknown"
#define TYP_CONTROL_STRING	 	"control"	
#define TYP_MANAGEMENT_STRING	 	"management"	
#define TYP_DATA_STRING		 	"Data"	
#define SUB_TYP_RTS			"rts"
#define SUB_TYP_CTS			"cts"
#define SUB_TYP_ACK			"ack"
#define SUB_TYP_PS_POLL			"ps-poll"
#define SUB_TYP_BEACON			"beacon"
#define SUB_TYP_PROBE_REQUEST		"probe Request"
#define SUB_TYP_PROBE_RESPONSE		"probe Response"
#define SUB_TYP_ATIM			"atim"
#define SUB_TYP_DEAUTHETICATION		"deauthetication"
#define SUB_TYP_DISASSOCIATION		"disassociation"
#define SUB_TYP_ASSOCIATION_REQUEST	"association Request"
#define SUB_TYP_REASSOCIATION_REQUEST	"reassociation Request"
#define SUB_TYP_REASSOCIATION_RESPONSE	"reassociation Response"
#define SUB_TYP_AUTHETICATION	        "authentication"
#define SUB_TYP_QOS_DATA		"qos data"

//delky poli
#define L_MAC_ADDRESS_STRING	             20
#define L_SSID_MAX_LENGTH		     32
#define L_GEN_SIZE_ADDR			      6 //delka adresy - informace pouzita pri generovani hlavicky - o kolik mam posunotu ukazatel pro generovani dalsi polozky
#define L_TIM_PARTIAL_VIRTUAL_BITMAP_LENGTH 251*2 + 2
#define L_COUNTRY_STRING_LENGTH		      3
#define L_COUNTRY_IE_LENGTH	            256
#define L_SUPPORTED_RATE_LENGTH	            256
#define L_EXTENDED_SUPPORTED_RATE_LENGTH    256
#define L_OUI_LENGTH			      3
#define L_PAIRWISE_CIPHER_SUITE_COUNT         4
#define L_AUTHENTICATION_SUITE_COUNT	      4
#define L_CHALLENGE_TEXT_LENGTH	            253


//definice parametru pole obsahujici informace, ktere polozky z tagget parametru jsou obsazeny v hlavice
#define TP_COUNT_TAGGED_PARAMETERS	15 //velikost pole taggedParameters
#define TP_FH_PARAMETER_SET	  	 0 //index po pole taggedParameters
#define TP_DS_PARAMETER_SET		 1
#define TP_CF_PARAMETER_SET		 2
#define TP_IBSS_PARAMETER_SET		 3
#define TP_TRAFFIC_INDICATION_MAP	 4
#define TP_COUNTRY			 5
#define TP_POWER_CONSTRAINT		 6
#define TP_CHANNEL_SWITCH		 7
#define TP_QUIET			 8
#define TP_TPC_REPORT			 9
#define TP_ERP_INFORMATION		10
#define TP_SUPPORTED_RATE		11
#define TP_EXTENDED_SUPPORTED_RATE	12
#define TP_RSN				13

//struktura obsahujici trojici informaci pro Country IE
typedef struct {
	uint8_t firstChannelNumber;
	uint8_t numberOfChannel;
	uint8_t maxTransmitPower;
} ciect; //country information elemetn constraint triplet

typedef struct {
	uint8_t suiteOUI[L_OUI_LENGTH];
	uint8_t suiteType;
} suite;

class IeeeHeader : public Header {
	private:
		bool taggedParameters [TP_COUNT_TAGGED_PARAMETERS];

		uint16_t frameControl;		
		uint16_t duration;	
		//IeeeHeader obsahuje az 4 adresy ... dle typu ramce maji ruzne vyznamy, proto jsou pojmenovany obecne a dle typu se jim priradi vyznam	
		uint64_t addr1;
		char addr1s[L_MAC_ADDRESS_STRING];
		uint64_t addr2;
		char addr2s[L_MAC_ADDRESS_STRING];
		uint64_t addr3;
		char addr3s[L_MAC_ADDRESS_STRING];
		uint64_t addr4;
		char addr4s[L_MAC_ADDRESS_STRING];

		uint16_t aid;
		uint16_t seqCtl;
		uint64_t timestamp;
		uint16_t beaconInterval;
		//copability info
		uint16_t capabilityInfo;		

		char ssid[L_SSID_MAX_LENGTH+1];

		//hf parameter set
		uint8_t fhParameterSetLength;
		uint16_t fhParameterSetDwellTime;
		uint8_t fhParameterSetHopSet;
		uint8_t fhParameterSetHopPattern;
		uint8_t fhParameterSetHopIndex;
		uint8_t dsParameterSet;
		uint16_t ibssParameterSet;

		//tim
		uint8_t timDtimCount;
		uint8_t timDtimPeriod;
		uint8_t timBitmapControl;
		char timPartialVirtualBitmap [L_TIM_PARTIAL_VIRTUAL_BITMAP_LENGTH+1];

		//country information element
		char countryString[L_COUNTRY_STRING_LENGTH+1];
		ciect countryConstraintTriplet [L_COUNTRY_IE_LENGTH]; //pole obsahujici jednotlive struktury dat
		bool countryConstraintTripletFlag [L_COUNTRY_IE_LENGTH]; //pole obsahujici informaci, ktere z poli je obsazeno a ktere je prazdne

		//power constraint
		uint8_t localPowerConstraint;
		
		//channel switch
		uint8_t channelSwitchMode;
		uint8_t newChannelNumber;
		uint8_t channelSwitchCount;

		//quiet
		uint8_t quietCount;
		uint8_t quietPeriod;
		uint16_t quietDuration;
		uint16_t quietOffset;
	
		//TPC report
		uint8_t transmitPower;
		uint8_t linkMargin;

		//ERP
		uint8_t erp;

		//Supported rates
		uint8_t supportedRate[L_SUPPORTED_RATE_LENGTH];
		bool supportedRateFlag[L_SUPPORTED_RATE_LENGTH];

		//Extended supported rates
		uint8_t extendedSupportedRate[L_EXTENDED_SUPPORTED_RATE_LENGTH];
		bool extendedSupportedRateFlag[L_EXTENDED_SUPPORTED_RATE_LENGTH];

		//reason code
		uint16_t reasonCode;
	
		//listen interval
		uint16_t listenInterval;

		//cf parameter set
		uint8_t cfpCount;
		uint8_t cfpPeriod;
		uint16_t cfpMaxDuration;
		uint16_t cfpRemaining;

		//RSN
		uint16_t rsnVersion;
		suite groupCipherSuite;
		bool pairwiseCipherSuiteItem[L_PAIRWISE_CIPHER_SUITE_COUNT];
		suite pairwiseCipherSuite[L_PAIRWISE_CIPHER_SUITE_COUNT];
		bool authenticationSuiteItem[L_AUTHENTICATION_SUITE_COUNT];
		suite authenticationSuite[L_AUTHENTICATION_SUITE_COUNT];
		uint16_t rsnCapabilities;

		//status code
		uint16_t statusCode;
	
		//association id
		uint16_t associationId;
		
		//authentication algorithm number
		uint16_t authenticationAlgorithmNumber;	
	
		//authentication algorithm number
		uint16_t authenticationSequenceNumber;	

		//challenge text
		char challengeText[L_CHALLENGE_TEXT_LENGTH * 2 + 1];
		
		//qos
		uint16_t qos;

		uint64_t stringToMac(const char * s);
		bool isSetAnyCountryTriplet();
		bool isSetAnySupportedRate();
		bool isSetAnyExtendedSupportedRate();
		void copyOUI(uint8_t * oui, char * string, int length);
	public:
		IeeeHeader();
	       ~IeeeHeader();

		virtual uint32_t generateHeader(uint8_t * data);
		virtual void printHeader();
		virtual void deleteHeader();
		virtual TiXmlElement * saveHeader();
		virtual void loadHeader(TiXmlElement * element);
		
		void getTypeString(char * string);
		void getSubTypeString(char * string);

		void setDATAframe();
		void setQosDATAframe();
		void setRTSframe();
		void setCTSframe();
		void setACKframe();
		void setPSPOLLframe();
		void setBEACONframe();
		void setPROBEREQUESTframe();
		void setPROBERESPONSEframe();
		void setATIMframe();
		void setDEAUTHETICATIONframe();
		void setDISASSOCIATIONframe();
		void setASSOCIATIONREQUESTframe();
		void setREASSOCIATIONREQUESTframe();
		void setREASSOCIATIONRESPONSEframe();
		void setAUTHENTICATIONframe();

		/*Pole frame control*/
		void setFrameControl(uint16_t frameControl);
		uint16_t getFrameControl();
		void delFrameControl();
		
		void setBitFrameControl(uint8_t numberBit, uint8_t valueBit);
		uint8_t getBitFrameControl(uint8_t numberBit);

		void setProtocol(uint8_t protocol);
		uint8_t getProtocol();
		void delProtocol();

		void setType(uint8_t type);
		uint8_t getType();
		void delType();

		void setSubType(uint8_t subType);
		uint8_t getSubType();
		void delSubType();

		void setToDS(uint8_t toDS);
		uint8_t getToDS();
		void delToDS();

		void setFromDS(uint8_t fromDS);
		uint8_t getFromDS();
		void delFromDS();

		void setMoreFrag(uint8_t moreFrag);
		uint8_t getMoreFrag();
		void delMoreFrag();

		void setRetry(uint8_t retry);
		uint8_t getRetry();
		void delRetry();

		void setPwrMgmt(uint8_t pwrMgmt);
		uint8_t getPwrMgmt();
		void delPwrMgmt();

		void setMoreData(uint8_t moreData);
		uint8_t getMoreData();
		void delMoreData();

		void setProtectedFrame(uint8_t protectedFrame);
		uint8_t getProtectedFrame();
		void delProtectedFrame();

		void setOrder(uint8_t order);
		uint8_t getOrder();
		void delOrder();

		/*Dalsi pole*/
		/*Control frame*/
		void setDuration(uint16_t duration);
		uint16_t getDuration();
		void delDuration();

		void setReceiverAddress(char * receiverAddress); //addr1
		char * getReceiverAddress();
		void delReceiverAddress();

		void setTransmitterAddress(char * transmitterAddress); //addr2
		char * getTransmitterAddress();
		void delTransmitterAddress();	

		void setAid(uint16_t aid);
		uint16_t getAid();
		void delAid();	

		void setBSSID(char * bssid); //addr1
		char * getBSSID();
		void delBSSID();		

		/*Management frame*/
		void setDA(char * da); //addr2
		char * getDA();
		void delDA();

		void setSA(char * sa); //addr3
		char * getSA();
		void delSA();

		void setSeqCtl(uint16_t seqCtl);
		uint16_t getSeqCtl();
		void delSeqCtl();

		void setSequence(uint16_t sequence);
		uint16_t getSequence();
		void delSequence();

		void setFragment(uint16_t fragment);
		uint16_t getFragment();
		void delFragment();

		void setTimestamp(uint64_t timestamp);
		uint64_t getTimestamp();
		void delTimestamp();

		void setBeaconInterval(uint16_t beaconInterval);
		uint16_t getBeaconInterval();
		void delBeaconInterval();

		void setCapabilityInfo(uint16_t capabilityInfo);
		uint16_t getCapabilityInfo();
		void delCapabilityInfo();

		void setCapabilityInfoKeyword(Keyword keywordInfo);

		void setBitCapabilityInfo(uint8_t numberBit, uint8_t valueBit);
		uint8_t getBitCapabilityInfo(uint8_t numberBit);

		void setSSID(char * ssid);
		char * getSSID();
		void delSSID();

		void setFHparameterSetDwellTime(uint16_t fhParameterSetDwellTime);
		uint16_t getFHparameterSetDwellTime();
		void delFHparameterSetDwellTime();

		void setFHparameterSetHopSet(uint8_t fhParameterSetHopSet);
		uint8_t getFHparameterSetHopSet();
		void delFHparameterSetHopSet();

		void setFHparameterSetHopPattern(uint8_t fhParameterSetHopPattern);
		uint8_t getFHparameterSetHopPattern();
		void delFHparameterSetHopPattern();

		void setFHparameterSetHopIndex(uint8_t fhParameterSetHopIndex);
		uint8_t getFHparameterSetHopIndex();
		void delFHparameterSetHopIndex();

		void delFHparameterSet();

		void setDSparameterSet(uint8_t dsParameterSet);
		uint8_t getDSparameterSet();
		void delDSparameterSet();

		void setIBSSparameterSet(uint16_t ibssParameterSet);
		uint16_t getIBSSparameterSet();
		void delIBSSparameterSet();

		//tim
		void setTIMdTimCount(uint8_t timDtimCount);
		uint8_t getTIMdTimCount();
		void delTIMdTimCount();

		void setTIMdTimPeriod(uint8_t timDtimPeriod);
		uint8_t getTIMdTimPeriod();
		void delTIMdTimPeriod();

		void setTIMbitmapControl(uint8_t timBitmapControl);
		uint8_t getTIMbitmapControl();
		void delTIMbitmapControl();

		void setTIMpartialVirtualBitmap(char * timPartialVirtualBitmap);
		char * getTIMpartialVirtualBitmap();
		void delTIMpartialVirtualBitmap();

		void delTIM();

		//local power constraint
		void setLocalPowerConstraint(uint8_t localPowerConstraint);
		uint8_t getLocalPowerConstraint();
		void delLocalPowerConstraint();

		//country
		void setCountryString(char * countryString);
		char * getCountryString();
		void delCountryString();

		void addCountryConstraintTriplet(uint8_t index, ciect triplet);
		ciect getCountryConstraint(uint8_t index);
		void delCountryConstraintTirplet(uint8_t index);
		
		void delCountry();

		//CHANNEL switch
		void setChannelSwitchMode(uint8_t channelSwitchMode);
		uint8_t getChannelSwitchMode();
		void delChannelSwitchMode();

		void setNewChannelNumber(uint8_t newChannelNumber);
		uint8_t getNewChannelNumber();
		void delNewChannelNumber();

		void setChannelSwitchCount(uint8_t channelSwitchCount);
		uint8_t getChannelSwitchCount();
		void delChannelSwitchCount();

		void delChannelSwitch();

		//Quiet
		void setQuietCount(uint8_t quietCount);
		uint8_t getQuietCount();
		void delQuietCount();

		void setQuietPeriod(uint8_t quietPeriod);
		uint8_t getQuietPeriod();
		void delQuietPeriod();

		void setQuietDuration(uint8_t quietDuration);
		uint8_t getQuietDuration();
		void delQuietDuration();

		void setQuietOffset(uint8_t quietOffset);
		uint8_t getQuietOffset();
		void delQuietOffset();

		void delQuiet();

		//tpc report
		void setTransmitPower(uint8_t transmitPower);
		uint8_t getTransmitPower();
		void delTransmitPower();
		
		void setLinkMargin(uint8_t linkMargin);
		uint8_t getLinkMargin();
		void delLinkMargin();

		void delTPCReport();

		//erp
		void setERP(uint8_t erp);
		uint8_t getERP();
		void delERP();
		
		void setERPKeyword(Keyword keywordInfo);

		void setBitERP(uint8_t numberBit, uint8_t valueBit);
		uint8_t getBitERP(uint8_t numberBit);

		//supported rate
		void addSupportedRate(uint8_t supportedRate, uint8_t index);
		uint8_t getSupportedRate(uint8_t index);
		void delSupportedRate(uint8_t index);
		void delSupportedRateAll();

		//extended supported rate
		void addExtendedSupportedRate(uint8_t extendedSupportedRate, uint8_t index);
		uint8_t getExtendedSupportedRate(uint8_t index);
		void delExtendedSupportedRate(uint8_t index);
		void delExtendedSupportedRateAll();

		//reason code
		void setReasonCode(uint8_t reasonCode);
		uint8_t getReasonCode();
		void delReasonCode();

		//listen interval
		void setListenInterval(uint8_t listenInterval);
		uint8_t getListenInterval();
		void delListenInterval();

		//current ap address
		void setCurrentAPaddress(char * currentAPaddress);
		char * getCurrentAPaddress();
		void delCurrentAPaddress();

		//CF parameter set
		void setCFPcount(uint8_t cfpCount);
		uint8_t getCFPcount();
		void delCFPcount();

		void setCFPperiod(uint8_t cfpPeriod);
		uint8_t getCFPperiod();
		void delCFPperiod();

		void setCFPmaxDuration(uint16_t cfpMaxDuration);
		uint16_t getCFPmaxDuration();
		void delCFPmaxDuration();

		void setCFPremaining(uint16_t cfpRemaining);
		uint16_t getCFPremaining();
		void delCFPremaining();
	
		void delCFP();

		//RSN
		void setRSNversion(uint16_t rsnVersion);
		uint16_t getRSNversion();
		void delRSNversion();

		void setGroupCipherSuite(suite groupCipherSuite);		
		suite getGroupCipherSuite();
		void delGroupCipherSuite();

		void addPairwiseCipherSuite(suite pairwiseCipherSuite, int index);
		suite getPairwiseCipherSuite(int index);
		void delPairwiseCipherSuite(int index);
		void delPairwiseCipherSuiteAll();

		void addAuthenticationSuite(suite authenticationSuite, int index);
		suite getAuthenticationSuite(int index);
		void delAuthenticationSuite(int index);
		void delAuthenticationSuiteAll();
		
		void setRSNcapabilities(uint16_t rsnCapabilities);
		uint16_t getRSNcapabilities();
		void delRSNcapabilities();

		void setRSNcapabilitiesKeyword(Keyword keywordInfo);

		void delRSN();

		//status code
		void setStatusCode(uint8_t statusCode);
		uint8_t getStatusCode();
		void delStatusCode();

		//association id
		void setAssociationId(uint8_t associationId);
		uint8_t getAssociationId();
		void delAssociationId();

		//authentication algorithm number
		void setAuthenticationAlgorithmNumber(uint8_t authenticationAlgorithmNumber);
		uint8_t getAuthenticationAlgorithmNumber();
		void delAuthenticationAlgorithmNumber();

		//authentication sequence number
		void setAuthenticationSequenceNumber(uint8_t authenticationSequenceNumber);
		uint8_t getAuthenticationSequenceNumber();
		void delAuthenticationSequenceNumber();

		//challenge text
		void setChallengeText(char * challengeText);
		char * getChallengeText();
		void delChallengeText();

		void setQos(uint16_t qos);
		uint16_t getQos();
		void delQos();

		/*Data frame*/
		void setAddress1(char * address1);
		char * getAddress1();
		void delAddress1();

		void setAddress2(char * address2);
		char * getAddress2();
		void delAddress2();

		void setAddress3(char * address3);
		char * getAddress3();
		void delAddress3();

		void setAddress4(char * address4);
		char * getAddress4();
		void delAddress4();

		static int parseIV(int lengthFrame, uint8_t * frame, uint8_t * iv);

	private:
		uint32_t generateDATA(uint8_t * data);
		uint32_t generateRTS(uint8_t * data);
		uint32_t generateCTS(uint8_t * data);
		uint32_t generateACK(uint8_t * data);
		uint32_t generatePSPOLL(uint8_t * data);
		uint32_t generateBEACON(uint8_t * data);
		uint32_t generatePROBEREQUEST(uint8_t * data);
		uint32_t generatePROBERESPONSE(uint8_t * data);
		uint32_t generateATIM(uint8_t * data);
		uint32_t generateDEAUTHETICATION(uint8_t * data);
		uint32_t generateDISASSOCIATION(uint8_t * data);
		uint32_t generateASSOCIATIONREQUEST(uint8_t * data);
		uint32_t generateREASSOCIATIONREQUEST(uint8_t * data);
		uint32_t generateREASSOCIATIONRESPONSE(uint8_t * data);
		uint32_t generateAUTHENTICATION(uint8_t * data);

		uint32_t generateAddress1(uint8_t * data);
		uint32_t generateAddress2(uint8_t * data);
		uint32_t generateAddress3(uint8_t * data);
		uint32_t generateAddress4(uint8_t * data);
		uint32_t generateDuration(uint8_t * data);
		uint32_t generateReceiveAddress(uint8_t * data);
		uint32_t generateTransmitterAddress(uint8_t * data);
		uint32_t generateAID(uint8_t * data);
		uint32_t generateBSSID(uint8_t * data);
		uint32_t generateDA(uint8_t * data);	
		uint32_t generateSA(uint8_t * data);
		uint32_t generateSeqCtl(uint8_t * data);
		uint32_t generateTimestamp(uint8_t * data);
		uint32_t generateBeaconInterval(uint8_t * data);
		uint32_t generateCapabilityInfo(uint8_t * data);
		uint32_t generateSSID(uint8_t * data);
		uint32_t generateFHparameterSet(uint8_t * data);
		uint32_t generateDSparameterSet(uint8_t * data);
		uint32_t generateCFparameterSet(uint8_t * data);	
		uint32_t generateIBSSparameterSet(uint8_t * data);
		uint32_t generateTIM(uint8_t * data);
		uint32_t generatePowerConstraint(uint8_t * data);		
		uint32_t generateChannelSwitch(uint8_t * data);		
		uint32_t generateCountry(uint8_t * data);
		uint32_t generateQuiet(uint8_t * data);
		uint32_t generateTCPreport(uint8_t * data);
		uint32_t generateERP(uint8_t * data);
		uint32_t generateSupportedRate(uint8_t * data);
		uint32_t generateExtendedSupportedRate(uint8_t * data);
		uint32_t generateRSN(uint8_t * data);
		uint32_t generateReaconCode(uint8_t * data);
		uint32_t generateListenInterval(uint8_t * data);
		uint32_t generateCurrentAPaddress(uint8_t * data);
		uint32_t generateStatusCode(uint8_t * data);
		uint32_t generateAssociationId(uint8_t * data);
		uint32_t generateAuthenticationAlgorithmNumber(uint8_t * data);
		uint32_t generateAuthenticationSequenceNumber(uint8_t * data);
		uint32_t generateChallengeText(uint8_t * data);
		uint32_t generateQos(uint8_t * data);

		void setAddr1(char * addr);
		void setAddr2(char * addr);
		void setAddr3(char * addr);
		void setAddr4(char * addr);

		void printDATA();
		void printRTS();
		void printCTS();
		void printACK();
		void printPSPOLL();
		void printBEACON();
		void printPROBEREQUEST();
		void printPROBERESPONSE();
		void printATIM();
		void printDEAUTHETICATION();
		void printDISASSOCIATION();
		void printASSOCIATIONREQUEST();
		void printREASSOCIATIONREQUEST();
		void printREASSOCIATIONRESPONSE();
		void printAUTHENTICATION();

		void printAddress1();
		void printAddress2();
		void printAddress3();
		void printAddress4();
		void printDuration();
		void printReceiverAddress();
		void printTransmitterAddress();
		void printAID();
		void printBSSID();
		void printDA();
		void printSA();
		void printSeqCtl();	
		void printTimestamp();
		void printBeaconInterval();
		void printCapabilityInfo();
		void printSSID();
		void printFHparameterSet();
		void printDSparameterSet();
		void printCFparameterSet();
		void printIBSSparameterSet();
		void printTrafficIndicationMap();
		void printPowerConstraint();
		void printCountry();
		void printChannelSwitch();
		void printQuiet();
		void printTPCreport();
		void printERPinformation();
		void printSupportedRate();
		void printExtendedSupportedRate();
		void printRSN();
		void printReaconCode();
		void printListenInterval();
		void printCurrentAPaddress();
		void printStatusCode();
		void printAssociationId();
		void printAuthenticationAlgorithmNumber();
		void printAuthenticationSequenceNumber();
		void printChallengeText();
		void printQos();
};

#endif
