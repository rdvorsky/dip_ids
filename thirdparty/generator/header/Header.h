/* Soubor: Header.h
 * Autor: Pavel Svanda
 * Email: svandap@gmail.com
 * Datum vytvoreni: 25.6.2012
 * Popis: deklarace tridy Header, ktera slouzi jako abstraktni trida pro praci s hlavickami
 */

#ifndef HEADER_H
#define HEADER_H


#ifdef TIXML_USE_STL
	#include <iostream>
	#include <sstream>
	using namespace std;
#else
	#include <stdio.h>
#endif

#if defined( WIN32 ) && defined( TUNE )
	#include <crtdbg.h>
	_CrtMemState startMemState;
	_CrtMemState endMemState;
#endif

#include "../tinyXml/tinyxml.h"
#include "../other/Utilities.h"

#include <stdint.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <netinet/in.h>
#include <cstdlib>

typedef struct {
	uint64_t include;	//informace, ktere "klicova slova" zahrnout - ktere bity
	uint64_t exclude;	//informace, ktere vyloucit
} Keyword;

class Header {
	public:
		virtual uint32_t generateHeader(uint8_t * data) { return 0; };
		virtual void printHeader() { };
		virtual void deleteHeader() { };
		virtual TiXmlElement * saveHeader() { return NULL; };
		virtual void loadHeader(TiXmlElement * element) { };
};

#endif
