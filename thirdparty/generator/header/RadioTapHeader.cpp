/* Soubor: RadioTapHeader.cpp
 * Autor: Pavel Svanda
 * Email: svandap@gmail.com
 * Datum vytvoreni: 26.6.2012
 * Popis: definice tridy RadioTapHeader, ktera slouzi jako reprezentace RadioTap hlavicky
 */
#include "RadioTapHeader.h"

/* RadioTapHeader::RadioTapHeader
 * Popis: konstruktor
 * Parametry: -
 * Navratova hodnota: -
 */
RadioTapHeader::RadioTapHeader() {
	this -> deleteHeader();
}

/* RadioTapHeader::~RadioTapHeader
 * Popis: destruktor
 * Parametry: -
 * Navratova hodnota: -
 */
RadioTapHeader::~RadioTapHeader() {

}

/* RadioTapHeader::setVersion
 * Popis: nastavi verzi radiotap
 * Parametry: uint8_t version - verze - podporovana verze cislo 0
 * Navratova hodnota: -
 */
void RadioTapHeader::setVersion(uint8_t version) {
	this -> version = version;
}

/* RadioTapHeader::getVersion
 * Popis: vrati verzi radiotap
 * Parametry: -
 * Navratova hodnota: verze radiotap
 */
uint8_t RadioTapHeader::getVersion() {
	return this -> version;
}

/* RadioTapHeader::delVersion
 * Popis: odstrani (reseruje) verzi radiotap
 * Parametry: -
 * Navratova hodnota: -
 */
void RadioTapHeader::delVersion() {
	this -> version = D_VERSION;
}

/* RadioTapHeader::setPad
 * Popis: nastavi vypln radiotap
 * Parametry: uint8_t pad - vypln - nastavena na 0
 * Navratova hodnota: -
 */
void RadioTapHeader::setPad(uint8_t pad) {
	this -> pad = pad;
}

/* RadioTapHeader::getPad
 * Popis: vrati hodnotu vyplne radiotap
 * Parametry: -
 * Navratova hodnota: hodnota vyplne
 */
uint8_t RadioTapHeader::getPad() {
	return this -> pad;
}

/* RadioTapHeader::delPad
 * Popis: odstrani (reseruje) pad radiotap
 * Parametry: -
 * Navratova hodnota: -
 */
void RadioTapHeader::delPad() {
	this -> pad = D_PAD;
}

/* RadioTapeader::setLen
 * Popis: nastavi velikost hlavicky - nastavuje se automaticky
 * Parametry: uint16_t len - velikost hlavicky
 * Navratova hodnota: -
 */
/*void RadioTapHeader::setLen(uint16_t len) {
	this -> len = len;
}*/

/* RadioTapHeader::getLen
 * Popis: vrati velikost hlavicky, ktera se urci z nastavenych polozek
 * Parametry: -
 * Navratova hodnota: velikost hlavicky radiotap
 */
uint16_t RadioTapHeader::getLen() {
	//zjistime velikost hlavicky
	uint8_t header [LENGTH_HEADER];
	this -> len = this -> generateHeader(header);
	return this -> len;
}

/* RadioTapHeader::delLen
 * Popis: odstrani (reseruje) len radiotap
 * Parametry: -
 * Navratova hodnota: -
 */
/*void RadioTapHeader::delLen() {
	this -> len = D_LEN;
}*/

/* RadioTapHeader::setPresent
 * Popis: nastavi present pole cele najednou
 * Parametry: uint32_t present - bitove pole present
 * Navratova hodnota: -
 */
void RadioTapHeader::setPresent(uint32_t present) {
	this -> present = present;
}

/* RadioTapHeader::getPresent
 * Popis: vrati hodnotu pole present
 * Parametry: -
 * Navratova hodnota: hodnota pole present
 */
uint32_t RadioTapHeader::getPresent() {
	return this -> present;
}

/* RadioTapHeader::setBitPresent
 * Popis: umoznuje nastavit libovolny bit pole present
 * Parametry: uint8_t numberBit - cislo bitu, ktery nastavujeme (pokud zadame cislo bitu vetsi nez je, tak se nic neprovede)
 *            uint8_t valueBit - hodnota bitu, na ktery jej nastavujeme (0 - nastavujeme na nulu, ostatni - nastavujeme na 1)
 * Navratova hodnota: -
 */
void RadioTapHeader::setBitPresent(uint8_t numberBit, uint8_t valueBit) {
	//hodnota nastavovaneho bitu je 0
	if(valueBit == 0) {
		this -> present &= (~(uint32_t)pow(2, numberBit));
	} else {
	//hodnota nastavovaneho bitu je 1
		this -> present |= (uint32_t)pow(2, numberBit);
	}	
}

/* RadioTapHeader::getBitPresent
 * Popis: vrati hodnotu konkretniho bitu v poli present
 * Parametry: uint8_t numberBit - cislo bitu, jehoz hodnotu chceme ziskat
 * Navratova hodnota: hodnota bitu
 */
uint8_t RadioTapHeader::getBitPresent(uint8_t numberBit) {
	if((this -> present & (uint32_t)pow(2,numberBit)) != 0) {
		return 1;
	} else {
		return 0;
	}
}

/* RadioTapHeader::delPresent
 * Popis: odstrani (reseruje) present radiotap
 * Parametry: -
 * Navratova hodnota: -
 */
void RadioTapHeader::delPresent() {
	this -> present = D_PRESENT;
}

//GETry a SETry rozsirujicich polozek hlavicky - dale bez popisu
void RadioTapHeader::setTSFTmactime(uint64_t TSFTmactime) {
	this -> setBitPresent(TSFT, 1);
	this -> TSFTmactime = TSFTmactime;
}

uint64_t RadioTapHeader::getTSFTmactime() {
	return this -> TSFTmactime;
}

void RadioTapHeader::delTSFTmactime() {
	this -> setBitPresent(TSFT, 0);
	this -> TSFTmactime = D_TSFT_MACTIME;
}

void RadioTapHeader::setFlags(uint8_t flags) {
	this -> setBitPresent(FLAGS, 1);
	this -> flags = flags;
}

void RadioTapHeader::setFlagsKeyword(Keyword keywordInfo) {
	this -> setBitPresent(FLAGS, 1);
	//include seznam
	this -> flags = this -> flags | keywordInfo.include;
	//exclude seznam
	this -> flags = this -> flags & (~keywordInfo.exclude);
}

void RadioTapHeader::setBitFlags(uint8_t numberBit, uint8_t valueBit) {
	this -> setBitPresent(FLAGS, 1);
	//hodnota nastavovaneho bitu je 0
	if(valueBit == 0) {
		this -> flags &= (~(uint8_t)pow(2, numberBit));
	} else {
	//hodnota nastavovaneho bitu je 1
		this -> flags |= (uint8_t)pow(2, numberBit);
	}
}

uint8_t RadioTapHeader::getFlags() {
	return this -> flags;
}
	
uint8_t RadioTapHeader::getBitFlags(uint8_t numberBit) {
	if((this -> flags & (uint8_t)pow(2,numberBit)) != 0) {
		return 1;
	} else {
		return 0;
	}
}

void RadioTapHeader::delFlags() {
	this -> setBitPresent(FLAGS, 0);
	this -> flags = D_FLAGS;
}

void RadioTapHeader::setRate(uint8_t rate) {
	this -> setBitPresent(RATE, 1);
	this -> rate = rate;
}

uint8_t RadioTapHeader::getRate() {
	return this -> rate;
}

void RadioTapHeader::delRate() {
	this -> setBitPresent(RATE, 0);
	this -> rate = D_RATE;
}

void RadioTapHeader::setChannelFrequency(uint16_t channelFrequency) {
	this -> setBitPresent(CHANNEL, 1);
	this -> channelFrequency = channelFrequency;
}

uint16_t RadioTapHeader::getChannelFrequency() {
	return this -> channelFrequency;
}

void RadioTapHeader::delChannelFrequency() {
	this -> channelFrequency = D_CHANNEL_FREQUENCY;
}

void RadioTapHeader::setChannelFlags(uint16_t channelFlags) {
	this -> setBitPresent(CHANNEL, 1);
	this -> channelFlags = channelFlags;
}

void RadioTapHeader::setChannelFlagsKeyword(Keyword keywordInfo) {
	this -> setBitPresent(CHANNEL, 1);
	//include seznam
	this -> channelFlags = this -> channelFlags | keywordInfo.include;
	//exclude seznam
	this -> channelFlags = this -> channelFlags & (~keywordInfo.exclude);
}

void RadioTapHeader::setBitChannelFlags(uint8_t numberBit, uint8_t valueBit) {
	this -> setBitPresent(CHANNEL, 1);
	//hodnota nastavovaneho bitu je 0
	if(valueBit == 0) {
		this -> channelFlags &= (~(uint16_t)pow(2, numberBit));
	} else {
	//hodnota nastavovaneho bitu je 1
		this -> channelFlags |= (uint16_t)pow(2, numberBit);
	}	
}

uint16_t RadioTapHeader::getChannelFlags() {
	return this -> channelFlags;
}

uint8_t RadioTapHeader::getBitChannelFlags(uint8_t numberBit) {
	if((this -> channelFlags & (uint16_t)pow(2,numberBit)) != 0) {
		return 1;
	} else {
		return 0;
	}
}

void RadioTapHeader::delChannelFlags() {
	this -> channelFlags = D_CHANNEL_FLAGS;
}

void RadioTapHeader::delChannel() {
	this -> setBitPresent(CHANNEL, 0);
	this -> channelFrequency = D_CHANNEL_FREQUENCY;
	this -> channelFlags = D_CHANNEL_FLAGS;
}

void RadioTapHeader::setFHSShopSet(uint8_t FHSShopSet) {
	this -> setBitPresent(FHSS, 1);
	this -> FHSShopSet = FHSShopSet;
}

uint8_t RadioTapHeader::getFHSShopSet() {
	return this -> FHSShopSet;
}

void RadioTapHeader::delFHSShopSet() {
	this -> FHSShopSet = D_FHSS_HOP_SET;
}


void RadioTapHeader::setFHSShopPattern(uint8_t FHSShopPattern){
	this -> setBitPresent(FHSS, 1);
	this -> FHSShopPattern = FHSShopPattern;
}

uint8_t RadioTapHeader::getFHSShopPattern() {
	return this -> FHSShopPattern;
}

void RadioTapHeader::delFHSShopPattern() {
	this -> FHSShopPattern = D_FHSS_HOP_PATTERN;
}

void RadioTapHeader::delFHSShop() {
	this -> setBitPresent(FHSS, 0);
	this -> FHSShopSet = D_FHSS_HOP_SET;
	this -> FHSShopPattern = D_FHSS_HOP_PATTERN;
}

void RadioTapHeader::setAntennaSignal(int8_t antennaSignal) {
	this -> setBitPresent(ANTENNA_SIGNAL, 1);
	this -> antennaSignal = antennaSignal;
}

int8_t RadioTapHeader::getAntennaSignal() {
	return this -> antennaSignal;
}

void RadioTapHeader::delAntennaSignal() {
	this -> setBitPresent(ANTENNA_SIGNAL, 0);
	this -> antennaSignal = D_ANTENNA_SIGNAL;
}

void RadioTapHeader::setAntennaNoise(uint8_t antennaNoise) {
	this -> setBitPresent(ANTENNA_NOISE, 1);
	this -> antennaNoise = antennaNoise;
}

int8_t RadioTapHeader::getAntennaNoise() {
	return this -> antennaNoise;
}

void RadioTapHeader::delAntennaNoise() {
	this -> setBitPresent(ANTENNA_NOISE, 0);
	this -> antennaNoise = D_ANTENNA_NOISE;
}

void RadioTapHeader::setLockQuality(uint16_t lockQuality) {
	this -> setBitPresent(LOCK_QUALITY, 1);
	this -> lockQuality = lockQuality;
}

uint16_t RadioTapHeader::getLockQuality() {
	return this -> lockQuality;
}

void RadioTapHeader::delLockQuality() {
	this -> setBitPresent(LOCK_QUALITY, 0);
	this -> lockQuality = D_LOCK_QUALITY;
}

void RadioTapHeader::setTXattenuation(uint16_t TXattenuation) {
	this -> setBitPresent(TX_ATTENUATION, 1);
	this -> TXattenuation = TXattenuation;
}

uint16_t RadioTapHeader::getTXattenuation() {
	return this -> TXattenuation;
}

void RadioTapHeader::delTXattenuation() {
	this -> setBitPresent(TX_ATTENUATION, 0);
	this -> TXattenuation = D_TX_ATTENUATION;
}

void RadioTapHeader::setDbTXattenuation(uint16_t dbTXattenuation) {
	this -> setBitPresent(DB_TX_ATTENUATION, 1);
	this -> dbTXattenuation = dbTXattenuation;
}

uint16_t RadioTapHeader::getDbTXattenuation() {
	return this -> dbTXattenuation;
}

void RadioTapHeader::delDbTXattenuation() {
	this -> setBitPresent(DB_TX_ATTENUATION, 0);
	this -> dbTXattenuation = D_DB_TX_ATTENUATION;
}

void RadioTapHeader::setDbmTXpower(int8_t dbmTXpower) {
	this -> setBitPresent(DBM_TX_POWER, 1);
	this -> dbmTXpower = dbmTXpower;
}

int8_t RadioTapHeader::getDbmTXpower() {
	return this -> dbmTXpower;
}

void RadioTapHeader::delDbmTXpower() {
	this -> setBitPresent(DBM_TX_POWER, 0);
	this -> dbmTXpower = D_DBM_TX_POWER;
}

void RadioTapHeader::setAntenna(uint8_t antenna) {
	this -> setBitPresent(ANTENNA, 1);
	this -> antenna = antenna;
}

uint8_t RadioTapHeader::getAntenna() {
	return this -> antenna;
}

void RadioTapHeader::delAntenna() {
	this -> setBitPresent(ANTENNA, 0);
	this -> antenna = D_ANTENNA;
}

void RadioTapHeader::setDbAntennaSignal(uint8_t dbAntennaSignal) {
	this -> setBitPresent(DB_ANTENNA_SIGNAL, 1);
	this -> dbAntennaSignal = dbAntennaSignal;
}

uint8_t RadioTapHeader::getDbAntennaSignal() {
	return this -> dbAntennaSignal;
}

void RadioTapHeader::delDbAntennaSignal() {
	this -> setBitPresent(DB_ANTENNA_SIGNAL, 0);
	this -> dbAntennaSignal = D_DB_ANTENNA_SIGNAL;
}

void RadioTapHeader::setDbAntennaNoise(uint8_t dbAntennaNoise) {
	this -> setBitPresent(DB_ANTENNA_NOISE, 1);
	this -> dbAntennaNoise = dbAntennaNoise;
}

uint8_t RadioTapHeader::getDbAntennaNoise() {
	return this -> dbAntennaNoise;
}

void RadioTapHeader::delDbAntennaNoise() {
	this -> setBitPresent(DB_ANTENNA_NOISE, 0);
	this -> dbAntennaNoise = D_DB_ANTENNA_NOISE;
}

void RadioTapHeader::setRXflags(uint16_t RXflags) {
	this -> setBitPresent(RX_FLAGS, 1);
	this -> RXflags = RXflags;
}

void RadioTapHeader::setRXflagsKeyword(Keyword keywordInfo) {
	this -> setBitPresent(RX_FLAGS, 1);
	//include seznam
	this -> RXflags = this -> RXflags | keywordInfo.include;
	//exclude seznam
	this -> RXflags = this -> RXflags & (~keywordInfo.exclude);
}

void RadioTapHeader::setBitRXflags(uint8_t numberBit, uint8_t valueBit) {
	this -> setBitPresent(RX_FLAGS, 1);
	//hodnota nastavovaneho bitu je 0
	if(valueBit == 0) {
		this -> RXflags &= (~(uint16_t)pow(2, numberBit));
	} else {
	//hodnota nastavovaneho bitu je 1
		this -> RXflags |= (uint16_t)pow(2, numberBit);
	}
}
		
uint16_t RadioTapHeader::getRXflags() {
	return this -> RXflags;
}

uint8_t RadioTapHeader::getBitRXflags(uint8_t numberBit) {
	if((this -> RXflags & (uint16_t)pow(2,numberBit)) != 0) {
		return 1;
	} else {
		return 0;
	}
}

void RadioTapHeader::delRXflags() {
	this -> setBitPresent(RX_FLAGS, 0);
	this -> RXflags = D_RX_FLAGS;
}

void RadioTapHeader::setRTSretries(uint8_t RTSretries) {
	this -> setBitPresent(RTS_RETRIES, 1);
	this -> RTSretries = RTSretries;
}

uint8_t RadioTapHeader::getRTSretries() {
	return this -> RTSretries;
}

void RadioTapHeader::delRTSretries() {
	this -> setBitPresent(RTS_RETRIES, 0);
	this -> RTSretries = D_RTS_RETRIES;
}

void RadioTapHeader::setDataRetries(uint8_t dataRetries) {
	this -> setBitPresent(DATA_RETRIES, 1);
	this -> dataRetries = dataRetries;
}

uint8_t RadioTapHeader::getDataRetries() {
	return this -> dataRetries;
}

void RadioTapHeader::delDataRetries() {
	this -> setBitPresent(DATA_RETRIES, 0);
	this -> dataRetries = D_DATA_RETRIES;
}

void RadioTapHeader::setMCSknown(uint8_t MCSknown) {
	this -> setBitPresent(MCS, 1);
	this -> MCSknown = MCSknown;
}

void RadioTapHeader::setBitMCSknown(uint8_t numberBit, uint8_t valueBit) {
	this -> setBitPresent(MCS, 1);
	//hodnota nastavovaneho bitu je 0
	if(valueBit == 0) {
		this -> MCSknown&= (~(uint8_t)pow(2, numberBit));
	} else {
	//hodnota nastavovaneho bitu je 1
		this -> MCSknown |= (uint8_t)pow(2, numberBit);
	}
}

uint8_t RadioTapHeader::getMCSknown() {
	return this -> MCSknown;
}

uint8_t RadioTapHeader::getBitMCSknown(int numberBit) {
	if((this -> MCSknown & (uint8_t)pow(2,numberBit)) != 0) {
		return 1;
	} else {
		return 0;
	}
}

void RadioTapHeader::delMCSknown() {
	this -> MCSknown = D_MCS_KNOWN;
}

void RadioTapHeader::setMCSflags(uint8_t MCSflags) {
	this -> setBitPresent(MCS, 1);
	this -> MCSflags = MCSflags;
}

void RadioTapHeader::setMCSflagsKeyword(Keyword keywordInfo) {
	this -> setBitPresent(MCS, 1);
	//include seznam
	this -> MCSflags = this -> MCSflags | keywordInfo.include;
	//exclude seznam
	this -> MCSflags = this -> MCSflags & (~keywordInfo.exclude);
}

void RadioTapHeader::setBitMCSFlags(uint8_t numberBit, uint8_t valueBit) {
	this -> setBitPresent(MCS, 1);
	//hodnota nastavovaneho bitu je 0
	if(valueBit == 0) {
		this -> MCSflags &= (~(uint8_t)pow(2, numberBit));
	} else {
	//hodnota nastavovaneho bitu je 1
		this -> MCSflags |= (uint8_t)pow(2, numberBit);
	}
}

uint8_t RadioTapHeader::getMCSflags() {
	return this -> MCSflags;
}

uint8_t RadioTapHeader::getBitMCSflags(int numberBit) {
	if((this -> MCSflags & (uint8_t)pow(2,numberBit)) != 0) {
		return 1;
	} else {
		return 0;
	}
}

void RadioTapHeader::delMCSflags() {
	this -> MCSflags = D_MCS_FLAGS;
}

void RadioTapHeader::setMCSmcs(uint8_t MCSmcs) {
	this -> setBitPresent(MCS, 1);
	this -> MCSmcs = MCSmcs;
}

uint8_t RadioTapHeader::getMCSmcs() {
	return this -> MCSmcs;
}

void RadioTapHeader::delMCSmcs() {
	this -> MCSmcs = D_MCS_MCS;
}

void RadioTapHeader::delMCS() {
	this -> setBitPresent(MCS, 0);
	this -> MCSknown = D_MCS_KNOWN;
	this -> MCSflags = D_MCS_FLAGS;
	this -> MCSmcs = D_MCS_MCS;
}

void RadioTapHeader::setVendorNamespaceOUI(uint8_t * vendorNamespaceOUI) {
	this -> setBitPresent(VENDOR_NAMESPACE, 1);
	this -> vendorNamespaceOUI [0] = vendorNamespaceOUI[0];
	this -> vendorNamespaceOUI [1] = vendorNamespaceOUI[1];
	this -> vendorNamespaceOUI [2] = vendorNamespaceOUI[2];
	this -> vendorNamespaceOUI [3] = 0;
}

uint8_t  * RadioTapHeader::getVendorNamespaceOUI() {
	return this -> vendorNamespaceOUI;
}

void RadioTapHeader::delVendorNamespaceOUI() {
	this -> vendorNamespaceOUI [0] = D_VENDOR_NAMESPACE_OUI_1;
	this -> vendorNamespaceOUI [1] = D_VENDOR_NAMESPACE_OUI_2;
	this -> vendorNamespaceOUI [2] = D_VENDOR_NAMESPACE_OUI_3;
	this -> vendorNamespaceOUI [3] = 0;
}

void RadioTapHeader::setVendorNamespaceSubNamespace(uint8_t vendorNamespaceSubNamespace) {
	this -> setBitPresent(VENDOR_NAMESPACE, 1);
	this -> vendorNamespaceSubNamespace = vendorNamespaceSubNamespace;
}

uint8_t RadioTapHeader::getVendorNamespaceSubNamespace() {
	return this -> vendorNamespaceSubNamespace;
}

void RadioTapHeader::delVendorNamespaceSubNamespace() {
	this -> vendorNamespaceSubNamespace = D_VENDOR_NAMESPACE_SUB_NAMESPACE;
}

void RadioTapHeader::setVendorNamespaceSkipLength(uint16_t vendorNamespaceSkipLength) {
	this -> setBitPresent(VENDOR_NAMESPACE, 1);
	this -> vendorNamespaceSkipLength = vendorNamespaceSkipLength;
}

uint16_t RadioTapHeader::getVendorNamespaceSkipLength() {
	return this -> vendorNamespaceSkipLength;
}

void RadioTapHeader::delVendorNamespaceSkipLength() {
	this -> vendorNamespaceSubNamespace = D_VENDOR_NAMESPACE_SKIP_LENGTH;
}

void RadioTapHeader::delVendorNamespace() {
	this -> setBitPresent(VENDOR_NAMESPACE, 0);
	this -> vendorNamespaceOUI [0] = D_VENDOR_NAMESPACE_OUI_1;
	this -> vendorNamespaceOUI [1] = D_VENDOR_NAMESPACE_OUI_2;
	this -> vendorNamespaceOUI [2] = D_VENDOR_NAMESPACE_OUI_3;
	this -> vendorNamespaceOUI [3] = 0;
	this -> vendorNamespaceSubNamespace = D_VENDOR_NAMESPACE_SUB_NAMESPACE;
	this -> vendorNamespaceSubNamespace = D_VENDOR_NAMESPACE_SKIP_LENGTH;
}		

/* RadioTapHeader::insertPadding
 * Popis: funkce provede zarovnani dat v rozsirujici hlavicce na potrebnou velikost
 * Parametry: uint8_t ** data  - data, ve kterych se budu provadet zarovnani
 *	      uint8_t lengthData - informace o delce dosavadnich dat
 *	      uint8_t neededPadding - informace na jakou delku se bude zarovnavat
 * Navratova hodnota: nova delka dat - soucet puvodni delky a vlozenoho zarovanani  
 */
uint32_t RadioTapHeader::insertPadding(uint8_t ** data, uint32_t lengthData, uint8_t neededPadding) {
	uint8_t padding [] = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
	int lengthPadding;
	
	if(lengthData % neededPadding != 0) {
		lengthPadding = lengthData % neededPadding;
		Utilities::memcpy8(*data, padding, lengthPadding, LITTLE_ENDIAN_ORDER);
		lengthData += lengthPadding * sizeof(uint8_t);
		*data += lengthPadding * sizeof(uint8_t);
	}

	return lengthData;
}


/* RadioTapHeader::generateHeader
 * Popis: funkce provede vyganerovani hlavicky ze zadanych udaji
 * Parametry: uint8_t * data - ukazatel do pameti, kam se hlavicka vygeneruje
 * Navratova hodnota: delka hlavicky
 */
uint32_t RadioTapHeader::generateHeader(uint8_t * data) {
	uint32_t lengthData = 0;
	uint8_t * locationLengthDataInHeader;

	//povina pole
	Utilities::memcpy8(data, &this -> version, 1, LITTLE_ENDIAN_ORDER);
	data += sizeof(uint8_t);
	Utilities::memcpy8(data, &this -> pad, 1, LITTLE_ENDIAN_ORDER);
	data += sizeof(uint8_t);
	locationLengthDataInHeader = data;
	//Utilities::memcpy16(data, &this -> len, 1, LITTLE_ENDIAN_ORDER);
	//delka dat se musi nastavit na konci podle skutecne delky dat
	data += sizeof(uint16_t);
	Utilities::memcpy32(data, &this -> present, 1, LITTLE_ENDIAN_ORDER);
	data += sizeof(uint32_t);

	//castecna delka dat povinnych poli
	lengthData = 2 * sizeof(uint8_t) + sizeof(uint16_t) + sizeof(uint32_t);

	//rozsirujici pole hlavicky
	//projdu pres jednotlive bity present pole a poskladam hlavicku
	//zaroven pricitam delku dat

	if(this -> getBitPresent(TSFT)) {
		lengthData = this -> insertPadding(&data, lengthData, INT_64); 
		Utilities::memcpy64(data, &this -> TSFTmactime, 1, LITTLE_ENDIAN_ORDER);
		lengthData += sizeof(uint64_t);
		data += sizeof(uint64_t);
	}	

	if(this -> getBitPresent(FLAGS)) {
		lengthData = this -> insertPadding(&data, lengthData, INT_8); 
		Utilities::memcpy8(data, &this -> flags, 1, LITTLE_ENDIAN_ORDER);
		lengthData += sizeof(uint8_t);
		data += sizeof(uint8_t);
	}

	if(this -> getBitPresent(RATE)) {
		lengthData = this -> insertPadding(&data, lengthData, INT_8);
		Utilities::memcpy8(data, &this -> rate, 1, LITTLE_ENDIAN_ORDER);
		lengthData += sizeof(uint8_t);
		data += sizeof(uint8_t);
	}

	if(this -> getBitPresent(CHANNEL)) {
		lengthData = this -> insertPadding(&data, lengthData, INT_16);
		Utilities::memcpy16(data, &this -> channelFrequency, 1, LITTLE_ENDIAN_ORDER);
		lengthData += sizeof(uint16_t);
		data += sizeof(uint16_t);

		lengthData = this -> insertPadding(&data, lengthData, INT_16);
		Utilities::memcpy16(data, &this -> channelFlags, 1, LITTLE_ENDIAN_ORDER);
		lengthData += sizeof(uint16_t);
		data += sizeof(uint16_t);
	}

	if(this -> getBitPresent(FHSS)) {
		lengthData = this -> insertPadding(&data, lengthData, INT_8);
		Utilities::memcpy8(data, &this -> FHSShopSet, 1, LITTLE_ENDIAN_ORDER);
		lengthData += sizeof(uint8_t);
		data += sizeof(uint8_t);

		lengthData = this -> insertPadding(&data, lengthData, INT_8);
		Utilities::memcpy8(data, &this -> FHSShopPattern, 1, LITTLE_ENDIAN_ORDER);
		lengthData += sizeof(uint8_t);
		data += sizeof(uint8_t);
	}

	if(this -> getBitPresent(ANTENNA_SIGNAL)) {
		lengthData = this -> insertPadding(&data, lengthData, INT_8);
		Utilities::memcpy8(data, &this -> antennaSignal, 1, LITTLE_ENDIAN_ORDER);
		lengthData += sizeof(int8_t);
		data += sizeof(int8_t);
	}

	if(this -> getBitPresent(ANTENNA_NOISE)) {
		lengthData = this -> insertPadding(&data, lengthData, INT_8);
		Utilities::memcpy8(data, &this -> antennaNoise, 1, LITTLE_ENDIAN_ORDER);
		lengthData += sizeof(uint8_t);
		data += sizeof(uint8_t);
	}

	if(this -> getBitPresent(LOCK_QUALITY)) {
		lengthData = this -> insertPadding(&data, lengthData, INT_16);
		Utilities::memcpy16(data, &this -> lockQuality, 1, LITTLE_ENDIAN_ORDER);
		lengthData += sizeof(uint16_t);
		data += sizeof(uint16_t);
	}
	
	if(this -> getBitPresent(TX_ATTENUATION)) {
		lengthData = this -> insertPadding(&data, lengthData, INT_16);
		Utilities::memcpy16(data, &this -> TXattenuation, 1, LITTLE_ENDIAN_ORDER);
		lengthData += sizeof(uint16_t);
		data += sizeof(uint16_t);
	}

	if(this -> getBitPresent(DB_TX_ATTENUATION)) {
		lengthData = this -> insertPadding(&data, lengthData, INT_16);
		Utilities::memcpy16(data, &this -> dbTXattenuation, 1, LITTLE_ENDIAN_ORDER);
		lengthData += sizeof(uint16_t);
		data += sizeof(uint16_t);
	}

	if(this -> getBitPresent(DBM_TX_POWER)) {
		lengthData = this -> insertPadding(&data, lengthData, INT_8);
		Utilities::memcpy8(data, &this -> dbmTXpower, 1, LITTLE_ENDIAN_ORDER);
		lengthData += sizeof(int8_t);
		data += sizeof(int8_t);
	}

	if(this -> getBitPresent(ANTENNA)) {
		lengthData = this -> insertPadding(&data, lengthData, INT_8);
		Utilities::memcpy8(data, &this -> antenna, 1, LITTLE_ENDIAN_ORDER);
		lengthData += sizeof(int8_t);
		data += sizeof(int8_t);
	}

	if(this -> getBitPresent(DB_ANTENNA_SIGNAL)) {
		lengthData = this -> insertPadding(&data, lengthData, INT_8);
		Utilities::memcpy8(data, &this -> dbAntennaSignal, 1, LITTLE_ENDIAN_ORDER);
		lengthData += sizeof(uint8_t);
		data += sizeof(uint8_t);
	}
	
	if(this -> getBitPresent(DB_ANTENNA_NOISE)) {
		lengthData = this -> insertPadding(&data, lengthData, INT_8);
		Utilities::memcpy8(data, &this -> dbAntennaNoise, 1, LITTLE_ENDIAN_ORDER);
		lengthData += sizeof(uint8_t);
		data += sizeof(uint8_t);
	}
	
	if(this -> getBitPresent(RX_FLAGS)) {
		lengthData = this -> insertPadding(&data, lengthData, INT_16);
		Utilities::memcpy16(data, &this -> RXflags, 1, LITTLE_ENDIAN_ORDER);
		lengthData += sizeof(uint16_t);
		data += sizeof(uint16_t);
	}

	if(this -> getBitPresent(RTS_RETRIES)) {
		lengthData = this -> insertPadding(&data, lengthData, INT_8);
		Utilities::memcpy8(data, &this -> RTSretries, 1, LITTLE_ENDIAN_ORDER);
		lengthData += sizeof(uint8_t);
		data += sizeof(uint8_t);
	}

	if(this -> getBitPresent(DATA_RETRIES)) {
		lengthData = this -> insertPadding(&data, lengthData, INT_8);
		Utilities::memcpy8(data, &this -> dataRetries, 1, LITTLE_ENDIAN_ORDER);
		lengthData += sizeof(uint8_t);
		data += sizeof(uint8_t);
	}

	if(this -> getBitPresent(MCS)) {
		lengthData = this -> insertPadding(&data, lengthData, INT_8);
		Utilities::memcpy8(data, &this -> MCSknown, 1, LITTLE_ENDIAN_ORDER);
		lengthData += sizeof(uint8_t);
		data += sizeof(uint8_t);

		lengthData = this -> insertPadding(&data, lengthData, INT_8);
		Utilities::memcpy8(data, &this -> MCSflags, 1, LITTLE_ENDIAN_ORDER);
		lengthData += sizeof(uint8_t);
		data += sizeof(uint8_t);

		lengthData = this -> insertPadding(&data, lengthData, INT_8);
		Utilities::memcpy8(data, &this -> MCSmcs, 1, LITTLE_ENDIAN_ORDER);
		lengthData += sizeof(uint8_t);
		data += sizeof(uint8_t);
	}

	if(this -> getBitPresent(VENDOR_NAMESPACE)) {
		lengthData = this -> insertPadding(&data, lengthData, INT_8);
		Utilities::memcpy8(data, &this -> vendorNamespaceOUI, 3, LITTLE_ENDIAN_ORDER);
		lengthData += 3 * sizeof(uint8_t);
		data += 3 * sizeof(uint8_t);

		lengthData = this -> insertPadding(&data, lengthData, INT_8);
		Utilities::memcpy8(data, &this -> vendorNamespaceSubNamespace, 1, LITTLE_ENDIAN_ORDER);
		lengthData += sizeof(uint8_t);
		data += sizeof(uint8_t);

		lengthData = this -> insertPadding(&data, lengthData, INT_16);
		Utilities::memcpy16(data, &this -> vendorNamespaceSkipLength, 1, LITTLE_ENDIAN_ORDER);
		lengthData += sizeof(uint16_t);
		data += sizeof(uint16_t);
	}
	
	//nakonec upravim skutecnou delku hlavicky
	this -> len = lengthData;
	Utilities::memcpy16(locationLengthDataInHeader, &this -> len, 1, LITTLE_ENDIAN_ORDER);
	return this -> len;	
}

/* RadioTapHeader::printHeader
 * Popis: funkce provede zobrazeni hlavicky na terminalu ve strukturovane podobe
 * Parametry: -
 * Navratova hodnota: -
 */
void RadioTapHeader::printHeader() {
	//zjistime velikost hlavicky
	uint8_t header [LENGTH_HEADER];
	this -> len = this -> generateHeader(header);
	//vypiseme cely ramec
	printf("RadioTap:\r\n");
	printf("\tVersion ..................... 0x%02x\r\n", this -> version);
	printf("\tPad ......................... 0x%02x\r\n", this -> pad);
	printf("\tLen ......................... 0x%04x\r\n", this -> len);
	printf("\tPresent ..................... 0x%08x\r\n", this -> present);
	printf("\t\tTSFT ................ %d\r\n", this -> getBitPresent(TSFT));
	printf("\t\tFlags ............... %d\r\n", this -> getBitPresent(FLAGS));
	printf("\t\tRate ................ %d\r\n", this -> getBitPresent(RATE));
	printf("\t\tChannel ............. %d\r\n", this -> getBitPresent(CHANNEL));
	printf("\t\tFHSS ................ %d\r\n", this -> getBitPresent(FHSS));
        printf("\t\tAntenna signal ...... %d\r\n", this -> getBitPresent(ANTENNA_SIGNAL));
	printf("\t\tAntenna noise ....... %d\r\n", this -> getBitPresent(ANTENNA_NOISE));
	printf("\t\tLock quality ........ %d\r\n", this -> getBitPresent(LOCK_QUALITY));
	printf("\t\tTX attenuation ...... %d\r\n", this -> getBitPresent(TX_ATTENUATION));
	printf("\t\tdB TX attenuation ... %d\r\n", this -> getBitPresent(DB_TX_ATTENUATION));
	printf("\t\tdBm TX power ........ %d\r\n", this -> getBitPresent(DBM_TX_POWER));
	printf("\t\tAntenna ............. %d\r\n", this -> getBitPresent(ANTENNA));
	printf("\t\tdB antenna signal ... %d\r\n", this -> getBitPresent(DB_ANTENNA_SIGNAL));
	printf("\t\tdB antenna noise .... %d\r\n", this -> getBitPresent(DB_ANTENNA_NOISE));
	printf("\t\tRX flags ............ %d\r\n", this -> getBitPresent(RX_FLAGS));
	printf("\t\tRTS retries ......... %d\r\n", this -> getBitPresent(RTS_RETRIES));
	printf("\t\tDATA retries ........ %d\r\n", this -> getBitPresent(DATA_RETRIES));
	//printf("\t\tNotes ............... %d\r\n", this -> getBitPresent(NOTES));
	printf("\t\tMCS ................. %d\r\n", this -> getBitPresent(MCS));
	//printf("\t\tReset to Radiotap na. %d\r\n", this -> getBitPresent(RESET_TO_RADIOTAP_NAMESPACE));
	printf("\t\tVendor namespace .... %d\r\n", this -> getBitPresent(VENDOR_NAMESPACE));
	printf("\t\text ................. %d\r\n", this -> getBitPresent(EXT));
	
	printf("\r\n");

	if(this -> getBitPresent(TSFT)) {
		printf("\tTSFT mactime ............................... %lld\r\n", this -> getTSFTmactime());
	}	
	if(this -> getBitPresent(FLAGS)) {
		printf("\tFlags ...................................... %d (0x%02x)\r\n", this -> getFlags(), this -> getFlags());
		printf("\t\t sent/receive during CFP ........... %d\r\n", this -> getBitFlags(SENT_RECEIVE_DURING_CFP));
		printf("\t\t sent/receive with short preamble .. %d\r\n", this -> getBitFlags(SENT_RECEIVE_WITH_SHORT_PREAMBLE));
		printf("\t\t sent/receive with WEP encryption .. %d\r\n", this -> getBitFlags(SENT_RECEIVE_WITH_WEP_ENCRYPTION));
		printf("\t\t sent/receive with fragmentation ... %d\r\n", this -> getBitFlags(SENT_RECEIVE_WITH_FRAGMENTATION));
		printf("\t\t frame includes FCS ................ %d\r\n", this -> getBitFlags(FRAME_INCLUDES_FCS));
		printf("\t\t frame has padding 802.11 h and p .. %d\r\n", this -> getBitFlags(FRAME_HAS_PADDING_802_11));
		printf("\t\t frame failed FCS check ............ %d\r\n", this -> getBitFlags(FRAME_FAILED_FCS_CHECK));
		printf("\t\t frame used short guard interval ... %d\r\n", this -> getBitFlags(FRAME_USED_SHORT_GUARD_INTERVAL));
	}

	if(this -> getBitPresent(RATE)) {
		printf("\tRate ....................................... %d Kbps\r\n", this -> getRate()*500);
	}

	if(this -> getBitPresent(CHANNEL)) {
		printf("\tChannel frequency .......................... %d MHz\r\n", this -> getChannelFrequency());
		printf("\tChannel flags .............................. %d (0x%04x)\r\n", this -> getChannelFlags(),this -> getChannelFlags());
		printf("\t\tTurbo channel ...................... %d\r\n", this -> getBitChannelFlags(TURBO_CHANNEL));
		printf("\t\tCCK channel ........................ %d\r\n", this -> getBitChannelFlags(CCK_CHANNEL));
		printf("\t\tOFDM channel ....................... %d\r\n", this -> getBitChannelFlags(OFDM_CHANNEL));
		printf("\t\t2 GHz spectrum channel ............. %d\r\n", this -> getBitChannelFlags(GHZ2_SPECTRUM_CHANNEL));
		printf("\t\t5 GHz spectrum channel ............. %d\r\n", this -> getBitChannelFlags(GHZ5_SPECTRUM_CHANNEL));
		printf("\t\tOnly passive scan allowed .......... %d\r\n", this -> getBitChannelFlags(ONLY_PASSIVE_SCAN_ALLOWED));
		printf("\t\tDynamic CCK-OFDM channel ........... %d\r\n", this -> getBitChannelFlags(DYNAMIC_CCK_OFDM_CHANNEL));
		printf("\t\tGFSK channel (FHSS PHY) ............ %d\r\n", this -> getBitChannelFlags(GFSK_CHANNEL));
	}

	if(this -> getBitPresent(FHSS)) {
		printf("\tFHSS hop set ............................... %d\r\n", this -> getFHSShopSet());
		printf("\tFHSS hop pattern ........................... %d\r\n", this -> getFHSShopPattern());
	}

	if(this -> getBitPresent(ANTENNA_SIGNAL)) {
		printf("\tAntenna signal ............................. %d dBm\r\n", this -> getAntennaSignal());
	}

	if(this -> getBitPresent(ANTENNA_NOISE)) {
		printf("\tAntenna noise .............................. %d dBm\r\n", this -> getAntennaNoise());
	}

	if(this -> getBitPresent(LOCK_QUALITY)) {
		printf("\tLock quality ............................... %d\r\n", this -> getLockQuality());
	}
	
	if(this -> getBitPresent(TX_ATTENUATION)) {
		printf("\tTX attenuation ............................. %d\r\n", this -> getTXattenuation());
	}

	if(this -> getBitPresent(DB_TX_ATTENUATION)) {
		printf("\tdB TX attenuation .......................... %d dB\r\n", this -> getDbTXattenuation());
	}

	if(this -> getBitPresent(DBM_TX_POWER)) {
		printf("\tdBm TX power ............................... %d dBm\r\n", this -> getDbmTXpower());
	}

	if(this -> getBitPresent(ANTENNA)) {
		printf("\tAntenna .................................... %d\r\n", this -> getAntenna());
	}

	if(this -> getBitPresent(DB_ANTENNA_SIGNAL)) {
		printf("\tdB antenna signal .......................... %d dB\r\n", this -> getDbAntennaSignal());
	}
	
	if(this -> getBitPresent(DB_ANTENNA_NOISE)) {
		printf("\tdB antenna noise ........................... %d dB\r\n", this -> getDbAntennaNoise());
	}
	
	if(this -> getBitPresent(RX_FLAGS)) {
		printf("\tRX flags ................................... %d\r\n", this -> getRXflags());
		printf("\t\treserved [was FCS failed but........ %d\r\n", this -> getBitRXflags(RX_RESERVED));
		printf("\t\tPLCP CRC check failed .............. %d\r\n", this -> getBitRXflags(PLCP_CRC_CHECK_FAILED));
	}

	if(this -> getBitPresent(RTS_RETRIES)) {
		printf("\tRTS retries ................................ %d\r\n", this -> getRTSretries());
	}

	if(this -> getBitPresent(DATA_RETRIES)) {
		printf("\tdata retries ............................... %d\r\n", this -> getDataRetries());
	}

	if(this -> getBitPresent(MCS)) {
		printf("\tMCS known .................................. %d\r\n", this -> getMCSknown());
		printf("\t\tbandwidth .......................... %d\r\n", this -> getBitMCSknown(BANDWIDTH));	
		printf("\t\tMCS index known..................... %d\r\n", this -> getBitMCSknown(MCS_INDEX_KNOWN));
		printf("\t\tguard interval ..................... %d\r\n", this -> getBitMCSknown(GUARD_INTERVAL));	
		printf("\t\tHT format .......................... %d\r\n", this -> getBitMCSknown(HT_FORMAT));
		printf("\t\tFEC TYPE ........................... %d\r\n", this -> getBitMCSknown(FEC_TYPE));
		printf("\t\treserved ........................... %d\r\n", this -> getBitMCSknown(MCS_RESERVED));
		printf("\tMCS flags .................................. %d (0x%02x)\r\n", this -> getMCSflags(), this -> getMCSflags());
		printf("\t\tguard interval ..................... %d\r\n", this -> getBitMCSflags(GUARD_INTERVAL));
		printf("\t\tHT format .......................... %d\r\n", this -> getBitMCSflags(HT_FORMAT));
		printf("\t\tFEC type ........................... %d\r\n", this -> getBitMCSflags(FEC_TYPE));
		printf("\t\treserved ........................... %d\r\n", this -> getBitMCSflags(MCS_RESERVED));
		printf("\tMCS ........................................ %d\r\n", this -> getMCSmcs());
	}

	if(this -> getBitPresent(VENDOR_NAMESPACE)) {
		printf("\t Vendor namespace OUI ................. %02x%02x%02x\r\n", this -> getVendorNamespaceOUI()[0],
								     this -> getVendorNamespaceOUI()[1],
								     this -> getVendorNamespaceOUI()[2]);
		printf("\t Vendor namespace sub namespace ............ %d\r\n", this -> getVendorNamespaceSubNamespace());
		printf("\t Vendor namespace skip length .............. %d\r\n", this -> getVendorNamespaceSkipLength());
	}	
}

/* RadioTapHeader::deleteHeader
 * Popis: funkce provede odstraneni hodnot hlavicky
 * Parametry: -
 * Navratova hodnota: -
 */
void RadioTapHeader::deleteHeader() {
	this -> version = D_VERSION;
	this -> pad = D_PAD;
	this -> len = D_LEN;
	this -> present = D_PRESENT;
} 

/* RadioTapHeader::saveHeader
 * Popis: funkce provede ulozeni hlavicky do xml souboru
 * Parametry: -
 * Navratova hodnota: objekt, tvorici strom xml souboru
 */
TiXmlElement * RadioTapHeader::saveHeader() {
	TiXmlElement * radioTapHeaderElement = new TiXmlElement( "radioTapHeader" );
	
	//povinne polozky
	TiXmlElement * versionElement = new TiXmlElement( "version" );
	versionElement -> SetAttribute("value", this -> version );
	radioTapHeaderElement -> LinkEndChild( versionElement ); 

	TiXmlElement * padElement = new TiXmlElement( "pad" );
	padElement -> SetAttribute("value", this -> pad );
	radioTapHeaderElement -> LinkEndChild( padElement ); 

	TiXmlElement * lenElement = new TiXmlElement( "len" );
	lenElement -> SetAttribute("value", this -> len );
	radioTapHeaderElement -> LinkEndChild( lenElement ); 
	
	TiXmlElement * presentElement = new TiXmlElement( "present" );
	presentElement -> SetAttribute("value", this -> present );
	radioTapHeaderElement -> LinkEndChild( presentElement ); 

	//rozsirujici polozky
	//TiXmlElement * TSFTmactimeElement = new TiXmlElement( "TSFTmactime" );
	//TSFTmactimeElement -> SetAttribute("value", this -> TSFTmactime );
	//radioTapHeaderElement -> LinkEndChild( TSFTmactimeElement ); 

	char pom [200];
	sprintf(pom, "%lld", this -> TSFTmactime);
	TiXmlElement * TSFTmactimeElement = new TiXmlElement( "TSFTmactime" );
	TSFTmactimeElement -> LinkEndChild(new TiXmlText(pom));
	radioTapHeaderElement -> LinkEndChild( TSFTmactimeElement ); 

	TiXmlElement * flagsElement = new TiXmlElement( "flags" );
	flagsElement -> SetAttribute("value", this -> flags);
	radioTapHeaderElement -> LinkEndChild( flagsElement ); 
	
	TiXmlElement * antennaElement = new TiXmlElement( "antenna" );
	antennaElement -> SetAttribute("value", this -> antenna );
	radioTapHeaderElement -> LinkEndChild( antennaElement ); 

	TiXmlElement * antennaNoiseElement = new TiXmlElement( "antennaNoise" );
	antennaNoiseElement -> SetAttribute("value", this -> antennaNoise );
	radioTapHeaderElement -> LinkEndChild( antennaNoiseElement ); 

	TiXmlElement * antennaSignalElement = new TiXmlElement( "antennaSignal" );
	antennaSignalElement -> SetAttribute("value", this -> antennaSignal );
	radioTapHeaderElement -> LinkEndChild( antennaSignalElement ); 

	TiXmlElement * channelFrequencyElement = new TiXmlElement( "channelFrequency" );
	channelFrequencyElement -> SetAttribute("value", this -> channelFrequency );
	radioTapHeaderElement -> LinkEndChild( channelFrequencyElement ); 

	TiXmlElement * channelFlagsElement = new TiXmlElement( "channelFlags" );
	channelFlagsElement -> SetAttribute("value", this -> channelFlags );
	radioTapHeaderElement -> LinkEndChild( channelFlagsElement ); 

	TiXmlElement * FHSShopSetElement = new TiXmlElement( "FHSShopSet" );
	FHSShopSetElement -> SetAttribute("value", this -> FHSShopSet);
	radioTapHeaderElement -> LinkEndChild( FHSShopSetElement ); 

	TiXmlElement * FHSShopPatternElement = new TiXmlElement( "FHSShopPattern" );
	FHSShopPatternElement -> SetAttribute("value", this -> FHSShopPattern );
	radioTapHeaderElement -> LinkEndChild( FHSShopPatternElement ); 

	TiXmlElement * lockQualityElement = new TiXmlElement( "lockQuality" );
	lockQualityElement -> SetAttribute("value", this -> lockQuality );
	radioTapHeaderElement -> LinkEndChild( lockQualityElement ); 

	TiXmlElement * MCSknownElement = new TiXmlElement( "MCSknown" );
	MCSknownElement -> SetAttribute("value", this -> MCSknown );
	radioTapHeaderElement -> LinkEndChild( MCSknownElement ); 

	TiXmlElement * MCSflagsElement = new TiXmlElement( "MCSflags" );
	MCSflagsElement -> SetAttribute("value", this -> MCSflags );
	radioTapHeaderElement -> LinkEndChild( MCSflagsElement ); 

	TiXmlElement * MCSmcsElement = new TiXmlElement( "MCSmcs" );
	MCSmcsElement -> SetAttribute("value", this -> MCSmcs );
	radioTapHeaderElement -> LinkEndChild( MCSmcsElement ); 

	TiXmlElement * RXflagsElement = new TiXmlElement( "RXflags" );
	RXflagsElement -> SetAttribute("value", this -> RXflags );
	radioTapHeaderElement -> LinkEndChild( RXflagsElement ); 

	TiXmlElement * rateElement = new TiXmlElement( "rate" );
	rateElement -> SetAttribute("value", this -> rate );
	radioTapHeaderElement -> LinkEndChild( rateElement ); 	

	TiXmlElement * TXattenuationElement = new TiXmlElement( "TXattenuation" );
	TXattenuationElement -> SetAttribute("value", this -> TXattenuation );
	radioTapHeaderElement -> LinkEndChild( TXattenuationElement ); 

	TiXmlElement * vendorNamespaceOUIElement = new TiXmlElement( "vendorNamespaceOUI" );
	vendorNamespaceOUIElement -> LinkEndChild(new TiXmlText((char *)this -> vendorNamespaceOUI));
	radioTapHeaderElement -> LinkEndChild( vendorNamespaceOUIElement ); 

	TiXmlElement * vendorNamespaceSubNamespaceElement = new TiXmlElement( "vendorNamespaceSubNamespace" );
	vendorNamespaceSubNamespaceElement -> SetAttribute("value", this -> vendorNamespaceSubNamespace );
	radioTapHeaderElement -> LinkEndChild( vendorNamespaceSubNamespaceElement ); 

	TiXmlElement * vendorNamespaceSkipLengthElement = new TiXmlElement( "vendorNamespaceSkipLength" );
	vendorNamespaceSkipLengthElement -> SetAttribute("value", this -> vendorNamespaceSkipLength );
	radioTapHeaderElement -> LinkEndChild( vendorNamespaceSkipLengthElement ); 

	TiXmlElement * dbTXattenuationElement = new TiXmlElement( "dbTXattenuation" );
	dbTXattenuationElement -> SetAttribute("value", this -> dbTXattenuation );
	radioTapHeaderElement -> LinkEndChild( dbTXattenuationElement ); 

	TiXmlElement * dbAntennaNoiseElement = new TiXmlElement( "dbAntennaNoise" );
	dbAntennaNoiseElement -> SetAttribute("value", this -> dbAntennaNoise );
	radioTapHeaderElement -> LinkEndChild( dbAntennaNoiseElement ); 

	TiXmlElement * dbAntennaSignalElement = new TiXmlElement( "dbAntennaSignal" );
	dbAntennaSignalElement -> SetAttribute("value", this -> dbAntennaSignal );
	radioTapHeaderElement -> LinkEndChild( dbAntennaSignalElement ); 

	TiXmlElement * dbmTXpowerElement = new TiXmlElement( "dbmTXpower" );
	dbmTXpowerElement -> SetAttribute("value", this -> dbmTXpower );
	radioTapHeaderElement -> LinkEndChild( dbmTXpowerElement ); 

	TiXmlElement * RTSretriesElement = new TiXmlElement( "RTSretries" );
	RTSretriesElement -> SetAttribute("value", this -> RTSretries);
	radioTapHeaderElement -> LinkEndChild( RTSretriesElement ); 

	TiXmlElement * dataRetriesElement = new TiXmlElement( "dataRetries" );
	dataRetriesElement -> SetAttribute("value", this -> dataRetries );
	radioTapHeaderElement -> LinkEndChild( dataRetriesElement ); 

	return radioTapHeaderElement;
}

/* RadioTapHeader::loadHeader
 * Popis: funkce provede nacteni hlavicky z xml souboru
 * Parametry: element - objekt, tvorici strom xml souboru
 * Navratova hodnota: -
 */
void RadioTapHeader::loadHeader(TiXmlElement * element) {
	TiXmlHandle hRadioTapHeaderElement(element);
	int pom = 0;
	//povinne polozky
	TiXmlElement * versionElement = hRadioTapHeaderElement.FirstChild( "version" ).Element();
	if(versionElement) { versionElement -> QueryIntAttribute("value", &pom ); this -> version = (uint8_t)pom; }

	TiXmlElement * padElement = hRadioTapHeaderElement.FirstChild( "pad" ).Element();
	if(padElement) { padElement -> QueryIntAttribute("value", &pom ); this -> pad = (uint8_t)pom; }

	TiXmlElement * lenElement = hRadioTapHeaderElement.FirstChild( "len" ).Element();
	if(lenElement) { lenElement -> QueryIntAttribute("value", &pom ); this -> len = (uint16_t)pom; }

	TiXmlElement * presentElement = hRadioTapHeaderElement.FirstChild( "present" ).Element();
	if(presentElement) { presentElement -> QueryIntAttribute("value", &pom ); this -> present = (uint32_t)pom; }

	//rozsirujici polozky
	//TiXmlElement * TSFTmactimeElement = hRadioTapHeaderElement.FirstChild( "TSFTmactime" ).Element();
	//if(TSFTmactimeElement) { TSFTmactimeElement -> QueryIntAttribute("value", &pom ); this -> TSFTmactime = pom; }

	TiXmlElement * TSFTmactimeElement = hRadioTapHeaderElement.FirstChild( "TSFTmactime" ).Element();
	if(TSFTmactimeElement) {
		if(TSFTmactimeElement -> GetText() != NULL) {
			this -> TSFTmactime = strtoll(TSFTmactimeElement -> GetText(),NULL, 10);
		}
	}

	TiXmlElement * flagsElement = hRadioTapHeaderElement.FirstChild( "flags" ).Element();
	if(flagsElement) { flagsElement -> QueryIntAttribute("value", &pom ); this -> flags = (uint8_t)pom; }

	TiXmlElement * antennaElement = hRadioTapHeaderElement.FirstChild( "antenna" ).Element();
	if(antennaElement) { antennaElement -> QueryIntAttribute("value", &pom); this -> antenna = (uint8_t)pom; }

	TiXmlElement * antennaNoiseElement = hRadioTapHeaderElement.FirstChild( "antennaNoise" ).Element();
	if(antennaNoiseElement) { antennaNoiseElement -> QueryIntAttribute("value", &pom ); this -> antennaNoise = (uint8_t)pom; }

	TiXmlElement * antennaSignalElement = hRadioTapHeaderElement.FirstChild( "antennaSignal" ).Element();
	if(antennaSignalElement) { antennaSignalElement -> QueryIntAttribute("value", &pom ); this -> antennaSignal = (uint8_t)pom; }

	TiXmlElement * channelFrequencyElement = hRadioTapHeaderElement.FirstChild( "channelFrequency" ).Element();
	if(channelFrequencyElement) { channelFrequencyElement -> QueryIntAttribute("value", &pom); this -> channelFrequency = (uint16_t)pom; }

	TiXmlElement * channelFlagsElement = hRadioTapHeaderElement.FirstChild( "channelFlags" ).Element();
	if(channelFlagsElement) { channelFlagsElement -> QueryIntAttribute("value", &pom); this -> channelFlags = (uint16_t)pom; }

	TiXmlElement * FHSShopSetElement = hRadioTapHeaderElement.FirstChild( "FHSShopSet" ).Element();
	if(FHSShopSetElement) { FHSShopSetElement -> QueryIntAttribute("value", &pom); this -> FHSShopSet = (uint8_t)pom; }

	TiXmlElement * FHSShopPatternElement = hRadioTapHeaderElement.FirstChild( "FHSShopPattern" ).Element();
	if(FHSShopPatternElement) { FHSShopPatternElement -> QueryIntAttribute("value", &pom ); this -> FHSShopPattern = (uint8_t)pom; }

	TiXmlElement * lockQualityElement = hRadioTapHeaderElement.FirstChild( "lockQuality" ).Element();
	if(lockQualityElement) { lockQualityElement -> QueryIntAttribute("value", &pom ); this -> lockQuality = (uint16_t)pom;}

	TiXmlElement * MCSknownElement = hRadioTapHeaderElement.FirstChild( "MCSknown" ).Element();
	if(MCSknownElement) { MCSknownElement -> QueryIntAttribute("value", &pom ); this -> MCSknown = (uint8_t)pom; }

	TiXmlElement * MCSflagsElement = hRadioTapHeaderElement.FirstChild( "MCSflags" ).Element();
	if(MCSflagsElement) { MCSflagsElement -> QueryIntAttribute("value", &pom); this -> MCSflags = (uint8_t)pom; }

	TiXmlElement * MCSmcsElement = hRadioTapHeaderElement.FirstChild( "MCSmcs" ).Element();
	if(MCSmcsElement) { MCSmcsElement -> QueryIntAttribute("value", &pom ); this -> MCSmcs = (uint8_t)pom; }

	TiXmlElement * RXflagsElement = hRadioTapHeaderElement.FirstChild( "RXflags" ).Element();
	if(RXflagsElement) { RXflagsElement -> QueryIntAttribute("value", &pom ); this -> RXflags = (uint16_t)pom; }

	TiXmlElement * rateElement = hRadioTapHeaderElement.FirstChild( "rate" ).Element();
	if(rateElement) { rateElement -> QueryIntAttribute("value", &pom ); this -> rate = (uint8_t)pom; }

	TiXmlElement * TXattenuationElement = hRadioTapHeaderElement.FirstChild( "TXattenuation" ).Element();
	if(TXattenuationElement) { TXattenuationElement -> QueryIntAttribute("value", &pom); this -> TXattenuation = (uint16_t)pom; }

	TiXmlElement * vendorNamespaceOUIElement = hRadioTapHeaderElement.FirstChild( "vendorNamespaceOUI" ).Element();
	if(vendorNamespaceOUIElement) {
		if(vendorNamespaceOUIElement -> GetText() != NULL) {
			Utilities::memcpy8(this -> vendorNamespaceOUI, vendorNamespaceOUIElement -> GetText(), 3, LITTLE_ENDIAN_ORDER);
			this -> vendorNamespaceOUI[3] = 0;
		} else {
			this -> vendorNamespaceOUI[0] = 0;
		}
	}

	TiXmlElement * vendorNamespaceSubNamespaceElement = hRadioTapHeaderElement.FirstChild( "vendorNamespaceSubNamespace" ).Element();
	if(vendorNamespaceSubNamespaceElement) { vendorNamespaceSubNamespaceElement -> QueryIntAttribute("value", &pom ); 
                                                 this -> vendorNamespaceSubNamespace = (uint8_t)pom; }

	TiXmlElement * vendorNamespaceSkipLengthElement = hRadioTapHeaderElement.FirstChild( "vendorNamespaceSkipLength" ).Element();
	if(vendorNamespaceSkipLengthElement) { vendorNamespaceSkipLengthElement -> QueryIntAttribute("value", &pom ); 
                                               this -> vendorNamespaceSkipLength = (uint16_t)pom; }

	TiXmlElement * dbTXattenuationElement = hRadioTapHeaderElement.FirstChild( "dbTXattenuation" ).Element();
	if(dbTXattenuationElement) { dbTXattenuationElement -> QueryIntAttribute("value", (int*)&pom ); this -> dbTXattenuation = (uint16_t)pom; }

	TiXmlElement * dbAntennaNoiseElement = hRadioTapHeaderElement.FirstChild( "dbAntennaNoise" ).Element();
	if(dbAntennaNoiseElement) { dbAntennaNoiseElement -> QueryIntAttribute("value", &pom ); this -> dbAntennaNoise = (uint8_t)pom; }

	TiXmlElement * dbAntennaSignalElement = hRadioTapHeaderElement.FirstChild( "dbAntennaSignal" ).Element();
	if(dbAntennaSignalElement) { dbAntennaSignalElement -> QueryIntAttribute("value", &pom ); this -> dbAntennaSignal = (uint8_t)pom; }

	TiXmlElement * dbmTXpowerElement = hRadioTapHeaderElement.FirstChild( "dbmTXpower" ).Element();
	if(dbmTXpowerElement) { dbmTXpowerElement -> QueryIntAttribute("value", &pom ); this -> dbmTXpower = (uint8_t)pom; }

	TiXmlElement * RTSretriesElement = hRadioTapHeaderElement.FirstChild( "RTSretries" ).Element();
	if(RTSretriesElement) { RTSretriesElement -> QueryIntAttribute("value", &pom ); this -> RTSretries = (uint8_t)pom; }

	TiXmlElement * dataRetriesElement = hRadioTapHeaderElement.FirstChild( "dataRetries" ).Element();
	if(dataRetriesElement) { dataRetriesElement -> QueryIntAttribute("value", &pom ); this -> dataRetries = (uint8_t)pom; }
}
