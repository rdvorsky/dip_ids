/* Soubor: LLCHeader.h
 * Autor: Pavel Svanda
 * Email: svandap@gmail.com
 * Datum vytvoreni: 13.3.2013
 * Popis: deklarace tridy LLCHeader, ktera slouzi jako reprezentace LLC hlavicky
 */
#ifndef LLCHEADER_H
#define LLCHEADER_H

#include "Header.h"

#define LENGTH_LLC_HEADER	   8

//typy paketu, ktere nasleduji za LLC
#define V_LLC_IPv4		0x0800
#define V_LLC_IPv6		0x86dd
#define V_LLC_ARP		0x0806
#define V_LLC_RARP		0x8035
#define V_LLC_MPLSu		0x8847
#define V_LLC_MPLSm		0x8848
#define V_LLC_JUMBO_FRAME	0x8870
#define V_LLC_EAPOL		0x880e
#define V_LLC_LLDP		0x88cc
#define V_LLC_AARP		0x80f3

//textove retezce typu ramcu
#define S_LLC_IPv4		"IPv4"
#define S_LLC_IPv6		"IPv6"
#define S_LLC_ARP		"ARP"
#define S_LLC_RARP		"RARP"
#define S_LLC_MPLSu		"MPLS unicast"
#define S_LLC_MPLSm		"MPLS multicast"
#define S_LLC_JUMBO_FRAME	"JUMBO FRAME"
#define S_LLC_EAPOL		"EAPOL"
#define S_LLC_LLDP		"LLDP"
#define S_LLC_AARP		"AARP"
#define S_LLC_UNKNOWN		"unknown"

//defaultni hodnoty
#define D_DNAP  		0xaa
#define D_SNAP			0xaa
#define D_CONTROL_FIELD 	0x03
#define D_ORGANIZATION_CODE	0x00
#define D_LLC_TYPE		0x00

class LLCHeader : public Header {
	private:
		uint8_t  dnap;
		uint8_t  snap;
		uint8_t  controlField;
		uint32_t organizationCode;
		uint16_t  type;
	public:
		LLCHeader();
	       ~LLCHeader();

		//getry a setry
		uint8_t getDnap();
		void setDnap(uint8_t dnap);
		void delDnap();

		uint8_t getSnap();
		void setSnap(uint8_t snap);
		void delSnap();
		
		uint8_t getControlField();
		void setControlField(uint8_t controlField);
		void delControlField();


		uint32_t getOrganizationCode();
		void setOrganizationCode(uint32_t organizationCode);
		void delOrganizationCode();

		uint16_t getType();
		void setType(uint16_t type);
		void delType();

		void getTypeString(char * string);

		//dalsi funkce
		virtual uint32_t generateHeader(uint8_t * data);
		virtual void printHeader();
		virtual void deleteHeader();
		virtual TiXmlElement * saveHeader();
		virtual void loadHeader(TiXmlElement * element);

};

#endif
