/* Soubor: LLCHeader.cpp
 * Autor: Pavel Svanda
 * Email: svandap@gmail.com
 * Datum vytvoreni: 13.3.2013
 * Popis: definice tridy LLCHeader, ktera slouzi jako reprezentace LLC hlavicky
 */
#include "LLCHeader.h"

/* LLCHeader::LLCHeader
 * Popis: konstruktor
 * Parametry: -
 * Navratova hodnota: -
 */
LLCHeader::LLCHeader() {
	this -> deleteHeader();
}

/* LLCHeader::~LLCHeader
 * Popis: destruktor
 * Parametry: -
 * Navratova hodnota: -
 */
LLCHeader::~LLCHeader() {

}

/* Getry a setry
 */
uint8_t LLCHeader::getDnap() {
	return this -> dnap;
}

void LLCHeader::setDnap(uint8_t dnap) {
	this -> dnap = dnap;
}

void LLCHeader::delDnap() {
	this -> dnap = D_DNAP;
}

uint8_t LLCHeader::getSnap() {
	return this -> snap;
}

void LLCHeader::setSnap(uint8_t snap) {
	this -> snap = snap;
}

void LLCHeader::delSnap() {
	this -> snap = D_SNAP;
}
		
uint8_t LLCHeader::getControlField() {
	return this -> controlField;
}

void LLCHeader::setControlField(uint8_t controlField) {
	this -> controlField = controlField;
}

void LLCHeader::delControlField() {
	this -> controlField = D_CONTROL_FIELD;
}

uint32_t LLCHeader::getOrganizationCode() {
	return this -> organizationCode;
}

void LLCHeader::setOrganizationCode(uint32_t organizationCode) {
	this -> organizationCode = organizationCode;
}

void LLCHeader::delOrganizationCode() {
	this -> organizationCode = D_ORGANIZATION_CODE;
}

uint16_t LLCHeader::getType() {
	return this -> type;
}

void LLCHeader::setType(uint16_t type) {
	this -> type = type;
}

void LLCHeader::delType() {
	this -> type = D_LLC_TYPE;
}

/* LLCHeader::getTypeString
 * Popis: funkce navrati textovy retezec nazvu typu
 * Parametry: string - misto, kam se retezec ulozi
 * Navratova hodnota: -
 */
void LLCHeader::getTypeString(char * string) {
	switch(this->getType()) {
		case V_LLC_IPv4 : {
			strcpy(string, S_LLC_IPv4);
		}; break;
		case V_LLC_IPv6 : {
			strcpy(string, S_LLC_IPv6);
		}; break;
		case V_LLC_ARP : {
			strcpy(string, S_LLC_ARP);
		}; break;
		case V_LLC_RARP : {
			strcpy(string, S_LLC_RARP);
		}; break;
		case V_LLC_MPLSu : {
			strcpy(string, S_LLC_MPLSu);
		}; break;
		case V_LLC_MPLSm : {
			strcpy(string, S_LLC_MPLSm);
		}; break;
		case V_LLC_JUMBO_FRAME : {
			strcpy(string, S_LLC_JUMBO_FRAME);
		}; break;
		case V_LLC_EAPOL : {
			strcpy(string, S_LLC_EAPOL);
		}; break;
		case V_LLC_LLDP : {
			strcpy(string, S_LLC_LLDP);
		}; break;
		case V_LLC_AARP : {
			strcpy(string, S_LLC_AARP);
		}; break;
		default : {
			strcpy(string, S_LLC_UNKNOWN);
		}
	}
}

/* LLCHeader::generateHeader
 * Popis: funkce provede vyganerovani hlavicky ze zadanych udaji
 * Parametry: uint8_t * data - ukazatel do pameti, kam se hlavicka vygeneruje
 * Navratova hodnota: delka hlavicky
 */
uint32_t LLCHeader::generateHeader(uint8_t * data) {
	uint32_t length = 0;

	Utilities::memcpy8(data, &this -> dnap, 1, BIG_ENDIAN_ORDER);
	data += sizeof(uint8_t);
	length += sizeof(uint8_t);
	Utilities::memcpy8(data, &this -> snap, 1, BIG_ENDIAN_ORDER);
	data += sizeof(uint8_t);
	length += sizeof(uint8_t);
	Utilities::memcpy8(data, &this -> controlField, 1, BIG_ENDIAN_ORDER);
	data += sizeof(uint8_t);
	length += sizeof(uint8_t);
	Utilities::memcpy24(data, &this -> organizationCode, 1, BIG_ENDIAN_ORDER);
	data += (sizeof(uint8_t) * 3);
	length += (sizeof(uint8_t) * 3);
	Utilities::memcpy16(data, &this -> type, 1, BIG_ENDIAN_ORDER);
	data += sizeof(uint16_t);
	length += sizeof(uint16_t);

	return length;
}

/* LLCHeader::printHeader
 * Popis: funkce provede zobrazeni hlavicky na terminalu ve strukturovane podobe
 * Parametry: -
 * Navratova hodnota: -
 */
void LLCHeader::printHeader() {
	//vypiseme cely ramec
	printf("LLC:\r\n");
	printf("\tdnap ........................ 0x%02x\r\n", this -> dnap);
	printf("\tsnap ........................ 0x%02x\r\n", this -> snap);
	printf("\tcontrol field ............... 0x%02x\r\n", this -> controlField);
	printf("\torganization code ........... 0x%06x\r\n", this -> organizationCode);
	char str[200];
	this -> getTypeString(str);
	printf("\ttype ........................ 0x%04x (%s)\r\n", this -> type, str);
}

/* LLCHeader::deleteHeader
 * Popis: funkce provede odstraneni hodnot hlavicky
 * Parametry: -
 * Navratova hodnota: -
 */
void LLCHeader::deleteHeader() {
	this -> delDnap();
	this -> delSnap();
	this -> delControlField();
	this -> delOrganizationCode();
	this -> delType();
} 

/* LLCHeader::saveHeader
 * Popis: funkce provede ulozeni hlavicky do xml souboru
 * Parametry: -
 * Navratova hodnota: objekt, tvorici strom xml souboru
 */
TiXmlElement * LLCHeader::saveHeader() {
	TiXmlElement * llcHeaderElement = new TiXmlElement( "llcHeader" );
	
	//povinne polozky
	TiXmlElement * dnapElement = new TiXmlElement( "dnap" );
	dnapElement -> SetAttribute("value", this -> dnap );
	llcHeaderElement -> LinkEndChild( dnapElement ); 

	TiXmlElement * snapElement = new TiXmlElement( "snap" );
	snapElement -> SetAttribute("value", this -> snap );
	llcHeaderElement -> LinkEndChild( snapElement ); 

	TiXmlElement * controlFieldElement = new TiXmlElement( "controlField" );
	controlFieldElement -> SetAttribute("value", this -> controlField );
	llcHeaderElement -> LinkEndChild( controlFieldElement ); 

	TiXmlElement * organizationCodeElement = new TiXmlElement( "organizationCode" );
	organizationCodeElement -> SetAttribute("value", this -> organizationCode );
	llcHeaderElement -> LinkEndChild( organizationCodeElement ); 

	TiXmlElement * typeElement = new TiXmlElement( "type" );
	typeElement -> SetAttribute("value", this -> type );
	llcHeaderElement -> LinkEndChild( typeElement ); 

	return llcHeaderElement;
}

/* LLCHeader::loadHeader
 * Popis: funkce provede nacteni hlavicky z xml souboru
 * Parametry: element - objekt, tvorici strom xml souboru
 * Navratova hodnota: -
 */
void LLCHeader::loadHeader(TiXmlElement * element) {
	TiXmlHandle hLLCHeaderElement(element);
	int pom = 0;

	TiXmlElement * dnapElement = hLLCHeaderElement.FirstChild( "dnap" ).Element();
	if(dnapElement) { dnapElement -> QueryIntAttribute("value", &pom ); this -> dnap = (uint8_t)pom; }

	TiXmlElement * snapElement = hLLCHeaderElement.FirstChild( "snap" ).Element();
	if(snapElement) { snapElement -> QueryIntAttribute("value", &pom ); this -> snap = (uint8_t)pom; }

	TiXmlElement * controlFieldElement = hLLCHeaderElement.FirstChild( "controlField" ).Element();
	if(controlFieldElement) { controlFieldElement -> QueryIntAttribute("value", &pom ); this -> controlField = (uint8_t)pom; }

	TiXmlElement * organizationCodeElement = hLLCHeaderElement.FirstChild( "organizationCode" ).Element();
	if(organizationCodeElement) { organizationCodeElement -> QueryIntAttribute("value", &pom ); this -> organizationCode = (uint32_t)pom; }

	TiXmlElement * typeElement = hLLCHeaderElement.FirstChild( "type" ).Element();
	if(typeElement) { typeElement -> QueryIntAttribute("value", &pom ); this -> type = (uint16_t)pom; }
}
