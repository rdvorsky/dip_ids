/* Soubor: IeeeHeader.cpp
 * Autor: Pavel Svanda
 * Email: svandap@gmail.com
 * Datum vytvoreni: 3.7.2012
 * Popis: definice tridy IeeeHeader, ktera slouzi jako reprezentace Ieee hlavicky
 */

#include "IeeeHeader.h"

/* IeeeHeader::IeeeHeader
 * Popis: konstruktor
 * Parametry: -
 * Navratova hodnota: -
 */
IeeeHeader::IeeeHeader() {
	this -> deleteHeader();
}

/* IeeeHeader::~IeeeHeader
 * Popis: destruktor
 * Parametry: -
 * Navratova hodnota: -
 */
IeeeHeader::~IeeeHeader() {

}

/* IeeeHeader::setDATAframe
 * Popis: funkce prednastavi hlavicku povinnymi polozkami, tak aby se jednalo o DATA ramec
 * Parametry: -
 * Navratova hodnota: -
 */
void IeeeHeader::setDATAframe() {
	this -> setProtocol(0);
	this -> setType(T_DATA_FRAME);
	this -> setSubType(ST_DATA);
	this -> setToDS(0);
	this -> setFromDS(0);
	this -> setMoreFrag(0);
	this -> setRetry(0);
	this -> setMoreData(0);
	this -> setProtectedFrame(0);
	this -> setOrder(0);
}

/* IeeeHeader::setQosDATAframe
 * Popis: funkce prednastavi hlavicku povinnymi polozkami, tak aby se jednalo o Qos DATA ramec
 * Parametry: -
 * Navratova hodnota: -
 */
void IeeeHeader::setQosDATAframe() {
	this -> setProtocol(0);
	this -> setType(T_DATA_FRAME);
	this -> setSubType(ST_QOS_DATA);
	this -> setToDS(0);
	this -> setFromDS(0);
	this -> setMoreFrag(0);
	this -> setRetry(0);
	this -> setMoreData(0);
	this -> setProtectedFrame(0);
	this -> setOrder(0);
}

/* IeeeHeader::setRTSframe
 * Popis: funkce prednastavi hlavicku povinnymi polozkami, tak aby se jednalo o RTS ramec
 * Parametry: -
 * Navratova hodnota: -
 */
void IeeeHeader::setRTSframe() {
	this -> setProtocol(0);
	this -> setType(T_CONTROL_FRAME);
	this -> setSubType(ST_RTS);
	this -> setToDS(0);
	this -> setFromDS(0);
	this -> setMoreFrag(0);
	this -> setRetry(0);
	this -> setMoreData(0);
	this -> setProtectedFrame(0);
	this -> setOrder(0);
}

/* IeeeHeader::setCTSframe
 * Popis: funkce prednastavi hlavicku povinnymi polozkami, tak aby se jednalo o CTS ramec
 * Parametry: -
 * Navratova hodnota: -
 */
void IeeeHeader::setCTSframe() {
	this -> setProtocol(0);
	this -> setType(T_CONTROL_FRAME);
	this -> setSubType(ST_CTS);
	this -> setToDS(0);
	this -> setFromDS(0);
	this -> setMoreFrag(0);
	this -> setRetry(0);
	this -> setMoreData(0);
	this -> setProtectedFrame(0);
	this -> setOrder(0);
}

/* IeeeHeader::setACKframe
 * Popis: funkce prednastavi hlavicku povinnymi polozkami, tak aby se jednalo o ACK ramec
 * Parametry: -
 * Navratova hodnota: -
 */
void IeeeHeader::setACKframe() {
	this -> setProtocol(0);
	this -> setType(T_CONTROL_FRAME);
	this -> setSubType(ST_ACK);
	this -> setToDS(0);
	this -> setFromDS(0);
	this -> setRetry(0);
	this -> setMoreData(0);
	this -> setProtectedFrame(0);
	this -> setOrder(0);
}

/* IeeeHeader::setPSPOLLframe
 * Popis: funkce prednastavi hlavicku povinnymi polozkami, tak aby se jednalo o PS-POLL ramec
 * Parametry: -
 * Navratova hodnota: -
 */
void IeeeHeader::setPSPOLLframe() {
	this -> setProtocol(0);
	this -> setType(T_CONTROL_FRAME);
	this -> setSubType(ST_PS_POLL);
	this -> setToDS(0);
	this -> setFromDS(0);
	this -> setMoreFrag(0);
	this -> setRetry(0);
	this -> setMoreData(0);
	this -> setProtectedFrame(0);
	this -> setOrder(0);
}

/* IeeeHeader::setBEACONframe
 * Popis: funkce prednastavi hlavicku povinnymi polozkami, tak aby se jednalo o BEACON ramec
 * Parametry: -
 * Navratova hodnota: -
 */
void IeeeHeader::setBEACONframe() {
	this -> setProtocol(0);
	this -> setType(T_MANAGEMENT_FRAME);
	this -> setSubType(ST_BEACON);
}

/* IeeeHeader::setPROBEREQUESTframe
 * Popis: funkce prednastavi hlavicku povinnymi polozkami, tak aby se jednalo o PROBE REQUEST ramec
 * Parametry: -
 * Navratova hodnota: -
 */
void IeeeHeader::setPROBEREQUESTframe() {
	this -> setProtocol(0);
	this -> setType(T_MANAGEMENT_FRAME);
	this -> setSubType(ST_PROBE_REQUEST);
}

/* IeeeHeader::setPROBERESPONSEframe
 * Popis: funkce prednastavi hlavicku povinnymi polozkami, tak aby se jednalo o PROBE RESPONSE ramec
 * Parametry: -
 * Navratova hodnota: -
 */
void IeeeHeader::setPROBERESPONSEframe() {
	this -> setProtocol(0);
	this -> setType(T_MANAGEMENT_FRAME);
	this -> setSubType(ST_PROBE_RESPONSE);
}

/* IeeeHeader::setATIMframe
 * Popis: funkce prednastavi hlavicku povinnymi polozkami, tak aby se jednalo o ATIM ramec
 * Parametry: -
 * Navratova hodnota: -
 */
void IeeeHeader::setATIMframe() {
	this -> setProtocol(0);
	this -> setType(T_MANAGEMENT_FRAME);
	this -> setSubType(ST_ATIM);
}

/* IeeeHeader::setDEAUTHETICATIONframe
 * Popis: funkce prednastavi hlavicku povinnymi polozkami, tak aby se jednalo o DEAUTHETICATION ramec
 * Parametry: -
 * Navratova hodnota: -
 */
void IeeeHeader::setDEAUTHETICATIONframe() {
	this -> setProtocol(0);
	this -> setType(T_MANAGEMENT_FRAME);
	this -> setSubType(ST_DEAUTHETICATION);
}

/* IeeeHeader::setDISASSOCIATIONframe
 * Popis: funkce prednastavi hlavicku povinnymi polozkami, tak aby se jednalo o DISASSOCIATION ramec
 * Parametry: -
 * Navratova hodnota: -
 */
void IeeeHeader::setDISASSOCIATIONframe() {
	this -> setProtocol(0);
	this -> setType(T_MANAGEMENT_FRAME);
	this -> setSubType(ST_DISASSOCIATION);
}

/* IeeeHeader::setASSOCIATIONREQUESTframe
 * Popis: funkce prednastavi hlavicku povinnymi polozkami, tak aby se jednalo o ASSOCIATION REQUESTramec
 * Parametry: -
 * Navratova hodnota: -
 */
void IeeeHeader::setASSOCIATIONREQUESTframe() {
	this -> setProtocol(0);
	this -> setType(T_MANAGEMENT_FRAME);
	this -> setSubType(ST_ASSOCIATION_REQUEST);
}

/* IeeeHeader::setREASSOCIATIONREQUESTframe
 * Popis: funkce prednastavi hlavicku povinnymi polozkami, tak aby se jednalo o REASSOCIATION REQUEST ramec
 * Parametry: -
 * Navratova hodnota: -
 */
void IeeeHeader::setREASSOCIATIONREQUESTframe() {
	this -> setProtocol(0);
	this -> setType(T_MANAGEMENT_FRAME);
	this -> setSubType(ST_REASSOCIATION_REQUEST);
}

/* IeeeHeader::setREASSOCIATIONRESPONSEframe
 * Popis: funkce prednastavi hlavicku povinnymi polozkami, tak aby se jednalo o REASSOCIATION RESPONSE ramec
 * Parametry: -
 * Navratova hodnota: -
 */
void IeeeHeader::setREASSOCIATIONRESPONSEframe() {
	this -> setProtocol(0);
	this -> setType(T_MANAGEMENT_FRAME);
	this -> setSubType(ST_REASSOCIATION_RESPONSE);
	this -> taggedParameters [TP_SUPPORTED_RATE] = true;
}

/* IeeeHeader::setAUTHETICATIONframe
 * Popis: funkce prednastavi hlavicku povinnymi polozkami, tak aby se jednalo o AUTHETICATION ramec
 * Parametry: -
 * Navratova hodnota: -
 */
void IeeeHeader::setAUTHENTICATIONframe() {
	this -> setProtocol(0);
	this -> setType(T_MANAGEMENT_FRAME);
	this -> setSubType(ST_AUTHETICATION);
}

/* IeeeHeader::setFrameControl
 * Popis: nastavi hodnotu do pole frame control - jedna hodnota definujici cele pole
 * Parametry: frameControl - nastavovana hodnota
 * Navratova hodnota: -
 */
void IeeeHeader::setFrameControl(uint16_t frameControl) {
	this -> frameControl = frameControl;
}

/* IeeeHeader::getFrameControl
 * Popis: vrati hodnotu celeho pole frame control
 * Parametry: -
 * Navratova hodnota: hodnota frame control
 */
uint16_t IeeeHeader::getFrameControl() {
	return this -> frameControl;
}

/* IeeeHeader::delFrameControl
 * Popis: nastavi hodnotru frame control na default hodnotu
 * Parametry: -
 * Navratova hodnota: -
 */
void IeeeHeader::delFrameControl() {
	this -> frameControl = D_FRAME_CONTROL;
}

/* IeeeHeader::setBitFrameControl
 * Popis: nastavi jednotlive bity frame control
 * Parametry: numberBit - cislo bitu, ktery nastavujeme
 *	      valueBit  - hodnota bitu, na ktery jej nastavujeme
 * Navratova hodnota: -
 */		
void IeeeHeader::setBitFrameControl(uint8_t numberBit, uint8_t valueBit) {
	//hodnota nastavovaneho bitu je 0
	if(valueBit == 0) {
		this -> frameControl &= (~(uint32_t)pow(2, numberBit));
	} else {
	//hodnota nastavovaneho bitu je 1
		this -> frameControl |= (uint32_t)pow(2, numberBit);
	}	
}

/* IeeeHeader::getBitFrameControl
 * Popis: vrati hodnotu pozadovaneho bitu pole frame control
 * Parametry: numberBit - cislo bitu, ktery je pozadovan
 * Navratova hodnota: hodnota bitu
 */
uint8_t IeeeHeader::getBitFrameControl(uint8_t numberBit) {
	if((this -> frameControl & (uint32_t)pow(2,numberBit)) != 0) {
		return 1;
	} else {
		return 0;
	}
}

//Zde nasleduje nastaveni jednotlivych polozek pole frame control - nektera jsou vice bitova, tak to nelze resit pomoci setBitFrameControl
void IeeeHeader::setProtocol(uint8_t protocol) {
	int op = 1;
	int bit = protocol & op;
	this -> setBitFrameControl(FC_PROTOCOL1, bit);
	op = 2;
	bit = protocol & op;
	this -> setBitFrameControl(FC_PROTOCOL2, bit);
}
uint8_t IeeeHeader::getProtocol() {
	int bit1 = this->getBitFrameControl(FC_PROTOCOL2);
	int bit0 = this->getBitFrameControl(FC_PROTOCOL1);
	return bit1*2+bit0;
}
void IeeeHeader::delProtocol() {
	this -> setProtocol(D_PROTOCOL);
}

void IeeeHeader::setType(uint8_t type) {
	int op = 1;
	int bit = type & op;
	this -> setBitFrameControl(FC_TYPE1, bit);
	op = 2;
	bit = type & op;
	this -> setBitFrameControl(FC_TYPE2, bit);
}

uint8_t IeeeHeader::getType() {
	int bit1 = this->getBitFrameControl(FC_TYPE2);
	int bit0 = this->getBitFrameControl(FC_TYPE1);
	return bit1*2+bit0;
}

void IeeeHeader::delType() {
	this -> setType(D_TYPE);
}

void IeeeHeader::setSubType(uint8_t subType) {
	int op = 1;
	int bit = subType & op;
	this -> setBitFrameControl(FC_SUB_TYPE1, bit);
	op = 2;
	bit = subType & op;
	this -> setBitFrameControl(FC_SUB_TYPE2, bit);
	op = 4;
	bit = subType & op;
	this -> setBitFrameControl(FC_SUB_TYPE3, bit);
	op = 8;
	bit = subType & op;
	this -> setBitFrameControl(FC_SUB_TYPE4, bit);
}

uint8_t IeeeHeader::getSubType() {
	int bit3 = this->getBitFrameControl(FC_SUB_TYPE4);
	int bit2 = this->getBitFrameControl(FC_SUB_TYPE3);
	int bit1 = this->getBitFrameControl(FC_SUB_TYPE2);
	int bit0 = this->getBitFrameControl(FC_SUB_TYPE1);
	return bit3*8+bit2*4+bit1*2+bit0;
}

void IeeeHeader::delSubType() {
	this -> setSubType(D_SUB_TYPE);
}

void IeeeHeader::setToDS(uint8_t toDS) {
	if(toDS == 0 )
		this -> setBitFrameControl(FC_TO_DS, 0); 
	else
		this -> setBitFrameControl(FC_TO_DS, 1); 
}

uint8_t IeeeHeader::getToDS() {
	return this->getBitFrameControl(FC_TO_DS);
}

void IeeeHeader::delToDS() {
	this -> setToDS(D_TO_DS);
}

void IeeeHeader::setFromDS(uint8_t fromDS) {
	if(fromDS == 0 )
		this -> setBitFrameControl(FC_FROM_DS, 0); 
	else
		this -> setBitFrameControl(FC_FROM_DS, 1); 
}

uint8_t IeeeHeader::getFromDS() {
	return this->getBitFrameControl(FC_FROM_DS);
}

void IeeeHeader::delFromDS() {
	this -> setFromDS(D_FROM_DS);
}

void IeeeHeader::setMoreFrag(uint8_t moreFrag) {
	if(moreFrag == 0 )
		this -> setBitFrameControl(FC_MORE_FRAG, 0); 
	else
		this -> setBitFrameControl(FC_MORE_FRAG, 1); 
}

uint8_t IeeeHeader::getMoreFrag() {
	return this->getBitFrameControl(FC_MORE_FRAG);
}
void IeeeHeader::delMoreFrag() {
	this -> setMoreFrag(D_MORE_FRAG);
}

void IeeeHeader::setRetry(uint8_t retry) {
	if(retry == 0 )
		this -> setBitFrameControl(FC_RETRY, 0); 
	else
		this -> setBitFrameControl(FC_RETRY, 1); 
}

uint8_t IeeeHeader::getRetry() {
	return this->getBitFrameControl(FC_RETRY);
}

void IeeeHeader::delRetry() {
	this -> setRetry(D_RETRY);
}

void IeeeHeader::setPwrMgmt(uint8_t pwrMgmt) {
	if(pwrMgmt == 0 )
		this -> setBitFrameControl(FC_PWR_MGMT, 0); 
	else
		this -> setBitFrameControl(FC_PWR_MGMT, 1); 
}

uint8_t IeeeHeader::getPwrMgmt() {
	return this->getBitFrameControl(FC_PWR_MGMT);
}

void IeeeHeader::delPwrMgmt() {
	this -> setPwrMgmt(D_PWR_MGMT);
}

void IeeeHeader::setMoreData(uint8_t moreData) {
	if(moreData == 0 )
		this -> setBitFrameControl(FC_MORE_DATA, 0); 
	else
		this -> setBitFrameControl(FC_MORE_DATA, 1); 
}

uint8_t IeeeHeader::getMoreData() {
	return this->getBitFrameControl(FC_MORE_DATA);
}

void IeeeHeader::delMoreData() {
	this -> setMoreData(D_MORE_DATA);
}

void IeeeHeader::setProtectedFrame(uint8_t protectedFrame) {
	if(protectedFrame == 0 )
		this -> setBitFrameControl(FC_PROTECTED_FRAME, 0); 
	else
		this -> setBitFrameControl(FC_PROTECTED_FRAME, 1); 
}

uint8_t IeeeHeader::getProtectedFrame() {
	return this->getBitFrameControl(FC_PROTECTED_FRAME);
}

void IeeeHeader::delProtectedFrame() {
	this -> setProtectedFrame(D_PROTECTED_FRAME);
}

void IeeeHeader::setOrder(uint8_t order) {
	if(order == 0 )
		this -> setBitFrameControl(FC_ORDER, 0); 
	else
		this -> setBitFrameControl(FC_ORDER, 1); 
}

uint8_t IeeeHeader::getOrder() {
	return this->getBitFrameControl(FC_ORDER);
}

void IeeeHeader::delOrder() {
	this -> setOrder(D_ORDER);
}

/* IeeeHeader::setDuration
 * Popis: nastavi hodnotu pole duration
 * Parametry: duration - nastavovana hodnota
 * Navratova hodnota: -
 */
void IeeeHeader::setDuration(uint16_t duration) {
	this -> duration = duration;
}

/* IeeeHeader::getDuration
 * Popis: vrati hodnotu pole duration
 * Parametry: -
 * Navratova hodnota: hodnota duration
 */
uint16_t IeeeHeader::getDuration() {
	return this -> duration;
}

/* IeeeHeader::delDuration
 * Popis: nastavi hodnotu duration na default hodnotu
 * Parametry: -
 * Navratova hodnota: -
 */
void IeeeHeader::delDuration() {
	this -> duration = D_DURATION;
}

/* IeeeHeader::setReceiveAddress
 * Popis: nastavi hodnotu pole adresy receiverAddress - odpovida addr1
 * Parametry: receiverAddress - nastavovana hodnota
 * Navratova hodnota: -
 */
void IeeeHeader::setReceiverAddress(char * receiverAddress) {
	strcpy(this -> addr1s, receiverAddress);
	this -> addr1 = this -> stringToMac(receiverAddress);
}

/* IeeeHeader::getReceiverAddress
 * Popis: vrati hodnotu adresy receiverAddress - odpovida addr1
 * Parametry: -
 * Navratova hodnota: hodnota receiverAddress
 */
char * IeeeHeader::getReceiverAddress() {
	return this -> addr1s;
}

/* IeeeHeader::delReceiverAddress
 * Popis: nastavi hodnotu receiverAddress na default hodnotu - odpovida addr1
 * Parametry: -
 * Navratova hodnota: -
 */
void IeeeHeader::delReceiverAddress() {
	this -> addr1 = D_ADDR1;
	strcpy(this -> addr1s, D_ADDR1S);
}

/* IeeeHeader::setTransmitterAddress
 * Popis: nastavi hodnotu pole adresy transmitterAddress - odpovida addr2
 * Parametry: transmitterAddress - nastavovana hodnota
 * Navratova hodnota: -
 */
void IeeeHeader::setTransmitterAddress(char * transmitterAddress) {
	strcpy(this -> addr2s, transmitterAddress);
	this -> addr2 = this -> stringToMac(transmitterAddress);
}

/* IeeeHeader::getTransmitterAddress
 * Popis: vrati hodnotu adresy transmitterAddress - odpovida addr2
 * Parametry: -
 * Navratova hodnota: hodnota transmitterAddress
 */
char * IeeeHeader::getTransmitterAddress() {
	return this -> addr2s;
}

/* IeeeHeader::delTransmitterAddress
 * Popis: nastavi hodnotu transmitterAddress na default hodnotu - odpovida addr2
 * Parametry: -
 * Navratova hodnota: -
 */
void IeeeHeader::delTransmitterAddress() {
	this -> addr2 = D_ADDR2;
	strcpy(this -> addr2s, D_ADDR2S);
}

/* IeeeHeader::setAid
 * Popis: nastavi hodnotu pole aid
 * Parametry: aid - nastavovana hodnota
 * Navratova hodnota: -
 */
void IeeeHeader::setAid(uint16_t aid) {
	this -> aid = aid;
}

/* IeeeHeader::getAid
 * Popis: vrati hodnotu aid
 * Parametry: -
 * Navratova hodnota: hodnota aid
 */
uint16_t IeeeHeader::getAid() {
	return this -> aid;
}

/* IeeeHeader::delAid
 * Popis: nastavi hodnotu aid na default hodnotu
 * Parametry: -
 * Navratova hodnota: -
 */
void IeeeHeader::delAid() {
	this -> aid = D_AID;
}

/* IeeeHeader::setBSSID
 * Popis: nastavi hodnotu pole bssid
 * Parametry: bssid - nastavovana hodnota
 * Navratova hodnota: -
 */
void IeeeHeader::setBSSID(char * bssid) {
	strcpy(this -> addr1s, bssid);
	this -> addr1 = this -> stringToMac(bssid);
}

/* IeeeHeader::getBSSID
 * Popis: vrati hodnotu bssid
 * Parametry: -
 * Navratova hodnota: hodnota bssid
 */
char * IeeeHeader::getBSSID() {
	return this -> addr1s;
}

/* IeeeHeader::delBSSID
 * Popis: nastavi hodnotu bssid na default hodnotu
 * Parametry: -
 * Navratova hodnota: -
 */
void IeeeHeader::delBSSID() {
	this -> addr1 = D_ADDR1;
	strcpy(this -> addr1s, D_ADDR1S);
}	

/* Setry a getry pro jednotliva pole hlavicek
 */
void IeeeHeader::setAddr1(char * addr) {
	strcpy(this -> addr1s, addr);
	this -> addr1 = this -> stringToMac(addr);
}

void IeeeHeader::setAddr2(char * addr) {
	strcpy(this -> addr2s, addr);
	this -> addr2 = this -> stringToMac(addr);
}

void IeeeHeader::setAddr3(char * addr) {
	strcpy(this -> addr3s, addr);
	this -> addr3 = this -> stringToMac(addr);
}

void IeeeHeader::setAddr4(char * addr){
	strcpy(this -> addr4s, addr);
	this -> addr4 = this -> stringToMac(addr);
}

void IeeeHeader::setDA(char * da) {
	strcpy(this -> addr2s, da);
	this -> addr2 = this -> stringToMac(da);
}
char * IeeeHeader::getDA() {
	return this -> addr2s;
}
void IeeeHeader::delDA() {
	this -> addr2 = D_ADDR2;
	strcpy(this -> addr2s, D_ADDR2S);
}

void IeeeHeader::setSA(char * sa) {
	strcpy(this -> addr3s, sa);
	this -> addr3 = this -> stringToMac(sa);
}
char * IeeeHeader::getSA() {
	return this -> addr3s;
}
void IeeeHeader::delSA() {
	this -> addr3 = D_ADDR3;
	strcpy(this -> addr3s, D_ADDR3S);
}

void IeeeHeader::setSeqCtl(uint16_t seqCtl) {
	this -> seqCtl = seqCtl;
}
uint16_t IeeeHeader::getSeqCtl() {
	return this -> seqCtl;
}
void IeeeHeader::delSeqCtl() {
	this -> seqCtl = D_SEQ_CTL;
}

void IeeeHeader::setSequence(uint16_t sequence) {
	this -> seqCtl = ((0x0FFF & sequence) << 4) | (this -> seqCtl & 0x000F);
}

uint16_t IeeeHeader::getSequence() {
	return ((this -> seqCtl >> 4) & 0x0FFF);
}

void IeeeHeader::delSequence() {
	this -> seqCtl = ((0x0FFF & D_SEQ_CTL) << 4) | (this -> seqCtl & 0x000F);
}

void IeeeHeader::setFragment(uint16_t fragment) {
	this -> seqCtl = ((0x000F & fragment) | (this -> seqCtl & 0xFFF0));
}

uint16_t IeeeHeader::getFragment() {
	return (this ->seqCtl & 0x000F);
}

void IeeeHeader::delFragment() {
	this -> seqCtl = ((0x000F & D_SEQ_CTL) | (this -> seqCtl & 0xFFF0));
}

void IeeeHeader::setTimestamp(uint64_t timestamp) {
	this -> timestamp = timestamp;
}

uint64_t IeeeHeader::getTimestamp() {
	return this -> timestamp;
}

void IeeeHeader::delTimestamp() {
	this -> timestamp = D_TIMESTAMP;
}

void IeeeHeader::setBeaconInterval(uint16_t beaconInterval) {
	this -> beaconInterval = beaconInterval;
}

uint16_t IeeeHeader::getBeaconInterval() {
	return this -> beaconInterval;
}

void IeeeHeader::delBeaconInterval() {
	this -> beaconInterval = D_BEACON_INTERVAL;
}

void IeeeHeader::setCapabilityInfo(uint16_t capabilityInfo) {
	this -> capabilityInfo = capabilityInfo;
}

void IeeeHeader::setCapabilityInfoKeyword(Keyword keywordInfo) {
	//include seznam
	this -> capabilityInfo = this -> capabilityInfo | keywordInfo.include;
	//exclude seznam
	this -> capabilityInfo = this -> capabilityInfo & (~keywordInfo.exclude);
}

uint16_t IeeeHeader::getCapabilityInfo() {
	return this -> capabilityInfo;
}

void IeeeHeader::delCapabilityInfo() {
	this -> capabilityInfo = D_CAPABILITY_INFO;
}

void IeeeHeader::setBitCapabilityInfo(uint8_t numberBit, uint8_t valueBit) {
	//hodnota nastavovaneho bitu je 0
	if(valueBit == 0) {
		this -> capabilityInfo &= (~(uint32_t)pow(2, numberBit));
	} else {
	//hodnota nastavovaneho bitu je 1
		this -> capabilityInfo |= (uint32_t)pow(2, numberBit);
	}	
}

uint8_t IeeeHeader::getBitCapabilityInfo(uint8_t numberBit) {
	if((this -> capabilityInfo & (uint32_t)pow(2,numberBit)) != 0) {
		return 1;
	} else {
		return 0;
	}
}

void IeeeHeader::setSSID(char * ssid) {
	int length = strlen(ssid);
	ssid[length] = 0;
	if(length > L_SSID_MAX_LENGTH)
		length = L_SSID_MAX_LENGTH;
	strncpy(this -> ssid, ssid, L_SSID_MAX_LENGTH);
	this -> ssid[length] = 0;
}

char * IeeeHeader::getSSID() {
	return this -> ssid;
}

void IeeeHeader::delSSID() {
	strcpy(this -> ssid, D_SSID);
}

void IeeeHeader::setFHparameterSetDwellTime(uint16_t fhParameterSetDwellTime) {
	this -> fhParameterSetDwellTime = fhParameterSetDwellTime;
	this -> taggedParameters[TP_FH_PARAMETER_SET] = true;
}

uint16_t IeeeHeader::getFHparameterSetDwellTime() {
	return this -> fhParameterSetDwellTime;
}

void IeeeHeader::delFHparameterSetDwellTime() {
	this -> fhParameterSetDwellTime = D_FH_PARAMETER_SET_DWELL_TIME;
}

void IeeeHeader::setFHparameterSetHopSet(uint8_t fhParameterSetHopSet) {
	this -> fhParameterSetHopSet = fhParameterSetHopSet;
	this -> taggedParameters[TP_FH_PARAMETER_SET] = true;
}

uint8_t IeeeHeader::getFHparameterSetHopSet() {
	return this -> fhParameterSetHopSet;
}

void IeeeHeader::delFHparameterSetHopSet() {
	this -> fhParameterSetHopSet = D_FH_PARAMETER_SET_HOP_SET;
}

void IeeeHeader::setFHparameterSetHopPattern(uint8_t fhParameterSetHopPattern) {
	this -> fhParameterSetHopPattern = fhParameterSetHopPattern;
}

uint8_t IeeeHeader::getFHparameterSetHopPattern() {
	return this -> fhParameterSetHopPattern;
}

void IeeeHeader::delFHparameterSetHopPattern() {
	this -> fhParameterSetHopPattern = D_FH_PARAMETER_SET_HOP_PATTERN;
}

void IeeeHeader::setFHparameterSetHopIndex(uint8_t fhParameterSetHopIndex) {
	this -> fhParameterSetHopIndex = fhParameterSetHopIndex;
}

uint8_t IeeeHeader::getFHparameterSetHopIndex() {
	return this -> fhParameterSetHopIndex;
}

void IeeeHeader::delFHparameterSetHopIndex() {
	this -> fhParameterSetHopIndex = D_FH_PARAMETER_SET_HOP_INDEX;
}

void IeeeHeader::delFHparameterSet() {
	this -> delFHparameterSetDwellTime();
	this -> delFHparameterSetHopSet();
	this -> delFHparameterSetHopPattern();
	this -> delFHparameterSetHopIndex();
	this -> taggedParameters[TP_FH_PARAMETER_SET] = false;
}

void IeeeHeader::setDSparameterSet(uint8_t dsParameterSet) {
	this -> dsParameterSet = dsParameterSet;
	this -> taggedParameters[TP_DS_PARAMETER_SET] = true;
}

uint8_t IeeeHeader::getDSparameterSet() {
	return this -> dsParameterSet;
}

void IeeeHeader::delDSparameterSet() {
	this -> dsParameterSet = D_DS_PARAMETER_SET;
	this -> taggedParameters[TP_DS_PARAMETER_SET] = false;
}

void IeeeHeader::setIBSSparameterSet(uint16_t ibssParameterSet) {
	this -> ibssParameterSet = ibssParameterSet;
	this -> taggedParameters[TP_IBSS_PARAMETER_SET] = true;
}

uint16_t IeeeHeader::getIBSSparameterSet() {
	return this -> ibssParameterSet;
}

void IeeeHeader::delIBSSparameterSet() {
	this -> ibssParameterSet = D_IBSS_PARAMETER_SET;
	this -> taggedParameters[TP_IBSS_PARAMETER_SET] = false;
}

void IeeeHeader::setTIMdTimCount(uint8_t timDtimCount) {
	this -> timDtimCount = timDtimCount;
	this -> taggedParameters[TP_TRAFFIC_INDICATION_MAP] = true;
}

uint8_t IeeeHeader::getTIMdTimCount() {
	return this -> timDtimCount;
}

void IeeeHeader::delTIMdTimCount() {
	this -> timDtimCount = D_TIM_DTIM_COUNT;
}

void IeeeHeader::setTIMdTimPeriod(uint8_t timDtimPeriod) {
	this -> timDtimPeriod = timDtimPeriod;
	this -> taggedParameters[TP_TRAFFIC_INDICATION_MAP] = true;
}

uint8_t IeeeHeader::getTIMdTimPeriod() {
	return this -> timDtimPeriod;
}

void IeeeHeader::delTIMdTimPeriod() {
	this -> timDtimPeriod = D_TIM_DTIM_PERIOD;
}

void IeeeHeader::setTIMbitmapControl(uint8_t timBitmapControl) {
	this -> timBitmapControl = timBitmapControl;
	this -> taggedParameters[TP_TRAFFIC_INDICATION_MAP] = true;
}

uint8_t IeeeHeader::getTIMbitmapControl() {
	return this -> timBitmapControl;
}

void IeeeHeader::delTIMbitmapControl() {
	this -> timBitmapControl = D_TIM_BITMAP_CONTROL;
}

void IeeeHeader::setTIMpartialVirtualBitmap(char * timPartialVirtualBitmap) {
	timPartialVirtualBitmap = &timPartialVirtualBitmap[2];
	int length = strlen(timPartialVirtualBitmap);
	timPartialVirtualBitmap[length] = 0;
	if(length > L_TIM_PARTIAL_VIRTUAL_BITMAP_LENGTH)
		length = L_TIM_PARTIAL_VIRTUAL_BITMAP_LENGTH;
	strncpy(this -> timPartialVirtualBitmap, timPartialVirtualBitmap, length);
	this -> timPartialVirtualBitmap[length] = 0;	
	this -> taggedParameters[TP_TRAFFIC_INDICATION_MAP] = true;
}
char * IeeeHeader::getTIMpartialVirtualBitmap() {
	return this -> timPartialVirtualBitmap;
}

void IeeeHeader::delTIMpartialVirtualBitmap() {
	strcpy(this -> timPartialVirtualBitmap, D_TIM_PARTICIAL_VIRTUAL_BITMAP);
}

void IeeeHeader::delTIM() {
	this -> delTIMdTimCount();
	this -> delTIMdTimPeriod();
	this -> delTIMbitmapControl();
	this -> delTIMpartialVirtualBitmap();
	this -> taggedParameters[TP_TRAFFIC_INDICATION_MAP] = false;
}

void IeeeHeader::setLocalPowerConstraint(uint8_t localPowerConstraint) {
	this -> localPowerConstraint = localPowerConstraint;
	this -> taggedParameters[TP_POWER_CONSTRAINT] = true;
}

uint8_t IeeeHeader::getLocalPowerConstraint() {
	return this -> localPowerConstraint;
}

void IeeeHeader::delLocalPowerConstraint() {
	this -> localPowerConstraint = D_LOCAL_POWER_CONSTRAINT;
	this -> taggedParameters[TP_POWER_CONSTRAINT] = false;
}

void IeeeHeader::setCountryString(char * countryString) {
	strncpy(this -> countryString, countryString, L_COUNTRY_STRING_LENGTH);
	this -> countryString[L_COUNTRY_STRING_LENGTH] = 0;
	this -> taggedParameters[TP_COUNTRY] = true;
}

char * IeeeHeader::getCountryString() {
	return this -> countryString;
}

void IeeeHeader::delCountryString() {
	strcpy(this -> countryString, D_COUNTRY_STRING);
}


void IeeeHeader::addCountryConstraintTriplet(uint8_t index, ciect triplet) {
	if(index < L_COUNTRY_IE_LENGTH) {
		this -> countryConstraintTriplet[index] = triplet;
		this -> countryConstraintTripletFlag[index] = true;
	}
	this -> taggedParameters[TP_COUNTRY] = true;
}

ciect IeeeHeader::getCountryConstraint(uint8_t index) {
	if(index < L_COUNTRY_IE_LENGTH && this -> countryConstraintTripletFlag[index] == true) {
		return this -> countryConstraintTriplet[index];
	}
	else {
		ciect d;
		d.firstChannelNumber = D_FIRST_CHANNEL_NUMBER;
		d.numberOfChannel = D_NUMBER_OF_CHANNEL;
		d.maxTransmitPower = D_MAX_TRANSMIT_POWER;
		return d;
	}
}

void IeeeHeader::delCountryConstraintTirplet(uint8_t index) {
	if(index < L_COUNTRY_IE_LENGTH && this -> countryConstraintTripletFlag[index] == true) {
		ciect d;
		d.firstChannelNumber = D_FIRST_CHANNEL_NUMBER;
		d.numberOfChannel = D_NUMBER_OF_CHANNEL;
		d.maxTransmitPower = D_MAX_TRANSMIT_POWER;
		this -> countryConstraintTripletFlag[index] = false;
		this -> countryConstraintTriplet[index] = d;
	}
}

void IeeeHeader::delCountry() {
	this -> delCountryString();
	for(int i = 0; i < L_COUNTRY_IE_LENGTH; i++) {
		this -> countryConstraintTripletFlag[i]=true;
		this -> delCountryConstraintTirplet(i);
	}
	this -> taggedParameters[TP_COUNTRY] = false;
}

void IeeeHeader::setChannelSwitchMode(uint8_t channelSwitchMode) {
	this -> channelSwitchMode = channelSwitchMode;
	this -> taggedParameters[TP_CHANNEL_SWITCH] = true;
}

uint8_t IeeeHeader::getChannelSwitchMode() {
	return this -> channelSwitchMode;
}

void IeeeHeader::delChannelSwitchMode() {
	this -> channelSwitchMode = D_CHANNEL_SWITCH_MODE;
}

void IeeeHeader::setNewChannelNumber(uint8_t newChannelNumber) {
	this -> newChannelNumber = newChannelNumber;
	this -> taggedParameters[TP_CHANNEL_SWITCH] = true;
}

uint8_t IeeeHeader::getNewChannelNumber() {
	return this -> newChannelNumber;
}

void IeeeHeader::delNewChannelNumber() {
	this -> newChannelNumber = D_NEW_CHANNEL_NUMBER;
}

void IeeeHeader::setChannelSwitchCount(uint8_t channelSwitchCount) {
	this -> channelSwitchCount = channelSwitchCount;
	this -> taggedParameters[TP_CHANNEL_SWITCH] = true;
}

uint8_t IeeeHeader::getChannelSwitchCount() {
	return this -> channelSwitchCount;
}

void IeeeHeader::delChannelSwitchCount() {
	this -> channelSwitchCount = D_CHANNEL_SWITCH_COUNT;
}

void IeeeHeader::delChannelSwitch() {
	this -> delChannelSwitchMode();
	this -> delNewChannelNumber();
	this -> delChannelSwitchCount();
	this -> taggedParameters[TP_CHANNEL_SWITCH] = false;
}

void IeeeHeader::setQuietCount(uint8_t quietCount) {
	this -> quietCount = quietCount;
	this -> taggedParameters[TP_QUIET] = true;
}

uint8_t IeeeHeader::getQuietCount() {
	return this -> quietCount;
}

void IeeeHeader::delQuietCount() {
	this -> quietCount = D_QUIET_COUNT;
}

void IeeeHeader::setQuietPeriod(uint8_t quietPeriod) {	
	this -> quietPeriod = quietPeriod;
	this -> taggedParameters[TP_QUIET] = true;
}

uint8_t IeeeHeader::getQuietPeriod() {
	return this -> quietPeriod;
}

void IeeeHeader::delQuietPeriod() {
	this -> quietPeriod = D_QUIET_PERIOD;
}

void IeeeHeader::setQuietDuration(uint8_t quietDuration) {
	this -> quietDuration = quietDuration;
	this -> taggedParameters[TP_QUIET] = true;
}

uint8_t IeeeHeader::getQuietDuration() {
	return this -> quietDuration;
}

void IeeeHeader::delQuietDuration() {
	this -> quietDuration = D_QUIET_DURATION;
}

void IeeeHeader::setQuietOffset(uint8_t quietOffset) {
	this -> quietOffset = quietOffset;
	this -> taggedParameters[TP_QUIET] = true;
}

uint8_t IeeeHeader::getQuietOffset() {
	return this -> quietOffset;
}

void IeeeHeader::delQuietOffset() {
	this -> quietOffset = D_QUIET_OFFSET;
}

void IeeeHeader::delQuiet() {
	this -> delQuietCount();
	this -> delQuietPeriod();
	this -> delQuietDuration();
	this -> delQuietOffset();
	this -> taggedParameters[TP_QUIET] = false;
}

void IeeeHeader::setTransmitPower(uint8_t transmitPower) {
	this -> transmitPower = transmitPower;
	this -> taggedParameters[TP_TPC_REPORT] = true;
}

uint8_t IeeeHeader::getTransmitPower() {
	return this -> transmitPower;
}

void IeeeHeader::delTransmitPower() {
	this -> transmitPower = D_TRANSMIT_POWER;
}
		
void IeeeHeader::setLinkMargin(uint8_t linkMargin) {
	this -> linkMargin = linkMargin;
	this -> taggedParameters[TP_TPC_REPORT] = true;
}

uint8_t IeeeHeader::getLinkMargin() {
	return this -> linkMargin;
}

void IeeeHeader::delLinkMargin() {
	this -> linkMargin = D_LINK_MARGIN;
}

void IeeeHeader::delTPCReport() {
	this -> delTransmitPower();
	this -> delLinkMargin();
	this -> taggedParameters[TP_TPC_REPORT] = false;
}

void IeeeHeader::setERP(uint8_t erp) {
	this -> erp = erp;
	this -> taggedParameters[TP_ERP_INFORMATION] = true;
}

void IeeeHeader::setERPKeyword(Keyword keywordInfo) {
	//include seznam
	this -> erp = this -> erp | keywordInfo.include;
	//exclude seznam
	this -> erp = this -> erp & (~keywordInfo.exclude);

	this -> taggedParameters[TP_ERP_INFORMATION] = true;
}

uint8_t IeeeHeader::getERP() {
	return this -> erp;
}

void IeeeHeader::delERP() {
	this -> erp = D_ERP;
	this -> taggedParameters[TP_ERP_INFORMATION] = false;
}

void IeeeHeader::setBitERP(uint8_t numberBit, uint8_t valueBit) {
	//hodnota nastavovaneho bitu je 0
	if(valueBit == 0) {
		this -> erp &= (~(uint32_t)pow(2, numberBit));
	} else {
	//hodnota nastavovaneho bitu je 1
		this -> erp |= (uint32_t)pow(2, numberBit);
	}
}

uint8_t IeeeHeader::getBitERP(uint8_t numberBit) {
	if((this -> erp & (uint32_t)pow(2,numberBit)) != 0) {
		return 1;
	} else {
		return 0;
	}
}

void IeeeHeader::addSupportedRate(uint8_t supportedRate, uint8_t index) {
	this -> taggedParameters [TP_SUPPORTED_RATE] = true;
	if(index < L_SUPPORTED_RATE_LENGTH) {
		this -> supportedRateFlag[index] = true;
		this -> supportedRate[index] = supportedRate;
	}
}

uint8_t IeeeHeader::getSupportedRate(uint8_t index) {
	if(index < L_SUPPORTED_RATE_LENGTH && this -> supportedRateFlag[index]) {
		return this -> supportedRate[index];
	}
	return D_SUPPORTED_RATE;	
}

void IeeeHeader::delSupportedRate(uint8_t index) {
	if(index < L_SUPPORTED_RATE_LENGTH) {
		this -> supportedRateFlag[index] = false;
		this -> supportedRate[index] = D_SUPPORTED_RATE;
	}
}

void IeeeHeader::delSupportedRateAll() {
	this -> taggedParameters [TP_SUPPORTED_RATE] = false;
	for(int i = 0; i < L_SUPPORTED_RATE_LENGTH; i++) {
		this -> delSupportedRate(i);
	}
}

void IeeeHeader::addExtendedSupportedRate(uint8_t extendedSupportedRate, uint8_t index) {
	this -> taggedParameters [TP_EXTENDED_SUPPORTED_RATE] = true;
	if(index < L_EXTENDED_SUPPORTED_RATE_LENGTH) {
		this -> extendedSupportedRateFlag[index] = true;
		this -> extendedSupportedRate[index] = extendedSupportedRate;
	}
}

uint8_t IeeeHeader::getExtendedSupportedRate(uint8_t index) {
	if(index < L_EXTENDED_SUPPORTED_RATE_LENGTH && this -> extendedSupportedRateFlag[index]) {
		return this -> extendedSupportedRate[index];
	}
	return D_EXTENDED_SUPPORTED_RATE;	
}

void IeeeHeader::delExtendedSupportedRate(uint8_t index) {
	if(index < L_EXTENDED_SUPPORTED_RATE_LENGTH) {
		this -> extendedSupportedRateFlag[index] = false;
		this -> extendedSupportedRate[index] = D_EXTENDED_SUPPORTED_RATE;
	}
}

void IeeeHeader::delExtendedSupportedRateAll() {
	this -> taggedParameters [TP_EXTENDED_SUPPORTED_RATE] = false;
	for(int i = 0; i < L_EXTENDED_SUPPORTED_RATE_LENGTH; i++) {
		this -> delExtendedSupportedRate(i);
	}
}

void IeeeHeader::setReasonCode(uint8_t reasonCode) {
	this -> reasonCode = reasonCode;
}

uint8_t IeeeHeader::getReasonCode() {
	return this -> reasonCode;
}

void IeeeHeader::delReasonCode() {
	this -> reasonCode = D_REASON_CODE;
}

void IeeeHeader::setListenInterval(uint8_t listenInterval) {
	this -> listenInterval = listenInterval;
}

uint8_t IeeeHeader::getListenInterval() {
	return this -> listenInterval;
}

void IeeeHeader::delListenInterval() {
	this -> listenInterval = D_LISTEN_INTERVAL;
}

void IeeeHeader::setCurrentAPaddress(char * currentAPaddress) {
	strcpy(this -> addr4s, currentAPaddress);
	this -> addr4 = this -> stringToMac(currentAPaddress);
}

char * IeeeHeader::getCurrentAPaddress() {
	return this -> addr4s;
}

void IeeeHeader::delCurrentAPaddress() {
	strcpy(this -> addr4s, D_ADDR4S);
}

void IeeeHeader::setCFPcount(uint8_t cfpCount) {
	this -> taggedParameters[TP_CF_PARAMETER_SET] = true;
	this -> cfpCount = cfpCount;
}

uint8_t IeeeHeader::getCFPcount() {
	return this -> cfpCount;
}

void IeeeHeader::delCFPcount() {
	this -> cfpCount = D_CFP_COUNT;
}

void IeeeHeader::setCFPperiod(uint8_t cfpPeriod) {
	this -> taggedParameters[TP_CF_PARAMETER_SET] = true;
	this -> cfpPeriod = cfpPeriod;
}

uint8_t IeeeHeader::getCFPperiod() {
	return this -> cfpPeriod;
}

void IeeeHeader::delCFPperiod() {
	this -> cfpPeriod = D_CFP_PERIOD;
}

void IeeeHeader::setCFPmaxDuration(uint16_t cfpMaxDuration) {
	this -> taggedParameters[TP_CF_PARAMETER_SET] = true;
	this -> cfpMaxDuration = cfpMaxDuration;
}

uint16_t IeeeHeader::getCFPmaxDuration() {
	return this -> cfpMaxDuration;
}

void IeeeHeader::delCFPmaxDuration() {
	this -> cfpMaxDuration = D_CFP_MAX_DURATION;
}

void IeeeHeader::setCFPremaining(uint16_t cfpRemaining) {
	this -> taggedParameters[TP_CF_PARAMETER_SET] = true;
	this -> cfpRemaining = cfpRemaining;
}

uint16_t IeeeHeader::getCFPremaining() {
	return this -> cfpRemaining;
}

void IeeeHeader::delCFPremaining() {
	this -> cfpRemaining = D_CFP_REMAINING;
}

void IeeeHeader::delCFP() {
	this -> delCFPcount();
	this -> delCFPperiod();
	this -> delCFPmaxDuration();
	this -> delCFPremaining();
	this -> taggedParameters[TP_CF_PARAMETER_SET] = false;
}

void IeeeHeader::setRSNversion(uint16_t rsnVersion) {
	this -> taggedParameters[TP_RSN] = true;
	this -> rsnVersion = rsnVersion;
}

uint16_t IeeeHeader::getRSNversion() {
	return this -> rsnVersion;
}

void IeeeHeader::delRSNversion() {
	this -> rsnVersion = D_RSN_VERSION;
}

void IeeeHeader::setGroupCipherSuite(suite groupCipherSuite) {
	this -> taggedParameters[TP_RSN] = true;
	this -> groupCipherSuite = groupCipherSuite;
}

suite IeeeHeader::getGroupCipherSuite() {
	return this -> groupCipherSuite;
}

void IeeeHeader::delGroupCipherSuite() {
	this -> groupCipherSuite.suiteOUI[0] = D_GROUP_CIPHER_SUITE_OUI_0;
	this -> groupCipherSuite.suiteOUI[1] = D_GROUP_CIPHER_SUITE_OUI_1;
	this -> groupCipherSuite.suiteOUI[2] = D_GROUP_CIPHER_SUITE_OUI_2;
	this -> groupCipherSuite.suiteType = D_GROUP_CIPHER_SUITE_TYPE;
}

void IeeeHeader::addPairwiseCipherSuite(suite pairwiseCipherSuite, int index) {
	this -> taggedParameters[TP_RSN] = true;
	if(index < L_PAIRWISE_CIPHER_SUITE_COUNT) {
		this -> pairwiseCipherSuiteItem [index] = true;
		this -> pairwiseCipherSuite[index] = pairwiseCipherSuite;
	}
}

suite IeeeHeader::getPairwiseCipherSuite(int index) {
	if(index < L_PAIRWISE_CIPHER_SUITE_COUNT && this -> pairwiseCipherSuiteItem [index]) {
		return this -> pairwiseCipherSuite[index];
	}
	suite s;
	s.suiteOUI[0] = D_GROUP_CIPHER_SUITE_OUI_0;
	s.suiteOUI[1] = D_GROUP_CIPHER_SUITE_OUI_1;
	s.suiteOUI[2] = D_GROUP_CIPHER_SUITE_OUI_2;
	s.suiteType = D_GROUP_CIPHER_SUITE_TYPE;
	return s;
}

void IeeeHeader::delPairwiseCipherSuite(int index) {
	if(index < L_PAIRWISE_CIPHER_SUITE_COUNT) {
		this -> pairwiseCipherSuiteItem[index] = false;
		this -> pairwiseCipherSuite[index].suiteOUI[0] = D_GROUP_CIPHER_SUITE_OUI_0;
		this -> pairwiseCipherSuite[index].suiteOUI[1] = D_GROUP_CIPHER_SUITE_OUI_1;
		this -> pairwiseCipherSuite[index].suiteOUI[2] = D_GROUP_CIPHER_SUITE_OUI_2;
		this -> pairwiseCipherSuite[index].suiteType = D_GROUP_CIPHER_SUITE_TYPE;
	}
}

void IeeeHeader::delPairwiseCipherSuiteAll() {
	for(int i = 0; i < L_PAIRWISE_CIPHER_SUITE_COUNT; i++) {
		this -> pairwiseCipherSuiteItem[i] = false;
		this -> delPairwiseCipherSuite(i);	
	}
}

void IeeeHeader::IeeeHeader::addAuthenticationSuite(suite authenticationSuite, int index) {
	this -> taggedParameters[TP_RSN] = true;
	if(index < L_AUTHENTICATION_SUITE_COUNT) {
		this -> authenticationSuiteItem [index] = true;
		this -> authenticationSuite[index] = authenticationSuite;
	}
}

suite IeeeHeader::getAuthenticationSuite(int index) {
	if(index < L_AUTHENTICATION_SUITE_COUNT && this -> authenticationSuiteItem [index]) {
		return this -> authenticationSuite[index];
	}
	suite s;
	s.suiteOUI[0] = D_AUTHENTICATION_SUITE_OUI_0;
	s.suiteOUI[1] = D_AUTHENTICATION_SUITE_OUI_1;
	s.suiteOUI[2] = D_AUTHENTICATION_SUITE_OUI_2;
	s.suiteType = D_AUTHENTICATION_SUITE_TYPE;
	return s;
}

void IeeeHeader::delAuthenticationSuite(int index) {
	if(index < L_AUTHENTICATION_SUITE_COUNT) {
		this -> authenticationSuiteItem[index] = false;
		this -> authenticationSuite[index].suiteOUI[0] = D_AUTHENTICATION_SUITE_OUI_0;
		this -> authenticationSuite[index].suiteOUI[1] = D_AUTHENTICATION_SUITE_OUI_1;
		this -> authenticationSuite[index].suiteOUI[2] = D_AUTHENTICATION_SUITE_OUI_2;
		this -> authenticationSuite[index].suiteType = D_AUTHENTICATION_SUITE_TYPE;
	}
}

void IeeeHeader::delAuthenticationSuiteAll() {
	for(int i = 0; i < L_AUTHENTICATION_SUITE_COUNT; i++) {
		this -> authenticationSuiteItem[i] = false;
		this -> delAuthenticationSuite(i);	
	}
}
		
void IeeeHeader::setRSNcapabilities(uint16_t rsnCapabilities) {
	this -> taggedParameters[TP_RSN] = true;
	this -> rsnCapabilities = rsnCapabilities;
}

uint16_t IeeeHeader::getRSNcapabilities() {
	return this -> rsnCapabilities;
}

void IeeeHeader::delRSNcapabilities() {
	this -> rsnCapabilities = D_RSN_CAPABILITIES;
}

void IeeeHeader::setRSNcapabilitiesKeyword(Keyword keywordInfo) {
	this -> taggedParameters[TP_RSN] = true;
	//include seznam
	this -> rsnCapabilities = this -> rsnCapabilities | keywordInfo.include;
	//exclude seznam
	this -> rsnCapabilities = this -> rsnCapabilities & (~keywordInfo.exclude);
}

void IeeeHeader::delRSN() {
	this -> delRSNversion();
	this -> delGroupCipherSuite();
	this -> delPairwiseCipherSuiteAll();
	this -> delAuthenticationSuiteAll();
	this -> delRSNcapabilities();
}

void IeeeHeader::setStatusCode(uint8_t statusCode) {
	this -> statusCode = statusCode;
}

uint8_t IeeeHeader::getStatusCode() {
	return this -> statusCode;
}

void IeeeHeader::delStatusCode() {
	this -> statusCode = D_STATUS_CODE;
}

void IeeeHeader::setAssociationId(uint8_t associationId) {
	this -> associationId = associationId;
}

uint8_t IeeeHeader::getAssociationId() {
	return this -> associationId;
}

void IeeeHeader::delAssociationId() {
	this -> associationId = D_ASSOCIATION_ID;
}

void IeeeHeader::setAuthenticationAlgorithmNumber(uint8_t authenticationAlgorithmNumber) {
	this -> authenticationAlgorithmNumber = authenticationAlgorithmNumber;
}

uint8_t IeeeHeader::getAuthenticationAlgorithmNumber() {
	return this -> authenticationAlgorithmNumber;
}

void IeeeHeader::delAuthenticationAlgorithmNumber() {
	this -> authenticationAlgorithmNumber = D_AUTHENTICATION_ALGORITHM_NUMBER;
}

void IeeeHeader::setAuthenticationSequenceNumber(uint8_t authenticationSequenceNumber) {
	this -> authenticationSequenceNumber = authenticationSequenceNumber;
}

uint8_t IeeeHeader::getAuthenticationSequenceNumber() {
	return this -> authenticationSequenceNumber;
}

void IeeeHeader::delAuthenticationSequenceNumber() {
	this -> authenticationSequenceNumber = D_AUTHENTICATION_SEQUENCE_NUMBER;
}

void IeeeHeader::setChallengeText(char * challengeText) {
	challengeText = &challengeText[2];
	challengeText[strlen(challengeText)] = 0;
	strncpy(this -> challengeText, challengeText, L_CHALLENGE_TEXT_LENGTH);
}

char * IeeeHeader::getChallengeText() {
	return this -> challengeText;
}

void IeeeHeader::delChallengeText() {
	strcpy(this -> challengeText, D_CHALLENGE_TEXT);
}

void IeeeHeader::setQos(uint16_t qos) {
	this -> qos = qos;
}

uint16_t IeeeHeader::getQos() {
	return this -> qos;
}

void IeeeHeader::delQos() {
	this -> qos = D_QOS;
}

void IeeeHeader::setAddress1(char * address1) {
	strcpy(this -> addr1s, address1);
	this -> addr1 = this -> stringToMac(address1);
}
char * IeeeHeader::getAddress1() {
	return this -> addr1s;
}
void IeeeHeader::delAddress1() {
	this -> addr1 = D_ADDR1;
	strcpy(this -> addr1s, D_ADDR1S);
}

void IeeeHeader::setAddress2(char * address2) {
	strcpy(this -> addr2s, address2);
	this -> addr2 = this -> stringToMac(address2);
}

char * IeeeHeader::getAddress2() {
	return this -> addr2s;
}

void IeeeHeader::delAddress2() {
	this -> addr2 = D_ADDR2;
	strcpy(this -> addr2s, D_ADDR2S);
}

void IeeeHeader::setAddress3(char * address3) {
	strcpy(this -> addr3s, address3);
	this -> addr3 = this -> stringToMac(address3);
}

char * IeeeHeader::getAddress3() {
	return this -> addr3s;
}

void IeeeHeader::delAddress3() {
	this -> addr3 = D_ADDR3;
	strcpy(this -> addr3s, D_ADDR3S);
}

void IeeeHeader::setAddress4(char * address4) {
	strcpy(this -> addr4s, address4);
	this -> addr4 = this -> stringToMac(address4);
}

char * IeeeHeader::getAddress4() {
	return this -> addr4s;
}

void IeeeHeader::delAddress4() {
	this -> addr4 = D_ADDR4;
	strcpy(this -> addr4s, D_ADDR4S);
}

/* IeeeHeader::generateHeader
 * Popis: funkce provede vyganerovani hlavicky ze zadanych udaji
 * Parametry: uint8_t * data - ukazatel do pameti, kam se hlavicka vygeneruje
 * Navratova hodnota: delka hlavicky
 */		
uint32_t IeeeHeader::generateHeader(uint8_t * data) {
	uint32_t lengthData = 0;
	
	//Povinna pole pro vsechny typy ramcu

	//Frame control
	Utilities::memcpy16(data, &this -> frameControl, 1, LITTLE_ENDIAN_ORDER);
	data += sizeof(uint16_t);

	lengthData = sizeof(uint16_t);

	//generovani ruznych typu ramcu
	switch(this -> getType()) {
		case T_CONTROL_FRAME:    {
			switch(this -> getSubType()) {
				case ST_RTS: {
					lengthData += this -> generateRTS(data);
				}; break;
				case ST_CTS: {
					lengthData += this -> generateCTS(data);
				}; break;
				case ST_ACK: {
					lengthData += this -> generateACK(data);
				}; break;
				case ST_PS_POLL: {
					lengthData += this -> generatePSPOLL(data);
				}; break;
			}
		}; break;
		case T_MANAGEMENT_FRAME: {
			switch(this -> getSubType()) {
				case ST_BEACON: {
					lengthData += this -> generateBEACON(data);
				}; break;
				case ST_PROBE_REQUEST: {
					lengthData += this -> generatePROBEREQUEST(data);
				}; break;
			        case ST_PROBE_RESPONSE: {
					lengthData += this -> generatePROBERESPONSE(data);
				}; break;
			        case ST_ATIM: {
					lengthData += this -> generateATIM(data);
				}; break;
			        case ST_DEAUTHETICATION: {
					lengthData += this -> generateDEAUTHETICATION(data);
				}; break;
			        case ST_DISASSOCIATION: {
					lengthData += this -> generateDISASSOCIATION(data);
				}; break;
			        case ST_ASSOCIATION_REQUEST: {
					lengthData += this -> generateASSOCIATIONREQUEST(data);
				}; break;
			        case ST_REASSOCIATION_REQUEST: {
					lengthData += this -> generateREASSOCIATIONREQUEST(data);
				}; break;
			        case ST_REASSOCIATION_RESPONSE: {
					lengthData += this -> generateREASSOCIATIONRESPONSE(data);
				}; break;
			        case ST_AUTHETICATION: {
					lengthData += this -> generateAUTHENTICATION(data);
				}; break;
			}
			
		}; break;
		case T_DATA_FRAME: 	 {
			lengthData += this -> generateDATA(data);
		}; break;
	}

	return lengthData;
}

//generovani jednotlivych polozek hlavicky
uint32_t IeeeHeader::generateDuration(uint8_t * data) {
	uint32_t lengthData = 0;
	Utilities::memcpy16(data, &this -> duration, 1, LITTLE_ENDIAN_ORDER);
	data += sizeof(uint16_t);
	lengthData += sizeof(uint16_t);
	return lengthData;
}

uint32_t IeeeHeader::generateReceiveAddress(uint8_t * data) {
	uint32_t lengthData = 0;
	Utilities::memcpy8(data, &this -> addr1, L_GEN_SIZE_ADDR, LITTLE_ENDIAN_ORDER);
	data += L_GEN_SIZE_ADDR;
	lengthData += L_GEN_SIZE_ADDR;
	return lengthData;
}

uint32_t IeeeHeader::generateTransmitterAddress(uint8_t * data) {
	uint32_t lengthData = 0;
	Utilities::memcpy8(data, &this -> addr2, L_GEN_SIZE_ADDR, LITTLE_ENDIAN_ORDER);
	data += L_GEN_SIZE_ADDR;
	lengthData += L_GEN_SIZE_ADDR;
	return lengthData;
}

uint32_t IeeeHeader::generateAID(uint8_t * data) {
	uint32_t lengthData = 0;
	Utilities::memcpy16(data, &this -> aid, 1, LITTLE_ENDIAN_ORDER);
	data += sizeof(uint16_t);
	lengthData += sizeof(uint16_t);
	return lengthData;
}

uint32_t IeeeHeader::generateBSSID(uint8_t * data) {
	uint32_t lengthData = 0;
	Utilities::memcpy8(data, &this -> addr1, L_GEN_SIZE_ADDR, LITTLE_ENDIAN_ORDER);
	data += L_GEN_SIZE_ADDR;
	lengthData += L_GEN_SIZE_ADDR;
	return lengthData;
}

uint32_t IeeeHeader::generateDA(uint8_t * data) {
	uint32_t lengthData = 0;
	Utilities::memcpy8(data, &this -> addr2, L_GEN_SIZE_ADDR, LITTLE_ENDIAN_ORDER);
	data += L_GEN_SIZE_ADDR;
	lengthData += L_GEN_SIZE_ADDR;
	return lengthData;
}

uint32_t IeeeHeader::generateSA(uint8_t * data) {
	uint32_t lengthData = 0;
	Utilities::memcpy8(data, &this -> addr3, L_GEN_SIZE_ADDR, LITTLE_ENDIAN_ORDER);
	data += L_GEN_SIZE_ADDR;
	lengthData += L_GEN_SIZE_ADDR;
	return lengthData;
}

uint32_t IeeeHeader::generateSeqCtl(uint8_t * data) {
	uint32_t lengthData = 0;
	Utilities::memcpy16(data, &this -> seqCtl, 1, LITTLE_ENDIAN_ORDER);
	data += sizeof(uint16_t);
	lengthData += sizeof(uint16_t);
	return lengthData;
}

uint32_t IeeeHeader::generateTimestamp(uint8_t * data) {
	uint32_t lengthData = 0;
	Utilities::memcpy64(data, &this -> timestamp, 1, LITTLE_ENDIAN_ORDER);
	data += sizeof(uint64_t);
	lengthData += sizeof(uint64_t);

	return lengthData;
}

uint32_t IeeeHeader::generateBeaconInterval(uint8_t * data) {
	uint32_t lengthData = 0;
	Utilities::memcpy16(data, &this -> beaconInterval, 1, LITTLE_ENDIAN_ORDER);
	data += sizeof(uint16_t);
	lengthData += sizeof(uint16_t);
	return lengthData;
}

uint32_t IeeeHeader::generateCapabilityInfo(uint8_t * data) {
	uint32_t lengthData = 0;
	Utilities::memcpy16(data, &this -> capabilityInfo, 1, LITTLE_ENDIAN_ORDER);
	data += sizeof(uint16_t);
	lengthData += sizeof(uint16_t);
	return lengthData;
}

uint32_t IeeeHeader::generateSSID(uint8_t * data) {
	uint32_t lengthData = 0;
	int ssidLength = strlen(this -> ssid);
	int id	       = 0;
	Utilities::memcpy8(data, &id, 1, LITTLE_ENDIAN_ORDER);
	data += sizeof(uint8_t);
	lengthData += sizeof(uint8_t);
	
	Utilities::memcpy8(data, &ssidLength, 1, LITTLE_ENDIAN_ORDER);
	data += sizeof(uint8_t);
	lengthData += sizeof(uint8_t);

	Utilities::memcpy8(data, this -> ssid, ssidLength, LITTLE_ENDIAN_ORDER);
	data += sizeof(uint8_t) * ssidLength;
	lengthData += sizeof(uint8_t) * ssidLength;
	return lengthData;
}

uint32_t IeeeHeader::generateFHparameterSet(uint8_t * data) {
	uint32_t lengthData = 0;
	if(this -> taggedParameters[TP_FH_PARAMETER_SET]) {
		int id = 2;
		int length = 5;
		Utilities::memcpy8(data, &id, 1, LITTLE_ENDIAN_ORDER);
		data += sizeof(uint8_t);
		lengthData += sizeof(uint8_t);

		Utilities::memcpy8(data, &length, 1, LITTLE_ENDIAN_ORDER);
		data += sizeof(uint8_t);
		lengthData += sizeof(uint8_t);

		Utilities::memcpy16(data, &this -> fhParameterSetDwellTime, 1, LITTLE_ENDIAN_ORDER);
		data += sizeof(uint16_t);
		lengthData += sizeof(uint16_t);

		Utilities::memcpy8(data, &this -> fhParameterSetHopSet, 1, LITTLE_ENDIAN_ORDER);
		data += sizeof(uint8_t);
		lengthData += sizeof(uint8_t);

		Utilities::memcpy8(data, &this -> fhParameterSetHopPattern, 1, LITTLE_ENDIAN_ORDER);
		data += sizeof(uint8_t);
		lengthData += sizeof(uint8_t);

		Utilities::memcpy8(data, &this -> fhParameterSetHopIndex, 1, LITTLE_ENDIAN_ORDER);
		data += sizeof(uint8_t);
		lengthData += sizeof(uint8_t);
	}
	return lengthData;
}

uint32_t IeeeHeader::generateDSparameterSet(uint8_t * data) {
	uint32_t lengthData = 0;
	if(this -> taggedParameters[TP_DS_PARAMETER_SET]) {
		int id = 3;
		int length = 1;
		Utilities::memcpy8(data, &id, 1, LITTLE_ENDIAN_ORDER);
		data += sizeof(uint8_t);
		lengthData += sizeof(uint8_t);

		Utilities::memcpy8(data, &length, 1, LITTLE_ENDIAN_ORDER);
		data += sizeof(uint8_t);
		lengthData += sizeof(uint8_t);

		Utilities::memcpy8(data, &this -> dsParameterSet, 1, LITTLE_ENDIAN_ORDER);
		data += sizeof(uint8_t);
		lengthData += sizeof(uint8_t);
	}
	return lengthData;
}

uint32_t IeeeHeader::generateCFparameterSet(uint8_t * data) {
	uint32_t lengthData = 0;
	if(this -> taggedParameters[TP_CF_PARAMETER_SET]) {
		int id = 4;
		int length = 6;
		Utilities::memcpy8(data, &id, 1, LITTLE_ENDIAN_ORDER);
		data += sizeof(uint8_t);
		lengthData += sizeof(uint8_t);

		Utilities::memcpy8(data, &length, 1, LITTLE_ENDIAN_ORDER);
		data += sizeof(uint8_t);
		lengthData += sizeof(uint8_t);

		Utilities::memcpy8(data, &this -> cfpCount, 1, LITTLE_ENDIAN_ORDER);
		data += sizeof(uint8_t);
		lengthData += sizeof(uint8_t);

		Utilities::memcpy8(data, &this -> cfpPeriod, 1, LITTLE_ENDIAN_ORDER);
		data += sizeof(uint8_t);
		lengthData += sizeof(uint8_t);	

		Utilities::memcpy16(data, &this -> cfpMaxDuration, 1, LITTLE_ENDIAN_ORDER);
		data += sizeof(uint16_t);
		lengthData += sizeof(uint16_t);	

		Utilities::memcpy16(data, &this -> cfpRemaining, 1, LITTLE_ENDIAN_ORDER);
		data += sizeof(uint16_t);
		lengthData += sizeof(uint16_t);
	}
	return lengthData;
}

uint32_t IeeeHeader::generateIBSSparameterSet(uint8_t * data) {
	uint32_t lengthData = 0;
	if(this -> taggedParameters[TP_IBSS_PARAMETER_SET]) {
		int id = 6;
		int length = 2;
		Utilities::memcpy8(data, &id, 1, LITTLE_ENDIAN_ORDER);
		data += sizeof(uint8_t);
		lengthData += sizeof(uint8_t);

		Utilities::memcpy8(data, &length, 1, LITTLE_ENDIAN_ORDER);
		data += sizeof(uint8_t);
		lengthData += sizeof(uint8_t);

		Utilities::memcpy16(data, &this -> ibssParameterSet, 1, LITTLE_ENDIAN_ORDER);
		data += sizeof(uint16_t);
		lengthData += sizeof(uint16_t);
	}
	return lengthData;
}

uint32_t IeeeHeader::generateTIM(uint8_t * data) {
	uint32_t lengthData = 0;
	if(this -> taggedParameters[TP_TRAFFIC_INDICATION_MAP]) {
		int timLength = strlen(this -> timPartialVirtualBitmap);
		int id 	       = 5;
		int length;
		Utilities::memcpy8(data, &id, 1, LITTLE_ENDIAN_ORDER);
		data += sizeof(uint8_t);
		lengthData += sizeof(uint8_t);
		
		int l = timLength / 2;
		if(timLength % 2 == 1) l++; 
		length = l + 3;

		Utilities::memcpy8(data, &length, 1, LITTLE_ENDIAN_ORDER);
		data += sizeof(uint8_t);
		lengthData += sizeof(uint8_t);
	
		Utilities::memcpy8(data, &this -> timDtimCount, 1, LITTLE_ENDIAN_ORDER);
		data += sizeof(uint8_t);
		lengthData += sizeof(uint8_t);

		Utilities::memcpy8(data, &this -> timDtimPeriod, 1, LITTLE_ENDIAN_ORDER);
		data += sizeof(uint8_t);
		lengthData += sizeof(uint8_t);

		Utilities::memcpy8(data, &this -> timBitmapControl, 1, LITTLE_ENDIAN_ORDER);
		data += sizeof(uint8_t);
		lengthData += sizeof(uint8_t);

		uint8_t pomChar[3]; pomChar[2] = 0;
		uint8_t p;
		int start = 0;
		if(timLength % 2 == 1) {//licha delka .. pridame nulu z leva
			pomChar[0] = '0';
			pomChar[1] = this -> timPartialVirtualBitmap[0];
			p = strtoll((char *)pomChar, NULL, 16);			
			Utilities::memcpy8(data, &p, 1, LITTLE_ENDIAN_ORDER);
			data += sizeof(uint8_t);
			lengthData += sizeof(uint8_t);
			start++;
			timLength--;
		} 
		for(int i = start; i < timLength; i = i + 2) {
			pomChar[0] = this -> timPartialVirtualBitmap[i];
			pomChar[1] = this -> timPartialVirtualBitmap[i+1];
			p = strtoll((char *)pomChar, NULL, 16);			
			Utilities::memcpy8(data, &p, 1, LITTLE_ENDIAN_ORDER);
			data += sizeof(uint8_t);
			lengthData += sizeof(uint8_t);
		}
	}
	return lengthData;
}

uint32_t IeeeHeader::generatePowerConstraint(uint8_t * data) {
	uint32_t lengthData = 0;
	if(this -> taggedParameters[TP_POWER_CONSTRAINT]) {
		int id     = 32;
		int length = 1;
		Utilities::memcpy8(data, &id, 1, LITTLE_ENDIAN_ORDER);
		data += sizeof(uint8_t);
		lengthData += sizeof(uint8_t);

		Utilities::memcpy8(data, &length, 1, LITTLE_ENDIAN_ORDER);
		data += sizeof(uint8_t);
		lengthData += sizeof(uint8_t);
	
		Utilities::memcpy8(data, &this -> localPowerConstraint, 1, LITTLE_ENDIAN_ORDER);
		data += sizeof(uint8_t);
		lengthData += sizeof(uint8_t);
	}
	return lengthData;
}

uint32_t IeeeHeader::generateChannelSwitch(uint8_t * data) {
	uint32_t lengthData = 0;
	if(this -> taggedParameters[TP_CHANNEL_SWITCH]) {
		int id     = 37;
		int length = 3;
		Utilities::memcpy8(data, &id, 1, LITTLE_ENDIAN_ORDER);
		data += sizeof(uint8_t);
		lengthData += sizeof(uint8_t);

		Utilities::memcpy8(data, &length, 1, LITTLE_ENDIAN_ORDER);
		data += sizeof(uint8_t);
		lengthData += sizeof(uint8_t);
	
		Utilities::memcpy8(data, &this -> channelSwitchMode, 1, LITTLE_ENDIAN_ORDER);
		data += sizeof(uint8_t);
		lengthData += sizeof(uint8_t);

		Utilities::memcpy8(data, &this -> newChannelNumber, 1, LITTLE_ENDIAN_ORDER);
		data += sizeof(uint8_t);
		lengthData += sizeof(uint8_t);

		Utilities::memcpy8(data, &this -> channelSwitchCount, 1, LITTLE_ENDIAN_ORDER);
		data += sizeof(uint8_t);
		lengthData += sizeof(uint8_t);
	}
	return lengthData;
}

uint32_t IeeeHeader::generateCountry(uint8_t * data) {
	uint32_t lengthData = 0;
	if(this -> taggedParameters[TP_COUNTRY]) {
		int id     = 7;
		int length = 0;
		uint8_t * pomData;
		Utilities::memcpy8(data, &id, 1, LITTLE_ENDIAN_ORDER);
		data += sizeof(uint8_t);
		lengthData += sizeof(uint8_t);

		pomData = data; //vlastni hodnotu zapisi az na konec
		data += sizeof(uint8_t);
		lengthData += sizeof(uint8_t); 

		Utilities::memcpy8(data, this -> countryString, 3, LITTLE_ENDIAN_ORDER);
		data += sizeof(uint8_t) * 3;
		lengthData += sizeof(uint8_t) * 3;
		length+=3;	

		/*Utilities::memcpy8(data, &length, 1, LITTLE_ENDIAN_ORDER);
		data += sizeof(uint8_t);
		lengthData += sizeof(uint8_t);*/
		if(this -> isSetAnyCountryTriplet()) {
			for(int i = 0; i < L_COUNTRY_IE_LENGTH; i++) {
				if(this -> countryConstraintTripletFlag[i] == true) {
					Utilities::memcpy8(data, &this -> countryConstraintTriplet[i].firstChannelNumber, 1, LITTLE_ENDIAN_ORDER);
					data += sizeof(uint8_t);
					lengthData += sizeof(uint8_t);

					Utilities::memcpy8(data, &this -> countryConstraintTriplet[i].numberOfChannel, 1, LITTLE_ENDIAN_ORDER);
					data += sizeof(uint8_t);
					lengthData += sizeof(uint8_t);

					Utilities::memcpy8(data, &this -> countryConstraintTriplet[i].maxTransmitPower, 1, LITTLE_ENDIAN_ORDER);
					data += sizeof(uint8_t);
					lengthData += sizeof(uint8_t);
			
					length += 3;
				} 
			}
		} else {
			int pom = D_FIRST_CHANNEL_NUMBER;
			Utilities::memcpy8(data, &pom, 1, LITTLE_ENDIAN_ORDER);
			data += sizeof(uint8_t);
			lengthData += sizeof(uint8_t);

			pom = D_NUMBER_OF_CHANNEL;
			Utilities::memcpy8(data, &pom, 1, LITTLE_ENDIAN_ORDER);
			data += sizeof(uint8_t);
			lengthData += sizeof(uint8_t);
		
			pom = D_MAX_TRANSMIT_POWER;
			Utilities::memcpy8(data, &pom, 1, LITTLE_ENDIAN_ORDER);
			data += sizeof(uint8_t);
			lengthData += sizeof(uint8_t);
			
			length += sizeof(uint8_t) * 3;
		}

		//v pripade, kdy delka je suda, tak pridame vypln
		if(length % 2 == 0) {
			int pad = 0;
			Utilities::memcpy8(data, &pad, 1, LITTLE_ENDIAN_ORDER);
			data += sizeof(uint8_t);
			lengthData += sizeof(uint8_t);
			length++;
		}
		Utilities::memcpy8(pomData, &length, 1, LITTLE_ENDIAN_ORDER);
	}
	return lengthData;
}

uint32_t IeeeHeader::generateQuiet(uint8_t * data) {
	uint32_t lengthData = 0;
	if(this -> taggedParameters[TP_QUIET]) {
		int id     = 40;
		int length = 6;
		Utilities::memcpy8(data, &id, 1, LITTLE_ENDIAN_ORDER);
		data += sizeof(uint8_t);
		lengthData += sizeof(uint8_t);

		Utilities::memcpy8(data, &length, 1, LITTLE_ENDIAN_ORDER);
		data += sizeof(uint8_t);
		lengthData += sizeof(uint8_t);
	
		Utilities::memcpy8(data, &this -> quietCount, 1, LITTLE_ENDIAN_ORDER);
		data += sizeof(uint8_t);
		lengthData += sizeof(uint8_t);

		Utilities::memcpy8(data, &this -> quietPeriod, 1, LITTLE_ENDIAN_ORDER);
		data += sizeof(uint8_t);
		lengthData += sizeof(uint8_t);

		Utilities::memcpy16(data, &this -> quietDuration, 1, LITTLE_ENDIAN_ORDER);
		data += sizeof(uint16_t);
		lengthData += sizeof(uint16_t);

		Utilities::memcpy16(data, &this -> quietOffset, 1, LITTLE_ENDIAN_ORDER);
		data += sizeof(uint16_t);
		lengthData += sizeof(uint16_t);
	}
	return lengthData;
}

uint32_t IeeeHeader::generateTCPreport(uint8_t * data) {
	uint32_t lengthData = 0;
	if(this -> taggedParameters[TP_TPC_REPORT]) {
		int id     = 35;
		int length = 2;
		Utilities::memcpy8(data, &id, 1, LITTLE_ENDIAN_ORDER);
		data += sizeof(uint8_t);
		lengthData += sizeof(uint8_t);

		Utilities::memcpy8(data, &length, 1, LITTLE_ENDIAN_ORDER);
		data += sizeof(uint8_t);
		lengthData += sizeof(uint8_t);
	
		Utilities::memcpy8(data, &this -> transmitPower, 1, LITTLE_ENDIAN_ORDER);
		data += sizeof(uint8_t);
		lengthData += sizeof(uint8_t);

		Utilities::memcpy8(data, &this -> linkMargin, 1, LITTLE_ENDIAN_ORDER);
		data += sizeof(uint8_t);
		lengthData += sizeof(uint8_t);
	}
	return lengthData;
}

uint32_t IeeeHeader::generateERP(uint8_t * data) {
	uint32_t lengthData = 0;
	if(this -> taggedParameters[TP_ERP_INFORMATION]) {
		int id     = 42;
		int length = 1;
		Utilities::memcpy8(data, &id, 1, LITTLE_ENDIAN_ORDER);
		data += sizeof(uint8_t);
		lengthData += sizeof(uint8_t);

		Utilities::memcpy8(data, &length, 1, LITTLE_ENDIAN_ORDER);
		data += sizeof(uint8_t);
		lengthData += sizeof(uint8_t);
	
		Utilities::memcpy8(data, &this -> erp, 1, LITTLE_ENDIAN_ORDER);
		data += sizeof(uint8_t);
		lengthData += sizeof(uint8_t);
	}
	return lengthData;
}

uint32_t IeeeHeader::generateSupportedRate(uint8_t * data) {
	uint32_t lengthData = 0;
	if(this -> taggedParameters[TP_SUPPORTED_RATE]) {
		int id     = 1;
		int length = 0;
		uint8_t * pomData;
		Utilities::memcpy8(data, &id, 1, LITTLE_ENDIAN_ORDER);
		data += sizeof(uint8_t);
		lengthData += sizeof(uint8_t);

		pomData = data;
		data += sizeof(uint8_t);
		lengthData += sizeof(uint8_t);

		if(this -> isSetAnySupportedRate()) {
			for(int i = 0; i < L_SUPPORTED_RATE_LENGTH; i++) {
				if(this -> supportedRateFlag[i]) {
					length++;
					Utilities::memcpy8(data, &this -> supportedRate[i], 1, LITTLE_ENDIAN_ORDER);
					data += sizeof(uint8_t);
					lengthData += sizeof(uint8_t);
				}
			}
		} else {
			uint8_t pom = D_SUPPORTED_RATE;
			Utilities::memcpy8(data, &pom, 1, LITTLE_ENDIAN_ORDER);
			data += sizeof(uint8_t);
			lengthData += sizeof(uint8_t);
			length++;
		}
		Utilities::memcpy8(pomData, &length, 1, LITTLE_ENDIAN_ORDER);
	}
	return lengthData;
}

uint32_t IeeeHeader::generateExtendedSupportedRate(uint8_t * data) {
	uint32_t lengthData = 0;
	if(this -> taggedParameters[TP_EXTENDED_SUPPORTED_RATE]) {
		int id     = 50;
		int length = 0;
		uint8_t * pomData;
		Utilities::memcpy8(data, &id, 1, LITTLE_ENDIAN_ORDER);
		data += sizeof(uint8_t);
		lengthData += sizeof(uint8_t);

		pomData = data;
		data += sizeof(uint8_t);
		lengthData += sizeof(uint8_t);

		if(this -> isSetAnyExtendedSupportedRate()) {
			for(int i = 0; i < L_EXTENDED_SUPPORTED_RATE_LENGTH; i++) {
				if(this -> extendedSupportedRateFlag[i]) {
					length++;
					Utilities::memcpy8(data, &this -> extendedSupportedRate[i], 1, LITTLE_ENDIAN_ORDER);
					data += sizeof(uint8_t);
					lengthData += sizeof(uint8_t);
				}
			}
		} else {
			uint8_t pom = D_EXTENDED_SUPPORTED_RATE;
			Utilities::memcpy8(data, &pom, 1, LITTLE_ENDIAN_ORDER);
			data += sizeof(uint8_t);
			lengthData += sizeof(uint8_t);
			length++;
		}

		Utilities::memcpy8(pomData, &length, 1, LITTLE_ENDIAN_ORDER);
	}
	return lengthData;
}

uint32_t IeeeHeader::generateRSN(uint8_t * data) {
	uint32_t lengthData = 0;
	if(this -> taggedParameters[TP_RSN]) {
		int id     = 48;
		int length = 0;
		uint8_t * pomData; 
		uint8_t * countPos;
		Utilities::memcpy8(data, &id, 1, LITTLE_ENDIAN_ORDER);
		data += sizeof(uint8_t);
		lengthData += sizeof(uint8_t);

		pomData = data;
		data += sizeof(uint8_t);
		lengthData += sizeof(uint8_t);
		
		length = lengthData;

		Utilities::memcpy16(data, &this -> rsnVersion, 1, LITTLE_ENDIAN_ORDER);
		data += sizeof(uint16_t);
		lengthData += sizeof(uint16_t);

		Utilities::memcpy8(data, &this -> groupCipherSuite.suiteOUI[0], 1, LITTLE_ENDIAN_ORDER);
		data += sizeof(uint8_t);
		lengthData += sizeof(uint8_t);
		Utilities::memcpy8(data, &this -> groupCipherSuite.suiteOUI[1], 1, LITTLE_ENDIAN_ORDER);
		data += sizeof(uint8_t);
		lengthData += sizeof(uint8_t);
		Utilities::memcpy8(data, &this -> groupCipherSuite.suiteOUI[2], 1, LITTLE_ENDIAN_ORDER);
		data += sizeof(uint8_t);
		lengthData += sizeof(uint8_t);
		Utilities::memcpy8(data, &this -> groupCipherSuite.suiteType, 1, LITTLE_ENDIAN_ORDER);
		data += sizeof(uint8_t);
		lengthData += sizeof(uint8_t);
		
		//count
		countPos = data;
		data += sizeof(uint16_t);
		lengthData += sizeof(uint16_t);

		bool exist = false;
		int count = 0;
		for(int i = 0; i < L_PAIRWISE_CIPHER_SUITE_COUNT; i++) {
			if(this -> pairwiseCipherSuiteItem[i]) {
				exist = true;
				count++;
				Utilities::memcpy8(data, &this -> pairwiseCipherSuite[i].suiteOUI[0], 1, LITTLE_ENDIAN_ORDER);
				data += sizeof(uint8_t);
				lengthData += sizeof(uint8_t);
				Utilities::memcpy8(data, &this -> pairwiseCipherSuite[i].suiteOUI[1], 1, LITTLE_ENDIAN_ORDER);
				data += sizeof(uint8_t);
				lengthData += sizeof(uint8_t);
				Utilities::memcpy8(data, &this -> pairwiseCipherSuite[i].suiteOUI[2], 1, LITTLE_ENDIAN_ORDER);
				data += sizeof(uint8_t);
				lengthData += sizeof(uint8_t);
				Utilities::memcpy8(data, &this -> pairwiseCipherSuite[i].suiteType, 1, LITTLE_ENDIAN_ORDER);
				data += sizeof(uint8_t);
				lengthData += sizeof(uint8_t);	
			}
		}
		if(!exist) {
			count++;
			uint8_t pom = D_GROUP_CIPHER_SUITE_OUI_0;
			Utilities::memcpy8(data, &pom, 1, LITTLE_ENDIAN_ORDER);
			data += sizeof(uint8_t);
			lengthData += sizeof(uint8_t);
			pom = D_GROUP_CIPHER_SUITE_OUI_1;
			Utilities::memcpy8(data, &pom, 1, LITTLE_ENDIAN_ORDER);
			data += sizeof(uint8_t);
			lengthData += sizeof(uint8_t);				
			pom = D_GROUP_CIPHER_SUITE_OUI_2;
			Utilities::memcpy8(data, &pom, 1, LITTLE_ENDIAN_ORDER);
			data += sizeof(uint8_t);
			lengthData += sizeof(uint8_t);
			pom = D_GROUP_CIPHER_SUITE_TYPE;
			Utilities::memcpy8(data, &pom, 1, LITTLE_ENDIAN_ORDER);
			data += sizeof(uint8_t);
			lengthData += sizeof(uint8_t);	
		}
		Utilities::memcpy16(countPos, &count, 1, LITTLE_ENDIAN_ORDER);

		//count
		countPos = data;
		data += sizeof(uint16_t);
		lengthData += sizeof(uint16_t);

		exist = false;
		count = 0;
		for(int i = 0; i < L_AUTHENTICATION_SUITE_COUNT; i++) {
			if(this -> authenticationSuiteItem[i]) {
				exist = true;
				count++;
				Utilities::memcpy8(data, &this -> authenticationSuite[i].suiteOUI[0], 1, LITTLE_ENDIAN_ORDER);
				data += sizeof(uint8_t);
				lengthData += sizeof(uint8_t);
				Utilities::memcpy8(data, &this -> authenticationSuite[i].suiteOUI[1], 1, LITTLE_ENDIAN_ORDER);
				data += sizeof(uint8_t);
				lengthData += sizeof(uint8_t);
				Utilities::memcpy8(data, &this -> authenticationSuite[i].suiteOUI[2], 1, LITTLE_ENDIAN_ORDER);
				data += sizeof(uint8_t);
				lengthData += sizeof(uint8_t);
				Utilities::memcpy8(data, &this -> authenticationSuite[i].suiteType, 1, LITTLE_ENDIAN_ORDER);
				data += sizeof(uint8_t);
				lengthData += sizeof(uint8_t);	
			}
		}
		if(!exist) {
			count++;
			uint8_t pom = D_AUTHENTICATION_SUITE_OUI_0;
			Utilities::memcpy8(data, &pom, 1, LITTLE_ENDIAN_ORDER);
			data += sizeof(uint8_t);
			lengthData += sizeof(uint8_t);
			pom = D_AUTHENTICATION_SUITE_OUI_1;
			Utilities::memcpy8(data, &pom, 1, LITTLE_ENDIAN_ORDER);
			data += sizeof(uint8_t);
			lengthData += sizeof(uint8_t);				
			pom = D_AUTHENTICATION_SUITE_OUI_2;
			Utilities::memcpy8(data, &pom, 1, LITTLE_ENDIAN_ORDER);
			data += sizeof(uint8_t);
			lengthData += sizeof(uint8_t);
			pom = D_AUTHENTICATION_SUITE_TYPE;
			Utilities::memcpy8(data, &pom, 1, LITTLE_ENDIAN_ORDER);
			data += sizeof(uint8_t);
			lengthData += sizeof(uint8_t);	
		}
		Utilities::memcpy16(countPos, &count, 1, LITTLE_ENDIAN_ORDER);

		Utilities::memcpy16(data, &this -> rsnCapabilities, 1, LITTLE_ENDIAN_ORDER);
		data += sizeof(uint16_t);
		lengthData += sizeof(uint16_t);

		length = lengthData - length;
		Utilities::memcpy8(pomData, &length, 1, LITTLE_ENDIAN_ORDER);
	}
	return lengthData;
}

uint32_t IeeeHeader::generateReaconCode(uint8_t * data) {
	uint32_t lengthData = 0;
	Utilities::memcpy16(data, &this -> reasonCode, 1, LITTLE_ENDIAN_ORDER);
	data += sizeof(uint16_t);
	lengthData += sizeof(uint16_t);
	return lengthData;
}

uint32_t IeeeHeader::generateListenInterval(uint8_t * data) {
	uint32_t lengthData = 0;
	Utilities::memcpy16(data, &this -> listenInterval, 1, LITTLE_ENDIAN_ORDER);
	data += sizeof(uint16_t);
	lengthData += sizeof(uint16_t);
	return lengthData;
}

uint32_t IeeeHeader::generateCurrentAPaddress(uint8_t * data) {
	uint32_t lengthData = 0;
	Utilities::memcpy8(data, &this -> addr4, L_GEN_SIZE_ADDR, LITTLE_ENDIAN_ORDER);
	data += L_GEN_SIZE_ADDR;
	lengthData += L_GEN_SIZE_ADDR;
	return lengthData;
}

uint32_t IeeeHeader::generateStatusCode(uint8_t * data) {
	uint32_t lengthData = 0;
	Utilities::memcpy16(data, &this -> statusCode, 1, LITTLE_ENDIAN_ORDER);
	data += sizeof(uint16_t);
	lengthData += sizeof(uint16_t);
	return lengthData;
}

uint32_t IeeeHeader::generateAssociationId(uint8_t * data) {
	uint32_t lengthData = 0;
	Utilities::memcpy16(data, &this -> associationId, 1, LITTLE_ENDIAN_ORDER);
	data += sizeof(uint16_t);
	lengthData += sizeof(uint16_t);
	return lengthData;
}

uint32_t IeeeHeader::generateAuthenticationAlgorithmNumber(uint8_t * data) {
	uint32_t lengthData = 0;
	Utilities::memcpy16(data, &this -> authenticationAlgorithmNumber, 1, LITTLE_ENDIAN_ORDER);
	data += sizeof(uint16_t);
	lengthData += sizeof(uint16_t);
	return lengthData;
}

uint32_t IeeeHeader::generateAuthenticationSequenceNumber(uint8_t * data) {
	uint32_t lengthData = 0;
	Utilities::memcpy16(data, &this -> authenticationSequenceNumber, 1, LITTLE_ENDIAN_ORDER);
	data += sizeof(uint16_t);
	lengthData += sizeof(uint16_t);
	return lengthData;
}

uint32_t IeeeHeader::generateChallengeText(uint8_t * data) {
	uint32_t lengthData = 0;
	uint8_t pom [3];
	uint8_t p;
	pom[2] = 0;
	int start = 0;
	int countCycle = strlen(this -> challengeText);
	int id     = 16;
	int length = 0;
	uint8_t * pomData; 

	Utilities::memcpy8(data, &id, 1, LITTLE_ENDIAN_ORDER);
	data += sizeof(uint8_t);
	lengthData += sizeof(uint8_t);

	pomData = data;
	data += sizeof(uint8_t);
	lengthData += sizeof(uint8_t);
		
	length = lengthData;
	if(countCycle % 2 == 1) {
		countCycle = countCycle - 1;
		start = 1;
		pom[0] = '0';
		pom[1] = this -> challengeText[0];
		p = strtoll((char *)pom, NULL, 16);
		Utilities::memcpy8(data, &p, 1, LITTLE_ENDIAN_ORDER);
		data += sizeof(uint8_t);
		lengthData += sizeof(uint8_t);
	}
	for(int i = start; i < countCycle; i = i + 2) {
		pom[0]   = this -> challengeText[i];
		pom[1] = this -> challengeText[i+1];
		p = strtoll((char *)pom, NULL, 16);
		Utilities::memcpy8(data, &p, 1, LITTLE_ENDIAN_ORDER);
		data += sizeof(uint8_t);
		lengthData += sizeof(uint8_t);
	}
	length = lengthData - length;
	Utilities::memcpy8(pomData, &length, 1, LITTLE_ENDIAN_ORDER);
	return lengthData;
}

uint32_t IeeeHeader::generateQos(uint8_t * data) {
	uint32_t lengthData = 0;
	Utilities::memcpy16(data, &this -> qos, 1, LITTLE_ENDIAN_ORDER);
	data += sizeof(uint16_t);
	lengthData += sizeof(uint16_t);
	return lengthData;
}

uint32_t IeeeHeader::generateAddress1(uint8_t * data) {
	uint32_t lengthData = 0;
	Utilities::memcpy8(data, &this -> addr1, L_GEN_SIZE_ADDR, LITTLE_ENDIAN_ORDER);
	data += L_GEN_SIZE_ADDR;
	lengthData += L_GEN_SIZE_ADDR;
	return lengthData;
}

uint32_t IeeeHeader::generateAddress2(uint8_t * data) {
	uint32_t lengthData = 0;
	Utilities::memcpy8(data, &this -> addr2, L_GEN_SIZE_ADDR, LITTLE_ENDIAN_ORDER);
	data += L_GEN_SIZE_ADDR;
	lengthData += L_GEN_SIZE_ADDR;
	return lengthData;
}

uint32_t IeeeHeader::generateAddress3(uint8_t * data) {
	uint32_t lengthData = 0;
	Utilities::memcpy8(data, &this -> addr3, L_GEN_SIZE_ADDR, LITTLE_ENDIAN_ORDER);
	data += L_GEN_SIZE_ADDR;
	lengthData += L_GEN_SIZE_ADDR;
	return lengthData;
}

uint32_t IeeeHeader::generateAddress4(uint8_t * data) {
	uint32_t lengthData = 0;
	Utilities::memcpy8(data, &this -> addr4, L_GEN_SIZE_ADDR, LITTLE_ENDIAN_ORDER);
	data += L_GEN_SIZE_ADDR;
	lengthData += L_GEN_SIZE_ADDR;
	return lengthData;
}
/*
uint32_t IeeeHeader::generate(uint8_t * data) {
	uint32_t lengthData = 0;

	return lengthData;
}
*/

/* IeeeHeader::generateDATA
 * Popis: funkce provede vyganerovani typickych casti pro datovy ramec
 * Parametry: uint8_t * data - ukazatel do pameti, kam se casti ramce vygeneruji
 * Navratova hodnota: delka vygenerovanych dat
 */
uint32_t IeeeHeader::generateDATA(uint8_t * data) {
	uint32_t lengthData = 0;
	lengthData += this -> generateDuration(data+lengthData);
	lengthData += this -> generateAddress1(data+lengthData);	
	lengthData += this -> generateAddress2(data+lengthData);	
	lengthData += this -> generateAddress3(data+lengthData);	
	lengthData += this -> generateSeqCtl(data+lengthData);
	if(this -> getToDS() == 1 && this -> getFromDS() == 1) {
		lengthData += this -> generateAddress4(data+lengthData);	
	}
	if(this -> getSubType() == ST_QOS_DATA) {
		lengthData += this -> generateQos(data+lengthData);
	}
	return lengthData;
}

/* IeeeHeader::generateRTS
 * Popis: funkce provede vyganerovani typickych casti pro RTS ramec
 * Parametry: uint8_t * data - ukazatel do pameti, kam se casti ramce vygeneruji
 * Navratova hodnota: delka vygenerovanych dat
 */
uint32_t IeeeHeader::generateRTS(uint8_t * data) {
	uint32_t lengthData = 0;
	lengthData += this -> generateDuration(data+lengthData);
	lengthData += this -> generateReceiveAddress(data+lengthData);
	lengthData += this -> generateTransmitterAddress(data+lengthData);
	return lengthData;
}

/* IeeeHeader::generateCTS
 * Popis: funkce provede vyganerovani typickych casti pro CTS ramec
 * Parametry: uint8_t * data - ukazatel do pameti, kam se casti ramce vygeneruji
 * Navratova hodnota: delka vygenerovanych dat
 */
uint32_t IeeeHeader::generateCTS(uint8_t * data) {
	uint32_t lengthData = 0;
	lengthData += this -> generateDuration(data+lengthData);
	lengthData += this -> generateReceiveAddress(data+lengthData);
	return lengthData;
}

/* IeeeHeader::generateACK
 * Popis: funkce provede vyganerovani typickych casti pro ACK ramec
 * Parametry: uint8_t * data - ukazatel do pameti, kam se casti ramce vygeneruji
 * Navratova hodnota: delka vygenerovanych dat
 */
uint32_t IeeeHeader::generateACK(uint8_t * data) {
	uint32_t lengthData = 0;
	lengthData += this -> generateDuration(data+lengthData);
	lengthData += this -> generateReceiveAddress(data+lengthData);
	return lengthData;
}

/* IeeeHeader::generatePSPOLL
 * Popis: funkce provede vyganerovani typickych casti pro PS-POLL ramec
 * Parametry: uint8_t * data - ukazatel do pameti, kam se casti ramce vygeneruji
 * Navratova hodnota: delka vygenerovanych dat
 */
uint32_t IeeeHeader::generatePSPOLL(uint8_t * data) {
	uint32_t lengthData = 0;
	lengthData += this -> generateAID(data+lengthData);
	lengthData += this -> generateBSSID(data+lengthData);	
	lengthData += this -> generateTransmitterAddress(data+lengthData);
	return lengthData;
}

/* IeeeHeader::generateBEACON
 * Popis: funkce provede vyganerovani typickych casti pro BEACON ramec
 * Parametry: uint8_t * data - ukazatel do pameti, kam se casti ramce vygeneruji
 * Navratova hodnota: delka vygenerovanych dat
 */
uint32_t IeeeHeader::generateBEACON(uint8_t * data) {
	uint32_t lengthData = 0;
	lengthData += this -> generateDuration(data+lengthData);
	lengthData += this -> generateDA(data+lengthData);	
	lengthData += this -> generateSA(data+lengthData);
	lengthData += this -> generateBSSID(data+lengthData);
	lengthData += this -> generateSeqCtl(data+lengthData);
	lengthData += this -> generateTimestamp(data+lengthData);
	lengthData += this -> generateBeaconInterval(data+lengthData);
	lengthData += this -> generateCapabilityInfo(data+lengthData);
	lengthData += this -> generateSSID(data+lengthData);
	lengthData += this -> generateFHparameterSet(data+lengthData);
	lengthData += this -> generateDSparameterSet(data+lengthData);
	lengthData += this -> generateCFparameterSet(data+lengthData);	
	lengthData += this -> generateIBSSparameterSet(data+lengthData);
	lengthData += this -> generateTIM(data+lengthData);
	lengthData += this -> generatePowerConstraint(data+lengthData);		
	lengthData += this -> generateChannelSwitch(data+lengthData);		
	lengthData += this -> generateCountry(data+lengthData);
	lengthData += this -> generateQuiet(data+lengthData);
	lengthData += this -> generateTCPreport(data+lengthData);
	lengthData += this -> generateERP(data+lengthData);
	lengthData += this -> generateSupportedRate(data+lengthData);
	lengthData += this -> generateExtendedSupportedRate(data+lengthData);
	lengthData += this -> generateRSN(data+lengthData);	
	return lengthData;
}

/* IeeeHeader::generatePROBEREQUEST
 * Popis: funkce provede vyganerovani typickych casti pro PROBE REQUEST ramec
 * Parametry: uint8_t * data - ukazatel do pameti, kam se casti ramce vygeneruji
 * Navratova hodnota: delka vygenerovanych dat
 */
uint32_t IeeeHeader::generatePROBEREQUEST(uint8_t * data) {
	uint32_t lengthData = 0;
	lengthData += this -> generateDuration(data+lengthData);
	lengthData += this -> generateDA(data+lengthData);	
	lengthData += this -> generateSA(data+lengthData);
	lengthData += this -> generateBSSID(data+lengthData);
	lengthData += this -> generateSeqCtl(data+lengthData);
	lengthData += this -> generateSSID(data+lengthData);
	lengthData += this -> generateExtendedSupportedRate(data+lengthData);
	lengthData += this -> generateSupportedRate(data+lengthData);	
	return lengthData;
}

/* IeeeHeader::generatePROBERESPONSE
 * Popis: funkce provede vyganerovani typickych casti pro PROBE RESPONSE ramec
 * Parametry: uint8_t * data - ukazatel do pameti, kam se casti ramce vygeneruji
 * Navratova hodnota: delka vygenerovanych dat
 */
uint32_t IeeeHeader::generatePROBERESPONSE(uint8_t * data) {
	uint32_t lengthData = 0;
	lengthData += this -> generateDuration(data+lengthData);
	lengthData += this -> generateDA(data+lengthData);	
	lengthData += this -> generateSA(data+lengthData);
	lengthData += this -> generateBSSID(data+lengthData);
	lengthData += this -> generateSeqCtl(data+lengthData);
	lengthData += this -> generateTimestamp(data+lengthData);
	lengthData += this -> generateBeaconInterval(data+lengthData);
	lengthData += this -> generateCapabilityInfo(data+lengthData);
	lengthData += this -> generateSSID(data+lengthData);
	lengthData += this -> generateSupportedRate(data+lengthData);
	lengthData += this -> generateFHparameterSet(data+lengthData);
	lengthData += this -> generateDSparameterSet(data+lengthData);
	lengthData += this -> generateCFparameterSet(data+lengthData);	
	lengthData += this -> generateIBSSparameterSet(data+lengthData);
	lengthData += this -> generateCountry(data+lengthData);

	lengthData += this -> generatePowerConstraint(data+lengthData);		

	lengthData += this -> generateQuiet(data+lengthData);

	lengthData += this -> generateTCPreport(data+lengthData);
	lengthData += this -> generateERP(data+lengthData);
	lengthData += this -> generateExtendedSupportedRate(data+lengthData);
	lengthData += this -> generateRSN(data+lengthData);	
	return lengthData;
}

/* IeeeHeader::generateATIM
 * Popis: funkce provede vyganerovani typickych casti pro ATIM ramec
 * Parametry: uint8_t * data - ukazatel do pameti, kam se casti ramce vygeneruji
 * Navratova hodnota: delka vygenerovanych dat
 */
uint32_t IeeeHeader::generateATIM(uint8_t * data) {
	uint32_t lengthData = 0;
	lengthData += this -> generateDuration(data+lengthData);
	lengthData += this -> generateDA(data+lengthData);	
	lengthData += this -> generateSA(data+lengthData);
	lengthData += this -> generateBSSID(data+lengthData);
	lengthData += this -> generateSeqCtl(data+lengthData);
	return lengthData;
}

/* IeeeHeader::generateDEAUTHETICATION
 * Popis: funkce provede vyganerovani typickych casti pro DEAUTHETICATION ramec
 * Parametry: uint8_t * data - ukazatel do pameti, kam se casti ramce vygeneruji
 * Navratova hodnota: delka vygenerovanych dat
 */
uint32_t IeeeHeader::generateDEAUTHETICATION(uint8_t * data) {
	uint32_t lengthData = 0;
	lengthData += this -> generateDuration(data+lengthData);
	lengthData += this -> generateDA(data+lengthData);	
	lengthData += this -> generateSA(data+lengthData);
	lengthData += this -> generateBSSID(data+lengthData);
	lengthData += this -> generateSeqCtl(data+lengthData);
	lengthData += this -> generateReaconCode(data+lengthData);
	return lengthData;
}

/* IeeeHeader::generateDISASSOCIATION
 * Popis: funkce provede vyganerovani typickych casti pro DISASSOCIATION ramec
 * Parametry: uint8_t * data - ukazatel do pameti, kam se casti ramce vygeneruji
 * Navratova hodnota: delka vygenerovanych dat
 */
uint32_t IeeeHeader::generateDISASSOCIATION(uint8_t * data) {
	uint32_t lengthData = 0;
	lengthData += this -> generateDuration(data+lengthData);
	lengthData += this -> generateDA(data+lengthData);	
	lengthData += this -> generateSA(data+lengthData);
	lengthData += this -> generateBSSID(data+lengthData);
	lengthData += this -> generateSeqCtl(data+lengthData);
	lengthData += this -> generateReaconCode(data+lengthData);
	return lengthData;
}

/* IeeeHeader::generateASSOCIATIONREQUEST
 * Popis: funkce provede vyganerovani typickych casti pro ASSOCIATION REQUEST ramec
 * Parametry: uint8_t * data - ukazatel do pameti, kam se casti ramce vygeneruji
 * Navratova hodnota: delka vygenerovanych dat
 */
uint32_t IeeeHeader::generateASSOCIATIONREQUEST(uint8_t * data) {
	uint32_t lengthData = 0;
	lengthData += this -> generateDuration(data+lengthData);
	lengthData += this -> generateDA(data+lengthData);	
	lengthData += this -> generateSA(data+lengthData);
	lengthData += this -> generateBSSID(data+lengthData);
	lengthData += this -> generateSeqCtl(data+lengthData);
	lengthData += this -> generateCapabilityInfo(data+lengthData);
	lengthData += this -> generateListenInterval(data+lengthData);
	lengthData += this -> generateSSID(data+lengthData);
	lengthData += this -> generateSupportedRate(data+lengthData);
	return lengthData;
}

/* IeeeHeader::generateREASSOCIATIONREQUEST
 * Popis: funkce provede vyganerovani typickych casti pro REASSOCIATION REQUEST ramec
 * Parametry: uint8_t * data - ukazatel do pameti, kam se casti ramce vygeneruji
 * Navratova hodnota: delka vygenerovanych dat
 */
uint32_t IeeeHeader::generateREASSOCIATIONREQUEST(uint8_t * data) {
	uint32_t lengthData = 0;
	lengthData += this -> generateDuration(data+lengthData);
	lengthData += this -> generateDA(data+lengthData);	
	lengthData += this -> generateSA(data+lengthData);
	lengthData += this -> generateBSSID(data+lengthData);
	lengthData += this -> generateSeqCtl(data+lengthData);
	lengthData += this -> generateCapabilityInfo(data+lengthData);
	lengthData += this -> generateListenInterval(data+lengthData);
	lengthData += this -> generateCurrentAPaddress(data+lengthData);
	lengthData += this -> generateSSID(data+lengthData);
	lengthData += this -> generateSupportedRate(data+lengthData);
	return lengthData;
}

/* IeeeHeader::generateREASSOCIATIONRESPONSE
 * Popis: funkce provede vyganerovani typickych casti pro REASSOCIATION RESPONSE ramec
 * Parametry: uint8_t * data - ukazatel do pameti, kam se casti ramce vygeneruji
 * Navratova hodnota: delka vygenerovanych dat
 */
uint32_t IeeeHeader::generateREASSOCIATIONRESPONSE(uint8_t * data) {
	this -> taggedParameters [TP_SUPPORTED_RATE] = true;
	uint32_t lengthData = 0;
	lengthData += this -> generateDuration(data+lengthData);
	lengthData += this -> generateDA(data+lengthData);	
	lengthData += this -> generateSA(data+lengthData);
	lengthData += this -> generateBSSID(data+lengthData);
	lengthData += this -> generateSeqCtl(data+lengthData);
	lengthData += this -> generateCapabilityInfo(data+lengthData);
	lengthData += this -> generateStatusCode(data+lengthData);
	lengthData += this -> generateAssociationId(data+lengthData);
	lengthData += this -> generateSupportedRate(data+lengthData);
	return lengthData;
}

/* IeeeHeader::generateAUTHENTIATION
 * Popis: funkce provede vyganerovani typickych casti pro AUTHENTIATION ramec
 * Parametry: uint8_t * data - ukazatel do pameti, kam se casti ramce vygeneruji
 * Navratova hodnota: delka vygenerovanych dat
 */
uint32_t IeeeHeader::generateAUTHENTICATION(uint8_t * data) {
	uint32_t lengthData = 0;
	lengthData += this -> generateDuration(data+lengthData);
	lengthData += this -> generateDA(data+lengthData);	
	lengthData += this -> generateSA(data+lengthData);
	lengthData += this -> generateBSSID(data+lengthData);
	lengthData += this -> generateSeqCtl(data+lengthData);
	lengthData += this -> generateAuthenticationAlgorithmNumber(data+lengthData);
	lengthData += this -> generateAuthenticationSequenceNumber(data+lengthData);
	lengthData += this -> generateStatusCode(data+lengthData);
	lengthData += this -> generateChallengeText(data+lengthData);
	return lengthData;
}

/* IeeeHeader::printHeader
 * Popis: funkce provede zobrazeni hlavicky na terminalu ve strukturovane podobe
 * Parametry: -
 * Navratova hodnota: -
 */
void IeeeHeader::printHeader() {
	char string[100];

	printf("IeeeHeader:\r\n");
	//vypiseme spolecna pole pro vsechny ramce
	printf("\tFrame control ............... 0x%04x\r\n", this -> frameControl);
	printf("\t\tProtocol ............ %d\r\n", this -> getProtocol());
	this -> getTypeString(string);
	printf("\t\tType ................ %d (%s)\r\n", this -> getType(), string);
	this -> getSubTypeString(string);
	printf("\t\tSub type ............ %d (%s)\r\n", this -> getSubType(), string);
	printf("\t\tTo DS ............... %d\r\n", this -> getToDS());
	printf("\t\tFrom DS ............. %d\r\n", this -> getFromDS());
	printf("\t\tMore frag ........... %d\r\n", this -> getMoreFrag());
	printf("\t\tRetry ............... %d\r\n", this -> getRetry());
	printf("\t\tPwr mgmt ............ %d\r\n", this -> getPwrMgmt());
	printf("\t\tMore data ........... %d\r\n", this -> getMoreData());
	printf("\t\tProtected frame ..... %d\r\n", this -> getProtectedFrame());
	printf("\t\tOrder ............... %d\r\n", this -> getOrder());
	
	//vypsani ruznych typu ramcu
	switch(this -> getType()) {
		case T_CONTROL_FRAME:    {
			switch(this -> getSubType()) {
				case ST_RTS: {
					this -> printRTS();
				}; break;
				case ST_CTS: {
					this -> printCTS();
				}; break;
				case ST_ACK: {
					this -> printACK();
				}; break;
				case ST_PS_POLL: {
					this -> printPSPOLL();
				}; break;
			}
		}; break;
		case T_MANAGEMENT_FRAME: {
			switch(this -> getSubType()) {
				case ST_BEACON: {
					this -> printBEACON();
				}; break;
				case ST_PROBE_REQUEST: {
					this -> printPROBEREQUEST();
				}; break;
			        case ST_PROBE_RESPONSE: {
					this -> printPROBERESPONSE();
				}; break;
			        case ST_ATIM: {
					this -> printATIM();
				}; break;
			        case ST_DEAUTHETICATION: {
					this -> printDEAUTHETICATION();
				}; break;
			        case ST_DISASSOCIATION: {
					this -> printDISASSOCIATION();
				}; break;
			        case ST_ASSOCIATION_REQUEST: {
					this -> printASSOCIATIONREQUEST();
				}; break;
			        case ST_REASSOCIATION_REQUEST: {
					this -> printREASSOCIATIONREQUEST();
				}; break;
			        case ST_REASSOCIATION_RESPONSE: {
					this -> printREASSOCIATIONRESPONSE();
				}; break;
			        case ST_AUTHETICATION: {
					this -> printAUTHENTICATION();
				}; break;
			}
		}; break;
		case T_DATA_FRAME: 	 {
			this -> printDATA();
		}; break;
	}
}

/* IeeeHeader::getTypeString
 * Popis: funkce navrati textovy retezec nazvu typu
 * Parametry: string - misto, kam se retezec ulozi
 * Navratova hodnota: -
 */
void IeeeHeader::getTypeString(char * string) {
	switch(this->getType()) {
		case T_CONTROL_FRAME : {
			strcpy(string, TYP_CONTROL_STRING);
		}; break;
		case T_MANAGEMENT_FRAME : {
			strcpy(string, TYP_MANAGEMENT_STRING);
		}; break;
		case T_DATA_FRAME : {
			strcpy(string, TYP_DATA_STRING);
		}; break;
		default : {
			strcpy(string, UNKNOWN);
		}
	}
}

/* IeeeHeader::getSubTypeString
 * Popis: funkce navrati textovy retezec nazvu sub typu
 * Parametry: string - misto, kam se retezec ulozi
 * Navratova hodnota: -
 */
void IeeeHeader::getSubTypeString(char * string) {
	switch(this->getType()) {
		case T_CONTROL_FRAME : {
			switch(this -> getSubType()) {
				case ST_RTS : {
					strcpy(string, SUB_TYP_RTS);
				}; break;
				case ST_CTS : {
					strcpy(string, SUB_TYP_CTS);
				}; break;
				case ST_ACK : {
					strcpy(string, SUB_TYP_ACK);
				}; break;
				case ST_PS_POLL : {
					strcpy(string, SUB_TYP_PS_POLL);
				}; break;
			}
		}; break;
		case T_MANAGEMENT_FRAME : {
			switch(this -> getSubType()) {
				case ST_BEACON : {
					strcpy(string, SUB_TYP_BEACON);
				}; break;
			        case ST_PROBE_REQUEST: {
					strcpy(string, SUB_TYP_PROBE_REQUEST);
				}; break;
			        case ST_PROBE_RESPONSE: {
					strcpy(string, SUB_TYP_PROBE_RESPONSE);
				}; break;
			        case ST_ATIM: {
					strcpy(string, SUB_TYP_ATIM);
				}; break;
			        case ST_DEAUTHETICATION: {
					strcpy(string, SUB_TYP_DEAUTHETICATION);
				}; break;
			        case ST_DISASSOCIATION: {
					strcpy(string, SUB_TYP_DISASSOCIATION);
				}; break;
			        case ST_ASSOCIATION_REQUEST: {
					strcpy(string, SUB_TYP_ASSOCIATION_REQUEST);
				}; break;
			        case ST_REASSOCIATION_REQUEST: {
					strcpy(string, SUB_TYP_REASSOCIATION_REQUEST);
				}; break;
			        case ST_REASSOCIATION_RESPONSE: {
					strcpy(string, SUB_TYP_REASSOCIATION_RESPONSE);
				}; break;
			        case ST_AUTHETICATION: {
					strcpy(string, SUB_TYP_AUTHETICATION);
				}; break;
			}
		}; break;
		case T_DATA_FRAME : {
			switch(this -> getSubType()) {
				case ST_QOS_DATA : {
					strcpy(string, SUB_TYP_QOS_DATA);
				}; break;
				default : {
					strcpy(string, TYP_DATA_STRING);
				}; break;
			}
		}; break;
		default : {
			strcpy(string, UNKNOWN);
		}
	}
}

//tisk jednotlivych polozek hlavicky
void IeeeHeader::printDuration() {
	printf("\tDuration: ................... 0x%04x\r\n", this -> duration);
}

void IeeeHeader::printReceiverAddress() {
	printf("\tReceiver address: ........... %s\r\n", this -> addr1s);
}

void IeeeHeader::printTransmitterAddress() {
	printf("\tTransmitter address: ........ %s\r\n", this -> addr2s);
}

void IeeeHeader::printAID() {
	printf("\tAID: ........................ 0x%04x\r\n", this -> aid);
}

void IeeeHeader::printBSSID() {
	printf("\tBSSID: ...................... %s\r\n", this -> addr1s);
}


void IeeeHeader::printDA() {
	printf("\tDA: ......................... %s\r\n", this -> addr2s);	
}

void IeeeHeader::printSA() {
	printf("\tSA: ......................... %s\r\n", this -> addr3s);
}

void IeeeHeader::printSeqCtl() {
	printf("\tSeqCtl: ..................... 0x%04x\r\n", this -> seqCtl);
	printf("\t\tSeq: ................ 0x%03x\r\n", this -> getSequence());
	printf("\t\tFrag: ............... 0x%01x\r\n", this -> getFragment());
}

void IeeeHeader::printTimestamp() {
	printf("\tTimestamp: .................. 0x%016llx\r\n", this -> timestamp);
}

void IeeeHeader::printBeaconInterval() {
	printf("\tBeacon interval: ............ %d ms (0x%04x)\r\n", this -> beaconInterval, this -> beaconInterval);
}

void IeeeHeader::printCapabilityInfo() {
	printf("\tCapability info: ............ 0x%04x\r\n", this -> capabilityInfo);
	printf("\t\tESS ................. %d\r\n", this -> getBitCapabilityInfo(CI_ESS));
	printf("\t\tIBSS ................ %d\r\n", this -> getBitCapabilityInfo(CI_IBSS));
	printf("\t\tCF-Pollable ......... %d\r\n", this -> getBitCapabilityInfo(CI_CF_POLLABLE));
	printf("\t\tCF-Poll request ..... %d\r\n", this -> getBitCapabilityInfo(CI_CF_POLL_REQUEST));
	printf("\t\tPrivacy ............. %d\r\n", this -> getBitCapabilityInfo(CI_PRIVACY));
	printf("\t\tShort preambule ..... %d\r\n", this -> getBitCapabilityInfo(CI_SHORT_PREAMBULE));
	printf("\t\tPBCC ................ %d\r\n", this -> getBitCapabilityInfo(CI_PBCC));
	printf("\t\tChannel agility ..... %d\r\n", this -> getBitCapabilityInfo(CI_CHANNEL_AGILITY));
	printf("\t\tShort slot time ..... %d\r\n", this -> getBitCapabilityInfo(CI_SHORT_SLOT_TIME));
	printf("\t\tDSSS-OFDM ........... %d\r\n", this -> getBitCapabilityInfo(CI_DSSS_OFDM));
}

void IeeeHeader::printSSID() {
	printf("\tSSID ........................ \"%s\"\r\n", this -> ssid);
}

void IeeeHeader::printFHparameterSet() {
	if(this -> taggedParameters[TP_FH_PARAMETER_SET]) {
		printf("\tFH parameter set\r\n");
		printf("\t\tFH parameter set dwell time  0x%04x\r\n", this -> fhParameterSetDwellTime);
		printf("\t\tFH parameter set hop set     0x%02x\r\n", this -> fhParameterSetHopSet);
		printf("\t\tFH parameter set hop pattern 0x%02x\r\n", this -> fhParameterSetHopPattern);
		printf("\t\tFH parameter set hop index   0x%02x\r\n", this -> fhParameterSetHopIndex);
	}
}

void IeeeHeader::printDSparameterSet() {
	if(this -> taggedParameters[TP_DS_PARAMETER_SET]) {
		printf("\tDS parameter set ............ 0x%02x\r\n", this -> dsParameterSet);
	}
}

void IeeeHeader::printCFparameterSet() {
	if(this -> taggedParameters[TP_CF_PARAMETER_SET]) {
		printf("\tCF parameter set\r\n");
		printf("\t\tCF parameter set count        0x%02x\r\n", this -> cfpCount);
		printf("\t\tCF parameter set period       0x%02x\r\n", this -> cfpPeriod);
		printf("\t\tCF parameter set max duration 0x%04x\r\n", this -> cfpMaxDuration);
		printf("\t\tCF parameter set remaining    0x%04x\r\n", this -> cfpRemaining);
	}
}

void IeeeHeader::printIBSSparameterSet() {
	if(this -> taggedParameters[TP_IBSS_PARAMETER_SET]) {
		printf("\tIBSS parameter set .......... 0x%04x\r\n", this -> ibssParameterSet);
	}
}

void IeeeHeader::printTrafficIndicationMap() {
	if(this -> taggedParameters[TP_TRAFFIC_INDICATION_MAP]) {
		printf("\tTraffic indication map\r\n");
		printf("\t\tTIM dtim count     0x%02x\r\n", this -> timDtimCount);
		printf("\t\tTIM dtim period    0x%02x\r\n", this -> timDtimPeriod);
		printf("\t\tTIM bitmap control 0x%02x\r\n", this -> timBitmapControl);
		printf("\t\tTIM partial virtual bitmap 0x%s\r\n", this -> timPartialVirtualBitmap);
	}
}

void IeeeHeader::printPowerConstraint() {
	if(this -> taggedParameters[TP_POWER_CONSTRAINT]) {
		printf("\tLocal power constraint	0x%02x\r\n", this -> localPowerConstraint);
	}
}

void IeeeHeader::printCountry() {
	if(this -> taggedParameters[TP_COUNTRY]) {
		printf("\tCountry\r\n");
		printf("\t\tCountry string ........... \"%s\"\r\n", this -> countryString);
		if(this -> isSetAnyCountryTriplet()) {
			for(int i = 0; i < L_COUNTRY_IE_LENGTH; i++) {
				if(this -> countryConstraintTripletFlag[i] == true) {
					printf("\t\t%d:\r\n", i);
					printf("\t\tFirst channel number ........ 0x%02x\r\n", this -> countryConstraintTriplet[i].firstChannelNumber);
					printf("\t\tNumber of channels .......... 0x%02x\r\n", this -> countryConstraintTriplet[i].numberOfChannel);
					printf("\t\tMax transmit power .......... 0x%02x\r\n", this -> countryConstraintTriplet[i].maxTransmitPower);
				} 
			}
		} else {
			printf("\t\tFirst channel number ........ 0x%02x\r\n", D_FIRST_CHANNEL_NUMBER);
			printf("\t\tNumber of channels .......... 0x%02x\r\n", D_NUMBER_OF_CHANNEL);
			printf("\t\tMax transmit power .......... 0x%02x\r\n", D_MAX_TRANSMIT_POWER);
		}
	}
}

void IeeeHeader::printChannelSwitch() {
	if(this -> taggedParameters[TP_CHANNEL_SWITCH]) {
		printf("\tChannel Switch\r\n");
		printf("\t\tChannel switch mode ..... 0x%02x\r\n", this -> channelSwitchMode);
		printf("\t\tNew channel number ...... 0x%02x\r\n", this -> newChannelNumber);
		printf("\t\tChannel switch count .... 0x%02x\r\n", this -> channelSwitchCount);
	}
}

void IeeeHeader::printQuiet() {
	if(this -> taggedParameters[TP_QUIET]) {
		printf("\tQuiet\r\n");
		printf("\t\tQuiet count ............. 0x%02x\r\n", this -> quietCount);
		printf("\t\tQuiet period ............ 0x%02x\r\n", this -> quietPeriod);
		printf("\t\tQuiet duration .......... 0x%04x\r\n", this -> quietDuration);
		printf("\t\tQuiet offset ............ 0x%04x\r\n", this -> quietOffset);
	}
}

void IeeeHeader::printTPCreport() {
	if(this -> taggedParameters[TP_TPC_REPORT]) {
		printf("\tTPC report\r\n");
		printf("\t\tTransmit power .......... 0x%02x\r\n", this -> transmitPower);
		printf("\t\tLink margin ............. 0x%02x\r\n", this -> linkMargin);
	}
}

void IeeeHeader::printERPinformation() {
	if(this -> taggedParameters[TP_ERP_INFORMATION]) {
		printf("\tERP ....................... 0x%02x\r\n", this -> erp);
	}
}

void IeeeHeader::printSupportedRate() {
	if(this -> taggedParameters[TP_SUPPORTED_RATE]) {
		printf("\tSupported rate  ~ 0x01 = ((0x01 & 0x7f) * 500) Kbps \r\n");
		if(this -> isSetAnySupportedRate()) {
			for(int i = 0; i < L_SUPPORTED_RATE_LENGTH; i++) {
				if(this -> supportedRateFlag[i]) {
					printf("\t\t%d: .................. %d Kbps\r\n", i, (this -> supportedRate[i] & 0x7f) * 500);
				}
			}
		} else {
			printf("\t\t%d: .................. %d Kbps\r\n", 0, (D_SUPPORTED_RATE & 0x7f) * 500);
		}
	}
}

void IeeeHeader::printExtendedSupportedRate() {
	if(this -> taggedParameters[TP_EXTENDED_SUPPORTED_RATE]) {
		printf("\tExtended supported rate ~ 0x01 = ((0x01 & 0x7f) * 500) Kbps\r\n");
		if(this -> isSetAnyExtendedSupportedRate()) {
			for(int i = 0; i < L_EXTENDED_SUPPORTED_RATE_LENGTH; i++) {
				if(this -> extendedSupportedRateFlag[i]) {
					printf("\t\t%d: .................. %d Kbps\r\n", i, (this -> extendedSupportedRate[i] & 0x7f) * 500);
				}
			}
		} else {
			printf("\t\t%d: .................. %d Kbps\r\n", 0, ( D_EXTENDED_SUPPORTED_RATE & 0x7f) * 500);
		}		
	}
}

void IeeeHeader::printRSN() {
	if(this -> taggedParameters[TP_RSN]) {
		printf("\tRSN\r\n");
		printf("\t\tVersion ........................... 0x%04x\r\n", this -> rsnVersion);
		printf("\t\tGroup cipher suite OUI ............ %02x-%02x-%02x\r\n", this -> groupCipherSuite.suiteOUI[0], 
									      this -> groupCipherSuite.suiteOUI[1], 
									      this -> groupCipherSuite.suiteOUI[2]);
		printf("\t\tGroup cipher suite type ........... 0x%02x\r\n", this -> groupCipherSuite.suiteType);
		bool exist = false;
		printf("\t\tPairwise cipher suite\r\n");
		for(int i = 0; i < L_PAIRWISE_CIPHER_SUITE_COUNT; i++) {
			if(this -> pairwiseCipherSuiteItem[i]) {
				exist = true;
				printf("\t\t%d:\r\n",i);
				printf("\t\t\tOUI ....................... %02x-%02x-%02x\r\n", this -> pairwiseCipherSuite[i].suiteOUI[0], 
											      this -> pairwiseCipherSuite[i].suiteOUI[1], 
											      this -> pairwiseCipherSuite[i].suiteOUI[2]);
				printf("\t\t\ttype ...................... 0x%02x\r\n", this -> pairwiseCipherSuite[i].suiteType);				
			}
		}
		if(!exist) {
			printf("\t\t0:\r\n");
			printf("\t\t\tOUI ....................... %02x-%02x-%02x\r\n", D_GROUP_CIPHER_SUITE_OUI_0, 
										      D_GROUP_CIPHER_SUITE_OUI_1, 
										      D_GROUP_CIPHER_SUITE_OUI_2);
			printf("\t\t\ttype ...................... 0x%02x\r\n", D_GROUP_CIPHER_SUITE_TYPE);				
		}

		exist = false;
		printf("\t\tAuthentication suite\r\n");
		for(int i = 0; i < L_AUTHENTICATION_SUITE_COUNT; i++) {
			if(this -> authenticationSuiteItem[i]) {
				exist = true;
				printf("\t\t%d:\r\n",i);
				printf("\t\t\tOUI ....................... %02x-%02x-%02x\r\n", this -> authenticationSuite[i].suiteOUI[0], 
											      	    this -> authenticationSuite[i].suiteOUI[1], 
											            this -> authenticationSuite[i].suiteOUI[2]);
				printf("\t\t\ttype ...................... 0x%02x\r\n", this -> authenticationSuite[i].suiteType);				
			}
		}
		if(!exist) {
			printf("\t\t0:\r\n");
			printf("\t\t\tOUI ....................... %02x-%02x-%02x\r\n", D_AUTHENTICATION_SUITE_OUI_0, 
										         D_AUTHENTICATION_SUITE_OUI_1, 
										         D_AUTHENTICATION_SUITE_OUI_2);
			printf("\t\t\ttype ...................... 0x%02x\r\n", D_AUTHENTICATION_SUITE_TYPE);				
		}
		printf("\t\tCapabilities ...................... 0x%04x\r\n", this -> rsnCapabilities);
	}
}

void IeeeHeader::printReaconCode() {
	printf("\tReason code: ................ 0x%04x\r\n", this -> reasonCode);
}

void IeeeHeader::printListenInterval() {
	printf("\tListen interval: ............ 0x%04x\r\n", this -> listenInterval);
}

void IeeeHeader::printCurrentAPaddress() {
	printf("\tCurrent ap address: ......... %s\r\n", this -> addr4s);
}

void IeeeHeader::printStatusCode() {
	printf("\tStatus code: ................ 0x%04x\r\n", this -> statusCode);
}

void IeeeHeader::printAssociationId() {
	printf("\tAssociation id: ............. 0x%04x\r\n", this -> associationId);
}


void IeeeHeader::printAuthenticationAlgorithmNumber() {
	printf("\tAuthentication algorithm number: 0x%04x\r\n", this -> authenticationAlgorithmNumber);
}

void IeeeHeader::printAuthenticationSequenceNumber() {
	printf("\tAuthentication sequence number: 0x%04x\r\n", this -> authenticationSequenceNumber);
}

void IeeeHeader::printChallengeText() {
	printf("\tChallenge text: ............. 0x%s\r\n", this -> challengeText);
}

void IeeeHeader::printQos() {
	printf("\tQos ......................... 0x%04x\r\n", this -> qos);
}

void IeeeHeader::printAddress1() {
	printf("\tAddress1: ................... %s\r\n", this -> addr1s);
}

void IeeeHeader::printAddress2() {
	printf("\tAddress2: ................... %s\r\n", this -> addr2s);
}

void IeeeHeader::printAddress3() {
	printf("\tAddress3: ................... %s\r\n", this -> addr3s);
}

void IeeeHeader::printAddress4() {
	printf("\tAddress4: ................... %s\r\n", this -> addr4s);
}
/*
void IeeeHeader::print() {
}
*/

/* IeeeHeader::printDATA
 * Popis: funkce provede zobrazeni typickych casti pro DATovy ramec
 * Parametry: -
 * Navratova hodnota: -
 */
void IeeeHeader::printDATA() {
	this -> printDuration();
	this -> printAddress1();
	this -> printAddress2();
	this -> printAddress3();
	this -> printSeqCtl();	
	if(this -> getToDS() == 1 && this -> getFromDS() == 1) {
		this -> printAddress4();	
	}
	if(this -> getSubType() == ST_QOS_DATA) {
		this -> printQos();
	}
}

/* IeeeHeader::printRTS
 * Popis: funkce provede zobrazeni typickych casti pro RTS ramec
 * Parametry: -
 * Navratova hodnota: -
 */
void IeeeHeader::printRTS() {
	this -> printDuration();
	this -> printReceiverAddress();
	this -> printTransmitterAddress();
}

/* IeeeHeader::printCTS
 * Popis: funkce provede zobrazeni typickych casti pro CTS ramec
 * Parametry: -
 * Navratova hodnota: -
 */
void IeeeHeader::printCTS() {
	this -> printDuration();
	this -> printReceiverAddress();
}

/* IeeeHeader::printACK
 * Popis: funkce provede zobrazeni typickych casti pro ACK ramec
 * Parametry: -
 * Navratova hodnota: -
 */
void IeeeHeader::printACK() {
	this -> printDuration();
	this -> printReceiverAddress();
}

/* IeeeHeader::printPSPOLL
 * Popis: funkce provede zobrazeni typickych casti pro PS-POLL ramec
 * Parametry: -
 * Navratova hodnota: -
 */
void IeeeHeader::printPSPOLL() {
	this -> printAID();
	this -> printBSSID();
	this -> printTransmitterAddress(); 
}

/* IeeeHeader::printBEACON
 * Popis: funkce provede zobrazeni typickych casti pro BEACON ramec
 * Parametry: -
 * Navratova hodnota: -
 */
void IeeeHeader::printBEACON() {
	this -> printDuration();
	this -> printDA();
	this -> printSA();
	this -> printBSSID();
	this -> printSeqCtl();	
	this -> printTimestamp();
	this -> printBeaconInterval();
	this -> printCapabilityInfo();
	this -> printSSID();
	this -> printFHparameterSet();
	this -> printDSparameterSet();
	this -> printCFparameterSet();
	this -> printIBSSparameterSet();
	this -> printTrafficIndicationMap();
	this -> printPowerConstraint();
	this -> printCountry();
	this -> printChannelSwitch();
	this -> printQuiet();
	this -> printTPCreport();
	this -> printERPinformation();
	this -> printSupportedRate();
	this -> printExtendedSupportedRate();
	this -> printRSN();
}

/* IeeeHeader::printPROBEREQUEST
 * Popis: funkce provede zobrazeni typickych casti pro PROBE REQUEST ramec
 * Parametry: -
 * Navratova hodnota: -
 */
void IeeeHeader::printPROBEREQUEST() {
	this -> printDuration();
	this -> printDA();
	this -> printSA();
	this -> printBSSID();
	this -> printSeqCtl();
	this -> printSSID();
	this -> printSupportedRate();
	this -> printExtendedSupportedRate();		
}

/* IeeeHeader::printPROBERESPONSE
 * Popis: funkce provede zobrazeni typickych casti pro PROBE RESPONSE ramec
 * Parametry: -
 * Navratova hodnota: -
 */
void IeeeHeader::printPROBERESPONSE() {
	this -> printDuration();
	this -> printDA();
	this -> printSA();
	this -> printBSSID();
	this -> printSeqCtl();
	this -> printTimestamp();
	this -> printBeaconInterval();
	this -> printCapabilityInfo();
	this -> printSSID();
	this -> printSupportedRate();
	this -> printFHparameterSet();
	this -> printDSparameterSet();
	this -> printCFparameterSet();
	this -> printIBSSparameterSet();	
	this -> printCountry();
	this -> printPowerConstraint();
	this -> printQuiet();
	this -> printTPCreport();
	this -> printERPinformation();
	this -> printExtendedSupportedRate();
	this -> printRSN();	
}

/* IeeeHeader::printATIM
 * Popis: funkce provede zobrazeni typickych casti pro ATIM ramec
 * Parametry: -
 * Navratova hodnota: -
 */
void IeeeHeader::printATIM() {
	this -> printDuration();
	this -> printDA();
	this -> printSA();
	this -> printBSSID();
	this -> printSeqCtl();
}

/* IeeeHeader::printDEAUTHETICATION
 * Popis: funkce provede zobrazeni typickych casti pro DEAUTHETICATION ramec
 * Parametry: -
 * Navratova hodnota: -
 */
void IeeeHeader::printDEAUTHETICATION() {
	this -> printDuration();
	this -> printDA();
	this -> printSA();
	this -> printBSSID();
	this -> printSeqCtl();
	this -> printReaconCode();
}

/* IeeeHeader::printDISASSOCIATION
 * Popis: funkce provede zobrazeni typickych casti pro DISASSOCIATION ramec
 * Parametry: -
 * Navratova hodnota: -
 */
void IeeeHeader::printDISASSOCIATION() {
	this -> printDuration();
	this -> printDA();
	this -> printSA();
	this -> printBSSID();
	this -> printSeqCtl();
	this -> printReaconCode();
}

/* IeeeHeader::printASSOCIATIONREQUEST
 * Popis: funkce provede zobrazeni typickych casti pro ASSOCIATION REQUEST ramec
 * Parametry: -
 * Navratova hodnota: -
 */
void IeeeHeader::printASSOCIATIONREQUEST() {
	this -> printDuration();
	this -> printDA();
	this -> printSA();
	this -> printBSSID();
	this -> printSeqCtl();
	this -> printCapabilityInfo();
	this -> printListenInterval();
	this -> printSSID();
	this -> printSupportedRate();
}

/* IeeeHeader::printREASSOCIATIONREQUEST
 * Popis: funkce provede zobrazeni typickych casti pro REASSOCIATION REQUEST ramec
 * Parametry: -
 * Navratova hodnota: -
 */
void IeeeHeader::printREASSOCIATIONREQUEST() {
	this -> printDuration();
	this -> printDA();
	this -> printSA();
	this -> printBSSID();
	this -> printSeqCtl();
	this -> printCapabilityInfo();
	this -> printListenInterval();
	this -> printCurrentAPaddress();
	this -> printSSID();
	this -> printSupportedRate();
}

/* IeeeHeader::printREASSOCIATIONRESPONSE
 * Popis: funkce provede zobrazeni typickych casti pro REASSOCIATION RESPONSE ramec
 * Parametry: -
 * Navratova hodnota: -
 */
void IeeeHeader::printREASSOCIATIONRESPONSE() {
	this -> taggedParameters [TP_SUPPORTED_RATE] = true;
	this -> printDuration();
	this -> printDA();
	this -> printSA();
	this -> printBSSID();
	this -> printSeqCtl();
	this -> printCapabilityInfo();
	this -> printStatusCode();
	this -> printAssociationId();
	this -> printSupportedRate();
}

/* IeeeHeader::printAUTHENTICATION
 * Popis: funkce provede zobrazeni typickych casti pro AUTHENTICATION ramec
 * Parametry: -
 * Navratova hodnota: -
 */
void IeeeHeader::printAUTHENTICATION() {
	this -> printDuration();
	this -> printDA();
	this -> printSA();
	this -> printBSSID();
	this -> printAuthenticationAlgorithmNumber();
	this -> printAuthenticationSequenceNumber();
	this -> printStatusCode();
	this -> printChallengeText();
	this -> printSeqCtl();
}

/* IeeeHeader::deleteHeader
 * Popis: funkce provede odstraneni hodnot hlavicky
 * Parametry: -
 * Navratova hodnota: -
 */
void IeeeHeader::deleteHeader() {
	for(int i = 0; i < TP_COUNT_TAGGED_PARAMETERS; i++) {
		this -> taggedParameters[i] = false;
	}
	this -> delFrameControl();		
	this -> delDuration();
	this -> addr1 = D_ADDR1;
	this -> addr2 = D_ADDR2;
	this -> addr3 = D_ADDR3;
	this -> addr4 = D_ADDR4;
	strcpy(this -> addr1s, D_ADDR1S);
	strcpy(this -> addr2s, D_ADDR2S);
	strcpy(this -> addr3s, D_ADDR3S);
	strcpy(this -> addr4s, D_ADDR4S);
	this -> delSeqCtl();
	this -> delTimestamp();
	this -> delBeaconInterval();
	this -> delCapabilityInfo();
	this -> delSSID();
	this -> delFHparameterSet();
	this -> delDSparameterSet();
	this -> delIBSSparameterSet();
	this -> delTIM();
	this -> delLocalPowerConstraint();
	this -> delCountry();
	this -> delChannelSwitch();
	this -> delQuiet();
	this -> delTPCReport();
	this -> delERP();	
	this -> delSupportedRateAll();
	this -> delExtendedSupportedRateAll();
	this -> delReasonCode();
	this -> delListenInterval();
	this -> delCFP();
	this -> delRSN();
	this -> delStatusCode();
	this -> delAssociationId();
	this -> delAuthenticationAlgorithmNumber();
	this -> delAuthenticationSequenceNumber();
	this -> delChallengeText();
	this -> delQos();
} 

/* IeeeHeader::stringToMac
 * Popis: funkce prevede textovy retezec obsahujici mac adresu na int
 * Parametry: s - retezec s mac adresou
 * Navratova hodnota: hodnota adresy v int
 */
uint64_t IeeeHeader::stringToMac(const char * s) {
    unsigned char a[6];
    int last = -1;
    int rc = sscanf(s, "%hhx:%hhx:%hhx:%hhx:%hhx:%hhx%n", a + 0, a + 1, a + 2, a + 3, a + 4, a + 5, &last);
    return
        uint64_t(a[0])       |
        uint64_t(a[1]) <<  8 |
        uint64_t(a[2]) << 16 |
        uint64_t(a[3]) << 24 |
        uint64_t(a[4]) << 32 |
        uint64_t(a[5]) << 40 ;
}

/* IeeeHeader::isSetAnyCountryTriplet
 * Popis: funkce zjisti, zdali je neco nastaveno v CountryTripletu
 * Parametry: -
 * Navratova hodnota: inforamce, zdali je nejaky triplet nastaven
 */
bool IeeeHeader::isSetAnyCountryTriplet() {
	for(int i = 0; i < L_COUNTRY_IE_LENGTH; i++) {
		if( this -> countryConstraintTripletFlag[i] == true) {
			return true;
		}
	}
	return false;
}

/* IeeeHeader::isSetAnySupportedRate
 * Popis: funkce zjisti, zdali je neco nastaveno v supported rate
 * Parametry: -
 * Navratova hodnota: inforamce, zdali je nejaky supported rate nastaven
 */
bool IeeeHeader::isSetAnySupportedRate() {
	for(int i = 0; i < L_SUPPORTED_RATE_LENGTH; i++) {
		if( this -> supportedRateFlag[i] == true) {
			return true;
		}
	}
	return false;
}

/* IeeeHeader::isSetAnyExtendedSupportedRate
 * Popis: funkce zjisti, zdali je neco nastaveno v extended supported rate
 * Parametry: -
 * Navratova hodnota: inforamce, zdali je nejaky extended supported rate nastaven
 */
bool IeeeHeader::isSetAnyExtendedSupportedRate() {
	for(int i = 0; i < L_EXTENDED_SUPPORTED_RATE_LENGTH; i++) {
		if( this -> extendedSupportedRateFlag[i] == true) {
			return true;
		}
	}
	return false;
}

/* IeeeHeader::saveHeader
 * Popis: funkce provede ulozeni hlavicky do xml souboru
 * Parametry: -
 * Navratova hodnota: objekt, tvorici strom xml souboru
 */
TiXmlElement * IeeeHeader::saveHeader() {
	TiXmlElement * ieeeHeaderElement = new TiXmlElement( "ieeeHeader" );

	TiXmlElement * taggedParametersSetElement = new TiXmlElement( "taggedParametersSet" );
	char pom [200];
	for(int i = 0; i < TP_COUNT_TAGGED_PARAMETERS; i++) {
		sprintf(pom, "taggedParameter%d", i);
		TiXmlElement * taggedParametersElement = new TiXmlElement( pom );
		taggedParametersElement -> SetAttribute("value", this -> taggedParameters[i] );
		taggedParametersSetElement -> LinkEndChild( taggedParametersElement ); 
	}
	ieeeHeaderElement -> LinkEndChild( taggedParametersSetElement );
	
	TiXmlElement * frameControlElement = new TiXmlElement( "frameControl" );
	frameControlElement -> SetAttribute("value", this -> frameControl );
	ieeeHeaderElement -> LinkEndChild( frameControlElement ); 

	TiXmlElement * durationElement = new TiXmlElement( "duration" );
	durationElement -> SetAttribute("value", this -> duration );
	ieeeHeaderElement -> LinkEndChild( durationElement ); 

	TiXmlElement * addr1sElement = new TiXmlElement( "addr1s" );
	addr1sElement -> LinkEndChild(new TiXmlText((char *)this -> addr1s));
	ieeeHeaderElement -> LinkEndChild( addr1sElement ); 

	TiXmlElement * addr2sElement = new TiXmlElement( "addr2s" );
	addr2sElement -> LinkEndChild(new TiXmlText((char *)this -> addr2s));
	ieeeHeaderElement -> LinkEndChild( addr2sElement ); 

	TiXmlElement * addr3sElement = new TiXmlElement( "addr3s" );
	addr3sElement -> LinkEndChild(new TiXmlText((char *)this -> addr3s));
	ieeeHeaderElement -> LinkEndChild( addr3sElement ); 

	TiXmlElement * addr4sElement = new TiXmlElement( "addr4s" );
	addr4sElement -> LinkEndChild(new TiXmlText((char *)this -> addr4s));
	ieeeHeaderElement -> LinkEndChild( addr4sElement ); 

	TiXmlElement * aidElement = new TiXmlElement( "aid" );
	aidElement -> SetAttribute("value", this -> aid );
	ieeeHeaderElement -> LinkEndChild( aidElement ); 

	TiXmlElement * seqCtlElement = new TiXmlElement( "seqCtl" );
	seqCtlElement -> SetAttribute("value", this -> seqCtl );
	ieeeHeaderElement -> LinkEndChild( seqCtlElement ); 

	//TiXmlElement * timestampElement = new TiXmlElement( "timestamp" );
	//timestampElement -> SetAttribute("value", this -> timestamp );
	//ieeeHeaderElement -> LinkEndChild( timestampElement );

	sprintf(pom, "%lld", this -> timestamp);
	TiXmlElement * timestampElement = new TiXmlElement( "timestamp" );
	timestampElement -> LinkEndChild(new TiXmlText(pom));
	ieeeHeaderElement -> LinkEndChild( timestampElement );  

	TiXmlElement * beaconIntervalElement = new TiXmlElement( "beaconInterval" );
	beaconIntervalElement -> SetAttribute("value", this -> beaconInterval );
	ieeeHeaderElement -> LinkEndChild( beaconIntervalElement ); 

	TiXmlElement * capabilityInfoElement = new TiXmlElement( "capabilityInfo" );
	capabilityInfoElement -> SetAttribute("value", this -> capabilityInfo );
	ieeeHeaderElement -> LinkEndChild( capabilityInfoElement ); 

	TiXmlElement * ssidElement = new TiXmlElement( "ssid" );
	ssidElement -> LinkEndChild(new TiXmlText((char *)this -> ssid));
	ieeeHeaderElement -> LinkEndChild( ssidElement ); 

	TiXmlElement * fhParameterSetLengthElement = new TiXmlElement( "fhParameterSetLength" );
	fhParameterSetLengthElement -> SetAttribute("value", this -> fhParameterSetLength );
	ieeeHeaderElement -> LinkEndChild( fhParameterSetLengthElement ); 

	TiXmlElement * fhParameterSetDwellTimeElement = new TiXmlElement( "fhParameterSetDwellTime" );
	fhParameterSetDwellTimeElement -> SetAttribute("value", this -> fhParameterSetDwellTime );
	ieeeHeaderElement -> LinkEndChild( fhParameterSetDwellTimeElement ); 

	TiXmlElement * fhParameterSetHopSetElement = new TiXmlElement( "fhParameterSetHopSet" );
	fhParameterSetHopSetElement -> SetAttribute("value", this -> fhParameterSetHopSet );
	ieeeHeaderElement -> LinkEndChild( fhParameterSetHopSetElement ); 

	TiXmlElement * fhParameterSetHopPatternElement = new TiXmlElement( "fhParameterSetHopPattern" );
	fhParameterSetHopPatternElement -> SetAttribute("value", this -> fhParameterSetHopPattern );
	ieeeHeaderElement -> LinkEndChild( fhParameterSetHopPatternElement ); 

	TiXmlElement * fhParameterSetHopIndexElement = new TiXmlElement( "fhParameterSetHopIndex" );
	fhParameterSetHopIndexElement -> SetAttribute("value", this -> fhParameterSetHopIndex );
	ieeeHeaderElement -> LinkEndChild( fhParameterSetHopIndexElement ); 

	TiXmlElement * dsParameterSetElement = new TiXmlElement( "dsParameterSet" );
	dsParameterSetElement -> SetAttribute("value", this -> dsParameterSet );
	ieeeHeaderElement -> LinkEndChild( dsParameterSetElement ); 

	TiXmlElement * cfParameterSetCountElement = new TiXmlElement( "cfParameterSetCount" );
	cfParameterSetCountElement -> SetAttribute("value", this -> cfpCount );
	ieeeHeaderElement -> LinkEndChild( cfParameterSetCountElement );

	TiXmlElement * cfParameterSetPeriodElement = new TiXmlElement( "cfParameterSetPeriod" );
	cfParameterSetPeriodElement -> SetAttribute("value", this -> cfpPeriod );
	ieeeHeaderElement -> LinkEndChild( cfParameterSetPeriodElement ); 

	TiXmlElement * cfParameterSetMaxDurationElement = new TiXmlElement( "cfParameterSetMaxDuration" );
	cfParameterSetMaxDurationElement -> SetAttribute("value", this -> cfpMaxDuration );
	ieeeHeaderElement -> LinkEndChild( cfParameterSetMaxDurationElement );

	TiXmlElement * cfParameterSetRemainingElement = new TiXmlElement( "cfParameterSetRemaining" );
	cfParameterSetRemainingElement -> SetAttribute("value", this -> cfpRemaining );
	ieeeHeaderElement -> LinkEndChild( cfParameterSetRemainingElement );

	TiXmlElement * ibssParameterSetElement = new TiXmlElement( "ibssParameterSet" );
	ibssParameterSetElement -> SetAttribute("value", this -> ibssParameterSet );
	ieeeHeaderElement -> LinkEndChild( ibssParameterSetElement ); 

	TiXmlElement * timDtimCountElement = new TiXmlElement( "timDtimCount" );
	timDtimCountElement -> SetAttribute("value", this -> timDtimCount );
	ieeeHeaderElement -> LinkEndChild( timDtimCountElement ); 

	TiXmlElement * timDtimPeriodElement = new TiXmlElement( "timDtimPeriod" );
	timDtimPeriodElement -> SetAttribute("value", this -> timDtimPeriod );
	ieeeHeaderElement -> LinkEndChild( timDtimPeriodElement ); 

	TiXmlElement * timBitmapControlElement = new TiXmlElement( "timBitmapControl" );
	timBitmapControlElement -> SetAttribute("value", this -> timBitmapControl );
	ieeeHeaderElement -> LinkEndChild( timBitmapControlElement ); 

	TiXmlElement * timPartialVirtualBitmapElement = new TiXmlElement( "timPartialVirtualBitmap" );
	timPartialVirtualBitmapElement -> LinkEndChild(new TiXmlText((char *)this -> timPartialVirtualBitmap));
	ieeeHeaderElement -> LinkEndChild( timPartialVirtualBitmapElement ); 

	TiXmlElement * countryStringElement = new TiXmlElement( "countryString" );
	countryStringElement -> LinkEndChild(new TiXmlText((char *)this -> countryString));
	ieeeHeaderElement -> LinkEndChild( countryStringElement ); 

	TiXmlElement * countryConstraintTripletSetElement = new TiXmlElement( "countryConstraintTripletSet" );
	for(int i = 0; i < L_COUNTRY_IE_LENGTH ; i++) {
		if( this -> countryConstraintTripletFlag[i] == true ) {
			sprintf(pom, "countryConstraintTriplet%d", i);
			TiXmlElement * countryConstraintTripletElement = new TiXmlElement( pom );
			countryConstraintTripletElement -> SetAttribute("index", i);
			countryConstraintTripletElement -> SetAttribute("firstChannelNumber", this -> countryConstraintTriplet[i].firstChannelNumber);
			countryConstraintTripletElement -> SetAttribute("numberOfChannel", this -> countryConstraintTriplet[i].numberOfChannel);
			countryConstraintTripletElement -> SetAttribute("maxTransmitPower", this -> countryConstraintTriplet[i].maxTransmitPower);
			countryConstraintTripletSetElement -> LinkEndChild( countryConstraintTripletElement );
		}
	}
	ieeeHeaderElement -> LinkEndChild( countryConstraintTripletSetElement );

	TiXmlElement * localPowerConstraintElement = new TiXmlElement( "localPowerConstraint" );
	localPowerConstraintElement -> SetAttribute("value", this -> localPowerConstraint );
	ieeeHeaderElement -> LinkEndChild( localPowerConstraintElement ); 

	TiXmlElement * channelSwitchModeElement = new TiXmlElement( "channelSwitchMode" );
	channelSwitchModeElement -> SetAttribute("value", this -> channelSwitchMode );
	ieeeHeaderElement -> LinkEndChild( channelSwitchModeElement ); 

	TiXmlElement * newChannelNumberElement = new TiXmlElement( "newChannelNumber" );
	newChannelNumberElement -> SetAttribute("value", this -> newChannelNumber );
	ieeeHeaderElement -> LinkEndChild( newChannelNumberElement ); 

	TiXmlElement * channelSwitchCountElement = new TiXmlElement( "channelSwitchCount" );
	channelSwitchCountElement -> SetAttribute("value", this -> channelSwitchCount );
	ieeeHeaderElement -> LinkEndChild( channelSwitchCountElement ); 

	TiXmlElement * quietCountElement = new TiXmlElement( "quietCount" );
	quietCountElement -> SetAttribute("value", this -> quietCount );
	ieeeHeaderElement -> LinkEndChild( quietCountElement ); 

	TiXmlElement * quietPeriodElement = new TiXmlElement( "quietPeriod" );
	quietPeriodElement -> SetAttribute("value", this -> quietPeriod );
	ieeeHeaderElement -> LinkEndChild( quietPeriodElement ); 

	TiXmlElement * quietDurationElement = new TiXmlElement( "quietDuration" );
	quietDurationElement -> SetAttribute("value", this -> quietDuration );
	ieeeHeaderElement -> LinkEndChild( quietDurationElement ); 

	TiXmlElement * quietOffsetElement = new TiXmlElement( "quietOffset" );
	quietOffsetElement -> SetAttribute("value", this -> quietOffset );
	ieeeHeaderElement -> LinkEndChild( quietOffsetElement );

	TiXmlElement * transmitPowerElement = new TiXmlElement( "transmitPower" );
	transmitPowerElement -> SetAttribute("value", this -> transmitPower );
	ieeeHeaderElement -> LinkEndChild( transmitPowerElement );

	TiXmlElement * linkMarginElement = new TiXmlElement( "linkMargin" );
	linkMarginElement -> SetAttribute("value", this -> linkMargin );
	ieeeHeaderElement -> LinkEndChild( linkMarginElement );

	TiXmlElement * erpElement = new TiXmlElement( "erp" );
	erpElement -> SetAttribute("value", this -> erp );
	ieeeHeaderElement -> LinkEndChild( erpElement );

	TiXmlElement * supportedRateSetElement = new TiXmlElement( "supportedRateSet" );
	for(int i = 0; i < L_SUPPORTED_RATE_LENGTH ; i++) {
		if( this -> supportedRateFlag[i] == true ) {
			sprintf(pom, "supportedRate%d", i);
			TiXmlElement * supportedRateElement = new TiXmlElement( pom );
			supportedRateElement -> SetAttribute("index", i);
			supportedRateElement -> SetAttribute("supportedRate", this -> supportedRate[i]);
			supportedRateSetElement -> LinkEndChild( supportedRateElement );
		}
	}
	ieeeHeaderElement -> LinkEndChild( supportedRateSetElement );

	TiXmlElement * extendedSupportedRateSetElement = new TiXmlElement( "extendedSupportedRateSet" );
	for(int i = 0; i < L_EXTENDED_SUPPORTED_RATE_LENGTH ; i++) {
		if( this -> extendedSupportedRateFlag[i] == true ) {
			sprintf(pom, "extendedSupportedRate%d", i);
			TiXmlElement * extendedSupportedRateElement = new TiXmlElement( pom );
			extendedSupportedRateElement -> SetAttribute("index", i);
			extendedSupportedRateElement -> SetAttribute("extendedSupportedRate", this -> extendedSupportedRate[i]);
			extendedSupportedRateSetElement -> LinkEndChild( extendedSupportedRateElement );
		}
	}
	ieeeHeaderElement -> LinkEndChild( extendedSupportedRateSetElement );

	TiXmlElement * reasonCodeElement = new TiXmlElement( "reasonCode" );
	reasonCodeElement -> SetAttribute("value", this -> reasonCode );
	ieeeHeaderElement -> LinkEndChild( reasonCodeElement ); 

	TiXmlElement * listenIntervalElement = new TiXmlElement( "listenInterval" );
	listenIntervalElement -> SetAttribute("value", this -> listenInterval );
	ieeeHeaderElement -> LinkEndChild( listenIntervalElement ); 

	TiXmlElement * rsnVersionElement = new TiXmlElement( "rsnVersion" );
	rsnVersionElement -> SetAttribute("value", this -> rsnVersion );
	ieeeHeaderElement -> LinkEndChild( rsnVersionElement ); 

	TiXmlElement * groupCipherSuiteElement = new TiXmlElement( "groupCipherSuite" );
	TiXmlElement * groupCipherSuiteOUIElement = new TiXmlElement( "oui" );
	sprintf(pom, "%02x-%02x-%02x", this -> groupCipherSuite.suiteOUI[0], this -> groupCipherSuite.suiteOUI[1], this -> groupCipherSuite.suiteOUI[2]);
	groupCipherSuiteOUIElement -> LinkEndChild(new TiXmlText((char *)pom));
	groupCipherSuiteElement -> LinkEndChild( groupCipherSuiteOUIElement ); 
	TiXmlElement * groupCipherSuiteTypeElement = new TiXmlElement( "type" );
	groupCipherSuiteTypeElement -> SetAttribute("value", this -> groupCipherSuite.suiteType );
	groupCipherSuiteElement -> LinkEndChild( groupCipherSuiteTypeElement );
	ieeeHeaderElement -> LinkEndChild( groupCipherSuiteElement );


	TiXmlElement * pairwiseCipherSuiteElement = new TiXmlElement( "pairwiseCipherSuite" );
	for(int i = 0; i < L_PAIRWISE_CIPHER_SUITE_COUNT; i++) {
		if(this -> pairwiseCipherSuiteItem[i]) {
			sprintf(pom, "pairwiseCipherSuiteItem%d", i);
			TiXmlElement * pairwiseCipherSuiteItemElement = new TiXmlElement( pom );
				TiXmlElement * pairwiseCipherSuiteOUIElement = new TiXmlElement( "oui" );
				sprintf(pom, "%02x-%02x-%02x", this -> pairwiseCipherSuite[i].suiteOUI[0], this -> pairwiseCipherSuite[i].suiteOUI[1],
							       this -> pairwiseCipherSuite[i].suiteOUI[2]);
				pairwiseCipherSuiteOUIElement -> LinkEndChild(new TiXmlText((char *)pom));
				pairwiseCipherSuiteItemElement -> LinkEndChild( pairwiseCipherSuiteOUIElement ); 
				TiXmlElement * pairwiseCipherSuiteTypeElement = new TiXmlElement( "type" );
				pairwiseCipherSuiteTypeElement -> SetAttribute("value", this -> pairwiseCipherSuite[i].suiteType );
				pairwiseCipherSuiteItemElement -> LinkEndChild( pairwiseCipherSuiteTypeElement );
			pairwiseCipherSuiteElement -> LinkEndChild( pairwiseCipherSuiteItemElement );
		}
	}		
	ieeeHeaderElement -> LinkEndChild( pairwiseCipherSuiteElement );

	TiXmlElement * authenticationSuiteElement = new TiXmlElement( "authenticationSuite" );
	for(int i = 0; i < L_AUTHENTICATION_SUITE_COUNT; i++) {
		if(this -> authenticationSuiteItem[i]) {
			sprintf(pom, "authenticationSuiteItem%d", i);
			TiXmlElement * authenticationSuiteItemElement = new TiXmlElement( pom );
				TiXmlElement * authenticationSuiteOUIElement = new TiXmlElement( "oui" );
				sprintf(pom, "%02x-%02x-%02x", this -> authenticationSuite[i].suiteOUI[0], this -> authenticationSuite[i].suiteOUI[1],
							       this -> authenticationSuite[i].suiteOUI[2]);
				authenticationSuiteOUIElement -> LinkEndChild(new TiXmlText((char *)pom));
				authenticationSuiteItemElement -> LinkEndChild( authenticationSuiteOUIElement ); 
				TiXmlElement * authenticationSuiteTypeElement = new TiXmlElement( "type" );
				authenticationSuiteTypeElement -> SetAttribute("value", this -> authenticationSuite[i].suiteType );
				authenticationSuiteItemElement -> LinkEndChild( authenticationSuiteTypeElement );
			authenticationSuiteElement -> LinkEndChild( authenticationSuiteItemElement );
		}
	}
	ieeeHeaderElement -> LinkEndChild( authenticationSuiteElement );

	TiXmlElement * rsnCapabilitiesElement = new TiXmlElement( "rsnCapabilities" );
	rsnCapabilitiesElement -> SetAttribute("value", this -> rsnCapabilities );
	ieeeHeaderElement -> LinkEndChild( rsnCapabilitiesElement ); 

	TiXmlElement * statusCodeElement = new TiXmlElement( "statusCode" );
	statusCodeElement -> SetAttribute("value", this -> statusCode );
	ieeeHeaderElement -> LinkEndChild( statusCodeElement ); 

	TiXmlElement * associationIdElement = new TiXmlElement( "associationId" );
	associationIdElement -> SetAttribute("value", this -> associationId );
	ieeeHeaderElement -> LinkEndChild( associationIdElement ); 

	TiXmlElement * authenticationAlgorithmNumberElement = new TiXmlElement( "authenticationAlgorithmNumber" );
	authenticationAlgorithmNumberElement -> SetAttribute("value", this -> authenticationAlgorithmNumber );
	ieeeHeaderElement -> LinkEndChild( authenticationAlgorithmNumberElement ); 

	TiXmlElement * authenticationSequenceNumberElement = new TiXmlElement( "authenticationSequenceNumber" );
	authenticationSequenceNumberElement -> SetAttribute("value", this -> authenticationSequenceNumber );
	ieeeHeaderElement -> LinkEndChild( authenticationSequenceNumberElement ); 
	
	TiXmlElement * challengeTextElement = new TiXmlElement( "challengeText" );
	challengeTextElement -> LinkEndChild(new TiXmlText((char *)this -> challengeText));
	ieeeHeaderElement -> LinkEndChild( challengeTextElement ); 

	TiXmlElement * qosElement = new TiXmlElement( "qos" );
	qosElement -> SetAttribute("value", this -> qos );
	ieeeHeaderElement -> LinkEndChild( qosElement );

	return ieeeHeaderElement;
}

/* IeeeHeader::loadHeader
 * Popis: funkce provede nacteni hlavicky z xml souboru
 * Parametry: element - objekt, tvorici strom xml souboru
 * Navratova hodnota: -
 */
void IeeeHeader::loadHeader(TiXmlElement * element) {
	this -> deleteHeader();
	char pomStr[200];
	int pom = 0;
	TiXmlHandle hIeeeHeaderElement(element);

	TiXmlElement * taggedParametersSetElement = hIeeeHeaderElement.FirstChild( "taggedParametersSet" ).Element();
	if(taggedParametersSetElement) {
		TiXmlHandle hTaggedParametersSetElement(taggedParametersSetElement);
		for(int i = 0; i < TP_COUNT_TAGGED_PARAMETERS; i++) {
			this -> taggedParameters[i] = false;
			sprintf(pomStr, "taggedParameter%d", i);
			TiXmlElement * taggedParametersElement = hTaggedParametersSetElement.FirstChild(pomStr).Element();
			if(taggedParametersElement) {
				taggedParametersElement -> QueryBoolAttribute("value", &this -> taggedParameters[i] );
			}
		}
	}

	TiXmlElement * frameControlElement = hIeeeHeaderElement.FirstChild( "frameControl" ).Element();
	if(frameControlElement) {frameControlElement -> QueryIntAttribute("value", &pom ); this -> frameControl = (uint16_t) pom; }

	TiXmlElement * durationElement = hIeeeHeaderElement.FirstChild( "duration" ).Element();
	if(durationElement) { durationElement -> QueryIntAttribute("value", &pom ); this -> duration = (uint16_t) pom; }

	TiXmlElement * addr1s = hIeeeHeaderElement.FirstChild( "addr1s" ).Element();
	if(addr1s) {
		if(addr1s -> GetText() != NULL) {
			this -> setAddr1((char *)addr1s -> GetText());			
		}
	}

	TiXmlElement * addr2s = hIeeeHeaderElement.FirstChild( "addr2s" ).Element();
	if(addr2s) {
		if(addr2s -> GetText() != NULL) {
			this -> setAddr2((char *)addr2s -> GetText());			
		}
	}

	TiXmlElement * addr3s = hIeeeHeaderElement.FirstChild( "addr3s" ).Element();
	if(addr3s) {
		if(addr3s -> GetText() != NULL) {
			this -> setAddr3((char *)addr3s -> GetText());			
		}
	}

	TiXmlElement * addr4s = hIeeeHeaderElement.FirstChild( "addr4s" ).Element();
	if(addr4s) {
		if(addr4s -> GetText() != NULL) {
			this -> setAddr4((char *)addr4s -> GetText());			
		}
	}

	TiXmlElement * aidElement = hIeeeHeaderElement.FirstChild( "aid" ).Element();
	if(aidElement) { aidElement -> QueryIntAttribute("value", &pom ); this -> aid = (uint16_t) pom; }

	TiXmlElement * seqCtlElement = hIeeeHeaderElement.FirstChild( "seqCtl" ).Element();
	if(seqCtlElement) { seqCtlElement -> QueryIntAttribute("value", &pom ); this -> seqCtl = (uint16_t) pom; }

	//TiXmlElement * timestampElement = hIeeeHeaderElement.FirstChild( "timestamp" ).Element();
	//if(timestampElement) { timestampElement -> QueryIntAttribute("value", &pom); this -> timestamp = pom; }

	TiXmlElement * timestampElement = hIeeeHeaderElement.FirstChild( "timestamp" ).Element();
	if(timestampElement) {
		if(timestampElement -> GetText() != NULL) {
			this -> timestamp = strtoll(timestampElement -> GetText(),NULL, 10);
		}
	}

	TiXmlElement * beaconIntervalElement = hIeeeHeaderElement.FirstChild( "beaconInterval" ).Element();
	if(beaconIntervalElement) { beaconIntervalElement -> QueryIntAttribute("value", &pom ); this -> beaconInterval = (uint16_t) pom; }

	TiXmlElement * capabilityInfoElement = hIeeeHeaderElement.FirstChild( "capabilityInfo" ).Element();
	if(capabilityInfoElement) { capabilityInfoElement -> QueryIntAttribute("value", &pom ); this -> capabilityInfo = (uint16_t) pom; }

	TiXmlElement * ssidElement = hIeeeHeaderElement.FirstChild( "ssid" ).Element();
	if(ssidElement && ssidElement -> GetText()) {
		int ssidLength = strlen(ssidElement -> GetText());
		if( ssidLength > L_SSID_MAX_LENGTH )
			ssidLength = L_SSID_MAX_LENGTH;	
		Utilities::memcpy8(this -> ssid, ssidElement -> GetText(), ssidLength, LITTLE_ENDIAN_ORDER);
		this -> ssid[ssidLength] = 0;
	}

	TiXmlElement * fhParameterSetLengthElement = hIeeeHeaderElement.FirstChild( "fhParameterSetLength" ).Element();
	if(fhParameterSetLengthElement) { fhParameterSetLengthElement -> QueryIntAttribute("value", &pom ); this -> fhParameterSetLength = (uint8_t) pom; }

	TiXmlElement * fhParameterSetDwellTimeElement = hIeeeHeaderElement.FirstChild( "fhParameterSetDwellTime" ).Element();
	if(fhParameterSetDwellTimeElement) { fhParameterSetDwellTimeElement -> QueryIntAttribute("value", &pom ); this -> fhParameterSetDwellTime = (uint16_t) pom; }

	TiXmlElement * fhParameterSetHopSetElement = hIeeeHeaderElement.FirstChild( "fhParameterSetHopSet" ).Element();
	if(fhParameterSetHopSetElement) { fhParameterSetHopSetElement -> QueryIntAttribute("value", &pom ); this -> fhParameterSetHopSet = (uint8_t) pom; } 

	TiXmlElement * fhParameterSetHopPatternElement = hIeeeHeaderElement.FirstChild( "fhParameterSetHopPattern" ).Element();
	if(fhParameterSetHopPatternElement) { fhParameterSetHopPatternElement -> QueryIntAttribute("value", &pom); this -> fhParameterSetHopPattern = (uint8_t) pom; }

	TiXmlElement * fhParameterSetHopIndexElement = hIeeeHeaderElement.FirstChild( "fhParameterSetHopIndex" ).Element();
	if(fhParameterSetHopIndexElement) { fhParameterSetHopIndexElement -> QueryIntAttribute("value", &pom ); this -> fhParameterSetHopIndex = (uint8_t) pom; }

	TiXmlElement * dsParameterSetElement = hIeeeHeaderElement.FirstChild( "dsParameterSet" ).Element();
	if(dsParameterSetElement) { dsParameterSetElement -> QueryIntAttribute("value", &pom ); this -> dsParameterSet = (uint8_t) pom; }

	TiXmlElement * cfParameterSetCountElement = hIeeeHeaderElement.FirstChild( "dsParameterSetCount" ).Element();
	if(cfParameterSetCountElement) { cfParameterSetCountElement -> QueryIntAttribute("value", &pom ); this -> cfpCount = (uint8_t) pom; }

	TiXmlElement * cfParameterSetPeriodElement = hIeeeHeaderElement.FirstChild( "dsParameterSetPeriod" ).Element();
	if(cfParameterSetPeriodElement) { cfParameterSetPeriodElement -> QueryIntAttribute("value", &pom ); this -> cfpPeriod = (uint8_t) pom; }

	TiXmlElement * cfParameterSetMaxDurationElement = hIeeeHeaderElement.FirstChild( "dsParameterSetMaxDuration" ).Element();
	if(cfParameterSetMaxDurationElement) { cfParameterSetMaxDurationElement -> QueryIntAttribute("value", &pom ); this -> cfpMaxDuration = (uint16_t) pom; }

	TiXmlElement * cfParameterSetRemainingElement = hIeeeHeaderElement.FirstChild( "dsParameterSetRemaining" ).Element();
	if(cfParameterSetRemainingElement) { cfParameterSetRemainingElement -> QueryIntAttribute("value", &pom ); this -> cfpRemaining = (uint16_t) pom; }

	TiXmlElement * ibssParameterSetElement = hIeeeHeaderElement.FirstChild( "ibssParameterSet" ).Element();
	if(ibssParameterSetElement) { ibssParameterSetElement -> QueryIntAttribute("value", &pom ); this -> ibssParameterSet = (uint16_t)pom; }

	TiXmlElement * timDtimCountElement = hIeeeHeaderElement.FirstChild( "timDtimCount" ).Element();
	if(timDtimCountElement) { timDtimCountElement -> QueryIntAttribute("value", &pom ); this -> timDtimCount = (uint8_t) pom; }

	TiXmlElement * timDtimPeriodElement = hIeeeHeaderElement.FirstChild( "timDtimPeriod" ).Element();
	if(timDtimPeriodElement) { timDtimPeriodElement -> QueryIntAttribute("value", &pom ); this -> timDtimPeriod = (uint8_t) pom; }

	TiXmlElement * timBitmapControlElement = hIeeeHeaderElement.FirstChild( "timBitmapControl" ).Element();
	if(timBitmapControlElement) { timBitmapControlElement -> QueryIntAttribute("value", &pom ); this -> timBitmapControl = (uint8_t) pom; }

	TiXmlElement * timPartialVirtualBitmapElement = hIeeeHeaderElement.FirstChild( "timPartialVirtualBitmap" ).Element();
	if(timPartialVirtualBitmapElement && timPartialVirtualBitmapElement -> GetText()) {
		int timPartialVirtualBitmapLength = strlen(timPartialVirtualBitmapElement -> GetText());
		if( timPartialVirtualBitmapLength > L_TIM_PARTIAL_VIRTUAL_BITMAP_LENGTH )
			timPartialVirtualBitmapLength = L_TIM_PARTIAL_VIRTUAL_BITMAP_LENGTH;	
		Utilities::memcpy8(this -> timPartialVirtualBitmap, timPartialVirtualBitmapElement -> GetText(), timPartialVirtualBitmapLength, LITTLE_ENDIAN_ORDER);
		this -> timPartialVirtualBitmap[timPartialVirtualBitmapLength] = 0;
	}

	TiXmlElement * countryStringElement = hIeeeHeaderElement.FirstChild( "countryString" ).Element();
	if(countryStringElement && countryStringElement -> GetText()) {
		int countryStringLength = strlen(countryStringElement -> GetText());
		if( countryStringLength > L_COUNTRY_STRING_LENGTH )
			countryStringLength = L_COUNTRY_STRING_LENGTH;	
		Utilities::memcpy8(this -> countryString, countryStringElement -> GetText(), countryStringLength, LITTLE_ENDIAN_ORDER);
		this -> countryString[countryStringLength] = 0;
	}

	TiXmlElement * countryConstraintTripletSetElement = hIeeeHeaderElement.FirstChild( "countryConstraintTripletSet" ).Element();
	if(countryConstraintTripletSetElement) {
		TiXmlHandle hcountryConstraintTripletSetElement(countryConstraintTripletSetElement);
		for(int i = 0; i < L_COUNTRY_IE_LENGTH ; i++) {
			sprintf(pomStr, "countryConstraintTriplet%d", i);
			TiXmlElement * countryConstraintTripletElement = hcountryConstraintTripletSetElement.FirstChild( pomStr ).Element();
			if( countryConstraintTripletElement ) {
				int pom;
				int index = 0;
				countryConstraintTripletElement -> QueryIntAttribute("index", &index );
				ciect a;
				countryConstraintTripletElement -> QueryIntAttribute("firstChannelNumber", &pom );
				a.firstChannelNumber = (uint8_t) pom;
				countryConstraintTripletElement -> QueryIntAttribute("numberOfChannel", &pom );
				a.numberOfChannel = (uint8_t) pom;
				countryConstraintTripletElement -> QueryIntAttribute("maxTransmitPower", &pom );
				a.maxTransmitPower = (uint8_t) pom;
				if(index >=0 && index < L_COUNTRY_IE_LENGTH) {
					this -> countryConstraintTriplet[index] = a;	
					this -> countryConstraintTripletFlag[i] = true; 
				}
			}
		}
	}

	TiXmlElement * localPowerConstraintElement = hIeeeHeaderElement.FirstChild( "localPowerConstraint" ).Element();
	if(localPowerConstraintElement) { localPowerConstraintElement -> QueryIntAttribute("value", &pom ); this -> localPowerConstraint = (uint8_t) pom; }

	TiXmlElement * channelSwitchModeElement = hIeeeHeaderElement.FirstChild( "channelSwitchMode" ).Element();
	if(channelSwitchModeElement) { channelSwitchModeElement -> QueryIntAttribute("value", &pom ); this -> channelSwitchMode = (uint8_t) pom; }

	TiXmlElement * newChannelNumberElement = hIeeeHeaderElement.FirstChild( "newChannelNumber" ).Element();
	if(newChannelNumberElement) { newChannelNumberElement -> QueryIntAttribute("value", &pom ); this -> newChannelNumber = (uint8_t) pom; }

	TiXmlElement * channelSwitchCountElement = hIeeeHeaderElement.FirstChild( "channelSwitchCount" ).Element();
	if(channelSwitchCountElement) { channelSwitchCountElement -> QueryIntAttribute("value", &pom ); this -> channelSwitchCount = (uint8_t) pom; }

	TiXmlElement * quietCountElement = hIeeeHeaderElement.FirstChild( "quietCount" ).Element();
	if(quietCountElement) { quietCountElement -> QueryIntAttribute("value", &pom ); this -> quietCount = (uint8_t) pom; }

	TiXmlElement * quietPeriodElement = hIeeeHeaderElement.FirstChild( "quietPeriod" ).Element();
	if(quietPeriodElement) { quietPeriodElement -> QueryIntAttribute("value", &pom ); this -> quietPeriod = (uint8_t) pom; }

	TiXmlElement * quietDurationElement = hIeeeHeaderElement.FirstChild( "quietDuration" ).Element();
	if(quietDurationElement) { quietDurationElement -> QueryIntAttribute("value", &pom ); this -> quietDuration = (uint16_t) pom; }

	TiXmlElement * quietOffsetElement = hIeeeHeaderElement.FirstChild( "quietOffset" ).Element();
	if(quietOffsetElement) { quietOffsetElement -> QueryIntAttribute("value", &pom );  this -> quietOffset = (uint16_t) pom; }

	TiXmlElement * transmitPowerElement = hIeeeHeaderElement.FirstChild( "transmitPower" ).Element();
	if(transmitPowerElement) { transmitPowerElement -> QueryIntAttribute("value", &pom ); this -> transmitPower = (uint8_t) pom; }

	TiXmlElement * linkMarginElement = hIeeeHeaderElement.FirstChild( "linkMargin" ).Element();
	if(linkMarginElement) { linkMarginElement -> QueryIntAttribute("value", &pom); this -> linkMargin = (uint8_t) pom; }
	
	TiXmlElement * erpElement = hIeeeHeaderElement.FirstChild( "erp" ).Element();
	if(erpElement) { erpElement -> QueryIntAttribute("value", &pom ); this -> erp = (uint8_t) pom; }

	TiXmlElement * supportedRateSetElement = hIeeeHeaderElement.FirstChild( "supportedRateSet" ).Element();
	if(supportedRateSetElement) {
		TiXmlHandle hsupportedRateSetElement(supportedRateSetElement);
		for(int i = 0; i < L_SUPPORTED_RATE_LENGTH ; i++) {
			sprintf(pomStr, "supportedRate%d", i);
			TiXmlElement * supportedRateElement = hsupportedRateSetElement.FirstChild( pomStr ).Element();
			if(supportedRateElement) {
				int index;
				int pom;
				supportedRateElement -> QueryIntAttribute("index", &index );
				supportedRateElement -> QueryIntAttribute("supportedRate", &pom );
				if(index >= 0 && index < L_SUPPORTED_RATE_LENGTH) {
					this -> supportedRate[index] = (uint8_t)pom;
					this -> supportedRateFlag[index] = true;
				}
			}
		}
	}

	TiXmlElement * extendedSupportedRateSetElement = hIeeeHeaderElement.FirstChild( "extendedSupportedRateSet" ).Element();
	if(extendedSupportedRateSetElement) {
		TiXmlHandle hextendedSupportedRateSetElement(extendedSupportedRateSetElement);
		for(int i = 0; i < L_EXTENDED_SUPPORTED_RATE_LENGTH ; i++) {
			sprintf(pomStr, "extendedSupportedRate%d", i);
			TiXmlElement * extendedSupportedRateElement = hextendedSupportedRateSetElement.FirstChild( pomStr ).Element();
			if(extendedSupportedRateElement) {
				int index;
				int pom;
				extendedSupportedRateElement -> QueryIntAttribute("index", &index );
				extendedSupportedRateElement -> QueryIntAttribute("extendedSupportedRate", &pom );
				if(index >= 0 && index < L_EXTENDED_SUPPORTED_RATE_LENGTH) {
					this -> extendedSupportedRate[index] = (uint8_t)pom;
					this -> extendedSupportedRateFlag[index] = true;
				}
			}
		}
	}

	TiXmlElement * reasonCodeElement = hIeeeHeaderElement.FirstChild( "reasonCode" ).Element();
	if(reasonCodeElement) { reasonCodeElement -> QueryIntAttribute("value", &pom );  this -> reasonCode = (uint16_t) pom; }

	TiXmlElement * listenIntervalElement = hIeeeHeaderElement.FirstChild( "listenInterval" ).Element();
	if(listenIntervalElement) { listenIntervalElement -> QueryIntAttribute("value", &pom );  this -> listenInterval = (uint16_t) pom; }

	TiXmlElement * rsnVersionElement = hIeeeHeaderElement.FirstChild( "rsnVersion" ).Element();
	if(rsnVersionElement) { rsnVersionElement -> QueryIntAttribute("value", &pom );  this -> rsnVersion = (uint8_t) pom; }

	TiXmlElement * groupCipherSuiteElement = hIeeeHeaderElement.FirstChild( "groupCipherSuite" ).Element();
	if(groupCipherSuiteElement) {
		TiXmlHandle hgroupCipherSuiteElement(groupCipherSuiteElement);
		TiXmlElement * groupCipherSuiteOUIElement = hgroupCipherSuiteElement.FirstChild( "oui" ).Element();
		if(groupCipherSuiteOUIElement) {
			if(groupCipherSuiteOUIElement -> GetText() != NULL) {
				this -> copyOUI(this -> groupCipherSuite.suiteOUI, (char *) groupCipherSuiteOUIElement -> GetText(), L_OUI_LENGTH);
			}
		}
		TiXmlElement * groupCipherSuiteTypeElement = hgroupCipherSuiteElement.FirstChild( "type" ).Element();
		if(groupCipherSuiteTypeElement) {
			int pom;
			groupCipherSuiteTypeElement -> QueryIntAttribute("value", &pom ); this -> groupCipherSuite.suiteType = (uint8_t) pom;
		}		
	}
	
	TiXmlElement * pairwiseCipherSuiteElement = hIeeeHeaderElement.FirstChild( "pairwiseCipherSuite" ).Element();
	if(pairwiseCipherSuiteElement) {
		TiXmlHandle hpairwiseCipherSuiteElement(pairwiseCipherSuiteElement);
		for(int i = 0; i < L_PAIRWISE_CIPHER_SUITE_COUNT; i++) {
			sprintf(pomStr, "pairwiseCipherSuiteItem%d", i );
			TiXmlElement * pairwiseCipherSuiteItemElement = hpairwiseCipherSuiteElement.FirstChild( pomStr ).Element();
			if(pairwiseCipherSuiteItemElement) {
				TiXmlHandle hpairwiseCipherSuiteItemElement(pairwiseCipherSuiteItemElement);
				TiXmlElement * pairwiseCipherSuiteOUIElement = hpairwiseCipherSuiteItemElement.FirstChild( "oui" ).Element();
				if(pairwiseCipherSuiteOUIElement) {
					this -> pairwiseCipherSuiteItem[i] = true;
					if(pairwiseCipherSuiteOUIElement -> GetText() != NULL) {
						this -> copyOUI(this -> pairwiseCipherSuite[i].suiteOUI, 
                                                                (char *) pairwiseCipherSuiteOUIElement -> GetText(), L_OUI_LENGTH);
					}
				}
				TiXmlElement * pairwiseCipherSuiteTypeElement = hpairwiseCipherSuiteItemElement.FirstChild( "type" ).Element();
				if(pairwiseCipherSuiteTypeElement) {
					this -> pairwiseCipherSuiteItem[i] = true;
					int pom;
					pairwiseCipherSuiteTypeElement -> QueryIntAttribute("value", &pom ); this -> pairwiseCipherSuite[i].suiteType = (uint8_t) pom;
				}
			}		
		}
	}

	TiXmlElement * authenticationSuiteElement = hIeeeHeaderElement.FirstChild( "authenticationSuite" ).Element();
	if(authenticationSuiteElement) {
		TiXmlHandle hauthenticationSuiteElement(authenticationSuiteElement);
		for(int i = 0; i < L_AUTHENTICATION_SUITE_COUNT; i++) {
			sprintf(pomStr, "authenticationSuiteItem%d", i );
			TiXmlElement * authenticationSuiteItemElement = hauthenticationSuiteElement.FirstChild( pomStr ).Element();
			if(authenticationSuiteItemElement) {
				TiXmlHandle hauthenticationSuiteItemElement(authenticationSuiteItemElement);
				TiXmlElement * authenticationSuiteOUIElement = hauthenticationSuiteItemElement.FirstChild( "oui" ).Element();
				if(authenticationSuiteOUIElement) {
					this -> authenticationSuiteItem[i] = true;
					if(authenticationSuiteOUIElement -> GetText() != NULL) {
						this -> copyOUI(this -> authenticationSuite[i].suiteOUI,
						(char *) authenticationSuiteOUIElement -> GetText(), L_OUI_LENGTH);
					}
				}
				TiXmlElement * authenticationSuiteTypeElement = hauthenticationSuiteItemElement.FirstChild( "type" ).Element();
				if(authenticationSuiteTypeElement) {
					this -> authenticationSuiteItem[i] = true;
					int pom;
					authenticationSuiteTypeElement -> QueryIntAttribute("value", &pom ); this -> authenticationSuite[i].suiteType = (uint8_t) pom;
				}		
			}
		}
	}

	TiXmlElement * rsnCapabilitiesElement = hIeeeHeaderElement.FirstChild( "rsnCapabilities" ).Element();
	if(rsnCapabilitiesElement) { rsnCapabilitiesElement -> QueryIntAttribute("value", &pom );  this -> rsnCapabilities = (uint8_t) pom; }

	TiXmlElement * statusCodeElement = hIeeeHeaderElement.FirstChild( "statusCode" ).Element();
	if(statusCodeElement) { statusCodeElement -> QueryIntAttribute("value", &pom );  this -> statusCode = (uint16_t) pom; }

	TiXmlElement * associationIdElement = hIeeeHeaderElement.FirstChild( "associationId" ).Element();
	if(associationIdElement) { associationIdElement -> QueryIntAttribute("value", &pom );  this -> associationId = (uint16_t) pom; }

	TiXmlElement * authenticationAlgorithmNumberElement = hIeeeHeaderElement.FirstChild( "authenticationAlgorithmNumber" ).Element();
	if(authenticationAlgorithmNumberElement) { authenticationAlgorithmNumberElement -> QueryIntAttribute("value", &pom );  
						   this -> authenticationAlgorithmNumber = (uint16_t) pom; }

	TiXmlElement * authenticationSequenceNumberElement = hIeeeHeaderElement.FirstChild( "authenticationSequenceNumber" ).Element();
	if(authenticationSequenceNumberElement) { authenticationSequenceNumberElement -> QueryIntAttribute("value", &pom ); 
						  this -> authenticationSequenceNumber = (uint16_t) pom; }
	
	TiXmlElement * challengeText = hIeeeHeaderElement.FirstChild( "challengeText" ).Element();
	if(challengeText) {
		if(challengeText -> GetText() != NULL) {
			strncpy(this -> challengeText, challengeText -> GetText(), L_CHALLENGE_TEXT_LENGTH);			
		}
	}

	TiXmlElement * qosElement = hIeeeHeaderElement.FirstChild( "qos" ).Element();
	if(qosElement) { qosElement -> QueryIntAttribute("value", &pom ); this -> qos = (uint16_t) pom; }
}

/* IeeeHeader::copyOUI
 * Popis: funkce provede nakopirovani OUI z retezce
 * Parametry: oui - misto, kde se bude ukladat oui retezec
 *            string - retezec, ze ktereho se nacita oui
 *	      lengt - delka nacitaneho oui retezce
 * Navratova hodnota: -
 */
void IeeeHeader::copyOUI(uint8_t * oui, char * string, int length) {
	char pom [3];
	pom[2] = 0;
	for(int i = 0; i < length; i++) {
		pom[0] = string[0];
		pom[1] = string[1];
		oui[i] = strtoll((char *)pom, NULL, 16);
		string = &string[3];
	}
}

/* IeeeHeader::parseIV
 * Popis: funkce vrati incializacni vektor
 * Parametry: int lengthFrame - delka ramce, ze ktereho budeme parsovat iv (bez radiotap header)
 *            uint8_t * frame - ramec, ze ktereho budeme parsovat iv (bez radiotap header)
 *            uint8_t * iv    - misto, kam ulozime nalezeny iv
 * Navratova hodnota: delka iv
 */
int IeeeHeader::parseIV(int lengthFrame, uint8_t * frame, uint8_t * iv) {
	int lengthIeeeHeader = 24;
	int positionKey      = 0;
	int lengthIV         = 0;
	//datovy ramec musi byt dlouhy minimalne 24 byte
	if(lengthFrame < lengthIeeeHeader) return 0;
	//overim typ ramce - pokud neni datovy nebo neobsahuje data, tak vracim nulu
	if(!((frame[0] & 0x08) == 0x08) || (frame[0] & 0x40) == 0x40) return 0;
	//pokud neni sifrovany, tak take vracim nulu
	if(!((frame[1] & 0x40) == 0x40)) return 0;
	//zjistim umisteni iv vektoru
	//pokud toDS = 1 a fromDS = 1 => obsahuje addr4 - 6 bytu
	if((frame[1] & 0x03) == 0x03) { lengthIeeeHeader += 6; }
	if(lengthFrame < lengthIeeeHeader) return 0;
	//pokud qosData - subType = 8 => obsahuje qos - 2 byte
	if((frame[0] & 0x80) == 0x80) { lengthIeeeHeader += 2; }
	if(lengthFrame < lengthIeeeHeader) return 0;
	//position key v ieee header
	positionKey = lengthIeeeHeader + 3;
	if(lengthFrame < positionKey) return 0;
	//ted musim zjistit jak je zasifrovan
	//rozlisime wep a tkip s ccmp
	if(!((frame[positionKey] & 0x20) == 0x20)) {
		//wep
		Utilities::memcpy8(iv    , &frame[lengthIeeeHeader + 2], 1, LITTLE_ENDIAN_ORDER);
		Utilities::memcpy8(iv + 1, &frame[lengthIeeeHeader + 1], 1, LITTLE_ENDIAN_ORDER);
		Utilities::memcpy8(iv + 2, &frame[lengthIeeeHeader    ], 1, LITTLE_ENDIAN_ORDER);
		lengthIV = 3;
	} else {
		if(lengthFrame < (lengthIeeeHeader + 8)) return 0;
		if(((frame[positionKey - 3] | 0x20) & 0x7f) == frame[positionKey - 2]) {
			//tkip
			Utilities::memcpy8(iv    ,&frame[lengthIeeeHeader + 7], 1, LITTLE_ENDIAN_ORDER);
			Utilities::memcpy8(iv + 1,&frame[lengthIeeeHeader + 6], 1, LITTLE_ENDIAN_ORDER);
			Utilities::memcpy8(iv + 2,&frame[lengthIeeeHeader + 5], 1, LITTLE_ENDIAN_ORDER);
			Utilities::memcpy8(iv + 3,&frame[lengthIeeeHeader + 4], 1, LITTLE_ENDIAN_ORDER);
			Utilities::memcpy8(iv + 4,&frame[lengthIeeeHeader    ], 1, LITTLE_ENDIAN_ORDER);
			Utilities::memcpy8(iv + 5,&frame[lengthIeeeHeader + 2], 1, LITTLE_ENDIAN_ORDER);
			lengthIV = 6;
		} else {
			//ccmp
			Utilities::memcpy8(iv    ,&frame[lengthIeeeHeader + 7], 1, LITTLE_ENDIAN_ORDER);
			Utilities::memcpy8(iv + 1,&frame[lengthIeeeHeader + 6], 1, LITTLE_ENDIAN_ORDER);
			Utilities::memcpy8(iv + 2,&frame[lengthIeeeHeader + 5], 1, LITTLE_ENDIAN_ORDER);
			Utilities::memcpy8(iv + 3,&frame[lengthIeeeHeader + 4], 1, LITTLE_ENDIAN_ORDER);
			Utilities::memcpy8(iv + 4,&frame[lengthIeeeHeader + 1], 1, LITTLE_ENDIAN_ORDER);
			Utilities::memcpy8(iv + 5,&frame[lengthIeeeHeader    ], 1, LITTLE_ENDIAN_ORDER);
			lengthIV = 6;
		}
	}
	//prevedu na string
	if(lengthIV != 0) {
		char pomIV [2*6+1];
		for(int i = 0; i < lengthIV; i++) {
			sprintf(&pomIV[2*i], "%02x", iv[i]);
		}
		pomIV[2*lengthIV] = 0;
		sprintf((char *)iv, "%s", pomIV);
	}
	return lengthIV;
}
