/* Soubor: Statement.h
 * Autor: Pavel Svanda
 * Email: svandap@gmail.com
 * Datum vytvoreni: 17.3.2013
 * Popis: deklarace abstraktni tridy Statement, ktera slouzi k odvozovani dalsich statementu
 */
#ifndef STATEMENT_H
#define STATEMENT_H

#include <stdio.h>
#include <sys/time.h>

#include "VariableTable.h"
#include "AExpression.h"
#include "../other/Configuration.h"
#include "../interface/WiFiInterface.h"
#include "../other/Scapy.h"
#include "../crypto/KeyLoader.h"

typedef struct {
	VariableTable  * variableTable;
	Configuration  * configuration;
	WiFiInterface  * interface;
	KeyLoader      * keyLoader;
	bool           * breakLoop;
	timeval        * timer;
} InterpreterData;


class Statement {
	public:
		Statement() {};
	        virtual ~Statement() {};

		virtual bool executeStatement(InterpreterData interpreterData) { return true; };
};
#endif
