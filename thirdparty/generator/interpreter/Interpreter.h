/* Soubor: Interpreter.h
 * Autor: Pavel Svanda
 * Email: svandap@gmail.com
 * Datum vytvoreni: 17.3.2013
 * Popis: deklarace tridy Interpreter, ktera slouzi k ulozeni stromu prikazu, ktery je nasledne vykonan
 */
#ifndef INTERPRETER_H
#define INTERPRETER_H

#include "WhileStatement.h"
#include "ForStatement.h"
#include "IfStatement.h"
#include "BodyStatement.h"
#include "VariableTable.h"
#include "CommandStatement.h"
#include "../lang/Lang.h"

static bool breakLoop;

class Interpreter {
	private:
		Statement * rootStatement;
		VariableTable variableTable;
		Configuration configuration;
		WiFiInterface * interface;
		KeyLoader keyLoader;
		
		timeval timer; 
	public:

		Interpreter();
	       ~Interpreter();

		void setRootStatement(Statement * statement);
		void setInterface(WiFiInterface * interface);
		void executeStatement();
		void showAllVariable();
		VariableTable * getVariableTable();
		Configuration * getConfiguration();
};
#endif
