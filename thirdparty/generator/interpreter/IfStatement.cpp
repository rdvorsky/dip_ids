/* Soubor: IfStatement.cpp
 * Autor: Pavel Svanda
 * Email: svandap@gmail.com
 * Datum vytvoreni: 17.3.2013
 * Popis: definice tridy IfStatement, ktera slouzi k ulozeni prikazu if
 */
#include "IfStatement.h"

/* IfStatement::IfStatement
 * Popis: kostruktor
 * Parametry: -
 * Navratova hodnota: -
 */
IfStatement::IfStatement() {
	this -> condition = NULL;
	this -> body = NULL;
	this -> elseBody = NULL;
}

/* IfStatement::~IfStatement
 * Popis: destruktor
 * Parametry: -
 * Navratova hodnota: -
 */
IfStatement::~IfStatement() {
	if(this -> condition != NULL)
		delete this -> condition;
	if(this -> body != NULL)
		delete this -> body;
	if(this -> elseBody != NULL)
		delete this -> elseBody;
}

/* IfStatement::setCondition
 * Popis: funkce nastavi podminku prikazu if
 * Parametry: AExpression * condition - podminka
 * Navratova hodnota: -
 */
void IfStatement::setCondition(AExpression * condition) {
	if(this -> condition != NULL)
		delete this -> condition;
	this -> condition = condition;
}

/* IfStatement::setBody
 * Popis: funkce nastavi telo prikazu if
 * Parametry: BodyStatement * body - telo prikazu if
 * Navratova hodnota: -
 */
void IfStatement::setBody(BodyStatement * body) {
	if(this -> body != NULL)
		delete this -> body;
	this -> body = body;
}

/* IfStatement::setElseBody
 * Popis: funkce nastavi else telo prikazu if
 * Parametry: BodyStatement * elseBody
 * Navratova hodnota: -
 */
void IfStatement::setElseBody(BodyStatement * elseBody) {
	if(this -> elseBody != NULL)
		delete this -> elseBody;
	this -> elseBody = elseBody;
}

/* IfStatement::executeStatement
 * Popis: funkce provede vykonani prikazu if
 * Parametry: InterpreterData interpreterData - promenne, ktere muze prikaz vyzadovat ke svemu vykonani
 * Navratova hodnota: navratova hodnota prikazu
 */
bool IfStatement::executeStatement(InterpreterData interpreterData) {
	VariableTable * variableTable = interpreterData.variableTable;

	Variable var = this -> condition -> evaluateExpression(variableTable);
	if((var.type == VT_INTEGER && var.value) || var.type != VT_INTEGER) {
		return this -> body -> executeStatement(interpreterData);
	} else {
		if(this -> elseBody != NULL)
			return this -> elseBody -> executeStatement(interpreterData);
	}

	return true;
}
