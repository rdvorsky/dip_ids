/* Soubor: WhileStatement.cpp
 * Autor: Pavel Svanda
 * Email: svandap@gmail.com
 * Datum vytvoreni: 17.3.2013
 * Popis: definice tridy WhileStatement, ktera slouzi k ulozeni prikazu while
 */
#include "WhileStatement.h"

/* WhileStatement::WhileStatement
 * Popis: konstruktor
 * Parametry: -
 * Navratova hodnota: -
 */
WhileStatement::WhileStatement() {
	this -> condition = NULL;
	this -> body = NULL;
}

/* WhileStatement::WhileStatement
 * Popis: destruktor
 * Parametry: -
 * Navratova hodnota: -
 */
WhileStatement::~WhileStatement() {
	if(this -> condition != NULL)
		delete this -> condition;
	if(this -> body != NULL)
		delete this -> body;
}

/* WhileStatement::setCondition
 * Popis: funkce provede nastaveni podminky cyklu
 * Parametry: AExpression * condition
 * Navratova hodnota: -
 */
void WhileStatement::setCondition(AExpression * condition) {
	if(this -> condition != NULL)
		delete this -> condition;
	this -> condition = condition;
}

/* WhileStatement::setBody
 * Popis: funkce provede nastaveni tela cyklu
 * Parametry: BodyStatement * body
 * Navratova hodnota: -
 */
void WhileStatement::setBody(BodyStatement * body) {
	if(this -> body != NULL)
		delete this -> body;
	this -> body = body;
}

/* WhileStatement::executeStatement
 * Popis: funkce provede vykonani cyklu
 * Parametry: InterpreterData interpreterData - data, ktere muze potrbovat vyraz pro jeho vykonani
 * Navratova hodnota: navratova hodnota prikazu
 */
bool WhileStatement::executeStatement(InterpreterData interpreterData) {
	VariableTable * variableTable = interpreterData.variableTable;
	bool * breakLoop	      = interpreterData.breakLoop;

	Variable var = this -> condition -> evaluateExpression(variableTable);
	while(var.value) {
		if(this -> body != NULL) {
			if(!this -> body -> executeStatement(interpreterData)) return true;
		}
		var = this -> condition -> evaluateExpression(variableTable);
		if(*breakLoop) { *breakLoop = false; return true; }
	}
	return true;
}
