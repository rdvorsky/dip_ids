/* Soubor: NumberValueExpression.cpp
 * Autor: Pavel Svanda
 * Email: svandap@gmail.com
 * Datum vytvoreni: 17.3.2013
 * Popis: definice tridy NumberValueExpression, ktera slouzi k ulozeni ciselne hodnoty
 */
#include "NumberValueExpression.h"

/* NubmerValueExpression::NumberValueExpression
 * Popis: konstruktor
 * Parametry: -
 * Navratova hodnota: -
 */
NubmerValueExpression::NumberValueExpression() {
	this -> value = 0;
}

/* NubmerValueExpression::NumberValueExpression
 * Popis: konstruktor
 * Parametry: int value - hodnota ciselneho vyrazu
 * Navratova hodnota: -
 */
NubmerValueExpression::NumberValueExpression(int value) {
	this -> value = value;
}

/* NubmerValueExpression::~NumberValueExpression
 * Popis: destruktor
 * Parametry: -
 * Navratova hodnota: -
 */
NubmerValueExpression::~NumberValueExpression() {
}

/* NubmerValueExpression::setValue
 * Popis: funkce provede nastaveni ciselne hodnoty
 * Parametry: int value - nastavovana hodnota
 * Navratova hodnota: -
 */
void NubmerValueExpression::setValue(int value) {
	this -> value = value;
}

/* NubmerValueExpression::evaluateExpression
 * Popis: funkce provede vyhodnoceni vyrazu - navraceni ciselne hodnoty
 * Parametry: VariableTable * variableTable - tabulka promennych
 * Navratova hodnota: -
 */
void NubmerValueExpression::evaluateExpression(VariableTable * variableTable) {
	return value;
}
