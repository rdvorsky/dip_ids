/* Soubor: VariableValueExpression.h
 * Autor: Pavel Svanda
 * Email: svandap@gmail.com
 * Datum vytvoreni: 17.3.2013
 * Popis: deklarace tridy VariableValueExpression, ktera slouzi k ulozeni ciselne hodnoty
 */
#ifndef VARIABLE_VALUE_EXPRESSION_H
#define VARIABLE_VALUE_EXPRESSION_H

#include "AExpression.h"

class VariableValueExpression : public AExpression {
	private:
		char variableName[LENGTH_VARIABLE_NAME];
	public:
		VariableValueExpression();
		VariableValueExpression(char * variableName);
	        virtual ~VariableValueExpression();

		void setVariableName(char * variableName);
		virtual void getVariableName(char * variableName);
		virtual Variable evaluateExpression(VariableTable * variableTable);
};
#endif
