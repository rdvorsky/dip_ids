/* Soubor: WhileStatement.h
 * Autor: Pavel Svanda
 * Email: svandap@gmail.com
 * Datum vytvoreni: 17.3.2013
 * Popis: deklarace tridy WhileStatement, ktera slouzi k ulozeni prikazu while
 */
#ifndef WHILE_STATEMENT_H
#define WHILE_STATEMENT_H

#include "Statement.h"
#include "AExpression.h"
#include "BodyStatement.h"
#include "ExpressionStatement.h"
#include "AssignStatement.h"
#include "BExpression.h"
#include "UExpression.h"
#include "NumberValueExpression.h"
#include "VariableValueExpression.h"

extern bool BREAK_LOOP;

class WhileStatement : public Statement {
	private:
		AExpression * condition;
		BodyStatement * body;
	public:
		WhileStatement();
	        virtual ~WhileStatement();

		void setCondition(AExpression * condition);
		void setBody(BodyStatement * body);
		virtual bool executeStatement(InterpreterData interpreterData);
};
#endif
