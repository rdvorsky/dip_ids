/* Soubor: ExpressionStatement.h
 * Autor: Pavel Svanda
 * Email: svandap@gmail.com
 * Datum vytvoreni: 17.3.2013
 * Popis: deklarace tridy ExpressionStatement, ktera slouzi k ulozeni prikazu vyrazu
 */
#ifndef EXPRESSION_STATEMENT_H
#define EXPRESSION_STATEMENT_H

#include "Statement.h"

class ExpressionStatement : public Statement {
	private:
		AExpression * expression;

	public:
		ExpressionStatement();
		ExpressionStatement(AExpression * expression);
	        virtual ~ExpressionStatement();

		void setExpression(AExpression * expression);
		virtual bool executeStatement(InterpreterData interpreterData);
};
#endif
