/* Soubor: UExpression.cpp
 * Autor: Pavel Svanda
 * Email: svandap@gmail.com
 * Datum vytvoreni: 17.3.2013
 * Popis: definice tridy UExpression, ktera slouzi k ulozeni unarniho vyrazu
 */
#include "UExpression.h"

/* UExpression::UExpression
 * Popis: konstruktor
 * Parametry: -
 * Navratova hodnota: -
 */
UExpression::UExpression() {
	this -> expression = NULL;
	this -> operation = 0;
}

/* UExpression::UExpression
 * Popis: konstruktor
 * Parametry: AExpression * expression - nastavovany vyraz
 *            int operation            - typ operace, ktera ma byt provedena 
 * Navratova hodnota: -
 */
UExpression::UExpression(AExpression * expression, int operation) {
	this -> expression = expression;
	this -> operation = operation;
}

/* UExpression::~UExpression
 * Popis: destruktor
 * Parametry: -
 * Navratova hodnota: -
 */
UExpression::~UExpression() {
	if(this -> expression != NULL)
		delete this -> expression;
}

/* UExpression::setExpression
 * Popis: funkce provede nastaveni vyrazu
 * Parametry: AExpression * expression - nastavovany vyraz
 * Navratova hodnota: -
 */
void UExpression::setExpression(AExpression * expression) {
	if(this -> expression != NULL)
		delete this -> expression;
	this -> expression = expression;
}

/* UExpression::evaluateExpression
 * Popis: funkce provede vyhodnoceni vyrazu
 * Parametry: VariableTable * variableTable
 * Navratova hodnota: hodnota vyrazu
 */
Variable UExpression::evaluateExpression(VariableTable * variableTable) {
	switch(this -> operation) {
		case OP_NEG : {
			Variable var = this -> expression -> evaluateExpression(variableTable);
			if(var.type == VT_INTEGER)
				if(var.value) var.value = 0;
				else          var.value = 1;
			else
				printf("%s\r\n", LANG_INTERPRETER_BAD_DATA_TYPE);
			return var;
		}; break;
		case OP_INC : {
			Variable var = this -> expression -> evaluateExpression(variableTable);
			if(var.type == VT_INTEGER)
				var.value += 1;
			else
				printf("%s\r\n", LANG_INTERPRETER_BAD_DATA_TYPE);

			//pokud je vyraz typu VariableValueExpression, tak musim ulozit vysledek
			if(typeid(*this -> expression) == typeid(VariableValueExpression)) {
				char varName [LENGTH_VARIABLE_NAME];
				this -> expression -> getVariableName(varName);
				if(var.type == VT_INTEGER)
					variableTable -> setIntVariable(varName, var.value);
			}
			return var;
		}; break;
		case OP_DEC : {
			Variable var = this -> expression -> evaluateExpression(variableTable);
			if(var.type == VT_INTEGER)
				var.value -= 1;
			else
				printf("%s\r\n", LANG_INTERPRETER_BAD_DATA_TYPE);
			//pokud je vyraz typu VariableValueExpression, tak musim ulozit vysledek
			if(typeid(*this -> expression) == typeid(VariableValueExpression)) {
				char varName [LENGTH_VARIABLE_NAME];
				this -> expression -> getVariableName(varName);
				if(var.type == VT_INTEGER)
					variableTable -> setIntVariable(varName, var.value);
			}
			return var;
		}; break;
	}
}
