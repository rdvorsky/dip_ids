/* Soubor: ForStatement.h
 * Autor: Pavel Svanda
 * Email: svandap@gmail.com
 * Datum vytvoreni: 17.3.2013
 * Popis: deklarace tridy ForStatement, ktera slouzi k ulozeni prikazu for
 */
#ifndef FOR_STATEMENT_H
#define FOR_STATEMENT_H

#include "Statement.h"
#include "AExpression.h"
#include "BodyStatement.h"
#include "ExpressionStatement.h"
#include "AssignStatement.h"
#include "BExpression.h"
#include "UExpression.h"
#include "NumberValueExpression.h"
#include "VariableValueExpression.h"

extern bool BREAK_LOOP;

class ForStatement : public Statement {
	private:
		AExpression * initCondition;
		AExpression * condition;
		AExpression * action;
		BodyStatement * body;
	public:
		ForStatement();
	        virtual ~ForStatement();

		void setInitCondition(AExpression * initCondition);
		void setCondition(AExpression * condition);
		void setAction(AExpression * action);
		void setBody(BodyStatement * body);
		virtual bool executeStatement(InterpreterData interpreterData);
};
#endif
