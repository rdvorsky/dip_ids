/* Soubor: VariableValueExpression.cpp
 * Autor: Pavel Svanda
 * Email: svandap@gmail.com
 * Datum vytvoreni: 17.3.2013
 * Popis: definice tridy VariableValueExpression, ktera slouzi k ulozeni ciselne hodnoty
 */
#include "VariableValueExpression.h"

/* VariableValueExpression::VariableValueExpression
 * Popis: konstruktor
 * Parametry: -
 * Navratova hodnota: -
 */
VariableValueExpression::VariableValueExpression() {
	this -> variableName[0] = 0;
}

/* VariableValueExpression::VariableValueExpression
 * Popis: konstruktor
 * Parametry: char * variableName - jmeno promenne
 * Navratova hodnota: -
 */
VariableValueExpression::VariableValueExpression(char * variableName) {
	int lengthVariableName = strlen(variableName);
	if(lengthVariableName > LENGTH_VARIABLE_NAME)
		lengthVariableName = LENGTH_VARIABLE_NAME;
	strncpy(this-> variableName, variableName, lengthVariableName);
	this -> variableName[lengthVariableName] = 0;
}

/* VariableValueExpression::~VariableValueExpression
 * Popis: destruktor
 * Parametry: -
 * Navratova hodnota: -
 */
VariableValueExpression::~VariableValueExpression() {
}

/* VariableValueExpression::setVariableName
 * Popis: funce provede nastaveni jmena promenne
 * Parametry: char * variableName - jmeno promenne
 * Navratova hodnota: -
 */
void VariableValueExpression::setVariableName(char * variableName) {
	int lengthVariableName = strlen(variableName);
	if(lengthVariableName > LENGTH_VARIABLE_NAME)
		lengthVariableName = LENGTH_VARIABLE_NAME;
	strncpy(this-> variableName, variableName, lengthVariableName);
	this -> variableName[lengthVariableName] = 0;
}

/* VariableValueExpression::getVariableName
 * Popis: funce provede navraceni jmena promenne
 * Parametry: char * variableName - misto, kam se ulozi jmeno promenne
 * Navratova hodnota: -
 */
void VariableValueExpression::getVariableName(char * variableName) {
	strcpy(variableName, this -> variableName);
}

/* VariableValueExpression::evaluateExpression
 * Popis: funce provede vykonani vyrazu
 * Parametry: VariableTable * variableTable - tabulka promennych
 * Navratova hodnota: hodnota vyrazu
 */
Variable VariableValueExpression::evaluateExpression(VariableTable * variableTable) {
	return variableTable -> getVariable(this -> variableName);
}
