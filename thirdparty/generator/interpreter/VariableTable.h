/* Soubor: VariableTable.h
 * Autor: Pavel Svanda
 * Email: svandap@gmail.com
 * Datum vytvoreni: 17.3.2013
 * Popis: deklarace tridy VariableTable, ktera slouzi k ukladani promennych
 */
#ifndef VARIABLETABLE_H
#define VARIABLETABLE_H

#include <stdio.h>
#include <string>
#include <string.h>
#include "../frame/Frame.h"
#include "../other/Alias.h"
#include <map>

using namespace std;

#define LENGTH_VARIABLE_NAME    200

#define VT_UNKNOWN		0
#define VT_INTEGER 		1
#define VT_FRAME 		2
#define VT_STRING 		3
#define VT_KEYWORD 		4
#define VT_CAPTURE_FRAME	5

#define CONTENT_RADIOTAP_HEADER 	","
#define CONTENT_IEEE_HEADER	 	"."
#define CONTENT_LLC_HEADER	  	";"


typedef struct {
	int type;
	long long int value;
	Frame frame;
	string str;
	Keyword keyword;
} Variable;

typedef map<string, Variable> VariableList;
typedef	map<string, Variable>::iterator VariableListIt;

class VariableTable {
	private:
		VariableList   variableList;
		VariableListIt variableListIt;
	public:
		VariableTable();
	       ~VariableTable();

		void setFrameVariable(char * name, Frame frame);
		void setIntVariable(char * name, long long int value);
		void setStringVariable(char * name, char * str);
		void setCaptureFrameVariable(char * name, char * str);
		Variable getVariable(char * name);
		void showAllVariable();
		void showFrameVariable();
		bool delVariable(char * name);
};
#endif
