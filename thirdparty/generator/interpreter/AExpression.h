/* Soubor: AExpression.h
 * Autor: Pavel Svanda
 * Email: svandap@gmail.com
 * Datum vytvoreni: 17.3.2013
 * Popis: deklarace abstranktni tridy AExpression, ktera slouzi k odvozovani dalsich vyrazu
 */
#ifndef AEXPRESSION_H
#define AEXPRESSION_H

#include <stdio.h>
#include <stdlib.h>
#include <typeinfo>
#include "VariableTable.h"

#define OP_PLUS		1
#define OP_MINUS	2
#define OP_MUL		3
#define OP_DIV		4
#define OP_LT		5
#define OP_GT		6
#define OP_LE		7
#define OP_GE		8
#define OP_EQ		9
#define OP_NE		10
#define OP_ORL		11
#define OP_ANDL		12
#define OP_OR		13
#define OP_AND		14
#define OP_NEG		15
#define OP_INC		16
#define OP_DEC		17
#define OP_ASSIGN	18
#define OP_MOD		19

class AExpression {
		
	public:
		AExpression() {};
 	        virtual ~AExpression() {};

		virtual Variable evaluateExpression(VariableTable * variableTable) {};
		virtual void getVariableName(char * variableName) {};
};
#endif
