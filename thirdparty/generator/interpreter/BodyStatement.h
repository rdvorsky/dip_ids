/* Soubor: BodyStatement.h
 * Autor: Pavel Svanda
 * Email: svandap@gmail.com
 * Datum vytvoreni: 17.3.2013
 * Popis: deklarace tridy BodyStatement, ktera slouzi k ulozeni seznamu prikazu
 */
#ifndef BODY_STATEMENT_H
#define BODY_STATEMENT_H

#include "Statement.h"
#include "CommandStatement.h"
#include <list>

using namespace std;

typedef list<Statement *> StatementList;
typedef	list<Statement *>::iterator StatementListIt;

class BodyStatement : public Statement {
	private:
		StatementList   statementList;
		StatementListIt statementListIt;
	public:
		BodyStatement();
	        virtual ~BodyStatement();

		void addStatement(Statement * statement);
		virtual bool executeStatement(InterpreterData interpreterData);
};
#endif
