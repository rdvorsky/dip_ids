/* Soubor: KeywordValueExpression.cpp
 * Autor: Pavel Svanda
 * Email: svandap@gmail.com
 * Datum vytvoreni: 17.3.2013
 * Popis: definice tridy KeywordValueExpression, ktera slouzi k ulozeni keywordu
 */
#include "KeywordValueExpression.h"

/* KeywordValueExpression::KeywordValueExpression
 * Popis: konstruktor
 * Parametry: -
 * Navratova hodnota: -
 */
KeywordValueExpression::KeywordValueExpression() {
}

/* KeywordValueExpression::KeywordValueExpression
 * Popis: konstruktor
 * Parametry: Keyword keyword - nastavovana hodnota klicoveho slova
 * Navratova hodnota: -
 */
KeywordValueExpression::KeywordValueExpression(Keyword keyword) {
	this -> keyword = keyword;
}

/* KeywordValueExpression::~KeywordValueExpression
 * Popis: destruktor
 * Parametry: -
 * Navratova hodnota: -
 */
KeywordValueExpression::~KeywordValueExpression() {
}

/* KeywordValueExpression::setValue
 * Popis: funkce provede nastaveni klicoveho slova
 * Parametry: Keyword keyword - nastavovane klicove slovo
 * Navratova hodnota: -
 */
void KeywordValueExpression::setValue(Keyword keyword) {
	this -> keyword = keyword;
}

/* KeywordValueExpression::evaluateExpression
 * Popis: funkce provede vyhodnoceni vyrazu - provede navraceni hodnoty klicoveho slova
 * Parametry: VariableTable * variableTable - tablka promennych
 * Navratova hodnota: navracene klicove slovo
 */
Variable KeywordValueExpression::evaluateExpression(VariableTable * variableTable) {
	Variable var;
	var.type = VT_KEYWORD;
	var.keyword = this -> keyword;
	return var;
}
