/* Soubor: Interpreter.cpp
 * Autor: Pavel Svanda
 * Email: svandap@gmail.com
 * Datum vytvoreni: 17.3.2013
 * Popis: deklarace tridy Interpreter, ktera slouzi k ulozeni stromu prikazu, ktery je nasledne vykonan
 */

#include "Interpreter.h"

/* breakLoopFunction
 * Popis: funkce, ktera byla odchycena na udalost zaslani signalu SIGUSR1, provede nastaveni priznaku konce cyklu
 * Parametry: int param -
 * Navratova hodnota: -
 */
void breakLoopFunction(int param) {
	breakLoop = true;
}

/* Interpreter::Interpreter
 * Popis: kostruktor
 * Parametry: -
 * Navratova hodnota: -
 */
Interpreter::Interpreter() {
	breakLoop = false;
	gettimeofday(&this -> timer, NULL);
	signal(SIGUSR1, breakLoopFunction);
	this -> rootStatement = NULL;
	this -> interface = NULL;
}

/* Interpreter::~Interpreter
 * Popis: destruktor
 * Parametry: -
 * Navratova hodnota: -
 */
Interpreter::~Interpreter() {
	if(this -> rootStatement != NULL)
		delete this -> rootStatement;
}

/* Interpreter::setInterface
 * Popis: funkce provede nastaveni pouziteho rozhrani
 * Parametry: WiFiInterface * interface - nastavovane rozhrani
 * Navratova hodnota: -
 */
void Interpreter::setInterface(WiFiInterface * interface) {
	this -> interface = interface;
}

/* Interpreter::setRootStatement
 * Popis: funkce provede nastaveni korene prikazu
 * Parametry: Statement * root - nastavovany koren prikazu
 * Navratova hodnota: -
 */
void Interpreter::setRootStatement(Statement * root) {
	//smazeme predchozi strom
	if(this -> rootStatement != NULL)
		delete this -> rootStatement;

	this -> rootStatement = root;
}

/* Interpreter::executeStatement
 * Popis: funkce provede vykonani prikazu
 * Parametry: -
 * Navratova hodnota: -
 */
void Interpreter::executeStatement() {
	breakLoop = false; 

	InterpreterData interpreterData;
	interpreterData.variableTable = &this -> variableTable;
	interpreterData.configuration = &this -> configuration;
	interpreterData.interface     =  this -> interface;
	interpreterData.keyLoader     = &this -> keyLoader;
	interpreterData.breakLoop     = &breakLoop;
	interpreterData.timer	      = &this -> timer;
	if(this -> rootStatement != NULL)		
		this -> rootStatement -> executeStatement(interpreterData);
}

/* Interpreter::showAllVariable
 * Popis: funkce provede vypsani vsech promennych
 * Parametry: -
 * Navratova hodnota: -
 */
void Interpreter::showAllVariable() {
	this -> variableTable.showAllVariable();
}

/* Interpreter::getVariableTable
 * Popis: funkce provede vraceni odkazu na tabulku promennych
 * Parametry: -
 * Navratova hodnota: navraceny odkaz na tabulku promennych
 */
VariableTable * Interpreter::getVariableTable() {
	return &this -> variableTable;
}

/* Interpreter::getConfiguration
 * Popis: funkce provede vraceni odkazu konfiguraci programu
 * Parametry: -
 * Navratova hodnota: navraceny odkaz na konfiguraci
 */
Configuration * Interpreter::getConfiguration() {
	return &this -> configuration;
}
