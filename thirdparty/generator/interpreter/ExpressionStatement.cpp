/* Soubor: ExpressionStatement.cpp
 * Autor: Pavel Svanda
 * Email: svandap@gmail.com
 * Datum vytvoreni: 17.3.2013
 * Popis: definice tridy ExpressionStatement, ktera slouzi k ulozeni prikazu vyrazu
 */
#include "ExpressionStatement.h"

/* ExpressionStatement::ExpressionStatement
 * Popis: konstruktor
 * Parametry: -
 * Navratova hodnota: -
 */
ExpressionStatement::ExpressionStatement() {
	this -> expression = NULL;
}

/* ExpressionStatement::ExpressionStatement
 * Popis: konstruktor
 * Parametry: AExpression * expression - nastavovany vyraz
 * Navratova hodnota: -
 */
ExpressionStatement::ExpressionStatement(AExpression * expression) {
	this -> expression = expression;
}

/* ExpressionStatement::~ExpressionStatement
 * Popis: destruktor
 * Parametry: -
 * Navratova hodnota: -
 */
ExpressionStatement::~ExpressionStatement() {
	if(this -> expression != NULL)
		delete this -> expression;
}

/* ExpressionStatement::setExpression
 * Popis: funkce provede nastaveni vyrazu
 * Parametry: AExpression * expression - nastavovany vyraz
 * Navratova hodnota: -
 */
void ExpressionStatement::setExpression(AExpression * expression) {
	if(this -> expression != NULL)
		delete this -> expression;
	this -> expression = expression;
}

/* ExpressionStatement::executeStatement
 * Popis: funkce provede vykonani prikazu vyrazu
 * Parametry: InterpreterData interpreterData - data interpretru, ktere muze prikaz vyzadovat
 * Navratova hodnota: navratova hodnota vyrazu
 */
bool ExpressionStatement::executeStatement(InterpreterData interpreterData) {
	VariableTable * variableTable = interpreterData.variableTable;

	if(this -> expression != NULL)
		this -> expression -> evaluateExpression(variableTable);
	return true;
}
