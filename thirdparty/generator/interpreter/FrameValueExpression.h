/* Soubor: FrameValueExpression.h
 * Autor: Pavel Svanda
 * Email: svandap@gmail.com
 * Datum vytvoreni: 17.3.2013
 * Popis: deklarace tridy FrameValueExpression, ktera slouzi k ulozeni ramce
 */
#ifndef FRAME_VALUE_EXPRESSION_H
#define FRAME_VALUE_EXPRESSION_H

#define FVO_SET 	0
#define FVO_GET		1
#define FVO_DEL		2

#include "AExpression.h"
#include "VariableValueExpression.h"
#include "FrameProperty.h"
#include <list>

using namespace std;

typedef list<FrameProperty *> PropertyList;
typedef	list<FrameProperty *>::iterator PropertyListIt;

class FrameValueExpression : public AExpression {
	private:
		PropertyList   propertyList;
		PropertyListIt propertyListIt;

		AExpression * frameName;

		int operation;
		
	public:
		FrameValueExpression();
		FrameValueExpression(AExpression * frameName);
		FrameValueExpression(AExpression * frameName, FrameProperty * property);
		FrameValueExpression(AExpression * frameName, FrameProperty * property, int operation);
	        virtual ~FrameValueExpression();

		void setFrameProperty(FrameProperty * property);
		void setFrameName(AExpression * frameName);
		virtual Variable evaluateExpression(VariableTable * variableTable);
		void setProperty(Variable * var, FrameProperty * property, VariableTable * variableTable);
		FrameValueExpression * addProperty(PropertyList propertyList);
		PropertyList getPropertyList();
		Variable getProperty(Variable * var, FrameProperty * property, VariableTable * variableTable);
		Variable delProperty(Variable * var, FrameProperty * property, VariableTable * variableTable);
};
#endif
