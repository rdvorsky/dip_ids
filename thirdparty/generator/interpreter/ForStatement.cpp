/* Soubor: ForStatement.cpp
 * Autor: Pavel Svanda
 * Email: svandap@gmail.com
 * Datum vytvoreni: 17.3.2013
 * Popis: definice tridy ForStatement, ktera slouzi k ulozeni prikazu for
 */
#include "ForStatement.h"

/* ForStatement::ForStatement
 * Popis: konstruktor
 * Parametry: -
 * Navratova hodnota: -
 */
ForStatement::ForStatement() {
	this -> initCondition = NULL;
	this -> condition = NULL;
	this -> action = NULL;
	this -> body = NULL;
}

/* ForStatement::~ForStatement
 * Popis: destruktor
 * Parametry: -
 * Navratova hodnota: -
 */
ForStatement::~ForStatement() {
	if(this -> initCondition != NULL)
		delete this -> initCondition;
	if(this -> condition != NULL)
		delete this -> condition;
	if(this -> action != NULL)
		delete this -> action;
	if(this -> body != NULL)
		delete this -> body;
}

/* ForStatement::setInitCondition
 * Popis: funkce provede nastaveni pocatecni podminky
 * Parametry: AExpression * initCondition - nastavovana podminka
 * Navratova hodnota: -
 */
void ForStatement::setInitCondition(AExpression * initCondition) {
	if(this -> initCondition != NULL)
		delete this -> initCondition;
	this -> initCondition = initCondition;
}

/* ForStatement::setCondition
 * Popis: funkce provede nastaveni koncove podminky
 * Parametry: AExpression * condition - nastavovana podminka
 * Navratova hodnota: -
 */
void ForStatement::setCondition(AExpression * condition) {
	if(this -> condition != NULL)
		delete this -> condition;
	this -> condition = condition;
}

/* ForStatement::setAction
 * Popis: funkce provede nastaveni akce, ktera se provede na konci cyklu
 * Parametry: AExpression * action - nastavovana akce
 * Navratova hodnota: -
 */
void ForStatement::setAction(AExpression * action) {
	if(this -> action != NULL)
		delete this -> action;
	this -> action = action;
}

/* ForStatement::setBody
 * Popis: funkce provede nastaveni tela prikazu for
 * Parametry: BodyStatement * body - nastavovane tele prikazu
 * Navratova hodnota: -
 */
void ForStatement::setBody(BodyStatement * body) {
	if(this -> body != NULL)
		delete this -> body;
	this -> body = body;
}

/* ForStatement::executeStatement
 * Popis: funkce provede vykonani prikazu
 * Parametry: InterpreterData interpreterData - data, ktere mohou byt potreba pro vykonani vyrazu
 * Navratova hodnota: navratova hodnota vyrazu
 */
bool ForStatement::executeStatement(InterpreterData interpreterData) {
	VariableTable * variableTable = interpreterData.variableTable;
	bool * breakLoop	      = interpreterData.breakLoop;

	Variable var;
	if(this -> initCondition != NULL)
		this -> initCondition -> evaluateExpression(variableTable);		
	if(this -> condition != NULL)
		var = this -> condition -> evaluateExpression(variableTable);
	while(	this -> initCondition == NULL || (
		this -> condition != NULL &&
		var.value)) {
		if(!this -> body -> executeStatement(interpreterData)) return true;
		if(this -> action != NULL)
			this -> action -> evaluateExpression(variableTable);
		
		if(this -> condition != NULL)
			var = this -> condition -> evaluateExpression(variableTable);
		if(*breakLoop) { *breakLoop = false; return true; }
	}
	return true;
}
