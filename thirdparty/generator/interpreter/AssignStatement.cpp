/* Soubor: AssignStatement.cpp
 * Autor: Pavel Svanda
 * Email: svandap@gmail.com
 * Datum vytvoreni: 17.3.2013
 * Popis: definice tridy AssignStatement, ktera slouzi k ulozeni vysledku vyrazu
 */
#include "AssignStatement.h"

/* AssignStatement::AssignStatement
 * Popis: konstruktor
 * Parametry: -
 * Navratova hodnota: -
 */
AssignStatement::AssignStatement() {
	this -> variableName[0];
	this -> expression = NULL;
}

/* AssignStatement::AssignStatement
 * Popis: konstruktor
 * Parametry: char * variableName      - nazev promenne, do ktere je provedeno ukladani
 *            AExpression * expression - ukladany vyraz
 * Navratova hodnota: -
 */
AssignStatement::AssignStatement(char * variableName, AExpression * expression) {
	int lengthVariableName = strlen(variableName);
	if(lengthVariableName > LENGTH_VARIABLE_NAME)
		lengthVariableName = LENGTH_VARIABLE_NAME;
	strncpy(this -> variableName, variableName, lengthVariableName);
	this -> variableName[lengthVariableName] = 0;
	this -> expression = expression;
}

/* AssignStatement::AssignStatement
 * Popis: konstruktor
 * Parametry: Aexpression * variableName - promenna, do ktere je ukladano
 *            AExpression * expression   - ukladany vyraz
 * Navratova hodnota: -
 */
AssignStatement::AssignStatement(AExpression * variableName, AExpression * expression) {
	char name[LENGTH_VARIABLE_NAME];
	variableName -> getVariableName(name);
	
	int lengthVariableName = strlen(name);
	if(lengthVariableName > LENGTH_VARIABLE_NAME)
		lengthVariableName = LENGTH_VARIABLE_NAME;
	strncpy(this -> variableName, name, lengthVariableName);
	this -> variableName[lengthVariableName] = 0;
	this -> expression = expression;
	delete variableName;
}

/* AssignStatement::~AssignStatement
 * Popis: destruktor
 * Parametry:
 * Navratova hodnota: -
 */
AssignStatement::~AssignStatement() {
	if(this -> expression != NULL)
		delete this -> expression;
}

/* AssignStatement::setAssign
 * Popis: funkce prevede ulozeni vyrazu a promenne
 * Parametry: char * variableName      - promenna, do ktere je ukladano
 *            AExpression * expression - ukladany vyraz
 * Navratova hodnota: -
 */
void AssignStatement::setAssign(char * variableName, AExpression * expression) {
	if(this -> expression != NULL)
		delete this -> expression;
	int lengthVariableName = strlen(variableName);
	if(lengthVariableName > LENGTH_VARIABLE_NAME)
		lengthVariableName = LENGTH_VARIABLE_NAME;
	strncpy(this -> variableName, variableName, lengthVariableName);
	this -> expression = expression;
}

/* AssignStatement::executeStatement
 * Popis: funkce prevede vykonani prikazu - provede se ulozeni ulozeni vysledku vyrazu do promenne
 * Parametry: InterpreterData interpreterData - sturktura obsahujici potrebne struktury pro vykonani prikazu
 * Navratova hodnota: navratova hodnota prikazu
 */
bool AssignStatement::executeStatement(InterpreterData interpreterData) {
	VariableTable * variableTable = interpreterData.variableTable;

	Variable var = this -> expression -> evaluateExpression(variableTable);
	if(var.type == VT_INTEGER)
		variableTable -> setIntVariable(this -> variableName, var.value);
	if(var.type == VT_STRING)
		variableTable -> setStringVariable(this -> variableName, (char *)var.str.c_str());
	if(var.type == VT_FRAME)
		variableTable -> setFrameVariable(this -> variableName, var.frame);
	return true;
}
