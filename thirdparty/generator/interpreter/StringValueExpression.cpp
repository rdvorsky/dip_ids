/* Soubor: StringValueExpression.cpp
 * Autor: Pavel Svanda
 * Email: svandap@gmail.com
 * Datum vytvoreni: 17.3.2013
 * Popis: definice tridy StringValueExpression, ktera slouzi k ulozeni retezce
 */
#include "StringValueExpression.h"

/* StringValueExpression::StringValueExpression
 * Popis: konstruktor
 * Parametry: -
 * Navratova hodnota: -
 */
StringValueExpression::StringValueExpression() {
	string str("");
	this -> value = str;
}

/* StringValueExpression::StringValueExpression
 * Popis: konstruktor
 * Parametry: char * value - nastavovany retezec
 * Navratova hodnota: -
 */
StringValueExpression::StringValueExpression(char * value) {
	string str(value);
	this -> value = str;
}

/* StringValueExpression::~StringValueExpression
 * Popis: destruktor
 * Parametry: -
 * Navratova hodnota: -
 */
StringValueExpression::~StringValueExpression() {
}

/* StringValueExpression::setValue
 * Popis: funkce provede nastaveni retezce
 * Parametry: char * value - nastavovany retezec
 * Navratova hodnota: -
 */
void StringValueExpression::setValue(char * value) {
	string str(value);
	this -> value = str;
}

/* StringValueExpression::evaluateExpression
 * Popis: funkce provede vyhodnoceni vyrazu - navraceni retezce
 * Parametry: VariableTable * variableTable - tabulka promennych
 * Navratova hodnota: navraceny retezec
 */
Variable StringValueExpression::evaluateExpression(VariableTable * variableTable) {
	Variable var;
	var.type = VT_STRING;
	var.str = this -> value;
	return var;
}
