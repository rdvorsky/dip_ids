/* Soubor: FrameValueExpression.cpp
 * Autor: Pavel Svanda
 * Email: svandap@gmail.com
 * Datum vytvoreni: 17.3.2013
 * Popis: definice tridy FrameValueExpression, ktera slouzi k ulozeni ramce
 */
#include "FrameValueExpression.h"

/* FrameValueExpression::FrameValueExpression
 * Popis: konstruktor
 * Parametry: -
 * Navratova hodnota: -
 */
FrameValueExpression::FrameValueExpression() {
	this -> frameName = NULL;
	this -> operation = FVO_SET;
}

/* FrameValueExpression::FrameValueExpression
 * Popis: konstruktor
 * Parametry: AExpression * frameName - jmeno nastavovaneho ramce
 * Navratova hodnota: -
 */
FrameValueExpression::FrameValueExpression(AExpression * frameName) {
	this -> frameName = frameName;
	this -> operation = FVO_SET;
}

/* FrameValueExpression::FrameValueExpression
 * Popis: konstruktor
 * Parametry: AExpression * frameName  - jmeno nastavovaneho ramce
 *            FrameProperty * property - nastavovane vlastnosti ramce
 * Navratova hodnota: -
 */
FrameValueExpression::FrameValueExpression(AExpression * frameName, FrameProperty * property) {
	this -> propertyList.push_back(property);
	this -> frameName = frameName;
	this -> operation = FVO_SET;
}

/* FrameValueExpression::FrameValueExpression
 * Popis: konstruktor
 * Parametry: AExpression * frameName  - jmeno nastavovaneho ramce
 *            FrameProperty * property - nastavovane vlastnosti ramce
 *            int operation            - typ operace - vypsani, nastaveni, smazani
 * Navratova hodnota: -
 */
FrameValueExpression::FrameValueExpression(AExpression * frameName, FrameProperty * property, int operation) {
	this -> propertyList.push_back(property);
	this -> frameName = frameName;
	this -> operation = operation;
}

/* FrameValueExpression::~FrameValueExpression
 * Popis: destruktor
 * Parametry: -
 * Navratova hodnota: -
 */
FrameValueExpression::~FrameValueExpression() {
	for(this -> propertyListIt = this -> propertyList.begin(); this -> propertyListIt != this -> propertyList.end(); this -> propertyListIt++) {
		delete *this -> propertyListIt;
	}
	this -> propertyList.erase(this -> propertyList.begin(), this -> propertyList.end());
	if(this -> frameName != NULL)
		delete this -> frameName;
}

/* FrameValueExpression::setFrameName
 * Popis: funkce provede nastaveni jmena ramce
 * Parametry: AExpression * frameName - jmeno ramce
 * Navratova hodnota: -
 */
void FrameValueExpression::setFrameName(AExpression * frameName) {
	this -> frameName = frameName;
}

/* FrameValueExpression::setFrameProperty
 * Popis: funkce provede nastaveni vlastnosti ramce
 * Parametry: FrameProperty * property - nastavovana vlastnost
 * Navratova hodnota: -
 */
void FrameValueExpression::setFrameProperty(FrameProperty * property) {
	this -> propertyList.push_back(property);
}

/* FrameValueExpression::addProperty
 * Popis: funkce provede pridani vlastnosti ramce
 * Parametry: PropertyList propertyList - seznam pridavanych vlastnosti
 * Navratova hodnota: ukazatel na sebe sama
 */
FrameValueExpression * FrameValueExpression::addProperty(PropertyList propertyList) {
	for(this -> propertyListIt = propertyList.begin(); this -> propertyListIt != propertyList.end(); this -> propertyListIt++) {
		this -> propertyList.push_back(*this -> propertyListIt);
	}
	return this;
}

/* FrameValueExpression::getPropertyList
 * Popis: funkce provede navraceni seznamu promennych
 * Parametry: -
 * Navratova hodnota: seznam promennych
 */
PropertyList FrameValueExpression::getPropertyList() {
	return this -> propertyList;
}

/* FrameValueExpression::evaluateExpression
 * Popis: funkce provede vyhodnoceni vyrazu
 * Parametry: VariableTable * variableTable - tabulka promennych
 * Navratova hodnota: hodnota vyrazu
 */
Variable FrameValueExpression::evaluateExpression(VariableTable * variableTable) {
	//nactu ramec z tabulky a priradim mu vsechny hodnoty
	char varName[LENGTH_VARIABLE_NAME];
	this -> frameName -> getVariableName(varName);
	Variable var = variableTable -> getVariable(varName);
	var.type = VT_FRAME;
	//vcyklu projdu vsechny ulozene hodnoty a vykonam je
	for(this -> propertyListIt = this -> propertyList.begin(); this -> propertyListIt != this -> propertyList.end(); this -> propertyListIt++) {
		FrameProperty * property = *this -> propertyListIt;
		switch(this -> operation) {
			case FVO_SET : {
				this -> setProperty(&var, property, variableTable);
			}; break;
			case FVO_GET : {
				return this -> getProperty(&var, property, variableTable);
			}; break;
			case FVO_DEL : {
				this -> delProperty(&var, property, variableTable);
			}; break;
		}
	}

	variableTable -> setFrameVariable(varName, var.frame);
	return var;
}

/* FrameValueExpression::setProperty
 * Popis: funkce provede nastaveni vlastnosti danemu ramci
 * Parametry: Variable * var                - vlastni ramec
 *            FrameProperty * property      - nastavovana vlastnost
 *            VariableTable * variableTable - tabulka promennych
 * Navratova hodnota: -
 */
void FrameValueExpression::setProperty(Variable * var, FrameProperty * property, VariableTable * variableTable) {
	switch(property -> getPropertyType()) {
		case PT_RADIOTAP_VERSION : {
			Variable value = property -> getValue(0, variableTable);
			var -> frame.radioTapHeader.setVersion(value.value);
			var -> frame.includeRadioTapHeader(true);
		}; break;
		case PT_RADIOTAP_PAD : {
			Variable value = property -> getValue(0, variableTable);
			var -> frame.radioTapHeader.setPad(value.value);
			var -> frame.includeRadioTapHeader(true);
		}; break;
		case PT_RADIOTAP_ANTENNA : {
			Variable value = property -> getValue(0, variableTable);
			var -> frame.radioTapHeader.setAntenna(value.value);
			var -> frame.includeRadioTapHeader(true);
		}; break;
		case PT_RADIOTAP_ANTENNA_NOISE : {
			Variable value = property -> getValue(0, variableTable);
			var -> frame.radioTapHeader.setAntennaNoise(value.value);
			var -> frame.includeRadioTapHeader(true);
		}; break;
		case PT_RADIOTAP_ANTENNA_SIGNAL : {
			Variable value = property -> getValue(0, variableTable);
			var -> frame.radioTapHeader.setAntennaSignal(value.value);
			var -> frame.includeRadioTapHeader(true);
		}; break;		
		case PT_RADIOTAP_CHANNEL_FREQUENCY : {
			Variable value = property -> getValue(0, variableTable);
			var -> frame.radioTapHeader.setChannelFrequency(value.value);
			var -> frame.includeRadioTapHeader(true);
		}; break;
		case PT_RADIOTAP_CHANNEL_FLAGS : {
			Variable value = property -> getValue(0, variableTable);
			if(value.type == VT_INTEGER) {
				var -> frame.radioTapHeader.setChannelFlags(value.value);
			} else {
				var -> frame.radioTapHeader.setChannelFlagsKeyword(value.keyword);
			}
			var -> frame.includeRadioTapHeader(true);
		}; break;
		case PT_RADIOTAP_FHSS_HOP_SET : {
			Variable value = property -> getValue(0, variableTable);
			var -> frame.radioTapHeader.setFHSShopSet(value.value);
			var -> frame.includeRadioTapHeader(true);
		}; break;
		case PT_RADIOTAP_FHSS_HOP_PATTERN : {
			Variable value = property -> getValue(0, variableTable);
			var -> frame.radioTapHeader.setFHSShopPattern(value.value);
			var -> frame.includeRadioTapHeader(true);
		}; break;
		case PT_RADIOTAP_FLAGS : {
			Variable value = property -> getValue(0, variableTable);
			if(value.type == VT_INTEGER) {
				var -> frame.radioTapHeader.setFlags(value.value);
			} else {
				var -> frame.radioTapHeader.setFlagsKeyword(value.keyword);
			}
			var -> frame.includeRadioTapHeader(true);
		}; break;
		case PT_RADIOTAP_LOCK_QUALITY : {
			Variable value = property -> getValue(0, variableTable);
			var -> frame.radioTapHeader.setLockQuality(value.value);
			var -> frame.includeRadioTapHeader(true);
		}; break;
		case PT_RADIOTAP_MCS_KNOWN : {
			Variable value = property -> getValue(0, variableTable);
			var -> frame.radioTapHeader.setMCSknown(value.value);
			var -> frame.includeRadioTapHeader(true);
		}; break;
		case PT_RADIOTAP_MCS_FLAGS : {
			Variable value = property -> getValue(0, variableTable);
			if(value.type == VT_INTEGER) {
				var -> frame.radioTapHeader.setMCSflags(value.value);
			} else {
				var -> frame.radioTapHeader.setMCSflagsKeyword(value.keyword);
			}
			var -> frame.includeRadioTapHeader(true);
		}; break;
		case PT_RADIOTAP_MCS_MCS : {
			Variable value = property -> getValue(0, variableTable);
			var -> frame.radioTapHeader.setMCSmcs(value.value);
			var -> frame.includeRadioTapHeader(true);
		}; break;
		case PT_RADIOTAP_RX_FLAGS : {
			Variable value = property -> getValue(0, variableTable);
			if(value.type == VT_INTEGER) {
				var -> frame.radioTapHeader.setRXflags(value.value);
			} else {
				var -> frame.radioTapHeader.setRXflagsKeyword(value.keyword);
			}
			var -> frame.includeRadioTapHeader(true);
		}; break;
		case PT_RADIOTAP_TSFT : {
			Variable value = property -> getValue(0, variableTable);
			var -> frame.radioTapHeader.setTSFTmactime(value.value);
			var -> frame.includeRadioTapHeader(true);
		}; break;
		case PT_RADIOTAP_TX_ATTENUATION : {
			Variable value = property -> getValue(0, variableTable);
			var -> frame.radioTapHeader.setTXattenuation(value.value);
			var -> frame.includeRadioTapHeader(true);
		}; break;
		case PT_RADIOTAP_VENDOR_NAMESPACE_OUI : {
			Variable value = property -> getValue(0, variableTable);
			uint8_t pom [L_OUI_LENGTH+1];
			Utilities::copyOUI(pom, (char *)value.str.c_str(), L_OUI_LENGTH);
			var -> frame.radioTapHeader.setVendorNamespaceOUI(pom);
			var -> frame.includeRadioTapHeader(true);
		}; break;
		case PT_RADIOTAP_VENDOR_NAMESPACE_SUB_NAMESPACE : {
			Variable value = property -> getValue(0, variableTable);
			var -> frame.radioTapHeader.setVendorNamespaceSubNamespace(value.value);
			var -> frame.includeRadioTapHeader(true);
		}; break;
		case PT_RADIOTAP_VENDOR_NAMESPACE_SKIP_LENGTH : {
			Variable value = property -> getValue(0, variableTable);
			var -> frame.radioTapHeader.setVendorNamespaceSkipLength(value.value);
			var -> frame.includeRadioTapHeader(true);
		}; break;
		case PT_RADIOTAP_DB_TX_ATTENUATION : {
			Variable value = property -> getValue(0, variableTable);
			var -> frame.radioTapHeader.setDbTXattenuation(value.value);
			var -> frame.includeRadioTapHeader(true);
		}; break;
		case PT_RADIOTAP_DB_ANTENNA_NOISE : {
			Variable value = property -> getValue(0, variableTable);
			var -> frame.radioTapHeader.setDbAntennaNoise(value.value);
			var -> frame.includeRadioTapHeader(true);
		}; break;
		case PT_RADIOTAP_DB_ANTENNA_SIGNAL : {
			Variable value = property -> getValue(0, variableTable);
			var -> frame.radioTapHeader.setDbAntennaSignal(value.value);
			var -> frame.includeRadioTapHeader(true);
		}; break;
		case PT_RADIOTAP_DBM_TX_POWER : {
			Variable value = property -> getValue(0, variableTable);
			var -> frame.radioTapHeader.setDbmTXpower(value.value);
			var -> frame.includeRadioTapHeader(true);
		}; break;
		case PT_RADIOTAP_RTS_RETRIES : {
			Variable value = property -> getValue(0, variableTable);
			var -> frame.radioTapHeader.setRTSretries(value.value);
			var -> frame.includeRadioTapHeader(true);
		}; break;
		case PT_RADIOTAP_DATA_RETRIES : {
			Variable value = property -> getValue(0, variableTable);
			var -> frame.radioTapHeader.setDataRetries(value.value);
			var -> frame.includeRadioTapHeader(true);
		}; break;
		/******************ieee*************************/
		case PT_IEEE_TYPE_DATA : {
			var -> frame.ieeeHeader.setDATAframe();
			var -> frame.includeIeeeHeader(true);
		}; break;
		case PT_IEEE_TYPE_QOS_DATA : {
			var -> frame.ieeeHeader.setQosDATAframe();
			var -> frame.includeIeeeHeader(true);
		}; break;
		case PT_IEEE_TYPE_RTS : {
			var -> frame.ieeeHeader.setRTSframe();
			var -> frame.includeIeeeHeader(true);
		}; break;
		case PT_IEEE_TYPE_CTS : {
			var -> frame.ieeeHeader.setCTSframe();
			var -> frame.includeIeeeHeader(true);
		}; break;
		case PT_IEEE_TYPE_ACK : {
			var -> frame.ieeeHeader.setACKframe();
			var -> frame.includeIeeeHeader(true);
		}; break;
		case PT_IEEE_TYPE_PS_POLL : {
			var -> frame.ieeeHeader.setPSPOLLframe();
			var -> frame.includeIeeeHeader(true);
		}; break;
		case PT_IEEE_TYPE_BEACON : {
			var -> frame.ieeeHeader.setBEACONframe();
			var -> frame.includeIeeeHeader(true);
		}; break;
		case PT_IEEE_TYPE_PROBE_REQUEST : {
			var -> frame.ieeeHeader.setPROBEREQUESTframe();
			var -> frame.includeIeeeHeader(true);
		}; break;
		case PT_IEEE_TYPE_PROBE_RESPONSE : {
			var -> frame.ieeeHeader.setPROBERESPONSEframe();
			var -> frame.includeIeeeHeader(true);
		}; break;
		case PT_IEEE_TYPE_ATIM : {
			var -> frame.ieeeHeader.setATIMframe();
			var -> frame.includeIeeeHeader(true);
		}; break;
		case PT_IEEE_TYPE_DEAUTHETICATION : {
			var -> frame.ieeeHeader.setDEAUTHETICATIONframe();
			var -> frame.includeIeeeHeader(true);
		}; break;
		case PT_IEEE_TYPE_DISASSOCIATION : {
			var -> frame.ieeeHeader.setDISASSOCIATIONframe();
			var -> frame.includeIeeeHeader(true);
		}; break;
		case PT_IEEE_TYPE_ASSOCIATION_REQUEST : {
			var -> frame.ieeeHeader.setASSOCIATIONREQUESTframe();
			var -> frame.includeIeeeHeader(true);
		}; break;
		case PT_IEEE_TYPE_REASSOCIATION_REQUEST : {
			var -> frame.ieeeHeader.setREASSOCIATIONREQUESTframe();
			var -> frame.includeIeeeHeader(true);
		}; break;
		case PT_IEEE_TYPE_ASSOCIATION_RESPONSE : {
			var -> frame.ieeeHeader.setREASSOCIATIONRESPONSEframe();
			var -> frame.includeIeeeHeader(true);
		}; break;
		case PT_IEEE_TYPE_AUTHENTICATION : {
			var -> frame.ieeeHeader.setAUTHENTICATIONframe();
			var -> frame.includeIeeeHeader(true);
		}; break;
		case PT_IEEE_FRAME_CONTROL : {
			Variable value = property -> getValue(0, variableTable);
			var -> frame.ieeeHeader.setFrameControl(value.value);
			var -> frame.includeIeeeHeader(true);
		}; break;
		case PT_IEEE_PROTOCOL : {
			Variable value = property -> getValue(0, variableTable);
			var -> frame.ieeeHeader.setProtocol(value.value);
			var -> frame.includeIeeeHeader(true);
		}; break;
		case PT_IEEE_TYPE : {
			Variable value = property -> getValue(0, variableTable);
			var -> frame.ieeeHeader.setType(value.value);
			var -> frame.includeIeeeHeader(true);
		}; break;
		case PT_IEEE_SUB_TYPE : {
			Variable value = property -> getValue(0, variableTable);
			var -> frame.ieeeHeader.setSubType(value.value);
			var -> frame.includeIeeeHeader(true);
		}; break;
		case PT_IEEE_TO_DS : {
			Variable value = property -> getValue(0, variableTable);
			var -> frame.ieeeHeader.setToDS(value.value);
			var -> frame.includeIeeeHeader(true);
		}; break;
		case PT_IEEE_FROM_DS : {
			Variable value = property -> getValue(0, variableTable);
			var -> frame.ieeeHeader.setFromDS(value.value);
			var -> frame.includeIeeeHeader(true);
		}; break;
		case PT_IEEE_MORE_FRAG : {
			Variable value = property -> getValue(0, variableTable);
			var -> frame.ieeeHeader.setMoreFrag(value.value);
			var -> frame.includeIeeeHeader(true);
		}; break;
		case PT_IEEE_RETRY : {
			Variable value = property -> getValue(0, variableTable);
			var -> frame.ieeeHeader.setRetry(value.value);
			var -> frame.includeIeeeHeader(true);
		}; break;
		case PT_IEEE_PWR_MGMT : {
			Variable value = property -> getValue(0, variableTable);
			var -> frame.ieeeHeader.setPwrMgmt(value.value);
			var -> frame.includeIeeeHeader(true);
		}; break;
		case PT_IEEE_MORE_DATA : {
			Variable value = property -> getValue(0, variableTable);
			var -> frame.ieeeHeader.setMoreData(value.value);
			var -> frame.includeIeeeHeader(true);
		}; break;
		case PT_IEEE_PROTECTED_FRAME : {
			Variable value = property -> getValue(0, variableTable);
			var -> frame.ieeeHeader.setProtectedFrame(value.value);
			var -> frame.includeIeeeHeader(true);
		}; break;
		case PT_IEEE_ORDER : {
			Variable value = property -> getValue(0, variableTable);
			var -> frame.ieeeHeader.setOrder(value.value);
			var -> frame.includeIeeeHeader(true);
		}; break;
		case PT_IEEE_TRANSMITTER_ADDRESS : {
			Variable value = property -> getValue(0, variableTable);
			var -> frame.ieeeHeader.setTransmitterAddress((char *)value.str.c_str());
			var -> frame.includeIeeeHeader(true);
		}; break;
		case PT_IEEE_RECEIVER_ADDRESS : {
			Variable value = property -> getValue(0, variableTable);
			var -> frame.ieeeHeader.setReceiverAddress((char *)value.str.c_str());
			var -> frame.includeIeeeHeader(true);
		}; break;
		case PT_IEEE_BSSID : {
			Variable value = property -> getValue(0, variableTable);
			var -> frame.ieeeHeader.setBSSID((char *)value.str.c_str());
			var -> frame.includeIeeeHeader(true);
		}; break;
		case PT_IEEE_DURATION : {
			Variable value = property -> getValue(0, variableTable);
			var -> frame.ieeeHeader.setDuration(value.value);
			var -> frame.includeIeeeHeader(true);
		}; break;
		case PT_IEEE_AID : {
			Variable value = property -> getValue(0, variableTable);
			var -> frame.ieeeHeader.setAid(value.value);
			var -> frame.includeIeeeHeader(true);
		}; break;
		case PT_IEEE_DA : {
			Variable value = property -> getValue(0, variableTable);
			var -> frame.ieeeHeader.setDA((char *)value.str.c_str());
			var -> frame.includeIeeeHeader(true);
		}; break;
		case PT_IEEE_SA : {
			Variable value = property -> getValue(0, variableTable);
			var -> frame.ieeeHeader.setSA((char *)value.str.c_str());
			var -> frame.includeIeeeHeader(true);
		}; break;
		case PT_IEEE_SEQ_CTL : {
			Variable value = property -> getValue(0, variableTable);
			var -> frame.ieeeHeader.setSeqCtl(value.value);
			var -> frame.includeIeeeHeader(true);
		}; break;
		case PT_IEEE_FRAGMENT : {
			Variable value = property -> getValue(0, variableTable);
			var -> frame.ieeeHeader.setFragment(value.value);
			var -> frame.includeIeeeHeader(true);
		}; break;
		case PT_IEEE_SEQUENCE : {
			Variable value = property -> getValue(0, variableTable);
			var -> frame.ieeeHeader.setSequence(value.value);
			var -> frame.includeIeeeHeader(true);
		}; break;
		case PT_IEEE_TIMESTAMP : {
			Variable value = property -> getValue(0, variableTable);
			var -> frame.ieeeHeader.setTimestamp(value.value);
			var -> frame.includeIeeeHeader(true);
		}; break;
		case PT_IEEE_BEACON_INTERVAL : {
			Variable value = property -> getValue(0, variableTable);
			var -> frame.ieeeHeader.setBeaconInterval(value.value);
			var -> frame.includeIeeeHeader(true);
		}; break;
		case PT_IEEE_CAPABILITY_INFO : {
			Variable value = property -> getValue(0, variableTable);
			if(value.type == VT_INTEGER) {
				var -> frame.ieeeHeader.setCapabilityInfo(value.value);
			} else {
				var -> frame.ieeeHeader.setCapabilityInfoKeyword(value.keyword);
			}
			var -> frame.includeIeeeHeader(true);
		}; break;
		case PT_IEEE_SSID : {
			Variable value = property -> getValue(0, variableTable);
			var -> frame.ieeeHeader.setSSID((char *)value.str.c_str());
			var -> frame.includeIeeeHeader(true);
		}; break;
		case PT_IEEE_FH_PARAMETER_SET_DWELL_TIME : {
			Variable value = property -> getValue(0, variableTable);
			var -> frame.ieeeHeader.setFHparameterSetDwellTime(value.value);
			var -> frame.includeIeeeHeader(true);
		}; break;
		case PT_IEEE_FH_PARAMETER_SET_HOP_SET : {
			Variable value = property -> getValue(0, variableTable);
			var -> frame.ieeeHeader.setFHparameterSetHopSet(value.value);
			var -> frame.includeIeeeHeader(true);
		}; break;
		case PT_IEEE_FH_PARAMETER_SET_HOP_PATTERN : {
			Variable value = property -> getValue(0, variableTable);
			var -> frame.ieeeHeader.setFHparameterSetHopPattern(value.value);
			var -> frame.includeIeeeHeader(true);
		}; break;
		case PT_IEEE_FH_PARAMETER_SET_HOP_INDEX : {
			Variable value = property -> getValue(0, variableTable);
			var -> frame.ieeeHeader.setFHparameterSetHopIndex(value.value);
			var -> frame.includeIeeeHeader(true);
		}; break;
		case PT_IEEE_CF_PARAMETER_SET_COUNT : {
			Variable value = property -> getValue(0, variableTable);
			var -> frame.ieeeHeader.setCFPcount(value.value);
			var -> frame.includeIeeeHeader(true);
		}; break;
		case PT_IEEE_CF_PARAMETER_SET_PERIOD : {
			Variable value = property -> getValue(0, variableTable);
			var -> frame.ieeeHeader.setCFPperiod(value.value);
			var -> frame.includeIeeeHeader(true);
		}; break;
		case PT_IEEE_CF_PARAMETER_SET_MAX_DURATION : {
			Variable value = property -> getValue(0, variableTable);
			var -> frame.ieeeHeader.setCFPmaxDuration(value.value);
			var -> frame.includeIeeeHeader(true);
		}; break;
		case PT_IEEE_CF_PARAMETER_SET_REMAINING : {
			Variable value = property -> getValue(0, variableTable);
			var -> frame.ieeeHeader.setCFPremaining(value.value);
			var -> frame.includeIeeeHeader(true);
		}; break;
		case PT_IEEE_DS_PARAMETER_SET : {
			Variable value = property -> getValue(0, variableTable);
			var -> frame.ieeeHeader.setDSparameterSet(value.value);
			var -> frame.includeIeeeHeader(true);
		}; break;
		case PT_IEEE_IBSS_PARAMETER_SET : {
			Variable value = property -> getValue(0, variableTable);
			var -> frame.ieeeHeader.setIBSSparameterSet(value.value);
			var -> frame.includeIeeeHeader(true);
		}; break;
		case PT_IEEE_TIM_DTIM_COUNT : {
			Variable value = property -> getValue(0, variableTable);
			var -> frame.ieeeHeader.setTIMdTimCount(value.value);
			var -> frame.includeIeeeHeader(true);
		}; break;
		case PT_IEEE_TIM_DTIM_PERIOD : {
			Variable value = property -> getValue(0, variableTable);
			var -> frame.ieeeHeader.setTIMdTimPeriod(value.value);
			var -> frame.includeIeeeHeader(true);
		}; break;
		case PT_IEEE_TIM_BITMAP_CONTROL : {
			Variable value = property -> getValue(0, variableTable);
			var -> frame.ieeeHeader.setTIMbitmapControl(value.value);
			var -> frame.includeIeeeHeader(true);
		}; break;
		case PT_IEEE_TIM_PARTIAL_VIRTUAL_BITMAP : {
			Variable value = property -> getValue(0, variableTable);
			var -> frame.ieeeHeader.setTIMpartialVirtualBitmap((char *)value.str.c_str());
			var -> frame.includeIeeeHeader(true);
		}; break;
		case PT_IEEE_COUNTRY_STRING : {
			Variable value = property -> getValue(0, variableTable);
			var -> frame.ieeeHeader.setCountryString((char *)value.str.c_str());
			var -> frame.includeIeeeHeader(true);
		}; break;
		case PT_IEEE_COUNTRY_CONSTRAINT : {
			ciect d;
			Variable value = property -> getValue(0, variableTable);
			int index = value.value;
			value = property -> getValue(1, variableTable);
			d.firstChannelNumber = value.value;
			value = property -> getValue(2, variableTable);
			d.numberOfChannel = value.value;
			value = property -> getValue(3, variableTable);
			d.maxTransmitPower = value.value;
			var -> frame.ieeeHeader.addCountryConstraintTriplet(index, d);
			var -> frame.includeIeeeHeader(true);
		}; break;
		case PT_IEEE_LOCAL_POWER_CONSTRAINT : {
			Variable value = property -> getValue(0, variableTable);
			var -> frame.ieeeHeader.setLocalPowerConstraint(value.value);
			var -> frame.includeIeeeHeader(true);
		}; break;
		case PT_IEEE_CHANNEL_SWITCH_MODE : {
			Variable value = property -> getValue(0, variableTable);
			var -> frame.ieeeHeader.setChannelSwitchMode(value.value);
			var -> frame.includeIeeeHeader(true);
		}; break;
		case PT_IEEE_NEW_CHANNEL_NUMBER : {
			Variable value = property -> getValue(0, variableTable);
			var -> frame.ieeeHeader.setNewChannelNumber(value.value);
			var -> frame.includeIeeeHeader(true);
		}; break;
		case PT_IEEE_CHANNEL_SWITCH_COUNT : {
			Variable value = property -> getValue(0, variableTable);
			var -> frame.ieeeHeader.setChannelSwitchCount(value.value);
			var -> frame.includeIeeeHeader(true);
		}; break;
		case PT_IEEE_QUIET_COUNT : {
			Variable value = property -> getValue(0, variableTable);
			var -> frame.ieeeHeader.setQuietCount(value.value);
			var -> frame.includeIeeeHeader(true);
		}; break;
		case PT_IEEE_QUIET_PERIOD : {
			Variable value = property -> getValue(0, variableTable);
			var -> frame.ieeeHeader.setQuietPeriod(value.value);
			var -> frame.includeIeeeHeader(true);
		}; break;
		case PT_IEEE_QUIET_DURATION : {
			Variable value = property -> getValue(0, variableTable);
			var -> frame.ieeeHeader.setQuietDuration(value.value);
			var -> frame.includeIeeeHeader(true);
		}; break;
		case PT_IEEE_QUIET_OFFSET : {
			Variable value = property -> getValue(0, variableTable);
			var -> frame.ieeeHeader.setQuietOffset(value.value);
			var -> frame.includeIeeeHeader(true);
		}; break;
		case PT_IEEE_TRANSMIT_POWER : {
			Variable value = property -> getValue(0, variableTable);
			var -> frame.ieeeHeader.setTransmitPower(value.value);
			var -> frame.includeIeeeHeader(true);
		}; break;
		case PT_IEEE_LINK_MARGIN : {
			Variable value = property -> getValue(0, variableTable);
			var -> frame.ieeeHeader.setLinkMargin(value.value);
			var -> frame.includeIeeeHeader(true);
		}; break;
		case PT_IEEE_ERP : {
			Variable value = property -> getValue(0, variableTable);
			if(value.type == VT_INTEGER) {
				var -> frame.ieeeHeader.setERP(value.value);
			} else {
				var -> frame.ieeeHeader.setERPKeyword(value.keyword);
			}
			var -> frame.includeIeeeHeader(true);
		}; break;
		case PT_IEEE_SUPPORTED_RATE : {
			Variable index = property -> getValue(0, variableTable);
			Variable value = property -> getValue(1, variableTable);
			var -> frame.ieeeHeader.addSupportedRate(index.value, value.value);
			var -> frame.includeIeeeHeader(true);
		}; break;
		case PT_IEEE_EXTENDED_SUPPORTED_RATE : {
			Variable index = property -> getValue(0, variableTable);
			Variable value = property -> getValue(1, variableTable);
			var -> frame.ieeeHeader.addExtendedSupportedRate(index.value, value.value);
			var -> frame.includeIeeeHeader(true);
		}; break;
		case PT_IEEE_REASON_CODE : {
			Variable value = property -> getValue(0, variableTable);
			var -> frame.ieeeHeader.setReasonCode(value.value);
			var -> frame.includeIeeeHeader(true);
		}; break;
		case PT_IEEE_LISTEN_INTERVAL : {
			Variable value = property -> getValue(0, variableTable);
			var -> frame.ieeeHeader.setListenInterval(value.value);
			var -> frame.includeIeeeHeader(true);
		}; break;
		case PT_IEEE_CURRENT_AP_ADDRESS : {
			Variable value = property -> getValue(0, variableTable);
			var -> frame.ieeeHeader.setCurrentAPaddress((char *)value.str.c_str());
			var -> frame.includeIeeeHeader(true);
		}; break;
		case PT_IEEE_RSN_VERSION : {
			Variable value = property -> getValue(0, variableTable);
			var -> frame.ieeeHeader.setRSNversion(value.value);
			var -> frame.includeIeeeHeader(true);
		}; break;
		case PT_IEEE_GROUP_CIPHER_SUITE : {
			Variable value = property -> getValue(0, variableTable);
			suite s;
	   	        Utilities::copyOUI(s.suiteOUI, (char *)value.str.c_str(), L_OUI_LENGTH);
			value = property -> getValue(1, variableTable);
			s.suiteType= value.value;
			var -> frame.ieeeHeader.setGroupCipherSuite(s);
			var -> frame.includeIeeeHeader(true);
		}; break;
		case PT_IEEE_PAIRWISE_CIPHER_SUITE : {
			Variable index = property -> getValue(0, variableTable);
			Variable value = property -> getValue(1, variableTable);
			suite s;
	   	        Utilities::copyOUI(s.suiteOUI, (char *)value.str.c_str(), L_OUI_LENGTH);
			value = property -> getValue(2, variableTable);
			s.suiteType= value.value;
			var -> frame.ieeeHeader.addPairwiseCipherSuite(s, index.value);
			var -> frame.includeIeeeHeader(true);
		}; break;
		case PT_IEEE_AUTHENTICATION_SUITE : {
			Variable index = property -> getValue(0, variableTable);
			Variable value = property -> getValue(1, variableTable);
			suite s;
	   	        Utilities::copyOUI(s.suiteOUI, (char *)value.str.c_str(), L_OUI_LENGTH);
			value = property -> getValue(2, variableTable);
			s.suiteType= value.value;
			var -> frame.ieeeHeader.addAuthenticationSuite(s, index.value);
			var -> frame.includeIeeeHeader(true);
		}; break;
		case PT_IEEE_RSN_CAPABILITIES : {
			Variable value = property -> getValue(0, variableTable);
			if(value.type == VT_INTEGER) {
				var -> frame.ieeeHeader.setRSNcapabilities(value.value);
			} else {
				var -> frame.ieeeHeader.setRSNcapabilitiesKeyword(value.keyword);
			}
			var -> frame.includeIeeeHeader(true);
		}; break;
		case PT_IEEE_STATUS_CODE: {
			Variable value = property -> getValue(0, variableTable);
			var -> frame.ieeeHeader.setStatusCode(value.value);
			var -> frame.includeIeeeHeader(true);
		}; break;
		case PT_IEEE_ASSOCIATION_ID : {
			Variable value = property -> getValue(0, variableTable);
			var -> frame.ieeeHeader.setAssociationId(value.value);
			var -> frame.includeIeeeHeader(true);
		}; break;
		case PT_IEEE_AUTHENTICATION_ALGORITHM_NUMBER : {
			Variable value = property -> getValue(0, variableTable);
			var -> frame.ieeeHeader.setAuthenticationAlgorithmNumber(value.value);
			var -> frame.includeIeeeHeader(true);
		}; break;
		case PT_IEEE_AUTHENTICATION_SEQUENCE_NUMBER : {
			Variable value = property -> getValue(0, variableTable);
			var -> frame.ieeeHeader.setAuthenticationSequenceNumber(value.value);
			var -> frame.includeIeeeHeader(true);
		}; break;
		case PT_IEEE_CHALLENGE_TEXT : {
			Variable value = property -> getValue(0, variableTable);
			var -> frame.ieeeHeader.setChallengeText((char *)value.str.c_str());
			var -> frame.includeIeeeHeader(true);
		}; break;
		case PT_IEEE_QOS : {
			Variable value = property -> getValue(0, variableTable);
			var -> frame.ieeeHeader.setQos(value.value);
			var -> frame.includeIeeeHeader(true);
		}; break;
		case PT_IEEE_ADDRESS1 : {
			Variable value = property -> getValue(0, variableTable);
			var -> frame.ieeeHeader.setAddress1((char *)value.str.c_str());
			var -> frame.includeIeeeHeader(true);
		}; break;
		case PT_IEEE_ADDRESS2 : {
			Variable value = property -> getValue(0, variableTable);
			var -> frame.ieeeHeader.setAddress2((char *)value.str.c_str());
			var -> frame.includeIeeeHeader(true);
		}; break;
		case PT_IEEE_ADDRESS3 : {
			Variable value = property -> getValue(0, variableTable);
			var -> frame.ieeeHeader.setAddress3((char *)value.str.c_str());
			var -> frame.includeIeeeHeader(true);
		}; break;
		case PT_IEEE_ADDRESS4 : {
			Variable value = property -> getValue(0, variableTable);
			var -> frame.ieeeHeader.setAddress4((char *)value.str.c_str());
			var -> frame.includeIeeeHeader(true);
		}; break;
		/******************llc**************************/
		case PT_LLC_DNAP : {
			Variable value = property -> getValue(0, variableTable);
			var -> frame.llcHeader.setDnap(value.value);
			var -> frame.includeLLCHeader(true);
		}; break;
		case PT_LLC_SNAP : {
			Variable value = property -> getValue(0, variableTable);
			var -> frame.llcHeader.setSnap(value.value);
			var -> frame.includeLLCHeader(true);
		}; break;
		case PT_LLC_CONTROL_FIELD : {
			Variable value = property -> getValue(0, variableTable);
			var -> frame.llcHeader.setControlField(value.value);
			var -> frame.includeLLCHeader(true);
		}; break;
		case PT_LLC_ORGANIZATION_CODE : {
			Variable value = property -> getValue(0, variableTable);
			var -> frame.llcHeader.setOrganizationCode(value.value);
			var -> frame.includeLLCHeader(true);
		}; break;
		case PT_LLC_TYPE : {
			Variable value = property -> getValue(0, variableTable);
			var -> frame.llcHeader.setType(value.value);
			var -> frame.includeLLCHeader(true);
		}; break;
		/******************payload************************/
		case PT_PAYLOAD_DATA : {
			Variable value = property -> getValue(0, variableTable);
			if(value.type == VT_CAPTURE_FRAME) {
				value.str = "0x" + value.str;
				var -> frame.payload.setData((uint8_t *)value.str.c_str());
			} else {
				var -> frame.payload.setData((uint8_t *)value.str.c_str());
			}
			var -> frame.includePayload(true);
		}; break;
		case PT_PAYLOAD_CIPHER : {
			Variable value = property -> getValue(0, variableTable);
			var -> frame.payload.setCipher(value.value);
			var -> frame.includePayload(true);
		}; break;
		case PT_PAYLOAD_VECTOR : {
			Variable value = property -> getValue(0, variableTable);
			if(value.type == VT_STRING) {
				var -> frame.payload.setIV((uint8_t *)value.str.c_str());
				var -> frame.includePayload(true);
			} else {
				var -> frame.payload.setIVNumber(value.value);
				var -> frame.includePayload(true);
			}
		}; break;
		case PT_PAYLOAD_KEY : {
			Variable value = property -> getValue(0, variableTable);
			var -> frame.payload.setKey((uint8_t *)value.str.c_str());
			var -> frame.includePayload(true);
		}; break;
		case PT_PAYLOAD_KEY_INDEX : {
			Variable value = property -> getValue(0, variableTable);
			var -> frame.payload.setKeyIndex(value.value);
			var -> frame.includePayload(true);
		}; break;
	}
}

/* FrameValueExpression::getProperty
 * Popis: funkce provede navraceni vlastnosti danemu ramci
 * Parametry: Variable * var                - vlastni ramec
 *            FrameProperty * property      - vlastnost
 *            VariableTable * variableTable - tabulka promennych
 * Navratova hodnota: -
 */
Variable FrameValueExpression::getProperty(Variable * var, FrameProperty * property, VariableTable * variableTable) {
	Variable value;
	switch(property -> getPropertyType()) {
		case PT_RADIOTAP_VERSION : {
			value.type = VT_INTEGER;
			value.value = var -> frame.radioTapHeader.getVersion();
		}; break;
		case PT_RADIOTAP_PAD : {
			value.type = VT_INTEGER;
			value.value = var -> frame.radioTapHeader.getPad();
		}; break;
		case PT_RADIOTAP_LEN : {
			value.type = VT_INTEGER;
			value.value = var -> frame.radioTapHeader.getLen();
		}; break;
		case PT_RADIOTAP_ANTENNA : {
			value.type = VT_INTEGER;
			value.value = var -> frame.radioTapHeader.getAntenna();
		}; break;
		case PT_RADIOTAP_ANTENNA_NOISE : {
			value.type = VT_INTEGER;
			value.value = var -> frame.radioTapHeader.getAntennaNoise();
		}; break;
		case PT_RADIOTAP_ANTENNA_SIGNAL : {
			value.type = VT_INTEGER;
			value.value = var -> frame.radioTapHeader.getAntennaSignal();
		}; break;
		case PT_RADIOTAP_CHANNEL_FREQUENCY : {
			value.type = VT_INTEGER;
			value.value = var -> frame.radioTapHeader.getChannelFrequency();
		}; break;
		case PT_RADIOTAP_CHANNEL_FLAGS : {
			value.type = VT_INTEGER;
			value.value = var -> frame.radioTapHeader.getFlags();
		}; break;
		case PT_RADIOTAP_FHSS_HOP_SET : {
			value.type = VT_INTEGER;
			value.value = var -> frame.radioTapHeader.getFHSShopSet();
		}; break;
		case PT_RADIOTAP_FHSS_HOP_PATTERN : {
			value.type = VT_INTEGER;
			value.value = var -> frame.radioTapHeader.getFHSShopPattern();
		}; break;
		case PT_RADIOTAP_FLAGS : {
			value.type = VT_INTEGER;
			value.value = var -> frame.radioTapHeader.getFlags();
		}; break;
		case PT_RADIOTAP_LOCK_QUALITY : {
			value.type = VT_INTEGER;
			value.value = var -> frame.radioTapHeader.getLockQuality();
		}; break;
		case PT_RADIOTAP_MCS_KNOWN : {
			value.type = VT_INTEGER;
			value.value = var -> frame.radioTapHeader.getMCSknown();
		}; break;
		case PT_RADIOTAP_MCS_FLAGS : {
			value.type = VT_INTEGER;
			value.value = var -> frame.radioTapHeader.getMCSflags();
		}; break;
		case PT_RADIOTAP_MCS_MCS : {
			value.type = VT_INTEGER;
			value.value = var -> frame.radioTapHeader.getMCSmcs();
		}; break;
		case PT_RADIOTAP_RX_FLAGS : {
			value.type = VT_INTEGER;
			value.value = var -> frame.radioTapHeader.getRXflags();
		}; break;
		case PT_RADIOTAP_TSFT : {
			value.type = VT_INTEGER;
			value.value = var -> frame.radioTapHeader.getTSFTmactime();
		}; break;
		case PT_RADIOTAP_TX_ATTENUATION : {
			value.type = VT_INTEGER;
			value.value = var -> frame.radioTapHeader.getTXattenuation();
		}; break;
		case PT_RADIOTAP_VENDOR_NAMESPACE_OUI : {
			value.type = VT_STRING;
			string str((char *) var -> frame.radioTapHeader.getVendorNamespaceOUI());
			value.str = str;
		}; break;
		case PT_RADIOTAP_VENDOR_NAMESPACE_SUB_NAMESPACE : {
			value.type = VT_INTEGER;
			value.value = var -> frame.radioTapHeader.getVendorNamespaceSubNamespace();
		}; break;
		case PT_RADIOTAP_VENDOR_NAMESPACE_SKIP_LENGTH : {
			value.type = VT_INTEGER;
			value.value = var -> frame.radioTapHeader.getVendorNamespaceSkipLength();
		}; break;
		case PT_RADIOTAP_DB_TX_ATTENUATION : {
			value.type = VT_INTEGER;
			value.value = var -> frame.radioTapHeader.getDbTXattenuation();
		}; break;
		case PT_RADIOTAP_DB_ANTENNA_NOISE : {
			value.type = VT_INTEGER;
			value.value = var -> frame.radioTapHeader.getDbAntennaNoise();
		}; break;
		case PT_RADIOTAP_DB_ANTENNA_SIGNAL : {
			value.type = VT_INTEGER;
			value.value = var -> frame.radioTapHeader.getDbAntennaSignal();
		}; break;
		case PT_RADIOTAP_DBM_TX_POWER : {
			value.type = VT_INTEGER;
			value.value = var -> frame.radioTapHeader.getDbmTXpower();
		}; break;
		case PT_RADIOTAP_RTS_RETRIES : {
			value.type = VT_INTEGER;
			value.value = var -> frame.radioTapHeader.getRTSretries();
		}; break;
		case PT_RADIOTAP_DATA_RETRIES : {
			value.type = VT_INTEGER;
			value.value = var -> frame.radioTapHeader.getDataRetries();
		}; break;
		/******************ieee*************************/
		case PT_IEEE_FRAME_CONTROL : {
			value.type = VT_INTEGER;
			value.value = var -> frame.ieeeHeader.getFrameControl();
		}; break;
		case PT_IEEE_PROTOCOL : {
			value.type = VT_INTEGER;
			value.value = var -> frame.ieeeHeader.getProtocol();
		}; break;
		case PT_IEEE_TYPE : {
			value.type = VT_INTEGER;
			value.value = var -> frame.ieeeHeader.getType();
		}; break;
		case PT_IEEE_SUB_TYPE : {
			value.type = VT_INTEGER;
			value.value = var -> frame.ieeeHeader.getSubType();
		}; break;
		case PT_IEEE_TO_DS : {
			value.type = VT_INTEGER;
			value.value = var -> frame.ieeeHeader.getToDS();
		}; break;
		case PT_IEEE_FROM_DS : {
			value.type = VT_INTEGER;
			value.value = var -> frame.ieeeHeader.getFromDS();
		}; break;
		case PT_IEEE_MORE_FRAG : {
			value.type = VT_INTEGER;
			value.value = var -> frame.ieeeHeader.getMoreFrag();
		}; break;
		case PT_IEEE_RETRY : {
			value.type = VT_INTEGER;
			value.value = var -> frame.ieeeHeader.getRetry();
		}; break;
		case PT_IEEE_PWR_MGMT : {
			value.type = VT_INTEGER;
			value.value = var -> frame.ieeeHeader.getPwrMgmt();
		}; break;
		case PT_IEEE_MORE_DATA : {
			value.type = VT_INTEGER;
			value.value = var -> frame.ieeeHeader.getMoreData();
		}; break;
		case PT_IEEE_PROTECTED_FRAME : {
			value.type = VT_INTEGER;
			value.value = var -> frame.ieeeHeader.getProtectedFrame();
		}; break;
		case PT_IEEE_ORDER : {
			value.type = VT_INTEGER;
			value.value = var -> frame.ieeeHeader.getOrder();
		}; break;
		case PT_IEEE_TRANSMITTER_ADDRESS : {
			value.type = VT_STRING;
			string str((char *)var -> frame.ieeeHeader.getTransmitterAddress());
			value.str = str;
		}; break;
		case PT_IEEE_RECEIVER_ADDRESS : {
			value.type = VT_STRING;
			string str((char *)var -> frame.ieeeHeader.getReceiverAddress());
			value.str = str;
		}; break;
		case PT_IEEE_BSSID : {
			value.type = VT_STRING;
			string str((char *)var -> frame.ieeeHeader.getBSSID());
			value.str = str;
		}; break;
		case PT_IEEE_DURATION : {
			value.type = VT_INTEGER;
			value.value = var -> frame.ieeeHeader.getDuration();
		}; break;
		case PT_IEEE_AID : {
			value.type = VT_INTEGER;
			value.value = var -> frame.ieeeHeader.getAid();
		}; break;
		case PT_IEEE_DA : {
			value.type = VT_STRING;
			string str((char *)var -> frame.ieeeHeader.getDA());
			value.str = str;
		}; break;
		case PT_IEEE_SA : {
			value.type = VT_STRING;
			string str((char *)var -> frame.ieeeHeader.getSA());
			value.str = str;
		}; break;
		case PT_IEEE_SEQ_CTL : {
			value.type = VT_INTEGER;
			value.value = var -> frame.ieeeHeader.getSeqCtl();
		}; break;
		case PT_IEEE_FRAGMENT : {
			value.type = VT_INTEGER;
			value.value = var -> frame.ieeeHeader.getFragment();
		}; break;
		case PT_IEEE_SEQUENCE : {
			value.type = VT_INTEGER;
			value.value = var -> frame.ieeeHeader.getSequence();
		}; break;
		case PT_IEEE_TIMESTAMP : {
			value.type = VT_INTEGER;
			value.value = var -> frame.ieeeHeader.getTimestamp();
		}; break;
		case PT_IEEE_BEACON_INTERVAL : {
			value.type = VT_INTEGER;
			value.value = var -> frame.ieeeHeader.getBeaconInterval();
		}; break;
		case PT_IEEE_CAPABILITY_INFO : {
			value.type = VT_INTEGER;
			value.value = var -> frame.ieeeHeader.getCapabilityInfo();
		}; break;
		case PT_IEEE_SSID : {
			value.type = VT_STRING;
			string str((char *)var -> frame.ieeeHeader.getSSID());
			value.str = str;
		}; break;
		case PT_IEEE_FH_PARAMETER_SET_DWELL_TIME : {
			value.type = VT_INTEGER;
			value.value = var -> frame.ieeeHeader.getFHparameterSetDwellTime();
		}; break;
		case PT_IEEE_FH_PARAMETER_SET_HOP_SET : {
			value.type = VT_INTEGER;
			value.value = var -> frame.ieeeHeader.getFHparameterSetHopSet();
		}; break;
		case PT_IEEE_FH_PARAMETER_SET_HOP_PATTERN : {
			value.type = VT_INTEGER;
			value.value = var -> frame.ieeeHeader.getFHparameterSetHopPattern();
		}; break;
		case PT_IEEE_FH_PARAMETER_SET_HOP_INDEX : {
			value.type = VT_INTEGER;
			value.value = var -> frame.ieeeHeader.getFHparameterSetHopIndex();
		}; break;
		case PT_IEEE_CF_PARAMETER_SET_COUNT : {
			value.type = VT_INTEGER;
			value.value = var -> frame.ieeeHeader.getCFPcount();
		}; break;
		case PT_IEEE_CF_PARAMETER_SET_PERIOD : {
			value.type = VT_INTEGER;
			value.value = var -> frame.ieeeHeader.getCFPperiod();
		}; break;
		case PT_IEEE_CF_PARAMETER_SET_MAX_DURATION : {
			value.type = VT_INTEGER;
			value.value = var -> frame.ieeeHeader.getCFPmaxDuration();
		}; break;
		case PT_IEEE_CF_PARAMETER_SET_REMAINING : {
			value.type = VT_INTEGER;
			value.value = var -> frame.ieeeHeader.getCFPremaining();
		}; break;
		case PT_IEEE_DS_PARAMETER_SET : {
			value.type = VT_INTEGER;
			value.value = var -> frame.ieeeHeader.getDSparameterSet();
		}; break;
		case PT_IEEE_IBSS_PARAMETER_SET : {
			value.type = VT_INTEGER;
			value.value = var -> frame.ieeeHeader.getIBSSparameterSet();
		}; break;
		case PT_IEEE_TIM_DTIM_COUNT : {
			value.type = VT_INTEGER;
			value.value = var -> frame.ieeeHeader.getTIMdTimCount();
		}; break;
		case PT_IEEE_TIM_DTIM_PERIOD : {
			value.type = VT_INTEGER;
			value.value = var -> frame.ieeeHeader.getTIMdTimPeriod();
		}; break;
		case PT_IEEE_TIM_BITMAP_CONTROL : {
			value.type = VT_INTEGER;
			value.value = var -> frame.ieeeHeader.getTIMbitmapControl();
		}; break;
		case PT_IEEE_TIM_PARTIAL_VIRTUAL_BITMAP : {
			value.type = VT_STRING;
			string str((char *)var -> frame.ieeeHeader.getTIMpartialVirtualBitmap());
			value.str = str;
		}; break;
		case PT_IEEE_COUNTRY_STRING : {
			value.type = VT_STRING;
			string str((char *)var -> frame.ieeeHeader.getCountryString());
			value.str = str;
		}; break;
		case PT_IEEE_COUNTRY_CONSTRAINT : {
			Variable index = property -> getValue(0, variableTable);
			value.type = VT_STRING;
			ciect d = var -> frame.ieeeHeader.getCountryConstraint(index.value);
			char pom[200];
			sprintf(pom, "%d %d %d", d.firstChannelNumber, d.numberOfChannel, d.maxTransmitPower);
			string str(pom);
			value.str = str;
		}; break;
		case PT_IEEE_LOCAL_POWER_CONSTRAINT : {
			value.type = VT_INTEGER;
			value.value = var -> frame.ieeeHeader.getLocalPowerConstraint();
		}; break;
		case PT_IEEE_CHANNEL_SWITCH_MODE : {
			value.type = VT_INTEGER;
			value.value = var -> frame.ieeeHeader.getChannelSwitchMode();
		}; break;
		case PT_IEEE_NEW_CHANNEL_NUMBER : {
			value.type = VT_INTEGER;
			value.value = var -> frame.ieeeHeader.getNewChannelNumber();
		}; break;
		case PT_IEEE_CHANNEL_SWITCH_COUNT : {
			value.type = VT_INTEGER;
			value.value = var -> frame.ieeeHeader.getChannelSwitchCount();
		}; break;
		case PT_IEEE_QUIET_COUNT : {
			value.type = VT_INTEGER;
			value.value = var -> frame.ieeeHeader.getQuietCount();
		}; break;
		case PT_IEEE_QUIET_PERIOD : {
			value.type = VT_INTEGER;
			value.value = var -> frame.ieeeHeader.getQuietPeriod();
		}; break;
		case PT_IEEE_QUIET_DURATION : {
			value.type = VT_INTEGER;
			value.value = var -> frame.ieeeHeader.getQuietDuration();
		}; break;
		case PT_IEEE_QUIET_OFFSET : {
			value.type = VT_INTEGER;
			value.value = var -> frame.ieeeHeader.getQuietOffset();
		}; break;
		case PT_IEEE_TRANSMIT_POWER : {
			value.type = VT_INTEGER;
			value.value = var -> frame.ieeeHeader.getTransmitPower();
		}; break;
		case PT_IEEE_LINK_MARGIN : {
			value.type = VT_INTEGER;
			value.value = var -> frame.ieeeHeader.getLinkMargin();
		}; break;
		case PT_IEEE_ERP : {
			value.type = VT_INTEGER;
			value.value = var -> frame.ieeeHeader.getERP();
		}; break;
		case PT_IEEE_SUPPORTED_RATE : {
			Variable index = property -> getValue(0, variableTable);
			value.type = VT_INTEGER;
			value.value = var -> frame.ieeeHeader.getSupportedRate(index.value);
		}; break;
		case PT_IEEE_EXTENDED_SUPPORTED_RATE : {
			Variable index = property -> getValue(0, variableTable);
			value.type = VT_INTEGER;
			value.value = var -> frame.ieeeHeader.getExtendedSupportedRate(index.value);
		}; break;
		case PT_IEEE_REASON_CODE : {
			value.type = VT_INTEGER;
			value.value = var -> frame.ieeeHeader.getReasonCode();
		}; break;
		case PT_IEEE_LISTEN_INTERVAL : {
			value.type = VT_INTEGER;
			value.value = var -> frame.ieeeHeader.getListenInterval();
		}; break;
		case PT_IEEE_CURRENT_AP_ADDRESS : {
			value.type = VT_STRING;
			string str((char *)var -> frame.ieeeHeader.getCurrentAPaddress());
			value.str = str;
		}; break;
		case PT_IEEE_RSN_VERSION : {
			value.type = VT_INTEGER;
			value.value = var -> frame.ieeeHeader.getRSNversion();
		}; break;
		case PT_IEEE_GROUP_CIPHER_SUITE : {
			value.type = VT_STRING;
			char pom[200];
			suite s = var -> frame.ieeeHeader.getGroupCipherSuite();
			sprintf(pom, "%02x-%02x-%02x, %d\r\n", s.suiteOUI[0], s.suiteOUI[1], s.suiteOUI[2], s.suiteType); 
			string str(str);
			value.str = str;

		}; break;
		case PT_IEEE_PAIRWISE_CIPHER_SUITE : {
			Variable index = property -> getValue(0, variableTable);
			value.type = VT_STRING;
			char pom[200];
			suite s = var -> frame.ieeeHeader.getPairwiseCipherSuite(index.value);
			sprintf(pom, "%02x-%02x-%02x, %d\r\n", s.suiteOUI[0], s.suiteOUI[1], s.suiteOUI[2], s.suiteType); 
			string str(str);
			value.str = str;

		}; break;
		case PT_IEEE_AUTHENTICATION_SUITE : {
			Variable index = property -> getValue(0, variableTable);
			value.type = VT_STRING;
			char pom[200];
			suite s = var -> frame.ieeeHeader.getAuthenticationSuite(index.value);
			sprintf(pom, "%02x-%02x-%02x, %d\r\n", s.suiteOUI[0], s.suiteOUI[1], s.suiteOUI[2], s.suiteType); 
			string str(str);
			value.str = str;
		}; break;
		case PT_IEEE_RSN_CAPABILITIES : {
			value.type = VT_INTEGER;
			value.value = var -> frame.ieeeHeader.getRSNcapabilities();
		}; break;
		case PT_IEEE_STATUS_CODE : {
			value.type = VT_INTEGER;
			value.value = var -> frame.ieeeHeader.getStatusCode();
		}; break;
		case PT_IEEE_ASSOCIATION_ID : {
			value.type = VT_INTEGER;
			value.value = var -> frame.ieeeHeader.getAssociationId();
		}; break;
		case PT_IEEE_AUTHENTICATION_ALGORITHM_NUMBER : {
			value.type = VT_INTEGER;
			value.value = var -> frame.ieeeHeader.getAuthenticationAlgorithmNumber();
		}; break;
		case PT_IEEE_AUTHENTICATION_SEQUENCE_NUMBER : {
			value.type = VT_INTEGER;
			value.value = var -> frame.ieeeHeader.getAuthenticationSequenceNumber();
		}; break;
		case PT_IEEE_CHALLENGE_TEXT : {
			value.type = VT_STRING;
			string str((char *)var -> frame.ieeeHeader.getChallengeText());
			value.str = str;
		}; break;
		case PT_IEEE_QOS : {
			value.type = VT_INTEGER;
			value.value = var -> frame.ieeeHeader.getQos();
		}; break;
		case PT_IEEE_ADDRESS1 : {
			value.type = VT_STRING;
			string str((char *)var -> frame.ieeeHeader.getAddress1());
			value.str = str;
		}; break;
		case PT_IEEE_ADDRESS2 : {
			value.type = VT_STRING;
			string str((char *)var -> frame.ieeeHeader.getAddress2());
			value.str = str;
		}; break;
		case PT_IEEE_ADDRESS3 : {
			value.type = VT_STRING;
			string str((char *)var -> frame.ieeeHeader.getAddress3());
			value.str = str;
		}; break;
		case PT_IEEE_ADDRESS4 : {
			value.type = VT_STRING;
			string str((char *)var -> frame.ieeeHeader.getAddress4());
			value.str = str;
		}; break;
		/******************llc**************************/
		case PT_LLC_DNAP : {
			value.type = VT_INTEGER;
			value.value = var -> frame.llcHeader.getDnap();
		}; break;
		case PT_LLC_SNAP : {
			value.type = VT_INTEGER;
			value.value = var -> frame.llcHeader.getSnap();
		}; break;
		case PT_LLC_CONTROL_FIELD : {
			value.type = VT_INTEGER;
			value.value = var -> frame.llcHeader.getControlField();
		}; break;
		case PT_LLC_ORGANIZATION_CODE : {
			value.type = VT_INTEGER;
			value.value = var -> frame.llcHeader.getOrganizationCode();
		}; break;
		case PT_LLC_TYPE : {
			value.type = VT_INTEGER;
			value.value = var -> frame.llcHeader.getType();
		}; break;
		/******************payload************************/
		case PT_PAYLOAD_DATA : {
			value.type = VT_STRING;
			uint8_t data[2*LENGTH_PAYLOAD_DATA];
  		        var -> frame.payload.getData(data);
			string str((char *)data);
			value.str = str;
		}; break;
		case PT_PAYLOAD_CIPHER : {
			value.type = VT_STRING;
			char strc[200];
  		        var -> frame.payload.getCipherStr(strc);
			string str(strc);
			value.str = str;
		}; break;
		case PT_PAYLOAD_VECTOR : {
			value.type = VT_STRING;
			uint8_t iv[2*LENGTH_IV];
  		        var -> frame.payload.getIV(iv);
			string str((char *)iv);
			value.str = str;
		}; break;
		case PT_PAYLOAD_KEY : {
			value.type = VT_STRING;
			uint8_t data[LENGTH_KEY];
  		        var -> frame.payload.getKey(data);
			string str((char *)data);
			value.str = str;
		}; break;
		case PT_PAYLOAD_KEY_INDEX : {
			value.type = VT_INTEGER;
			value.value = var -> frame.payload.getKeyIndex();
		}; break;
	}
	return value;
}

/* FrameValueExpression::delProperty
 * Popis: funkce provede smazani vlastnosti danemu ramci
 * Parametry: Variable * var                - vlastni ramec
 *            FrameProperty * property      - vlastnost
 *            VariableTable * variableTable - tabulka promennych
 * Navratova hodnota: -
 */
Variable FrameValueExpression::delProperty(Variable * var, FrameProperty * property, VariableTable * variableTable) {
	Variable value;
	switch(property -> getPropertyType()) {
		case PT_RADIOTAP_VERSION : {
			value.type = VT_INTEGER;
			var -> frame.radioTapHeader.delVersion();
			value.value = var -> frame.radioTapHeader.getVersion();
		}; break;
		case PT_RADIOTAP_PAD : {
			value.type = VT_INTEGER;
			var -> frame.radioTapHeader.delPad();
			value.value = var -> frame.radioTapHeader.getPad();
		}; break;
		case PT_RADIOTAP_ANTENNA : {
			value.type = VT_INTEGER;
			var -> frame.radioTapHeader.delAntenna();
			value.value = var -> frame.radioTapHeader.getAntenna();
		}; break;
		case PT_RADIOTAP_ANTENNA_NOISE : {
			value.type = VT_INTEGER;
			var -> frame.radioTapHeader.delAntennaNoise();
			value.value = var -> frame.radioTapHeader.getAntennaNoise();
		}; break;
		case PT_RADIOTAP_ANTENNA_SIGNAL : {
			value.type = VT_INTEGER;
			var -> frame.radioTapHeader.delAntennaSignal();
			value.value = var -> frame.radioTapHeader.getAntennaSignal();
		}; break;
		case PT_RADIOTAP_CHANNEL_FREQUENCY : {
			value.type = VT_INTEGER;
			var -> frame.radioTapHeader.delChannelFrequency();
			value.value = var -> frame.radioTapHeader.getChannelFrequency();
		}; break;
		case PT_RADIOTAP_CHANNEL_FLAGS : {
			value.type = VT_INTEGER;
			var -> frame.radioTapHeader.delChannelFlags();
			value.value = var -> frame.radioTapHeader.getChannelFlags();
		}; break;
		case PT_RADIOTAP_CHANNEL : {
			value.type = VT_INTEGER;
			var -> frame.radioTapHeader.delChannel();
			value.value = 0;
		}; break;
		case PT_RADIOTAP_FHSS_HOP_SET : {
			value.type = VT_INTEGER;
			var -> frame.radioTapHeader.delFHSShopSet();
			value.value = var -> frame.radioTapHeader.getFHSShopSet();
		}; break;
		case PT_RADIOTAP_FHSS_HOP_PATTERN : {
			value.type = VT_INTEGER;
			var -> frame.radioTapHeader.delFHSShopPattern();
			value.value = var -> frame.radioTapHeader.getFHSShopPattern();
		}; break;
		case PT_RADIOTAP_FHSS_HOP : {
			value.type = VT_INTEGER;
			var -> frame.radioTapHeader.delFHSShop();
			value.value = 0;
		}; break;
		case PT_RADIOTAP_FLAGS : {
			value.type = VT_INTEGER;
			var -> frame.radioTapHeader.delFlags();
			value.value = var -> frame.radioTapHeader.getFlags();
		}; break;
		case PT_RADIOTAP_LOCK_QUALITY : {
			value.type = VT_INTEGER;
			var -> frame.radioTapHeader.delLockQuality();
			value.value = var -> frame.radioTapHeader.getLockQuality();
		}; break;
		case PT_RADIOTAP_MCS_KNOWN : {
			value.type = VT_INTEGER;
			var -> frame.radioTapHeader.delMCSknown();
			value.value = var -> frame.radioTapHeader.getMCSknown();
		}; break;
		case PT_RADIOTAP_MCS_FLAGS : {
			value.type = VT_INTEGER;
			var -> frame.radioTapHeader.delMCSflags();
			value.value = var -> frame.radioTapHeader.getMCSflags();
		}; break;
		case PT_RADIOTAP_MCS_MCS : {
			value.type = VT_INTEGER;
			var -> frame.radioTapHeader.delMCSmcs();
			value.value = var -> frame.radioTapHeader.getMCSmcs();
		}; break;
		case PT_RADIOTAP_MCS : {
			value.type = VT_INTEGER;
			var -> frame.radioTapHeader.delMCS();
			value.value = 0;
		}; break;
		case PT_RADIOTAP_RX_FLAGS : {
			value.type = VT_INTEGER;
			var -> frame.radioTapHeader.delRXflags();
			value.value = var -> frame.radioTapHeader.getRXflags();
		}; break;
		case PT_RADIOTAP_RATE : {
			value.type = VT_INTEGER;
			var -> frame.radioTapHeader.delRate();
			value.value = var -> frame.radioTapHeader.getRate();
		}; break;
		case PT_RADIOTAP_TSFT : {
			value.type = VT_INTEGER;
			var -> frame.radioTapHeader.delTSFTmactime();
			value.value = var -> frame.radioTapHeader.getTSFTmactime();
		}; break;
		case PT_RADIOTAP_TX_ATTENUATION : {
			value.type = VT_INTEGER;
			var -> frame.radioTapHeader.delTXattenuation();
			value.value = var -> frame.radioTapHeader.getTXattenuation();
		}; break;
		case PT_RADIOTAP_VENDOR_NAMESPACE_OUI : {
			value.type = VT_STRING;
			var -> frame.radioTapHeader.delVendorNamespaceOUI();
			string str((char *) var -> frame.radioTapHeader.getVendorNamespaceOUI());
			value.str = str;
		}; break;
		case PT_RADIOTAP_VENDOR_NAMESPACE_SUB_NAMESPACE : {
			value.type = VT_INTEGER;
			var -> frame.radioTapHeader.delVendorNamespaceSubNamespace();
			value.value = var -> frame.radioTapHeader.getVendorNamespaceSubNamespace();
		}; break;
		case PT_RADIOTAP_VENDOR_NAMESPACE_SKIP_LENGTH : {
			value.type = VT_INTEGER;
			var -> frame.radioTapHeader.delVendorNamespaceSkipLength();
			value.value = var -> frame.radioTapHeader.getVendorNamespaceSkipLength();
		}; break;
		case PT_RADIOTAP_VENDOR_NAMESPACE : {
			value.type = VT_INTEGER;
			var -> frame.radioTapHeader.delVendorNamespace();
			value.value = 0;
		}; break;
		case PT_RADIOTAP_DB_TX_ATTENUATION : {
			value.type = VT_INTEGER;
			var -> frame.radioTapHeader.delDbTXattenuation();
			value.value = var -> frame.radioTapHeader.getDbTXattenuation();
		}; break;
		case PT_RADIOTAP_DB_ANTENNA_NOISE : {
			value.type = VT_INTEGER;
			var -> frame.radioTapHeader.delDbAntennaNoise();
			value.value = var -> frame.radioTapHeader.getDbAntennaNoise();
		}; break;
		case PT_RADIOTAP_DB_ANTENNA_SIGNAL : {
			value.type = VT_INTEGER;
			var -> frame.radioTapHeader.delDbAntennaSignal();
			value.value = var -> frame.radioTapHeader.getDbAntennaSignal();
		}; break;
		case PT_RADIOTAP_DBM_TX_POWER : {
			value.type = VT_INTEGER;
			var -> frame.radioTapHeader.delDbmTXpower();
			value.value = var -> frame.radioTapHeader.getDbmTXpower();
		}; break;
		case PT_RADIOTAP_RTS_RETRIES : {
			value.type = VT_INTEGER;
			var -> frame.radioTapHeader.delRTSretries();
			value.value = var -> frame.radioTapHeader.getRTSretries();
		}; break;
		case PT_RADIOTAP_DATA_RETRIES : {
			value.type = VT_INTEGER;
			var -> frame.radioTapHeader.delDataRetries();
			value.value = var -> frame.radioTapHeader.getDataRetries();
		}; break;
		/******************ieee*************************/
		case PT_IEEE_FRAME_CONTROL : {
			value.type = VT_INTEGER;
			var -> frame.ieeeHeader.delFrameControl();
			value.value = var -> frame.ieeeHeader.getFrameControl();
		}; break;
		case PT_IEEE_PROTOCOL : {
			value.type = VT_INTEGER;
			var -> frame.ieeeHeader.delProtocol();
			value.value = var -> frame.ieeeHeader.getProtocol();
		}; break;
		case PT_IEEE_TYPE : {
			value.type = VT_INTEGER;
			var -> frame.ieeeHeader.delType();
			value.value = var -> frame.ieeeHeader.getType();
		}; break;
		case PT_IEEE_SUB_TYPE : {
			value.type = VT_INTEGER;
			var -> frame.ieeeHeader.delSubType();
			value.value = var -> frame.ieeeHeader.getSubType();
		}; break;
		case PT_IEEE_TO_DS : {
			value.type = VT_INTEGER;
			var -> frame.ieeeHeader.delToDS();
			value.value = var -> frame.ieeeHeader.getToDS();
		}; break;
		case PT_IEEE_FROM_DS : {
			value.type = VT_INTEGER;
			var -> frame.ieeeHeader.delFromDS();
			value.value = var -> frame.ieeeHeader.getFromDS();
		}; break;
		case PT_IEEE_MORE_FRAG : {
			value.type = VT_INTEGER;
			var -> frame.ieeeHeader.delMoreFrag();
			value.value = var -> frame.ieeeHeader.getMoreFrag();
		}; break;
		case PT_IEEE_RETRY : {
			value.type = VT_INTEGER;
			var -> frame.ieeeHeader.delRetry();
			value.value = var -> frame.ieeeHeader.getRetry();
		}; break;
		case PT_IEEE_PWR_MGMT : {
			value.type = VT_INTEGER;
			var -> frame.ieeeHeader.delPwrMgmt();
			value.value = var -> frame.ieeeHeader.getPwrMgmt();
		}; break;
		case PT_IEEE_MORE_DATA : {
			value.type = VT_INTEGER;
			var -> frame.ieeeHeader.delMoreData();
			value.value = var -> frame.ieeeHeader.getMoreData();
		}; break;
		case PT_IEEE_PROTECTED_FRAME : {
			value.type = VT_INTEGER;
			var -> frame.ieeeHeader.delProtectedFrame();
			value.value = var -> frame.ieeeHeader.getProtectedFrame();
		}; break;
		case PT_IEEE_ORDER : {
			value.type = VT_INTEGER;
			var -> frame.ieeeHeader.delOrder();
			value.value = var -> frame.ieeeHeader.getOrder();
		}; break;
		case PT_IEEE_TRANSMITTER_ADDRESS : {
			value.type = VT_STRING;
			var -> frame.ieeeHeader.delTransmitterAddress();
			string str((char *)var -> frame.ieeeHeader.getTransmitterAddress());
			value.str = str;
		}; break;
		case PT_IEEE_RECEIVER_ADDRESS : {
			value.type = VT_STRING;
			var -> frame.ieeeHeader.delReceiverAddress();
			string str((char *)var -> frame.ieeeHeader.getReceiverAddress());
			value.str = str;
		}; break;
		case PT_IEEE_BSSID : {
			value.type = VT_STRING;
			var -> frame.ieeeHeader.delBSSID();
			string str((char *)var -> frame.ieeeHeader.getBSSID());
			value.str = str;
		}; break;
		case PT_IEEE_DURATION : {
			value.type = VT_INTEGER;
			var -> frame.ieeeHeader.delDuration();
			value.value = var -> frame.ieeeHeader.getDuration();
		}; break;
		case PT_IEEE_AID : {
			value.type = VT_INTEGER;
			var -> frame.ieeeHeader.delAid();
			value.value = var -> frame.ieeeHeader.getAid();
		}; break;
		case PT_IEEE_DA : {
			value.type = VT_STRING;
			var -> frame.ieeeHeader.delDA();
			string str((char *)var -> frame.ieeeHeader.getDA());
			value.str = str;
		}; break;
		case PT_IEEE_SA : {
			value.type = VT_STRING;
			var -> frame.ieeeHeader.delSA();
			string str((char *)var -> frame.ieeeHeader.getSA());
			value.str = str;
		}; break;
		case PT_IEEE_SEQ_CTL : {
			value.type = VT_INTEGER;
			var -> frame.ieeeHeader.delSeqCtl();
			value.value = var -> frame.ieeeHeader.getSeqCtl();
		}; break;
		case PT_IEEE_FRAGMENT : {
			value.type = VT_INTEGER;
			var -> frame.ieeeHeader.delFragment();
			value.value = var -> frame.ieeeHeader.getFragment();
		}; break;
		case PT_IEEE_SEQUENCE : {
			value.type = VT_INTEGER;
			var -> frame.ieeeHeader.delSequence();
			value.value = var -> frame.ieeeHeader.getSequence();
		}; break;
		case PT_IEEE_TIMESTAMP : {
			value.type = VT_INTEGER;
			var -> frame.ieeeHeader.delTimestamp();
			value.value = var -> frame.ieeeHeader.getTimestamp();
		}; break;
		case PT_IEEE_BEACON_INTERVAL : {
			value.type = VT_INTEGER;
			var -> frame.ieeeHeader.delBeaconInterval();
			value.value = var -> frame.ieeeHeader.getBeaconInterval();
		}; break;
		case PT_IEEE_CAPABILITY_INFO : {
			value.type = VT_INTEGER;
			var -> frame.ieeeHeader.delCapabilityInfo();
			value.value = var -> frame.ieeeHeader.getCapabilityInfo();
		}; break;
		case PT_IEEE_SSID : {
			value.type = VT_STRING;
			var -> frame.ieeeHeader.delSSID();
			string str((char *)var -> frame.ieeeHeader.getSSID());
		}; break;
		case PT_IEEE_FH_PARAMETER_SET_DWELL_TIME : {
			value.type = VT_INTEGER;
			var -> frame.ieeeHeader.delFHparameterSetDwellTime();
			value.value = var -> frame.ieeeHeader.getFHparameterSetDwellTime();
		}; break;
		case PT_IEEE_FH_PARAMETER_SET_HOP_SET : {
			value.type = VT_INTEGER;
			var -> frame.ieeeHeader.delFHparameterSetHopSet();
			value.value = var -> frame.ieeeHeader.getFHparameterSetHopSet();
		}; break;
		case PT_IEEE_FH_PARAMETER_SET_HOP_PATTERN : {
			value.type = VT_INTEGER;
			var -> frame.ieeeHeader.delFHparameterSetHopPattern();
			value.value = var -> frame.ieeeHeader.getFHparameterSetHopPattern();
		}; break;
		case PT_IEEE_FH_PARAMETER_SET_HOP_INDEX : {
			value.type = VT_INTEGER;
			var -> frame.ieeeHeader.delFHparameterSetHopIndex();
			value.value = var -> frame.ieeeHeader.getFHparameterSetHopIndex();
		}; break;
		case PT_IEEE_FH_PARAMETER_SET : {
			value.type = VT_INTEGER;
			var -> frame.ieeeHeader.delFHparameterSet();
		}; break;
		case PT_IEEE_CF_PARAMETER_SET_COUNT : {
			value.type = VT_INTEGER;
			var -> frame.ieeeHeader.delCFPcount();
			value.value = var -> frame.ieeeHeader.getCFPcount();
		}; break;
		case PT_IEEE_CF_PARAMETER_SET_PERIOD : {
			value.type = VT_INTEGER;
			var -> frame.ieeeHeader.delCFPperiod();
			value.value = var -> frame.ieeeHeader.getCFPperiod();
		}; break;
		case PT_IEEE_CF_PARAMETER_SET_MAX_DURATION : {
			value.type = VT_INTEGER;
			var -> frame.ieeeHeader.delCFPmaxDuration();
			value.value = var -> frame.ieeeHeader.getCFPmaxDuration();
		}; break;
		case PT_IEEE_CF_PARAMETER_SET_REMAINING : {
			value.type = VT_INTEGER;
			var -> frame.ieeeHeader.delCFPremaining();
			value.value = var -> frame.ieeeHeader.getCFPremaining();
		}; break;
		case PT_IEEE_CF_PARAMETER_SET : {
			value.type = VT_INTEGER;
			var -> frame.ieeeHeader.delCFP();
		}; break;
		case PT_IEEE_DS_PARAMETER_SET : {
			value.type = VT_INTEGER;
			var -> frame.ieeeHeader.delDSparameterSet();
			value.value = var -> frame.ieeeHeader.getDSparameterSet();
		}; break;
		case PT_IEEE_IBSS_PARAMETER_SET : {
			value.type = VT_INTEGER;
			var -> frame.ieeeHeader.delIBSSparameterSet();
			value.value = var -> frame.ieeeHeader.getIBSSparameterSet();
		}; break;
		case PT_IEEE_TIM_DTIM_COUNT : {
			value.type = VT_INTEGER;
			var -> frame.ieeeHeader.delTIMdTimCount();
			value.value = var -> frame.ieeeHeader.getTIMdTimCount();
		}; break;
		case PT_IEEE_TIM_DTIM_PERIOD : {
			value.type = VT_INTEGER;
			var -> frame.ieeeHeader.delTIMdTimPeriod();
			value.value = var -> frame.ieeeHeader.getTIMdTimPeriod();
		}; break;
		case PT_IEEE_TIM_BITMAP_CONTROL : {
			value.type = VT_INTEGER;
			var -> frame.ieeeHeader.delTIMbitmapControl();
			value.value = var -> frame.ieeeHeader.getTIMbitmapControl();
		}; break;
		case PT_IEEE_TIM_PARTIAL_VIRTUAL_BITMAP : {
			value.type = VT_STRING;
			var -> frame.ieeeHeader.delTIMpartialVirtualBitmap();
			string str(var -> frame.ieeeHeader.getTIMpartialVirtualBitmap());
			value.str = str;
		}; break;
		case PT_IEEE_TIM : {
			value.type = VT_INTEGER;
			var -> frame.ieeeHeader.delTIM();
			value.value = 0;
		}; break;
		case PT_IEEE_COUNTRY_STRING : {
			value.type = VT_STRING;
			var -> frame.ieeeHeader.delCountryString();
			string str(var -> frame.ieeeHeader.getCountryString());
			value.str = str;
		}; break;
		case PT_IEEE_COUNTRY_CONSTRAINT : {
			Variable index = property -> getValue(0, variableTable);
			value.type = VT_STRING;
			var -> frame.ieeeHeader.delCountryConstraintTirplet(index.value);
			char pom[200];
			ciect d = var -> frame.ieeeHeader.getCountryConstraint(index.value);
			sprintf(pom, "%d %d %d",d.firstChannelNumber, d.numberOfChannel, d.maxTransmitPower);
			string str(pom);
			value.str = str;
		}; break;
		case PT_IEEE_COUNTRY : {
			value.type = VT_INTEGER;
			var -> frame.ieeeHeader.delCountry();
			value.value = 0;
		}; break;
		case PT_IEEE_LOCAL_POWER_CONSTRAINT : {
			value.type = VT_INTEGER;
			var -> frame.ieeeHeader.delLocalPowerConstraint();
			value.value = var -> frame.ieeeHeader.getLocalPowerConstraint();
		}; break;
		case PT_IEEE_CHANNEL_SWITCH_MODE : {
			value.type = VT_INTEGER;
			var -> frame.ieeeHeader.delChannelSwitchMode();
			value.value = var -> frame.ieeeHeader.getChannelSwitchMode();
		}; break;
		case PT_IEEE_NEW_CHANNEL_NUMBER : {
			value.type = VT_INTEGER;
			var -> frame.ieeeHeader.delNewChannelNumber();
			value.value = var -> frame.ieeeHeader.getNewChannelNumber();
		}; break;
		case PT_IEEE_CHANNEL_SWITCH_COUNT : {
			value.type = VT_INTEGER;
			var -> frame.ieeeHeader.delChannelSwitchCount();
			value.value = var -> frame.ieeeHeader.getChannelSwitchCount();
		}; break;
		case PT_IEEE_CHANNEL_SWITCH : {
			value.type = VT_INTEGER;
			var -> frame.ieeeHeader.delChannelSwitch();
			value.value = 0;
		}; break;
		case PT_IEEE_QUIET_COUNT : {
			value.type = VT_INTEGER;
			var -> frame.ieeeHeader.delQuietCount();
			value.value = var -> frame.ieeeHeader.getQuietCount();
		}; break;
		case PT_IEEE_QUIET_PERIOD : {
			value.type = VT_INTEGER;
			var -> frame.ieeeHeader.delQuietPeriod();
			value.value = var -> frame.ieeeHeader.getQuietPeriod();
		}; break;
		case PT_IEEE_QUIET_DURATION : {
			value.type = VT_INTEGER;
			var -> frame.ieeeHeader.delQuietDuration();
			value.value = var -> frame.ieeeHeader.getQuietDuration();
		}; break;
		case PT_IEEE_QUIET_OFFSET : {
			value.type = VT_INTEGER;
			var -> frame.ieeeHeader.delQuietOffset();
			value.value = var -> frame.ieeeHeader.getQuietOffset();
		}; break;
		case PT_IEEE_QUIET : {
			value.type = VT_INTEGER;
			var -> frame.ieeeHeader.delQuiet();
			value.value = 0;
		}; break;
		case PT_IEEE_TRANSMIT_POWER : {
			value.type = VT_INTEGER;
			var -> frame.ieeeHeader.delTransmitPower();
			value.value = var -> frame.ieeeHeader.getTransmitPower();
		}; break;
		case PT_IEEE_LINK_MARGIN : {
			value.type = VT_INTEGER;
			var -> frame.ieeeHeader.delLinkMargin();
			value.value = var -> frame.ieeeHeader.getLinkMargin();
		}; break;
		case PT_IEEE_ERP : {
			value.type = VT_INTEGER;
			var -> frame.ieeeHeader.delERP();
			value.value = var -> frame.ieeeHeader.getERP();
		}; break;
		case PT_IEEE_SUPPORTED_RATE : {
			Variable index = property -> getValue(0, variableTable);
			value.type = VT_INTEGER;
			var -> frame.ieeeHeader.delSupportedRate(index.value);
			value.value = var -> frame.ieeeHeader.getSupportedRate(index.value);
		}; break;
		case PT_IEEE_SUPPORTED_RATE_ALL : {
			value.type = VT_INTEGER;
			var -> frame.ieeeHeader.delSupportedRateAll();
			value.value = 0;
		}; break;
		case PT_IEEE_EXTENDED_SUPPORTED_RATE : {
			Variable index = property -> getValue(0, variableTable);
			value.type = VT_INTEGER;
			var -> frame.ieeeHeader.delExtendedSupportedRate(index.value);
			value.value = var -> frame.ieeeHeader.getExtendedSupportedRate(index.value);
		}; break;
		case PT_IEEE_EXTENDED_SUPPORTED_RATE_ALL : {
			value.type = VT_INTEGER;
			var -> frame.ieeeHeader.delExtendedSupportedRateAll();
			value.value = 0;
		}; break;
		case PT_IEEE_REASON_CODE : {
			value.type = VT_INTEGER;
			var -> frame.ieeeHeader.delReasonCode();
			value.value = var -> frame.ieeeHeader.getReasonCode();
		}; break;
		case PT_IEEE_LISTEN_INTERVAL : {
			value.type = VT_INTEGER;
			var -> frame.ieeeHeader.delListenInterval();
			value.value = var -> frame.ieeeHeader.getListenInterval();
		}; break;
		case PT_IEEE_CURRENT_AP_ADDRESS : {
			value.type = VT_STRING;
			var -> frame.ieeeHeader.delCurrentAPaddress();
			string str((char *)var -> frame.ieeeHeader.getCurrentAPaddress());
			value.str = str;
		}; break;
		case PT_IEEE_RSN_VERSION : {
			value.type = VT_INTEGER;
			var -> frame.ieeeHeader.delRSNversion();
			value.value = var -> frame.ieeeHeader.getRSNversion();
		}; break;
		case PT_IEEE_GROUP_CIPHER_SUITE : {
			value.type = VT_STRING;
			var -> frame.ieeeHeader.delGroupCipherSuite();
			char pom[200];
			suite s = var -> frame.ieeeHeader.getGroupCipherSuite();
			sprintf(pom, "%02x-%02x-%02x, %d\r\n", s.suiteOUI[0], s.suiteOUI[1], s.suiteOUI[2], s.suiteType); 
			string str(str);
			value.str = str;
		}; break;
		case PT_IEEE_PAIRWISE_CIPHER_SUITE : {
			Variable index = property -> getValue(0, variableTable);
			value.type = VT_STRING;
			var -> frame.ieeeHeader.delPairwiseCipherSuite(index.value);
			char pom[200];
			suite s = var -> frame.ieeeHeader.getPairwiseCipherSuite(index.value);
			sprintf(pom, "%02x-%02x-%02x, %d\r\n", s.suiteOUI[0], s.suiteOUI[1], s.suiteOUI[2], s.suiteType); 
			string str(str);
			value.str = str;
		}; break;
		case PT_IEEE_PAIRWISE_CIPHER_SUITE_ALL : {
			value.type = VT_INTEGER;
			var -> frame.ieeeHeader.delPairwiseCipherSuiteAll();
			value.value = 0;
		}; break;
		case PT_IEEE_AUTHENTICATION_SUITE : {
			Variable index = property -> getValue(0, variableTable);
			value.type = VT_STRING;
			var -> frame.ieeeHeader.delAuthenticationSuite(index.value);
			char pom[200];
			suite s = var -> frame.ieeeHeader.getAuthenticationSuite(index.value);
			sprintf(pom, "%02x-%02x-%02x, %d\r\n", s.suiteOUI[0], s.suiteOUI[1], s.suiteOUI[2], s.suiteType); 
			string str(str);
			value.str = str;
		}; break;
		case PT_IEEE_AUTHENTICATION_SUITE_ALL : {
			value.type = VT_INTEGER;
			var -> frame.ieeeHeader.delAuthenticationSuiteAll();
			value.value = 0;
		}; break;
		case PT_IEEE_RSN_CAPABILITIES : {
			value.type = VT_INTEGER;
			var -> frame.ieeeHeader.delRSNcapabilities();
			value.value = var -> frame.ieeeHeader.getRSNcapabilities();
		}; break;
		case PT_IEEE_RSN : {
			value.type = VT_INTEGER;
			var -> frame.ieeeHeader.delRSNcapabilities();
			value.value = 0;
		}; break;
		case PT_IEEE_STATUS_CODE : {
			value.type = VT_INTEGER;
			var -> frame.ieeeHeader.delStatusCode();
			value.value = var -> frame.ieeeHeader.getStatusCode();
		}; break;
		case PT_IEEE_ASSOCIATION_ID : {
			value.type = VT_INTEGER;
			var -> frame.ieeeHeader.delAssociationId();
			value.value = var -> frame.ieeeHeader.getAssociationId();
		}; break;
		case PT_IEEE_AUTHENTICATION_ALGORITHM_NUMBER : {
			value.type = VT_INTEGER;
			var -> frame.ieeeHeader.delAuthenticationAlgorithmNumber();
			value.value = var -> frame.ieeeHeader.getAuthenticationAlgorithmNumber();
		}; break;
		case PT_IEEE_AUTHENTICATION_SEQUENCE_NUMBER : {
			value.type = VT_INTEGER;
			var -> frame.ieeeHeader.delAuthenticationSequenceNumber();
			value.value = var -> frame.ieeeHeader.getAuthenticationSequenceNumber();
		}; break;
		case PT_IEEE_CHALLENGE_TEXT : {
			value.type = VT_STRING;
			var -> frame.ieeeHeader.delChallengeText();
			string str((char *)var -> frame.ieeeHeader.getChallengeText());
			value.str = str;
		}; break;
		case PT_IEEE_QOS : {
			value.type = VT_INTEGER;
			var -> frame.ieeeHeader.delQos();
			value.value = var -> frame.ieeeHeader.getQos();
		}; break;
		case PT_IEEE_ADDRESS1 : {
			value.type = VT_STRING;
			var -> frame.ieeeHeader.delAddress1();
			string str((char *)var -> frame.ieeeHeader.getAddress1());
			value.str = str;
		}; break;
		case PT_IEEE_ADDRESS2 : {
			value.type = VT_STRING;
			var -> frame.ieeeHeader.delAddress2();
			string str((char *)var -> frame.ieeeHeader.getAddress2());
			value.str = str;
		}; break;
		case PT_IEEE_ADDRESS3 : {
			value.type = VT_STRING;
			var -> frame.ieeeHeader.delAddress3();
			string str((char *)var -> frame.ieeeHeader.getAddress3());
			value.str = str;
		}; break;
		case PT_IEEE_ADDRESS4 : {
			value.type = VT_STRING;
			var -> frame.ieeeHeader.delAddress4();
			string str((char *)var -> frame.ieeeHeader.getAddress4());
			value.str = str;
		}; break;
		/******************llc**************************/
		case PT_LLC_DNAP : {
			value.type = VT_INTEGER;
			var -> frame.llcHeader.delDnap();
			value.value = var -> frame.llcHeader.getDnap();
		}; break;
		case PT_LLC_SNAP : {
			value.type = VT_INTEGER;
			var -> frame.llcHeader.delSnap();
			value.value = var -> frame.llcHeader.getSnap();
		}; break;
		case PT_LLC_CONTROL_FIELD : {
			value.type = VT_INTEGER;
			var -> frame.llcHeader.delControlField();
			value.value = var -> frame.llcHeader.getControlField();
		}; break;
		case PT_LLC_ORGANIZATION_CODE: {
			value.type = VT_INTEGER;
			var -> frame.llcHeader.delOrganizationCode();
			value.value = var -> frame.llcHeader.getOrganizationCode();
		}; break;
		case PT_LLC_TYPE: {
			value.type = VT_INTEGER;
			var -> frame.llcHeader.delType();
			value.value = var -> frame.llcHeader.getType();
		}; break;
		case PT_LLC : {
			value.type = VT_INTEGER;
			var -> frame.llcHeader.deleteHeader();
			var -> frame.includeLLCHeader(false);
		}; break; 
		/******************payload**********************/
		case PT_PAYLOAD_DATA : {
			value.type = VT_STRING;
			var -> frame.payload.delData();
			uint8_t data[2*LENGTH_PAYLOAD_DATA];
  		        var -> frame.payload.getData(data);
			string str((char *)data);
			value.str = str;
		}; break;
		case PT_PAYLOAD_CIPHER : {
			value.type = VT_STRING;
			var -> frame.payload.delCipher();
			char strc[200];
  		        var -> frame.payload.getCipherStr(strc);
			string str(strc);
			value.str = str;
		}; break;
		case PT_PAYLOAD_VECTOR : {
			value.type = VT_STRING;
			var -> frame.payload.delIV();
			uint8_t data[2*LENGTH_IV];
  		        var -> frame.payload.getIV(data);
			string str((char *)data);
			value.str = str;
		}; break;
		case PT_PAYLOAD_KEY : {
			value.type = VT_STRING;
			var -> frame.payload.delKey();
			uint8_t data[LENGTH_KEY];
  		        var -> frame.payload.getKey(data);
			string str((char *)data);
			value.str = str;
		}; break;
		case PT_PAYLOAD_KEY_INDEX : {
			value.type = VT_INTEGER;
			var -> frame.payload.delKeyIndex();
			value.value = var -> frame.payload.getKeyIndex();
		}; break;
		case PT_PAYLOAD : {
			value.type = VT_INTEGER;
			var -> frame.payload.delPayload();
			var -> frame.includePayload(false);
		}; break; 
	}
	return value;
}
