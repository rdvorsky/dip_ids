/* Soubor: CommnadStatement.h
 * Autor: Pavel Svanda
 * Email: svandap@gmail.com
 * Datum vytvoreni: 17.3.2013
 * Popis: deklarace tridy CommnadStatement, ktera slouzi k ulozeni prikazu, ktery je nasledne vykonan
 */
#ifndef COMMAND_STATEMENT_H
#define COMMAND_STATEMENT_H

#include "Statement.h"
#include "VariableValueExpression.h"
#include <list>

#define ST_SEND		1
#define ST_DELAY	2	
#define ST_PRINT	3
#define ST_LIST		4
#define ST_LIST_ALL 	5
#define ST_DUMP		6
#define ST_LOAD		7
#define ST_SAVE		8
#define ST_CREATE_ALIAS	9
#define ST_DEL_ALIAS	10
#define ST_LIST_ALIAS	11
#define ST_ISEND	12
#define ST_ISEND_LIST	13
#define ST_SLEEP	14
#define ST_MSLEEP	21
#define ST_USLEEP	22
#define ST_CAPTURE	15
#define ST_CAPTURE_RET	23
#define ST_PARSE_IV	16
#define ST_PARSE_IV_VAR 24
#define ST_KEY_START	17
#define ST_KEY_LIST	18
#define ST_KEY_GTK	19
#define ST_KEY_PTK	20
#define ST_DEL		25
#define ST_BREAK	26
#define ST_TIME_START	27
#define ST_TIME		28
#define ST_KEY_IWLIST	29
#define ST_STR		30
#define ST_SCAPY	31

using namespace std;

typedef list<AExpression *> ParameterList;
typedef	list<AExpression *>::iterator ParameterListIt;

class CommandStatement : public Statement {
	private:
		int statementType;
		ParameterList   parameterList;
		ParameterListIt parameterListIt;
	public:
		CommandStatement();
		CommandStatement(int statementType);
	        virtual ~CommandStatement();

		void setStatementType(int statementType);
		void addParameter(AExpression * parameter);
		virtual bool executeStatement(InterpreterData interpreterData);
};
#endif
