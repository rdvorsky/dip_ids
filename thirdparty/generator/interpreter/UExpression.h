/* Soubor: UExpression.h
 * Autor: Pavel Svanda
 * Email: svandap@gmail.com
 * Datum vytvoreni: 17.3.2013
 * Popis: deklarace tridy UExpression, ktera slouzi k ulozeni unarniho vyrazu
 */
#ifndef UEXPRESSION_H
#define UEXPRESSION_H


#include "AExpression.h"
#include "NumberValueExpression.h"
#include "VariableValueExpression.h"

class UExpression : public AExpression {
	private:
		AExpression * expression;

		int operation;
	public:
		UExpression();
		UExpression(AExpression * expression, int operation);
	        virtual ~UExpression();

		void setExpression(AExpression * expression);
		virtual Variable evaluateExpression(VariableTable * variableTable);
};
#endif
