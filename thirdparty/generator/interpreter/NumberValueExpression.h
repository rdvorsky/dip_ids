/* Soubor: NumberValueExpression.h
 * Autor: Pavel Svanda
 * Email: svandap@gmail.com
 * Datum vytvoreni: 17.3.2013
 * Popis: deklarace tridy NumberValueExpression, ktera slouzi k ulozeni ciselne hodnoty
 */
#ifndef NUMBER_VALUE_EXPRESSION_H
#define NUMBER_VALUE_EXPRESSION_H

#include "AExpression.h"

class NumberValueExpression : public AExpression {
	private:
		int value;
	public:
		NumberValueExpression();
		NumberValueExpression(int value);
	        virtual ~NumberValueExpression();

		void setValue(int value);
		virtual Variable evaluateExpression(VariableTable * variableTable);
};
#endif
