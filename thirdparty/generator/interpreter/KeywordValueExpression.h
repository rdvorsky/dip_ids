/* Soubor: KeywordValueExpression.h
 * Autor: Pavel Svanda
 * Email: svandap@gmail.com
 * Datum vytvoreni: 17.3.2013
 * Popis: deklarace tridy KeywordValueExpression, ktera slouzi k ulozeni keywordu
 */
#ifndef KEYWORD_VALUE_EXPRESSION_H
#define KEYWORD_VALUE_EXPRESSION_H

#include "AExpression.h"

class KeywordValueExpression : public AExpression {
	private:
		Keyword keyword;
	public:
		KeywordValueExpression();
		KeywordValueExpression(Keyword keyword);
	        virtual ~KeywordValueExpression();

		void setValue(Keyword keyword);
		virtual Variable evaluateExpression(VariableTable * variableTable);
};
#endif
