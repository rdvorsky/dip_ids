/* Soubor: BExpression.h
 * Autor: Pavel Svanda
 * Email: svandap@gmail.com
 * Datum vytvoreni: 17.3.2013
 * Popis: deklarace tridy BExpression, ktera slouzi k ulozeni binarniho vyrazu
 */
#ifndef BEXPRESSION_H
#define BEXPRESSION_H


#include "AExpression.h"
#include "NumberValueExpression.h"
#include "StringValueExpression.h"
#include "VariableValueExpression.h"
#include "FrameValueExpression.h"
#include "KeywordValueExpression.h"

class BExpression : public AExpression {
	private:
		AExpression * leftExpression;
		AExpression * rightExpression;		

		int operation;
	public:
		BExpression();
		BExpression(AExpression * leftExpression, AExpression * rightExpression, int operation);
	        virtual ~BExpression();

		void setLeftExpression(AExpression * expression);
		void setRightExpression(AExpression * expression);
		virtual Variable evaluateExpression(VariableTable * variableTable);
};
#endif
