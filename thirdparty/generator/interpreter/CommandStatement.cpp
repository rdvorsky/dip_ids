/* Soubor: CommandStatement.cpp
 * Autor: Pavel Svanda
 * Email: svandap@gmail.com
 * Datum vytvoreni: 17.3.2013
 * Popis: definice tridy CommnadStatement, ktera slouzi k ulozeni prikazu, ktery je nasledne vykonan
 */
#include "CommandStatement.h"

/* CommandStatement::CommandStatement
 * Popis: konstruktor
 * Parametry: -
 * Navratova hodnota: -
 */
CommandStatement::CommandStatement() {
	this -> statementType = 0;
}

/* CommandStatement::CommandStatement
 * Popis: konstruktor
 * Parametry: int statementType - typ prikazu
 * Navratova hodnota: -
 */
CommandStatement::CommandStatement(int statementType) {
	this -> statementType = statementType;
}

/* CommandStatement::~CommandStatement
 * Popis: destruktor
 * Parametry: -
 * Navratova hodnota: -
 */
CommandStatement::~CommandStatement() {
	for(this -> parameterListIt = this -> parameterList.begin(); this -> parameterListIt != this -> parameterList.end(); this -> parameterListIt++) {
		delete *this -> parameterListIt;
	}
	this -> parameterList.erase(this -> parameterList.begin(), this -> parameterList.end());
}

/* CommandStatement::setStatementType
 * Popis: funkce provede nastaveni typu prikazu
 * Parametry: int statementType
 * Navratova hodnota: -
 */
void CommandStatement::setStatementType(int statementType) {
	this -> statementType = statementType;
}

/* CommandStatement::addParameter
 * Popis: funkce provede pridani parametru prikazu
 * Parametry: AExpression * parameter - parametr prikazu
 * Navratova hodnota: -
 */
void CommandStatement::addParameter(AExpression * parameter) {
	this -> parameterList.push_back(parameter);
}

/* CommandStatement::executeStatement
 * Popis: funkce provede vyhodnoceni prikazu
 * Parametry: InterpreterData interpreterData - data interpretru, ktere mohou jednotlive prikazi vyzadovat k jejich vykonani
 * Navratova hodnota: navratova hodnota prikazu
 */
bool CommandStatement::executeStatement(InterpreterData interpreterData) {
	VariableTable * variableTable = interpreterData.variableTable;
	Configuration * configuration = interpreterData.configuration;
        WiFiInterface * interface     = interpreterData.interface;
	KeyLoader * keyLoader	      = interpreterData.keyLoader;
	bool * breakLoop	      = interpreterData.breakLoop;
	timeval * timer		      = interpreterData.timer;	    

	switch(this -> statementType) {
		case ST_LIST: {
			variableTable -> showFrameVariable();
		}; break;
		case ST_LIST_ALL: {
			variableTable -> showAllVariable();
		}; break;
		case ST_PRINT: {
			this -> parameterListIt = this -> parameterList.begin();
			AExpression * parameter = *this -> parameterListIt;
			Variable var = parameter -> evaluateExpression(variableTable);
			switch(var.type) {
				case VT_INTEGER : {
					printf("\r%lld\r\n", var.value);
				}; break;
				case VT_CAPTURE_FRAME : {
					printf("0x\r%s\r\n", var.str.c_str());
				}; break;
				case VT_STRING : {
					printf("\r%s\r\n", var.str.c_str());
				}; break;
				case VT_FRAME : {
					var.frame.printFrame();
				}; break;
				default : {
					printf("\r%s\r\n", LANG_INTERPERTER_UNKNOWN_VARIABLE);
				}
			}
		}; break;
		case ST_DUMP: {
			this -> parameterListIt = this -> parameterList.begin();
			AExpression * parameter = *this -> parameterListIt;
			Variable var = parameter -> evaluateExpression(variableTable);
			switch(var.type) {
				case VT_FRAME : {
					var.frame.dumpFrame();
					char fName[LENGTH_VARIABLE_NAME];
					parameter -> getVariableName(fName);
					variableTable -> setFrameVariable(fName, var.frame);
				}; break;
				case VT_CAPTURE_FRAME : {
					uint8_t frame[WI_LENGTH_CAPTURED_FRAME];
					int lengthFrame = Utilities::strToHex(frame, (char *)var.str.c_str());
					Utilities::hexDump(NULL, (void *)frame, lengthFrame);
				}; break;
				case VT_STRING : {
					 Utilities::hexDump (NULL, (void *)var.str.c_str(), strlen(var.str.c_str()));					
				}; break;
				case VT_INTEGER: {
					char pom[sizeof(var.value)];
					uint64_t m = 0x00000000000000ff;
					for(int i = 0; i < sizeof(var.value); i++) {
						pom[i] = var.value & m;
						var.value = var.value >> 8;						
					}
					Utilities::hexDump(NULL, (void *)pom, sizeof(var.value));
				}; break;
				default : {
					printf("\t%s\r\n", LANG_INTERPERTER_UNKNOWN_VARIABLE);
				}
			}
		}; break;
		case ST_STR: {
			this -> parameterListIt = this -> parameterList.begin();
			AExpression * parameter = *this -> parameterListIt;
			Variable var = parameter -> evaluateExpression(variableTable);
			switch(var.type) {
				case VT_FRAME : {
					var.frame.strFrame();
					char fName[LENGTH_VARIABLE_NAME];
					parameter -> getVariableName(fName);
					variableTable -> setFrameVariable(fName, var.frame);
				}; break;
				case VT_CAPTURE_FRAME : {
					uint8_t frame[WI_LENGTH_CAPTURED_FRAME];
					int lengthFrame = Utilities::strToHex(frame, (char *)var.str.c_str());
					Utilities::hexStr(NULL, (void *)frame, lengthFrame);
				}; break;
				case VT_STRING : {
					 printf("\r%s\r\n", var.str.c_str());
				}; break;
				case VT_INTEGER: {
					printf("\r%lld\r\n", var.value);
				}; break;
				default : {
					printf("\t%s\r\n", LANG_INTERPERTER_UNKNOWN_VARIABLE);
				}
			}
		}; break;
		case ST_LOAD: {
			this -> parameterListIt = this -> parameterList.begin();
			AExpression * frameName = *this -> parameterListIt;
			this -> parameterListIt++; 
			AExpression * pathName  = *this -> parameterListIt;

			char fName[LENGTH_VARIABLE_NAME];
			frameName -> getVariableName(fName);
			Variable frame = variableTable -> getVariable(fName);
			frame.type = VT_FRAME;

			char pName[MAX_LENGTH_ALIAS_AND_PATH];
			if(typeid(*pathName) == typeid(VariableValueExpression)) {
				pathName -> getVariableName(pName);
				if(!configuration->alias.getPath(pName, pName)) {
					printf("\t%s\r\n", LANG_ALIAS_NOT_EXIST);
					return true;
				}
			} else {
				Variable path = pathName -> evaluateExpression(variableTable);
				sprintf(pName, "%s", path.str.c_str());
			}	
			if(!frame.frame.loadFrame(pName)) {
				printf("\t%s\r\n", LANG_FRAME_READ_KO);
			} else {
				variableTable -> setFrameVariable(fName, frame.frame);
				printf("\t%s\r\n", LANG_FRAME_READ_OK);
			}
		}; break;
		case ST_SAVE: {
			this -> parameterListIt = this -> parameterList.begin();
			AExpression * frameName = *this -> parameterListIt;
			this -> parameterListIt++; 
			AExpression * pathName  = *this -> parameterListIt;

			char fName[LENGTH_VARIABLE_NAME];
			frameName -> getVariableName(fName);
			Variable frame = variableTable -> getVariable(fName);
			frame.type = VT_FRAME;

			char pName[MAX_LENGTH_ALIAS_AND_PATH];
			if(typeid(*pathName) == typeid(VariableValueExpression)) {
				pathName -> getVariableName(pName);
				if(!configuration->alias.getPath(pName, pName)) {
					printf("\t%s\r\n", LANG_ALIAS_NOT_EXIST);
					return true;
				}
			} else {
				Variable path = pathName -> evaluateExpression(variableTable);
				sprintf(pName, "%s", path.str.c_str());
			}	
			if(!frame.frame.saveFrame(pName)) {
				printf("\t%s\r\n", LANG_FRAME_SAVE_KO);				
			} else {
				printf("\t%s\r\n", LANG_FRAME_SAVE_OK);	
				variableTable -> setFrameVariable(fName, frame.frame);
			}
		}; break;
		case ST_CREATE_ALIAS : {
			this -> parameterListIt = this -> parameterList.begin();
			AExpression * aliasName = *this -> parameterListIt;
			this -> parameterListIt++; 
			AExpression * pathName  = *this -> parameterListIt;

			char name[LENGTH_VARIABLE_NAME];
			aliasName -> getVariableName(name);
			Variable path = pathName -> evaluateExpression(variableTable);
			configuration -> alias.setAlias(name, (char *)path.str.c_str());
			printf("\t%s\r\n", LANG_ALIAS_SAVE_OK);
			configuration -> saveConfiguration();
		}; break;
		case ST_DEL_ALIAS : {
			this -> parameterListIt = this -> parameterList.begin();
			AExpression * aliasName = *this -> parameterListIt;
			char name[LENGTH_VARIABLE_NAME];
			aliasName -> getVariableName(name);
			if(configuration -> alias.delAlias(name)) {
				printf("\t%s\r\n", LANG_ALIAS_DEL_OK);
			} else {
				printf("\t%s\r\n", LANG_ALIAS_DEL_KO);
			}
		}; break;
		case ST_LIST_ALIAS : {
			if(!configuration -> alias.listAlias()) {
				printf("\t%s\r\n", LANG_ALIAS_LIST_EMPTY);
			}
		}; break;
		case ST_ISEND : {
			this -> parameterListIt = this -> parameterList.begin();
			AExpression * frameName = *this -> parameterListIt;
			Variable varFrame = frameName -> evaluateExpression(variableTable);
			char name[LENGTH_VARIABLE_NAME];
			frameName -> getVariableName(name);

			this -> parameterListIt++; 
			AExpression * delay = *this -> parameterListIt;
			Variable varDelay = delay -> evaluateExpression(variableTable);
			this -> parameterListIt++; 
			AExpression * repeat  = *this -> parameterListIt;
			Variable varRepeat = repeat -> evaluateExpression(variableTable);

			if(varFrame.type == VT_FRAME) {
			  	uint8_t data [MAX_DATA_LENGTH];
				int sizeFrame = varFrame.frame.generateFrame(data);
				interface -> sendFrame((char *)name, data, sizeFrame, varDelay.value, varRepeat.value);
				char fName[LENGTH_VARIABLE_NAME];
				frameName -> getVariableName(fName);
				variableTable -> setFrameVariable(fName, varFrame.frame);
			} else {
				printf("\t%s\r\n", LANG_FRAME_NOT_EXIST);
			}
		}; break;
		case ST_SEND : {
			this -> parameterListIt = this -> parameterList.begin();
			AExpression * frameName = *this -> parameterListIt;
			Variable varFrame = frameName -> evaluateExpression(variableTable);
			char name[LENGTH_VARIABLE_NAME];
			frameName -> getVariableName(name);

			if(varFrame.type == VT_FRAME) {
			  	uint8_t data [MAX_DATA_LENGTH];
				int sizeFrame = varFrame.frame.generateFrame(data);
				char fName[LENGTH_VARIABLE_NAME];
				frameName -> getVariableName(fName);
				variableTable -> setFrameVariable(fName, varFrame.frame);
				interface -> sendFrameB(data, sizeFrame);
			} else {
				printf("\t%s\r\n", LANG_FRAME_NOT_EXIST);
			}
		}; break;
		case ST_SLEEP : {
			this -> parameterListIt = this -> parameterList.begin();
			AExpression * parameter = *this -> parameterListIt;
			Variable var = parameter -> evaluateExpression(variableTable);
			sleep(var.value);
		}; break;
		case ST_MSLEEP : {
			this -> parameterListIt = this -> parameterList.begin();
			AExpression * parameter = *this -> parameterListIt;
			Variable var = parameter -> evaluateExpression(variableTable);
			usleep(1000 * var.value);
		}; break;
		case ST_USLEEP : {
			this -> parameterListIt = this -> parameterList.begin();
			AExpression * parameter = *this -> parameterListIt;
			Variable var = parameter -> evaluateExpression(variableTable);
			usleep(var.value);
		}; break;
		case ST_CAPTURE : {
			this -> parameterListIt = this -> parameterList.begin();
			AExpression * parameter = *this -> parameterListIt;
			Variable varFilter = parameter -> evaluateExpression(variableTable);
			if(interface -> captureFrame((char *)varFilter.str.c_str())) {
				//ramec mame odchycen
				printf("\t%s\r\n", LANG_INTERPRETER_CAPTURE_OK);
			} else {
				//ramec neni odchycen
				printf("\t%s\r\n", LANG_INTERPRETER_CAPTURE_KO);
			}
		}; break;
		case ST_CAPTURE_RET : {
			this -> parameterListIt = this -> parameterList.begin();
			AExpression * varName = *this -> parameterListIt;
			this -> parameterListIt++;
			AExpression * parameter = *this -> parameterListIt;	
			Variable varFilter = parameter -> evaluateExpression(variableTable);
			if(interface -> captureFrame((char *)varFilter.str.c_str())) {
				//ramec mame odchycen
				char name[LENGTH_VARIABLE_NAME];
				varName -> getVariableName(name);

				uint8_t frame[WI_LENGTH_CAPTURED_FRAME];
				int lengthFrame = interface -> getCapturedFrame(frame);	
				char frameStr[WI_LENGTH_CAPTURED_FRAME * 2 + 1];
				int k = 0;
				for(int i = 0; i < lengthFrame; i++) {
					sprintf(&frameStr[k], "%02x", frame[i]);
					k +=2;
				}
				frameStr[k] = 0;
				variableTable -> setCaptureFrameVariable(name, frameStr);
				printf("\t%s\r\n", LANG_INTERPRETER_CAPTURE_OK);
			} else {
				//ramec neni odchycen
				printf("\t%s\r\n", LANG_INTERPRETER_CAPTURE_KO);
			}
		}; break;
		case ST_ISEND_LIST : {
			interface -> sendFrameSummary();
		}; break;
		case ST_PARSE_IV : {
			this -> parameterListIt = this -> parameterList.begin();
			AExpression * varName = *this -> parameterListIt;
			char name[LENGTH_VARIABLE_NAME];
			varName -> getVariableName(name);

			uint8_t frame[WI_LENGTH_CAPTURED_FRAME];
			int lengthFrame = interface -> getCapturedFrame(frame);	
			uint8_t iv[LENGTH_IV];
			int lengthIV = Frame::parseIV(lengthFrame, frame, iv);
			long long int ivNumber = 0;
			if(lengthIV != 0) {
				ivNumber = strtoll((char *)iv, NULL, 16);
			}
			variableTable -> setIntVariable(name, ivNumber);
		}; break;
		case ST_PARSE_IV_VAR : {
			this -> parameterListIt = this -> parameterList.begin();
			AExpression * varName = *this -> parameterListIt;
			char name[LENGTH_VARIABLE_NAME];
			varName -> getVariableName(name);

			this -> parameterListIt++;
			AExpression * parameter = *this -> parameterListIt;
			Variable data = parameter -> evaluateExpression(variableTable);
			long long int ivNumber = 0;
			switch(data.type) {
				case VT_STRING : {};
				case VT_CAPTURE_FRAME : {
					uint8_t frame[WI_LENGTH_CAPTURED_FRAME];
					int lengthFrame = Utilities::strToHex(frame, (char *)data.str.c_str());			
					uint8_t iv[LENGTH_IV];
					int lengthIV = Frame::parseIV(lengthFrame, frame, iv);
					if(lengthIV != 0) {
						ivNumber = strtoll((char *)iv, NULL, 16);
					}
				}; break;
				default : {
					printf("\t%s\r\n", LANG_INTERPRETER_BAD_TYPE);
				}; 
			}
			variableTable -> setIntVariable(name, ivNumber);
		}; break;
		case ST_KEY_START : {
			this -> parameterListIt = this -> parameterList.begin();
			AExpression * parameter = *this -> parameterListIt;
			Variable varInterface = parameter -> evaluateExpression(variableTable);

			this -> parameterListIt++; 
			parameter = *this -> parameterListIt;
			Variable varConfig;
			if(typeid(*parameter) == typeid(VariableValueExpression)) {
				char pName[MAX_LENGTH_ALIAS_AND_PATH];
				parameter -> getVariableName(pName);
				if(!configuration->alias.getPath(pName, pName)) {
					printf("\t%s\r\n", LANG_ALIAS_NOT_EXIST);
					return true;
				}
				string var(pName);
				varConfig.str = var;
			} else {
				varConfig = parameter -> evaluateExpression(variableTable);
			}

			this -> parameterListIt++; 
			parameter = *this -> parameterListIt;
			Variable procName = parameter -> evaluateExpression(variableTable);

			if(!keyLoader -> startLoadKey((char *) varInterface.str.c_str(), (char *)varConfig.str.c_str(), (char *) procName.str.c_str())) {
				printf("%s\r\n",keyLoader -> getErrorLog());
			}
		}; break;
		case ST_KEY_IWLIST : {
			this -> parameterListIt = this -> parameterList.begin();
			AExpression * parameter = *this -> parameterListIt;
			Variable varInterface = parameter -> evaluateExpression(variableTable);

			if(!keyLoader -> loadGTK((char *) varInterface.str.c_str())) {
				printf("%s\r\n",keyLoader -> getErrorLog());
			}
		}; break;
		case ST_KEY_LIST : {
			keyLoader -> showKeys();
		}; break;
		case ST_KEY_GTK : {
			this -> parameterListIt = this -> parameterList.begin();
			AExpression * varName = *this -> parameterListIt;
			char name[LENGTH_VARIABLE_NAME];
			varName -> getVariableName(name);

			uint8_t key[KL_KEY_LENGTH];
			int lengthKey = keyLoader -> getGTK(key);
			variableTable -> setStringVariable(name, (char *)key);			
		}; break;
		case ST_KEY_PTK : {
			this -> parameterListIt = this -> parameterList.begin();
			AExpression * varName = *this -> parameterListIt;
			char name[LENGTH_VARIABLE_NAME];
			varName -> getVariableName(name);

			uint8_t key[KL_KEY_LENGTH];
			int lengthKey = keyLoader -> getPTK(key);
			variableTable -> setStringVariable(name, (char *)key);						
		}; break;
		case ST_DEL : {
			this -> parameterListIt = this -> parameterList.begin();
			AExpression * varName = *this -> parameterListIt;
			char name[LENGTH_VARIABLE_NAME];
			varName -> getVariableName(name);
			if(variableTable -> delVariable(name)) {
				printf("\t%s\r\n", LANG_INTERPERTER_DEL_OK);
			} else {
				printf("\t%s\r\n", LANG_INTERPERTER_DEL_KO);
			}
		}; break;
		case ST_SCAPY : {
			this -> parameterListIt = this -> parameterList.begin();
			AExpression * varName = *this -> parameterListIt;
			char name[LENGTH_VARIABLE_NAME];
			varName -> getVariableName(name);

			this -> parameterListIt++;
			AExpression * parameter = *this -> parameterListIt;
			Variable data = parameter -> evaluateExpression(variableTable);
			char scapyData[SCAPY_MAX_DATA_LENGTH];
			switch(data.type) {
				case VT_STRING : {
					int length = Scapy::loadData(data.str.c_str(), scapyData);
				}; break;
				default : {
					printf("\t%s\r\n", LANG_INTERPRETER_BAD_TYPE);
				}; 
			}		
			variableTable -> setCaptureFrameVariable(name, (char *)scapyData);
		}; break;
		case ST_BREAK : {
			return false;
		}; break;
		case ST_TIME_START : {
			timeval now;
			gettimeofday(&now, NULL);
			timer -> tv_sec  = now.tv_sec;
			timer -> tv_usec = now.tv_usec;
			return true;
		}; break;	
		case ST_TIME : {
			timeval now;
			gettimeofday(&now, NULL);			
			long long int useconds = (now.tv_usec + (1000000 - timer -> tv_usec));
			long long int seconds  = now.tv_sec  - (timer -> tv_sec + 1);
			int over;
			if(useconds >= 1000000) { over = useconds / 1000000; seconds += over; useconds = useconds - (over * 1000000); }
			unsigned long long int hours       = seconds / 3600;
			unsigned long long int minute      = (seconds - (hours * 3600)) / 60;
			unsigned long long int second      = seconds - (hours * 3600 + minute * 60);
			unsigned long long int milisecond  = useconds / 1000;
			unsigned long long int mikrosecond = useconds - (milisecond * 1000);
			printf("% 2lldh:%02lldm:%02llds:%03lldms:%03lldus\n", hours, minute, second, milisecond, mikrosecond);
			return true;
		}; break;
		default : {
			printf("%s\r\n", LANG_INTERPRETER_BAD_COMMAND);
		}
	}
	return true;
}
