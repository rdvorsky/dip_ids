/* Soubor: BExpression.cpp
 * Autor: Pavel Svanda
 * Email: svandap@gmail.com
 * Datum vytvoreni: 17.3.2013
 * Popis: definice tridy BExpression, ktera slouzi k ulozeni binarniho vyrazu
 */
#include "BExpression.h"

/* BExpression::BExpression
 * Popis: konstruktor
 * Parametry: -
 * Navratova hodnota: -
 */
BExpression::BExpression() {
	this -> leftExpression = NULL;
	this -> rightExpression = NULL;
	this -> operation = 0;
}

/* BExpression::BExpression
 * Popis: konstruktor
 * Parametry: AExpression * leftExpression  - levy operator vyrazu
 *            AExpression * rightExpression - pravy operator vyrazu
 *            int operation                 - typ operace
 * Navratova hodnota: -
 */
BExpression::BExpression(AExpression * leftExpression, AExpression * rightExpression, int operation) {
	this -> leftExpression = leftExpression;
	this -> rightExpression = rightExpression;
	this -> operation = operation;
}

/* BExpression::~BExpression
 * Popis: destruktor
 * Parametry: -
 * Navratova hodnota: -
 */
BExpression::~BExpression() {
	if(this -> leftExpression != NULL)
		delete this -> leftExpression;
	if(this -> rightExpression != NULL)
		delete this -> rightExpression;
}

/* BExpression::setLeftExpression
 * Popis: funkce nastavy levy operator vyrazu
 * Parametry: AExpression * expression - nastavovany vyraz
 * Navratova hodnota: -
 */
void BExpression::setLeftExpression(AExpression * expression) {
	if(this -> leftExpression != NULL)
		delete this -> leftExpression;
	this -> leftExpression = expression;
}

/* BExpression::setRightExpression
 * Popis: funkce nastavy pravy operator vyrazu
 * Parametry: AExpression * expression - nastavovany vyraz
 * Navratova hodnota: -
 */
void BExpression::setRightExpression(AExpression * expression) {
	if(this -> rightExpression != NULL)
		delete this -> rightExpression;
	this -> rightExpression = expression;
}

/* BExpression::evaluateExpression
 * Popis: funkce provede vykonani vyrazu
 * Parametry: VariableTable * variableTable - tabulka promennych, ze kterych muze cist vlastni hodnoty
 * Navratova hodnota: -
 */
Variable BExpression::evaluateExpression(VariableTable * variableTable) {
	Variable lval = this -> leftExpression -> evaluateExpression(variableTable);
	Variable rval = this -> rightExpression -> evaluateExpression(variableTable);
	Variable retVal;
	retVal.type = VT_INTEGER;

	if(!(lval.type == rval.type && rval.type == VT_INTEGER) && 
	   !(((lval.type == VT_STRING && rval.type == VT_INTEGER) || (lval.type == VT_INTEGER && rval.type == VT_STRING) ||
             (lval.type == VT_STRING && rval.type == VT_STRING)) && this -> operation == OP_PLUS) &&
           !(typeid(*this -> leftExpression) == typeid(VariableValueExpression) && this -> operation == OP_ASSIGN)) {
		printf("\t%s\r\n", LANG_INTERPRETER_BAD_TYPE);
		retVal.value = 0;
		return retVal;
	}

	switch(this -> operation) {
		case OP_PLUS : {
			if(lval.type == VT_STRING || rval.type == VT_STRING) {
				char str[5000];
				retVal.type = VT_STRING;
				if(lval.type == VT_INTEGER) {
					sprintf(str, "%lld", lval.value);
				} else {
					sprintf(str, "%s", lval.str.c_str());	
				}
				if(rval.type == VT_INTEGER) {
					sprintf(str, "%s%lld", str, rval.value);
				} else {
					sprintf(str, "%s%s", str, rval.str.c_str());
				}
				string var(str);
				retVal.str = var;
			} else {
				retVal.value = lval.value  + rval.value;
			}
			return retVal;
		}; break;
		case OP_MINUS : {
			retVal.value = lval.value  - rval.value;
			return retVal;
		}; break;
		case OP_MUL : {
			retVal.value = lval.value  * rval.value;
			return retVal;
		}; break;
		case OP_DIV : {
			retVal.value = lval.value  / rval.value;
			return retVal;
		}; break;
		case OP_MOD : {
			retVal.value = lval.value  % rval.value;
			return retVal;
		}; break;
		case OP_LT : {
			retVal.value = lval.value  < rval.value;
			return retVal;
		}; break;
		case OP_GT : {
			retVal.value = lval.value  > rval.value;
			return retVal;
		}; break;
		case OP_LE : {
			retVal.value = lval.value <= rval.value;
			return retVal;
		}; break;
		case OP_GE : {
			retVal.value = lval.value >= rval.value;
			return retVal;
		}; break;
		case OP_EQ : {
			retVal.value = lval.value == rval.value;
			return retVal;
		}; break;
		case OP_NE : {
			retVal.value = lval.value != rval.value;
			return retVal;
		}; break;
		case OP_ORL : {
			retVal.value = lval.value || rval.value;
			return retVal;
		}; break;
		case OP_ANDL : {
			retVal.value = lval.value && rval.value;
			return retVal;
		}; break;
		case OP_OR : {
			retVal.value = lval.value  | rval.value;
			return retVal;
		}; break;
		case OP_AND : {
			retVal.value = lval.value  & rval.value;
			return retVal;
		}; break;
		case OP_ASSIGN : {
			if(typeid(*this -> leftExpression) == typeid(VariableValueExpression)) {
				char varName [LENGTH_VARIABLE_NAME];
				this -> leftExpression -> getVariableName(varName);
				retVal.type = rval.type;
				switch(rval.type) {
					case VT_INTEGER : {
						variableTable -> setIntVariable(varName, rval.value);
						retVal.value = rval.value;
					}; break;
					case VT_FRAME : {	
						variableTable -> setFrameVariable(varName, rval.frame);
						retVal.frame = rval.frame;
					}; break;
					case VT_STRING : {
						variableTable -> setStringVariable(varName, (char *)rval.str.c_str());
						retVal.str = rval.str;
					}; break;
					case VT_CAPTURE_FRAME : {
						variableTable -> setCaptureFrameVariable(varName, (char *)rval.str.c_str());
						retVal.str = rval.str;
					}; break;
				}				
				return retVal;
			}
			retVal.value = 0;
			return retVal;
		}; break;
	}
}
