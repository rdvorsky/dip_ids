/* Soubor: StringValueExpression.h
 * Autor: Pavel Svanda
 * Email: svandap@gmail.com
 * Datum vytvoreni: 17.3.2013
 * Popis: deklarace tridy StringValueExpression, ktera slouzi k ulozeni retezce
 */
#ifndef STRING_VALUE_EXPRESSION_H
#define STRING_VALUE_EXPRESSION_H

#include "AExpression.h"

class StringValueExpression : public AExpression {
	private:
		string value;
	public:
		StringValueExpression();
		StringValueExpression(char * value);
	        virtual ~StringValueExpression();

		void setValue(char * value);
		virtual Variable evaluateExpression(VariableTable * variableTable);
};
#endif
