/* Soubor: BodyStatement.cpp
 * Autor: Pavel Svanda
 * Email: svandap@gmail.com
 * Datum vytvoreni: 17.3.2013
 * Popis: definice tridy BodyStatement, ktera slouzi k ulozeni seznamu prikazu
 */
#include "BodyStatement.h"

/* BodyStatement::BodyStatement
 * Popis: konstruktor
 * Parametry: -
 * Navratova hodnota: -
 */
BodyStatement::BodyStatement() {
}

/* BodyStatement::~BodyStatement
 * Popis: destruktor
 * Parametry: -
 * Navratova hodnota: -
 */
BodyStatement::~BodyStatement() {
	for(this -> statementListIt = this -> statementList.begin(); this -> statementListIt != this -> statementList.end(); this -> statementListIt++) {
		delete *this -> statementListIt;
	}
	this -> statementList.erase(this -> statementList.begin(), this -> statementList.end());
}

/* BodyStatement::addStatement
 * Popis: funkce prida novy prikaz do tela slozeneho prikazu
 * Parametry: Statement * statement - pridavany prikaz
 * Navratova hodnota: -
 */
void BodyStatement::addStatement(Statement * statement) {
	this -> statementList.push_back(statement);
}

/* BodyStatement::executeStatement
 * Popis: funkce prevede vykonani vsech prikazu, ktere jsou ulozeny v tele slozeneho prikazu
 * Parametry: InterpreterData interpreterData - sturktura obsahujici potrebne struktury pro vykonani prikazu
 * Navratova hodnota: navratova hodnota prikazu
 */
bool BodyStatement::executeStatement(InterpreterData interpreterData) {

	for(this -> statementListIt = this -> statementList.begin(); this -> statementListIt != this -> statementList.end(); this -> statementListIt++) {
		Statement * statement = *this -> statementListIt;
		if(statement != NULL && !statement -> executeStatement(interpreterData))
			return false;
	}
	return true;
}
