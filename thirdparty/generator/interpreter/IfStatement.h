/* Soubor: IfStatement.h
 * Autor: Pavel Svanda
 * Email: svandap@gmail.com
 * Datum vytvoreni: 17.3.2013
 * Popis: deklarace tridy FfStatement, ktera slouzi k ulozeni prikazu if
 */
#ifndef IF_STATEMENT_H
#define IF_STATEMENT_H

#include "Statement.h"
#include "AExpression.h"
#include "BodyStatement.h"
#include "ExpressionStatement.h"
#include "AssignStatement.h"
#include "BExpression.h"
#include "UExpression.h"
#include "NumberValueExpression.h"
#include "VariableValueExpression.h"


class IfStatement : public Statement {
	private:
		AExpression * condition;
		BodyStatement * body;
		BodyStatement * elseBody;
	public:
		IfStatement();
	        virtual ~IfStatement();

		void setCondition(AExpression * condition);
		void setBody(BodyStatement * body);
		void setElseBody(BodyStatement * elseBody);
		virtual bool executeStatement(InterpreterData interpreterData);
};
#endif
