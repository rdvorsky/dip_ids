/* Soubor: VariableTable.cpp
 * Autor: Pavel Svanda
 * Email: svandap@gmail.com
 * Datum vytvoreni: 17.3.2013
 * Popis: deklarace tridy VariableTable, ktera slouzi k ukladani promennych
 */

#include "VariableTable.h"

/* VariableTable::VariableTable
 * Popis: kostruktor
 * Parametry: -
 * Navratova hodnota: -
 */
VariableTable::VariableTable() {
}

/* VariableTable::~VariableTable
 * Popis: destruktor
 * Parametry: -
 * Navratova hodnota: -
 */
VariableTable::~VariableTable() {
	this -> variableList.erase(this -> variableList.begin(), this -> variableList.end());
}

/* VariableTable::setFrameVariable
 * Popis: funkce provede ulozeni promenne ramce
 * Parametry: char * name - jmeno ramce
 *            Frame frame - ukladany ramec
 * Navratova hodnota: -
 */
void VariableTable::setFrameVariable(char * name, Frame frame) {
	//zkontroluji, zdali tam promenna uz neni, pokud ano, tak ji nahradim
	string var(name);
	if(this -> variableList.find(var) == this -> variableList.end()) {
		//promenna zde jeste neni, tak ji vytvorim
		Variable newVariable;
		this -> variableList.insert ( pair<string,Variable>(var,newVariable) );
	}
	//promenna jiz existuje, tak ji mohu nastavit
	this -> variableListIt = this -> variableList.find(var);
	this -> variableListIt -> second.type = VT_FRAME;
	this -> variableListIt -> second.frame = frame;
}

/* VariableTable::setCaptureFrameVariable
 * Popis: funkce provede ulozeni promenne odchyceneho ramce
 * Parametry: char * name - jmeno ramce
 *            Frame frame - ukladany ramec
 * Navratova hodnota: -
 */
void VariableTable::setCaptureFrameVariable(char * name, char * str) {
	//zkontroluji, zdali tam promenna uz neni, pokud ano, tak ji nahradim
	string var(name);
	if(this -> variableList.find(var) == this -> variableList.end()) {
		//promenna zde jeste neni, tak ji vytvorim
		Variable newVariable;
		this -> variableList.insert ( pair<string,Variable>(var,newVariable) );
	}
	//promenna jiz existuje, tak ji mohu nastavit
	this -> variableListIt = this -> variableList.find(var);
	this -> variableListIt -> second.type = VT_CAPTURE_FRAME;
	string val(str);
	this -> variableListIt -> second.str = val;
}

/* VariableTable::setStringVariable
 * Popis: funkce provede ulozeni promenne textoveho retezce
 * Parametry: char * name - jmeno promenne
 *            Frame frame - ukladany retezec
 * Navratova hodnota: -
 */
void VariableTable::setStringVariable(char * name, char * str) {
	//zkontroluji, zdali tam promenna uz neni, pokud ano, tak ji nahradim
	string var(name);
	if(this -> variableList.find(var) == this -> variableList.end()) {
		//promenna zde jeste neni, tak ji vytvorim
		Variable newVariable;
		this -> variableList.insert ( pair<string,Variable>(var,newVariable) );
	}
	//promenna jiz existuje, tak ji mohu nastavit
	this -> variableListIt = this -> variableList.find(var);
	this -> variableListIt -> second.type = VT_STRING;
	string val(str);
	this -> variableListIt -> second.str = val;
}

/* VariableTable::setIntVariable
 * Popis: funkce provede ulozeni promenne ciselne hodnoty
 * Parametry: char * name - jmeno promenne
 *            Frame frame - ukladana hodnota
 * Navratova hodnota: -
 */
void VariableTable::setIntVariable(char * name, long long int value) {
	//zkontroluji, zdali tam promenna uz neni, pokud ano, tak ji nahradim
	string var(name);
	if(this -> variableList.find(var) == this -> variableList.end()) {
		//promenna zde jeste neni, tak ji vytvorim
		Variable newVariable;
		this -> variableList.insert ( pair<string,Variable>(var,newVariable) );
	}
	//promenna jiz existuje, tak ji mohu nastavit
	this -> variableListIt = this -> variableList.find(var);
	this -> variableListIt -> second.type = VT_INTEGER;
	this -> variableListIt -> second.value = value;
}

/* VariableTable::getVariable
 * Popis: funkce provede navraceni hodnoty promenne
 * Parametry: char * name - jmeno promenne
 * Navratova hodnota: hodnota promenne
 */
Variable VariableTable::getVariable(char * name) {
	string var(name);
	if(this -> variableList.find(var) == this -> variableList.end()) {
		//printf("%s \"%s\" %s\r\n", LANG_FRAME_VARIABLE, name, LANG_FRAME_UNDEFINE);
		Variable newVariable;
		newVariable.type = VT_UNKNOWN;
		return newVariable;
	}
	
	this -> variableListIt = this -> variableList.find(var);
	return this -> variableListIt -> second;
}

/* VariableTable::showAllVariable
 * Popis: funkce provede vypsani vsech promennych
 * Parametry: -
 * Navratova hodnota: -
 */
void VariableTable::showAllVariable() {
	bool isEmpty = true;
	for ( variableListIt = variableList.begin(); variableListIt != variableList.end(); variableListIt++ ) {
		if((*this -> variableListIt).second.type == VT_FRAME) {
			if(((*variableListIt).first.compare(CONTENT_RADIOTAP_HEADER) != 0 &&
		            (*variableListIt).first.compare(CONTENT_IEEE_HEADER) != 0 &&
			    (*variableListIt).first.compare(CONTENT_LLC_HEADER) != 0)) {
				isEmpty = false;
				printf("\t%10s", (*variableListIt).first.c_str());
				if((*variableListIt).second.frame.getIsSetRadioTapHeader()) {
					printf(" <%s", LANG_FRAME_RADIO_TAP);	
				}
				if((*variableListIt).second.frame.getIsSetIeeeHeader()) {
					char subType [200];
					(*variableListIt).second.frame.ieeeHeader.getSubTypeString(subType);
					printf(" <%s \"%s\">", LANG_FRAME_IEEE, subType);
				}
				if((*variableListIt).second.frame.getIsSetLLCHeader()) {
					char type [200];
					(*variableListIt).second.frame.llcHeader.getTypeString(type);
					printf(" <%s \"%s\">", LANG_FRAME_LLC, type);
				}
				if((*variableListIt).second.frame.getIsSetRadioTapHeader()) {
					printf(">");	
				}
				printf("\r\n");
			}
		} else if((*this -> variableListIt).second.type == VT_INTEGER){
			isEmpty = false;
			printf("\t%10s ", (*this -> variableListIt).first.c_str());			
			printf("<%s> (%lld)\r\n", LANG_FRAME_INTEGER, (*this -> variableListIt).second.value);
		} else if ((*this -> variableListIt).second.type == VT_STRING) {
			isEmpty = false;
			printf("\t%10s ", (*this -> variableListIt).first.c_str());			
			printf("<%s>  (%s)\r\n", LANG_FRAME_STRING, (*this -> variableListIt).second.str.c_str());
		} else if ((*this -> variableListIt).second.type == VT_CAPTURE_FRAME) {
			isEmpty = false;
			printf("\t%10s ", (*this -> variableListIt).first.c_str());			
			printf("<%s>  (%s)\r\n", LANG_FRAME_CAPTURE_FRAME, (*this -> variableListIt).second.str.c_str());
		}
	}

	if(isEmpty) {
		printf("\t\t%s\r\n", LANG_FRAME_EMPTY_LIST);
	}
}

/* VariableTable::showFrameVariable
 * Popis: funkce provede vypsani promennych typu ramec
 * Parametry: -
 * Navratova hodnota: -
 */
void VariableTable::showFrameVariable() {
	bool isEmpty = true;
	for ( variableListIt = variableList.begin(); variableListIt != variableList.end(); variableListIt++ ) {
		if((*this -> variableListIt).second.type == VT_FRAME) {
			if(((*variableListIt).first.compare(CONTENT_RADIOTAP_HEADER) != 0 &&
		            (*variableListIt).first.compare(CONTENT_IEEE_HEADER) != 0 &&
			    (*variableListIt).first.compare(CONTENT_LLC_HEADER) != 0)) {
				isEmpty = false;
				printf("\t%10s", (*variableListIt).first.c_str());
				if((*variableListIt).second.frame.getIsSetRadioTapHeader()) {
					printf(" <%s", LANG_FRAME_RADIO_TAP);	
				}
				if((*variableListIt).second.frame.getIsSetIeeeHeader()) {
					char subType [200];
					(*variableListIt).second.frame.ieeeHeader.getSubTypeString(subType);
					printf(" <%s \"%s\">", LANG_FRAME_IEEE, subType);
				}
				if((*variableListIt).second.frame.getIsSetLLCHeader()) {
					char type [200];
					(*variableListIt).second.frame.llcHeader.getTypeString(type);
					printf(" <%s \"%s\">", LANG_FRAME_LLC, type);
				}
				if((*variableListIt).second.frame.getIsSetRadioTapHeader()) {
					printf(">");	
				}
				printf("\r\n");
			}
		}
	}

	if(isEmpty) {
		printf("\t\t%s\r\n", LANG_FRAME_EMPTY_LIST);
	}
}

/* VariableTable::delVariable
 * Popis: funkce provede odstraneni promenne
 * Parametry: char * name - jmeno promenne
 * Navratova hodnota: informace, zdali se odstraneni podarilo nebo ne
 */
bool VariableTable::delVariable(char * name) {
	string varName(name);
	if(variableList.find(varName) != variableList.end()) {
		variableListIt=variableList.find(varName);
		variableList.erase(variableListIt); 
		return true;
	}
	return false;
}
