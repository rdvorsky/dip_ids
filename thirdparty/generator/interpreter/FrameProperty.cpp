/* Soubor: FrameProperty.cpp
 * Autor: Pavel Svanda
 * Email: svandap@gmail.com
 * Datum vytvoreni: 17.3.2013
 * Popis: definice tridy FrameProperty, ktera slouzi k ulozeni hodnoty ramce
 */
#include "FrameProperty.h"

/* FrameProperty::FrameProperty
 * Popis: konstruktor
 * Parametry: -
 * Navratova hodnota: -
 */
FrameProperty::FrameProperty() {
}

/* FrameProperty::FrameProperty
 * Popis: konstruktor
 * Parametry: int propertyType - typ vlastnosti
 * Navratova hodnota: -
 */
FrameProperty::FrameProperty(int propertyType) {
	this -> propertyType = propertyType;
}

/* FrameProperty::~FrameProperty
 * Popis: destruktor
 * Parametry: -
 * Navratova hodnota: -
 */
FrameProperty::~FrameProperty() {
	for(this -> valueListIt = this -> valueList.begin(); this -> valueListIt != this -> valueList.end(); this -> valueListIt++) {
		delete *this -> valueListIt;
	}
	this -> valueList.erase(this -> valueList.begin(), this -> valueList.end());
}

/* FrameProperty::getPropertyType
 * Popis: funce vrati typ vlastnosti
 * Parametry: -
 * Navratova hodnota: typ vlastnosti
 */
int FrameProperty::getPropertyType() {
	return this -> propertyType;
}

/* FrameProperty::setPropertyType
 * Popis: funkce nastavy typ vlastnosti
 * Parametry: -
 * Navratova hodnota: typ vlastnosti
 */
void FrameProperty::setPropertyType(int propertyType) {
	this -> propertyType = propertyType;
}

/* FrameProperty::addValue
 * Popis: funkce prida hodnotu vlasnosti
 * Parametry: AExpression * value - hodnota vlastnosti
 * Navratova hodnota: -
 */
void FrameProperty::addValue(AExpression * value) {
	this -> valueList.push_back(value);
}

/* FrameProperty::getValue
 * Popis: funkce vrati hodnotu na danem indexu
 * Parametry: int index                     - index ziskavane hodnoty
 *            VariableTable * variableTable - tabulka promennych
 * Navratova hodnota: hodnota vlastnosti na indexu 
 */
Variable FrameProperty::getValue(int index, VariableTable * variableTable) {
	int actIndex = 0;
	for(this -> valueListIt = this -> valueList.begin(); this -> valueListIt != this -> valueList.end(); this -> valueListIt++) {
		if(actIndex == index) {
			AExpression * value = *this -> valueListIt;
			return value -> evaluateExpression(variableTable);
		}
		actIndex++;
	}
	Variable var;
	var.type = VT_UNKNOWN;
	return var;
}
