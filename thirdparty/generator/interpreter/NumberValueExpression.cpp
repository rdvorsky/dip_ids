/* Soubor: NumberValueExpression.cpp
 * Autor: Pavel Svanda
 * Email: svandap@gmail.com
 * Datum vytvoreni: 17.3.2013
 * Popis: definice tridy NumberValueExpression, ktera slouzi k ulozeni ciselne hodnoty
 */
#include "NumberValueExpression.h"

/* NumberValueExpression::NumberValueExpression
 * Popis: konstruktor
 * Parametry: -
 * Navratova hodnota: -
 */
NumberValueExpression::NumberValueExpression() {
	this -> value = 0;
}

/* NumberValueExpression::NumberValueExpression
 * Popis: konstruktor
 * Parametry: int value - nastavovana hodnota
 * Navratova hodnota: -
 */
NumberValueExpression::NumberValueExpression(int value) {
	this -> value = value;
}

/* NumberValueExpression::~NumberValueExpression
 * Popis: destruktor
 * Parametry: -
 * Navratova hodnota: -
 */
NumberValueExpression::~NumberValueExpression() {
}

/* NumberValueExpression::setValue
 * Popis: funkce provede nastaveni ciselne hodnoty
 * Parametry: int value - nastavovana hodnota
 * Navratova hodnota: -
 */
void NumberValueExpression::setValue(int value) {
	this -> value = value;
}

/* NumberValueExpression::evaluateExpression
 * Popis: funkce provede vyhodnoceni vyrazu - navraceni hodnoty
 * Parametry: VariableTable * variableTable - tabulka promennych
 * Navratova hodnota: navracena hodnota
 */
Variable NumberValueExpression::evaluateExpression(VariableTable * variableTable) {
	Variable var;
	var.type = VT_INTEGER;
	var.value = this -> value;
	return var;
}
