/* Soubor: AssignStatement.h
 * Autor: Pavel Svanda
 * Email: svandap@gmail.com
 * Datum vytvoreni: 17.3.2013
 * Popis: deklarace tridy AssignStatement, ktera slouzi k ulozeni vysledku vyrazu
 */
#ifndef ASSIGN_STATEMENT_H
#define ASSIGN_STATEMENT_H

#include "Statement.h"

class AssignStatement : public Statement {
	private:
		char variableName [LENGTH_VARIABLE_NAME];
		AExpression * expression;

	public:
		AssignStatement();
		AssignStatement(char * variableName, AExpression * expression);
		AssignStatement(AExpression * variableName, AExpression * expression);
	        virtual ~AssignStatement();

		void setAssign(char *variableName, AExpression * expression);
		virtual bool executeStatement(InterpreterData interpreterData);
};
#endif
