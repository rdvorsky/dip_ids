/* Soubor: WifiInterface.cpp
 * Autor: Pavel Svanda
 * Email: svandap@gmail.com
 * Datum vytvoreni: 24.6.2012
 * Popis: definice tridy WiFiInterface reprezentujici fyzicke WiFi rozhrani, na ktere se zasilaji wifi pakety
 */
#include "WiFiInterface.h"

#define SNAPLEN 800
#define PROMISC 1
#define TO_MS   20

/* WiFiInterface::WiFiInterface
 * Popis: konstruktor
 * Parametry: -
 * Navratova hodnota: -
 */
WiFiInterface::WiFiInterface() {
	this -> interface = NULL;
	this -> errorBuffer[0] = '\0';
	this -> lengthCapturedFrame = 0;
	pthread_mutex_init(&this -> interfaceMutex, NULL);
	pthread_mutex_init(&this -> threadListMutex, NULL);
}

/* WiFiInterface::~WiFiInterface
 * Popis: destruktor
 * Parametry: -
 * Navratova hodnota: -
 */
WiFiInterface::~WiFiInterface() {
	Thread thread;
	pthread_mutex_lock(&this -> threadListMutex);
		while(!this -> threadList.empty()) {
			thread = this -> threadList.front();
			pthread_cancel(thread.thread);
			this -> threadList.pop_front();
		}
	pthread_mutex_unlock(&this -> threadListMutex);
	pthread_mutex_destroy(&this -> interfaceMutex);
	pthread_mutex_destroy(&this -> threadListMutex);

	pcap_close(this -> interface);
}

/* WiFiInterface::initWiFiInterface
 * Popis: funkce provede inicializaci rozhrani, na ktere se budou zasilat prislusne WiFi pakety
 * Parametry: char * jmenoRozhrani - jmeno fyzickeho rozhrani
 * Navratova hodnota: informuje o uspechu ci neuspechu nastaveni rozhrani
 */
bool WiFiInterface::initWiFiInterface(char * interface) {
	int encapsulation = 0;
	this -> interface = pcap_open_live(interface, SNAPLEN, PROMISC, TO_MS, this -> errorBuffer);
	if(this -> interface == NULL) {
		printf("%s %s  %s pcap: %s.\r\n", LANG_INTERFACE_NO_OPEN, interface, LANG_INTERFACE_NO_OPEN_IN, this -> errorBuffer);
		return false;
	}
	if(pcap_set_datalink (this -> interface, DLT_IEEE802_11_RADIO) == -1) {
		return false;
	}
	encapsulation = pcap_datalink(this -> interface);
	switch(encapsulation) {
		case DLT_IEEE802_11_RADIO: {
				//printf("DLT_IEEE802_11_RADIO Encap\r\n");
			}; break;
		default: {
				printf("%s %s !\r\n", LANG_INTERFACE_UNKNOWN_ENCAP, interface);
				return false;
			};
	}
	/*
	this -> interface = wi_open(interface);
        if (!this -> interface) {
        	return false;
	}*/
	return true;
}

/* sendFrameThread
 * Popis: funkce vlakna, ktere se postara o zaslani ramce
 * Parametry: void * data - ukazatel na strukturu, kde ma vlakno pozadovane informace
 * Navratova hodnota: -
 */
void * sendFrameThread(void * data) {
	Thread * threadData = (Thread *)data;
	int sendedData = 0;
	for(int i = 0; i < threadData -> repeat; i++) {
		pthread_mutex_lock(threadData -> interfaceMutex);
			sendedData = pcap_inject(threadData ->  interface, threadData -> frame, threadData -> sizeFrame);
		pthread_mutex_unlock(threadData -> interfaceMutex);
		pthread_mutex_lock(threadData -> threadListMutex);
			threadData -> sendedFrame++;
		pthread_mutex_unlock(threadData -> threadListMutex);

		if(threadData -> delay != 0) {
			usleep(threadData -> delay*1000);
		}
	}
	if (sendedData != threadData -> sizeFrame) {
		printf("%s\r\n", LANG_INTERFACE_SEND_ERROR);
	} else {
		//printf("%s\r\n", LANG_INTERFACE_SEND_OK);
	}
	//vlakno uz neni zive
	pthread_mutex_lock(threadData -> threadListMutex);
		threadData -> alive = false;
	pthread_mutex_unlock(threadData -> threadListMutex);
	pthread_exit(NULL);
}

/* WiFiInterface::sendFrame
 * Popis: funkce provede zaslani ramce na dane rozhrani
 * Parametry: char * variable - nazev zasilaneho ramce
 *            u8 * frame - ukazatel na pole, ve kterem je zasilany ramec ulozen. Toto pole se odesle na rozhrani.
 *            int sizeFrame - velikost ramce, ktery se bude odesilat
 *            int repeat - pocet opakovani zaslani
 *            int delay - zpozdeni mezi zaslanimi
 * Navratova hodnota: informace, zdali se zaslani zdarilo ci nikoliv
 */
bool WiFiInterface::sendFrame(char * variable, u8 * frame, int sizeFrame, int repeat, int delay) {
	string nameFrame(variable);

	Thread thread;
	thread.nameFrame       = nameFrame;
	thread.alive           = true;
	Utilities::memcpy8(thread.frame, frame, sizeFrame, LITTLE_ENDIAN_ORDER);
	thread.sizeFrame       = sizeFrame;
	thread.sendedFrame     = 0;
	thread.repeat          = repeat;
	thread.delay           = delay;
	thread.interface       =  this -> interface;
	thread.interfaceMutex  = &this -> interfaceMutex;
	thread.threadListMutex = &this -> threadListMutex;

	this -> threadList.push_back(thread);
	pthread_create(&(this->threadList.back()).thread, NULL, sendFrameThread, (void*)(&(this->threadList.back())));
	printf("\t%s\r\n", LANG_INTERFACE_INSERT_QUEUE );
	return true;	
}

/* WiFiInterface::sendFrameB
 * Popis: funkce provede zaslani ramce, blokuje program po dobu zasilani
 * Parametry: u8 * frame - data ramce, ktery se bude odesilat
 *            int sizeFrame - velikost ramce
 * Navratova hodnota: informace, zdali se odeslani podarilo
 */
bool WiFiInterface::sendFrameB(u8 * frame, int sizeFrame) {
	pthread_mutex_lock(&this -> interfaceMutex);
		int sendedData = pcap_inject(this ->  interface, frame, sizeFrame);
	pthread_mutex_unlock(&this -> interfaceMutex);

	if(sendedData != sizeFrame) return false;
	return true;
}

/* WiFiInterface::sendFrameSummary
 * Popis: funkce provede vypsani prehledu o zaslanych ramcich od posledniho zobrazeni
 * Parametry: -
 * Navratova hodnota: -
 */
void WiFiInterface::sendFrameSummary() {
	//projdu pole a vypisu jednotlive ramce, ty co byly uz zaslane, tak rovnou smazu
	printf("\t%10s - %8s/%6s (%s)\r\n", LANG_INTERFACE_FRAME_NAME, LANG_INTERFACE_SENDED, LANG_INTERFACE_TOTAL, LANG_INTERFACE_DELAY);
	bool isEmpty = true;
	for(this -> threadListIt = this -> threadList.begin(); this -> threadListIt != this -> threadList.end(); this -> threadListIt++) {
		isEmpty = false;
		pthread_mutex_lock(&this -> threadListMutex);
			printf("\t%10s - %8d/%-6d (%d)\r\n", this -> threadListIt -> nameFrame.c_str(),
                                                             this -> threadListIt -> sendedFrame, 
  							     this -> threadListIt -> repeat, 
 							     this -> threadListIt -> delay);
		pthread_mutex_unlock(&this -> threadListMutex);
	}
	if(isEmpty) {
		printf("\t\t%s\r\n", LANG_INTERFACE_EMPTY_QUEUE);
	}
	//pokud je uz zaslan, tak jej smazu
	this -> threadListIt = this -> threadList.begin();
	while(this -> threadListIt != this -> threadList.end()) {
		pthread_mutex_lock(&this -> threadListMutex);
		if(!this -> threadListIt -> alive) {
			pthread_cancel(this -> threadListIt -> thread);
			this -> threadList.erase(this -> threadListIt++);
		} else {
			++this -> threadListIt;
		}
		pthread_mutex_unlock(&this -> threadListMutex);
	}
}
/* saveFrame
 * Popis: funkce provede ulozeni odchyceneho ramce
 * Parametry: u_char * arg - parametr predany funkci z pcap_loop
 *            pcap_pkthdr * header - informace o ramci - predevsim jeho delce
 *            uchar * packet - data odchyceneho ramce
 * Navratova hodnota: -
 */
void saveFrame(u_char *arg, const struct pcap_pkthdr * header, const u_char * packet) {
	CaptureFrameInfo * captureFrameInfo = (CaptureFrameInfo *) arg;

	*(captureFrameInfo -> length) = header -> caplen;
	Utilities::memcpy8(captureFrameInfo -> frame, packet, header -> caplen, LITTLE_ENDIAN_ORDER);
}

/* WiFiInterface::captureFrame
 * Popis: funkce provede odchyceni ramce, ktery odpovida zadanemu filteru
 * Parametry: char * filter: filter specifikujici typ ramce
 * Navratova hodnota: informace, zdali se podarilo odchyceni nebo ne
 */
bool WiFiInterface::captureFrame(char * filter) {
	pthread_mutex_lock(&this -> interfaceMutex);
	struct bpf_program pgm;

	if(pcap_compile(this -> interface, &pgm, filter, 1, PCAP_NETMASK_UNKNOWN) < 0) {
		printf("error compile filter: %s\r\n", pcap_geterr(this -> interface));
		pthread_mutex_unlock(&this -> interfaceMutex);
		return false;
	}

	if(pcap_setfilter(this -> interface, &pgm) < 0) {
		printf("error set filter: %s\r\n", pcap_geterr(this -> interface));
		pthread_mutex_unlock(&this -> interfaceMutex);
		return false;
	}

	this -> captureFrameInfo.length = &this -> lengthCapturedFrame;
	this -> captureFrameInfo.frame  = this -> capturedFrame;
	if(pcap_loop(this -> interface, 1, &saveFrame, (u_char *) &this -> captureFrameInfo) < 0) {
		printf("error loop: %s\r\n", pcap_geterr(this -> interface));
		pthread_mutex_unlock(&this -> interfaceMutex);
		return false;
	}
	pthread_mutex_unlock(&this -> interfaceMutex);
	return true;
}

/* WiFiRozhrani::getCapturedFrame
 * Popis: funkce vrati odchyceny ramce
 * Parametry: - uint8_t * frame - misto, kam se ramec ulozi
 * Navratova hodnota: delka odchyceneho ramce
 */
int WiFiInterface::getCapturedFrame(uint8_t * frame) {
	Utilities::memcpy8(frame, this -> capturedFrame, this -> lengthCapturedFrame, LITTLE_ENDIAN_ORDER);
	return this -> lengthCapturedFrame;
}

/* WiFiRozhrani::getErrorBuffer
 * Popis: funkce frati ukazatel na chybovy buffer
 * Parametry: -
 * Navratova hodnota: ukazatel na chybovy buffer
 */
char * WiFiInterface::getErrorBuffer() {
	return this -> errorBuffer;
}
