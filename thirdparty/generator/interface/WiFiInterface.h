/* Soubor: WifiInterface.h
 * Autor: Pavel Svanda
 * Email: svandap@gmail.com
 * Datum vytvoreni: 24.6.2012
 * Popis: deklarace tridy WiFiRozhani reprezentujici fyzicke WiFi rozhrani, na ktere se zasilaji wifi pakety
 */
#ifndef WIFIINTERFACE_H
#define WIFIINTERFACE_H

#include <pcap.h>
#include <stdio.h>
#include <unistd.h>
#include <list>
#include <pthread.h>
#include <string>
#include "../frame/Frame.h"
#include "../lang/Lang.h"

#if !defined(PCAP_NETMASK_UNKNOWN)
	#define PCAP_NETMASK_UNKNOWN    0xffffffff
#endif

#define WI_LENGTH_CAPTURED_FRAME 	5000
#define WI_MAX_FILTER_LENGTH		5000
using namespace std;

typedef unsigned char u8;

typedef struct {
	pthread_t thread;			//vlakno, ktere provadi odesilani ramcu
	pcap_t * interface;			//rozhrani, na ktere se zasilaji ramce
	pthread_mutex_t * interfaceMutex;	//mutex na rozhrani	
	pthread_mutex_t * threadListMutex;	//mutex na seznam vlaken
	bool alive;				//informace, zdali se jeste provadi zasilani
	u8 frame [MAX_DATA_LENGTH];		//zasilany obsah
	int sizeFrame;				//velikost ramce
	string nameFrame;			//nazev ramce
	int sendedFrame;			//pocet jiz odeslanych ramcu
	int repeat;				//pocet opakovani zasilani ramcu
	int delay;				//prodleva mezi zasilanim
} Thread;

typedef struct {
	int * length;
	uint8_t * frame;
} CaptureFrameInfo;

typedef list<Thread> ThreadList;
typedef	list<Thread>::iterator ThreadListIt;

class WiFiInterface {
	private:
		ThreadList   threadList;
		ThreadListIt threadListIt;

		pthread_mutex_t interfaceMutex;
		pthread_mutex_t threadListMutex;
	
		pcap_t * interface;
		char errorBuffer [PCAP_ERRBUF_SIZE];

		CaptureFrameInfo captureFrameInfo;
		int lengthCapturedFrame;
		uint8_t capturedFrame [WI_LENGTH_CAPTURED_FRAME];

	public:
		WiFiInterface();
	       ~WiFiInterface();

		bool initWiFiInterface (char * interface);
		bool sendFrame( char * variable, u8 * frame, int sizeFrame, int repeat, int delay);
		bool sendFrameB(u8 * frame, int sizeFrame);
		void sendFrameSummary();
		char * getErrorBuffer();
		
		bool captureFrame(char * filter);
		int getCapturedFrame(uint8_t * frame);
};

#endif
