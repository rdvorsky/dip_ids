<?xml version="1.0" ?>
<frame>
    <radioTapHeader isPresent="1" generateCrcSum="1">
        <version value="0" />
        <pad value="0" />
        <len value="26" />
        <present value="18479" />
        <TSFTmactime>791232</TSFTmactime>
        <flags value="16" />
        <antenna value="1" />
        <antennaNoise value="0" />
        <antennaSignal value="-96" />
        <channelFrequency value="2472" />
        <channelFlags value="192" />
        <FHSShopSet value="0" />
        <FHSShopPattern value="0" />
        <lockQuality value="0" />
        <MCSknown value="0" />
        <MCSflags value="0" />
        <MCSmcs value="0" />
        <RXflags value="0" />
        <rate value="12" />
        <TXattenuation value="0" />
        <vendorNamespaceOUI></vendorNamespaceOUI>
        <vendorNamespaceSubNamespace value="0" />
        <vendorNamespaceSkipLength value="0" />
        <dbTXattenuation value="0" />
        <dbAntennaNoise value="0" />
        <dbAntennaSignal value="0" />
        <dbmTXpower value="0" />
        <RTSretries value="0" />
        <dataRetries value="0" />
    </radioTapHeader>
    <ieeeHeader isPresent="1">
        <taggedParametersSet>
            <taggedParameter0 value="0" />
            <taggedParameter1 value="1" />
            <taggedParameter2 value="0" />
            <taggedParameter3 value="0" />
            <taggedParameter4 value="1" />
            <taggedParameter5 value="0" />
            <taggedParameter6 value="0" />
            <taggedParameter7 value="0" />
            <taggedParameter8 value="0" />
            <taggedParameter9 value="0" />
            <taggedParameter10 value="1" />
            <taggedParameter11 value="1" />
            <taggedParameter12 value="0" />
            <taggedParameter13 value="0" />
            <taggedParameter14 value="0" />
        </taggedParametersSet>
        <frameControl value="128" />
        <duration value="0" />
        <addr1s>12:23:34:45:56:67</addr1s>
        <addr2s>ff:ff:ff:ff:ff:ff</addr2s>
        <addr3s>12:23:34:45:65:67</addr3s>
        <addr4s>00:00:00:00:00:00</addr4s>
        <aid value="0" />
        <seqCtl value="23792" />
        <timestamp>4660633675</timestamp>
        <beaconInterval value="100" />
        <capabilityInfo value="1041" />
        <ssid>wepBcn</ssid>
        <fhParameterSetLength value="0" />
        <fhParameterSetDwellTime value="0" />
        <fhParameterSetHopSet value="0" />
        <fhParameterSetHopPattern value="0" />
        <fhParameterSetHopIndex value="0" />
        <dsParameterSet value="13" />
        <cfParameterSetCount value="0" />
        <cfParameterSetPeriod value="0" />
        <cfParameterSetMaxDuration value="0" />
        <cfParameterSetRemaining value="0" />
        <ibssParameterSet value="0" />
        <timDtimCount value="0" />
        <timDtimPeriod value="1" />
        <timBitmapControl value="1" />
        <timPartialVirtualBitmap>0</timPartialVirtualBitmap>
        <countryString></countryString>
        <countryConstraintTripletSet />
        <localPowerConstraint value="0" />
        <channelSwitchMode value="0" />
        <newChannelNumber value="0" />
        <channelSwitchCount value="0" />
        <quietCount value="0" />
        <quietPeriod value="0" />
        <quietDuration value="0" />
        <quietOffset value="0" />
        <transmitPower value="0" />
        <linkMargin value="0" />
        <erp value="4" />
        <supportedRateSet>
            <supportedRate0 index="0" supportedRate="140" />
            <supportedRate1 index="1" supportedRate="146" />
            <supportedRate2 index="2" supportedRate="152" />
            <supportedRate3 index="3" supportedRate="164" />
            <supportedRate4 index="4" supportedRate="176" />
            <supportedRate5 index="5" supportedRate="72" />
            <supportedRate6 index="6" supportedRate="96" />
            <supportedRate7 index="7" supportedRate="108" />
        </supportedRateSet>
        <extendedSupportedRateSet />
        <reasonCode value="0" />
        <listenInterval value="0" />
        <rsnVersion value="1" />
        <groupCipherSuite>
            <oui>00-0f-ac</oui>
            <type value="0" />
        </groupCipherSuite>
        <pairwiseCipherSuite />
        <authenticationSuite />
        <rsnCapabilities value="0" />
        <statusCode value="0" />
        <associationId value="0" />
        <authenticationAlgorithmNumber value="0" />
        <authenticationSequenceNumber value="0" />
        <challengeText>0</challengeText>
        <qos value="0" />
    </ieeeHeader>
    <payload isPresent="0">
        <cipher value="1" />
        <data></data>
        <vector></vector>
        <evector></evector>
        <nonce></nonce>
        <key></key>
        <keyIndex value="1" />
    </payload>
</frame>
