<?xml version="1.0" ?>
<frame>
    <radioTapHeader isPresent="1" generateCrcSum="1">
        <version value="0" />
        <pad value="0" />
        <len value="26" />
        <present value="18479" />
        <TSFTmactime>385180983</TSFTmactime>
        <flags value="16" />
        <antenna value="1" />
        <antennaNoise value="0" />
        <antennaSignal value="-53" />
        <channelFrequency value="2472" />
        <channelFlags value="12" />
        <FHSShopSet value="0" />
        <FHSShopPattern value="0" />
        <lockQuality value="0" />
        <MCSknown value="0" />
        <MCSflags value="0" />
        <MCSmcs value="0" />
        <RXflags value="0" />
        <rate value="108" />
        <TXattenuation value="0" />
        <vendorNamespaceOUI></vendorNamespaceOUI>
        <vendorNamespaceSubNamespace value="0" />
        <vendorNamespaceSkipLength value="0" />
        <dbTXattenuation value="0" />
        <dbAntennaNoise value="0" />
        <dbAntennaSignal value="0" />
        <dbmTXpower value="0" />
        <RTSretries value="0" />
        <dataRetries value="0" />
    </radioTapHeader>
    <ieeeHeader isPresent="1">
        <taggedParametersSet>
            <taggedParameter0 value="0" />
            <taggedParameter1 value="0" />
            <taggedParameter2 value="0" />
            <taggedParameter3 value="0" />
            <taggedParameter4 value="0" />
            <taggedParameter5 value="0" />
            <taggedParameter6 value="0" />
            <taggedParameter7 value="0" />
            <taggedParameter8 value="0" />
            <taggedParameter9 value="0" />
            <taggedParameter10 value="0" />
            <taggedParameter11 value="0" />
            <taggedParameter12 value="0" />
            <taggedParameter13 value="0" />
            <taggedParameter14 value="0" />
        </taggedParametersSet>
        <frameControl value="16648" />
        <duration value="44" />
        <addr1s>00:4f:62:08:05:69</addr1s>
        <addr2s>70:f1:a1:59:19:0e</addr2s>
        <addr3s>00:08:a1:34:6e:12</addr3s>
        <addr4s>00:00:00:00:00:00</addr4s>
        <aid value="0" />
        <seqCtl value="9664" />
        <timestamp>0</timestamp>
        <beaconInterval value="0" />
        <capabilityInfo value="0" />
        <ssid></ssid>
        <fhParameterSetLength value="0" />
        <fhParameterSetDwellTime value="0" />
        <fhParameterSetHopSet value="0" />
        <fhParameterSetHopPattern value="0" />
        <fhParameterSetHopIndex value="0" />
        <dsParameterSet value="0" />
        <cfParameterSetCount value="0" />
        <cfParameterSetPeriod value="0" />
        <cfParameterSetMaxDuration value="0" />
        <cfParameterSetRemaining value="0" />
        <ibssParameterSet value="0" />
        <timDtimCount value="0" />
        <timDtimPeriod value="0" />
        <timBitmapControl value="0" />
        <timPartialVirtualBitmap>0</timPartialVirtualBitmap>
        <countryString></countryString>
        <countryConstraintTripletSet />
        <localPowerConstraint value="0" />
        <channelSwitchMode value="0" />
        <newChannelNumber value="0" />
        <channelSwitchCount value="0" />
        <quietCount value="0" />
        <quietPeriod value="0" />
        <quietDuration value="0" />
        <quietOffset value="0" />
        <transmitPower value="0" />
        <linkMargin value="0" />
        <erp value="0" />
        <supportedRateSet />
        <extendedSupportedRateSet />
        <reasonCode value="0" />
        <listenInterval value="0" />
        <rsnVersion value="1" />
        <groupCipherSuite>
            <oui>00-0f-ac</oui>
            <type value="0" />
        </groupCipherSuite>
        <pairwiseCipherSuite />
        <authenticationSuite />
        <rsnCapabilities value="0" />
        <statusCode value="0" />
        <associationId value="0" />
        <authenticationAlgorithmNumber value="0" />
        <authenticationSequenceNumber value="0" />
        <challengeText>0</challengeText>
        <qos value="0" />
    </ieeeHeader>
    <llcHeader isPresent="0">
        <dnap value="170" />
        <snap value="170" />
        <controlField value="3" />
        <organizationCode value="0" />
        <type value="0" />
    </llcHeader>
    <payload isPresent="1">
        <cipher value="3" />
        <data>aaaa03000000080045000054000040004001f08ac0a86466c0a86467080052bef80700012f283b5149bc0e0008090a0b0c0d0e0f101112131415161718191a1b1c1d1e1f202122232425262728292a2b2c2d2e2f3031323334353637</data>
        <vector>000000000204</vector>
        <evector>00000000</evector>
        <nonce>&#x10;</nonce>
        <key>5252c588d89e93f36dac7fc7094a271d8a4823c1b0867e361bf532839b9e6829d4e87369a1033f0505638bf40adc3f2a5e8c35485aa2c12d2deed3ba3cc373e2</key>
        <keyIndex value="0" />
    </payload>
</frame>
