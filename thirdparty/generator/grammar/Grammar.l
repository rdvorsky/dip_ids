/* Soubor: Grammar.l
 * Autor: Pavel Svanda
 * Email: svandap@gmail.com
 * Datum vytvoreni: 30.9.2012
 * Popis: definice tokenu gramatiky
 */

%{
extern "C"
 {
   int yylex(void);
   void readFromBuffer(char *, int);
   void deleteBuffer(void);
 }

#include "grammar.tab.h"
int c;
char *p;
YY_BUFFER_STATE bs;
extern int at_end;

#define MAX_INCLUDE_DEPTH 10
YY_BUFFER_STATE include_stack[MAX_INCLUDE_DEPTH];
int include_stack_ptr = 0;
%}

%%

<<EOF>>				{  
				  return END;
				}

"\n"				{ return ENTER;}
"\t"				|
" "				|
"//"(.)*				;

"("				return LPARENTHESIS;
")"				return RPARENTHESIS;
"{"				return LCPARENTHESIS;
"}"				return RCPARENTHESIS;
"/"				return SLASH;
"\""				return QUOAT;	
"+"				return PLUS;
";"				return SEMICOLONS;
"."				return DOT;
":"				return DD;
","				return COMMA;
"~"				return NOT;
"="				return ASSIGN;	
"-"				return MINUS;
"*"				return MULT;
"%"				return PERC;
"<"				return LT;
"<="				return LE;
">"				return GT;
">="				return GE;
"|"				return OR;
"||"				return ORL;
"&"				return AND;
"&&"				return AND;
"=="				return EQ;
"!="				return NE;
"!"				return NEG;

while				return WHILE;
for				return FOR;
if				return IF;
else				return ELSE;
int				return INT;

exit				{
				 at_end = 1;	
				 return END;
				}
send				return SEND;
isend				return ISEND;
sleep				return SLEEP;
msleep				return MSLEEP;
usleep				return USLEEP;
del				return DEL;
print				return PRINT;
dump				return DUMP;
str				return STR;
load				return LOAD;
save				return SAVE;
ls				|
list				return LIST;
all				return ALL;
key				return KEY;
iw				|
iwlist				return IW_LIST;
capture				return CAPTURE;
getIv				return PARSE_IV;
scapy				return SCAPY;
help				return HELP;
getgtk				return GET_GTK_KEY;
getptk				return GET_PTK_KEY;
break				return BREAK;
alias				return ALIAS;
time				return TIME;
start				return START;
stop				return STOP;

ieee				return H_IEEE;
radiotap			return H_RADIOTAP;
llc				return H_LLC;
value				return H_VALUE;

payload				return P_PAYLOAD;
payloadData			return P_PAYLOAD_DATA;
payloadCipher			return P_PAYLOAD_CIPHER;
payloadVector			|
payloadNonce			return P_PAYLOAD_VECTOR;
payloadKey			return P_PAYLOAD_KEY;
payloadKeyIndex			return P_PAYLOAD_KEY_INDEX;


version				return P_VERSION;
pad				return P_PAD;
len				return P_LEN;
antenna				return P_ANTENNA;
antennaNoise			return P_ANTENNA_NOISE;
ssiSignal			|
antennaSignal			return P_ANTENNA_SIGNAL;
channelFrequency 		return P_CHANNEL_FREQUENCY;
channelFlags			return P_CHANNEL_FLAGS;			
channel				return P_CHANNEL;
fhssHopSet			return P_FHSS_HOP_SET;
fhssHopPattern			return P_FHSS_HOP_PATTERN;
fhssHop				return P_FHSS_HOP;
flags				return P_FLAGS;
lockQuantity			return P_LOCK_QUALITY;
mcsKnown			return P_MCS_KNOWN;
mcsFlags			return P_MCS_FLAGS;
mcsMCS				return P_MCS_MCS;
mcs				return P_MCS;
rxFlags				return P_RX_FLAGS;
rate				return P_RATE;
tsft				return P_TSFT;
txAttenuation			return P_TX_ATTENUATION;
vendorNamespaceOUI 		return P_VENDOR_NAMESPACE_OUI;
vendorNamespaceSubNamespace 	return P_VENDOR_NAMESPACE_SUB_NAMESPACE;
vendorNamespaceSkipLength 	return P_VENDOR_NAMESPACE_SKIP_LENGTH;
vendorNamespace		 	return P_VENDOR_NAMESPACE;
dbTXattenuation 		return P_DB_TX_ATTENUATION;
dbAntennaNoise  		return P_DB_ANTENNA_NOISE;
dbAntennaSignal 		return P_DB_ANTENNA_SIGNAL;
dbmTXpower			return P_DBM_TX_POWER;
RTSretries			return P_RTS_RETRIES;
dataRetries			return P_DATA_RETRIES;

frameControl			return P_FRAME_CONTROL;
protocol			return P_PROTOCOL;
type				return P_TYPE;
subType				return P_SUB_TYPE;
toDS				return P_TO_DS;
fromDS				return P_FROM_DS;
moreFrag			return P_MORE_FRAG;
retry				return P_RETRY;
pwrMgmt				return P_PWR_MGMT;
moreData			return P_MORE_DATA;
protectedFrame			return P_PROTECTED_FRAME;
order				return P_ORDER;
duration			return P_DURATION;
aid				return P_AID;
bssid				return P_BSSID;
ra				|
receiverAddress			return P_RECEIVER_ADDRESS;
ta				|
transmitterAddress 		return P_TRANSMITTER_ADDRESS;
da				return P_DA;
sa				return P_SA;
seqCtl				return P_SEQ_CTL;
frag				return P_FRAGMENT;
seq				return P_SEQUENCE;
timestamp			return P_TIMESTAMP;
beaconInterval			return P_BEACON_INTERVAL;
capabilityInfo			return P_CAPABILITY_INFO;
ssid				return P_SSID;
fhParameterSet			return P_FH_PARAMETER_SET;
fhParameterSetDwellTime		return P_FH_PARAMETER_SET_DWELL_TIME;
fhParameterSetHopSet		return P_FH_PARAMETER_SET_HOP_SET;
fhParameterSetHopPattern	return P_FH_PARAMETER_SET_HOP_PATTERN;
fhParameterSetHopIndex		return P_FH_PARAMETER_SET_HOP_INDEX;
dsParameterSet			return P_DS_PARAMETER_SET;
cfParameterSet			return P_CF_PARAMETER_SET;
cfParameterSetCount		return P_CF_PARAMETER_SET_COUNT;
cfParameterSetPeriod		return P_CF_PARAMETER_SET_PERIOD;
cfParameterSetMaxDuration	return P_CF_PARAMETER_SET_MAX_DURATION;
cfParameterSetRemaining		return P_CF_PARAMETER_SET_REMAINING;
ibssParameterSet		return P_IBSS_PARAMETER_SET;
timDtimCount			return P_TIM_DTIM_COUNT;
timDtimPeriod			return P_TIM_DTIM_PERIOD;
timBitmapControl		return P_TIM_BITMAP_CONTROL;
timPartialVirtualBitmap		return P_TIM_PARTIAL_VIRTUAL_BITMAP;
tim				return P_TIM;
countryString			return P_COUNTRY_STRING;
countryConstraint		return P_COUNTRY_CONSTRAINT;
country				return P_COUNTRY;
localPowerConstraint		return P_LOCAL_POWER_CONSTRAINT;
channelSwitchMode		return P_CHANNEL_SWITCH_MODE;
newChannelNumber		return P_NEW_CHANNEL_NUMBER;
channelSwitchCount		return P_CHANNEL_SWITCH_COUNT;
channelSwitch			return P_CHANNEL_SWITCH;
quietCount			return P_QUIET_COUNT;
quietPeriod			return P_QUIET_PERIOD;
quietDuration			return P_QUIET_DURATION;
quietOffset			return P_QUIET_OFFSET;
quiet				return P_QUIET;
transmitPower			return P_TRANSMIT_POWER;
linkMargin			return P_LINK_MARGIN;
erp				return P_ERP;
supportedRate			return P_SUPPORTED_RATE;
extendedSupportedRate		return P_EXTENDED_SUPPORTED_RATE;
reasonCode			return P_REASON_CODE;
listenInterval			return P_LISTEN_INTERVAL;
currentAPaddress		return P_CURRENT_AP_ADDRESS;
rsnVersion			return P_RSN_VERSION;
groupCipherSuite		return P_GROUP_CIPHER_SUITE;
pairwiseCipherSuite		return P_PAIRWISE_CIPHER_SUITE;
authenticationSuite		return P_AUTHENTICATION_SUITE;
rsnCapabilities			return P_RSN_CAPABILITIES;
rsn				return P_RSN;
statusCode			return P_STATUS_CODE;
associationId			return P_ASSOCIATION_ID;
authenticationAlgorithmNumber   return P_AUTHENTICATION_ALGORITHM_NUMBER;
authenticationSequenceNumber    return P_AUTHENTICATION_SEQUENCE_NUMBER;
challengeText			return P_CHALLENGE_TEXT;
qos				return P_QOS;
address1			|
addr1				return P_ADDRESS1;
address2			|
addr2				return P_ADDRESS2;
address3			|
addr3				return P_ADDRESS3;
address4			|
addr4				return P_ADDRESS4;

dnap				return P_DNAP;
snap				return P_SNAP;
controlField			return P_CONTROL_FIELD;
organizationCode		return P_ORGANIZATION_CODE;
llcType				return P_LLC_TYPE;

TurboChannel	 		return K_TURBOCHANNEL;
CCKChannel	 		return K_CCKCHANNEL;
OFDMChannel	 		return K_OFDMCHANNEL;
2GHzSpectChannel 		return K_GHZ2SPECTCHANNEL;
5GHzSpectChannel		return K_GHZ5SPECTCHANNEL;
OnlyPassiveScan  		return K_ONLYPASSIVESCAN;
DynamicCCK-OFDM  		return K_DYNAMICCCKOFDM;
GFSKChannel	 		return K_GFSKCHANNEL;
sentReceiveDuringCFP 		return K_SENTRECEIVEDURINGCFP;
sentReceiveWithShortPreamble 	return K_SENTRECEIVEWITHSHORTPREAMBLE;
sentReceiveWithWEPencryption	return K_SENTRECEIVEWITHWEPENCRYPRION;
sentReceiveWithFragmentation	return K_SENTRECEIVEWITHFRAGMENTATION;
frameIncludesFCS		return K_FRAMEINSLUDESFCS;
frameHasPadding80211		return K_FRAMEHASPADDING80211;
frameFailedFCScheck		return K_FRAMEFAILEDFCSCHECK;
frameUSEDShortGuardInterval	return K_FRAMEUSEDSHORTGUARDINTERVAL;
rxReserved			return K_RXRESERVED;
plcpCRCcheckFailed		return K_PLCPCRCCHECKFAILED;
bandWidth			return K_BANDWIDTH;
bandWidthMCSflags		return K_BANDWIDTHMACSFLAGS;
mcsIndexKnown			return K_MCSINDEXKNOWN;
guardInterval			return K_GUARDINTERVAL;
htFormat			return K_HTFORMAT;
fecType				return K_FECTYPE;
mcsReserved			return K_MCSRESERVED;
crc				return K_FRAMEINSLUDESFCS;

ess				return K_ESS;
ibss				return K_IBSS;
cfPollable			return K_CF_POLLABLE;
cfPollRequest			return K_POLL_REQUEST;
privacy				return K_PRIVACY;
shortPreambule		 	return K_SHORT_PREAMBULE;
pbcc				return K_PBCC;
channelAgality			return K_CHANNEL_AGALITY;
shortSlotTime			return K_SHORT_SLOT_TIME;
dsssOfdm			return K_DSSS_OFDM;
nonErpPresen			return K_NON_ERP_PRESENT;
useProtection			return K_USE_PROTECTION;
barkerPreamble			return K_BARKER_PREAMBLE;

gcs				return K_GROUP_CIPHER_SUITE;
wep40				return K_WEP_40;
wep104				return K_WEP_104;
tkip				return K_TKIP;
ccmp				return K_CCMP;
plain				return K_PLAIN_TEXT;

pmk				return K_PRESHARED_MASTER_KEY;
psk				return K_PRE_SHARED_KEY;

data				return K_DATA;
qosData				return K_QOS_DATA;
rts				return K_RTS;
cts				return K_CTS;
ack				return K_ACK;
psPoll				return K_PS_POLL;
beacon				return K_BEACON;
probeRequest			return K_PROBE_REQUEST;
probeResponse			return K_PROBE_RESPONSE;
atim				return K_ATIM;
deauth				return K_DEAUTHETICATION;
disassociation			return K_DISASSOCIATION;
assocRequest			return K_ASSOCIATION_REQUEST;
reassocRequest			return K_REASSOCIATION_REQUEST;
assocResponse			return K_ASSOCIATION_RESPONSE;
authentication			return K_AUTHENTICATION;

ip4				|
ipv4				return K_IPv4;
ip6				|
ipv6				return K_IPv6;
arp				return K_ARP;
rarp				return K_RARP;
mplsu				return K_MPLSU;
mplsm				return K_MPLSM;
jumboframe			return K_JUMBO_FRAME;
eapol				return K_EAPOL;
lldp				return K_LLDP;
aarp				return K_AARP;


[a-zA-Z][a-zA-Z0-9]* 		{
		  	         yylval.string=strdup(yytext);
				 return STRING;
				}

[0-9a-fA-F][0-9a-fA-F](-)[0-9a-fA-F][0-9a-fA-F](-)[0-9a-fA-F][0-9a-fA-F] {
		  	         yylval.string=strdup(yytext);
				 return OUI;
				}

[0-9]+ 				{ 
				 yylval.number=strtoll(yytext, NULL, 10);
				 return NUMBER;
				}
(0x)[0-9a-fA-F]+		{
				 //yylval.number=strtoll(yytext, NULL, 16);
				 yylval.string=strdup(yytext);
				 return HEX_NUMBER;
				}
[0-9a-fA-F][0-9a-fA-F](:)[0-9a-fA-F][0-9a-fA-F](:)[0-9a-fA-F][0-9a-fA-F](:)[0-9a-fA-F][0-9a-fA-F](:)[0-9a-fA-F][0-9a-fA-F](:)[0-9a-fA-F][0-9a-fA-F] {
				 yylval.string=strdup(yytext);
				 return MAC_ADDRESS;
				}

[']([^'])*[']			{
				 yytext = &yytext[1];
			         yytext[strlen(yytext)-1] = 0;
		  	         yylval.string=strdup(yytext);
				 return TEXT;
				}

["](0x)[0-9a-fA-F]+["]		{
		  	         yylval.string=strdup(yytext);
				 return DATA;
				}

((\.|\.\.|[-_a-zA-Z0-9]*)\/)*([-_a-zA-Z0-9])+(\.frm)	|
((\.|\.\.|[-_a-zA-Z0-9]*)\/)*([-_a-zA-Z0-9])+(\.conf)	|
((\.|\.\.|[-_a-zA-Z0-9]*)\/)*([-_a-zA-Z0-9])+(\.txt)	{
				 yylval.string=strdup(yytext);
				 return FILEPATH;
				}
%%
/* readFromBuffer
 * Popis: funkce provede presmerovani vstupu lexikalniho analyzatoru do zadaneho bufferu
 * Parametry: char * buf - buffer, na ktery bude presmerovan vstup
 *            int lenBuf - delka zadaneho bufferu
 * Navratova hodnota: -
 */
void readFromBuffer(char * buf, int lenBuf) {
        include_stack[include_stack_ptr++] =  YY_CURRENT_BUFFER;
	yy_switch_to_buffer( yy_scan_buffer(buf, lenBuf) );
        BEGIN(INITIAL);
}

/* deleteBuffer
 * Popis: funkce provede presmerovani vstupu lexikalniho analyzatoru zpet na standardni vstup
 * Parametry: -
 * Navratova hodnota: -
 */
void deleteBuffer() {
        --include_stack_ptr;
  	yy_delete_buffer( YY_CURRENT_BUFFER );
	yy_switch_to_buffer( include_stack[include_stack_ptr] );
}
