/* Soubor: Grammar.y
 * Autor: Pavel Svanda
 * Email: svandap@gmail.com
 * Datum vytvoreni: 30.9.2012
 * Popis: definice pravidel gramatiky
 */
%{

	#include <stdio.h>
	#include <signal.h>
	#include <sys/ioctl.h>
	#include "../interpreter/Interpreter.h"
	#include "../other/CommandHistory.h"
	#include "../other/Utilities.h"
	#include "../help/Help.h"


	//stav aplikace, ktery se navraci pri ukonceni aplikace
	#define STATE_OK   0
	#define STATE_KO   1

	//inicializacni velikost bufferu
	#define	INPUT_BUFFER_SIZE 20000

	//slouzi k zadavani klicovych slov
	Keyword keywordInfo;

	//interpreter
	Interpreter interpreter;

	//napoveda
	Help help;

	/*//historie prikazu
	CommandHistory commandHistory;*/

	//jmeno procesu
	char procName[KL_LENGTH_PROC_NAME];

	void yyerror(const char *msg) {
		//fprintf(stderr, "error: %s\r\n", msg);
	}

	extern "C" {
		int yyparse(void);
		int yylex(void); 
		void readFromBuffer(char *, int);
		void deleteBuffer(void);
		int yywrap()
		{
		        return 1;
		}
	}

	int at_end = 0;
	extern int yychar;
	extern FILE * yyin;
%}

//prikazy
%token SEND ISEND DEL PRINT DUMP SLEEP MSLEEP USLEEP LOAD SAVE LIST KEY CAPTURE PARSE_IV HELP ALL GET_PTK_KEY GET_GTK_KEY BREAK TIME START STOP IW_LIST STR SCAPY

//konstrukce jazyka
%token WHILE FOR IF ELSE

//alias
%token ALIAS

//pomocne tokeny
%token END DOT DD COMMA SEMICOLONS ASSIGN LPARENTHESIS RPARENTHESIS LCPARENTHESIS RCPARENTHESIS SLASH QUOAT PLUS NOT ENTER H_RADIOTAP H_IEEE H_LLC H_VALUE
%token MINUS MULT LT GT LE GE OR AND NEG EQ NE ANDL ORL INT PERC

//vlastnosti ramcu
//payload
%token P_PAYLOAD P_PAYLOAD_DATA P_PAYLOAD_CIPHER P_PAYLOAD_VECTOR P_PAYLOAD_KEY P_PAYLOAD_KEY_INDEX
//RadioTapHeader
%token P_VERSION P_PAD P_LEN P_PRESENT P_ANTENNA P_ANTENNA_NOISE P_ANTENNA_SIGNAL P_CHANNEL_FREQUENCY P_CHANNEL_FLAGS P_FHSS_HOP_SET P_FHSS_HOP_PATTERN P_FLAGS
%token P_LOCK_QUALITY P_MCS_KNOWN P_MCS_FLAGS P_MCS_MCS P_RX_FLAGS P_RATE P_TSFT P_TX_ATTENUATION P_VENDOR_NAMESPACE_OUI P_CHANNEL P_FHSS_HOP P_MCS
%token P_VENDOR_NAMESPACE_SUB_NAMESPACE P_VENDOR_NAMESPACE_SKIP_LENGTH P_DB_TX_ATTENUATION P_DB_ANTENNA_NOISE P_DB_ANTENNA_SIGNAL P_DBM_TX_POWER P_RTS_RETRIES 
%token P_DATA_RETRIES P_VENDOR_NAMESPACE
//IeeeHeader
%token P_FRAME_CONTROL P_DURATION P_PROTOCOL P_TYPE P_SUB_TYPE P_TO_DS P_FROM_DS P_MORE_FRAG P_RETRY P_PWR_MGMT P_MORE_DATA P_PROTECTED_FRAME P_ORDER P_BSSID
%token P_RECEIVER_ADDRESS P_TRANSMITTER_ADDRESS P_DA P_SA P_SEQ_CTL P_TIMESTAMP P_BEACON_INTERVAL P_CAPABILITY_INFO P_SSID P_FH_PARAMETER_SET 
%token P_FH_PARAMETER_SET_DWELL_TIME P_FH_PARAMETER_SET_HOP_SET P_FH_PARAMETER_SET_HOP_PATTERN P_FH_PARAMETER_SET_HOP_INDEX P_DS_PARAMETER_SET P_IBSS_PARAMETER_SET
%token P_TIM_DTIM_COUNT P_TIM_DTIM_PERIOD P_TIM_BITMAP_CONTROL P_TIM_PARTIAL_VIRTUAL_BITMAP P_TIM P_COUNTRY_STRING P_COUNTRY_CONSTRAINT P_COUNTRY 
%token P_LOCAL_POWER_CONSTRAINT P_CHANNEL_SWITCH_MODE P_NEW_CHANNEL_NUMBER P_CHANNEL_SWITCH_COUNT P_CHANNEL_SWITCH P_QUIET_COUNT P_QUIET_PERIOD P_QUIET_DURATION 
%token P_QUIET_OFFSET P_QUIET P_TRANSMIT_POWER P_LINK_MARGIN P_ERP P_AID P_SUPPORTED_RATE P_EXTENDED_SUPPORTED_RATE P_REASON_CODE P_LISTEN_INTERVAL
%token P_CURRENT_AP_ADDRESS P_CF_PARAMETER_SET P_CF_PARAMETER_SET_COUNT P_CF_PARAMETER_SET_PERIOD P_CF_PARAMETER_SET_MAX_DURATION P_CF_PARAMETER_SET_REMAINING
%token P_RSN_VERSION P_GROUP_CIPHER_SUITE P_PAIRWISE_CIPHER_SUITE P_AUTHENTICATION_SUITE P_RSN_CAPABILITIES P_RSN P_STATUS_CODE P_ASSOCIATION_ID
%token P_AUTHENTICATION_ALGORITHM_NUMBER P_AUTHENTICATION_SEQUENCE_NUMBER P_CHALLENGE_TEXT P_QOS P_ADDRESS1 P_ADDRESS2 P_ADDRESS3 P_ADDRESS4
%token P_FRAGMENT P_SEQUENCE
//LLCheader
%token P_DNAP P_SNAP P_CONTROL_FIELD P_ORGANIZATION_CODE P_LLC_TYPE

//klicova slova
//RadioTapHeader
%token K_TURBOCHANNEL K_CCKCHANNEL K_OFDMCHANNEL K_GHZ2SPECTCHANNEL K_GHZ5SPECTCHANNEL K_ONLYPASSIVESCAN K_DYNAMICCCKOFDM K_GFSKCHANNEL K_SENTRECEIVEDURINGCFP 
%token K_SENTRECEIVEWITHSHORTPREAMBLE K_SENTRECEIVEWITHWEPENCRYPRION K_SENTRECEIVEWITHFRAGMENTATION K_FRAMEINSLUDESFCS K_FRAMEHASPADDING80211 K_FRAMEFAILEDFCSCHECK 
%token K_FRAMEUSEDSHORTGUARDINTERVAL K_RXRESERVED K_PLCPCRCCHECKFAILED K_BANDWIDTH K_BANDWIDTHMACSFLAGS K_MCSINDEXKNOWN K_GUARDINTERVAL K_HTFORMAT K_FECTYPE 
%token K_MCSRESERVED 
//IeeeHeader
%token K_RTS K_CTS K_ACK K_PS_POLL K_BEACON K_ESS K_IBSS K_CF_POLLABLE K_POLL_REQUEST K_PRIVACY K_SHORT_PREAMBULE K_PBCC K_CHANNEL_AGALITY K_SHORT_SLOT_TIME
%token K_DSSS_OFDM K_NON_ERP_PRESENT K_USE_PROTECTION K_BARKER_PREAMBLE K_PROBE_REQUEST K_ATIM K_DEAUTHETICATION K_ASSOCIATION_REQUEST K_REASSOCIATION_REQUEST 
%token K_ASSOCIATION_RESPONSE K_AUTHENTICATION K_PROBE_RESPONSE K_GROUP_CIPHER_SUITE K_WEP_40 K_WEP_104 K_TKIP K_CCMP K_PRESHARED_MASTER_KEY K_PRE_SHARED_KEY
%token K_DISASSOCIATION K_DATA K_QOS_DATA K_PLAIN_TEXT
//LLC
%token K_IPv4 K_IPv6 K_ARP K_RARP K_MPLSU K_MPLSM K_JUMBO_FRAME K_EAPOL K_LLDP K_AARP

%code requires{ class Frame; class Statement; class BodyStatement; class AExpression; }

%union {
        long long int number;
        char * string;
	Frame * frame;
	Statement * statement;
	BodyStatement * bodystatement;
	AExpression * expression;
}

%token <number> NUMBER
%token <string> HEX_NUMBER
%token <string> MAC_ADDRESS
%token <string> STRING
%token <string> TEXT
%token <string> KEYWORD
%token <string> PROPERTY
%token <string> FILEPATH
%token <string> OUI
%token <string> DATA

%type <statement>   command statement interpreter headerbuild
%type <bodystatement> bodystatement body else
%type <expression>  forexp1 forexp2 exp exp2 exp3 text number identifier radiotapheadercontent ieeeheadercontent llcheadercontent header framebuild path radiotapheader ieeeheader llcheader getframeproperty keyword numberkeyword
%type <expression>  repeat delay cipher keywordCipher
%type <number> subhelp keywordList

%right ASSIGN
%left  ANDL ORL
%left  AND
%left  OR
%left  EQ NE
%left  LT GT LE GE 
%left  PLUS MINUS
%left  MULT SLASH PERC
%left  NEG INC DEC
%left  RCPARENTHESIS

%start program

%%
// ***************************************************************************************************************************************************
// *********************************************************** PROGRAM *******************************************************************************
// ***************************************************************************************************************************************************
//hlavni programova smycka skladajici se z prikazu, stavby ramce a promenne, ktera zastupuje ramec a oporace nad nim
program:	program interpreter ENTER					{ //prikazy
										  printf("\r>> ");
										  YYACCEPT;
										}
	|	error ENTER							{ 
										  printf("\t%s\r\n", LANG_APP_PARSE_ERROR);
									 	  printf("\r>> ");
										  YYACCEPT;
										}
	|	ENTER								{
										  printf("\r>> ");
										  YYACCEPT;
										}
	|	program HELP subhelp						{
										  switch($3) {
											case 0 : { help.printHelp(); }; break;
											case 1 : { help.printRadioTapProperty(); }; break;
											case 2 : { help.printIeeeProperty(); }; break;
											case 3 : { help.printPayloadProperty(); }; break;
											case 4 : { help.printValue(); }; break;
											case 5 : { help.printLLCProperty(); }; break;
										  }
										  printf("\r>> ");
										  YYACCEPT;
										}
	|	LOAD LPARENTHESIS path RPARENTHESIS				{ 
										  char pName[MAX_LENGTH_ALIAS_AND_PATH];
										  if(typeid(*$3) == typeid(VariableValueExpression)) {
											$3 -> getVariableName(pName);
											if(!interpreter.getConfiguration()->alias.getPath(pName, pName)) {
												printf("\t%s\r\n", LANG_ALIAS_NOT_EXIST);
												YYACCEPT;
											}
										  } else {
											Variable path = $3 -> evaluateExpression(interpreter.getVariableTable());
											sprintf(pName, "%s", path.str.c_str());
										  } 
										  FILE * input = fopen(pName, "r+");
 										  if(input == NULL) {
											printf("\t%s\r\n", LANG_APP_LOAD_PROGRAM_KO);
										  } else {
											printf("\t%s\r\n>> ", LANG_APP_LOAD_PROGRAM_OK);
											char * buf;
											buf = NULL;
											int length = 0;
											while ((length = Utilities::readCommandFromFile(&input, &buf)) != 0) {
												printf("\r%s", buf);
												readFromBuffer(buf, length);
												yyparse();
												deleteBuffer();
											}
											printf("\r>>\r\n");
											fclose(input);
										  }
										  YYACCEPT;
										}										
	|									{ }
	|	END								{ //konec programu
										  YYACCEPT;
										}
	;

subhelp: 									{ $$ = 0; }
	|	H_RADIOTAP							{ $$ = 1; }
	|	H_IEEE								{ $$ = 2; }	
	|	P_PAYLOAD							{ $$ = 3; }
	|	H_VALUE								{ $$ = 4; }
	|	H_LLC								{ $$ = 5; }
	;

interpreter:	statement							{
										  interpreter.setRootStatement($1);
										  interpreter.executeStatement();
										}
	;
// ***************************************************************************************************************************************************
// *********************************************************** STATEMENT *****************************************************************************
// ***************************************************************************************************************************************************
statement:	WHILE LPARENTHESIS exp RPARENTHESIS bodystatement		{ 
										  WhileStatement * whileStatement = new WhileStatement();
										  whileStatement -> setCondition($3);
										  whileStatement -> setBody($5);
										  $$ = whileStatement;
										}
	|	FOR LPARENTHESIS forexp1 SEMICOLONS forexp2 SEMICOLONS forexp1 RPARENTHESIS bodystatement { 
										  ForStatement * forStatement = new ForStatement();
										  forStatement -> setInitCondition($3);
										  forStatement -> setCondition($5);
										  forStatement -> setAction($7);	
										  forStatement -> setBody($9);
										  $$ = forStatement;
										}
	|	IF LPARENTHESIS exp RPARENTHESIS bodystatement else 		{ 
										  IfStatement * ifStatement = new IfStatement();
										  ifStatement -> setCondition($3);
										  ifStatement -> setBody($5);
									          ifStatement -> setElseBody($6);
										  $$ = ifStatement;
										}
	|	exp								{ 
										  BodyStatement * bodyStatement = new BodyStatement();
										  bodyStatement -> addStatement(new ExpressionStatement($1));
										  $$ = bodyStatement;
										}
	|	exp3								{ 
										  BodyStatement * bodyStatement = new BodyStatement();
										  bodyStatement -> addStatement(new ExpressionStatement($1));
										  $$ = bodyStatement;
										}
	|	framebuild							{
										  BodyStatement * bodyStatement = new BodyStatement();
										  bodyStatement -> addStatement(new ExpressionStatement($1));
										  $$ = bodyStatement;
										}
	|	headerbuild							{ $$ = $1; }
	| 	command								{ $$ = $1; }
	;
forexp1: 	exp								{ $$ = $1; }
	|									{ $$ = NULL; }
	;
forexp2: 	exp2								{ $$ = $1; }
	|									{ $$ = NULL; }
	;
else:		ELSE bodystatement						{ $$ = $2; }
	|									{ $$ = NULL; }
	;

bodystatement:  enter LCPARENTHESIS body RCPARENTHESIS				{ $$ = $3; }
	;

body:		body statement enter						{
										  $1 -> addStatement($2);
										  $$ = $1;
										}
	|	enter								{ 
										  BodyStatement * bodyStatement = new BodyStatement();
										  $$ = bodyStatement;
										}
	;

repeat:		number	  							{ $$ = $1; }
	|	identifier  							{ $$ = $1; }
	;

delay:		number								{ $$ = $1; }
	|	identifier  							{ $$ = $1; }
	;

// ***************************************************************************************************************************************************
// *********************************************************** COMMAND  ******************************************************************************
// ***************************************************************************************************************************************************
command:  	SEND LPARENTHESIS identifier RPARENTHESIS			{ 
										  CommandStatement * commandStatement = new CommandStatement(ST_SEND);
										  commandStatement -> addParameter($3);
										  $$ = commandStatement;
										}
	|	ISEND LPARENTHESIS identifier COMMA repeat COMMA delay  RPARENTHESIS {
										  CommandStatement * commandStatement = new CommandStatement(ST_ISEND);
										  commandStatement -> addParameter($3);
										  commandStatement -> addParameter($5);
										  commandStatement -> addParameter($7);
										  $$ = commandStatement;
										}
	|	ISEND LPARENTHESIS identifier COMMA repeat RPARENTHESIS  	{
										  CommandStatement * commandStatement = new CommandStatement(ST_ISEND);
										  commandStatement -> addParameter($3);
										  commandStatement -> addParameter($5);
										  commandStatement -> addParameter(new NumberValueExpression(0));
										  $$ = commandStatement;
										}
	|	ISEND LPARENTHESIS identifier RPARENTHESIS  			{
										  CommandStatement * commandStatement = new CommandStatement(ST_ISEND);
										  commandStatement -> addParameter($3);
										  commandStatement -> addParameter(new NumberValueExpression(1));
										  commandStatement -> addParameter(new NumberValueExpression(0));
										  $$ = commandStatement;
										}
	|	ISEND LIST							{
										  CommandStatement * commandStatement = new CommandStatement(ST_ISEND_LIST);
										  $$ = commandStatement;
										}
	|	SLEEP LPARENTHESIS exp RPARENTHESIS				{
										  CommandStatement * commandStatement = new CommandStatement(ST_SLEEP);
										  commandStatement -> addParameter($3);
										  $$ = commandStatement;
										}
	|	MSLEEP LPARENTHESIS exp RPARENTHESIS				{
										  CommandStatement * commandStatement = new CommandStatement(ST_MSLEEP);
										  commandStatement -> addParameter($3);
										  $$ = commandStatement;
										}
	|	USLEEP LPARENTHESIS exp RPARENTHESIS				{
										  CommandStatement * commandStatement = new CommandStatement(ST_USLEEP);
										  commandStatement -> addParameter($3);
										  $$ = commandStatement;
										}
	|	DUMP LPARENTHESIS identifier RPARENTHESIS			{ 
										  CommandStatement * commandStatement = new CommandStatement(ST_DUMP);
										  commandStatement -> addParameter($3);
										  $$ = commandStatement;	
										}
	|	STR LPARENTHESIS identifier RPARENTHESIS			{ 
										  CommandStatement * commandStatement = new CommandStatement(ST_STR);
										  commandStatement -> addParameter($3);
										  $$ = commandStatement;	
										}
	|       CAPTURE	LPARENTHESIS exp RPARENTHESIS				{
										  CommandStatement * commandStatement = new CommandStatement(ST_CAPTURE);
										  commandStatement -> addParameter($3);
										  $$ = commandStatement;
										}
	|       identifier ASSIGN CAPTURE LPARENTHESIS exp RPARENTHESIS		{
										  CommandStatement * commandStatement = new CommandStatement(ST_CAPTURE_RET);
										  commandStatement -> addParameter($1);
										  commandStatement -> addParameter($5);
										  $$ = commandStatement;
										}
	|       identifier ASSIGN PARSE_IV LPARENTHESIS RPARENTHESIS		{
										  CommandStatement * commandStatement = new CommandStatement(ST_PARSE_IV);
										  commandStatement -> addParameter($1);
										  $$ = commandStatement;
										}
	|       identifier ASSIGN PARSE_IV LPARENTHESIS exp RPARENTHESIS	{
										  CommandStatement * commandStatement = new CommandStatement(ST_PARSE_IV_VAR);
										  commandStatement -> addParameter($1);
										  commandStatement -> addParameter($5);
										  $$ = commandStatement;
										}
	|       identifier ASSIGN SCAPY LPARENTHESIS exp RPARENTHESIS		{
										  CommandStatement * commandStatement = new CommandStatement(ST_SCAPY);
										  commandStatement -> addParameter($1);
										  commandStatement -> addParameter($5);
										  $$ = commandStatement;
										}
	|	KEY IW_LIST text						{
										  CommandStatement * commandStatement = new CommandStatement(ST_KEY_IWLIST);
										  commandStatement -> addParameter($3);
										  $$ = commandStatement;
										}
	|	KEY text path 							{
										  CommandStatement * commandStatement = new CommandStatement(ST_KEY_START);
										  commandStatement -> addParameter($2);
										  commandStatement -> addParameter($3);
										  commandStatement -> addParameter(new StringValueExpression(procName));
										  $$ = commandStatement;
										}
	|	KEY LIST							{
										  CommandStatement * commandStatement = new CommandStatement(ST_KEY_LIST);
										  $$ = commandStatement;
										}
	|	identifier ASSIGN GET_GTK_KEY LPARENTHESIS RPARENTHESIS		{
										  CommandStatement * commandStatement = new CommandStatement(ST_KEY_GTK);
										  commandStatement -> addParameter($1);
										  $$ = commandStatement;
										}
	|	identifier ASSIGN GET_PTK_KEY LPARENTHESIS RPARENTHESIS		{
										  CommandStatement * commandStatement = new CommandStatement(ST_KEY_PTK);
										  commandStatement -> addParameter($1);
										  $$ = commandStatement;
										}
	|	LIST								{ $$ = new CommandStatement(ST_LIST); }
	|	LIST ALL							{ $$ = new CommandStatement(ST_LIST_ALL); }
	|	PRINT LPARENTHESIS exp RPARENTHESIS				{ 
										  CommandStatement * commandStatement = new CommandStatement(ST_PRINT);
										  commandStatement -> addParameter($3);
										  $$ = commandStatement;
										}
	|	PRINT LPARENTHESIS getframeproperty RPARENTHESIS		{ 
										  CommandStatement * commandStatement = new CommandStatement(ST_PRINT);
										  commandStatement -> addParameter($3);
										  $$ = commandStatement;
										}

	|	DEL LPARENTHESIS identifier RPARENTHESIS			{
										  CommandStatement * commandStatement = new CommandStatement(ST_DEL);
										  commandStatement -> addParameter($3);
										  $$ = commandStatement;
										}
	|	identifier DOT LOAD LPARENTHESIS path RPARENTHESIS		{
										  CommandStatement * commandStatement = new CommandStatement(ST_LOAD);
										  commandStatement -> addParameter($1);
										  commandStatement -> addParameter($5);	
										  $$ = commandStatement;
										}
	|	identifier DOT SAVE LPARENTHESIS path RPARENTHESIS		{
										  CommandStatement * commandStatement = new CommandStatement(ST_SAVE);
										  commandStatement -> addParameter($1);
										  commandStatement -> addParameter($5);	
										  $$ = commandStatement;
										}
	|	ALIAS identifier ASSIGN QUOAT FILEPATH QUOAT			{
										  CommandStatement * commandStatement = new CommandStatement(ST_CREATE_ALIAS);
										  commandStatement -> addParameter($2);
										  commandStatement -> addParameter(new StringValueExpression($5)); free($5);	
										  $$ = commandStatement;
										}
	|	ALIAS identifier ASSIGN text					{
										  CommandStatement * commandStatement = new CommandStatement(ST_CREATE_ALIAS);
										  commandStatement -> addParameter($2);
										  commandStatement -> addParameter($4);	
										  $$ = commandStatement;
										}
	|	ALIAS DEL LPARENTHESIS identifier RPARENTHESIS			{
										  CommandStatement * commandStatement = new CommandStatement(ST_DEL_ALIAS);
										  commandStatement -> addParameter($4);
										  $$ = commandStatement;
										}
	|	ALIAS LIST							{
										  CommandStatement * commandStatement = new CommandStatement(ST_LIST_ALIAS);
										  $$ = commandStatement;
										}
	|	BREAK								{
										  CommandStatement * commandStatement = new CommandStatement(ST_BREAK);
										  $$ = commandStatement;
										}
	|	TIME START							{
										  CommandStatement * commandStatement = new CommandStatement(ST_TIME_START);
										  $$ = commandStatement;
										}
	|	TIME								{
										  CommandStatement * commandStatement = new CommandStatement(ST_TIME);
										  $$ = commandStatement;
										}
	;
// ***************************************************************************************************************************************************
// *********************************************************** EXPRESSION ****************************************************************************
// ***************************************************************************************************************************************************	
exp:       	exp PLUS exp 							{ $$ = new BExpression($1, $3, OP_PLUS); }
        | 	exp MINUS exp							{ $$ = new BExpression($1, $3, OP_MINUS); }
        | 	exp MULT exp							{ $$ = new BExpression($1, $3, OP_MUL); }
        | 	exp SLASH exp							{ $$ = new BExpression($1, $3, OP_DIV); }
        | 	exp PERC exp							{ $$ = new BExpression($1, $3, OP_MOD); }
        | 	exp LT exp							{ $$ = new BExpression($1, $3, OP_LT); }
        | 	exp GT exp							{ $$ = new BExpression($1, $3, OP_GT); }
        | 	exp LE exp							{ $$ = new BExpression($1, $3, OP_LE); }
        | 	exp GE exp							{ $$ = new BExpression($1, $3, OP_GE); }
        | 	exp EQ exp							{ $$ = new BExpression($1, $3, OP_EQ); }
        | 	exp NE exp							{ $$ = new BExpression($1, $3, OP_NE); }
        | 	exp ORL exp							{ $$ = new BExpression($1, $3, OP_ORL); }
        | 	exp ANDL exp							{ $$ = new BExpression($1, $3, OP_ANDL); }
        | 	exp OR exp							{ $$ = new BExpression($1, $3, OP_OR); }
        | 	exp AND exp							{ $$ = new BExpression($1, $3, OP_AND); }
	|	NEG exp								{ $$ = new UExpression($2,     OP_NEG); }
        | 	exp PLUS PLUS							{ $$ = new UExpression($1,     OP_INC); }
        | 	exp MINUS MINUS							{ $$ = new UExpression($1,     OP_DEC); }
        | 	identifier ASSIGN exp						{ $$ = new BExpression($1, $3, OP_ASSIGN); }
	|       LPARENTHESIS exp RPARENTHESIS					{ $$ = $2; }
        | 	identifier							{ $$ = $1; }
        | 	number								{ $$ = $1; }
        | 	text								{ $$ = $1; }
        ;
exp2:         	exp LT exp							{ $$ = new BExpression($1, $3, OP_LT); }
        | 	exp GT exp							{ $$ = new BExpression($1, $3, OP_GT); }
        | 	exp LE exp							{ $$ = new BExpression($1, $3, OP_LE); }
        | 	exp GE exp							{ $$ = new BExpression($1, $3, OP_GE); }
        | 	exp EQ exp							{ $$ = new BExpression($1, $3, OP_EQ); }
        | 	exp NE exp							{ $$ = new BExpression($1, $3, OP_NE); }
        | 	exp ORL exp							{ $$ = new BExpression($1, $3, OP_ORL); }
        | 	exp ANDL exp							{ $$ = new BExpression($1, $3, OP_ANDL); }
	;

exp3:		identifier ASSIGN getframeproperty				{ $$ = new BExpression($1, $3, OP_ASSIGN); }
	;

headerbuild:	identifier ASSIGN header					{ $$ = new AssignStatement($1, $3); }
	;

header:		radiotapheader SLASH ieeeheader SLASH llcheader			{ 
										  $$ = (AExpression *)(((FrameValueExpression *)$1 )
												-> addProperty(((FrameValueExpression *)$3) -> getPropertyList()) 
                                                                                                -> addProperty(((FrameValueExpression *)$5) -> getPropertyList())); 
										}
	|       radiotapheader SLASH ieeeheader					{ 
										  $$ = (AExpression *)(((FrameValueExpression *)$1 )
												-> addProperty(((FrameValueExpression *)$3) -> getPropertyList()));
										}
	|	radiotapheader SLASH llcheader					{ 
										  $$ = (AExpression *)(((FrameValueExpression *)$1 )
												-> addProperty(((FrameValueExpression *)$3) -> getPropertyList()));
										}
	|	ieeeheader     SLASH llcheader					{ 
										  $$ = (AExpression *)(((FrameValueExpression *)$1 )
												-> addProperty(((FrameValueExpression *)$3) -> getPropertyList()));
										}
	|	radiotapheader							{ $$ = $1; }
	|	ieeeheader							{ $$ = $1; }
	|	llcheader							{ $$ = $1; }
	;

radiotapheader:	H_RADIOTAP LPARENTHESIS radiotapheadercontent RPARENTHESIS	{ $$ = $3; }
	;
ieeeheader:     H_IEEE     LPARENTHESIS ieeeheadercontent RPARENTHESIS		{ $$ = $3; }	
	;
llcheader:	H_LLC      LPARENTHESIS llcheadercontent RPARENTHESIS		{ $$ = $3; }
	;
// ***************************************************************************************************************************************************
// *********************************************************** RADIOTAP HEADER CONTENT ***************************************************************
// ***************************************************************************************************************************************************
radiotapheadercontent:	radiotapheadercontent P_VERSION ASSIGN exp		{
										  FrameProperty * frameProperty = new FrameProperty(PT_RADIOTAP_VERSION);
										  frameProperty -> addValue($4);
										  ((FrameValueExpression *)$1) -> setFrameProperty(frameProperty);
										  $$ = $1; 
										}
	|      radiotapheadercontent P_PAD ASSIGN exp				{ 
										  FrameProperty * frameProperty = new FrameProperty(PT_RADIOTAP_PAD);
										  frameProperty -> addValue($4);
										  ((FrameValueExpression *)$1) -> setFrameProperty(frameProperty);
										  $$ = $1; 
										} 
	|      radiotapheadercontent P_ANTENNA ASSIGN exp			{ 
										  FrameProperty * frameProperty = new FrameProperty(PT_RADIOTAP_ANTENNA);
										  frameProperty -> addValue($4);
										  ((FrameValueExpression *)$1) -> setFrameProperty(frameProperty);
										  $$ = $1; 
										}
	|      radiotapheadercontent P_ANTENNA_NOISE ASSIGN exp			{ 
										  FrameProperty * frameProperty = new FrameProperty(PT_RADIOTAP_ANTENNA_NOISE);
										  frameProperty -> addValue($4);
										  ((FrameValueExpression *)$1) -> setFrameProperty(frameProperty);
										  $$ = $1; 
										}     
	|      radiotapheadercontent P_ANTENNA_SIGNAL ASSIGN exp		{ 
										  FrameProperty * frameProperty = new FrameProperty(PT_RADIOTAP_ANTENNA_SIGNAL);
										  frameProperty -> addValue($4);
										  ((FrameValueExpression *)$1) -> setFrameProperty(frameProperty);
										  $$ = $1; 
										}
	|      radiotapheadercontent P_CHANNEL_FREQUENCY ASSIGN exp		{ 
										  FrameProperty * frameProperty = new FrameProperty(PT_RADIOTAP_CHANNEL_FREQUENCY);
										  frameProperty -> addValue($4);
										  ((FrameValueExpression *)$1) -> setFrameProperty(frameProperty);
										  $$ = $1; 
										}
	|      radiotapheadercontent P_CHANNEL_FLAGS ASSIGN exp			{ 
										  FrameProperty * frameProperty = new FrameProperty(PT_RADIOTAP_CHANNEL_FLAGS);
										  frameProperty -> addValue($4);
										  ((FrameValueExpression *)$1) -> setFrameProperty(frameProperty);
										  $$ = $1; 
										}
	|      radiotapheadercontent P_CHANNEL_FLAGS ASSIGN QUOAT keyword QUOAT	{ 
										  FrameProperty * frameProperty = new FrameProperty(PT_RADIOTAP_CHANNEL_FLAGS);
										  frameProperty -> addValue($5);
										  ((FrameValueExpression *)$1) -> setFrameProperty(frameProperty);
										  $$ = $1; 
										  keywordInfo.include = keywordInfo.exclude = 0;
										}
	|      radiotapheadercontent P_FHSS_HOP_SET ASSIGN exp			{ 
										  FrameProperty * frameProperty = new FrameProperty(PT_RADIOTAP_FHSS_HOP_SET);
										  frameProperty -> addValue($4);
										  ((FrameValueExpression *)$1) -> setFrameProperty(frameProperty);
										  $$ = $1; 
										}
	|      radiotapheadercontent P_FHSS_HOP_PATTERN ASSIGN exp		{ 
										  FrameProperty * frameProperty = new FrameProperty(PT_RADIOTAP_FHSS_HOP_PATTERN);
										  frameProperty -> addValue($4);
										  ((FrameValueExpression *)$1) -> setFrameProperty(frameProperty);
										  $$ = $1; 
										}
	|      radiotapheadercontent P_FLAGS ASSIGN exp				{ 
										  FrameProperty * frameProperty = new FrameProperty(PT_RADIOTAP_FLAGS);
										  frameProperty -> addValue($4);
										  ((FrameValueExpression *)$1) -> setFrameProperty(frameProperty);
										  $$ = $1;  
										}
	|      radiotapheadercontent P_FLAGS ASSIGN QUOAT keyword QUOAT		{ 
										  FrameProperty * frameProperty = new FrameProperty(PT_RADIOTAP_FLAGS);
										  frameProperty -> addValue($5);
										  ((FrameValueExpression *)$1) -> setFrameProperty(frameProperty);
										  $$ = $1; 
								                  keywordInfo.include = keywordInfo.exclude = 0;
										}
	|      radiotapheadercontent P_LOCK_QUALITY ASSIGN exp			{ 
										  FrameProperty * frameProperty = new FrameProperty(PT_RADIOTAP_LOCK_QUALITY);
										  frameProperty -> addValue($4);
										  ((FrameValueExpression *)$1) -> setFrameProperty(frameProperty);
										  $$ = $1; 
										}
	|      radiotapheadercontent P_MCS_KNOWN ASSIGN exp			{ 
										  FrameProperty * frameProperty = new FrameProperty(PT_RADIOTAP_MCS_KNOWN);
										  frameProperty -> addValue($4);
										  ((FrameValueExpression *)$1) -> setFrameProperty(frameProperty);
										  $$ = $1; 
										}
	|      radiotapheadercontent P_MCS_FLAGS ASSIGN exp			{ 
										  FrameProperty * frameProperty = new FrameProperty(PT_RADIOTAP_MCS_FLAGS);
										  frameProperty -> addValue($4);
										  ((FrameValueExpression *)$1) -> setFrameProperty(frameProperty);
										  $$ = $1; 
										}
	|      radiotapheadercontent P_MCS_FLAGS ASSIGN QUOAT keyword QUOAT	{ 
       										  FrameProperty * frameProperty = new FrameProperty(PT_RADIOTAP_MCS_FLAGS);
										  frameProperty -> addValue($5);
										  ((FrameValueExpression *)$1) -> setFrameProperty(frameProperty);
										  $$ = $1; 
										  keywordInfo.include = keywordInfo.exclude = 0; 
										}
	|      radiotapheadercontent P_MCS_MCS ASSIGN exp			{ 
										  FrameProperty * frameProperty = new FrameProperty(PT_RADIOTAP_MCS_MCS);
										  frameProperty -> addValue($4);
										  ((FrameValueExpression *)$1) -> setFrameProperty(frameProperty);
										  $$ = $1; 
										}
	|      radiotapheadercontent P_RX_FLAGS ASSIGN exp			{ 
										  FrameProperty * frameProperty = new FrameProperty(PT_RADIOTAP_RX_FLAGS);
										  frameProperty -> addValue($4);
										  ((FrameValueExpression *)$1) -> setFrameProperty(frameProperty);
										  $$ = $1; 
										}
	|      radiotapheadercontent P_RX_FLAGS ASSIGN QUOAT keyword QUOAT	{ 
										  FrameProperty * frameProperty = new FrameProperty(PT_RADIOTAP_RX_FLAGS);
										  frameProperty -> addValue($5);
										  ((FrameValueExpression *)$1) -> setFrameProperty(frameProperty);
										  $$ = $1; 
										  keywordInfo.include = keywordInfo.exclude = 0; 
										}
	|      radiotapheadercontent P_RATE ASSIGN exp				{ 
										  FrameProperty * frameProperty = new FrameProperty(PT_RADIOTAP_RATE);
										  frameProperty -> addValue($4);
										  ((FrameValueExpression *)$1) -> setFrameProperty(frameProperty);
										  $$ = $1; 
										}
	|      radiotapheadercontent P_TSFT ASSIGN exp				{ 
										  FrameProperty * frameProperty = new FrameProperty(PT_RADIOTAP_TSFT);
										  frameProperty -> addValue($4);
										  ((FrameValueExpression *)$1) -> setFrameProperty(frameProperty);
										  $$ = $1; 
										}
	|      radiotapheadercontent P_TX_ATTENUATION ASSIGN exp	 	{
										  FrameProperty * frameProperty = new FrameProperty(PT_RADIOTAP_TX_ATTENUATION);
										  frameProperty -> addValue($4);
										  ((FrameValueExpression *)$1) -> setFrameProperty(frameProperty);
										  $$ = $1; 
										}
	|      radiotapheadercontent P_VENDOR_NAMESPACE_OUI ASSIGN QUOAT OUI QUOAT 	{
										  FrameProperty * frameProperty = new FrameProperty(PT_RADIOTAP_VENDOR_NAMESPACE_OUI);
										  frameProperty -> addValue(new StringValueExpression($5)); free($5);
										  ((FrameValueExpression *)$1) -> setFrameProperty(frameProperty);
										  $$ = $1; 
										}
	|      radiotapheadercontent P_VENDOR_NAMESPACE_SUB_NAMESPACE ASSIGN exp{
										  FrameProperty * frameProperty = 
											new FrameProperty(PT_RADIOTAP_VENDOR_NAMESPACE_SUB_NAMESPACE);
										  frameProperty -> addValue($4);
										  ((FrameValueExpression *)$1) -> setFrameProperty(frameProperty);
										  $$ = $1; 
										}
	|      radiotapheadercontent P_VENDOR_NAMESPACE_SKIP_LENGTH ASSIGN exp	{
										  FrameProperty * frameProperty = 
											new FrameProperty(PT_RADIOTAP_VENDOR_NAMESPACE_SKIP_LENGTH);
										  frameProperty -> addValue($4);
										  ((FrameValueExpression *)$1) -> setFrameProperty(frameProperty);
										  $$ = $1; 
										}
	|      radiotapheadercontent P_DB_TX_ATTENUATION ASSIGN exp		{
										  FrameProperty * frameProperty = new FrameProperty(PT_RADIOTAP_DB_TX_ATTENUATION);
										  frameProperty -> addValue($4);
										  ((FrameValueExpression *)$1) -> setFrameProperty(frameProperty);
										  $$ = $1; 
										}
	|      radiotapheadercontent P_DB_ANTENNA_NOISE ASSIGN exp		{
										  FrameProperty * frameProperty = new FrameProperty(PT_RADIOTAP_DB_ANTENNA_NOISE);
										  frameProperty -> addValue($4);
										  ((FrameValueExpression *)$1) -> setFrameProperty(frameProperty);
										  $$ = $1; 
										}
	|      radiotapheadercontent P_DB_ANTENNA_SIGNAL ASSIGN exp		{
										  FrameProperty * frameProperty = new FrameProperty(PT_RADIOTAP_DB_ANTENNA_SIGNAL);
										  frameProperty -> addValue($4);
										  ((FrameValueExpression *)$1) -> setFrameProperty(frameProperty);
										  $$ = $1; 
										}
	|      radiotapheadercontent P_DBM_TX_POWER ASSIGN exp			{
										  FrameProperty * frameProperty = new FrameProperty(PT_RADIOTAP_DBM_TX_POWER);
										  frameProperty -> addValue($4);
										  ((FrameValueExpression *)$1) -> setFrameProperty(frameProperty);
										  $$ = $1; 
										}
	|      radiotapheadercontent P_RTS_RETRIES ASSIGN exp			{
										  FrameProperty * frameProperty = new FrameProperty(PT_RADIOTAP_RTS_RETRIES);
										  frameProperty -> addValue($4);
										  ((FrameValueExpression *)$1) -> setFrameProperty(frameProperty);
										  $$ = $1; 
										}
	|      radiotapheadercontent P_DATA_RETRIES ASSIGN exp			{
										  FrameProperty * frameProperty = new FrameProperty(PT_RADIOTAP_DATA_RETRIES);
										  frameProperty -> addValue($4);
										  ((FrameValueExpression *)$1) -> setFrameProperty(frameProperty);
										  $$ = $1; 
										}
	|									{ 
										  string str(CONTENT_RADIOTAP_HEADER);
										  $$ = new FrameValueExpression(new VariableValueExpression((char *)str.c_str()));
										}
	;
// ***************************************************************************************************************************************************
// *********************************************************** IEEE HEADER CONTENT *******************************************************************
// ***************************************************************************************************************************************************
ieeeheadercontent: ieeeheadercontent P_FRAME_CONTROL ASSIGN exp			{ 
										  FrameProperty * frameProperty = new FrameProperty(PT_IEEE_FRAME_CONTROL);
										  frameProperty -> addValue($4);
										  ((FrameValueExpression *)$1) -> setFrameProperty(frameProperty);
										  $$ = $1;
										}
	|      ieeeheadercontent P_TYPE ASSIGN QUOAT K_DATA QUOAT		{
										  FrameProperty * frameProperty = new FrameProperty(PT_IEEE_TYPE_DATA);
										  ((FrameValueExpression *)$1) -> setFrameProperty(frameProperty);
										  $$ = $1;
										}
	|      ieeeheadercontent P_TYPE ASSIGN QUOAT K_QOS_DATA QUOAT		{
										  FrameProperty * frameProperty = new FrameProperty(PT_IEEE_TYPE_QOS_DATA);
										  ((FrameValueExpression *)$1) -> setFrameProperty(frameProperty);
										  $$ = $1;
										}
	|      ieeeheadercontent P_TYPE ASSIGN QUOAT K_RTS QUOAT		{
										  FrameProperty * frameProperty = new FrameProperty(PT_IEEE_TYPE_RTS);
										  ((FrameValueExpression *)$1) -> setFrameProperty(frameProperty);
										  $$ = $1;
										}
	|      ieeeheadercontent P_TYPE ASSIGN QUOAT K_CTS QUOAT		{
										  FrameProperty * frameProperty = new FrameProperty(PT_IEEE_TYPE_CTS);
										  ((FrameValueExpression *)$1) -> setFrameProperty(frameProperty);
										  $$ = $1;
										}
	|      ieeeheadercontent P_TYPE ASSIGN QUOAT K_ACK QUOAT	 	{
										  FrameProperty * frameProperty = new FrameProperty(PT_IEEE_TYPE_ACK);
										  ((FrameValueExpression *)$1) -> setFrameProperty(frameProperty);
										  $$ = $1;
										}
	|      ieeeheadercontent P_TYPE ASSIGN QUOAT K_PS_POLL QUOAT		{
										  FrameProperty * frameProperty = new FrameProperty(PT_IEEE_TYPE_PS_POLL);
										  ((FrameValueExpression *)$1) -> setFrameProperty(frameProperty);
										  $$ = $1;
										}
	|      ieeeheadercontent P_TYPE ASSIGN QUOAT K_BEACON QUOAT		{
										  FrameProperty * frameProperty = new FrameProperty(PT_IEEE_TYPE_BEACON);
										  ((FrameValueExpression *)$1) -> setFrameProperty(frameProperty);
										  $$ = $1;
										}
	|      ieeeheadercontent P_TYPE ASSIGN QUOAT K_PROBE_REQUEST QUOAT	{
										  FrameProperty * frameProperty = new FrameProperty(PT_IEEE_TYPE_PROBE_REQUEST);
										  ((FrameValueExpression *)$1) -> setFrameProperty(frameProperty);
										  $$ = $1;
										}
	|      ieeeheadercontent P_TYPE ASSIGN QUOAT K_PROBE_RESPONSE QUOAT	{
										  FrameProperty * frameProperty = new FrameProperty(PT_IEEE_TYPE_PROBE_RESPONSE);
										  ((FrameValueExpression *)$1) -> setFrameProperty(frameProperty);
										  $$ = $1;
										}
	|      ieeeheadercontent P_TYPE ASSIGN QUOAT K_ATIM QUOAT		{
										  FrameProperty * frameProperty = new FrameProperty(PT_IEEE_TYPE_ATIM);
										  ((FrameValueExpression *)$1) -> setFrameProperty(frameProperty);
										  $$ = $1;
										}
	|      ieeeheadercontent P_TYPE ASSIGN QUOAT K_DEAUTHETICATION QUOAT	{
										  FrameProperty * frameProperty = new FrameProperty(PT_IEEE_TYPE_DEAUTHETICATION);
										  ((FrameValueExpression *)$1) -> setFrameProperty(frameProperty);
										  $$ = $1;
										}
	|      ieeeheadercontent P_TYPE ASSIGN QUOAT K_DISASSOCIATION QUOAT	{
										  FrameProperty * frameProperty = new FrameProperty(PT_IEEE_TYPE_DISASSOCIATION);
										  ((FrameValueExpression *)$1) -> setFrameProperty(frameProperty);
										  $$ = $1;
										}
	|      ieeeheadercontent P_TYPE ASSIGN QUOAT K_ASSOCIATION_REQUEST QUOAT{
										  FrameProperty * frameProperty = new FrameProperty(PT_IEEE_TYPE_ASSOCIATION_REQUEST);
										  ((FrameValueExpression *)$1) -> setFrameProperty(frameProperty);
										  $$ = $1;
										}
	|      ieeeheadercontent P_TYPE ASSIGN QUOAT K_REASSOCIATION_REQUEST QUOAT {
										  FrameProperty * frameProperty = 
												new FrameProperty(PT_IEEE_TYPE_REASSOCIATION_REQUEST);
										  ((FrameValueExpression *)$1) -> setFrameProperty(frameProperty);
										  $$ = $1;
										}
	|      ieeeheadercontent P_TYPE ASSIGN QUOAT K_ASSOCIATION_RESPONSE QUOAT {
										  FrameProperty * frameProperty = 
												new FrameProperty(PT_IEEE_TYPE_ASSOCIATION_RESPONSE);
										  ((FrameValueExpression *)$1) -> setFrameProperty(frameProperty);
										  $$ = $1;
										}
	|      ieeeheadercontent P_TYPE ASSIGN QUOAT K_AUTHENTICATION QUOAT 	{
										  FrameProperty * frameProperty = new FrameProperty(PT_IEEE_TYPE_AUTHENTICATION);
										  ((FrameValueExpression *)$1) -> setFrameProperty(frameProperty);
										  $$ = $1;
										}
	|      ieeeheadercontent P_PROTOCOL ASSIGN exp				{
										  FrameProperty * frameProperty = new FrameProperty(PT_IEEE_PROTOCOL);
										  frameProperty -> addValue($4);
										  ((FrameValueExpression *)$1) -> setFrameProperty(frameProperty);
										  $$ = $1;
										}
	|      ieeeheadercontent P_TYPE ASSIGN exp				{
										  FrameProperty * frameProperty = new FrameProperty(PT_IEEE_TYPE);
										  frameProperty -> addValue($4);
										  ((FrameValueExpression *)$1) -> setFrameProperty(frameProperty);
										  $$ = $1;
										}
	|      ieeeheadercontent P_SUB_TYPE ASSIGN exp				{
										  FrameProperty * frameProperty = new FrameProperty(PT_IEEE_SUB_TYPE);
										  frameProperty -> addValue($4);
										  ((FrameValueExpression *)$1) -> setFrameProperty(frameProperty);
										  $$ = $1;
										}
	|      ieeeheadercontent P_TO_DS ASSIGN exp				{
										  FrameProperty * frameProperty = new FrameProperty(PT_IEEE_TO_DS);
										  frameProperty -> addValue($4);
										  ((FrameValueExpression *)$1) -> setFrameProperty(frameProperty);
										  $$ = $1;
										}
	|      ieeeheadercontent P_FROM_DS ASSIGN exp				{
										  FrameProperty * frameProperty = new FrameProperty(PT_IEEE_FROM_DS);
										  frameProperty -> addValue($4);
										  ((FrameValueExpression *)$1) -> setFrameProperty(frameProperty);
										  $$ = $1;
										}
	|      ieeeheadercontent P_MORE_FRAG ASSIGN exp				{
										  FrameProperty * frameProperty = new FrameProperty(PT_IEEE_MORE_FRAG);
										  frameProperty -> addValue($4);
										  ((FrameValueExpression *)$1) -> setFrameProperty(frameProperty);
										  $$ = $1;
										}
	|      ieeeheadercontent P_RETRY ASSIGN exp				{
										  FrameProperty * frameProperty = new FrameProperty(PT_IEEE_RETRY);
										  frameProperty -> addValue($4);
										  ((FrameValueExpression *)$1) -> setFrameProperty(frameProperty);
										  $$ = $1;
										}
	|      ieeeheadercontent P_PWR_MGMT ASSIGN exp				{
										  FrameProperty * frameProperty = new FrameProperty(PT_IEEE_PWR_MGMT);
										  frameProperty -> addValue($4);
										  ((FrameValueExpression *)$1) -> setFrameProperty(frameProperty);
										  $$ = $1;
										}
	|      ieeeheadercontent P_MORE_DATA ASSIGN exp				{
										  FrameProperty * frameProperty = new FrameProperty(PT_IEEE_MORE_DATA);
										  frameProperty -> addValue($4);
										  ((FrameValueExpression *)$1) -> setFrameProperty(frameProperty);
										  $$ = $1;
										}
	|      ieeeheadercontent P_PROTECTED_FRAME ASSIGN exp			{
										  FrameProperty * frameProperty = new FrameProperty(PT_IEEE_PROTECTED_FRAME);
										  frameProperty -> addValue($4);
										  ((FrameValueExpression *)$1) -> setFrameProperty(frameProperty);
										  $$ = $1;
										}
	|      ieeeheadercontent P_ORDER ASSIGN exp				{
										  FrameProperty * frameProperty = new FrameProperty(PT_IEEE_ORDER);
										  frameProperty -> addValue($4);
										  ((FrameValueExpression *)$1) -> setFrameProperty(frameProperty);
										  $$ = $1;
										}
	|      ieeeheadercontent P_BSSID ASSIGN QUOAT MAC_ADDRESS QUOAT		{
										  FrameProperty * frameProperty = new FrameProperty(PT_IEEE_BSSID);
										  frameProperty -> addValue(new StringValueExpression($5)); free($5);
										  ((FrameValueExpression *)$1) -> setFrameProperty(frameProperty);
										  $$ = $1;
										}
	|      ieeeheadercontent P_BSSID ASSIGN exp				{
										  FrameProperty * frameProperty = new FrameProperty(PT_IEEE_BSSID);
										  frameProperty -> addValue($4);
										  ((FrameValueExpression *)$1) -> setFrameProperty(frameProperty);
										  $$ = $1;
										}
	|      ieeeheadercontent P_RECEIVER_ADDRESS ASSIGN QUOAT MAC_ADDRESS QUOAT {
										  FrameProperty * frameProperty = new FrameProperty(PT_IEEE_RECEIVER_ADDRESS);
										  frameProperty -> addValue(new StringValueExpression($5)); free($5);
										  ((FrameValueExpression *)$1) -> setFrameProperty(frameProperty);
										  $$ = $1;
										}
	|      ieeeheadercontent P_RECEIVER_ADDRESS ASSIGN exp			{
										  FrameProperty * frameProperty = new FrameProperty(PT_IEEE_RECEIVER_ADDRESS);
										  frameProperty -> addValue($4);
										  ((FrameValueExpression *)$1) -> setFrameProperty(frameProperty);
										  $$ = $1;
										}
	|      ieeeheadercontent P_TRANSMITTER_ADDRESS ASSIGN QUOAT MAC_ADDRESS QUOAT {
										  FrameProperty * frameProperty = new FrameProperty(PT_IEEE_TRANSMITTER_ADDRESS);
										  frameProperty -> addValue(new StringValueExpression($5)); free($5);
										  ((FrameValueExpression *)$1) -> setFrameProperty(frameProperty);
										  $$ = $1;
										}
	|      ieeeheadercontent P_TRANSMITTER_ADDRESS ASSIGN exp		{
										  FrameProperty * frameProperty = new FrameProperty(PT_IEEE_TRANSMITTER_ADDRESS);
										  frameProperty -> addValue($4);
										  ((FrameValueExpression *)$1) -> setFrameProperty(frameProperty);
										  $$ = $1;
										}
	|      ieeeheadercontent P_DURATION ASSIGN exp				{ 
										  FrameProperty * frameProperty = new FrameProperty(PT_IEEE_DURATION);
										  frameProperty -> addValue($4);
										  ((FrameValueExpression *)$1) -> setFrameProperty(frameProperty);
										  $$ = $1;
										}
	|      ieeeheadercontent P_AID ASSIGN exp				{ 
										  FrameProperty * frameProperty = new FrameProperty(PT_IEEE_AID);
										  frameProperty -> addValue($4);
										  ((FrameValueExpression *)$1) -> setFrameProperty(frameProperty);
										  $$ = $1;
										}
	|      ieeeheadercontent P_DA ASSIGN QUOAT MAC_ADDRESS QUOAT 		{ 
										  FrameProperty * frameProperty = new FrameProperty(PT_IEEE_DA);
										  frameProperty -> addValue(new StringValueExpression($5)); free($5);
										  ((FrameValueExpression *)$1) -> setFrameProperty(frameProperty);
										  $$ = $1;
										}
	|      ieeeheadercontent P_DA ASSIGN exp		 		{ 
										  FrameProperty * frameProperty = new FrameProperty(PT_IEEE_DA);
										  frameProperty -> addValue($4);
										  ((FrameValueExpression *)$1) -> setFrameProperty(frameProperty);
										  $$ = $1;
										}
	|      ieeeheadercontent P_SA ASSIGN QUOAT MAC_ADDRESS QUOAT 		{ 
										  FrameProperty * frameProperty = new FrameProperty(PT_IEEE_SA);
										  frameProperty -> addValue(new StringValueExpression($5)); free($5);
										  ((FrameValueExpression *)$1) -> setFrameProperty(frameProperty);
										  $$ = $1;
										}
	|      ieeeheadercontent P_SA ASSIGN exp		 		{ 
										  FrameProperty * frameProperty = new FrameProperty(PT_IEEE_SA);
										  frameProperty -> addValue($4);
										  ((FrameValueExpression *)$1) -> setFrameProperty(frameProperty);
										  $$ = $1;
										}
	|      ieeeheadercontent P_SEQ_CTL ASSIGN exp				{ 
										  FrameProperty * frameProperty = new FrameProperty(PT_IEEE_SEQ_CTL);
										  frameProperty -> addValue($4);
										  ((FrameValueExpression *)$1) -> setFrameProperty(frameProperty);
										  $$ = $1;
										}
	|      ieeeheadercontent P_FRAGMENT ASSIGN exp				{
										  FrameProperty * frameProperty = new FrameProperty(PT_IEEE_FRAGMENT);
										  frameProperty -> addValue($4);
										  ((FrameValueExpression *)$1) -> setFrameProperty(frameProperty);
										  $$ = $1;
										} 
	|      ieeeheadercontent P_SEQUENCE ASSIGN exp				{
										  FrameProperty * frameProperty = new FrameProperty(PT_IEEE_SEQUENCE);
										  frameProperty -> addValue($4);
										  ((FrameValueExpression *)$1) -> setFrameProperty(frameProperty);
										  $$ = $1;
										}
	|      ieeeheadercontent P_TIMESTAMP ASSIGN exp				{ 
										  FrameProperty * frameProperty = new FrameProperty(PT_IEEE_TIMESTAMP);
										  frameProperty -> addValue($4);
										  ((FrameValueExpression *)$1) -> setFrameProperty(frameProperty);
										  $$ = $1; 
										}
	|      ieeeheadercontent P_BEACON_INTERVAL ASSIGN exp			{ 
										  FrameProperty * frameProperty = new FrameProperty(PT_IEEE_BEACON_INTERVAL);
										  frameProperty -> addValue($4);
										  ((FrameValueExpression *)$1) -> setFrameProperty(frameProperty);
										  $$ = $1;
										}
	|      ieeeheadercontent P_CAPABILITY_INFO ASSIGN exp			{	 
										  FrameProperty * frameProperty = new FrameProperty(PT_IEEE_CAPABILITY_INFO);
										  frameProperty -> addValue($4);
										  ((FrameValueExpression *)$1) -> setFrameProperty(frameProperty);
										  $$ = $1;
										}
	|      ieeeheadercontent P_CAPABILITY_INFO ASSIGN QUOAT keyword QUOAT	{ 
										  FrameProperty * frameProperty = new FrameProperty(PT_IEEE_CAPABILITY_INFO);
										  frameProperty -> addValue($5);
										  ((FrameValueExpression *)$1) -> setFrameProperty(frameProperty);
										  $$ = $1;
										}
	|      ieeeheadercontent P_SSID ASSIGN text				{ 
										  FrameProperty * frameProperty = new FrameProperty(PT_IEEE_SSID);
										  frameProperty -> addValue($4);
										  ((FrameValueExpression *)$1) -> setFrameProperty(frameProperty);
										  $$ = $1;
										}
	|      ieeeheadercontent P_FH_PARAMETER_SET_DWELL_TIME ASSIGN exp	{ 
										  FrameProperty * frameProperty = 
												new FrameProperty(PT_IEEE_FH_PARAMETER_SET_DWELL_TIME);
										  frameProperty -> addValue($4);
										  ((FrameValueExpression *)$1) -> setFrameProperty(frameProperty);
										  $$ = $1;
										}
	|      ieeeheadercontent P_FH_PARAMETER_SET_HOP_SET ASSIGN exp		{ 
										  FrameProperty * frameProperty = new FrameProperty(PT_IEEE_FH_PARAMETER_SET_HOP_SET);
										  frameProperty -> addValue($4);
										  ((FrameValueExpression *)$1) -> setFrameProperty(frameProperty);
										  $$ = $1;
										}
	|      ieeeheadercontent P_FH_PARAMETER_SET_HOP_PATTERN ASSIGN exp	{ 
										  FrameProperty * frameProperty = 
												new FrameProperty(PT_IEEE_FH_PARAMETER_SET_HOP_PATTERN);
										  frameProperty -> addValue($4);
										  ((FrameValueExpression *)$1) -> setFrameProperty(frameProperty);
										  $$ = $1;
										}
	|      ieeeheadercontent P_FH_PARAMETER_SET_HOP_INDEX ASSIGN exp	{ 
										  FrameProperty * frameProperty = 
												new FrameProperty(PT_IEEE_FH_PARAMETER_SET_HOP_INDEX);
										  frameProperty -> addValue($4);
										  ((FrameValueExpression *)$1) -> setFrameProperty(frameProperty);
										  $$ = $1;
										}
	|      ieeeheadercontent P_DS_PARAMETER_SET ASSIGN exp			{ 
										  FrameProperty * frameProperty = new FrameProperty(PT_IEEE_DS_PARAMETER_SET);
										  frameProperty -> addValue($4);
										  ((FrameValueExpression *)$1) -> setFrameProperty(frameProperty);
										  $$ = $1;
										}
	|      ieeeheadercontent P_CF_PARAMETER_SET_COUNT ASSIGN exp		{ 
										  FrameProperty * frameProperty = new FrameProperty(PT_IEEE_CF_PARAMETER_SET_COUNT);
										  frameProperty -> addValue($4);
										  ((FrameValueExpression *)$1) -> setFrameProperty(frameProperty);
										  $$ = $1;
										}
	|      ieeeheadercontent P_CF_PARAMETER_SET_PERIOD ASSIGN exp		{ 
										  FrameProperty * frameProperty = new FrameProperty(PT_IEEE_CF_PARAMETER_SET_PERIOD);
										  frameProperty -> addValue($4);
										  ((FrameValueExpression *)$1) -> setFrameProperty(frameProperty);
										  $$ = $1;
										}
	|      ieeeheadercontent P_CF_PARAMETER_SET_MAX_DURATION ASSIGN exp	{ 
										  FrameProperty * frameProperty = 
												new FrameProperty(PT_IEEE_CF_PARAMETER_SET_MAX_DURATION);
										  frameProperty -> addValue($4);
										  ((FrameValueExpression *)$1) -> setFrameProperty(frameProperty);
										  $$ = $1;
										}
	|      ieeeheadercontent P_CF_PARAMETER_SET_REMAINING ASSIGN exp	{ 
										  FrameProperty * frameProperty = 
												new FrameProperty(PT_IEEE_CF_PARAMETER_SET_REMAINING);
										  frameProperty -> addValue($4);
										  ((FrameValueExpression *)$1) -> setFrameProperty(frameProperty);
										  $$ = $1;
										}
	|      ieeeheadercontent P_IBSS_PARAMETER_SET ASSIGN exp		{ 
										  FrameProperty * frameProperty = new FrameProperty(PT_IEEE_IBSS_PARAMETER_SET);
										  frameProperty -> addValue($4);
										  ((FrameValueExpression *)$1) -> setFrameProperty(frameProperty);
										  $$ = $1;
										}
	|      ieeeheadercontent P_TIM_DTIM_COUNT ASSIGN exp			{ 
										  FrameProperty * frameProperty = new FrameProperty(PT_IEEE_TIM_DTIM_COUNT);
										  frameProperty -> addValue($4);
										  ((FrameValueExpression *)$1) -> setFrameProperty(frameProperty);
										  $$ = $1;
										}
	|      ieeeheadercontent P_TIM_DTIM_PERIOD ASSIGN exp			{ 
										  FrameProperty * frameProperty = new FrameProperty(PT_IEEE_TIM_DTIM_PERIOD);
										  frameProperty -> addValue($4);
										  ((FrameValueExpression *)$1) -> setFrameProperty(frameProperty);
										  $$ = $1;
										}
	|      ieeeheadercontent P_TIM_BITMAP_CONTROL ASSIGN exp		{ 
										  FrameProperty * frameProperty = new FrameProperty(PT_IEEE_TIM_BITMAP_CONTROL);
										  frameProperty -> addValue($4);
										  ((FrameValueExpression *)$1) -> setFrameProperty(frameProperty);
										  $$ = $1;
										}
	|      ieeeheadercontent P_TIM_PARTIAL_VIRTUAL_BITMAP ASSIGN HEX_NUMBER	{ 
										  FrameProperty * frameProperty = 
												new FrameProperty(PT_IEEE_TIM_PARTIAL_VIRTUAL_BITMAP);
										  frameProperty -> addValue(new StringValueExpression($4)); free($4);
										  ((FrameValueExpression *)$1) -> setFrameProperty(frameProperty);
										  $$ = $1;
										}
	|      ieeeheadercontent P_COUNTRY_STRING ASSIGN text			{	 
										  FrameProperty * frameProperty = new FrameProperty(PT_IEEE_COUNTRY_STRING);
										  frameProperty -> addValue($4);
										  ((FrameValueExpression *)$1) -> setFrameProperty(frameProperty);
										  $$ = $1;
										}
	|      ieeeheadercontent P_COUNTRY_CONSTRAINT ASSIGN exp DD exp DD exp DD exp {	 
										  FrameProperty * frameProperty = new FrameProperty(PT_IEEE_COUNTRY_CONSTRAINT);
										  frameProperty -> addValue($4);
										  frameProperty -> addValue($6);
										  frameProperty -> addValue($8);
										  frameProperty -> addValue($10);
										  ((FrameValueExpression *)$1) -> setFrameProperty(frameProperty);
										  $$ = $1;
										}
	|      ieeeheadercontent P_LOCAL_POWER_CONSTRAINT ASSIGN exp		{	 
										  FrameProperty * frameProperty = new FrameProperty(PT_IEEE_LOCAL_POWER_CONSTRAINT);
										  frameProperty -> addValue($4);
										  ((FrameValueExpression *)$1) -> setFrameProperty(frameProperty);
										  $$ = $1;
										}
	|      ieeeheadercontent P_CHANNEL_SWITCH_MODE ASSIGN exp		{	 
										  FrameProperty * frameProperty = new FrameProperty(PT_IEEE_CHANNEL_SWITCH_MODE);
										  frameProperty -> addValue($4);
										  ((FrameValueExpression *)$1) -> setFrameProperty(frameProperty);
										  $$ = $1;
										}
	|      ieeeheadercontent P_NEW_CHANNEL_NUMBER ASSIGN exp		{	 
										  FrameProperty * frameProperty = new FrameProperty(PT_IEEE_NEW_CHANNEL_NUMBER);
										  frameProperty -> addValue($4);
										  ((FrameValueExpression *)$1) -> setFrameProperty(frameProperty);
										  $$ = $1;
										}
	|      ieeeheadercontent P_CHANNEL_SWITCH_COUNT ASSIGN exp		{	 
										  FrameProperty * frameProperty = new FrameProperty(PT_IEEE_CHANNEL_SWITCH_COUNT);
										  frameProperty -> addValue($4);
										  ((FrameValueExpression *)$1) -> setFrameProperty(frameProperty);
										  $$ = $1;
										}
	|      ieeeheadercontent P_QUIET_COUNT ASSIGN exp			{	 
										  FrameProperty * frameProperty = new FrameProperty(PT_IEEE_QUIET_COUNT);
										  frameProperty -> addValue($4);
										  ((FrameValueExpression *)$1) -> setFrameProperty(frameProperty);
										  $$ = $1;
										}
	|      ieeeheadercontent P_QUIET_PERIOD ASSIGN exp			{	 
										  FrameProperty * frameProperty = new FrameProperty(PT_IEEE_QUIET_PERIOD);
										  frameProperty -> addValue($4);
										  ((FrameValueExpression *)$1) -> setFrameProperty(frameProperty);
										  $$ = $1;
										}
	|      ieeeheadercontent P_QUIET_DURATION ASSIGN exp			{	 
										  FrameProperty * frameProperty = new FrameProperty(PT_IEEE_QUIET_DURATION);
										  frameProperty -> addValue($4);
										  ((FrameValueExpression *)$1) -> setFrameProperty(frameProperty);
										  $$ = $1;
										}
	|      ieeeheadercontent P_QUIET_OFFSET ASSIGN exp			{	 
										  FrameProperty * frameProperty = new FrameProperty(PT_IEEE_QUIET_OFFSET);
										  frameProperty -> addValue($4);
										  ((FrameValueExpression *)$1) -> setFrameProperty(frameProperty);
										  $$ = $1;
										}
	|      ieeeheadercontent P_TRANSMIT_POWER ASSIGN exp			{	 
										  FrameProperty * frameProperty = new FrameProperty(PT_IEEE_TRANSMIT_POWER);
										  frameProperty -> addValue($4);
										  ((FrameValueExpression *)$1) -> setFrameProperty(frameProperty);
										  $$ = $1;
										}
	|      ieeeheadercontent P_LINK_MARGIN ASSIGN exp			{	 
										  FrameProperty * frameProperty = new FrameProperty(PT_IEEE_LINK_MARGIN);
										  frameProperty -> addValue($4);
										  ((FrameValueExpression *)$1) -> setFrameProperty(frameProperty);
										  $$ = $1;
										}
	|      ieeeheadercontent P_ERP ASSIGN exp				{	 
										  FrameProperty * frameProperty = new FrameProperty(PT_IEEE_ERP);
										  frameProperty -> addValue($4);
										  ((FrameValueExpression *)$1) -> setFrameProperty(frameProperty);
										  $$ = $1;
										}
	|      ieeeheadercontent P_ERP ASSIGN QUOAT keyword QUOAT		{	 
										  FrameProperty * frameProperty = new FrameProperty(PT_IEEE_ERP);
										  frameProperty -> addValue($5);
										  ((FrameValueExpression *)$1) -> setFrameProperty(frameProperty);
										  $$ = $1;
										  keywordInfo.include = keywordInfo.exclude = 0;
										}
	|      ieeeheadercontent P_SUPPORTED_RATE ASSIGN exp DD exp 		{	 
										  FrameProperty * frameProperty = new FrameProperty(PT_IEEE_SUPPORTED_RATE);
										  frameProperty -> addValue($4);
										  frameProperty -> addValue($6);
										  ((FrameValueExpression *)$1) -> setFrameProperty(frameProperty);
										  $$ = $1;
										}
	|      ieeeheadercontent P_EXTENDED_SUPPORTED_RATE ASSIGN exp DD exp	{	 
										  FrameProperty * frameProperty = new FrameProperty(PT_IEEE_EXTENDED_SUPPORTED_RATE);
										  frameProperty -> addValue($4);
										  frameProperty -> addValue($6);
										  ((FrameValueExpression *)$1) -> setFrameProperty(frameProperty);
										  $$ = $1;
										}
	|      ieeeheadercontent P_REASON_CODE ASSIGN exp		 	{	 
										  FrameProperty * frameProperty = new FrameProperty(PT_IEEE_REASON_CODE);
										  frameProperty -> addValue($4);
										  ((FrameValueExpression *)$1) -> setFrameProperty(frameProperty);
										  $$ = $1;
										}
	|      ieeeheadercontent P_LISTEN_INTERVAL ASSIGN exp	 		{	 
										  FrameProperty * frameProperty = new FrameProperty(PT_IEEE_LISTEN_INTERVAL);
										  frameProperty -> addValue($4);
										  ((FrameValueExpression *)$1) -> setFrameProperty(frameProperty);
										  $$ = $1;
										}
	|      ieeeheadercontent P_CURRENT_AP_ADDRESS ASSIGN QUOAT MAC_ADDRESS QUOAT {	 
										  FrameProperty * frameProperty = new FrameProperty(PT_IEEE_CURRENT_AP_ADDRESS);
										  frameProperty -> addValue(new StringValueExpression($5)); free($5);
										  ((FrameValueExpression *)$1) -> setFrameProperty(frameProperty);
										  $$ = $1;
										}
	|      ieeeheadercontent P_CURRENT_AP_ADDRESS ASSIGN exp		 {	 
										  FrameProperty * frameProperty = new FrameProperty(PT_IEEE_CURRENT_AP_ADDRESS);
										  frameProperty -> addValue($4);
										  ((FrameValueExpression *)$1) -> setFrameProperty(frameProperty);
										  $$ = $1;
										}
	|      ieeeheadercontent P_RSN_VERSION ASSIGN exp 			{	 
										  FrameProperty * frameProperty = new FrameProperty(PT_IEEE_RSN_VERSION);
										  frameProperty -> addValue($4);
										  ((FrameValueExpression *)$1) -> setFrameProperty(frameProperty);
										  $$ = $1;
										}
	|      ieeeheadercontent P_GROUP_CIPHER_SUITE ASSIGN  QUOAT OUI QUOAT DD numberkeyword {	 
										  FrameProperty * frameProperty = new FrameProperty(PT_IEEE_GROUP_CIPHER_SUITE);
										  frameProperty -> addValue(new StringValueExpression($5));  free($5);
										  frameProperty -> addValue($8);
										  ((FrameValueExpression *)$1) -> setFrameProperty(frameProperty);
										  $$ = $1;
										}
	|      ieeeheadercontent P_PAIRWISE_CIPHER_SUITE ASSIGN  exp DD QUOAT OUI QUOAT DD numberkeyword {	 
										  FrameProperty * frameProperty = new FrameProperty(PT_IEEE_PAIRWISE_CIPHER_SUITE);
										  frameProperty -> addValue($4);
										  frameProperty -> addValue(new StringValueExpression($7)); free($7);
										  frameProperty -> addValue($10);
										  ((FrameValueExpression *)$1) -> setFrameProperty(frameProperty);
										  $$ = $1;
										}
	|      ieeeheadercontent P_AUTHENTICATION_SUITE ASSIGN  exp DD QUOAT OUI QUOAT DD numberkeyword {	 
										  FrameProperty * frameProperty = new FrameProperty(PT_IEEE_AUTHENTICATION_SUITE);
										  frameProperty -> addValue($4);
										  frameProperty -> addValue(new StringValueExpression($7)); free($7);
										  frameProperty -> addValue($10);
										  ((FrameValueExpression *)$1) -> setFrameProperty(frameProperty);
										  $$ = $1;
										}
	|      ieeeheadercontent P_RSN_CAPABILITIES ASSIGN exp 			{	 
										  FrameProperty * frameProperty = new FrameProperty(PT_IEEE_RSN_CAPABILITIES);
										  frameProperty -> addValue($4);
										  ((FrameValueExpression *)$1) -> setFrameProperty(frameProperty);
										  $$ = $1;
										}
	|      ieeeheadercontent P_RSN_CAPABILITIES ASSIGN QUOAT keyword QUOAT	{	 
										  FrameProperty * frameProperty = new FrameProperty(PT_IEEE_RSN_CAPABILITIES);
										  frameProperty -> addValue($5);
										  ((FrameValueExpression *)$1) -> setFrameProperty(frameProperty);
										  $$ = $1; 
										  keywordInfo.include = keywordInfo.exclude = 0;
										}
	|      ieeeheadercontent P_STATUS_CODE ASSIGN exp 			{	 
										  FrameProperty * frameProperty = new FrameProperty(PT_IEEE_STATUS_CODE);
										  frameProperty -> addValue($4);
										  ((FrameValueExpression *)$1) -> setFrameProperty(frameProperty);
										  $$ = $1;
										}
	|      ieeeheadercontent P_ASSOCIATION_ID ASSIGN exp			{		 
										  FrameProperty * frameProperty = new FrameProperty(PT_IEEE_ASSOCIATION_ID);
										  frameProperty -> addValue($4);
										  ((FrameValueExpression *)$1) -> setFrameProperty(frameProperty);
										  $$ = $1;
										}
	|      ieeeheadercontent P_AUTHENTICATION_ALGORITHM_NUMBER ASSIGN exp 	{	 
										  FrameProperty * frameProperty = 
												new FrameProperty(PT_IEEE_AUTHENTICATION_ALGORITHM_NUMBER);
										  frameProperty -> addValue($4);
										  ((FrameValueExpression *)$1) -> setFrameProperty(frameProperty);
										  $$ = $1;
										}
	|      ieeeheadercontent P_AUTHENTICATION_SEQUENCE_NUMBER ASSIGN exp 	{	 
										  FrameProperty * frameProperty = 
												new FrameProperty(PT_IEEE_AUTHENTICATION_SEQUENCE_NUMBER);
										  frameProperty -> addValue($4);
										  ((FrameValueExpression *)$1) -> setFrameProperty(frameProperty);
										  $$ = $1;
										}
	|      ieeeheadercontent P_CHALLENGE_TEXT ASSIGN HEX_NUMBER		{	 
										  FrameProperty * frameProperty = new FrameProperty(PT_IEEE_CHALLENGE_TEXT);
										  frameProperty -> addValue(new StringValueExpression($4)); free($4);
										  ((FrameValueExpression *)$1) -> setFrameProperty(frameProperty);
										  $$ = $1;
										}
	|      ieeeheadercontent P_QOS ASSIGN exp				{	 
										  FrameProperty * frameProperty = new FrameProperty(PT_IEEE_QOS);
										  frameProperty -> addValue($4);
										  ((FrameValueExpression *)$1) -> setFrameProperty(frameProperty);
										  $$ = $1;
										}
	|      ieeeheadercontent P_ADDRESS1 ASSIGN QUOAT MAC_ADDRESS QUOAT 	{ 
										  FrameProperty * frameProperty = new FrameProperty(PT_IEEE_ADDRESS1);
										  frameProperty -> addValue(new StringValueExpression($5)); free($5);
										  ((FrameValueExpression *)$1) -> setFrameProperty(frameProperty);
										  $$ = $1;
										}
	|      ieeeheadercontent P_ADDRESS1 ASSIGN exp			 	{ 
										  FrameProperty * frameProperty = new FrameProperty(PT_IEEE_ADDRESS1);
										  frameProperty -> addValue($4);
										  ((FrameValueExpression *)$1) -> setFrameProperty(frameProperty);
										  $$ = $1;
										}
	|      ieeeheadercontent P_ADDRESS2 ASSIGN QUOAT MAC_ADDRESS QUOAT 	{ 
										  FrameProperty * frameProperty = new FrameProperty(PT_IEEE_ADDRESS2);
										  frameProperty -> addValue(new StringValueExpression($5)); free($5);
										  ((FrameValueExpression *)$1) -> setFrameProperty(frameProperty);
										  $$ = $1;
										}
	|      ieeeheadercontent P_ADDRESS2 ASSIGN exp			 	{ 
										  FrameProperty * frameProperty = new FrameProperty(PT_IEEE_ADDRESS2);
										  frameProperty -> addValue($4);
										  ((FrameValueExpression *)$1) -> setFrameProperty(frameProperty);
										  $$ = $1;
										}				
	|      ieeeheadercontent P_ADDRESS3 ASSIGN QUOAT MAC_ADDRESS QUOAT 	{ 
										  FrameProperty * frameProperty = new FrameProperty(PT_IEEE_ADDRESS3);
										  frameProperty -> addValue(new StringValueExpression($5)); free($5);
										  ((FrameValueExpression *)$1) -> setFrameProperty(frameProperty);
										  $$ = $1;
										}
	|      ieeeheadercontent P_ADDRESS3 ASSIGN exp			 	{ 
										  FrameProperty * frameProperty = new FrameProperty(PT_IEEE_ADDRESS3);
										  frameProperty -> addValue($4);
										  ((FrameValueExpression *)$1) -> setFrameProperty(frameProperty);
										  $$ = $1;
										}
	|      ieeeheadercontent P_ADDRESS4 ASSIGN QUOAT MAC_ADDRESS QUOAT 	{ 
										  FrameProperty * frameProperty = new FrameProperty(PT_IEEE_ADDRESS4);
										  frameProperty -> addValue(new StringValueExpression($5)); free($5);
										  ((FrameValueExpression *)$1) -> setFrameProperty(frameProperty);
										  $$ = $1;
										}
	|      ieeeheadercontent P_ADDRESS4 ASSIGN exp			 	{ 
										  FrameProperty * frameProperty = new FrameProperty(PT_IEEE_ADDRESS4);
										  frameProperty -> addValue($4);
										  ((FrameValueExpression *)$1) -> setFrameProperty(frameProperty);
										  $$ = $1;
										}
	|									{
										  string str(CONTENT_IEEE_HEADER);
										  $$ = new FrameValueExpression(new VariableValueExpression((char *)str.c_str()));
										}
	;
// ***************************************************************************************************************************************************
// *********************************************************** LLC HEADER CONTENT ********************************************************************
// ***************************************************************************************************************************************************
llcheadercontent: llcheadercontent P_DNAP ASSIGN exp				{
										  FrameProperty * frameProperty = new FrameProperty(PT_LLC_DNAP);
										  frameProperty -> addValue($4);
										  ((FrameValueExpression *)$1) -> setFrameProperty(frameProperty);
										  $$ = $1;
										}
	|      llcheadercontent P_SNAP ASSIGN exp				{
										  FrameProperty * frameProperty = new FrameProperty(PT_LLC_SNAP);
										  frameProperty -> addValue($4);
										  ((FrameValueExpression *)$1) -> setFrameProperty(frameProperty);
										  $$ = $1;
										}
	|      llcheadercontent P_CONTROL_FIELD ASSIGN exp			{
										  FrameProperty * frameProperty = new FrameProperty(PT_LLC_CONTROL_FIELD);
										  frameProperty -> addValue($4);
										  ((FrameValueExpression *)$1) -> setFrameProperty(frameProperty);
										  $$ = $1;
										}
	|      llcheadercontent P_ORGANIZATION_CODE ASSIGN exp			{
										  FrameProperty * frameProperty = new FrameProperty(PT_LLC_ORGANIZATION_CODE);
										  frameProperty -> addValue($4);
										  ((FrameValueExpression *)$1) -> setFrameProperty(frameProperty);
										  $$ = $1;
										}
	|      llcheadercontent P_LLC_TYPE ASSIGN numberkeyword			{
										  FrameProperty * frameProperty = new FrameProperty(PT_LLC_TYPE);
										  frameProperty -> addValue($4);
										  ((FrameValueExpression *)$1) -> setFrameProperty(frameProperty);
										  $$ = $1;
										}
	|									{
										  string str(CONTENT_LLC_HEADER);
										  $$ = new FrameValueExpression(new VariableValueExpression((char *)str.c_str()));
										}
	;
// ***************************************************************************************************************************************************
// ********************************************************** SET a DEL PROPERTY RADIOTAP ************************************************************
// ***************************************************************************************************************************************************
framebuild:	identifier DOT P_VERSION ASSIGN exp				{ 
										  FrameProperty * frameProperty = new FrameProperty(PT_RADIOTAP_VERSION);
										  frameProperty -> addValue($5);
										  $$ = new FrameValueExpression($1, frameProperty);
										}
	|	DEL LPARENTHESIS identifier DOT P_VERSION RPARENTHESIS		{
										  FrameProperty * frameProperty = new FrameProperty(PT_RADIOTAP_VERSION);
										  $$ = new FrameValueExpression($3, frameProperty, FVO_DEL);
										}
	|	identifier DOT P_PAD ASSIGN exp					{
										  FrameProperty * frameProperty = new FrameProperty(PT_RADIOTAP_PAD);
										  frameProperty -> addValue($5);
										  $$ = new FrameValueExpression($1, frameProperty);
										}
	|	DEL LPARENTHESIS identifier DOT P_PAD RPARENTHESIS	        {
										  FrameProperty * frameProperty = new FrameProperty(PT_RADIOTAP_PAD);
										  $$ = new FrameValueExpression($3, frameProperty, FVO_DEL);
										}
	|	identifier DOT P_ANTENNA ASSIGN exp				{
										  FrameProperty * frameProperty = new FrameProperty(PT_RADIOTAP_ANTENNA);
										  frameProperty -> addValue($5);
										  $$ = new FrameValueExpression($1, frameProperty);
										}
	|	DEL LPARENTHESIS identifier DOT P_ANTENNA RPARENTHESIS	        {
										  FrameProperty * frameProperty = new FrameProperty(PT_RADIOTAP_ANTENNA);
										  $$ = new FrameValueExpression($3, frameProperty, FVO_DEL);
										}
	|	identifier DOT P_ANTENNA_NOISE ASSIGN exp			{
										  FrameProperty * frameProperty = new FrameProperty(PT_RADIOTAP_ANTENNA_NOISE);
										  frameProperty -> addValue($5);
										  $$ = new FrameValueExpression($1, frameProperty);
										}
	|	DEL LPARENTHESIS identifier DOT P_ANTENNA_NOISE RPARENTHESIS	{
										  FrameProperty * frameProperty = new FrameProperty(PT_RADIOTAP_ANTENNA_NOISE);
										  $$ = new FrameValueExpression($3, frameProperty, FVO_DEL);
										}
	|	identifier DOT P_ANTENNA_SIGNAL ASSIGN exp			{
										  FrameProperty * frameProperty = new FrameProperty(PT_RADIOTAP_ANTENNA_SIGNAL);
										  frameProperty -> addValue($5);
										  $$ = new FrameValueExpression($1, frameProperty);
										}
	|	DEL LPARENTHESIS identifier DOT P_ANTENNA_SIGNAL RPARENTHESIS	{
										  FrameProperty * frameProperty = new FrameProperty(PT_RADIOTAP_ANTENNA_SIGNAL);
										  $$ = new FrameValueExpression($3, frameProperty, FVO_DEL);
										}
	|	identifier DOT P_CHANNEL_FREQUENCY ASSIGN exp			{
										  FrameProperty * frameProperty = new FrameProperty(PT_RADIOTAP_CHANNEL_FREQUENCY);
										  frameProperty -> addValue($5);
										  $$ = new FrameValueExpression($1, frameProperty);
										}
	|	DEL LPARENTHESIS identifier DOT P_CHANNEL_FREQUENCY RPARENTHESIS {
										  FrameProperty * frameProperty = new FrameProperty(PT_RADIOTAP_CHANNEL_FREQUENCY);
										  $$ = new FrameValueExpression($3, frameProperty, FVO_DEL);
										}
	|	identifier DOT P_CHANNEL_FLAGS ASSIGN exp			{
										  FrameProperty * frameProperty = new FrameProperty(PT_RADIOTAP_CHANNEL_FLAGS);
										  frameProperty -> addValue($5);
										  $$ = new FrameValueExpression($1, frameProperty);
										}
	|	identifier DOT P_CHANNEL_FLAGS ASSIGN QUOAT keyword QUOAT	{
										  FrameProperty * frameProperty = new FrameProperty(PT_RADIOTAP_CHANNEL_FLAGS);
										  frameProperty -> addValue($6);
										  keywordInfo.include = keywordInfo.exclude = 0;
										  $$ = new FrameValueExpression($1, frameProperty);
										}
	|	DEL LPARENTHESIS identifier DOT P_CHANNEL_FLAGS RPARENTHESIS  	{
										  FrameProperty * frameProperty = new FrameProperty(PT_RADIOTAP_CHANNEL_FLAGS);
										  $$ = new FrameValueExpression($3, frameProperty, FVO_DEL);
										}
	|	DEL LPARENTHESIS identifier DOT P_CHANNEL RPARENTHESIS 		{
										  FrameProperty * frameProperty = new FrameProperty(PT_RADIOTAP_CHANNEL);
										  $$ = new FrameValueExpression($3, frameProperty, FVO_DEL);
										}
	|	identifier DOT P_FHSS_HOP_SET ASSIGN exp			{
										  FrameProperty * frameProperty = new FrameProperty(PT_RADIOTAP_FHSS_HOP_SET);
										  frameProperty -> addValue($5);
										  $$ = new FrameValueExpression($1, frameProperty);
										}
	|	DEL LPARENTHESIS identifier DOT P_FHSS_HOP_SET RPARENTHESIS 	{
										  FrameProperty * frameProperty = new FrameProperty(PT_RADIOTAP_FHSS_HOP_SET);
										  $$ = new FrameValueExpression($3, frameProperty, FVO_DEL);
										}
	|	identifier DOT P_FHSS_HOP_PATTERN ASSIGN exp			{
										  FrameProperty * frameProperty = new FrameProperty(PT_RADIOTAP_FHSS_HOP_PATTERN);
										  frameProperty -> addValue($5);
										  $$ = new FrameValueExpression($1, frameProperty);
										}
	|	DEL LPARENTHESIS identifier DOT P_FHSS_HOP_PATTERN RPARENTHESIS {
										  FrameProperty * frameProperty = new FrameProperty(PT_RADIOTAP_FHSS_HOP_PATTERN);
										  $$ = new FrameValueExpression($3, frameProperty, FVO_DEL);
										}
	|	DEL LPARENTHESIS identifier DOT P_FHSS_HOP RPARENTHESIS 	{
										  FrameProperty * frameProperty = new FrameProperty(PT_RADIOTAP_FHSS_HOP);
										  $$ = new FrameValueExpression($3, frameProperty, FVO_DEL);
										}
	|	identifier DOT P_FLAGS ASSIGN exp				{
										  FrameProperty * frameProperty = new FrameProperty(PT_RADIOTAP_FLAGS);
										  frameProperty -> addValue($5);
										  $$ = new FrameValueExpression($1, frameProperty);
										}
	|	identifier DOT P_FLAGS ASSIGN QUOAT keyword QUOAT		{
										  FrameProperty * frameProperty = new FrameProperty(PT_RADIOTAP_FLAGS);
										  frameProperty -> addValue($6);
									          keywordInfo.include = keywordInfo.exclude = 0;	
										  $$ = new FrameValueExpression($1, frameProperty);
										}
	|	DEL LPARENTHESIS identifier DOT P_FLAGS RPARENTHESIS	        {
										  FrameProperty * frameProperty = new FrameProperty(PT_RADIOTAP_FLAGS);
										  $$ = new FrameValueExpression($3, frameProperty, FVO_DEL);
										}
	|	identifier DOT P_LOCK_QUALITY ASSIGN exp 			{
										  FrameProperty * frameProperty = new FrameProperty(PT_RADIOTAP_LOCK_QUALITY);
										  frameProperty -> addValue($5);
										  $$ = new FrameValueExpression($1, frameProperty);	
										}
	|	DEL LPARENTHESIS identifier DOT P_LOCK_QUALITY RPARENTHESIS 	{
										  FrameProperty * frameProperty = new FrameProperty(PT_RADIOTAP_LOCK_QUALITY);
										  $$ = new FrameValueExpression($3, frameProperty, FVO_DEL);
										}
	|	identifier DOT P_MCS_KNOWN ASSIGN exp	 			{
										  FrameProperty * frameProperty = new FrameProperty(PT_RADIOTAP_MCS_KNOWN);
										  frameProperty -> addValue($5);
										  $$ = new FrameValueExpression($1, frameProperty);	
										}
	|	DEL LPARENTHESIS identifier DOT P_MCS_KNOWN RPARENTHESIS 	{
										  FrameProperty * frameProperty = new FrameProperty(PT_RADIOTAP_MCS_KNOWN);
										  $$ = new FrameValueExpression($3, frameProperty, FVO_DEL);
										}
	|	identifier DOT P_MCS_FLAGS ASSIGN exp				{
										  FrameProperty * frameProperty = new FrameProperty(PT_RADIOTAP_MCS_FLAGS);
										  frameProperty -> addValue($5);
										  $$ = new FrameValueExpression($1, frameProperty);	
										}
	|	identifier DOT P_MCS_FLAGS ASSIGN QUOAT keyword QUOAT		{
										  FrameProperty * frameProperty = new FrameProperty(PT_RADIOTAP_MCS_FLAGS);
										  frameProperty -> addValue($6);
										  keywordInfo.include = keywordInfo.exclude = 0;
										  $$ = new FrameValueExpression($1, frameProperty);
										}
	|	DEL LPARENTHESIS identifier DOT P_MCS_FLAGS RPARENTHESIS 	{
										  FrameProperty * frameProperty = new FrameProperty(PT_RADIOTAP_MCS_FLAGS);
										  $$ = new FrameValueExpression($3, frameProperty, FVO_DEL);
										}
	|	identifier DOT P_MCS_MCS ASSIGN exp				{
										  FrameProperty * frameProperty = new FrameProperty(PT_RADIOTAP_MCS_MCS);
										  frameProperty -> addValue($5);
										  $$ = new FrameValueExpression($1, frameProperty);	
										}
	|	DEL LPARENTHESIS identifier DOT P_MCS_MCS RPARENTHESIS 		{
										  FrameProperty * frameProperty = new FrameProperty(PT_RADIOTAP_MCS_MCS);
										  $$ = new FrameValueExpression($3, frameProperty, FVO_DEL);
										}
	|	DEL LPARENTHESIS identifier DOT P_MCS RPARENTHESIS 		{
										  FrameProperty * frameProperty = new FrameProperty(PT_RADIOTAP_MCS);
										  $$ = new FrameValueExpression($3, frameProperty, FVO_DEL);
										}
	|	identifier DOT P_RX_FLAGS ASSIGN exp				{
										  FrameProperty * frameProperty = new FrameProperty(PT_RADIOTAP_RX_FLAGS);
										  frameProperty -> addValue($5);
										  $$ = new FrameValueExpression($1, frameProperty);	
										}
	|	identifier DOT P_RX_FLAGS ASSIGN QUOAT keyword QUOAT		{
										  FrameProperty * frameProperty = new FrameProperty(PT_RADIOTAP_RX_FLAGS);
										  frameProperty -> addValue($6);
										  keywordInfo.include = keywordInfo.exclude = 0;
										  $$ = new FrameValueExpression($1, frameProperty);	
										}
	|	DEL LPARENTHESIS identifier DOT P_RX_FLAGS RPARENTHESIS 	{
										  FrameProperty * frameProperty = new FrameProperty(PT_RADIOTAP_RX_FLAGS);
										  $$ = new FrameValueExpression($3, frameProperty, FVO_DEL);
										}
	|	identifier DOT P_RATE ASSIGN exp				{
										  FrameProperty * frameProperty = new FrameProperty(PT_RADIOTAP_RATE);
										  frameProperty -> addValue($5);
										  $$ = new FrameValueExpression($1, frameProperty);
										}
	|	DEL LPARENTHESIS identifier DOT P_RATE RPARENTHESIS 		{
										  FrameProperty * frameProperty = new FrameProperty(PT_RADIOTAP_RATE);
										  $$ = new FrameValueExpression($3, frameProperty, FVO_DEL);
										}
	|	identifier DOT P_TSFT ASSIGN exp				{
										  FrameProperty * frameProperty = new FrameProperty(PT_RADIOTAP_TSFT);
										  frameProperty -> addValue($5);
										  $$ = new FrameValueExpression($1, frameProperty);	
										}
	|	DEL LPARENTHESIS identifier DOT P_TSFT RPARENTHESIS 		{
										  FrameProperty * frameProperty = new FrameProperty(PT_RADIOTAP_TSFT);
										  $$ = new FrameValueExpression($3, frameProperty, FVO_DEL);
										}
	|	identifier DOT P_TX_ATTENUATION ASSIGN exp			{
										  FrameProperty * frameProperty = new FrameProperty(PT_RADIOTAP_TX_ATTENUATION);
										  frameProperty -> addValue($5);
										  $$ = new FrameValueExpression($1, frameProperty);	
										}
	|	DEL LPARENTHESIS identifier DOT P_TX_ATTENUATION RPARENTHESIS 	{
										  FrameProperty * frameProperty = new FrameProperty(PT_RADIOTAP_TX_ATTENUATION);
										  $$ = new FrameValueExpression($3, frameProperty, FVO_DEL);
										}
	|	identifier DOT P_VENDOR_NAMESPACE_OUI ASSIGN QUOAT OUI QUOAT	{
									 	  FrameProperty * frameProperty = new FrameProperty(PT_RADIOTAP_VENDOR_NAMESPACE_OUI);
										  frameProperty -> addValue(new StringValueExpression($6)); free($6);
										  $$ = new FrameValueExpression($1, frameProperty);
										}
	|	DEL LPARENTHESIS identifier DOT P_VENDOR_NAMESPACE_OUI RPARENTHESIS {
										  FrameProperty * frameProperty = new FrameProperty(PT_RADIOTAP_VENDOR_NAMESPACE_OUI);
										  $$ = new FrameValueExpression($3, frameProperty, FVO_DEL);
										}
	|	identifier DOT P_VENDOR_NAMESPACE_SUB_NAMESPACE ASSIGN exp	{
										  FrameProperty * frameProperty = 
												new FrameProperty(PT_RADIOTAP_VENDOR_NAMESPACE_SUB_NAMESPACE);
										  frameProperty -> addValue($5);
										  $$ = new FrameValueExpression($1, frameProperty);
										}
	|	DEL LPARENTHESIS identifier DOT P_VENDOR_NAMESPACE_SUB_NAMESPACE RPARENTHESIS {
										  FrameProperty * frameProperty = 
												new FrameProperty(PT_RADIOTAP_VENDOR_NAMESPACE_SUB_NAMESPACE);
										  $$ = new FrameValueExpression($3, frameProperty, FVO_DEL);
										}
	|	identifier DOT P_VENDOR_NAMESPACE_SKIP_LENGTH ASSIGN exp	{
										  FrameProperty * frameProperty = 
												new FrameProperty(PT_RADIOTAP_VENDOR_NAMESPACE_SKIP_LENGTH);
										  frameProperty -> addValue($5);
										  $$ = new FrameValueExpression($1, frameProperty);
										}
	|	DEL LPARENTHESIS identifier DOT P_VENDOR_NAMESPACE_SKIP_LENGTH RPARENTHESIS {
										  FrameProperty * frameProperty = 
												new FrameProperty(PT_RADIOTAP_VENDOR_NAMESPACE_SKIP_LENGTH);
										  $$ = new FrameValueExpression($3, frameProperty, FVO_DEL);
										}
	|	DEL LPARENTHESIS identifier DOT P_VENDOR_NAMESPACE RPARENTHESIS {
										  FrameProperty * frameProperty = new FrameProperty(PT_RADIOTAP_VENDOR_NAMESPACE);
										  $$ = new FrameValueExpression($3, frameProperty, FVO_DEL);
										}
	|	identifier DOT P_DB_TX_ATTENUATION ASSIGN exp			{
										  FrameProperty * frameProperty = new FrameProperty(PT_RADIOTAP_DB_TX_ATTENUATION);
										  frameProperty -> addValue($5);
										  $$ = new FrameValueExpression($1, frameProperty);	
										}
	|	DEL LPARENTHESIS identifier DOT P_DB_TX_ATTENUATION RPARENTHESIS {
										  FrameProperty * frameProperty = new FrameProperty(PT_RADIOTAP_DB_TX_ATTENUATION);
										  $$ = new FrameValueExpression($3, frameProperty, FVO_DEL);
										}
	|	identifier DOT P_DB_ANTENNA_NOISE ASSIGN exp			{
										  FrameProperty * frameProperty = new FrameProperty(PT_RADIOTAP_DB_ANTENNA_NOISE);
										  frameProperty -> addValue($5);
										  $$ = new FrameValueExpression($1, frameProperty);
										}
	|	DEL LPARENTHESIS identifier DOT P_DB_ANTENNA_NOISE RPARENTHESIS {
										  FrameProperty * frameProperty = new FrameProperty(PT_RADIOTAP_DB_ANTENNA_NOISE);
										  $$ = new FrameValueExpression($3, frameProperty, FVO_DEL);
										}
	|	identifier DOT P_DB_ANTENNA_SIGNAL ASSIGN exp			{
										  FrameProperty * frameProperty = new FrameProperty(PT_RADIOTAP_DB_ANTENNA_SIGNAL);
										  frameProperty -> addValue($5);
										  $$ = new FrameValueExpression($1, frameProperty);	
										}
	|	DEL LPARENTHESIS identifier DOT P_DB_ANTENNA_SIGNAL RPARENTHESIS {
										  FrameProperty * frameProperty = new FrameProperty(PT_RADIOTAP_DB_ANTENNA_SIGNAL);
										  $$ = new FrameValueExpression($3, frameProperty, FVO_DEL);
										}
	|	identifier DOT P_DBM_TX_POWER ASSIGN exp			{
										  FrameProperty * frameProperty = new FrameProperty(PT_RADIOTAP_DBM_TX_POWER);
										  frameProperty -> addValue($5);
										  $$ = new FrameValueExpression($1, frameProperty);
										}
	|	DEL LPARENTHESIS identifier DOT P_DBM_TX_POWER RPARENTHESIS 	{
										  FrameProperty * frameProperty = new FrameProperty(PT_RADIOTAP_DBM_TX_POWER);
										  $$ = new FrameValueExpression($3, frameProperty, FVO_DEL);
										}
	|	identifier DOT P_RTS_RETRIES ASSIGN exp				{
										  FrameProperty * frameProperty = new FrameProperty(PT_RADIOTAP_RTS_RETRIES);
										  frameProperty -> addValue($5);
										  $$ = new FrameValueExpression($1, frameProperty);
										}
	|	DEL LPARENTHESIS identifier DOT P_RTS_RETRIES RPARENTHESIS 	{
										  FrameProperty * frameProperty = new FrameProperty(PT_RADIOTAP_RTS_RETRIES);
										  $$ = new FrameValueExpression($3, frameProperty, FVO_DEL);
										}
	|	identifier DOT P_DATA_RETRIES ASSIGN exp			{
										  FrameProperty * frameProperty = new FrameProperty(PT_RADIOTAP_DATA_RETRIES);
										  frameProperty -> addValue($5);
										  $$ = new FrameValueExpression($1, frameProperty);
										}
	|	DEL LPARENTHESIS identifier DOT P_DATA_RETRIES RPARENTHESIS 	{
										  FrameProperty * frameProperty = new FrameProperty(PT_RADIOTAP_DATA_RETRIES);
										  $$ = new FrameValueExpression($3, frameProperty, FVO_DEL);
										}
// ***************************************************************************************************************************************************
// ********************************************************* SET a DEL PROPERTY IEEE *****************************************************************
// ***************************************************************************************************************************************************
	|	identifier DOT P_TYPE ASSIGN QUOAT K_DATA QUOAT			{
										  FrameProperty * frameProperty = new FrameProperty(PT_IEEE_TYPE_DATA);
										  $$ = new FrameValueExpression($1, frameProperty);	
										}
	|	identifier DOT P_TYPE ASSIGN QUOAT K_QOS_DATA QUOAT		{
										  FrameProperty * frameProperty = new FrameProperty(PT_IEEE_TYPE_QOS_DATA);
										  $$ = new FrameValueExpression($1, frameProperty);	
										}
	|	identifier DOT P_TYPE ASSIGN QUOAT K_RTS QUOAT			{
										  FrameProperty * frameProperty = new FrameProperty(PT_IEEE_TYPE_RTS);
										  $$ = new FrameValueExpression($1, frameProperty);
										}
	|	identifier DOT P_TYPE ASSIGN QUOAT K_CTS QUOAT			{
										  FrameProperty * frameProperty = new FrameProperty(PT_IEEE_TYPE_CTS);
										  $$ = new FrameValueExpression($1, frameProperty);
										}
	|	identifier DOT P_TYPE ASSIGN QUOAT K_ACK QUOAT			{
										  FrameProperty * frameProperty = new FrameProperty(PT_IEEE_TYPE_ACK);
										  $$ = new FrameValueExpression($1, frameProperty);
										}
	|	identifier DOT P_TYPE ASSIGN QUOAT K_PS_POLL QUOAT		{
										  FrameProperty * frameProperty = new FrameProperty(PT_IEEE_TYPE_PS_POLL);
										  $$ = new FrameValueExpression($1, frameProperty);
										}
	|	identifier DOT P_TYPE ASSIGN QUOAT K_BEACON QUOAT		{
										  FrameProperty * frameProperty = new FrameProperty(PT_IEEE_TYPE_BEACON);
										  $$ = new FrameValueExpression($1, frameProperty);
										}
	|	identifier DOT P_TYPE ASSIGN QUOAT K_PROBE_REQUEST QUOAT	{
										  FrameProperty * frameProperty = new FrameProperty(PT_IEEE_TYPE_PROBE_REQUEST);
										  $$ = new FrameValueExpression($1, frameProperty);
										}
	|	identifier DOT P_TYPE ASSIGN QUOAT K_PROBE_RESPONSE QUOAT	{
										  FrameProperty * frameProperty = new FrameProperty(PT_IEEE_TYPE_PROBE_RESPONSE);
										  $$ = new FrameValueExpression($1, frameProperty);
										}
	|	identifier DOT P_TYPE ASSIGN QUOAT K_ATIM QUOAT			{
										  FrameProperty * frameProperty = new FrameProperty(PT_IEEE_TYPE_ATIM);
										  $$ = new FrameValueExpression($1, frameProperty);
										}
	|      identifier DOT P_TYPE ASSIGN QUOAT K_DEAUTHETICATION QUOAT	{
										  FrameProperty * frameProperty = new FrameProperty(PT_IEEE_TYPE_DEAUTHETICATION);
										  $$ = new FrameValueExpression($1, frameProperty); 
										}
	|      identifier DOT P_TYPE ASSIGN QUOAT K_DISASSOCIATION QUOAT	{
										  FrameProperty * frameProperty = new FrameProperty(PT_IEEE_TYPE_DISASSOCIATION);
										  $$ = new FrameValueExpression($1, frameProperty);
										}
	|      identifier DOT P_TYPE ASSIGN QUOAT K_ASSOCIATION_REQUEST QUOAT   {
										  FrameProperty * frameProperty = new FrameProperty(PT_IEEE_TYPE_ASSOCIATION_REQUEST);
										  $$ = new FrameValueExpression($1, frameProperty);
										}
	|      identifier DOT P_TYPE ASSIGN QUOAT K_REASSOCIATION_REQUEST QUOAT	{
										  FrameProperty * frameProperty = 
												new FrameProperty(PT_IEEE_TYPE_REASSOCIATION_REQUEST);
										  $$ = new FrameValueExpression($1, frameProperty);
										}
	|      identifier DOT P_TYPE ASSIGN QUOAT K_ASSOCIATION_RESPONSE QUOAT	{
										  FrameProperty * frameProperty = 
												new FrameProperty(PT_IEEE_TYPE_ASSOCIATION_RESPONSE);
										  $$ = new FrameValueExpression($1, frameProperty);
										}
	|      identifier DOT P_TYPE ASSIGN QUOAT K_AUTHENTICATION QUOAT	{
										  FrameProperty * frameProperty = new FrameProperty(PT_IEEE_TYPE_AUTHENTICATION);
										  $$ = new FrameValueExpression($1, frameProperty);
										}
	|	identifier DOT P_FRAME_CONTROL ASSIGN exp			{
									  	  FrameProperty * frameProperty = new FrameProperty(PT_IEEE_FRAME_CONTROL);
										  frameProperty -> addValue($5);
										  $$ = new FrameValueExpression($1, frameProperty);	
										}
	|	DEL LPARENTHESIS identifier DOT P_FRAME_CONTROL RPARENTHESIS 	{
										  FrameProperty * frameProperty = new FrameProperty(PT_IEEE_FRAME_CONTROL);
										  $$ = new FrameValueExpression($3, frameProperty, FVO_DEL);
										}
	|	identifier DOT P_PROTOCOL ASSIGN exp				{
									  	  FrameProperty * frameProperty = new FrameProperty(PT_IEEE_PROTOCOL);
										  frameProperty -> addValue($5);
										  $$ = new FrameValueExpression($1, frameProperty);	
										}
	|	DEL LPARENTHESIS identifier DOT P_PROTOCOL RPARENTHESIS 	{
										  FrameProperty * frameProperty = new FrameProperty(PT_IEEE_PROTOCOL);
										  $$ = new FrameValueExpression($3, frameProperty, FVO_DEL);
										}
	|	identifier DOT P_TYPE ASSIGN exp				{
									  	  FrameProperty * frameProperty = new FrameProperty(PT_IEEE_TYPE);
										  frameProperty -> addValue($5);
										  $$ = new FrameValueExpression($1, frameProperty);	
										}
	|	DEL LPARENTHESIS identifier DOT P_TYPE RPARENTHESIS 		{
										  FrameProperty * frameProperty = new FrameProperty(PT_IEEE_TYPE);
										  $$ = new FrameValueExpression($3, frameProperty, FVO_DEL);
										}
	|	identifier DOT P_SUB_TYPE ASSIGN exp				{
									  	  FrameProperty * frameProperty = new FrameProperty(PT_IEEE_SUB_TYPE);
										  frameProperty -> addValue($5);
										  $$ = new FrameValueExpression($1, frameProperty);
										}
	|	DEL LPARENTHESIS identifier DOT P_SUB_TYPE RPARENTHESIS 	{
										  FrameProperty * frameProperty = new FrameProperty(PT_IEEE_SUB_TYPE);
										  $$ = new FrameValueExpression($3, frameProperty, FVO_DEL);
										}
	|	identifier DOT P_TO_DS ASSIGN exp				{
									  	  FrameProperty * frameProperty = new FrameProperty(PT_IEEE_TO_DS);
										  frameProperty -> addValue($5);
										  $$ = new FrameValueExpression($1, frameProperty);	
										}
	|	DEL LPARENTHESIS identifier DOT P_TO_DS RPARENTHESIS 		{
										  FrameProperty * frameProperty = new FrameProperty(PT_IEEE_TO_DS);
										  $$ = new FrameValueExpression($3, frameProperty, FVO_DEL);
										}
	|	identifier DOT P_FROM_DS ASSIGN exp				{
									  	  FrameProperty * frameProperty = new FrameProperty(PT_IEEE_FROM_DS);
										  frameProperty -> addValue($5);
										  $$ = new FrameValueExpression($1, frameProperty);	
										}
	|	DEL LPARENTHESIS identifier DOT P_FROM_DS RPARENTHESIS 		{
										  FrameProperty * frameProperty = new FrameProperty(PT_IEEE_FROM_DS);
										  $$ = new FrameValueExpression($3, frameProperty, FVO_DEL);
										}
	|	identifier DOT P_MORE_FRAG ASSIGN exp				{
									  	  FrameProperty * frameProperty = new FrameProperty(PT_IEEE_MORE_FRAG);
										  frameProperty -> addValue($5);
										  $$ = new FrameValueExpression($1, frameProperty);
										}
	|	DEL LPARENTHESIS identifier DOT P_MORE_FRAG RPARENTHESIS 	{
										  FrameProperty * frameProperty = new FrameProperty(PT_IEEE_MORE_FRAG);
										  $$ = new FrameValueExpression($3, frameProperty, FVO_DEL);
										}
	|	identifier DOT P_RETRY ASSIGN exp				{
									  	  FrameProperty * frameProperty = new FrameProperty(PT_IEEE_RETRY);
										  frameProperty -> addValue($5);
										  $$ = new FrameValueExpression($1, frameProperty);	
										}
	|	DEL LPARENTHESIS identifier DOT P_RETRY RPARENTHESIS 		{
										  FrameProperty * frameProperty = new FrameProperty(PT_IEEE_RETRY);
										  $$ = new FrameValueExpression($3, frameProperty, FVO_DEL);
										}
	|	identifier DOT P_PWR_MGMT ASSIGN exp				{
									  	  FrameProperty * frameProperty = new FrameProperty(PT_IEEE_PWR_MGMT);
										  frameProperty -> addValue($5);
										  $$ = new FrameValueExpression($1, frameProperty);
										}
	|	DEL LPARENTHESIS identifier DOT P_PWR_MGMT RPARENTHESIS 	{
										  FrameProperty * frameProperty = new FrameProperty(PT_IEEE_PWR_MGMT);
										  $$ = new FrameValueExpression($3, frameProperty, FVO_DEL);
										}
	|	identifier DOT P_MORE_DATA ASSIGN exp				{
									  	  FrameProperty * frameProperty = new FrameProperty(PT_IEEE_MORE_DATA);
										  frameProperty -> addValue($5);
										  $$ = new FrameValueExpression($1, frameProperty);
										}
	|	DEL LPARENTHESIS identifier DOT P_MORE_DATA RPARENTHESIS 	{
										  FrameProperty * frameProperty = new FrameProperty(PT_IEEE_MORE_DATA);
										  $$ = new FrameValueExpression($3, frameProperty, FVO_DEL);
										}
	|	identifier DOT P_PROTECTED_FRAME ASSIGN exp			{
									  	  FrameProperty * frameProperty = new FrameProperty(PT_IEEE_PROTECTED_FRAME);
										  frameProperty -> addValue($5);
										  $$ = new FrameValueExpression($1, frameProperty);	
										}
	|	DEL LPARENTHESIS identifier DOT P_PROTECTED_FRAME RPARENTHESIS 	{
										  FrameProperty * frameProperty = new FrameProperty(PT_IEEE_PROTECTED_FRAME);
										  $$ = new FrameValueExpression($3, frameProperty, FVO_DEL);
										}
	|	identifier DOT P_ORDER ASSIGN exp				{
										  FrameProperty * frameProperty = new FrameProperty(PT_IEEE_ORDER);
										  frameProperty -> addValue($5);
										  $$ = new FrameValueExpression($1, frameProperty);	
										}
	|	DEL LPARENTHESIS identifier DOT P_ORDER RPARENTHESIS 		{
										  FrameProperty * frameProperty = new FrameProperty(PT_IEEE_ORDER);
										  $$ = new FrameValueExpression($3, frameProperty, FVO_DEL);
										}
	|	identifier DOT P_TRANSMITTER_ADDRESS ASSIGN QUOAT MAC_ADDRESS QUOAT {
										  FrameProperty * frameProperty = new FrameProperty(PT_IEEE_TRANSMITTER_ADDRESS);
										  frameProperty -> addValue(new StringValueExpression($6)); free($6);
										  $$ = new FrameValueExpression($1, frameProperty);		
										}
	|	identifier DOT P_TRANSMITTER_ADDRESS ASSIGN exp			{
										  FrameProperty * frameProperty = new FrameProperty(PT_IEEE_TRANSMITTER_ADDRESS);
										  frameProperty -> addValue($5);
										  $$ = new FrameValueExpression($1, frameProperty);		
										}
	|	DEL LPARENTHESIS identifier DOT P_TRANSMITTER_ADDRESS RPARENTHESIS {
										  FrameProperty * frameProperty = new FrameProperty(PT_IEEE_TRANSMITTER_ADDRESS);
										  $$ = new FrameValueExpression($3, frameProperty, FVO_DEL);
										}
	|	identifier DOT P_RECEIVER_ADDRESS ASSIGN QUOAT MAC_ADDRESS QUOAT {
										  FrameProperty * frameProperty = new FrameProperty(PT_IEEE_RECEIVER_ADDRESS);
										  frameProperty -> addValue(new StringValueExpression($6)); free($6);
										  $$ = new FrameValueExpression($1, frameProperty);	
										}
	|	identifier DOT P_RECEIVER_ADDRESS ASSIGN exp			{
										  FrameProperty * frameProperty = new FrameProperty(PT_IEEE_RECEIVER_ADDRESS);
										  frameProperty -> addValue($5);
										  $$ = new FrameValueExpression($1, frameProperty);	
										}
	|	DEL LPARENTHESIS identifier DOT P_RECEIVER_ADDRESS RPARENTHESIS {
										  FrameProperty * frameProperty = new FrameProperty(PT_IEEE_RECEIVER_ADDRESS);
										  $$ = new FrameValueExpression($3, frameProperty, FVO_DEL);
										}
	|	identifier DOT P_BSSID ASSIGN QUOAT  MAC_ADDRESS QUOAT		{
										  FrameProperty * frameProperty = new FrameProperty(PT_IEEE_BSSID);
										  frameProperty -> addValue(new StringValueExpression($6)); free($6);
										  $$ = new FrameValueExpression($1, frameProperty);	
										}
	|	identifier DOT P_BSSID ASSIGN exp				{
										  FrameProperty * frameProperty = new FrameProperty(PT_IEEE_BSSID);
										  frameProperty -> addValue($5);
										  $$ = new FrameValueExpression($1, frameProperty);	
										}
	|	DEL LPARENTHESIS identifier DOT P_BSSID RPARENTHESIS 		{
										  FrameProperty * frameProperty = new FrameProperty(PT_IEEE_BSSID);
										  $$ = new FrameValueExpression($3, frameProperty, FVO_DEL);
										}
	|	identifier DOT P_DURATION ASSIGN exp				{
										  FrameProperty * frameProperty = new FrameProperty(PT_IEEE_DURATION);
										  frameProperty -> addValue($5);
										  $$ = new FrameValueExpression($1, frameProperty);	
										}
	|	DEL LPARENTHESIS identifier DOT P_DURATION RPARENTHESIS 	{
										  FrameProperty * frameProperty = new FrameProperty(PT_IEEE_DURATION);
										  $$ = new FrameValueExpression($3, frameProperty, FVO_DEL);
										}
	|	identifier DOT P_AID ASSIGN exp					{
										  FrameProperty * frameProperty = new FrameProperty(PT_IEEE_AID);
										  frameProperty -> addValue($5);
										  $$ = new FrameValueExpression($1, frameProperty);	
										}
	|	DEL LPARENTHESIS identifier DOT P_AID RPARENTHESIS 		{
										  FrameProperty * frameProperty = new FrameProperty(PT_IEEE_AID);
										  $$ = new FrameValueExpression($3, frameProperty, FVO_DEL);
										}
	|	identifier DOT P_DA ASSIGN QUOAT MAC_ADDRESS QUOAT 		{
										  FrameProperty * frameProperty = new FrameProperty(PT_IEEE_DA);
										  frameProperty -> addValue(new StringValueExpression($6)); free($6);
										  $$ = new FrameValueExpression($1, frameProperty);	
										}
	|	identifier DOT P_DA ASSIGN exp					{
										  FrameProperty * frameProperty = new FrameProperty(PT_IEEE_DA);
										  frameProperty -> addValue($5);
										  $$ = new FrameValueExpression($1, frameProperty);	
										}
	|	DEL LPARENTHESIS identifier DOT P_DA RPARENTHESIS 		{
										  FrameProperty * frameProperty = new FrameProperty(PT_IEEE_DA);
										  $$ = new FrameValueExpression($3, frameProperty, FVO_DEL);
										}
	|	identifier DOT P_SA ASSIGN QUOAT MAC_ADDRESS QUOAT 		{
										  FrameProperty * frameProperty = new FrameProperty(PT_IEEE_SA);
										  frameProperty -> addValue(new StringValueExpression($6)); free($6);
										  $$ = new FrameValueExpression($1, frameProperty);	
										}
	|	identifier DOT P_SA ASSIGN exp			 		{
										  FrameProperty * frameProperty = new FrameProperty(PT_IEEE_SA);
										  frameProperty -> addValue($5);
										  $$ = new FrameValueExpression($1, frameProperty);	
										}
	|	DEL LPARENTHESIS identifier DOT P_SA RPARENTHESIS 		{
										  FrameProperty * frameProperty = new FrameProperty(PT_IEEE_SA);
										  $$ = new FrameValueExpression($3, frameProperty, FVO_DEL);
										}
	|	identifier DOT P_SEQ_CTL ASSIGN exp				{
										  FrameProperty * frameProperty = new FrameProperty(PT_IEEE_SEQ_CTL);
										  frameProperty -> addValue($5);
										  $$ = new FrameValueExpression($1, frameProperty);	
										}
	|	DEL LPARENTHESIS identifier DOT P_SEQ_CTL RPARENTHESIS 		{
										  FrameProperty * frameProperty = new FrameProperty(PT_IEEE_SEQ_CTL);
										  $$ = new FrameValueExpression($3, frameProperty, FVO_DEL);
										}
	|	identifier DOT P_FRAGMENT ASSIGN exp				{
										  FrameProperty * frameProperty = new FrameProperty(PT_IEEE_FRAGMENT);
										  frameProperty -> addValue($5);
										  $$ = new FrameValueExpression($1, frameProperty);	
										}
	|	DEL LPARENTHESIS identifier DOT P_FRAGMENT RPARENTHESIS 	{
										  FrameProperty * frameProperty = new FrameProperty(PT_IEEE_FRAGMENT);
										  $$ = new FrameValueExpression($3, frameProperty, FVO_DEL);
										}
	|	identifier DOT P_SEQUENCE ASSIGN exp				{
										  FrameProperty * frameProperty = new FrameProperty(PT_IEEE_SEQUENCE);
										  frameProperty -> addValue($5);
										  $$ = new FrameValueExpression($1, frameProperty);
										}
	|	DEL LPARENTHESIS identifier DOT P_SEQUENCE RPARENTHESIS 	{
									 	  FrameProperty * frameProperty = new FrameProperty(PT_IEEE_SEQUENCE);
										  $$ = new FrameValueExpression($3, frameProperty, FVO_DEL);
										}
	|	identifier DOT P_TIMESTAMP ASSIGN exp				{
										  FrameProperty * frameProperty = new FrameProperty(PT_IEEE_TIMESTAMP);
										  frameProperty -> addValue($5);
										  $$ = new FrameValueExpression($1, frameProperty);	
										}
	|	DEL LPARENTHESIS identifier DOT P_TIMESTAMP RPARENTHESIS 	{
									 	  FrameProperty * frameProperty = new FrameProperty(PT_IEEE_TIMESTAMP);
										  $$ = new FrameValueExpression($3, frameProperty, FVO_DEL);
										}
	|	identifier DOT P_BEACON_INTERVAL ASSIGN exp			{
										  FrameProperty * frameProperty = new FrameProperty(PT_IEEE_BEACON_INTERVAL);
										  frameProperty -> addValue($5);
										  $$ = new FrameValueExpression($1, frameProperty);	
										}
	|	DEL LPARENTHESIS identifier DOT P_BEACON_INTERVAL RPARENTHESIS 	{
									 	  FrameProperty * frameProperty = new FrameProperty(PT_IEEE_BEACON_INTERVAL);
										  $$ = new FrameValueExpression($3, frameProperty, FVO_DEL);
										}
	|	identifier DOT P_CAPABILITY_INFO ASSIGN exp			{
										  FrameProperty * frameProperty = new FrameProperty(PT_IEEE_CAPABILITY_INFO);
										  frameProperty -> addValue($5);
										  $$ = new FrameValueExpression($1, frameProperty);	
										}
	|	identifier DOT P_CAPABILITY_INFO ASSIGN QUOAT keyword QUOAT	{
										  FrameProperty * frameProperty = new FrameProperty(PT_IEEE_CAPABILITY_INFO);
										  frameProperty -> addValue($6);
										  keywordInfo.include = keywordInfo.exclude = 0;
										  $$ = new FrameValueExpression($1, frameProperty);
										}
	|	DEL LPARENTHESIS identifier DOT P_CAPABILITY_INFO RPARENTHESIS 	{
									 	  FrameProperty * frameProperty = new FrameProperty(PT_IEEE_CAPABILITY_INFO);
										  $$ = new FrameValueExpression($3, frameProperty, FVO_DEL);
										}
	|	identifier DOT P_SSID ASSIGN exp				{
										  FrameProperty * frameProperty = new FrameProperty(PT_IEEE_SSID);
										  frameProperty -> addValue($5);
										  $$ = new FrameValueExpression($1, frameProperty);	
										}
	|	DEL LPARENTHESIS identifier DOT P_SSID RPARENTHESIS 		{
									 	  FrameProperty * frameProperty = new FrameProperty(PT_IEEE_SSID);
										  $$ = new FrameValueExpression($3, frameProperty, FVO_DEL);
										}
	|	identifier DOT P_FH_PARAMETER_SET_DWELL_TIME ASSIGN exp		{
										  FrameProperty * frameProperty = 
												new FrameProperty(PT_IEEE_FH_PARAMETER_SET_DWELL_TIME);
										  frameProperty -> addValue($5);
										  $$ = new FrameValueExpression($1, frameProperty);	
										}
	|	DEL LPARENTHESIS identifier DOT P_FH_PARAMETER_SET_DWELL_TIME RPARENTHESIS {
									 	  FrameProperty * frameProperty = 
												new FrameProperty(PT_IEEE_FH_PARAMETER_SET_DWELL_TIME);
										  $$ = new FrameValueExpression($3, frameProperty, FVO_DEL);
										}
	|	identifier DOT P_FH_PARAMETER_SET_HOP_SET ASSIGN exp		{
										  FrameProperty * frameProperty = new FrameProperty(PT_IEEE_FH_PARAMETER_SET_HOP_SET);
										  frameProperty -> addValue($5);
										  $$ = new FrameValueExpression($1, frameProperty);		
										}
	|	DEL LPARENTHESIS identifier DOT P_FH_PARAMETER_SET_HOP_SET RPARENTHESIS {
									 	  FrameProperty * frameProperty = new FrameProperty(PT_IEEE_FH_PARAMETER_SET_HOP_SET);
										  $$ = new FrameValueExpression($3, frameProperty, FVO_DEL);
										}
	|	identifier DOT P_FH_PARAMETER_SET_HOP_PATTERN ASSIGN exp	{
										  FrameProperty * frameProperty = 
												new FrameProperty(PT_IEEE_FH_PARAMETER_SET_HOP_PATTERN);
										  frameProperty -> addValue($5);
										  $$ = new FrameValueExpression($1, frameProperty);	
										}
	|	DEL LPARENTHESIS identifier DOT P_FH_PARAMETER_SET_HOP_PATTERN RPARENTHESIS {
									 	  FrameProperty * frameProperty = 
												new FrameProperty(PT_IEEE_FH_PARAMETER_SET_HOP_PATTERN);
										  $$ = new FrameValueExpression($3, frameProperty, FVO_DEL);
										}
	|	identifier DOT P_FH_PARAMETER_SET_HOP_INDEX ASSIGN exp		{
										  FrameProperty * frameProperty = 
												new FrameProperty(PT_IEEE_FH_PARAMETER_SET_HOP_INDEX);
										  frameProperty -> addValue($5);
										  $$ = new FrameValueExpression($1, frameProperty);	
										}
	|	DEL LPARENTHESIS identifier DOT P_FH_PARAMETER_SET_HOP_INDEX RPARENTHESIS {
									 	  FrameProperty * frameProperty = 
												new FrameProperty(PT_IEEE_FH_PARAMETER_SET_HOP_INDEX);
										  $$ = new FrameValueExpression($3, frameProperty, FVO_DEL);
										}
	|	DEL LPARENTHESIS identifier DOT P_FH_PARAMETER_SET RPARENTHESIS	{
									 	  FrameProperty * frameProperty = new FrameProperty(PT_IEEE_FH_PARAMETER_SET);
										  $$ = new FrameValueExpression($3, frameProperty, FVO_DEL);
										}
	|	identifier DOT P_CF_PARAMETER_SET_COUNT ASSIGN exp		{
										  FrameProperty * frameProperty = new FrameProperty(PT_IEEE_CF_PARAMETER_SET_COUNT);
										  frameProperty -> addValue($5);
										  $$ = new FrameValueExpression($1, frameProperty);
										}
	|	DEL LPARENTHESIS identifier DOT P_CF_PARAMETER_SET_COUNT RPARENTHESIS {
									 	  FrameProperty * frameProperty = new FrameProperty(PT_IEEE_CF_PARAMETER_SET_COUNT);
										  $$ = new FrameValueExpression($3, frameProperty, FVO_DEL);
										}
	|	identifier DOT P_CF_PARAMETER_SET_PERIOD ASSIGN exp		{
										  FrameProperty * frameProperty = new FrameProperty(PT_IEEE_CF_PARAMETER_SET_PERIOD);
										  frameProperty -> addValue($5);
										  $$ = new FrameValueExpression($1, frameProperty);	
										}
	|	DEL LPARENTHESIS identifier DOT P_CF_PARAMETER_SET_PERIOD RPARENTHESIS {
									 	  FrameProperty * frameProperty = new FrameProperty(PT_IEEE_CF_PARAMETER_SET_PERIOD);
										  $$ = new FrameValueExpression($3, frameProperty, FVO_DEL);
										}
	|	identifier DOT P_CF_PARAMETER_SET_MAX_DURATION ASSIGN exp	{
										  FrameProperty * frameProperty = 
												new FrameProperty(PT_IEEE_CF_PARAMETER_SET_MAX_DURATION);
										  frameProperty -> addValue($5);
										  $$ = new FrameValueExpression($1, frameProperty);	
										}
	|	DEL LPARENTHESIS identifier DOT P_CF_PARAMETER_SET_MAX_DURATION RPARENTHESIS {
									 	  FrameProperty * frameProperty = 
												new FrameProperty(PT_IEEE_CF_PARAMETER_SET_MAX_DURATION);
										  $$ = new FrameValueExpression($3, frameProperty, FVO_DEL);
										}
	|	identifier DOT P_CF_PARAMETER_SET_REMAINING ASSIGN exp		{
										  FrameProperty * frameProperty = 
												new FrameProperty(PT_IEEE_CF_PARAMETER_SET_REMAINING);
										  frameProperty -> addValue($5);
										  $$ = new FrameValueExpression($1, frameProperty);	
										}
	|	DEL LPARENTHESIS identifier DOT P_CF_PARAMETER_SET_REMAINING RPARENTHESIS {
									 	  FrameProperty * frameProperty = 
												new FrameProperty(PT_IEEE_CF_PARAMETER_SET_REMAINING);
										  $$ = new FrameValueExpression($3, frameProperty, FVO_DEL);
										}
	|	DEL LPARENTHESIS identifier DOT P_CF_PARAMETER_SET RPARENTHESIS {
									 	  FrameProperty * frameProperty = new FrameProperty(PT_IEEE_CF_PARAMETER_SET);
										  $$ = new FrameValueExpression($3, frameProperty, FVO_DEL);
										}
	|	identifier DOT P_DS_PARAMETER_SET ASSIGN exp			{
										  FrameProperty * frameProperty = new FrameProperty(PT_IEEE_DS_PARAMETER_SET);
										  frameProperty -> addValue($5);
										  $$ = new FrameValueExpression($1, frameProperty);	
										}
	|	DEL LPARENTHESIS identifier DOT P_DS_PARAMETER_SET RPARENTHESIS {
									 	  FrameProperty * frameProperty = new FrameProperty(PT_IEEE_DS_PARAMETER_SET);
										  $$ = new FrameValueExpression($3, frameProperty, FVO_DEL);
										}
	|	identifier DOT P_IBSS_PARAMETER_SET ASSIGN exp			{
										  FrameProperty * frameProperty = new FrameProperty(PT_IEEE_IBSS_PARAMETER_SET);
										  frameProperty -> addValue($5);
										  $$ = new FrameValueExpression($1, frameProperty);
										}
	|	DEL LPARENTHESIS identifier DOT P_IBSS_PARAMETER_SET RPARENTHESIS {
									 	  FrameProperty * frameProperty = new FrameProperty(PT_IEEE_IBSS_PARAMETER_SET);
										  $$ = new FrameValueExpression($3, frameProperty, FVO_DEL);
										}
	|	identifier DOT P_TIM_DTIM_COUNT ASSIGN exp			{
										  FrameProperty * frameProperty = new FrameProperty(PT_IEEE_TIM_DTIM_COUNT);
										  frameProperty -> addValue($5);
										  $$ = new FrameValueExpression($1, frameProperty);	
										}
	|	DEL LPARENTHESIS identifier DOT P_TIM_DTIM_COUNT RPARENTHESIS   {
									 	  FrameProperty * frameProperty = new FrameProperty(PT_IEEE_TIM_DTIM_COUNT);
										  $$ = new FrameValueExpression($3, frameProperty, FVO_DEL);
										}
	|	identifier DOT P_TIM_DTIM_PERIOD ASSIGN exp			{
										  FrameProperty * frameProperty = new FrameProperty(PT_IEEE_TIM_DTIM_PERIOD);
										  frameProperty -> addValue($5);
										  $$ = new FrameValueExpression($1, frameProperty);	
										}
	|	DEL LPARENTHESIS identifier DOT P_TIM_DTIM_PERIOD RPARENTHESIS {
									 	  FrameProperty * frameProperty = new FrameProperty(PT_IEEE_TIM_DTIM_PERIOD);
										  $$ = new FrameValueExpression($3, frameProperty, FVO_DEL);
										}
	|	identifier DOT P_TIM_BITMAP_CONTROL ASSIGN exp			{
										  FrameProperty * frameProperty = new FrameProperty(PT_IEEE_TIM_BITMAP_CONTROL);
										  frameProperty -> addValue($5);
										  $$ = new FrameValueExpression($1, frameProperty);	
										}
	|	DEL LPARENTHESIS identifier DOT P_TIM_BITMAP_CONTROL RPARENTHESIS {
									 	  FrameProperty * frameProperty = new FrameProperty(PT_IEEE_TIM_BITMAP_CONTROL);
										  $$ = new FrameValueExpression($3, frameProperty, FVO_DEL);
										}
	|	identifier DOT P_TIM_PARTIAL_VIRTUAL_BITMAP ASSIGN HEX_NUMBER	{
										  FrameProperty * frameProperty = 
												new FrameProperty(PT_IEEE_TIM_PARTIAL_VIRTUAL_BITMAP);
										  frameProperty -> addValue(new StringValueExpression($5)); free($5);
										  $$ = new FrameValueExpression($1, frameProperty);	
										}
	|	DEL LPARENTHESIS identifier DOT P_TIM_PARTIAL_VIRTUAL_BITMAP RPARENTHESIS {
									 	  FrameProperty * frameProperty = 
												new FrameProperty(PT_IEEE_TIM_PARTIAL_VIRTUAL_BITMAP);
										  $$ = new FrameValueExpression($3, frameProperty, FVO_DEL);
										}
	|	DEL LPARENTHESIS identifier DOT P_TIM RPARENTHESIS 		{
									 	  FrameProperty * frameProperty = new FrameProperty(PT_IEEE_TIM);
										  $$ = new FrameValueExpression($3, frameProperty, FVO_DEL);
										}
	|	identifier DOT P_COUNTRY_STRING ASSIGN text			{
										  FrameProperty * frameProperty = new FrameProperty(PT_IEEE_COUNTRY_STRING);
										  frameProperty -> addValue($5);
										  $$ = new FrameValueExpression($1, frameProperty);	
										}
	|	DEL LPARENTHESIS identifier DOT P_COUNTRY_STRING RPARENTHESIS   {
									 	  FrameProperty * frameProperty = new FrameProperty(PT_IEEE_COUNTRY_STRING);
										  $$ = new FrameValueExpression($3, frameProperty, FVO_DEL);
										}
	|	identifier DOT P_COUNTRY_CONSTRAINT ASSIGN exp DD exp DD exp DD exp {
										  FrameProperty * frameProperty = new FrameProperty(PT_IEEE_COUNTRY_CONSTRAINT);
										  frameProperty -> addValue($5);
										  frameProperty -> addValue($7);
										  frameProperty -> addValue($9);
										  frameProperty -> addValue($11);
										  $$ = new FrameValueExpression($1, frameProperty); 	
										}
	|	DEL LPARENTHESIS identifier DOT P_COUNTRY_CONSTRAINT ASSIGN exp RPARENTHESIS {
									 	  FrameProperty * frameProperty = new FrameProperty(PT_IEEE_COUNTRY_CONSTRAINT);
										  frameProperty -> addValue($7);
										  $$ = new FrameValueExpression($3, frameProperty, FVO_DEL);
										}
	|	DEL LPARENTHESIS identifier DOT P_COUNTRY RPARENTHESIS 		{
										  FrameProperty * frameProperty = new FrameProperty(PT_IEEE_COUNTRY);
										  $$ = new FrameValueExpression($3, frameProperty, FVO_DEL);
										}
	|	identifier DOT P_LOCAL_POWER_CONSTRAINT ASSIGN exp		{
									 	  FrameProperty * frameProperty = new FrameProperty(PT_IEEE_LOCAL_POWER_CONSTRAINT);
										  frameProperty -> addValue($5);
										  $$ = new FrameValueExpression($1, frameProperty);	
										}
	|	DEL LPARENTHESIS identifier DOT P_LOCAL_POWER_CONSTRAINT RPARENTHESIS   {
									 	  FrameProperty * frameProperty = new FrameProperty(PT_IEEE_LOCAL_POWER_CONSTRAINT);
										  $$ = new FrameValueExpression($3, frameProperty, FVO_DEL);
										}
	|	identifier DOT P_CHANNEL_SWITCH_MODE ASSIGN exp			{
									 	  FrameProperty * frameProperty = new FrameProperty(PT_IEEE_CHANNEL_SWITCH_MODE);
										  frameProperty -> addValue($5);
										  $$ = new FrameValueExpression($1, frameProperty);		
										}
	|	DEL LPARENTHESIS identifier DOT P_CHANNEL_SWITCH_MODE RPARENTHESIS   {
									 	  FrameProperty * frameProperty = new FrameProperty(PT_IEEE_CHANNEL_SWITCH_MODE);
										  $$ = new FrameValueExpression($3, frameProperty, FVO_DEL);
										}
	|	identifier DOT P_NEW_CHANNEL_NUMBER ASSIGN exp			{
									 	  FrameProperty * frameProperty = new FrameProperty(PT_IEEE_NEW_CHANNEL_NUMBER);
										  frameProperty -> addValue($5);
										  $$ = new FrameValueExpression($1, frameProperty);		
										}
	|	DEL LPARENTHESIS identifier DOT P_NEW_CHANNEL_NUMBER RPARENTHESIS   {
									 	  FrameProperty * frameProperty = new FrameProperty(PT_IEEE_NEW_CHANNEL_NUMBER);
										  $$ = new FrameValueExpression($3, frameProperty, FVO_DEL);
										}
	|	identifier DOT P_CHANNEL_SWITCH_COUNT ASSIGN exp		{
									 	  FrameProperty * frameProperty = new FrameProperty(PT_IEEE_CHANNEL_SWITCH_COUNT);
										  frameProperty -> addValue($5);
										  $$ = new FrameValueExpression($1, frameProperty);		
										}
	|	DEL LPARENTHESIS identifier DOT P_CHANNEL_SWITCH_COUNT RPARENTHESIS {
									 	  FrameProperty * frameProperty = new FrameProperty(PT_IEEE_CHANNEL_SWITCH_COUNT);
										  $$ = new FrameValueExpression($3, frameProperty, FVO_DEL);
										}

	|	DEL LPARENTHESIS identifier DOT P_CHANNEL_SWITCH RPARENTHESIS 	{
									 	  FrameProperty * frameProperty = new FrameProperty(PT_IEEE_CHANNEL_SWITCH);
										  $$ = new FrameValueExpression($3, frameProperty, FVO_DEL);
										}
	|	identifier DOT P_QUIET_COUNT ASSIGN exp				{
									 	  FrameProperty * frameProperty = new FrameProperty(PT_IEEE_QUIET_COUNT);
										  frameProperty -> addValue($5);
										  $$ = new FrameValueExpression($1, frameProperty);		
										}
	|	DEL LPARENTHESIS identifier DOT P_QUIET_COUNT RPARENTHESIS 	{
									 	  FrameProperty * frameProperty = new FrameProperty(PT_IEEE_QUIET_COUNT);
										  $$ = new FrameValueExpression($3, frameProperty, FVO_DEL);
										}
	|	identifier DOT P_QUIET_PERIOD ASSIGN exp			{
									 	  FrameProperty * frameProperty = new FrameProperty(PT_IEEE_QUIET_PERIOD);
										  frameProperty -> addValue($5);
										  $$ = new FrameValueExpression($1, frameProperty);		
										}
	|	DEL LPARENTHESIS identifier DOT P_QUIET_PERIOD RPARENTHESIS 	{
									 	  FrameProperty * frameProperty = new FrameProperty(PT_IEEE_QUIET_PERIOD);
										  $$ = new FrameValueExpression($3, frameProperty, FVO_DEL);
										}
	|	identifier DOT P_QUIET_DURATION ASSIGN exp			{
									 	  FrameProperty * frameProperty = new FrameProperty(PT_IEEE_QUIET_DURATION);
										  frameProperty -> addValue($5);
										  $$ = new FrameValueExpression($1, frameProperty);		
										}
	|	DEL LPARENTHESIS identifier DOT P_QUIET_DURATION RPARENTHESIS 	{
									 	  FrameProperty * frameProperty = new FrameProperty(PT_IEEE_QUIET_DURATION);
										  $$ = new FrameValueExpression($3, frameProperty, FVO_DEL);
										}
	|	identifier DOT P_QUIET_OFFSET ASSIGN exp			{
									 	  FrameProperty * frameProperty = new FrameProperty(PT_IEEE_QUIET_OFFSET);
										  frameProperty -> addValue($5);
										  $$ = new FrameValueExpression($1, frameProperty);		
										}
	|	DEL LPARENTHESIS identifier DOT P_QUIET_OFFSET RPARENTHESIS 	{
									 	  FrameProperty * frameProperty = new FrameProperty(PT_IEEE_QUIET_OFFSET);
										  $$ = new FrameValueExpression($3, frameProperty, FVO_DEL);
										}
	|	DEL LPARENTHESIS identifier DOT P_QUIET RPARENTHESIS 		{
									 	  FrameProperty * frameProperty = new FrameProperty(PT_IEEE_QUIET);
										  $$ = new FrameValueExpression($3, frameProperty, FVO_DEL);
										}
	|	identifier DOT P_TRANSMIT_POWER ASSIGN exp			{
									 	  FrameProperty * frameProperty = new FrameProperty(PT_IEEE_TRANSMIT_POWER);
										  frameProperty -> addValue($5);
										  $$ = new FrameValueExpression($1, frameProperty);	
										}
	|	DEL LPARENTHESIS identifier DOT P_TRANSMIT_POWER RPARENTHESIS 	{
									 	  FrameProperty * frameProperty = new FrameProperty(PT_IEEE_TRANSMIT_POWER);
										  $$ = new FrameValueExpression($3, frameProperty, FVO_DEL);
										}
	|	identifier DOT P_LINK_MARGIN ASSIGN exp				{
									 	  FrameProperty * frameProperty = new FrameProperty(PT_IEEE_LINK_MARGIN);
										  frameProperty -> addValue($5);
										  $$ = new FrameValueExpression($1, frameProperty);	
										}
	|	DEL LPARENTHESIS identifier DOT P_LINK_MARGIN RPARENTHESIS 	{
									 	  FrameProperty * frameProperty = new FrameProperty(PT_IEEE_LINK_MARGIN);
										  $$ = new FrameValueExpression($3, frameProperty, FVO_DEL);
										}
	|	identifier DOT P_ERP ASSIGN exp					{
									 	  FrameProperty * frameProperty = new FrameProperty(PT_IEEE_ERP);
										  frameProperty -> addValue($5);
										  $$ = new FrameValueExpression($1, frameProperty);	
										}
	|	identifier DOT P_ERP ASSIGN QUOAT keyword QUOAT			{
										  FrameProperty * frameProperty = new FrameProperty(PT_IEEE_ERP);
										  frameProperty -> addValue($6);
										  keywordInfo.include = keywordInfo.exclude = 0;
										  $$ = new FrameValueExpression($1, frameProperty);	
										}
	|	DEL LPARENTHESIS identifier DOT P_ERP RPARENTHESIS 		{
									 	  FrameProperty * frameProperty = new FrameProperty(PT_IEEE_ERP);
										  $$ = new FrameValueExpression($3, frameProperty, FVO_DEL);
										}
	|	identifier DOT P_SUPPORTED_RATE ASSIGN exp DD exp		{
									 	  FrameProperty * frameProperty = new FrameProperty(PT_IEEE_SUPPORTED_RATE);
										  frameProperty -> addValue($5);
										  frameProperty -> addValue($7);
										  $$ = new FrameValueExpression($1, frameProperty); 	
										}
	|	DEL LPARENTHESIS identifier DOT P_SUPPORTED_RATE ASSIGN exp RPARENTHESIS {
									 	  FrameProperty * frameProperty = new FrameProperty(PT_IEEE_SUPPORTED_RATE);
										  frameProperty -> addValue($7);
										  $$ = new FrameValueExpression($3, frameProperty, FVO_DEL);
										}
	|	DEL LPARENTHESIS identifier DOT P_SUPPORTED_RATE RPARENTHESIS 	{
									 	  FrameProperty * frameProperty = new FrameProperty(PT_IEEE_SUPPORTED_RATE_ALL);
										  $$ = new FrameValueExpression($3, frameProperty, FVO_DEL);
										}
	|	identifier DOT P_EXTENDED_SUPPORTED_RATE ASSIGN exp DD exp {
									 	  FrameProperty * frameProperty = new FrameProperty(PT_IEEE_EXTENDED_SUPPORTED_RATE);
										  frameProperty -> addValue($5);
										  frameProperty -> addValue($7);
										  $$ = new FrameValueExpression($1, frameProperty); 	
										}
	|	DEL LPARENTHESIS identifier DOT P_EXTENDED_SUPPORTED_RATE ASSIGN exp RPARENTHESIS {
									 	  FrameProperty * frameProperty = new FrameProperty(PT_IEEE_EXTENDED_SUPPORTED_RATE);
										  frameProperty -> addValue($7);
										  $$ = new FrameValueExpression($3, frameProperty, FVO_DEL);
										}
	|	DEL LPARENTHESIS identifier DOT P_EXTENDED_SUPPORTED_RATE RPARENTHESIS 	{
									 	  FrameProperty * frameProperty = 
												new FrameProperty(PT_IEEE_EXTENDED_SUPPORTED_RATE_ALL);
										  $$ = new FrameValueExpression($3, frameProperty, FVO_DEL);
										}
	|	identifier DOT P_REASON_CODE ASSIGN exp				{
									 	  FrameProperty * frameProperty = new FrameProperty(PT_IEEE_REASON_CODE);
										  frameProperty -> addValue($5);
										  $$ = new FrameValueExpression($1, frameProperty);
										}
	|	DEL LPARENTHESIS identifier DOT P_REASON_CODE RPARENTHESIS	{
									 	  FrameProperty * frameProperty = new FrameProperty(PT_IEEE_REASON_CODE);
										  $$ = new FrameValueExpression($3, frameProperty, FVO_DEL);
										}
	|	identifier DOT P_LISTEN_INTERVAL ASSIGN exp			{
									 	  FrameProperty * frameProperty = new FrameProperty(PT_IEEE_LISTEN_INTERVAL);
										  frameProperty -> addValue($5);
										  $$ = new FrameValueExpression($1, frameProperty);
										}
	|	DEL LPARENTHESIS identifier DOT P_LISTEN_INTERVAL RPARENTHESIS	{
									 	  FrameProperty * frameProperty = new FrameProperty(PT_IEEE_LISTEN_INTERVAL);
										  $$ = new FrameValueExpression($3, frameProperty, FVO_DEL);
										}
	|	identifier DOT P_CURRENT_AP_ADDRESS ASSIGN QUOAT MAC_ADDRESS QUOAT {
									 	  FrameProperty * frameProperty = new FrameProperty(PT_IEEE_CURRENT_AP_ADDRESS);
										  frameProperty -> addValue(new StringValueExpression($6)); free($6);
										  $$ = new FrameValueExpression($1, frameProperty);
										}
	|	identifier DOT P_CURRENT_AP_ADDRESS ASSIGN exp			{
									 	  FrameProperty * frameProperty = new FrameProperty(PT_IEEE_CURRENT_AP_ADDRESS);
										  frameProperty -> addValue($5);
										  $$ = new FrameValueExpression($1, frameProperty);
										}
	|	DEL LPARENTHESIS identifier DOT P_CURRENT_AP_ADDRESS RPARENTHESIS {
									 	  FrameProperty * frameProperty = new FrameProperty(PT_IEEE_CURRENT_AP_ADDRESS);
										  $$ = new FrameValueExpression($3, frameProperty, FVO_DEL);
										}
	|	identifier DOT P_RSN_VERSION ASSIGN exp				{
									 	  FrameProperty * frameProperty = new FrameProperty(PT_IEEE_RSN_VERSION);
										  frameProperty -> addValue($5);
										  $$ = new FrameValueExpression($1, frameProperty);
										}
	|	DEL LPARENTHESIS identifier DOT P_RSN_VERSION RPARENTHESIS 	{
									 	  FrameProperty * frameProperty = new FrameProperty(PT_IEEE_RSN_VERSION);
										  $$ = new FrameValueExpression($3, frameProperty, FVO_DEL);
										}
	|	identifier DOT P_GROUP_CIPHER_SUITE ASSIGN QUOAT OUI QUOAT DD numberkeyword {
									 	  FrameProperty * frameProperty = new FrameProperty(PT_IEEE_GROUP_CIPHER_SUITE);
										  frameProperty -> addValue(new StringValueExpression($6)); free($6);
										  frameProperty -> addValue($9);
										  $$ = new FrameValueExpression($1, frameProperty);
										}
	|	DEL LPARENTHESIS identifier DOT P_GROUP_CIPHER_SUITE RPARENTHESIS 	{
									 	  FrameProperty * frameProperty = new FrameProperty(PT_IEEE_REASON_CODE);
										  $$ = new FrameValueExpression($3, frameProperty, FVO_DEL);
										}
	|	identifier DOT P_PAIRWISE_CIPHER_SUITE ASSIGN  exp DD QUOAT OUI QUOAT DD numberkeyword {
									 	  FrameProperty * frameProperty = new FrameProperty(PT_IEEE_PAIRWISE_CIPHER_SUITE);
										  frameProperty -> addValue($5);
										  frameProperty -> addValue(new StringValueExpression($8)); free($8);
										  frameProperty -> addValue($11);
										  $$ = new FrameValueExpression($1, frameProperty);
										}
	|	DEL LPARENTHESIS identifier DOT P_PAIRWISE_CIPHER_SUITE ASSIGN exp RPARENTHESIS {
									 	  FrameProperty * frameProperty = new FrameProperty(PT_IEEE_PAIRWISE_CIPHER_SUITE);
										  frameProperty -> addValue($7);
										  $$ = new FrameValueExpression($3, frameProperty, FVO_DEL);
										}
	|	DEL LPARENTHESIS identifier DOT P_PAIRWISE_CIPHER_SUITE RPARENTHESIS {
									 	  FrameProperty * frameProperty = 
												new FrameProperty(PT_IEEE_PAIRWISE_CIPHER_SUITE_ALL);
										  $$ = new FrameValueExpression($3, frameProperty, FVO_DEL);
										}
	|	identifier DOT P_AUTHENTICATION_SUITE ASSIGN  exp DD QUOAT OUI QUOAT DD numberkeyword {
									 	  FrameProperty * frameProperty = new FrameProperty(PT_IEEE_AUTHENTICATION_SUITE);
										  frameProperty -> addValue($5);
										  frameProperty -> addValue(new StringValueExpression($8)); free($8);
										  frameProperty -> addValue($11);
										  $$ = new FrameValueExpression($1, frameProperty);
										}
	|	DEL LPARENTHESIS identifier DOT P_AUTHENTICATION_SUITE ASSIGN exp RPARENTHESIS {
									 	  FrameProperty * frameProperty = new FrameProperty(PT_IEEE_AUTHENTICATION_SUITE);
										  frameProperty -> addValue($7);
										  $$ = new FrameValueExpression($3, frameProperty, FVO_DEL);
										}
	|	DEL LPARENTHESIS identifier DOT P_AUTHENTICATION_SUITE RPARENTHESIS {
									 	  FrameProperty * frameProperty = new FrameProperty(PT_IEEE_AUTHENTICATION_SUITE_ALL);
										  $$ = new FrameValueExpression($3, frameProperty, FVO_DEL);
										}
	|	identifier DOT P_RSN_CAPABILITIES ASSIGN exp			{
									 	  FrameProperty * frameProperty = new FrameProperty(PT_IEEE_RSN_CAPABILITIES);
										  frameProperty -> addValue($5);
										  $$ = new FrameValueExpression($1, frameProperty);
										}
	|      identifier DOT P_RSN_CAPABILITIES ASSIGN QUOAT keyword QUOAT	{	 
										  FrameProperty * frameProperty = new FrameProperty(PT_IEEE_RSN_CAPABILITIES);
										  frameProperty -> addValue($6);
										  keywordInfo.include = keywordInfo.exclude = 0;
										  $$ = new FrameValueExpression($1, frameProperty);
										}
	|	DEL LPARENTHESIS identifier DOT P_RSN_CAPABILITIES RPARENTHESIS {
									 	  FrameProperty * frameProperty = new FrameProperty(PT_IEEE_RSN_CAPABILITIES);
										  $$ = new FrameValueExpression($3, frameProperty, FVO_DEL);
										}
	|	DEL LPARENTHESIS identifier DOT P_RSN RPARENTHESIS 		{
									 	  FrameProperty * frameProperty = new FrameProperty(PT_IEEE_RSN);
										  $$ = new FrameValueExpression($3, frameProperty, FVO_DEL);
										}
	|	identifier DOT P_STATUS_CODE ASSIGN exp				{
									 	  FrameProperty * frameProperty = new FrameProperty(PT_IEEE_STATUS_CODE);
										  frameProperty -> addValue($5);
										  $$ = new FrameValueExpression($1, frameProperty);
										}
	|	DEL LPARENTHESIS identifier DOT P_STATUS_CODE RPARENTHESIS 	{
									 	  FrameProperty * frameProperty = new FrameProperty(PT_IEEE_STATUS_CODE);
										  $$ = new FrameValueExpression($3, frameProperty, FVO_DEL);
										}
	|	identifier DOT P_ASSOCIATION_ID ASSIGN exp			{
									 	  FrameProperty * frameProperty = new FrameProperty(PT_IEEE_ASSOCIATION_ID);
										  frameProperty -> addValue($5);
										  $$ = new FrameValueExpression($1, frameProperty);
										}
	|	DEL LPARENTHESIS identifier DOT P_ASSOCIATION_ID RPARENTHESIS 	{
									 	  FrameProperty * frameProperty = new FrameProperty(PT_IEEE_ASSOCIATION_ID);
										  $$ = new FrameValueExpression($3, frameProperty, FVO_DEL);
										}
	|	identifier DOT P_AUTHENTICATION_ALGORITHM_NUMBER ASSIGN exp	{
									 	  FrameProperty * frameProperty = 
												new FrameProperty(PT_IEEE_AUTHENTICATION_ALGORITHM_NUMBER);
										  frameProperty -> addValue($5);
										  $$ = new FrameValueExpression($1, frameProperty);
										}
	|	DEL LPARENTHESIS identifier DOT P_AUTHENTICATION_ALGORITHM_NUMBER RPARENTHESIS 	{
									 	  FrameProperty * frameProperty = 
												new FrameProperty(PT_IEEE_AUTHENTICATION_ALGORITHM_NUMBER);
										  $$ = new FrameValueExpression($3, frameProperty, FVO_DEL);
										}
	|	identifier DOT P_AUTHENTICATION_SEQUENCE_NUMBER ASSIGN exp	{
									 	  FrameProperty * frameProperty = 
												new FrameProperty(PT_IEEE_AUTHENTICATION_SEQUENCE_NUMBER);
										  frameProperty -> addValue($5);
										  $$ = new FrameValueExpression($1, frameProperty);
										}
	|	DEL LPARENTHESIS identifier DOT P_AUTHENTICATION_SEQUENCE_NUMBER RPARENTHESIS {
									 	  FrameProperty * frameProperty = 
												new FrameProperty(PT_IEEE_AUTHENTICATION_SEQUENCE_NUMBER);
										  $$ = new FrameValueExpression($3, frameProperty, FVO_DEL);
										}
	|	identifier DOT P_CHALLENGE_TEXT ASSIGN HEX_NUMBER		{
									 	  FrameProperty * frameProperty = new FrameProperty(PT_IEEE_CHALLENGE_TEXT);
										  frameProperty -> addValue(new StringValueExpression($5)); free($5);
										  $$ = new FrameValueExpression($1, frameProperty);	
										}
	|	DEL LPARENTHESIS identifier DOT P_CHALLENGE_TEXT RPARENTHESIS   {
									 	  FrameProperty * frameProperty = new FrameProperty(PT_IEEE_CHALLENGE_TEXT);
										  $$ = new FrameValueExpression($3, frameProperty, FVO_DEL);
										}
	|	identifier DOT P_QOS ASSIGN exp					{
									 	  FrameProperty * frameProperty = new FrameProperty(PT_IEEE_QOS);
										  frameProperty -> addValue($5);
										  $$ = new FrameValueExpression($1, frameProperty);
										}
	|	DEL LPARENTHESIS identifier DOT P_QOS RPARENTHESIS 		{
									 	  FrameProperty * frameProperty = new FrameProperty(PT_IEEE_QOS);
										  $$ = new FrameValueExpression($3, frameProperty, FVO_DEL);
										}
	|	identifier DOT P_ADDRESS1 ASSIGN QUOAT MAC_ADDRESS QUOAT 	{
										  FrameProperty * frameProperty = new FrameProperty(PT_IEEE_ADDRESS1);
										  frameProperty -> addValue(new StringValueExpression($6)); free($6);
										  $$ = new FrameValueExpression($1, frameProperty);	
										}
	|	identifier DOT P_ADDRESS1 ASSIGN exp			 	{
										  FrameProperty * frameProperty = new FrameProperty(PT_IEEE_ADDRESS1);
										  frameProperty -> addValue($5);
										  $$ = new FrameValueExpression($1, frameProperty);	
										}
	|	DEL LPARENTHESIS identifier DOT P_ADDRESS1 RPARENTHESIS 	{
										  FrameProperty * frameProperty = new FrameProperty(PT_IEEE_ADDRESS1);
										  $$ = new FrameValueExpression($3, frameProperty, FVO_DEL);
										}
	|	identifier DOT P_ADDRESS2 ASSIGN QUOAT MAC_ADDRESS QUOAT 	{
										  FrameProperty * frameProperty = new FrameProperty(PT_IEEE_ADDRESS2);
										  frameProperty -> addValue(new StringValueExpression($6)); free($6);
										  $$ = new FrameValueExpression($1, frameProperty);	
										}
	|	identifier DOT P_ADDRESS2 ASSIGN exp 				{
										  FrameProperty * frameProperty = new FrameProperty(PT_IEEE_ADDRESS2);
										  frameProperty -> addValue($5);
										  $$ = new FrameValueExpression($1, frameProperty);	
										}
	|	DEL LPARENTHESIS identifier DOT P_ADDRESS2 RPARENTHESIS 	{
										  FrameProperty * frameProperty = new FrameProperty(PT_IEEE_ADDRESS2);
										  $$ = new FrameValueExpression($3, frameProperty, FVO_DEL);
										}
	|	identifier DOT P_ADDRESS3 ASSIGN QUOAT MAC_ADDRESS QUOAT 	{
										  FrameProperty * frameProperty = new FrameProperty(PT_IEEE_ADDRESS3);
										  frameProperty -> addValue(new StringValueExpression($6)); free($6);
										  $$ = new FrameValueExpression($1, frameProperty);	
										}
	|	identifier DOT P_ADDRESS3 ASSIGN exp			 	{
										  FrameProperty * frameProperty = new FrameProperty(PT_IEEE_ADDRESS3);
										  frameProperty -> addValue($5);
										  $$ = new FrameValueExpression($1, frameProperty);	
										}
	|	DEL LPARENTHESIS identifier DOT P_ADDRESS3 RPARENTHESIS 	{
										  FrameProperty * frameProperty = new FrameProperty(PT_IEEE_ADDRESS3);
										  $$ = new FrameValueExpression($3, frameProperty, FVO_DEL);
										}
	|	identifier DOT P_ADDRESS4 ASSIGN QUOAT MAC_ADDRESS QUOAT 	{
										  FrameProperty * frameProperty = new FrameProperty(PT_IEEE_ADDRESS4);
										  frameProperty -> addValue(new StringValueExpression($6)); free($6);
										  $$ = new FrameValueExpression($1, frameProperty);	
										}
	|	identifier DOT P_ADDRESS4 ASSIGN exp			 	{
										  FrameProperty * frameProperty = new FrameProperty(PT_IEEE_ADDRESS4);
										  frameProperty -> addValue($5);
										  $$ = new FrameValueExpression($1, frameProperty);	
										}
	|	DEL LPARENTHESIS identifier DOT P_ADDRESS4 RPARENTHESIS 	{
										  FrameProperty * frameProperty = new FrameProperty(PT_IEEE_ADDRESS4);
										  $$ = new FrameValueExpression($3, frameProperty, FVO_DEL);
										}
// ***************************************************************************************************************************************************
// ********************************************************* SET a DEL PROPERTY LLC ******************************************************************
// ***************************************************************************************************************************************************
	|	identifier DOT P_DNAP ASSIGN exp				{
										  FrameProperty * frameProperty = new FrameProperty(PT_LLC_DNAP);
										  frameProperty -> addValue($5);
										  $$ = new FrameValueExpression($1, frameProperty);	
										}
	|	DEL LPARENTHESIS identifier DOT P_DNAP RPARENTHESIS		{
										  FrameProperty * frameProperty = new FrameProperty(PT_LLC_DNAP);
										  $$ = new FrameValueExpression($3, frameProperty, FVO_DEL);
										}
	|	identifier DOT P_SNAP ASSIGN exp				{
										  FrameProperty * frameProperty = new FrameProperty(PT_LLC_SNAP);
										  frameProperty -> addValue($5);
										  $$ = new FrameValueExpression($1, frameProperty);	
										}
	|	DEL LPARENTHESIS identifier DOT P_SNAP RPARENTHESIS		{
										  FrameProperty * frameProperty = new FrameProperty(PT_LLC_SNAP);
										  $$ = new FrameValueExpression($3, frameProperty, FVO_DEL);
										}
	|	identifier DOT P_CONTROL_FIELD ASSIGN exp			{
										  FrameProperty * frameProperty = new FrameProperty(PT_LLC_CONTROL_FIELD);
										  frameProperty -> addValue($5);
										  $$ = new FrameValueExpression($1, frameProperty);	
										}
	|	DEL LPARENTHESIS identifier DOT P_CONTROL_FIELD	RPARENTHESIS	{
										  FrameProperty * frameProperty = new FrameProperty(PT_LLC_CONTROL_FIELD);
										  $$ = new FrameValueExpression($3, frameProperty, FVO_DEL);
										}
	|	identifier DOT P_ORGANIZATION_CODE ASSIGN exp			{
										  FrameProperty * frameProperty = new FrameProperty(PT_LLC_ORGANIZATION_CODE);
										  frameProperty -> addValue($5);
										  $$ = new FrameValueExpression($1, frameProperty);
										}
	|	DEL LPARENTHESIS identifier DOT P_ORGANIZATION_CODE RPARENTHESIS {
										  FrameProperty * frameProperty = new FrameProperty(PT_LLC_ORGANIZATION_CODE);
										  $$ = new FrameValueExpression($3, frameProperty, FVO_DEL);
										}
	|	identifier DOT P_LLC_TYPE ASSIGN numberkeyword			{
										  FrameProperty * frameProperty = new FrameProperty(PT_LLC_TYPE);
										  frameProperty -> addValue($5);
										  $$ = new FrameValueExpression($1, frameProperty);	
										}
	|	DEL LPARENTHESIS identifier DOT P_LLC_TYPE RPARENTHESIS		{
										  FrameProperty * frameProperty = new FrameProperty(PT_LLC_TYPE);
										  $$ = new FrameValueExpression($3, frameProperty, FVO_DEL);
										}
	|	DEL LPARENTHESIS identifier DOT H_LLC RPARENTHESIS		{
										  FrameProperty * frameProperty = new FrameProperty(PT_LLC);
										  $$ = new FrameValueExpression($3, frameProperty, FVO_DEL);
										}
// ***************************************************************************************************************************************************
// ********************************************************* SET a DEL PROPERTY PAYLOAD **************************************************************
// ***************************************************************************************************************************************************
	|	identifier DOT P_PAYLOAD_DATA ASSIGN HEX_NUMBER			{
										   FrameProperty * frameProperty = new FrameProperty(PT_PAYLOAD_DATA);
										   frameProperty -> addValue(new StringValueExpression($5)); free($5);
										   $$ = new FrameValueExpression($1, frameProperty);
										}
	|	identifier DOT P_PAYLOAD_DATA ASSIGN identifier			{
										   FrameProperty * frameProperty = new FrameProperty(PT_PAYLOAD_DATA);
										   frameProperty -> addValue($5);
										   $$ = new FrameValueExpression($1, frameProperty);
										}
	|       DEL LPARENTHESIS identifier DOT P_PAYLOAD_DATA RPARENTHESIS     {
										   FrameProperty * frameProperty = new FrameProperty(PT_PAYLOAD_DATA);
										   $$ = new FrameValueExpression($3, frameProperty, FVO_DEL);
										}
	|	identifier DOT P_PAYLOAD_CIPHER ASSIGN cipher			{
										   FrameProperty * frameProperty = new FrameProperty(PT_PAYLOAD_CIPHER);
										   frameProperty -> addValue($5);
										   $$ = new FrameValueExpression($1, frameProperty);
										}
	|       DEL LPARENTHESIS identifier DOT P_PAYLOAD_CIPHER RPARENTHESIS   {
										   FrameProperty * frameProperty = new FrameProperty(PT_PAYLOAD_CIPHER);
										   $$ = new FrameValueExpression($3, frameProperty, FVO_DEL);
										}
	| 	identifier DOT P_PAYLOAD_VECTOR ASSIGN exp			{
										   FrameProperty * frameProperty = new FrameProperty(PT_PAYLOAD_VECTOR);
										   frameProperty -> addValue($5);
										   $$ = new FrameValueExpression($1, frameProperty);
										}
	|       DEL LPARENTHESIS identifier DOT P_PAYLOAD_VECTOR RPARENTHESIS   {
										   FrameProperty * frameProperty = new FrameProperty(PT_PAYLOAD_VECTOR);
										   $$ = new FrameValueExpression($3, frameProperty, FVO_DEL);	
										}
	|	identifier DOT P_PAYLOAD_KEY ASSIGN HEX_NUMBER			{
										   FrameProperty * frameProperty = new FrameProperty(PT_PAYLOAD_KEY);
										   frameProperty -> addValue(new StringValueExpression($5)); free($5);
										   $$ = new FrameValueExpression($1, frameProperty);	
										}
	|	identifier DOT P_PAYLOAD_KEY ASSIGN identifier			{
										   FrameProperty * frameProperty = new FrameProperty(PT_PAYLOAD_KEY);
										   frameProperty -> addValue($5);
										   $$ = new FrameValueExpression($1, frameProperty);	
										}
	|       DEL LPARENTHESIS identifier DOT P_PAYLOAD_KEY RPARENTHESIS   	{
										   FrameProperty * frameProperty = new FrameProperty(PT_PAYLOAD_KEY);
										   $$ = new FrameValueExpression($3,frameProperty,FVO_DEL);
										}
	|	identifier DOT P_PAYLOAD_KEY_INDEX ASSIGN exp			{
										   FrameProperty * frameProperty = new FrameProperty(PT_PAYLOAD_KEY_INDEX);
										   frameProperty -> addValue($5);
										   $$ = new FrameValueExpression($1, frameProperty);	
										}
	|       DEL LPARENTHESIS identifier DOT P_PAYLOAD_KEY_INDEX RPARENTHESIS {
										   FrameProperty * frameProperty = new FrameProperty(PT_PAYLOAD_KEY_INDEX);
										   $$ = new FrameValueExpression($3,frameProperty,FVO_DEL);
										}
	|       DEL LPARENTHESIS identifier DOT P_PAYLOAD RPARENTHESIS   	{
										   FrameProperty * frameProperty = new FrameProperty(PT_PAYLOAD);
										   $$ = new FrameValueExpression($3, frameProperty, FVO_DEL);
										}
	;
// ***************************************************************************************************************************************************
// ********************************************************* GET PROPERTY RADIOTAP *******************************************************************
// ***************************************************************************************************************************************************
getframeproperty:	identifier DOT P_VERSION				{
										  FrameProperty * frameProperty = new FrameProperty(PT_RADIOTAP_VERSION);
										  $$ = new FrameValueExpression($1, frameProperty, FVO_GET);
										}
	| 	identifier DOT P_PAD						{  
										  FrameProperty * frameProperty = new FrameProperty(PT_RADIOTAP_PAD);
										  $$ = new FrameValueExpression($1, frameProperty, FVO_GET);			
										}
	| 	identifier DOT P_LEN						{  
										  FrameProperty * frameProperty = new FrameProperty(PT_RADIOTAP_LEN);
										  $$ = new FrameValueExpression($1, frameProperty, FVO_GET);
										}
	|	identifier DOT P_ANTENNA					{
										  FrameProperty * frameProperty = new FrameProperty(PT_RADIOTAP_ANTENNA);
										  $$ = new FrameValueExpression($1, frameProperty, FVO_GET);	
										}
	| 	identifier DOT P_ANTENNA_NOISE					{  
										  FrameProperty * frameProperty = new FrameProperty(PT_RADIOTAP_ANTENNA_NOISE);
										  $$ = new FrameValueExpression($1, frameProperty, FVO_GET);
										}
	|	identifier DOT P_ANTENNA_SIGNAL					{
										  FrameProperty * frameProperty = new FrameProperty(PT_RADIOTAP_ANTENNA_SIGNAL);
										  $$ = new FrameValueExpression($1, frameProperty, FVO_GET);	
										}
	|	identifier DOT P_CHANNEL_FREQUENCY				{
										  FrameProperty * frameProperty = new FrameProperty(PT_RADIOTAP_CHANNEL_FREQUENCY);
										  $$ = new FrameValueExpression($1, frameProperty, FVO_GET);	
										}
	|	identifier DOT P_CHANNEL_FLAGS					{
										  FrameProperty * frameProperty = new FrameProperty(PT_RADIOTAP_CHANNEL_FLAGS);
										  $$ = new FrameValueExpression($1, frameProperty, FVO_GET);	
										}
	|	identifier DOT P_FHSS_HOP_SET					{
										  FrameProperty * frameProperty = new FrameProperty(PT_RADIOTAP_FHSS_HOP_SET);
										  $$ = new FrameValueExpression($1, frameProperty, FVO_GET);	
										}
	|	identifier DOT P_FHSS_HOP_PATTERN				{
										  FrameProperty * frameProperty = new FrameProperty(PT_RADIOTAP_FHSS_HOP_PATTERN);
										  $$ = new FrameValueExpression($1, frameProperty, FVO_GET);	
										}
	|	identifier DOT P_FLAGS						{
										  FrameProperty * frameProperty = new FrameProperty(PT_RADIOTAP_FLAGS);
										  $$ = new FrameValueExpression($1, frameProperty, FVO_GET);	
										}
	|	identifier DOT P_LOCK_QUALITY		 			{
										  FrameProperty * frameProperty = new FrameProperty(PT_RADIOTAP_LOCK_QUALITY);
										  $$ = new FrameValueExpression($1, frameProperty, FVO_GET);		
										}
	|	identifier DOT P_MCS_KNOWN	 				{
										  FrameProperty * frameProperty = new FrameProperty(PT_RADIOTAP_MCS_KNOWN);
										  $$ = new FrameValueExpression($1, frameProperty, FVO_GET);	
										}
	|	identifier DOT P_MCS_FLAGS					{
										  FrameProperty * frameProperty = new FrameProperty(PT_RADIOTAP_MCS_FLAGS);
										  $$ = new FrameValueExpression($1, frameProperty, FVO_GET);	
										}
	|	identifier DOT P_MCS_MCS					{
										  FrameProperty * frameProperty = new FrameProperty(PT_RADIOTAP_MCS_MCS);
										  $$ = new FrameValueExpression($1, frameProperty, FVO_GET);	
										}
	|	identifier DOT P_RX_FLAGS					{
										  FrameProperty * frameProperty = new FrameProperty(PT_RADIOTAP_RX_FLAGS);
										  $$ = new FrameValueExpression($1, frameProperty, FVO_GET);	
										}
	|	identifier DOT P_RATE						{
										  FrameProperty * frameProperty = new FrameProperty(PT_RADIOTAP_RATE);
										  $$ = new FrameValueExpression($1, frameProperty, FVO_GET);	
										}
	|	identifier DOT P_TSFT						{
										  FrameProperty * frameProperty = new FrameProperty(PT_RADIOTAP_TSFT);
										  $$ = new FrameValueExpression($1, frameProperty, FVO_GET);	
										}
	|	identifier DOT P_TX_ATTENUATION					{
										  FrameProperty * frameProperty = new FrameProperty(PT_RADIOTAP_TX_ATTENUATION);
										  $$ = new FrameValueExpression($1, frameProperty, FVO_GET);	
										}
	|	identifier DOT P_VENDOR_NAMESPACE_OUI				{
										  FrameProperty * frameProperty = new FrameProperty(PT_RADIOTAP_VENDOR_NAMESPACE_OUI);
										  $$ = new FrameValueExpression($1, frameProperty, FVO_GET);	
										}
	|	identifier DOT P_VENDOR_NAMESPACE_SUB_NAMESPACE			{
										  FrameProperty * frameProperty = 
												new FrameProperty(PT_RADIOTAP_VENDOR_NAMESPACE_SUB_NAMESPACE);
										  $$ = new FrameValueExpression($1, frameProperty, FVO_GET);		
										}
	|	identifier DOT P_VENDOR_NAMESPACE_SKIP_LENGTH			{
										  FrameProperty * frameProperty = 
												new FrameProperty(PT_RADIOTAP_VENDOR_NAMESPACE_SKIP_LENGTH);
										  $$ = new FrameValueExpression($1, frameProperty, FVO_GET);	
										}
	|	identifier DOT P_DB_TX_ATTENUATION				{
										  FrameProperty * frameProperty = new FrameProperty(PT_RADIOTAP_DB_TX_ATTENUATION);
										  $$ = new FrameValueExpression($1, frameProperty, FVO_GET);	
										}
	|	identifier DOT P_DB_ANTENNA_NOISE				{
										  FrameProperty * frameProperty = new FrameProperty(PT_RADIOTAP_DB_ANTENNA_NOISE);
										  $$ = new FrameValueExpression($1, frameProperty, FVO_GET);	
										}
	|	identifier DOT P_DB_ANTENNA_SIGNAL				{
										  FrameProperty * frameProperty = new FrameProperty(PT_RADIOTAP_ANTENNA_SIGNAL);
										  $$ = new FrameValueExpression($1, frameProperty, FVO_GET);	
										}
	|	identifier DOT P_DBM_TX_POWER					{
										  FrameProperty * frameProperty = new FrameProperty(PT_RADIOTAP_DBM_TX_POWER);
										  $$ = new FrameValueExpression($1, frameProperty, FVO_GET);	
										}
	|	identifier DOT P_RTS_RETRIES					{
										  FrameProperty * frameProperty = new FrameProperty(PT_RADIOTAP_RTS_RETRIES);
										  $$ = new FrameValueExpression($1, frameProperty, FVO_GET);	
										}
	|	identifier DOT P_DATA_RETRIES					{
										  FrameProperty * frameProperty = new FrameProperty(PT_RADIOTAP_DATA_RETRIES);
										  $$ = new FrameValueExpression($1, frameProperty, FVO_GET);	
										}
// ***************************************************************************************************************************************************
// ********************************************************* GET PROPERTY IEEE ***********************************************************************
// ***************************************************************************************************************************************************
	|	identifier DOT P_FRAME_CONTROL					{ 
										  FrameProperty * frameProperty = new FrameProperty(PT_IEEE_FRAME_CONTROL);
										  $$ = new FrameValueExpression($1, frameProperty, FVO_GET);	
										}
	|	identifier DOT P_PROTOCOL					{
										  FrameProperty * frameProperty = new FrameProperty(PT_IEEE_PROTOCOL);
										  $$ = new FrameValueExpression($1, frameProperty, FVO_GET);
										}
	|	identifier DOT P_TYPE						{
										  FrameProperty * frameProperty = new FrameProperty(PT_IEEE_TYPE);
										  $$ = new FrameValueExpression($1, frameProperty, FVO_GET);
										}
	|	identifier DOT P_SUB_TYPE					{
										  FrameProperty * frameProperty = new FrameProperty(PT_IEEE_SUB_TYPE);
										  $$ = new FrameValueExpression($1, frameProperty, FVO_GET);
										}
	|	identifier DOT P_TO_DS						{
										  FrameProperty * frameProperty = new FrameProperty(PT_IEEE_TO_DS);
										  $$ = new FrameValueExpression($1, frameProperty, FVO_GET);	
										}
	|	identifier DOT P_FROM_DS					{
										  FrameProperty * frameProperty = new FrameProperty(PT_IEEE_FROM_DS);
										  $$ = new FrameValueExpression($1, frameProperty, FVO_GET);	
										}
	|	identifier DOT P_MORE_FRAG					{
										  FrameProperty * frameProperty = new FrameProperty(PT_IEEE_MORE_FRAG);
										  $$ = new FrameValueExpression($1, frameProperty, FVO_GET);	
										}
	|	identifier DOT P_RETRY						{
										  FrameProperty * frameProperty = new FrameProperty(PT_IEEE_RETRY);
										  $$ = new FrameValueExpression($1, frameProperty, FVO_GET);	
										}
	|	identifier DOT P_PWR_MGMT					{
										  FrameProperty * frameProperty = new FrameProperty(PT_IEEE_PWR_MGMT);
										  $$ = new FrameValueExpression($1, frameProperty, FVO_GET);	
										}
	|	identifier DOT P_MORE_DATA					{
										  FrameProperty * frameProperty = new FrameProperty(PT_IEEE_MORE_DATA);
										  $$ = new FrameValueExpression($1, frameProperty, FVO_GET);
										}
	|	identifier DOT P_PROTECTED_FRAME				{
										  FrameProperty * frameProperty = new FrameProperty(PT_IEEE_PROTECTED_FRAME);
										  $$ = new FrameValueExpression($1, frameProperty, FVO_GET);
										}
	|	identifier DOT P_ORDER						{
										  FrameProperty * frameProperty = new FrameProperty(PT_IEEE_ORDER);
										  $$ = new FrameValueExpression($1, frameProperty, FVO_GET);
										}
	|	identifier DOT P_TRANSMITTER_ADDRESS				{
										  FrameProperty * frameProperty = new FrameProperty(PT_IEEE_TRANSMITTER_ADDRESS);
										  $$ = new FrameValueExpression($1, frameProperty, FVO_GET);	
										}
	|	identifier DOT P_RECEIVER_ADDRESS				{
										  FrameProperty * frameProperty = new FrameProperty(PT_IEEE_RECEIVER_ADDRESS);
										  $$ = new FrameValueExpression($1, frameProperty, FVO_GET);
										}
	|	identifier DOT P_BSSID						{
										  FrameProperty * frameProperty = new FrameProperty(PT_IEEE_BSSID);
										  $$ = new FrameValueExpression($1, frameProperty, FVO_GET);	
										}
	|	identifier DOT P_DURATION					{
										  FrameProperty * frameProperty = new FrameProperty(PT_IEEE_DURATION);
										  $$ = new FrameValueExpression($1, frameProperty, FVO_GET);		
										}
	|	identifier DOT P_AID						{
										  FrameProperty * frameProperty = new FrameProperty(PT_IEEE_AID);
										  $$ = new FrameValueExpression($1, frameProperty, FVO_GET);	
										}
	|	identifier DOT P_DA						{
										  FrameProperty * frameProperty = new FrameProperty(PT_IEEE_DA);
										  $$ = new FrameValueExpression($1, frameProperty, FVO_GET);	
										}
	|	identifier DOT P_SA						{
										  FrameProperty * frameProperty = new FrameProperty(PT_IEEE_SA);
										  $$ = new FrameValueExpression($1, frameProperty, FVO_GET);	
										}
	|	identifier DOT P_SEQ_CTL					{
										  FrameProperty * frameProperty = new FrameProperty(PT_IEEE_SEQ_CTL);
										  $$ = new FrameValueExpression($1, frameProperty, FVO_GET);	
										}
	|	identifier DOT P_FRAGMENT					{
										  FrameProperty * frameProperty = new FrameProperty(PT_IEEE_FRAGMENT);
										  $$ = new FrameValueExpression($1, frameProperty, FVO_GET);	
										}
	|	identifier DOT P_SEQUENCE					{
										  FrameProperty * frameProperty = new FrameProperty(PT_IEEE_SEQUENCE);
										  $$ = new FrameValueExpression($1, frameProperty, FVO_GET);	
										}
	|	identifier DOT P_TIMESTAMP					{
										  FrameProperty * frameProperty = new FrameProperty(PT_IEEE_TIMESTAMP);
										  $$ = new FrameValueExpression($1, frameProperty, FVO_GET);	
										}
	|	identifier DOT P_BEACON_INTERVAL				{
										  FrameProperty * frameProperty = new FrameProperty(PT_IEEE_BEACON_INTERVAL);
										  $$ = new FrameValueExpression($1, frameProperty, FVO_GET);	
										}
	|	identifier DOT P_CAPABILITY_INFO				{
										  FrameProperty * frameProperty = new FrameProperty(PT_IEEE_CAPABILITY_INFO);
										  $$ = new FrameValueExpression($1, frameProperty, FVO_GET);	
										}
	|	identifier DOT P_SSID						{
										  FrameProperty * frameProperty = new FrameProperty(PT_IEEE_SSID);
										  $$ = new FrameValueExpression($1, frameProperty, FVO_GET);	
										}
	|	identifier DOT P_FH_PARAMETER_SET_DWELL_TIME			{
										  FrameProperty * frameProperty = 
												new FrameProperty(PT_IEEE_FH_PARAMETER_SET_DWELL_TIME);
										  $$ = new FrameValueExpression($1, frameProperty, FVO_GET);	
										}
	|	identifier DOT P_FH_PARAMETER_SET_HOP_SET			{
										  FrameProperty * frameProperty = new FrameProperty(PT_IEEE_FH_PARAMETER_SET_HOP_SET);
										  $$ = new FrameValueExpression($1, frameProperty, FVO_GET);	
										}
	|	identifier DOT P_FH_PARAMETER_SET_HOP_PATTERN			{
										  FrameProperty * frameProperty = 
												new FrameProperty(PT_IEEE_FH_PARAMETER_SET_HOP_PATTERN);
										  $$ = new FrameValueExpression($1, frameProperty, FVO_GET);	
										}
	|	identifier DOT P_FH_PARAMETER_SET_HOP_INDEX			{
										  FrameProperty * frameProperty = 
												new FrameProperty(PT_IEEE_FH_PARAMETER_SET_HOP_INDEX);
										  $$ = new FrameValueExpression($1, frameProperty, FVO_GET);	
										}
	|	identifier DOT P_CF_PARAMETER_SET_COUNT				{
										  FrameProperty * frameProperty = new FrameProperty(PT_IEEE_CF_PARAMETER_SET_COUNT);
										  $$ = new FrameValueExpression($1, frameProperty, FVO_GET);	
										}
	|	identifier DOT P_CF_PARAMETER_SET_PERIOD			{
										  FrameProperty * frameProperty = new FrameProperty(PT_IEEE_CF_PARAMETER_SET_PERIOD);
										  $$ = new FrameValueExpression($1, frameProperty, FVO_GET);		
										}
	|	identifier DOT P_CF_PARAMETER_SET_MAX_DURATION			{
										  FrameProperty * frameProperty = 
												new FrameProperty(PT_IEEE_CF_PARAMETER_SET_MAX_DURATION);
										  $$ = new FrameValueExpression($1, frameProperty, FVO_GET);	
										}
	|	identifier DOT P_CF_PARAMETER_SET_REMAINING			{
										  FrameProperty * frameProperty = 
												new FrameProperty(PT_IEEE_CF_PARAMETER_SET_REMAINING);
										  $$ = new FrameValueExpression($1, frameProperty, FVO_GET);	
										}
	|	identifier DOT P_DS_PARAMETER_SET				{
										  FrameProperty * frameProperty = new FrameProperty(PT_IEEE_DS_PARAMETER_SET);
										  $$ = new FrameValueExpression($1, frameProperty, FVO_GET);	
										}
	|	identifier DOT P_IBSS_PARAMETER_SET				{
										  FrameProperty * frameProperty = new FrameProperty(PT_IEEE_IBSS_PARAMETER_SET);
										  $$ = new FrameValueExpression($1, frameProperty, FVO_GET);	
										}
	|	identifier DOT P_TIM_DTIM_COUNT					{
										  FrameProperty * frameProperty = new FrameProperty(PT_IEEE_TIM_DTIM_COUNT);
										  $$ = new FrameValueExpression($1, frameProperty, FVO_GET);	
										}
	|	identifier DOT P_TIM_DTIM_PERIOD				{
										  FrameProperty * frameProperty = new FrameProperty(PT_IEEE_TIM_DTIM_PERIOD);
										  $$ = new FrameValueExpression($1, frameProperty, FVO_GET);
										}
	|	identifier DOT P_TIM_BITMAP_CONTROL				{
										  FrameProperty * frameProperty = new FrameProperty(PT_IEEE_TIM_BITMAP_CONTROL);
										  $$ = new FrameValueExpression($1, frameProperty, FVO_GET);
										}
	|	identifier DOT P_TIM_PARTIAL_VIRTUAL_BITMAP			{
										  FrameProperty * frameProperty = 
												new FrameProperty(PT_IEEE_TIM_PARTIAL_VIRTUAL_BITMAP);
										  $$ = new FrameValueExpression($1, frameProperty, FVO_GET);
										}
	|	identifier DOT P_COUNTRY_STRING					{
										  FrameProperty * frameProperty = new FrameProperty(PT_IEEE_COUNTRY_STRING);
										  $$ = new FrameValueExpression($1, frameProperty, FVO_GET);
										}
	|	identifier DOT P_COUNTRY_CONSTRAINT ASSIGN exp			{
										  FrameProperty * frameProperty = new FrameProperty(PT_IEEE_COUNTRY_CONSTRAINT);
										  frameProperty -> addValue($5);
										  $$ = new FrameValueExpression($1, frameProperty, FVO_GET);
										}
	|	identifier DOT P_LOCAL_POWER_CONSTRAINT				{
										  FrameProperty * frameProperty = new FrameProperty(PT_IEEE_LOCAL_POWER_CONSTRAINT);
										  $$ = new FrameValueExpression($1, frameProperty, FVO_GET);
										}
	|	identifier DOT P_CHANNEL_SWITCH_MODE				{
										  FrameProperty * frameProperty = new FrameProperty(PT_IEEE_CHANNEL_SWITCH_MODE);
										  $$ = new FrameValueExpression($1, frameProperty, FVO_GET);
										}
	|	identifier DOT P_NEW_CHANNEL_NUMBER				{
										  FrameProperty * frameProperty = new FrameProperty(PT_IEEE_NEW_CHANNEL_NUMBER);
										  $$ = new FrameValueExpression($1, frameProperty, FVO_GET);
										}
	|	identifier DOT P_CHANNEL_SWITCH_COUNT				{
										  FrameProperty * frameProperty = new FrameProperty(PT_IEEE_CHANNEL_SWITCH_COUNT);
										  $$ = new FrameValueExpression($1, frameProperty, FVO_GET);
										}
	|	identifier DOT P_QUIET_COUNT					{
										  FrameProperty * frameProperty = new FrameProperty(PT_IEEE_QUIET_COUNT);
										  $$ = new FrameValueExpression($1, frameProperty, FVO_GET);
										}
	|	identifier DOT P_QUIET_PERIOD					{
										  FrameProperty * frameProperty = new FrameProperty(PT_IEEE_QUIET_PERIOD);
										  $$ = new FrameValueExpression($1, frameProperty, FVO_GET);
										}
	|	identifier DOT P_QUIET_DURATION					{
										  FrameProperty * frameProperty = new FrameProperty(PT_IEEE_QUIET_DURATION);
										  $$ = new FrameValueExpression($1, frameProperty, FVO_GET);
										}
	|	identifier DOT P_QUIET_OFFSET					{
										  FrameProperty * frameProperty = new FrameProperty(PT_IEEE_QUIET_OFFSET);
										  $$ = new FrameValueExpression($1, frameProperty, FVO_GET);
										}
	|	identifier DOT P_TRANSMIT_POWER					{
										  FrameProperty * frameProperty = new FrameProperty(PT_IEEE_TRANSMIT_POWER);
										  $$ = new FrameValueExpression($1, frameProperty, FVO_GET);
										}
	|	identifier DOT P_LINK_MARGIN					{
										  FrameProperty * frameProperty = new FrameProperty(PT_IEEE_LINK_MARGIN);
										  $$ = new FrameValueExpression($1, frameProperty, FVO_GET);
										}
	|	identifier DOT P_ERP						{
										  FrameProperty * frameProperty = new FrameProperty(PT_IEEE_ERP);
										  $$ = new FrameValueExpression($1, frameProperty, FVO_GET);
										}
	|	identifier DOT P_SUPPORTED_RATE ASSIGN exp			{
										  FrameProperty * frameProperty = new FrameProperty(PT_IEEE_SUPPORTED_RATE);
										  frameProperty -> addValue($5);
										  $$ = new FrameValueExpression($1, frameProperty, FVO_GET);	
										}
	|	identifier DOT P_EXTENDED_SUPPORTED_RATE ASSIGN exp		{
										  FrameProperty * frameProperty = new FrameProperty(PT_IEEE_EXTENDED_SUPPORTED_RATE);
										  frameProperty -> addValue($5);
										  $$ = new FrameValueExpression($1, frameProperty, FVO_GET);
										}
	|	identifier DOT P_REASON_CODE					{
										  FrameProperty * frameProperty = new FrameProperty(PT_IEEE_REASON_CODE);
										  $$ = new FrameValueExpression($1, frameProperty, FVO_GET);
										}
	|	identifier DOT P_LISTEN_INTERVAL				{
										  FrameProperty * frameProperty = new FrameProperty(PT_IEEE_LISTEN_INTERVAL);
										  $$ = new FrameValueExpression($1, frameProperty, FVO_GET);
										}
	|	identifier DOT P_CURRENT_AP_ADDRESS				{
										  FrameProperty * frameProperty = new FrameProperty(PT_IEEE_CURRENT_AP_ADDRESS);
										  $$ = new FrameValueExpression($1, frameProperty, FVO_GET);
										}
	|	identifier DOT P_RSN_VERSION					{
										  FrameProperty * frameProperty = new FrameProperty(PT_IEEE_RSN_VERSION);
										  $$ = new FrameValueExpression($1, frameProperty, FVO_GET);
										}
	|	identifier DOT P_GROUP_CIPHER_SUITE				{
										  FrameProperty * frameProperty = new FrameProperty(PT_IEEE_GROUP_CIPHER_SUITE);
										  $$ = new FrameValueExpression($1, frameProperty, FVO_GET);
										}
	|	identifier DOT P_PAIRWISE_CIPHER_SUITE ASSIGN exp		{
										  FrameProperty * frameProperty = new FrameProperty(PT_IEEE_PAIRWISE_CIPHER_SUITE);
										  frameProperty -> addValue($5);
										  $$ = new FrameValueExpression($1, frameProperty, FVO_GET);
										}
	|	identifier DOT P_AUTHENTICATION_SUITE ASSIGN exp		{
										  FrameProperty * frameProperty = new FrameProperty(PT_IEEE_AUTHENTICATION_SUITE);
										  frameProperty -> addValue($5);
										  $$ = new FrameValueExpression($1, frameProperty, FVO_GET);
										}
	|	identifier DOT P_RSN_CAPABILITIES				{
										  FrameProperty * frameProperty = new FrameProperty(PT_IEEE_RSN_CAPABILITIES);
										  $$ = new FrameValueExpression($1, frameProperty, FVO_GET);
										}
	|	identifier DOT P_STATUS_CODE					{
										  FrameProperty * frameProperty = new FrameProperty(PT_IEEE_STATUS_CODE);
										  $$ = new FrameValueExpression($1, frameProperty, FVO_GET);	
										}
	|	identifier DOT P_ASSOCIATION_ID					{
										  FrameProperty * frameProperty = new FrameProperty(PT_IEEE_ASSOCIATION_ID);
										  $$ = new FrameValueExpression($1, frameProperty, FVO_GET);	
										}
	|	identifier DOT P_AUTHENTICATION_ALGORITHM_NUMBER		{
										  FrameProperty * frameProperty = 
												new FrameProperty(PT_IEEE_AUTHENTICATION_ALGORITHM_NUMBER);
										  $$ = new FrameValueExpression($1, frameProperty, FVO_GET);	
										}
	|	identifier DOT P_AUTHENTICATION_SEQUENCE_NUMBER			{
										  FrameProperty * frameProperty = 
												new FrameProperty(PT_IEEE_AUTHENTICATION_SEQUENCE_NUMBER);
										  $$ = new FrameValueExpression($1, frameProperty, FVO_GET);
										}
	|	identifier DOT P_CHALLENGE_TEXT					{
										  FrameProperty * frameProperty = new FrameProperty(PT_IEEE_CHALLENGE_TEXT);
										  $$ = new FrameValueExpression($1, frameProperty, FVO_GET);
										}
	|	identifier DOT P_QOS						{
										  FrameProperty * frameProperty = new FrameProperty(PT_IEEE_QOS);
										  $$ = new FrameValueExpression($1, frameProperty, FVO_GET);
										}
	|	identifier DOT P_ADDRESS1					{
										  FrameProperty * frameProperty = new FrameProperty(PT_IEEE_ADDRESS1);
										  $$ = new FrameValueExpression($1, frameProperty, FVO_GET);	
										}
	|	identifier DOT P_ADDRESS2					{
										  FrameProperty * frameProperty = new FrameProperty(PT_IEEE_ADDRESS2);
										  $$ = new FrameValueExpression($1, frameProperty, FVO_GET);	
										}
	|	identifier DOT P_ADDRESS3					{
										  FrameProperty * frameProperty = new FrameProperty(PT_IEEE_ADDRESS3);
										  $$ = new FrameValueExpression($1, frameProperty, FVO_GET);	
										}
	|	identifier DOT P_ADDRESS4					{
										  FrameProperty * frameProperty = new FrameProperty(PT_IEEE_ADDRESS4);
										  $$ = new FrameValueExpression($1, frameProperty, FVO_GET);	
										}
// ***************************************************************************************************************************************************
// ********************************************************* GET PROPERTY LLC ************************************************************************
// ***************************************************************************************************************************************************
	|	identifier DOT P_DNAP						{
										  FrameProperty * frameProperty = new FrameProperty(PT_LLC_DNAP);
										  $$ = new FrameValueExpression($1, frameProperty, FVO_GET);	
										}
	|	identifier DOT P_SNAP						{
										  FrameProperty * frameProperty = new FrameProperty(PT_LLC_SNAP);
										  $$ = new FrameValueExpression($1, frameProperty, FVO_GET);	
										}
	|	identifier DOT P_CONTROL_FIELD					{
										  FrameProperty * frameProperty = new FrameProperty(PT_LLC_CONTROL_FIELD);
										  $$ = new FrameValueExpression($1, frameProperty, FVO_GET);
										}
	|	identifier DOT P_ORGANIZATION_CODE				{
										  FrameProperty * frameProperty = new FrameProperty(PT_LLC_ORGANIZATION_CODE);
										  $$ = new FrameValueExpression($1, frameProperty, FVO_GET);	
										}
	|	identifier DOT P_LLC_TYPE					{
										  FrameProperty * frameProperty = new FrameProperty(PT_LLC_TYPE);
										  $$ = new FrameValueExpression($1, frameProperty, FVO_GET);	
										}
// ***************************************************************************************************************************************************
// ********************************************************* GET PROPERTY PAYLOAD ********************************************************************
// ***************************************************************************************************************************************************		
	|	identifier DOT P_PAYLOAD_DATA					{
										  FrameProperty * frameProperty = new FrameProperty(PT_PAYLOAD_DATA);
										  $$ = new FrameValueExpression($1, frameProperty, FVO_GET);
										}
	|	identifier DOT P_PAYLOAD_CIPHER					{
										  FrameProperty * frameProperty = new FrameProperty(PT_PAYLOAD_CIPHER);
										  $$ = new FrameValueExpression($1, frameProperty, FVO_GET);
										}
	|	identifier DOT P_PAYLOAD_VECTOR					{
										  FrameProperty * frameProperty = new FrameProperty(PT_PAYLOAD_VECTOR);
										  $$ = new FrameValueExpression($1, frameProperty, FVO_GET);	
										}
	|	identifier DOT P_PAYLOAD_KEY					{
										  FrameProperty * frameProperty = new FrameProperty(PT_PAYLOAD_KEY);
										  $$ = new FrameValueExpression($1, frameProperty, FVO_GET);
										}
	|	identifier DOT P_PAYLOAD_KEY_INDEX				{
										  FrameProperty * frameProperty = new FrameProperty(PT_PAYLOAD_KEY_INDEX);
										  $$ = new FrameValueExpression($1, frameProperty, FVO_GET);
										}
	;				
// ***************************************************************************************************************************************************
// ***************************************************************************************************************************************************
// ***************************************************************************************************************************************************
enter   :       enter ENTER							{ printf("   "); }
	|   
	;

identifier:	STRING								{ $$ = new VariableValueExpression($1); free($1);}
	;

keyword:	keywordinfo							{ 
										  $$ = new KeywordValueExpression(keywordInfo); 
										  keywordInfo.include = keywordInfo.exclude = 0; 
										}
	;

keywordinfo:	keywordList							{ keywordInfo.include = keywordInfo.include | $1; }
	| 	NOT keywordList							{ keywordInfo.exclude = keywordInfo.exclude | $2; }
	|       keywordList PLUS keywordinfo					{ keywordInfo.include = keywordInfo.include | $1; }
	| 	NOT keywordList PLUS keywordinfo				{ keywordInfo.exclude = keywordInfo.exclude | $2; }
	;

numberkeyword:  number								{ $$ = $1; }
	|	QUOAT keywordList QUOAT						{ $$ = new NumberValueExpression($2); }
	;
number:         NUMBER								{ $$ = new NumberValueExpression($1); }
	|       HEX_NUMBER    							{ $$ = new NumberValueExpression(strtoll($1, NULL, 16)); free($1);}
	;

text:		TEXT								{ $$ = new StringValueExpression($1); free($1);}
	;

path:		identifier							{ $$ = $1; }
	|	QUOAT FILEPATH QUOAT						{ $$ = new StringValueExpression($2); free($2); }
	|	text								{ $$ = $1; }
	;

keywordList: K_TURBOCHANNEL							{ $$ = V_TURBO_CHANNEL; }
	|    K_CCKCHANNEL							{ $$ = V_CCK_CHANNEL; }
	|    K_OFDMCHANNEL							{ $$ = V_OFDM_CHANNEL; }
	|    K_GHZ2SPECTCHANNEL							{ $$ = V_GHZ2_SPECTRUM_CHANNEL; }
	|    K_GHZ5SPECTCHANNEL							{ $$ = V_GHZ5_SPECTRUM_CHANNEL; }	
	|    K_ONLYPASSIVESCAN							{ $$ = V_ONLY_PASSIVE_SCAN_ALLOWED; }
	|    K_DYNAMICCCKOFDM							{ $$ = V_DYNAMIC_CCK_OFDM_CHANNEL; }
	|    K_GFSKCHANNEL							{ $$ = V_GFSK_CHANNEL; }
	|    K_SENTRECEIVEDURINGCFP						{ $$ = V_SENT_RECEIVE_DURING_CFP; }
	|    K_SENTRECEIVEWITHSHORTPREAMBLE					{ $$ = V_SENT_RECEIVE_WITH_SHORT_PREAMBLE; }
	|    K_SENTRECEIVEWITHWEPENCRYPRION					{ $$ = V_SENT_RECEIVE_WITH_WEP_ENCRYPTION; }
	|    K_SENTRECEIVEWITHFRAGMENTATION					{ $$ = V_SENT_RECEIVE_WITH_FRAGMENTATION; }
	|    K_FRAMEINSLUDESFCS							{ $$ = V_FRAME_INCLUDES_FCS; }
	|    K_FRAMEHASPADDING80211						{ $$ = V_FRAME_HAS_PADDING_802_11; }
	|    K_FRAMEFAILEDFCSCHECK						{ $$ = V_FRAME_FAILED_FCS_CHECK; }
	|    K_FRAMEUSEDSHORTGUARDINTERVAL					{ $$ = V_FRAME_USED_SHORT_GUARD_INTERVAL; }
	|    K_RXRESERVED							{ $$ = V_RX_RESERVED; }
	|    K_PLCPCRCCHECKFAILED						{ $$ = V_PLCP_CRC_CHECK_FAILED ; }
	|    K_BANDWIDTH							{ $$ = V_BANDWIDTH; }
	|    K_BANDWIDTHMACSFLAGS						{ $$ = V_BANDWIDTH_MCS_FLAGS; }	
	|    K_MCSINDEXKNOWN							{ $$ = V_MCS_INDEX_KNOWN; }	
	|    K_GUARDINTERVAL							{ $$ = V_GUARD_INTERVAL; }
	|    K_HTFORMAT								{ $$ = V_HT_FORMAT; }
	|    K_FECTYPE								{ $$ = V_FEC_TYPE; }
 	|    K_MCSRESERVED							{ $$ = V_MCS_RESERVED; }
	|    K_ESS								{ $$ = V_CI_ESS; }
        |    K_IBSS 								{ $$ = V_CI_IBSS; }
	|    K_CF_POLLABLE							{ $$ = V_CI_CF_POLLABLE; }
	|    K_POLL_REQUEST							{ $$ = V_CI_CF_POLL_REQUEST; }
 	|    K_PRIVACY								{ $$ = V_CI_PRIVACY; }
	|    K_SHORT_PREAMBULE							{ $$ = V_CI_SHORT_PREAMBULE; }
	|    K_PBCC								{ $$ = V_CI_PBCC; }
	|    K_CHANNEL_AGALITY							{ $$ = V_CI_CHANNEL_AGILITY; }
	|    K_SHORT_SLOT_TIME							{ $$ = V_CI_SHORT_SLOT_TIME; }
	|    K_DSSS_OFDM							{ $$ = V_CI_DSSS_OFDM; }
	|    K_NON_ERP_PRESENT							{ $$ = V_ERP_NON_ERP_PRESENT; }
	|    K_USE_PROTECTION							{ $$ = V_ERP_USE_PROTECTION; }
	|    K_BARKER_PREAMBLE							{ $$ = V_ERP_BARKER_PREAMBLE; }
	|    K_GROUP_CIPHER_SUITE						{ $$ = V_GROUP_CIPHER_SUITE; }
 	|    K_WEP_40								{ $$ = V_WEP_40; }
 	|    K_WEP_104								{ $$ = V_WEP_104; }
	|    K_TKIP								{ $$ = V_TKIP; }
	|    K_CCMP 								{ $$ = V_CCMP; }
	|    K_PRESHARED_MASTER_KEY						{ $$ = V_PRESHARED_MASTER_KEY; }
	|    K_PRE_SHARED_KEY							{ $$ = V_PRE_SHARED_KEY; }
	|    K_IPv4								{ $$ = V_LLC_IPv4; }
	|    K_IPv6								{ $$ = V_LLC_IPv6;  }
	|    K_ARP								{ $$ = V_LLC_ARP;  }
        |    K_RARP								{ $$ = V_LLC_RARP;  }	
	|    K_MPLSU								{ $$ = V_LLC_MPLSu;  }
	|    K_MPLSM 								{ $$ = V_LLC_MPLSm;  }
	|    K_JUMBO_FRAME 							{ $$ = V_LLC_JUMBO_FRAME;  }
	|    K_EAPOL 								{ $$ = V_LLC_EAPOL;  }
	|    K_LLDP 								{ $$ = V_LLC_LLDP;  }
	|    K_AARP								{ $$ = V_LLC_AARP;  }
	;

cipher  :    NUMBER								{ $$ = new NumberValueExpression($1); }
	|    QUOAT keywordCipher QUOAT						{ $$ = $2; }
	;

keywordCipher : K_PLAIN_TEXT							{ $$ = new NumberValueExpression(CIPHER_PLAIN_TEXT); }
 	|	K_WEP_40							{ $$ = new NumberValueExpression(CIPHER_WEP_40); }
 	|       K_WEP_104							{ $$ = new NumberValueExpression(CIPHER_WEP_104); }
	|       K_TKIP								{ $$ = new NumberValueExpression(CIPHER_TKIP); }
	|       K_CCMP 								{ $$ = new NumberValueExpression(CIPHER_CCMP); }
	;
%%
// ***************************************************************************************************************************************************
// ***************************************************************************************************************************************************
// ***************************************************************************************************************************************************

/* main
 * Popis: main funkce celeho programu
 * Parametry: argc - pocet parametru
 *            argv - pole obsahujici parametry predavane aplikaci
 *		   - index 1 - nazev aplikace
 *		   - index 2 - rozhrani, na ktere se budou injektovat pakety	
 * Navratova hodnota: informace o uspechu ci neuspechu behu aplikace
 */
int main (int argc, char * argv[]) {
	//initTerminal();

	int state;
	state = STATE_OK;
	keywordInfo.include = 0;
	keywordInfo.exclude = 0;
	if(argc != 2) {
		printf("\t%s\r\n", LANG_APP_BAD_PARAM);		
		printf("\r\n\t%s %s %s\r\n", LANG_APP_HOW_RUN, argv[0], LANG_APP_PARAM);
		fflush(stdout);
		return state = STATE_KO;
	}

	WiFiInterface * interface = new WiFiInterface();
	interpreter.setInterface(interface);
	
	if(!interface -> initWiFiInterface(argv[1])) {	
		printf("\t%s\r\n", LANG_APP_INIT_INTERFACE);
		fflush(stdout);
		return state = STATE_KO;
	}
	
	//ulozime jmeno procesu
	sprintf(procName, "%s", argv[0]);

	printf("\n\t%s\n\r\n", LANG_APP_BEGIN);
	printf("%s %d%s\r\n\r\n", LANG_APP_BREAK_LOOP1, getpid() , LANG_APP_BREAK_LOOP2);
	printf("\r>> ");
	
	while (! at_end) {
		yyparse();
	}
	delete interface;
	return state;
}
