/* Soubor: Help.h
 * Autor: Pavel Svanda
 * Email: svandap@gmail.com
 * Datum vytvoreni: 25.11.2012
 * Popis: deklarace tridy Help, ktera obsahuje napovedu k programu
 */

#ifndef HELP_H
#define HELP_H

#include <stdio.h>
#include "../lang/Lang.h"

class Help {
	public:
		void printHelp();
		void printRadioTapProperty();
		void printIeeeProperty();
		void printLLCProperty();
		void printPayloadProperty();
		void printValue();
};

#endif
