/* Soubor: Help.cpp
 * Autor: Pavel Svanda
 * Email: svandap@gmail.com
 * Datum vytvoreni: 25.11.2012
 * Popis: definice tridy Help, ktera obsahuje napovedu k programu
 */
#include "Help.h"

/* Help::printHelp
 * Popis: funkce vypise napovedu aplikace
 * Parametry: -
 * Navratova hodnota: -
 */
void Help::printHelp() {
	printf("\r\n");
	printf("\t%s\r\n", LANG_HELP_APP_NAME);
	printf("\r\n");

	printf("  %s\r\n", LANG_HELP_NOTE);
	printf("    %s\t - %s\r\n", LANG_HELP_VARIABLE, LANG_HELP_NOTE_VARIABLE);
	printf("    %s\t - %s\r\n", LANG_HELP_PACKET, LANG_HELP_NOTE_PACKET);
	printf("    %s\t - %s\r\n", LANG_HELP_FILTER, LANG_HELP_NOTE_FILTER);
	printf("    %s\t - %s\r\n", LANG_HELP_INTERFACE, LANG_HELP_NOTE_INTERFACE);
	printf("    %s\t - %s\r\n", LANG_HELP_CONFIG, LANG_HELP_NOTE_CONFIG);
	printf("    %s\t - %s\r\n", LANG_HELP_ALIAS_NAME, LANG_HELP_NOTE_ALIAS_NAME);
	printf("    %s\t - %s\r\n", LANG_HELP_PACKET_PATH, LANG_HELP_NOTE_PACKET_PATH);
	printf("    %s - %s\r\n", LANG_HELP_PROGRAM_PATH, LANG_HELP_NOTE_PROGRAM_PATH);
	printf("    %s\t - %s\r\n", LANG_HELP_PROPERTY, LANG_HELP_NOTE_PROPERTY);
	printf("    %s\t - %s\r\n", LANG_HELP_VALUE, LANG_HELP_NOTE_VALUE);
	printf("\r\n");
	printf("  %s\r\n\r\n", LANG_HELP_NOTE_KEYWORD);
	printf("\r\n");

	printf("  %s\r\n", LANG_HELP_COMMANG);
	printf("    help\r\n");	
		printf("\t%s\r\n", LANG_HELP_HELP);
	printf("    help radiotap\r\n");
		printf("\t%s\r\n", LANG_HELP_RADIOTAP_PROPERTY);
	printf("    help ieee\r\n");
		printf("\t%s\r\n", LANG_HELP_IEEE_PROPERTY);
	printf("    help llc\r\n");
		printf("\t%s\r\n", LANG_HELP_LLC_PROPERTY);
	printf("    help payload\r\n");
		printf("\t%s\r\n", LANG_HELP_PAYLOAD_PROPERTY);
	printf("    help value\r\n");
		printf("\t%s\r\n", LANG_HELP_VALUE_LIST);
	printf("    exit\r\n");
		printf("\t%s\r\n", LANG_HELP_EXIT);
	printf("    dump(%s)\r\n", LANG_HELP_VARIABLE);
		printf("\t%s\r\n", LANG_HELP_DUMP_VARIABLE);
	printf("    isend(%s [,count[,time(ms)]])\r\n", LANG_HELP_PACKET);
		printf("\t%s\r\n", LANG_HELP_ISEND_FRAME);
	printf("    isend list\r\n");
		printf("\t%s\r\n", LANG_HELP_ISEND);
	printf("    send(%s)\r\n", LANG_HELP_PACKET);
		printf("\t%s\r\n", LANG_HELP_SEND_FRAME);
	printf("    print(%s)\r\n", LANG_HELP_VARIABLE);
		printf("\t%s\r\n", LANG_HELP_PRINT_VARIABLE);
	printf("    capture(%s)\r\n", LANG_HELP_FILTER);
		printf("\t%s\r\n", LANG_HELP_CAPTURE_FRAME);
	printf("    getiv()\r\n");
		printf("\t%s\r\n", LANG_HELP_GETIV_FRAME);
	printf("    key '%s' '%s'\r\n", LANG_HELP_INTERFACE, LANG_HELP_CONFIG);
		printf("\t%s\r\n", LANG_HELP_GETIV_FRAME);
	printf("    key list\r\n");
		printf("\t%s\r\n", LANG_HELP_KEY_LIST);
	printf("    getGTK()\r\n");
		printf("\t%s\r\n", LANG_HELP_GET_GTK);
	printf("    getPTK()\r\n");
		printf("\t%s\r\n", LANG_HELP_GET_PTK);
	printf("    del(%s)\r\n", LANG_HELP_VARIABLE);
		printf("\t%s\r\n", LANG_HELP_DEL_VARIABLE);
	printf("    list\r\n");
		printf("\t%s\r\n", LANG_HELP_LIST);
	printf("    list all\r\n");
		printf("\t%s\r\n", LANG_HELP_LIST_ALL);
	printf("    alias list\r\n");
		printf("\t%s\r\n", LANG_HELP_ALIAS_LIST);
	printf("    alias %s = \"%s\"\r\n", LANG_HELP_ALIAS_NAME, LANG_HELP_PACKET_PATH);
		printf("\t%s\r\n", LANG_HELP_ALIAS_ADD);
	printf("    alias del(%s)\r\n", LANG_HELP_ALIAS_NAME);
		printf("\t%s\r\n", LANG_HELP_ALIAS_DEL);
	printf("    sleep(%s)\r\n", LANG_HELP_VARIABLE);
		printf("\t%s\r\n", LANG_HELP_SLEEP);
	printf("    msleep(%s)\r\n", LANG_HELP_VARIABLE);
		printf("\t%s\r\n", LANG_HELP_MSLEEP);
	printf("    usleep(%s)\r\n", LANG_HELP_VARIABLE);
		printf("\t%s\r\n", LANG_HELP_USLEEP);
	printf("    time start\r\n");
		printf("\t%s\r\n", LANG_HELP_TIME_START);
	printf("    time\r\n");
		printf("\t%s\r\n", LANG_HELP_TIME_PRINT);
	printf("    break\r\n");
		printf("\t%s\r\n", LANG_HELP_BREAK);
	printf("    load(%s | \"%s\")\r\n",LANG_HELP_ALIAS_NAME, LANG_HELP_PROGRAM_PATH);
		printf("\t%s\r\n", LANG_HELP_LOAD);

	printf("\r\n");

	printf("  %s\r\n", LANG_HELP_PACKET_COMMAND);
	printf("    %s.load(%s | \"%s\")\r\n", LANG_HELP_PACKET, LANG_HELP_ALIAS_NAME, LANG_HELP_PACKET_PATH);
		printf("\t%s\r\n", LANG_HELP_FRAME_LOAD);
	printf("    %s.save(%s | \"%s\")\r\n", LANG_HELP_PACKET, LANG_HELP_ALIAS_NAME, LANG_HELP_PACKET_PATH);
		printf("\t%s\r\n", LANG_HELP_FRAME_SAVE);
	printf("    %s = %s( [%s = %s]* )\r\n", LANG_HELP_PACKET, LANG_HELP_RADIOTAP, LANG_HELP_PROPERTY, LANG_HELP_VALUE);
	printf("    %s = %s( [%s = %s]* )\r\n", LANG_HELP_PACKET, LANG_HELP_IEEE, LANG_HELP_PROPERTY, LANG_HELP_VALUE);
	printf("    %s = %s( [%s = %s]* ) / %s( [%s = %s]* )\r\n", LANG_HELP_PACKET, LANG_HELP_RADIOTAP, LANG_HELP_PROPERTY, LANG_HELP_VALUE, LANG_HELP_IEEE, LANG_HELP_PROPERTY, LANG_HELP_VALUE);
		printf("\t%s\r\n", LANG_HELP_PACKET_HEADER);
	printf("    %s\r\n", LANG_HELP_PACKET);
		printf("\t%s\r\n", LANG_HELP_PRINT_FRAME);
	printf("    %s.%s\r\n", LANG_HELP_PACKET, LANG_HELP_PROPERTY);
		printf("\t%s\r\n", LANG_HELP_PACKET_GET_PROPERTY);
	printf("    %s.%s = %s\r\n", LANG_HELP_PACKET, LANG_HELP_PROPERTY, LANG_HELP_VALUE);
		printf("\t%s\r\n", LANG_HELP_PACKET_SET_PROPERTY);
	printf("    del(%s.%s)\r\n", LANG_HELP_PACKET, LANG_HELP_PROPERTY);
		printf("\t%s\r\n", LANG_HELP_PACKET_DEL_PROPERTY);
	printf("\r\n");
}

/* Help::printRadioTapProperty
 * Popis: funkce vypise seznam vlastnosti, ktere lze nastavit u RadioTap hlavicky
 * Parametry: -
 * Navratova hodnota: -
 */
void Help::printRadioTapProperty() {
	printf("\r\n");
	printf("\t%s\r\n", LANG_HELP_APP_NAME);
	printf("\r\n");

	printf("  %s\r\n", LANG_HELP_RADIOTAP_HEADER);

	printf("\r\n");
	printf("    %s\r\n", LANG_HELP_PACKET_RADIOTAP_IEEE);
	printf("    %s\r\n", LANG_HELP_PACKET_RADIOTAP_IEEE2);
	printf("\r\n");

	printf("    %s\r\n", LANG_HELP_RADIOTAP_VERSION);        
	printf("    %s\r\n", LANG_HELP_RADIOTAP_PAD);		  
	printf("    %s\r\n", LANG_HELP_RADIOTAP_LEN);            
	printf("    %s\r\n", LANG_HELP_RADIOTAP_ANTENNA);	  
	printf("    %s\r\n", LANG_HELP_RADIOTAP_ANTENNA_NOISE);  
	printf("    %s\r\n", LANG_HELP_RADIOTAP_ANTENNA_SIGNAL); 
	printf("    %s\r\n", LANG_HELP_RADIOTAP_CHANNEL);	
		printf("\t%s\r\n", LANG_HELP_RADIOTAP_CHANNEL_KW);   
	printf("    %s\r\n", LANG_HELP_RADIOTAP_FHSS_HOP_SET);	
	printf("    %s\r\n", LANG_HELP_RADIOTAP_FLAGS);	
		printf("\t%s\r\n", LANG_HELP_RADIOTAP_FLAGS_KW);	
		printf("\t%s\r\n", LANG_HELP_RADIOTAP_FLAGS_KW2);   
	printf("    %s\r\n", LANG_HELP_RADIOTAP_LOCK_QUALITY); 
	printf("    %s\r\n", LANG_HELP_RADIOTAP_MCS);		
		printf("\t%s\r\n", LANG_HELP_RADIOTAP_MCS_KW);	
	printf("    %s\r\n", LANG_HELP_RADIOTAP_RX_FLAGS);	
		printf("\t%s\r\n", LANG_HELP_RADIOTAP_RX_FLAGS_KW);	
	printf("    %s\r\n", LANG_HELP_RADIOTAP_RATE);		
	printf("    %s\r\n", LANG_HELP_RADIOTAP_TSFT);
	printf("    %s\r\n", LANG_HELP_RADIOTAP_TX_ATTENUATION);
	printf("    %s\r\n", LANG_HELP_RADIOTAP_VENDOR_NMS_OUI);
	printf("    %s\r\n", LANG_HELP_RADIOTAP_DB_TX_ATTENUATION);
	printf("    %s\r\n", LANG_HELP_RADIOTAP_DB_ANTENNA_NOISE);
	printf("    %s\r\n", LANG_HELP_RADIOTAP_DB_ANTENNA_SIGNAL);
	printf("    %s\r\n", LANG_HELP_RADIOTAP_DBM_TX_POWER);
	printf("    %s\r\n", LANG_HELP_RADIOTAP_RTS_RETRIES);
	printf("    %s\r\n", LANG_HELP_RADIOTAP_DATA_RETRIES);

	printf("\r\n");
	printf("    %s\r\n", LANG_HELP_VALUE_TYPE_COMMAND);

	printf("\r\n");
}

/* Help::printIeeeProperty
 * Popis: funkce vypise seznam vlastnosti, ktere lze nastavit u Ieee hlavicky
 * Parametry: -
 * Navratova hodnota: -
 */
void Help::printIeeeProperty() {
	printf("\r\n");
	printf("\t%s\r\n", LANG_HELP_APP_NAME);
	printf("\r\n");

	printf("  %s\r\n", LANG_HELP_IEEE_HEADER);

	printf("\r\n");
	printf("    %s\r\n", LANG_HELP_PACKET_RADIOTAP_IEEE);
	printf("    %s\r\n", LANG_HELP_PACKET_RADIOTAP_IEEE2);
	printf("\r\n");

	printf("    %s\r\n", LANG_HELP_IEEE_FRAME_CONTROL);
	printf("    %s\r\n", LANG_HELP_IEEE_PROTOCOL);
	printf("    %s\r\n", LANG_HELP_IEEE_TYPE);
		printf("\t%s\r\n", LANG_HELP_IEEE_TYPE_KW);
		printf("\t%s\r\n", LANG_HELP_IEEE_TYPE_KW2);
	printf("    %s\r\n", LANG_HELP_IEEE_SUB_TYPE);
	printf("    %s\r\n", LANG_HELP_IEEE_TO_DS);
	printf("    %s\r\n", LANG_HELP_IEEE_FROM_DS);
	printf("    %s\r\n", LANG_HELP_IEEE_MORE_FRAG);
	printf("    %s\r\n", LANG_HELP_IEEE_RETRY);
	printf("    %s\r\n", LANG_HELP_IEEE_PWR_MGMT);
	printf("    %s\r\n", LANG_HELP_IEEE_MORE_DATA);
	printf("    %s\r\n", LANG_HELP_IEEE_PROTECTED_FRAME);
	printf("    %s\r\n", LANG_HELP_IEEE_ORDER);
	printf("    %s\r\n", LANG_HELP_IEEE_DURATION);
	printf("    %s\r\n", LANG_HELP_IEEE_AID);
	printf("    %s\r\n", LANG_HELP_IEEE_BSSID);
	printf("    %s\r\n", LANG_HELP_IEEE_REC_ADDRESS);
	printf("    %s\r\n", LANG_HELP_IEEE_TRA_ADDRESS);
	printf("    %s\r\n", LANG_HELP_IEEE_DA);
	printf("    %s\r\n", LANG_HELP_IEEE_SA);
	printf("    %s\r\n", LANG_HELP_IEEE_SEQ_CTL);
	printf("    %s\r\n", LANG_HELP_IEEE_SEQ);
	printf("    %s\r\n", LANG_HELP_IEEE_FRAG);
	printf("    %s\r\n", LANG_HELP_IEEE_TIMESTAMP);
	printf("    %s\r\n", LANG_HELP_IEEE_BEACON_INTERVAL);
	printf("    %s\r\n", LANG_HELP_IEEE_CAPABILITY_INFO);
		printf("\t%s\r\n", LANG_HELP_IEEE_CAPABILITY_INFO_KW);
		printf("\t%s\r\n", LANG_HELP_IEEE_CAPABILITY_INFO_KW2);
	printf("    %s\r\n", LANG_HELP_IEEE_SSID);
	printf("    %s\r\n", LANG_HELP_IEEE_FH_PARAMETER_SET);
		printf("\t%s\r\n", LANG_HELP_IEEE_FH_PARAMETER_SET2);
	printf("    %s\r\n", LANG_HELP_IEEE_DS_PARAMETER_SET);
	printf("    %s\r\n", LANG_HELP_IEEE_CF_PARAMETER_SET);
		printf("\t%s\r\n", LANG_HELP_IEEE_CF_PARAMETER_SET2);
	printf("    %s\r\n", LANG_HELP_IEEE_IBSS_PARAMETER_SET);
	printf("    %s\r\n", LANG_HELP_IEEE_TIM);
	printf("    %s\r\n", LANG_HELP_IEEE_TIM2);
	printf("    %s\r\n", LANG_HELP_IEEE_COUNTRY);
	printf("    %s\r\n", LANG_HELP_IEEE_LOCAL_POWER_CONST);
	printf("    %s\r\n", LANG_HELP_IEEE_CHANNEL_SWITCH);
	printf("    %s\r\n", LANG_HELP_IEEE_QUIET);
	printf("    %s\r\n", LANG_HELP_IEEE_TRANSMIT_POWER);
	printf("    %s\r\n", LANG_HELP_IEEE_LING_MARGIN);
	printf("    %s\r\n", LANG_HELP_IEEE_ERP);
	printf("    %s\r\n", LANG_HELP_IEEE_SUPPOERTED_RATE);
	printf("    %s\r\n", LANG_HELP_IEEE_E_SUPPOERTED_RATE);
	printf("    %s\r\n", LANG_HELP_IEEE_REASON_CODE);
	printf("    %s\r\n", LANG_HELP_IEEE_LISTEN_INTERVAL);
	printf("    %s\r\n", LANG_HELP_IEEE_CURRENT_AP_ADDRESS);
	printf("    %s\r\n", LANG_HELP_IEEE_RSN);
		printf("\t%s\r\n", LANG_HELP_IEEE_RSN2);
		printf("\t%s\r\n", LANG_HELP_IEEE_RSN_KW);
	printf("    %s\r\n", LANG_HELP_IEEE_STATUS_CODE);
	printf("    %s\r\n", LANG_HELP_IEEE_ASSOCIATION_ID);
	printf("    %s\r\n", LANG_HELP_IEEE_AUTH_ALG_NUM);
	printf("    %s\r\n", LANG_HELP_IEEE_AUTH_SEQ_NUM);
	printf("    %s\r\n", LANG_HELP_IEEE_CHALLENGE_TEXT);
	printf("    %s\r\n", LANG_HELP_IEEE_QOS);
	printf("    %s\r\n", LANG_HELP_IEEE_ADDR1);
	printf("    %s\r\n", LANG_HELP_IEEE_ADDR2);
	printf("    %s\r\n", LANG_HELP_IEEE_ADDR3);
	printf("    %s\r\n", LANG_HELP_IEEE_ADDR4);

	printf("\r\n");
	printf("    %s\r\n", LANG_HELP_VALUE_TYPE_COMMAND);

	printf("\r\n");
}

/* Help::printLLCPropery
 * Popis: funkce vypise seznam vlastnosti, ktere lze nastavit u payloadu
 * Parametry: -
 * Navratova hodnota: -
 */
void Help::printLLCProperty() {
	printf("\r\n");
	printf("\t%s\r\n", LANG_HELP_APP_NAME);
	printf("\r\n");

	printf("  %s\r\n", LANG_HELP_LLC_VALUE);

	printf("\r\n");
	printf("    %s\r\n", LANG_HELP_PACKET_RADIOTAP_IEEE);
	printf("    %s\r\n", LANG_HELP_PACKET_RADIOTAP_IEEE2);
	printf("\r\n");

	printf("    %s\r\n", LANG_HELP_LLC_DNAP);
	printf("    %s\r\n", LANG_HELP_LLC_SNAP);
	printf("    %s\r\n", LANG_HELP_LLC_CONTROL_FIELD);
	printf("    %s\r\n", LANG_HELP_LLC_ORGANIZATION_CODE);
	printf("    %s\r\n", LANG_HELP_LLC_TYPE);
        	printf("\t%s\r\n", LANG_HELP_LLC_TYPE_KW);

	printf("\r\n");
	printf("    %s\r\n", LANG_HELP_VALUE_TYPE_COMMAND);

	printf("\r\n");
}

/* Help::printPayloadPropery
 * Popis: funkce vypise seznam vlastnosti, ktere lze nastavit u payloadu
 * Parametry: -
 * Navratova hodnota: -
 */
void Help::printPayloadProperty() {
	printf("\r\n");
	printf("\t%s\r\n", LANG_HELP_APP_NAME);
	printf("\r\n");

	printf("  %s\r\n", LANG_HELP_PAYLOAD_VALUE);

	printf("    %s\r\n", LANG_HELP_PAYLOAD_CIPHER);
	printf("    \t\t   %s\r\n", LANG_HELP_PAYLOAD_CIPHER_KW);
	printf("    %s\r\n", LANG_HELP_PAYLOAD_IV);
	printf("    %s\r\n", LANG_HELP_PAYLOAD_KEY);
	printf("    %s\r\n", LANG_HELP_PAYLOAD_KEY_INDEX);
	printf("    %s\r\n", LANG_HELP_PAYLOAD_DATA);

	printf("\r\n");
	printf("    %s\r\n", LANG_HELP_VALUE_TYPE_COMMAND);

	printf("\r\n");
}

/* Help::printValue
 * Popis: funkce vypise seznam hodnot, ktere lze nastavovat
 * Parametry: -
 * Navratova hodnota: -
 */
void Help::printValue() {
	printf("\r\n");
	printf("\t%s\r\n", LANG_HELP_APP_NAME);
	printf("\r\n");

	printf("  %s\r\n", LANG_HELP_VALUE_TYPE);

	printf("    %s\t - %s\r\n", LANG_HELP_VALUE_NUMBER, LANG_HELP_VALUE_NUMBER_NOTE);
	printf("    %s\t - %s\r\n", LANG_HELP_VALUE_KEYWORD, LANG_HELP_VALUE_KEYWORD_NOTE);
	printf("    %s\t - %s\r\n", LANG_HELP_VALUE_KEYLIST, LANG_HELP_VALUE_KEYLIST_NOTE);
	printf("    \t\t   %s\r\n", LANG_HELP_KEYLIST_REMOVE);
	printf("    %s\t - %s\r\n", LANG_HELP_VALUE_ADDRESS, LANG_HELP_VALUE_ADDRESS_NOTE);
	printf("    %s\t - %s\r\n", LANG_HELP_VALUE_STRING, LANG_HELP_VALUE_STRING_NOTE);
	printf("    %s\t - %s\r\n", LANG_HELP_VALUE_HEX_DATA, LANG_HELP_VALUE_HEX_DATA_NOTE);
	printf("    %s\t\t - %s\r\n", LANG_HELP_VALUE_OUI, LANG_HELP_VALUE_OUI_NOTE);
	printf("    %s\t - %s\r\n", LANG_HELP_VALUE_CIET, LANG_HELP_VALUE_CIET_NOTE);
	printf("    \t\t   %s\r\n", LANG_HELP_VALUE_CIET_NOTE2);
	printf("    %s\t - %s\r\n", LANG_HELP_VALUE_RATE, LANG_HELP_VALUE_RATE_NOTE);
	printf("    %s - %s\r\n", LANG_HELP_VALUE_G_CIPHER, LANG_HELP_VALUE_G_CIPHER_NOTE);
	printf("    %s\t - %s\r\n", LANG_HELP_VALUE_CIPHER, LANG_HELP_VALUE_CIPHER_NOTE);

	printf("\r\n");
	printf("  %s\r\n", LANG_HELP_VALUE_NOTE);
	printf("  %s\r\n", LANG_HELP_VALUE_NOTE2);
	printf("\r\n");
}
