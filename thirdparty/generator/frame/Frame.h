/* Soubor: Frame.h
 * Autor: Pavel Svanda
 * Email: svandap@gmail.com
 * Datum vytvoreni: 3.7.2012
 * Popis: deklarace tridy Frame, ktera slouzi jako reprezentace ramce posilaneho na wifi rozhrani
 */
#ifndef FRAME_H
#define FRAME_H

#include "../header/RadioTapHeader.h"
#include "../header/IeeeHeader.h"
#include "../header/LLCHeader.h"
#include "../payload/Payload.h"
#include "Crc32.h"

#define LENGTH_IEEE_HEADER		1000
#define MAX_DATA_LENGTH 		5000 //potreba zmenit take ve WiFiInterface.h

class Frame {
	private:
		bool isSetRadioTapHeader;
		bool isSetIeeeHeader;	
		bool isSetLLCHeader;	
		bool isSetPayload;
		bool isChange;

		uint8_t saveGenerateFrame[MAX_DATA_LENGTH];
		int lengthSaveGenerateFrame;
		
		uint32_t crcSum;
		bool _generateCrcSum;

		uint32_t generateCrcSum();

	public:
		RadioTapHeader 	radioTapHeader;
		IeeeHeader 	ieeeHeader;
		LLCHeader 	llcHeader;
		Payload    	payload;

		Frame();
	       ~Frame();

		uint32_t generateFrame(uint8_t * frame);

		void setProperty(char * property, int val);

		void setRadioTapHeader(RadioTapHeader radioTapHeader);
		RadioTapHeader getRadioTapHeader();
		void includeRadioTapHeader(bool include);

		void setIeeeHeader(IeeeHeader ieeeHeader);
		IeeeHeader getIeeeHeader();
		void includeIeeeHeader(bool include);

		void setLLCHeader(LLCHeader llcHeader);
		LLCHeader getLLCHeader();
		void includeLLCHeader(bool include);

		void setPayload(Payload payload);
		Payload getPayload();
		void includePayload(bool include);

		void setCrcSum(uint32_t crcSum);
		uint32_t getCrcSum();
		
		void setGenerateCrcSum(bool _generateCrcSum);
		bool getGenerateCrcSum();

		void printFrame();	
		void clearFrame();
		void dumpFrame();
		void strFrame();

		bool loadFrame(char * path);
		bool saveFrame(char * path);

		bool getIsSetRadioTapHeader();
		bool getIsSetIeeeHeader();	
		bool getIsSetLLCHeader();	
		bool getIsSetPayload();

		static int parseIV(int lengthFrame, uint8_t * frame, uint8_t * iv);		
};

#endif
