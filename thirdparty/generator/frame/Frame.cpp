/* Soubor: Frame.cpp
 * Autor: Pavel Svanda
 * Email: svandap@gmail.com
 * Datum vytvoreni: 3.7.2012
 * Popis: definice tridy Frame, ktera slouzi jako reprezentace ramce posilaneho na wifi rozhrani
 */

#include "Frame.h"

/* Frame::Frame
 * Popis: konstruktor
 * Parametry: -
 * Navratova hodnota: -
 */
Frame::Frame() {
	this -> isSetRadioTapHeader = false;
	this -> isSetIeeeHeader = false;
	this -> isSetLLCHeader = false;		
	this -> isSetPayload = false;
	this -> _generateCrcSum = true;
	this -> crcSum = 0;
	this -> isChange = true;
}

/* Frame::Frame
 * Popis: destruktor
 * Parametry: -
 * Navratova hodnota: -
 */
Frame::~Frame() {
}

/* Frame::setRadioTapHeader
 * Popis: funkce provede nastaveni radioTapHlavicky
 * Parametry: radioTapHeader - promenna radioTapHlavicky
 * Navratova hodnota: -
 */
void Frame::setRadioTapHeader(RadioTapHeader radioTapHeader) {
	this -> isChange = true;
	this -> isSetRadioTapHeader = true;
	this -> radioTapHeader = radioTapHeader;
}

/* Frame::getRadiotapHeader
 * Popis: funkce provede navraceni promenne radioTapHeader
 * Parametry: -
 * Navratova hodnota: promenna radioTap hlavicky
 */
RadioTapHeader Frame::getRadioTapHeader() {
	return this -> radioTapHeader;
}

/* Frame::includeRadioTapHeader
 * Popis: funkce provede zahrnuti (nebo odebrani) radioTap hlavicky do ramce
 * Parametry: include - informaci, zdali hlavicku zahrnuje nebo odebira
 * Navratova hodnota: -
 */
void Frame::includeRadioTapHeader(bool include) {
	this -> isChange = true;
	this -> isSetRadioTapHeader = include;
}

/* Frame::setIeeeHeader
 * Popis: funkce provede nastaveni ieeeHeader
 * Parametry: ieeeHeader - promenna ieeeHeader
 * Navratova hodnota: -
 */
void Frame::setIeeeHeader(IeeeHeader ieeeHeader) {
	this -> isChange = true;
	this -> isSetIeeeHeader = true;	
	this -> ieeeHeader = ieeeHeader;
}

/* Frame::getIeeeHeader
 * Popis: funkce provede navraceni promenne ieeeHlavicky
 * Parametry: -
 * Navratova hodnota: promenna ieeeHeader
 */
IeeeHeader Frame::getIeeeHeader() {
	return this -> ieeeHeader;
}

/* Frame::includeIeeeHeader
 * Popis: funkce provede zahrnuti (nebo odebrani) ieee hlavicky do ramce
 * Parametry: include - informaci, zdali hlavicku zahrnuje nebo odebira
 * Navratova hodnota: -
 */
void Frame::includeIeeeHeader(bool include) {
	this -> isChange = true;
	this -> isSetIeeeHeader = include;
}

/* Frame::setLLCHeader
 * Popis: funkce provede nastaveni llcHlavicky
 * Parametry: LLCHeader - promenna llcHlavicky
 * Navratova hodnota: -
 */
void Frame::setLLCHeader(LLCHeader llcHeader) {
	this -> isChange = true;
	this -> isSetLLCHeader = true;
	this -> llcHeader = llcHeader;
}

/* Frame::getLLCHeader
 * Popis: funkce provede navraceni promenne llcHeader
 * Parametry: -
 * Navratova hodnota: promenna llc hlavicky
 */
LLCHeader Frame::getLLCHeader() {
	return this -> llcHeader;
}

/* Frame::includeLLCHeader
 * Popis: funkce provede zahrnuti (nebo odebrani) llc hlavicky do ramce
 * Parametry: include - informaci, zdali hlavicku zahrnuje nebo odebira
 * Navratova hodnota: -
 */
void Frame::includeLLCHeader(bool include) {
	this -> isChange = true;
	if(!include) this -> llcHeader.deleteHeader();

	this -> isSetLLCHeader = include;
}

/* Frame::setPayload
 * Popis: funkce provede nastaveni payload
 * Parametry: payload - promenna payload
 * Navratova hodnota: -
 */
void Frame::setPayload(Payload payload) {
	this -> isChange = true;
	this -> payload = payload;
}

/* Frame::getPayload
 * Popis: funkce provede navraceni promenne payload
 * Parametry: -
 * Navratova hodnota: promenna payload
 */
Payload Frame::getPayload() {
	return this -> payload;
}

/* Frame::includePayload
 * Popis: funkce provede zahrnuti (nebo odebrani) payloadu do ramce
 * Parametry: include - informaci, zdali payload zahrnuje nebo odebira
 * Navratova hodnota: -
 */
void Frame::includePayload(bool include) {
	this -> isChange = true;
	this -> isSetPayload = include;
}

/* Frame::setCrcSum
 * Popis: funkce provede nastaveni vnitrni promenne na hodnotu zadaneho kontrolniho souctu
 * Parametry: crcSum - hodnota konrolniho souctu
 * Navratova hodnota: -
 */
void Frame::setCrcSum(uint32_t crcSum) {
	this -> isChange = true;
	this -> crcSum = crcSum;
}

/* Frame::getCrcSum
 * Popis: funkce navrati hodnotu kontrolniho souctu v zavisloti na nastaveni priznaku, zdali se ma generovat kontrolni soucet
 * Parametry: -
 * Navratova hodnota: hodnota kontrolniho souctu
 */
uint32_t Frame::getCrcSum() {
	if(this -> _generateCrcSum) {
		return this -> generateCrcSum();
	} else {
		return this -> crcSum;
	}
}

/* Ramec::setGenerovatCrcSoucet
 * Popis: funcke provede nastaveni priznaku rikajici, zdali se ma CRC soucet generovat nebo pouzit zadany uzivatelem
 * Parametry: _generateCrcSum - priznak, zdali mam generovat CRC soucet (true - generovat, false - pouzit zadany uzivatelem)
 * Navratova hodnota: -
 */		
void Frame::setGenerateCrcSum(bool _generateCrcSum) {
	this -> _generateCrcSum = _generateCrcSum;
}

/* Frame::getGenerateCrcSum
 * Popis: funkce provede navraceni priznaku rikajici, zdali se ma CRC soucet generovat nebo pouzit zadany uzivatelem
 * Parametry: -
 * Navratova hodnota: priznak, zdali se ma generovat CRC soucet (true - generovat, false - pouzit zadany uzivatelem)
 */
bool Frame::getGenerateCrcSum() {
	return this -> _generateCrcSum;
}

/* Frame::generateCrcSum
 * Popis: funkce provede vygenerovani CRC kontrolniho souctu
 * Parametry: -
 * Navratova hodnota: hodnota CRC kontrolniho souctu
 */
uint32_t Frame::generateCrcSum() {
	uint8_t llcHeader[LENGTH_LLC_HEADER];
	int lengthLLCheader = 0;

	Crc32 checkSum;	
	uint8_t frame [LENGTH_IEEE_HEADER + LENGTH_PAYLOAD_DATA];
	uint8_t * pFrame = frame;

	uint32_t lengthIeeeHeader = this -> ieeeHeader.generateHeader(pFrame);
	pFrame += lengthIeeeHeader;

	uint32_t lengthPayload = 0;
	if(this -> ieeeHeader.getType() == T_DATA_FRAME) {
		if(this -> isSetLLCHeader) {
			lengthLLCheader += this -> llcHeader.generateHeader(llcHeader);
		}
		lengthPayload = this -> payload.generatePayload(pFrame, lengthIeeeHeader, lengthLLCheader, llcHeader);
	}

	checkSum.addData(frame, lengthIeeeHeader + lengthPayload);	
	this -> crcSum = checkSum.getCrc32();
	return this -> crcSum;
}

/* Frame::generateFrame
 * Popis: funkce provede vygenerovani ramce dle zadanych dat a ulozi jej na adresu danou ukazatelem ramec
 * Parametry: frame - ukazatel do pameti, kam se bude ukladat vygenerovany ramec
 * Navratova hodnota: delka vygenerovaneho ramce
 */
uint32_t Frame::generateFrame(uint8_t * frame) {

	//pokud nebyl ramec zmenen, tak jej nactem z cache
	if(!this -> isChange) {
		Utilities::memcpy8(frame, this -> saveGenerateFrame, this -> lengthSaveGenerateFrame, LITTLE_ENDIAN_ORDER);
		return this -> lengthSaveGenerateFrame;
	}

	uint8_t llcHeader[LENGTH_LLC_HEADER];
	int lengthLLCheader = 0;
	uint8_t * beginIeee = frame;
	uint8_t * beginPayload = frame;
	uint8_t * generateFrame = frame;

	//vygeneruji RadioTap hlavicku
	beginIeee += this -> radioTapHeader.generateHeader(frame);
	//vygeneruji Ieee hlavicku
	beginPayload = beginIeee;
	beginPayload += this -> ieeeHeader.generateHeader(beginIeee);

	
	//vygeneruji payload, pokud se jedna o datovy ramec
	generateFrame = beginPayload;
	if(this -> ieeeHeader.getType() == T_DATA_FRAME) {
		if(this -> isSetLLCHeader) {
			lengthLLCheader += this -> llcHeader.generateHeader(llcHeader);
		}
		generateFrame += this -> payload.generatePayload(beginPayload, beginPayload - beginIeee, lengthLLCheader, llcHeader);
	}
	
	/*pokud je nastaven CRC kontrolni soucet, tak jej pridam*/
	if(this -> radioTapHeader.getBitPresent(FLAGS) && this -> radioTapHeader.getBitFlags(FRAME_INCLUDES_FCS)) {
		uint32_t sum;
		if(_generateCrcSum) {
			sum = this -> generateCrcSum();
		} else {
			sum = this -> crcSum;
		}
		Utilities::memcpy32(generateFrame, &sum, 1, LITTLE_ENDIAN_ORDER);
		generateFrame += sizeof(uint32_t);
	}

	//provedeme cache vygenerovaneho ramce
	this -> lengthSaveGenerateFrame = generateFrame - frame;
	Utilities::memcpy8(this -> saveGenerateFrame, frame, this -> lengthSaveGenerateFrame, LITTLE_ENDIAN_ORDER);
	this -> isChange = false;

	return  generateFrame - frame;
}

/* Frame::printFrame
 * Popis: funkce provede vytisknuti ramce
 * Parametry: -
 * Navratova hodnota: -
 */
void Frame::printFrame() {
	if(this -> isSetRadioTapHeader)
		this -> radioTapHeader.printHeader();
	if(this -> isSetIeeeHeader)
		this -> ieeeHeader.printHeader();
	if(this -> isSetLLCHeader)
		this -> llcHeader.printHeader();
	if(this -> isSetPayload) {
		printf("%s\r\n", LANG_FRAME_PAYLOAD);
		this -> payload.printPayload();
	}
	if(this -> radioTapHeader.getBitPresent(FLAGS) && this -> radioTapHeader.getBitFlags(FRAME_INCLUDES_FCS)) {
		printf("%s\r\n", LANG_FRAME_CRC);
		printf("\t0x%08x\r\n", this -> generateCrcSum());
	}
}

/* Frame::clearFrame
 * Popis: funkce provede vycisteni ramce - nastavi na default hodnoty
 * Parametry: -
 * Navratova hodnota: -
 */
void Frame::clearFrame() {
	RadioTapHeader newRadioTapHeader;
	IeeeHeader newIeeeHeader;
	LLCHeader newLLCHeader;
	Payload newPayload;
	this -> radioTapHeader = newRadioTapHeader;
	this -> ieeeHeader = newIeeeHeader;
	this -> llcHeader = newLLCHeader;
	this -> payload = newPayload;
	this -> isSetRadioTapHeader = false;
	this -> isSetIeeeHeader = false;	
	this -> isSetLLCHeader = false;
	this -> isSetPayload = false;
	this -> _generateCrcSum = true;
	this -> crcSum = 0;
	this -> isChange = true;
}

/* Frame::dumpFrame
 * Popis: funkce provede vypsani ramce v hexa
 * Parametry: -
 * Navratova hodnota: -
 */
void Frame::dumpFrame() {
	uint8_t data [MAX_DATA_LENGTH];
	int sizeFrame = this -> generateFrame(data);
	Utilities::hexDump(NULL, data, sizeFrame);
}

/* Frame::strFrame
 * Popis: funkce provede vypsani ramce v hexa
 * Parametry: -
 * Navratova hodnota: -
 */
void Frame::strFrame() {
	uint8_t data [MAX_DATA_LENGTH];
	int sizeFrame = this -> generateFrame(data);
	Utilities::hexStr(NULL, data, sizeFrame);
}

/* Frame::loadFrame
 * Popis: funkce provede nacteni ramce z xml souboru
 * Parametry: path - cesta k souboru
 * Navratova hodnota: informace, zdali se podarilo nacteni souboru
 */
bool Frame::loadFrame(char * path) {
	this -> clearFrame();

	TiXmlDocument loadFile( path );
	bool loadOkay = loadFile.LoadFile();
	if(!loadOkay) {
		return false;
	}
	TiXmlHandle hLoadFile(&loadFile);
	//nactu ramec
	TiXmlElement * frameElement = hLoadFile.FirstChild( "frame" ).Element();
	if(frameElement) {
		TiXmlHandle hFrameElement(frameElement);

		//nactu radioTapHlavicku
		TiXmlElement * radioTapHeaderElement = hFrameElement.FirstChild( "radioTapHeader" ).Element();
		if(radioTapHeaderElement) { 
			this -> radioTapHeader.loadHeader(radioTapHeaderElement);
			radioTapHeaderElement -> QueryBoolAttribute("isPresent", &this -> isSetRadioTapHeader);
			radioTapHeaderElement -> QueryBoolAttribute("generateCrcSum", &this -> _generateCrcSum);
		}

		//nactu ieeeHlavicku
		TiXmlElement * ieeeHeaderElement = hFrameElement.FirstChild( "ieeeHeader" ).Element();
		if(ieeeHeaderElement) {
			this -> ieeeHeader.loadHeader(ieeeHeaderElement);
			ieeeHeaderElement -> QueryBoolAttribute("isPresent", &this -> isSetIeeeHeader);
		}

		//nactu llcHlavicku
		TiXmlElement * llcHeaderElement = hFrameElement.FirstChild( "llcHeader" ).Element();
		if(llcHeaderElement) {
			this -> llcHeader.loadHeader(llcHeaderElement);
			llcHeaderElement -> QueryBoolAttribute("isPresent", &this -> isSetLLCHeader);
		}

		//nactu payload	
		TiXmlElement * payloadElement = hFrameElement.FirstChild( "payload" ).Element();
		if(payloadElement) {
			this -> payload.loadPayload(payloadElement);
			payloadElement -> QueryBoolAttribute("isPresent", &this -> isSetPayload);
		}
	} else {
		return false;
 	}

	return true;
}

/* Frame::saveFrame
 * Popis: funkce provede ulozeni ramce do xml souboru
 * Parametry: path - cesta k souboru
 * Navratova hodnota: informace, zdali se podarilo ulozeni souboru
 */
bool Frame::saveFrame(char * path) {
	TiXmlDocument saveFile;
	TiXmlDeclaration * decl = new TiXmlDeclaration( "1.0", "", "" );
	saveFile.LinkEndChild( decl );
	
	//ulozime promenne ramce
	//ramec
	TiXmlElement * frameElement = new TiXmlElement( "frame" );
	saveFile.LinkEndChild( frameElement );  

	//ulozime polozky radioTapHlavicky
	TiXmlElement * radioTapHeaderElement = this -> radioTapHeader.saveHeader();
	radioTapHeaderElement -> SetAttribute("isPresent", this -> isSetRadioTapHeader);
	radioTapHeaderElement -> SetAttribute("generateCrcSum", this -> _generateCrcSum);
	frameElement -> LinkEndChild( radioTapHeaderElement );

	//ulozime polozky ieeeHlavicky
	TiXmlElement * ieeeHeaderElement = this -> ieeeHeader.saveHeader();
	ieeeHeaderElement -> SetAttribute("isPresent", this -> isSetIeeeHeader);
	frameElement -> LinkEndChild( ieeeHeaderElement );

	//ulozime polozky llcHlavicky
	TiXmlElement * llcHeaderElement = this -> llcHeader.saveHeader();
	llcHeaderElement -> SetAttribute("isPresent", this -> isSetLLCHeader);
	frameElement -> LinkEndChild( llcHeaderElement );

	//paylaod
	TiXmlElement * payloadElement = this -> payload.savePayload();
	payloadElement -> SetAttribute("isPresent", this -> isSetPayload);
	frameElement -> LinkEndChild( payloadElement ); 

	bool saveOkay = saveFile.SaveFile( path );	
	return saveOkay;
}

/* Frame::getIsSetRadioTapHeader
 * Popis: funkce vrati informaci, zdali je nastavena RadioTap hlavicka
 * Parametry: -
 * Navratova hodnota: informace o nastaveni hlavicky
 */
bool Frame::getIsSetRadioTapHeader() {
	return this -> isSetRadioTapHeader;
}

/* Frame::getIsSetIeeeHeader
 * Popis: funkce vrati informaci, zdali je nastavena Ieee hlavicka
 * Parametry: -
 * Navratova hodnota: informace o nastaveni hlavicky
 */
bool Frame::getIsSetIeeeHeader() {
	return this -> isSetIeeeHeader;
}

/* Frame::getIsSetLLCHeader
 * Popis: funkce vrati informaci, zdali je nastavena LLC hlavicka
 * Parametry: -
 * Navratova hodnota: informace o nastaveni hlavicky
 */
bool Frame::getIsSetLLCHeader() {
	return this -> isSetLLCHeader;
}

/* Frame::getIsSetPayload
 * Popis: funkce vrati informaci, zdali je nastaven payload
 * Parametry: -
 * Navratova hodnota: informace o nastaveni payloadu
 */
bool Frame::getIsSetPayload() {
	return this -> isSetPayload;
}

/* Frame::parseIV
 * Popis: funkce vrati incializacni vektor
 * Parametry: int lengthFrame - delka ramce, ze ktereho budeme parsovat iv
 *            uint8_t * frame - ramec, ze ktereho budeme parsovat iv
 *            uint8_t * iv    - misto, kam ulozime nalezeny iv
 * Navratova hodnota: delka iv
 */
int Frame::parseIV(int lengthFrame, uint8_t * frame, uint8_t * iv) {
	uint8_t * beginIeee;

	//pokud ramec je kratsi, jak 4, tak je neco spatne
	if(lengthFrame < 4) return 0;

	//ramec neni prazdny - zjistime delku radiotap hlavicky
	int lengthRadioTapHeader = (frame[3] << 8) | frame[2];
	if(lengthFrame <= lengthRadioTapHeader)	return 0;

	//zjistim si zacatek ieee hlavicky
	beginIeee = &frame[lengthRadioTapHeader];
	//predam tride IeeeHeader;
	return IeeeHeader::parseIV(lengthFrame - lengthRadioTapHeader, beginIeee, iv);
}
