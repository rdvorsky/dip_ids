/* Soubor: Crc32.cpp
 * Autor: Pavel Svanda
 * Email: svandap@gmail.com
 * Datum vytvoreni: 25.6.2012
 * Popis: definice tridy Crc32 provadejici vypocet CRC kontrolniho souctu. Implementace tridy je ziskana z adresy 
 * http://tdistler.com/2011/06/22/crc32-a-simple-c-class . Trida byla mirne modifikovana pro me lepsi pouziti.
 */

#include "Crc32.h"

/* Crc32::Crc32
 * Popis: konstruktor
 * Parametry: -
 * Navratova hodnota: -
 */
Crc32::Crc32() { 
	reset(); 
}

/* Crc32::~Crc32
 * Popis: destruktor
 * Parametry: -
 * Navratova hodnota: -
 */
Crc32::~Crc32() {
}

/* Crc32::reset
 * Popis: funkce provede vymazani kontrolniho souctu
 * Parametry: -
 * Navratova hodnota: -
 */
void Crc32::reset() {
	this -> crc = (uint32_t)~0; 
}

/* Crc32::addData
 * Popis: funkce provede pridani dat, ze kterych se vypocita kontrolni soucet
 * Parametry: const uint8_t * data - ukazatel na data, ktera se maji pridat
 *            const uint32_t lengthData - delka dat, ktera se budou pridavat
 * Navratova hodnota: -
 */
void Crc32::addData(const uint8_t * data, const uint32_t lenghtData) {
	uint8_t * actualData = (uint8_t *)data;
        uint32_t remaining = lenghtData;
        for (; remaining--; ++actualData) {
            this -> crc = ( this -> crc >> 8 ) ^ kCrc32Table[(this -> crc ^ *actualData) & 0xff];
	}
}

/* Crc32::getCrc32
 * Popis: funkce navrati hodnotu kontrolniho souctu
 * Parametry: -
 * Navratova hodnota: hodnota kontrolniho souctu
 */
const uint32_t Crc32::getCrc32() { 
	return ~this -> crc; 
}
