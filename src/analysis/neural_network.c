/*
 * neural_network.c
 *
 *  Created on: 23.1.2014
 *      Author: rdy
 */

#include <stdlib.h>
#include <omp.h>

#include "common.h"
#include "neural_network.h"
#include "parallel_fann.h"

volatile sig_atomic_t 	ann_flag_stop_training = 0;

void ann_network_init_all_from_file(ann_t *ann){
	ann->ann_frame = ann_network_init_from_file(ANN_FILENAME_NET);
	ann->ann_pattern = ann_network_init_from_file(ANN_FILENAME_NET_PATTERN);
}

/**
 *
 */
struct fann *ann_network_frame_init(){

	struct fann *ann = fann_create_standard(
			4,
			ANN_TRAINING_NUM_INPUTS,
			ANN_TRAINING_NUM_INPUTS,
			ANN_TRAINING_NUM_INPUTS,
			ANN_TRAINING_NUM_OUTPUTS);

	return ann;
}


struct fann *ann_network_pattern_init(){

	struct fann *ann = fann_create_standard(
			3,
			ANN_TRAINING_PATTERN_NUM_INPUTS,
			ANN_TRAINING_PATTERN_NUM_INPUTS,
			ANN_TRAINING_PATTERN_NUM_OUTPUTS);

	return ann;
}


struct fann *ann_network_init_from_file(const char *filename){

	return fann_create_from_file(filename);
}

fann_type *ann_input_classify(struct fann *ann, fann_type input[]){

	return fann_run(ann,input);
}

void ann_training_stop(){
	ann_flag_stop_training = 1;
}

void ann_network_save(struct fann *ann, const char *filename){

	fann_save(ann, filename);
	fann_destroy(ann);
}

/**
 *
 * @param filename_train
 */
void ann_training_parallel(struct fann *ann, const char *filename_train, const char *filename_output_net, ann_type type){

	unsigned int i, max_epochs;
	double desired_error;
	struct fann_train_data *data;
	float error;

	if(ann == NULL){
		fprintf(stderr,"Error: first call ann_init_network().\n");
		exit(EXIT_FAILURE);
	}

	if(type == ANN_TYPE_FRAME){
		max_epochs = ANN_TRAINING_MAX_EPOCHS;
		desired_error = ANN_TRAINING_DESIRED_ERROR;
	}else{
		max_epochs = ANN_TRAINING_PATTERN_MAX_EPOCHS;
		desired_error = ANN_TRAINING_PATTERN_DESIRED_ERROR;
	}

	data = fann_read_train_from_file(filename_train);

	fann_set_train_error_function(ann, FANN_ERRORFUNC_LINEAR);

	/* Vstupna vrstva */
	if(type == ANN_TYPE_PATTERN){
		fann_set_activation_function_layer(ann,FANN_SIGMOID,0);
	}
	/* Skryte vrstvy */
	fann_set_activation_function_hidden(ann, type == ANN_TYPE_FRAME ? FANN_SIGMOID_SYMMETRIC : FANN_LINEAR);
	/* Vystupna vrstva */
	fann_set_activation_function_output(ann, type == ANN_TYPE_FRAME ? FANN_SIGMOID_SYMMETRIC : FANN_SIGMOID_SYMMETRIC);

	fann_set_learning_rate(ann,0.5f);
	fann_set_training_algorithm(ann,FANN_TRAIN_RPROP);


	printf("Max epochs %8d. Desired error: %.10f\n",max_epochs,desired_error);

	for(i = 1; i <= max_epochs; i++){

		if(type == ANN_TYPE_PATTERN){
			error = fann_train_epoch_quickprop_parallel(ann,data,omp_get_num_procs());
		}else{
			error = fann_train_epoch_irpropm_parallel(ann,data,omp_get_num_procs());
		}

		if((i % ANN_TRAINING_EPOCHS_BETWEEN_REPORTS == 0 ) || i == 1){
			printf("Epochs %12d. Current error: %.10f\n", i, error);
		}

		if(ann_flag_stop_training){
			ann_flag_stop_training = 0;
			break;
		}

		if(error <= desired_error) break;
	}

	ann_network_save(ann, filename_output_net);
	fann_destroy_train(data);
}


/**
 * @deprecated Pouzi ann_train_parallel.
 *
 * @param filename_train
 */
void ann_training(struct fann *ann, const char *filename_train, const char *filename_output_net){

	if(ann == NULL){
		fprintf(stderr,"Error: first call ann_init_network().\n");
		exit(EXIT_FAILURE);
	}

	fann_set_activation_function_hidden(ann, FANN_SIGMOID_SYMMETRIC);
	fann_set_activation_function_output(ann, FANN_SIGMOID_SYMMETRIC);

	fann_train_on_file(ann, filename_train, ANN_TRAINING_MAX_EPOCHS,
			ANN_TRAINING_EPOCHS_BETWEEN_REPORTS, ANN_TRAINING_DESIRED_ERROR);

	ann_network_save(ann,filename_output_net);
}

