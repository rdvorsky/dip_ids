/*
 * main.c
 *
 *  Created on: 12.3.2014
 *      Author: rdy
 */

#include "analyze.h"

/**
 * Main
 *
 * @return
 */
int main(int argc, char *argv[]) {

	int c;
	gboolean only_train_frame = FALSE, only_train_pattern = FALSE;
	char file_train_filename_frame[256], file_train_filename_pattern[256];

	while ((c = getopt(argc, argv, "f:p:")) != -1){

		switch (c) {
		case 'f':
			only_train_frame = TRUE;
			strcpy(file_train_filename_frame,optarg);
			break;
		case 'p':
			only_train_pattern = TRUE;
			strcpy(file_train_filename_pattern,optarg);
			break;
		}
	}

	anal_analysis_run(file_train_filename_frame,file_train_filename_pattern,only_train_frame,only_train_pattern);

	return EXIT_SUCCESS;
}

