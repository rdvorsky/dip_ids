/*
 ============================================================================
 Name        : analyze.c
 Author      : Radovan Dvorsky
 Version     :
 Copyright   : Your copyright notice
 Description : Hello World in C, Ansi-style
 ============================================================================
 */

#define FRAME_CLASSIFICATION

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <unistd.h>
#include <string.h>
#include <glib.h>

#include "analyze.h"
#include "database.h"
#include "debug.h"
#include "attack.h"
#include "common.h"
#include "stats.h"

const 	char		*fields_filter_frame[] 		= {"id","addr1","addr2","addr3","addr4","ssid","channel","timestamp",
												   "sig","aid","seq","bssid"};
ann_t 	ann_nets;
static 	GHashTable	*hash_table_ap_stats 		= NULL;
static 	GHashTable 	*hash_table_ap_pattern 		= NULL;
static 	int			pattern_actual_lenght		= ANN_TRAINING_PATTERN_LENGTH;
static 	int 		pattern_first_aid			= 0;
static 	int			pattern_aid_counter			= 0;
static 	long		pattern_counter				= 0;
static 	long		frame_counter				= 0;
static 	int			fann_file_num_patterns		= 0;

static	int			dropped_frames				= 0;
static	int			hit_frames					= 0;
static	int			miss_frames					= 0;
static	int			data_frames					= 0;
static	int			attack_data_frames			= 0;

static 	gboolean 	flag_phase_training 		= FALSE;
FILE 				*file_fann_training;
FILE				*file_stats_orig;
FILE				*file_stats_prediction;
FILE				*file_frame_data;
FILE				*file_stats_net;
FILE				*file_stats_orig_pattern;
FILE				*file_frame_stats;

const int 			anal_pattern_lengts[ATK_COUNT_ALL + 1]			= {0,ANAL_PATTERN_LEN_DEAUTH,ANAL_PATTERN_LEN_KOREK,
																		 ANAL_PATTERN_LEN_FRAG,ANAL_PATTERN_LEN_PTW,
																		 ANAL_PATTERN_LEN_AUTH,ANAL_PATTERN_LEN_CTS,
																		 ANAL_PATTERN_LEN_BT,ANAL_PATTERN_LEN_DEAUTH,
																		 ANAL_PATTERN_LEN_HOLE196};

fann_type			prediction_input[ANN_TRAINING_NUM_INPUTS*ANN_TRAINING_PATTERN_LENGTH];
fann_type			net_pattern_output[ANN_TRAINING_NUM_INPUTS];

/**
 * Callback funkcia pre zachytavanie SIG_INT signalu.
 * Pri zachyteni SIG_INT v trenovacej faze nedojde k okamzitemu ukonceniu
 * programu, ale pocka sa na dokoncenie epochy.
 *
 * @param sig
 */
void sig_int_handler(int sig){

	if(sig != SIGINT) return;

	/* Pri trenovacej faze neukoncuj hned, ale pockaj na koniec epochy a ulozenie suboru */
	if(flag_phase_training){
		puts("Stopping training...");
		ann_training_stop();
	}else{
		exit(EXIT_SUCCESS);
	}
}

ap_stats_t *anal_ap_stats_get_instance(GHashTable *hash_table, frame_data_t frame){
	ap_stats_t *ap = NULL;

	ap = g_hash_table_lookup(hash_table,&frame.bssid);

	if(ap == NULL){
		if(frame.fc_type == IEEE80211_FC0_TYPE_MGT){
			if((frame.fc_subtype << IEEE80211_FC0_SUBTYPE_SHIFT) == IEEE80211_FC0_SUBTYPE_BEACON){
				ap = calloc(1,sizeof(ap_stats_t));
				strcpy(ap->bssid,frame.addr3);
				strcpy(ap->ssid,frame.ssid);
				ap->bssid64 = frame.bssid;
				ap->hash_table_fake_dst_addr = g_hash_table_new((GHashFunc)g_string_hash, (GEqualFunc)g_string_equal);
				g_hash_table_insert(hash_table,&frame.bssid,ap);
			}
		}
	}

	return ap;
}

void anal_ap_stats_refresh(ap_stats_t *ap, frame_data_t frame){
	GString *key_dst;
	GString *dst_addr_counter = NULL;

	/* Management frames */
	if(frame.fc_type == IEEE80211_FC0_TYPE_MGT){


		switch (frame.fc_subtype << IEEE80211_FC0_SUBTYPE_SHIFT) {
		case IEEE80211_FC0_SUBTYPE_BEACON:
			ap->counter_beacon++;
			break;
		case IEEE80211_FC0_SUBTYPE_ASSOC_RESP:
			/* Novy klient */
			ap->counter_assoc_disassoc++;
			break;
		case IEEE80211_FC0_SUBTYPE_DISASSOC:
			/* Klient sa odpojil */
			ap->counter_assoc_disassoc--;
			break;
		case IEEE80211_FC0_SUBTYPE_AUTH:
			ap->counter_auth++;
			break;
		case IEEE80211_FC0_SUBTYPE_DEAUTH:
			ap->counter_deauth++;
			break;
		default:
			break;
		}

		ap->counter_mgt++;

		/* Data frames */
	}else if(frame.fc_type == IEEE80211_FC0_TYPE_DATA){

		if(frame.cipher == CIPHER_OPN) ap->counter_data_opn++;
		else if(frame.cipher == CIPHER_WEP) ap->counter_data_wep++;
		else if(frame.cipher == CIPHER_CCMP_TKIP) ap->counter_data_wpa++;

		if(frame.fc_to_ds == 1){

			/* Ked sa nerovnaju adresy bssid a cielova */
			if(strcmp(frame.addr1,frame.addr3) != 0){
				key_dst = g_string_new(frame.addr3);

				/* Broadcast prefix */
				if(g_str_has_prefix((g_string_ascii_up(key_dst)->str),"FF:") == TRUE){
					/* Nie je broadcast */
					if(g_string_equal(g_string_ascii_up(key_dst),g_string_new("FF:FF:FF:FF:FF:FF")) == FALSE){
						dst_addr_counter = g_hash_table_lookup(ap->hash_table_fake_dst_addr,key_dst);

						if(dst_addr_counter == NULL){
							g_hash_table_insert(ap->hash_table_fake_dst_addr,key_dst,key_dst);
							ap->counter_fake_dst_addr++;
						}
					}else{/* Broadcast mac adresa */
						ap->counter_broadcast_dst_addr++;
					}
				}
			}
		}

		ap->counter_data++;


		/* Control frames */
	}else if(frame.fc_type == IEEE80211_FC0_TYPE_CTL){

		switch(frame.fc_subtype << IEEE80211_FC0_SUBTYPE_SHIFT){
		case IEEE80211_FC0_SUBTYPE_ACK:
			ap->counter_ack++;
			break;
		case IEEE80211_FC0_SUBTYPE_CTS:
			ap->counter_cts++;
			break;
		case IEEE80211_FC0_SUBTYPE_RTS:
			ap->counter_rts++;
			break;
		}

		ap->counter_ctl++;
	}


	if(frame.fc_from_ds == 1 && frame.fc_to_ds == 0){
		ap->counter_total_out++;
	}else if(frame.fc_from_ds == 0 && frame.fc_to_ds == 1){
		ap->counter_total_in++;
	}

	if(frame.fc_more_frag == 1) ap->counter_fragmented++;
	if(frame.fc_retry == 1) ap->counter_retry++;


	ap->channel = frame.channel;
	ap->counter_total++;
	ap->size_total += frame.len;
	ap->size_dur += frame.dur;
}

/**
 * @deprecated Pouzi anal_ap_stats_get() a anal_ap_stats_refresh().
 * @param hash_table
 * @param frame
 * @return
 */
ap_stats_t *anal_ap_stats_refresh_old(GHashTable *hash_table, frame_data_t frame){

	ap_stats_t *ap = NULL;
	GString *key_dst;
	GString *dst_addr_counter = NULL;

	/* Management frames */
	if(frame.fc_type == IEEE80211_FC0_TYPE_MGT){

		/* Najde AP v hash tabulke */
		ap = g_hash_table_lookup(hash_table,&frame.bssid);

		switch (frame.fc_subtype << IEEE80211_FC0_SUBTYPE_SHIFT) {
			case IEEE80211_FC0_SUBTYPE_BEACON:

				/* AP este neexistuje, zisti MAC AP a vytvor zaznam v tabulke */
				if(ap == NULL){
					ap = calloc(1,sizeof(ap_stats_t));
					strcpy(ap->bssid,frame.addr3);
					strcpy(ap->ssid,frame.ssid);
					ap->bssid64 = frame.bssid;
					ap->hash_table_fake_dst_addr = g_hash_table_new((GHashFunc)g_string_hash, (GEqualFunc)g_string_equal);
					g_hash_table_insert(hash_table,&frame.bssid,ap);

				}

				ap->counter_beacon++;
				break;
			case IEEE80211_FC0_SUBTYPE_ASSOC_RESP:

				if(ap == NULL) return ap;

				/* Novy klient */
				ap->counter_assoc_disassoc++;
				break;
			case IEEE80211_FC0_SUBTYPE_DISASSOC:

				if(ap == NULL) return ap;

				/* Klient sa odpojil */
				ap->counter_assoc_disassoc--;
				break;
			case IEEE80211_FC0_SUBTYPE_AUTH:

				if(ap == NULL) return ap;

				ap->counter_auth++;
				break;
			case IEEE80211_FC0_SUBTYPE_DEAUTH:

				if(ap == NULL) return ap;

				ap->counter_deauth++;
				break;
			default:
				break;
		}

		if(ap == NULL) return ap;

		ap->counter_mgt++;

	/* Data frames */
	}else if(frame.fc_type == IEEE80211_FC0_TYPE_DATA){

		ap = g_hash_table_lookup(hash_table,&frame.bssid);

		if(ap == NULL) return ap;

		if(frame.cipher == CIPHER_OPN) ap->counter_data_opn++;
		else if(frame.cipher == CIPHER_WEP) ap->counter_data_wep++;
		else if(frame.cipher == CIPHER_CCMP_TKIP) ap->counter_data_wpa++;

		if(frame.fc_to_ds == 1){

			/* Ked sa nerovnaju adresy bssid a cielova */
			if(strcmp(frame.addr1,frame.addr3) != 0){
				key_dst = g_string_new(frame.addr3);

				/* Broadcast prefix */
				if(g_str_has_prefix((g_string_ascii_up(key_dst)->str),"FF:") == TRUE){
					/* Nie je broadcast */
					if(g_string_equal(g_string_ascii_up(key_dst),g_string_new("FF:FF:FF:FF:FF:FF")) == FALSE){
						dst_addr_counter = g_hash_table_lookup(ap->hash_table_fake_dst_addr,key_dst);

						if(dst_addr_counter == NULL){
							g_hash_table_insert(ap->hash_table_fake_dst_addr,key_dst,key_dst);
							ap->counter_fake_dst_addr++;
						}
					}else{/* Broadcast mac adresa */
						ap->counter_broadcast_dst_addr++;
					}
				}
			}
		}

		ap->counter_data++;


	/* Control frames */
	}else if(frame.fc_type == IEEE80211_FC0_TYPE_CTL){

		ap = g_hash_table_lookup(hash_table,&frame.bssid);
		if(ap == NULL) return ap;

		switch(frame.fc_subtype << IEEE80211_FC0_SUBTYPE_SHIFT){
			case IEEE80211_FC0_SUBTYPE_ACK:
				ap->counter_ack++;
				break;
			case IEEE80211_FC0_SUBTYPE_CTS:
				ap->counter_cts++;
				break;
			case IEEE80211_FC0_SUBTYPE_RTS:
				ap->counter_rts++;
				break;
		}

		ap->counter_ctl++;
	}


	if(frame.fc_from_ds == 1 && frame.fc_to_ds == 0){
		ap->counter_total_out++;
	}else if(frame.fc_from_ds == 0 && frame.fc_to_ds == 1){
		ap->counter_total_in++;
	}

	if(frame.fc_more_frag == 1) ap->counter_fragmented++;
	if(frame.fc_retry == 1) ap->counter_retry++;


	ap->channel = frame.channel;
	ap->counter_total++;
	ap->size_total += frame.len;
	ap->size_dur += frame.dur;

	return ap;
}

void anal_ap_stats_reset_pattern(ap_stats_t *ap, int pattern_len){
	anal_ap_stats_reset(ap);
	ap->len_pattern = pattern_len;
	/*ap->aid = 0;*/
}

void anal_ap_stats_reset(ap_stats_t *ap){
	ap->counter_ack = 0;
	ap->counter_assoc_disassoc = 0;
	ap->counter_auth = 0;
	ap->counter_beacon = 0;
	ap->counter_ctl = 0;
	ap->counter_cts = 0;
	ap->counter_data = 0;
	ap->counter_data_opn = 0;
	ap->counter_data_wep = 0;
	ap->counter_data_wpa = 0;
	ap->counter_deauth = 0;
	ap->counter_fragmented = 0;
	ap->counter_mgt = 0;
	ap->counter_retry = 0;
	ap->counter_rts = 0;
	ap->counter_total = 0;
	ap->counter_total_in = 0;
	ap->counter_total_out = 0;
	ap->size_dur = 0;
	ap->size_total = 0;
	g_hash_table_remove_all(ap->hash_table_fake_dst_addr);
	ap->counter_fake_dst_addr = 0;
	ap->counter_broadcast_dst_addr = 0;
	ap->time_first = 0.0f;
	ap->time_last = 0.0f;
	/*ap->aid = 0;*/
}

fann_type *anal_frame_to_array(frame_data_t *frame){

	fann_type *array = calloc(ANN_TRAINING_NUM_INPUTS,sizeof(fann_type));

	array[0] = frame->fc_type;
	array[1] = frame->fc_subtype;
	array[2] = frame->fc_from_ds;
	array[3] = frame->fc_to_ds;
	array[4] = frame->fc_more_frag;
	array[5] = frame->fc_retry;
	array[6] = frame->fc_protected_frame;
	array[7] = frame->fc_order;
	array[8] = frame->frag;
	array[9] = frame->dur;
	array[10] = frame->len;
	array[11] = frame->cipher;
	array[12] = frame->bcst;
	array[13] = frame->bcst_prefix;

	return array;

}

fann_type *anal_pattern_stats_to_array(pattern_stats_t *stats){
	fann_type *stats_array = calloc(ANN_TRAINING_PATTERN_NUM_INPUTS,sizeof(fann_type));

	stats_array[0] = stats->ack_rate;
	stats_array[1] = stats->beacon_rate;
	stats_array[2] = stats->ctl_frame_rate;
	stats_array[3] = stats->data_frame_rate;
	stats_array[4] = stats->data_opn_rate;
	stats_array[5] = stats->data_wep_rate;
	stats_array[6] = stats->data_wpa_rate;
	stats_array[7] = stats->fragmented_rate;
	stats_array[8] = stats->mgt_frame_rate;
	stats_array[9] = stats->retry_rate;
	stats_array[10] = stats->total_in_rate;
	stats_array[11] = stats->total_out_rate;
	stats_array[12] = stats->broadcast_dst_addr_rate;
	stats_array[13] = stats->fake_dst_addr_rate;
	stats_array[14] = (float)stats->avg_dur;
	stats_array[15] = (float)stats->avg_size;

	return stats_array;
}

void anal_pattern_stats_write(pattern_stats_t *stats, FILE *file){
	/* 21 - 4 = 17*/
	fprintf(file,"%f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f\n",
			stats->ack_rate,
			/*stats->auth_rate,*/
			stats->beacon_rate,
			stats->ctl_frame_rate,
			/*stats->cts_rate,*/
			stats->data_frame_rate,
			stats->data_opn_rate,
			stats->data_wep_rate,
			stats->data_wpa_rate,
			/*stats->deauth_rate,*/
			stats->fragmented_rate,
			stats->mgt_frame_rate,
			stats->retry_rate,
			/*stats->rts_rate,*/
			stats->total_in_rate,
			stats->total_out_rate,
			stats->broadcast_dst_addr_rate,
			stats->fake_dst_addr_rate,
			/*stats->elapsed_time,*/
			(float)stats->avg_dur,
			(float)stats->avg_size
	);
}


void anal_pattern_stats_calc(ap_stats_t *ap, pattern_stats_t *out_stats){

	u_int16_t dur_tmp, size_tmp;

	out_stats->ack_rate = (float)ap->counter_ack/(float)ap->counter_total;
	out_stats->auth_rate = (float)ap->counter_auth/(float)ap->counter_total;
	out_stats->beacon_rate = (float)ap->counter_beacon/(float)ap->counter_total;
	out_stats->ctl_frame_rate = (float)ap->counter_ctl/(float)ap->counter_total;
	out_stats->cts_rate = (float)ap->counter_cts/(float)ap->counter_total;
	out_stats->data_frame_rate = (float)ap->counter_data/(float)ap->counter_total;
	out_stats->data_opn_rate = (float)ap->counter_data_opn/(float)ap->counter_total;
	out_stats->data_wep_rate = (float)ap->counter_data_wep/(float)ap->counter_total;
	out_stats->data_wpa_rate = (float)ap->counter_data_wpa/(float)ap->counter_total;
	out_stats->deauth_rate = (float)ap->counter_deauth/(float)ap->counter_total;
	out_stats->fragmented_rate = (float)ap->counter_fragmented/(float)ap->counter_total;
	out_stats->mgt_frame_rate = (float)ap->counter_mgt/(float)ap->counter_total;
	out_stats->retry_rate = (float)ap->counter_retry/(float)ap->counter_total;
	out_stats->rts_rate = (float)ap->counter_rts/(float)ap->counter_total;
	out_stats->total_in_rate = (float)ap->counter_total_in/(float)ap->counter_total;
	out_stats->total_out_rate = (float)ap->counter_total_out/(float)ap->counter_total;
	out_stats->fake_dst_addr_rate = (float)ap->counter_fake_dst_addr/(float)ap->counter_total;
	out_stats->broadcast_dst_addr_rate = (float)ap->counter_broadcast_dst_addr/(float)ap->counter_total;
	/*stats->elapsed_time = ap->time_last - ap->time_first;*/

	dur_tmp = ap->size_dur/ap->counter_total;
	size_tmp = ap->size_total/ap->counter_total;

	if(dur_tmp < 50){
		out_stats->avg_dur = 0;
	}else if(dur_tmp >= 50 && dur_tmp < 100){
		out_stats->avg_dur = 1;
	}else if(dur_tmp >= 100 && dur_tmp < 200){
		out_stats->avg_dur = 2;
	}else if(dur_tmp >= 200 && dur_tmp < 300){
		out_stats->avg_dur = 3;
	}else if(dur_tmp >= 300 && dur_tmp < 400){
		out_stats->avg_dur = 4;
	}else if(dur_tmp >= 400){
		out_stats->avg_dur = 5;
	}

	if(size_tmp < 25){
		out_stats->avg_size = 0;
	}else if(size_tmp >= 25 && size_tmp < 40){
		out_stats->avg_size = 1;
	}else if(size_tmp >= 40 && size_tmp < 60){
		out_stats->avg_size = 2;
	}else if(size_tmp >= 60 && size_tmp < 100){
		out_stats->avg_size = 3;
	}else if(size_tmp >= 100 && size_tmp < 200){
		out_stats->avg_size = 4;
	}else if(size_tmp >= 200){
		out_stats->avg_size = 5;
	}
}

/**
 * Pomocna funkcia, ktora z hodnoty vytvori klasifikacny textovy retazec.
 * Napr. pre value = 3, max = 4 vytvori: 0 0 1 0.
 *
 * @param value
 * @param max
 * @return
 */
char *anal_classfication_string_build(unsigned int value, unsigned int max){

	char *result = calloc(max*2,sizeof(char));/* pocet znakov + medzera pre kazdy */
	unsigned int i;
	short bin = 0;
	char bin_string[3];

	for (i = 1; i <= max; ++i) {
		bin = (i == value) ? 1 : 0;
		(i == max) ? sprintf(bin_string,"%d",bin) : sprintf(bin_string,"%d ",bin);
		strcat(result,bin_string);
	}


	return result;
}


/**
 * Pomocna funkcia, ktora vyfiltruje jednotlive atributy podla hodnot poli fields_filter_*.
 *
 * @param field
 * @param type ANN_TYPE_FRAME | ANN_TYPE_PATTERN *
 * @return 0|1
 */
int filter_field(MYSQL_FIELD field){

	u_int8_t i;
	u_int8_t lenght = ARRAY_LENGTH(fields_filter_frame);

	for (i = 0; i < lenght; ++i) {
		if(strcmp(field.name, fields_filter_frame[i]) == 0){
			return 1;
		}
	}

	return 0;
}

/**
 * Uklada jednotlive riadky tabulky do suboru vo FANN formate.
 *
 * @param row
 * @param fields
 * @param num_rows
 * @param num_fields
 */
void foreach_pattern_to_fann_file(MYSQL_ROW row, MYSQL_FIELD *fields, u_int32_t num_rows, u_int16_t num_fields){

	int aid = 0;
	char *classification_string;
	pattern_stats_t stats;
	ap_stats_t *ap;
	frame_data_t packet = db_row_to_frame(row);

	UNUSED(fields);
	UNUSED(num_rows);

	/* return v pripade ak pre ramec este neexistuje AP */
	if((ap = anal_ap_stats_refresh_old(hash_table_ap_stats,packet)) == NULL) return;

	aid = atoi(row[num_fields - 1]);

	/* Pri prvom nastav aid */
	if(pattern_actual_lenght == ANN_TRAINING_PATTERN_LENGTH){
		pattern_first_aid = aid;

		/* Pri prvom nastav pocitatocny timestamp
		ap->time_first = packet.timestamp;*/
	}

	/* zmenil sa utok, zapis len ak bol prvy ramec vo vzore utocny */
	if(pattern_first_aid != aid) {
		if(pattern_first_aid == 0) goto drop_pattern;
		else goto write_pattern;
	}

	pattern_actual_lenght--;

	/* Koniec pattern, treba zapisat do suboru klasifikacny retazec */
	if(pattern_actual_lenght > 0) return;

write_pattern:
	/* Timestamp postledneho ramca vzoru
	ap->time_last = packet.timestamp; */

	/* Vypocitaj statistiky */
	anal_pattern_stats_calc(ap,&stats);
	/* Zapis statistiky a klasifikacny retazec do trenovacieho suboru */
	anal_pattern_stats_write(&stats,file_fann_training);


	/* Nepekny HACK */
	if(pattern_first_aid == 5 || pattern_first_aid == 6){
		pattern_first_aid = 1;/* Treba namapovat vsetky DoS id na jednotne id, nech sa vytvori genericky klasifiktator pre DoS */
	}


	/* Predpoklada, ze id vsetkych dos utokov je vacsie ako 4 */
	classification_string = anal_classfication_string_build(pattern_first_aid,ATK_COUNT_ALL);
	fprintf(file_fann_training,"%s\n",classification_string);
	free(classification_string);

	fann_file_num_patterns++;

drop_pattern:

	/* Znova-nastavenie pociatocnych hodnot */
	pattern_actual_lenght = ANN_TRAINING_PATTERN_LENGTH;
	pattern_first_aid = 0;
	pattern_aid_counter = 0;
	anal_ap_stats_reset(ap);
}

/**
 * Uklada jednotlive riadky tabulky do suboru vo FANN formate.
 *
 * @param row
 * @param fields
 * @param num_rows
 * @param num_fields
 */
void foreach_frame_to_fann_file(MYSQL_ROW row, MYSQL_FIELD *fields, u_int32_t num_rows, u_int16_t num_fields){

	int i, aid = 0;
	char *classification_string;
	frame_data_t frame = db_row_to_frame(row);
	UNUSED(num_rows);

	/* Ucenie len na datovych paketoch */
	if(frame.fc_type != IEEE80211_FC0_TYPE_DATA) return;

	/* Filtruje atributy a zapisuje ich hodnoty do suboru */
	for (i = 0; i < num_fields; ++i) {

		if(filter_field(fields[i])) continue;

		if(strlen(row[i]) == 0){
			row[i] = "-1";
		}

		fprintf(file_fann_training,"%s ",row[i]);
	}

	fprintf(file_fann_training,"\n");

	/* Zapise klasifikacny retazec */
	aid = atoi(row[num_fields - 1]);

	classification_string = anal_classfication_string_build(aid,ANN_TRAINING_NUM_OUTPUTS);
	fprintf(file_fann_training,"%s\n",classification_string);
	free(classification_string);
}

int anal_make_decision(fann_type *input, ann_type type){

	unsigned int i;
	float threshold = ANN_CLASSIFICATION_THRESHOLD;

	if(type == ANN_TYPE_PATTERN) threshold = ANN_CLASSIFICATION_PATTERN_THRESHOLD;

	for (i = 0; i < ATK_COUNT_ALL; ++i) {

		if((input[i] + threshold) >= 1){
			return i+1;
		}
	}

	return 0;
}


void anal_frame_prediction_write(int aid_prediction, FILE *file_output_stats){
	char *class_string = anal_classfication_string_build((unsigned int)aid_prediction,ATK_COUNT_ALL);

	fprintf(file_output_stats,"%s\n",class_string);
}

fann_type *anal_pattern_prediction_classify(fann_type *raw_data, dos_pattern_stats_t *stats_dos,
		u_int8_t output_prediction_vector[ATK_COUNT_ALL]){

	int i, aid_dos = 0;
	u_int8_t prediction = 0;
	fann_type *results = ann_input_classify(ann_nets.ann_pattern,raw_data);

	memset(output_prediction_vector,0,ATK_COUNT_ALL*sizeof(u_int8_t));

	for (i = 0; i < ATK_COUNT_ALL; ++i) {

		if(i == 0){
			if((results[0] + ANN_CLASSIFICATION_PATTERN_THRESHOLD) >= 1){
				if(stats_dos->auth_rate > stats_dos->deauth_rate && stats_dos->auth_rate > stats_dos->cts_rate){
					aid_dos = ATK_ID_AUTH;
				}
				else if(stats_dos->deauth_rate > stats_dos->auth_rate && stats_dos->deauth_rate > stats_dos->cts_rate){
					aid_dos = ATK_ID_DEAUTH;
				}
				else if(stats_dos->cts_rate > stats_dos->deauth_rate && stats_dos->cts_rate > stats_dos->auth_rate){
					aid_dos = ATK_ID_CTS;
				}else{
					/* TODO: treba zmenit spat na unknown id */
					aid_dos = ATK_ID_RTS;
				}

				output_prediction_vector[aid_dos-1] = 1;
			}
		}else{
			prediction = (results[i] + ANN_CLASSIFICATION_PATTERN_THRESHOLD) >= 1 ? 1 : 0;
			if(prediction == 1){
				output_prediction_vector[i] = 1;
			}
		}
	}

	return results;
}

/**
 * Na zaklade hodnot z FANN klasifikuje vstup. Vysledny
 * klasifikacny retazec zapise do zadaneho output suboru.
 *
 * @param input
 * @param length
 * @param type
 * @param file_output_stats
 */
void anal_pattern_prediction_write(u_int8_t *prediction_vector, int id_pattern,
						  FILE *file_output_stats, gboolean write_fann_results, fann_type *results){

	int i;

	fprintf(file_output_stats,"%d ",id_pattern);
	for (i = 0; i < ATK_COUNT_ALL; ++i) {

		fprintf(file_output_stats, "%d ",prediction_vector[i]);

		if(write_fann_results) fprintf(file_stats_net,"%f ",results[i]);
	}

	fprintf(file_output_stats,"\n");
	fprintf(file_stats_net,"\n");
}

int anal_frame_fields_filter(MYSQL_ROW row, MYSQL_FIELD *fields, u_int16_t num_fields, fann_type *input){

	int i = 0, k = 0;

	for (i = 0; i < num_fields; ++i) {

		if(filter_field(fields[i])) continue;

		if(strlen(row[i]) == 0){
			row[i] = "-1";
		}
		input[k++] = (fann_type)atoi(row[i]);
	}

	return k;
}

/**
 * Funkcia klasifikuje zadany ramec a vrati typ potencionalneho utoku.
 *
 * @param frame
 * @param fann_raw_data
 * @return ID utoku
 */
int anal_frame_prediction_classify(frame_data_t frame){

	int aid_prediction = 0;
	fann_type* results, *fann_raw_data = anal_frame_to_array(&frame);

	/*
	 * Urcenie predokladaneho utoku na zaklade framu.
	 * Pri datovych ramcoch sa pouzije neuronova siet.
	 * Pri ostatnych typoch typ ramca.
	 */
	if(frame.fc_type == IEEE80211_FC0_TYPE_DATA){
		results = ann_input_classify(ann_nets.ann_frame,fann_raw_data);
		if((aid_prediction = anal_make_decision(results,ANN_TYPE_FRAME)) == 0) {
			dropped_frames++;
			if(frame.aid > 0) miss_frames++;
			if(frame.aid == 0) hit_frames++;
		}

		if(aid_prediction > 0){
			attack_data_frames++;
			if(frame.aid > 0) hit_frames++;
			if(frame.aid == 0) miss_frames++;
		}

		data_frames++;

	}else if(frame.fc_type == IEEE80211_FC0_TYPE_MGT){
		switch (frame.fc_subtype << IEEE80211_FC0_SUBTYPE_SHIFT) {
		case IEEE80211_FC0_SUBTYPE_AUTH:
			aid_prediction = ATK_ID_AUTH;
			break;
		case IEEE80211_FC0_SUBTYPE_DEAUTH:
			aid_prediction = ATK_ID_DEAUTH;
			break;
		case IEEE80211_FC0_SUBTYPE_BEACON:/* Beacon framy nas nezaujimaju */
			aid_prediction = 0;
			break;
		default:
			aid_prediction = ATK_ID_UNKNOWN;
			break;
		}
	}else if(frame.fc_type == IEEE80211_FC0_TYPE_CTL){
		if ((frame.fc_subtype << IEEE80211_FC0_SUBTYPE_SHIFT) == IEEE80211_FC0_SUBTYPE_CTS) {
			aid_prediction = ATK_ID_CTS;
		}else{
			aid_prediction = ATK_ID_UNKNOWN;
		}
	}


	return aid_prediction;
}

/**
 * Funkcia urci typ predpokladaneho utoku na zaklade poskytnuteho
 * ramca (v pripade management a control ramcov) alebo v pripade
 * datovych ramcov pomocou neuronovej siete.
 *
 * @param frame
 * @param fann_data
 * @return
 */
int anal_frame_prediction(frame_data_t frame){

	int aid_prediction = 0;
	char *classification_string;

	/* Na zaklade id utoku z DB zapis klasifikacny retazec do suboru na kontrolu uspenosti klasifikacie ramcov */
	classification_string = anal_classfication_string_build(frame.aid,ATK_COUNT_ALL);
	fprintf(file_stats_orig,"%s\n",classification_string);

	/* Statistiky klasifikacie vzorov */
	fprintf(file_stats_orig_pattern,"%ld %s\n",pattern_counter, anal_classfication_string_build(frame.aid,ATK_COUNT_ALL));

	aid_prediction = anal_frame_prediction_classify(frame);

	/* Beacon framy vyfiltruj vsetky */
	if(frame.fc_type == IEEE80211_FC0_TYPE_MGT && (frame.fc_subtype << IEEE80211_FC0_SUBTYPE_SHIFT) == IEEE80211_FC0_SUBTYPE_BEACON){
		aid_prediction = 0;
	}

	free(classification_string);

	return aid_prediction;
}

/**
 * Pomocna funkcia, ktora klasifikuje riadok a zapisuje ich v podobe vzoru do vystupneho
 * suboru ANAL_CLASS_FRAME_OUTPUT.
 *
 * @param row
 * @param fields
 * @param num_rows
 * @param num_fields
 */
void foreach_classify_table_row(MYSQL_ROW row, MYSQL_FIELD *fields, u_int32_t num_rows, u_int16_t num_fields){

	int aid_prediction = 0;
	frame_data_t frame = db_row_to_frame(row);
	pattern_stats_t *pattern_stats;
	ap_stats_t *ap;

	UNUSED(num_rows);

	/* Obnova statistik sluziacich na vypocet metrik, return v pripade ak pre ramec este neexistuje AP */
	if((ap = anal_ap_stats_refresh_old(hash_table_ap_stats,frame)) == NULL) return;

#ifdef FRAME_CLASSIFICATION

	/* Filtruje atributy a vytvara vstupne pole pre neuronovu siet */
	anal_frame_fields_filter(row, fields,num_fields,prediction_input);

	/* Predikcie id potencionalneho utoku */
	aid_prediction = anal_frame_prediction(frame);
	anal_frame_prediction_write(aid_prediction,file_stats_prediction);

	/* Ak nie je ramec utocny nepvytvaraj a nepocitaj vzory */
	if(aid_prediction == 0) return;


#else
	fprintf(file_stats_orig_pattern,"%ld %s\n",pattern_counter,anal_classfication_string_build(frame.aid,ATK_COUNT_ALL));
#endif

	pattern_stats = g_hash_table_lookup(hash_table_ap_pattern,&frame.bssid);

	if(pattern_stats == NULL){
		pattern_stats = calloc(1,sizeof(pattern_stats_t));
		if(frame.aid > 0) pattern_stats->actual_aid = frame.aid;
#ifdef FRAME_CLASSIFICATION
		pattern_stats->actual_pattern_lenght = anal_pattern_lengts[aid_prediction];
#else
		pattern_stats->actual_pattern_lenght = ANN_TRAINING_PATTERN_LENGTH;
#endif
		g_hash_table_insert(hash_table_ap_pattern,&frame.bssid,pattern_stats);
	}

	if(frame.aid > 0) pattern_stats->actual_aid = frame.aid;

	/*fprintf(file_stats_orig_pattern,"%ld %s\n",pattern_counter,make_classfication_string(pattern_stats->actual_aid,ATK_COUNT_ALL));*/

	/* Pri prvom ramci vloz cas zaciatku */
	if(pattern_stats->actual_pattern_lenght == ANN_TRAINING_PATTERN_LENGTH){
		ap->time_first = frame.timestamp;
	}

	/* Zapis az ked bude dosiahnuta dlzka vzoru */
	if(--pattern_stats->actual_pattern_lenght == 0){

		ap->time_last = frame.timestamp;

		anal_pattern_stats_calc(ap,pattern_stats);

		/* Zapis ID a potom data */
		fprintf(file_frame_data,"%ld ",pattern_counter);
		anal_pattern_stats_write(pattern_stats,file_frame_data);
		fprintf(file_frame_stats,"%f %f %f\n",pattern_stats->auth_rate,pattern_stats->deauth_rate,pattern_stats->cts_rate);


		anal_ap_stats_reset(ap);

#ifdef FRAME_CLASSIFICATION
		pattern_stats->actual_pattern_lenght = anal_pattern_lengts[aid_prediction];
#else
		pattern_stats->actual_pattern_lenght = ANN_TRAINING_PATTERN_LENGTH;
#endif

		pattern_stats->actual_aid = 0;
		pattern_counter++;
	}
}


/**
 * Klasifikuje vzory ulozene v subore ANAL_CLASS_FRAME_OUTPUT a vysledky zapise
 * do STATS_* suborov.
 */
void classify_patterns(){

	int i = 0, res = 1, j = 0, id, res2;
	float value;
	fann_type input[ANN_TRAINING_PATTERN_NUM_INPUTS];
	fann_type *results;
	dos_pattern_stats_t pattern_dos_stats;
	u_int8_t prediction_vector[ATK_COUNT_ALL];

	UNUSED(res2);

	if((file_frame_data = fopen(ANAL_CLASS_FRAME_OUTPUT,"r")) == NULL){
		fprintf(stderr,"ERROR: \"%s\" fopen failed\n",ANAL_CLASS_FRAME_OUTPUT);
		exit(EXIT_FAILURE);
	}


	if((file_stats_net = fopen(ANAL_STATS_FILENAME_NET,"w+")) == NULL){
		fprintf(stderr,"ERROR: \"%s\" fopen failed\n",ANAL_STATS_FILENAME_NET);
		exit(EXIT_FAILURE);
	}

	if((file_stats_prediction = fopen(ANAL_STATS_FILENAME_PREDICTION_PATTERN,"w+")) == NULL){
		fprintf(stderr,"ERROR: \"%s\" fopen failed\n",ANAL_STATS_FILENAME_PREDICTION_PATTERN);
		exit(EXIT_FAILURE);
	}

	if((file_frame_stats = fopen(ANAL_STATS_FILENAME_STATS,"r")) == NULL){
		fprintf(stderr,"ERROR: \"%s\" fopen failed\n",ANAL_STATS_FILENAME_STATS);
		exit(EXIT_FAILURE);
	}


	/* Nacitava data zo vstupneho suboru a klasifikuje vzory */
	while(res != EOF){

		if(i < ANN_TRAINING_PATTERN_NUM_INPUTS - 1){
			if(i == 0){
				res = fscanf(file_frame_data,"%d %f ",&id, &value);
			}else{
				res = fscanf(file_frame_data,"%f ",&value);
			}
		}else{
			res = fscanf(file_frame_data,"%f \n",&value);
			res2 = fscanf(file_frame_stats,"%f %f %f\n",
					&pattern_dos_stats.auth_rate,
					&pattern_dos_stats.deauth_rate,
					&pattern_dos_stats.cts_rate);
		}

		input[j++] = value;

		/* Pattern kompletny */
		if(i == ANN_TRAINING_PATTERN_NUM_INPUTS - 1){
			results = anal_pattern_prediction_classify(input,&pattern_dos_stats,prediction_vector);
			anal_pattern_prediction_write(prediction_vector,id,file_stats_prediction,TRUE,results);
			i = 0;
			j = 0;
		}else{
			i++;
		}
	}

	fclose(file_frame_stats);
	fclose(file_frame_data);
	fclose(file_stats_net);
	fclose(file_stats_prediction);
}

/**
 * Klasifikuje vsetky ramce z "data" db tabulky. Vsetky ramce klasifikovane
 * ako utocne ulozi vo forme vzorov do suboru ANAL_CLASS_FRAME_OUTPUT.
 */
void classify_frames(){

	MYSQL_RES *result;


	if((file_stats_orig = fopen(ANAL_STATS_FILENAME_ORIG,"w+")) == NULL){
		fprintf(stderr,"ERROR: \"%s\" fopen failed\n",ANAL_STATS_FILENAME_ORIG);
		exit(EXIT_FAILURE);
	}

	if((file_stats_orig_pattern = fopen(ANAL_STATS_FILENAME_ORIG_PATTERN,"w+")) == NULL){
		fprintf(stderr,"ERROR: \"%s\" fopen failed\n",ANAL_STATS_FILENAME_ORIG_PATTERN);
		exit(EXIT_FAILURE);
	}

	if((file_frame_data = fopen(ANAL_CLASS_FRAME_OUTPUT,"w+")) == NULL){
		fprintf(stderr,"ERROR: \"%s\" fopen failed\n",ANAL_CLASS_FRAME_OUTPUT);
		exit(EXIT_FAILURE);
	}

	if((file_stats_prediction = fopen(ANAL_STATS_FILENAME_PREDICTION,"w+")) == NULL){
		fprintf(stderr,"ERROR: \"%s\" fopen failed\n",ANAL_STATS_FILENAME_PREDICTION);
		exit(EXIT_FAILURE);
	}

	if((file_frame_stats = fopen(ANAL_STATS_FILENAME_STATS,"w+")) == NULL){
		fprintf(stderr,"ERROR: \"%s\" fopen failed\n",ANAL_STATS_FILENAME_STATS);
		exit(EXIT_FAILURE);
	}

	result = db_query_select_all(DB_TABLE_NAME_TEST);
	db_table_foreach(result,foreach_classify_table_row);

	fclose(file_frame_stats);
	fclose(file_stats_orig_pattern);
	fclose(file_frame_data);
	fclose(file_stats_orig);
	fclose(file_stats_prediction);
}

/**
 * Vytvori vstupny trenovaci/testovaci subor pre FANN kniznicu
 *
 * @param db_table_name
 * @param output_file_name
 */
void create_fann_file_pattern(const char *db_table_name, const char *output_file_name){

	MYSQL_RES *result;
	FILE *tmp_training,*original_output;
	GString *tmp_output_file_name = g_string_append(g_string_new(output_file_name),".tmp");
	char buffer[1024];
	size_t len = 0;

	/* Najskor normalne data, potom utoky */
	result = db_query_select_all_where(db_table_name,"ORDER BY id");


	if((file_fann_training = fopen(output_file_name,"w+")) == NULL){
		fprintf(stderr,"ERROR: \"%s\" fopen failed\n",output_file_name);
		exit(EXIT_FAILURE);
	}

	if((tmp_training = fopen(tmp_output_file_name->str,"w+")) == NULL){
		fprintf(stderr,"ERROR: \"%s\" fopen failed\n",tmp_output_file_name->str);
		exit(EXIT_FAILURE);
	}

	/*
	 * Treba zamenit vystupy nech su data najskor zapisane do docasneho suboru,
	 * foreach_pattern_to_fann_file zapisuje do file_fann_training
	 */
	original_output = file_fann_training;
	file_fann_training = tmp_training;

	/* Spracuje kazdy riadok tabulky a zapise vsetky vzory do suboru file_fann_training */
	db_table_foreach(result,foreach_pattern_to_fann_file);

	file_fann_training = original_output;

	/* Hlavicka: [pocet riadkov] [pocet vstupov] [pocet vystupov]*/
	fprintf(file_fann_training,"%d %d %d\n",
			fann_file_num_patterns,
			ANN_TRAINING_PATTERN_NUM_INPUTS,
			ANN_TRAINING_PATTERN_NUM_OUTPUTS);

	/* Skopiruj obsah z docasneho suboru */
	rewind(tmp_training);
	while((len = fread(buffer,sizeof(char),1024,tmp_training)) > 0){
		fwrite(buffer,sizeof(char),len,file_fann_training);
	}

	remove(tmp_output_file_name->str);

	g_string_free(tmp_output_file_name,TRUE);
	fclose(tmp_training);
	fclose(file_fann_training);
	mysql_free_result(result);
}

/**
 * Vytvori vstupny trenovaci/testovaci subor pre FANN kniznicu
 *
 * @param db_table_name
 * @param output_file_name
 */
void create_fann_file_frame(const char *db_table_name, const char *output_file_name){

	MYSQL_RES *result;

	result = db_query_select_all(db_table_name);

	if((file_fann_training = fopen(output_file_name,"w+")) == NULL){
		fprintf(stderr,"ERROR: \"%s\" fopen failed\n",output_file_name);
		exit(EXIT_FAILURE);
	}

	/* Format: [pocet riadkov] [pocet vstupov] [pocet vystupov]*/
	fprintf(file_fann_training,"%ld %d %d\n",
			db_data_count(DB_TABLE_NAME_TRAINING),
			ANN_TRAINING_NUM_INPUTS,
			ANN_TRAINING_NUM_OUTPUTS);

	db_table_foreach(result,foreach_frame_to_fann_file);

	fclose(file_fann_training);
	mysql_free_result(result);
}

void anal_analysis_run(char *trainfile_frame, char *trainfile_pattern, gboolean only_train_frame, gboolean only_train_pattern){

	anal_init(&hash_table_ap_stats,NULL);

	hash_table_ap_pattern = g_hash_table_new((GHashFunc)g_int64_hash, (GEqualFunc)g_int64_equal);

	if(hash_table_ap_pattern == NULL){
		fprintf(stderr,"ERROR: g_hash_table_new failed.\n");
		exit(EXIT_FAILURE);
	}

	if(only_train_frame == FALSE){
		strcpy(trainfile_frame,APP_FILENAME_TRAINING);
	}

	if(only_train_pattern == FALSE){
		strcpy(trainfile_pattern,APP_FILENAME_TRAINING_PATTERN);
	}

	/* Ak sa len trenuje z poskytnuteho suboru neinicializuj mysql */
		if(!only_train_frame && !only_train_pattern) db_init();

		/* Vytvaraj subor len ak este neexistuje */
			if((fopen(trainfile_frame,"r") == NULL) || (fopen(trainfile_pattern,"r") == NULL)){

				if(only_train_frame) {
					fprintf(stderr,"Error: File \"%s\" doesn\'t exists!\n",trainfile_frame);
					exit(EXIT_FAILURE);
				}

				if(only_train_pattern) {
					fprintf(stderr,"Error: File \"%s\" doesn\'t exists!\n",trainfile_pattern);
					exit(EXIT_FAILURE);
				}

				puts("***********************************************************");
				puts("*                Creating training files                  *");
				puts("***********************************************************");
				printf("Creating \"frame\" training file %s............",trainfile_frame);
				fflush(stdout);
				create_fann_file_frame(DB_TABLE_NAME_TRAINING, trainfile_frame);
				puts("OK");
				printf("Creating \"pattern\" training file %s........",trainfile_pattern);
				fflush(stdout);
				create_fann_file_pattern(DB_TABLE_NAME_TRAINING, trainfile_pattern);
				puts("OK");
				puts("\nCOMPLETE\n");
			}


			/* Ak existuje natrenovana siet, tak netrenuj */
			if(((fopen(ANN_FILENAME_NET,"r") == NULL) && (only_train_frame == 0) && (only_train_pattern == 0))
					|| only_train_frame){
				flag_phase_training = 1;
				puts("***********************************************************");
				puts("*                     Network training                    *");
				puts("***********************************************************");
				puts("Frame network\n");
				ann_nets.ann_frame = ann_network_frame_init();
				ann_training_parallel(ann_nets.ann_frame,trainfile_frame, ANN_FILENAME_NET, ANN_TYPE_FRAME);
				puts("\nFrame network training complete\n\n");
				flag_phase_training = 0;
			}

			/* PATTERN */
			if(((fopen(ANN_FILENAME_NET_PATTERN,"r") == NULL) && (only_train_frame == 0) && (only_train_pattern == 0))
					|| only_train_pattern){
				flag_phase_training = 1;
				puts("Pattern network");
				ann_nets.ann_pattern = ann_network_pattern_init();
				ann_training_parallel(ann_nets.ann_pattern,trainfile_pattern, ANN_FILENAME_NET_PATTERN, ANN_TYPE_PATTERN);
				puts("\nPattern network training complete\n");
				flag_phase_training = 0;
			}

			if(only_train_frame || only_train_pattern) exit(EXIT_SUCCESS);

			/* Treba vyprazdnit, hash tabulky budu znovu pouzite */
			g_hash_table_remove_all(hash_table_ap_pattern);
			g_hash_table_remove_all(hash_table_ap_stats);


			puts("***********************************************************");
			puts("*                      Classification                     *");
			puts("***********************************************************");
	#ifdef FRAME_CLASSIFICATION
			if(ann_nets.ann_frame == NULL){
				ann_nets.ann_frame = ann_network_init_from_file(ANN_FILENAME_NET);
			}
			printf("Frames classification....................................");
			fflush(stdout);
	#endif
			classify_frames();
	#ifdef FRAME_CLASSIFICATION
			puts("OK");
	#endif

			frame_counter = 0;
			pattern_counter = 0;

			printf("Patterns classification..................................");
			fflush(stdout);
			if(ann_nets.ann_pattern == NULL){
				ann_nets.ann_pattern = ann_network_init_from_file(ANN_FILENAME_NET_PATTERN);
			}
			classify_patterns();
			puts("OK\n");
			puts("COMPLETE\n");


			puts("***********************************************************");
			puts("*                         Results                         *");
			puts("***********************************************************");
			puts("\n");
	#ifdef FRAME_CLASSIFICATION
			puts("-------------------Frame classification--------------------");
			stats_print(stats_compute_for_frames(ANAL_STATS_FILENAME_ORIG,ANAL_STATS_FILENAME_PREDICTION), ANN_TYPE_FRAME);
			printf("\n\nhit_frames: %d\n",hit_frames);
			printf("miss_frames: %d\n",miss_frames);
			printf("dropped_frames: %d\n",dropped_frames);
			printf("data_frames: %d\n",data_frames);
			printf("attack_data_frames: %d\n",attack_data_frames);
			puts("\n");
	#endif
			puts("------------------Pattern classification-------------------");
			stats_print(stats_compute_for_patterns(ANAL_STATS_FILENAME_ORIG_PATTERN,ANAL_STATS_FILENAME_PREDICTION_PATTERN),ANN_TYPE_PATTERN);
			puts("\nCOMPLETE\n");
}

/**
 * Inicializuje vsetky potrebne struktury.
 */
void anal_init(GHashTable **hash_table_detection, ann_t *detection_networks){

	struct sigaction sig_int;

	sig_int.sa_flags = 0;
	sig_int.sa_handler = sig_int_handler;
	sigemptyset(&sig_int.sa_mask);

	if(sigaction(SIGINT,&sig_int,NULL)){
		fprintf(stderr,"Error: sigaction()\n");
		exit(EXIT_FAILURE);
	}

	*hash_table_detection = g_hash_table_new((GHashFunc)g_int64_hash, (GEqualFunc)g_int64_equal);


	if(hash_table_detection == NULL){
		fprintf(stderr,"ERROR: g_hash_table_new failed.\n");
		exit(EXIT_FAILURE);
	}

	if(detection_networks != NULL) ann_nets = *detection_networks;
}
