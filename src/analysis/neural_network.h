/*
 * neural_network.h
 *
 *  Created on: 23.1.2014
 *      Author: rdy
 */

#include "attack.h"

#ifndef NEURAL_NETWORK_H_
#define NEURAL_NETWORK_H_

#define APP_FILENAME_TRAINING 					"frame.training"
#define APP_FILENAME_TRAINING_PATTERN			"pattern.training"
#define APP_FILENAME_TEST 						"data.test"
#define ANN_FILENAME_NET						"frame.net"
#define ANN_FILENAME_NET_PATTERN				"pattern.net"

#define ANN_TRAINING_NUM_INPUTS					14
#define ANN_TRAINING_NUM_OUTPUTS				ATK_COUNT_ALL
#define ANN_TRAINING_NUM_PROCS					4
#define ANN_TRAINING_EPOCHS_BETWEEN_REPORTS		5
#define ANN_TRAINING_MAX_EPOCHS					2200
#define ANN_TRAINING_DESIRED_ERROR				0.0000300000

#define ANN_TRAINING_PATTERN_NUM_INPUTS			16
#define ANN_TRAINING_PATTERN_NUM_OUTPUTS		ATK_COUNT_ALL
#define ANN_TRAINING_PATTERN_MAX_EPOCHS			50000
#define ANN_TRAINING_PATTERN_DESIRED_ERROR		0.0000100000
#define ANN_TRAINING_PATTERN_LENGTH				140
#define ANN_TRAINING_PATTERN_LIMIT				0.49f

#define ANN_CLASSIFICATION_THRESHOLD			0.47f
#define ANN_CLASSIFICATION_PATTERN_THRESHOLD	0.73f

#include <fann.h>
#include <signal.h>

#include "common.h"

typedef struct{
	struct fann *ann_frame;
	struct fann *ann_pattern;
} ann_t;

void ann_network_init_all_from_file(ann_t *ann);
struct fann *ann_network_frame_init();
struct fann *ann_network_pattern_init();
struct fann *ann_network_init_from_file(const char *filename);
void ann_training_stop();
fann_type *ann_input_classify(struct fann *ann, fann_type input[]);
void ann_training(struct fann *ann, const char *filename_train, const char *filename_output_net);
void ann_training_parallel(struct fann *ann, const char *filename_train, const char *filename_output_net, ann_type type);

#endif /* NEURAL_NETWORK_H_ */
