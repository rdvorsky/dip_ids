/*
 * stats.c
 *
 *  Created on: 18.2.2014
 *      Author: rdy
 */

#include "stats.h"

/**
 * Vypocita a vypise na stdout statistiky z dat ulozenych v strukture
 * statistics_t stats.
 *
 * @param stats statistics_t
 * @param type ANN_TYPE_FRAME | ANN_TYPE_PATTERN
 */
void stats_print(statistics_t stats, ann_type type){

	int i, start = 0, stop = ATK_COUNT_ALL;

	if(type == ANN_TYPE_FRAME){
		start = 0;
		stop = ATK_COUNT_ALL;
	}

	for (i = start; i < stop; ++i){

		if(type == ANN_TYPE_PATTERN){
			stats.false_positive_rate[i] = (double)stats.num_positive[i]/(double)stats.num_patterns;
			stats.false_negative_rate[i] = (double)stats.num_negative[i]/(double)stats.num_patterns;
			stats.succes_rate[i] = (double)stats.num_success[i]/(double)stats.num_patterns;
			stats.hit_rate[i] = (double)stats.num_hit_success[i]/(double)stats.num_hit[i];
		}else{
			stats.false_positive_rate[i] = (double)stats.num_positive[i]/(double)stats.num_rows;
			stats.false_negative_rate[i] = (double)stats.num_negative[i]/(double)stats.num_rows;
			stats.succes_rate[i] = (double)stats.num_success[i]/(double)stats.num_rows;
			stats.hit_rate[i] = (double)stats.num_hit_success[i]/(double)stats.num_hit[i];
		}

		printf("\n\t\t%s\n",atk_attack_names[i]);
		printf("false positive rate: \t\t\t%02.10f %%\n",stats.false_positive_rate[i]*100);
		printf("false negative rate: \t\t\t%02.10f %%\n",stats.false_negative_rate[i]*100);
		printf("hit rate: \t\t\t\t%02.10f %%\n",stats.hit_rate[i]*100);
		printf("success rate: \t\t\t\t%02.10f %%\n",stats.succes_rate[i]*100);
	}
}


/**
 * Na zaklade zadanych suborov porovna ich obsah a vypocita
 * data sluziace na konecne statistiky uspesnosti detekcie vzorov.
 *
 * @param file_real_name
 * @param file_prediction_name
 * @return statistics_t
 */
statistics_t stats_compute_for_patterns(const char *file_real_name, const char *file_prediction_name){

	statistics_t stats;
	FILE *file_real = fopen(file_real_name,"r");
	FILE *file_prediction = fopen(file_prediction_name,"r");
	int input_real[ATK_COUNT_ALL];
	int input_prediction[ATK_COUNT_ALL];
	int atk_sum[ATK_COUNT_ALL];
	int i, result_prediction = 0, result_orig = 0, id_pred, id_orig, flag_jump_back = 0;
	long position;

	memset(&stats,0,sizeof(stats));

	while(1){

		memset(atk_sum,0,sizeof(atk_sum));

		/* ID */
		result_prediction = fscanf(file_prediction,"%d ",&id_pred);

		/* Hodnoty */
		for (i = 0; i < ATK_COUNT_ALL; ++i) {
			if((i == (ATK_COUNT_ALL - 1))){
				result_prediction = fscanf(file_prediction,"%d\n",&input_prediction[i]);
			}else{
				result_prediction = fscanf(file_prediction,"%d ",&input_prediction[i]);
			}
		}

		/* Nie je co porovnavat, skonci */
		if(result_prediction == EOF) break;

		/* Porovnavaj zo vsetkymi rovnakymi ID */
		while(1){

			if(flag_jump_back){
				/* Nastav poziciu na predchadzajuce id a porovnaj znovu */
				fseek(file_real,position,SEEK_SET);
				flag_jump_back = 0;
			}else{
				position = ftell(file_real);
			}

			/* ID */
			result_orig = fscanf(file_real,"%d ",&id_orig);


			/* Hodnoty */
			for (i = 0; i < ATK_COUNT_ALL; ++i) {
				if((i == (ATK_COUNT_ALL - 1))){
					result_orig = fscanf(file_real,"%d\n",&input_real[i]);
				}else{
					result_orig = fscanf(file_real,"%d ",&input_real[i]);
				}
			}

			/* Nie je co citat */
			if(result_orig == EOF) goto end_fnc;

			/* Rozne ID, treba prejst na nove ID */
			if(id_orig != id_pred){
				flag_jump_back = 1;
				break;
			}

			/* Subor obsahuje niekolko rovnakych ID, treba vypocitat sumu */
			for (i = 0; i < ATK_COUNT_ALL; ++i) {
				atk_sum[i] += input_real[i];
			}


			stats.num_rows++;
		}


		/* Pocitaj statistiky */
		for (i = 0; i < ATK_COUNT_ALL; ++i){

			if(atk_sum[i] >= 1) stats.num_hit[i]++;

			if(atk_sum[i] >= 1 && input_prediction[i] == 0){
				stats.num_negative[i]++;
			}else if(atk_sum[i] == 0 && input_prediction[i] == 1){
				stats.num_positive[i]++;
			}else{
				if(atk_sum[i] >= 1 && input_prediction[i] == 1){
					stats.num_hit_success[i]++;
				}

				stats.num_success[i]++;
			}
		}

		stats.num_patterns++;
	}

end_fnc:
	fclose(file_real);
	fclose(file_prediction);

	return stats;
}

/**
 * Na zaklade zadanych suborov porovna ich obsah a vypocita
 * data sluziace na konecne statistiky uspesnosti detekcie ramcov.
 *
 * @param file_real_name
 * @param file_prediction_name
 * @return statistics_t
 */
statistics_t stats_compute_for_frames(const char *file_real_name, const char *file_prediction_name){

	statistics_t stats;
	FILE *file_real = fopen(file_real_name,"r");
	FILE *file_prediction = fopen(file_prediction_name,"r");
	int input_real[ATK_COUNT_ALL];
	int input_prediction[ATK_COUNT_ALL];
	int i, result_prediction = 0, result_orig = 0;


	memset(&stats,0,sizeof(stats));

	while(1){

		/* Nacitaj zo suborov hodnoty */
		for (i = 0; i < ATK_COUNT_ALL; ++i) {
			if((i == (ATK_COUNT_ALL - 1))){
				result_prediction 	= fscanf(file_prediction,"%d\n",&input_prediction[i]);
				result_orig 		= fscanf(file_real,"%d\n",&input_real[i]);
			}else{
				result_prediction 	= fscanf(file_prediction,"%d ",&input_prediction[i]);
				result_orig 		= fscanf(file_real,"%d ",&input_real[i]);
			}

			/* Nie je ziadna predpoved, nie je treba pokracovat */
			if(result_prediction == EOF) {
				goto exit_fnc;
			}
		}

		/* Testovacie data neobsahuju ziadne utocne patterny, nastav vsade 0 */
		if(result_orig == EOF){
			for (i = 0; i < ATK_COUNT_ALL; ++i) {
				input_real[i] = 0;
			}
		}

		/* Pocitaj statistiky */
		for (i = 0; i < ATK_COUNT_ALL; ++i){

			if(input_real[i] == 1) stats.num_hit[i]++;

			if(input_real[i] == 1 && input_prediction[i] == 0){
				stats.num_negative[i]++;
			}else if(input_real[i] == 0 && input_prediction[i] == 1){
				stats.num_positive[i]++;
			}else{
				if(input_real[i] == 1 && input_prediction[i] == 1) stats.num_hit_success[i]++;

				stats.num_success[i]++;
			}
		}
		stats.num_rows++;
	}

exit_fnc:
	fclose(file_real);
	fclose(file_prediction);

	return stats;
}

