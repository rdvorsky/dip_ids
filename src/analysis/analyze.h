/*
 * analyze.h
 *
 *  Created on: 2.3.2014
 *      Author: rdy
 */

#ifndef ANALYZE_H_
#define ANALYZE_H_

#include <glib.h>

#include "neural_network.h"

#define ANAL_STATS_OUTPUT_DIR					"output/"
#define ANAL_STATS_FILENAME_ORIG 				ANAL_STATS_OUTPUT_DIR "class_frame.orig"
#define ANAL_STATS_FILENAME_NET					ANAL_STATS_OUTPUT_DIR "classification.output"
#define ANAL_STATS_FILENAME_PREDICTION			ANAL_STATS_OUTPUT_DIR "class_frame.pred"
#define ANAL_CLASS_FRAME_OUTPUT					ANAL_STATS_OUTPUT_DIR "frame.data"
#define ANAL_STATS_FILENAME_STATS				ANAL_STATS_OUTPUT_DIR "frame.data.stats"
#define ANAL_STATS_FILENAME_ORIG_PATTERN		ANAL_STATS_OUTPUT_DIR "class_pattern.orig"
#define ANAL_STATS_FILENAME_PREDICTION_PATTERN	ANAL_STATS_OUTPUT_DIR "class_pattern.pred"

#define ANAL_PATTERN_LEN_DEFAULT				800
#define ANAL_PATTERN_LEN_DEAUTH					ANAL_PATTERN_LEN_DEFAULT
#define ANAL_PATTERN_LEN_KOREK					140
#define ANAL_PATTERN_LEN_FRAG					30
#define ANAL_PATTERN_LEN_PTW					ANAL_PATTERN_LEN_DEFAULT
#define ANAL_PATTERN_LEN_AUTH					ANAL_PATTERN_LEN_DEFAULT
#define ANAL_PATTERN_LEN_CTS					ANAL_PATTERN_LEN_DEFAULT
#define ANAL_PATTERN_LEN_BT						ANAL_PATTERN_LEN_DEFAULT
#define ANAL_PATTERN_LEN_HOLE196				4

typedef struct{
	int			actual_pattern_lenght;
	int			actual_aid;
	float		ctl_frame_rate;
	float		mgt_frame_rate;
	float		data_frame_rate;
	float		beacon_rate;
	float		auth_rate;
	float		deauth_rate;
	float		ack_rate;
	float		rts_rate;
	float		cts_rate;
	float		fragmented_rate;
	float		retry_rate;
	float		data_opn_rate;
	float		data_wep_rate;
	float		data_wpa_rate;
	float		total_in_rate;
	float		total_out_rate;
	u_int32_t	avg_dur;
	u_int32_t	avg_size;
	float		fake_dst_addr_rate;
	float		broadcast_dst_addr_rate;
	/*double		elapsed_time;*/
} pattern_stats_t;

typedef struct{
	float auth_rate;
	float deauth_rate;
	float cts_rate;
} dos_pattern_stats_t;

extern const int 	anal_pattern_lengts[ATK_COUNT_ALL + 1];

void anal_analysis_run(char *trainfile_frame, char *trainfile_pattern, gboolean only_train_frame, gboolean only_train_pattern);
void anal_init(GHashTable **hash_table_detection, ann_t *detection_networks);
ap_stats_t *anal_ap_stats_get_instance(GHashTable *hash_table, frame_data_t frame);
void anal_ap_stats_refresh(ap_stats_t *ap, frame_data_t frame);
ap_stats_t *anal_ap_stats_refresh_old(GHashTable *hash_table, frame_data_t data);
void anal_ap_stats_reset(ap_stats_t *ap);
void anal_ap_stats_reset_pattern(ap_stats_t *ap, int pattern_len);
fann_type *anal_frame_to_array(frame_data_t *frame);
fann_type *anal_pattern_stats_to_array(pattern_stats_t *stats);
void anal_pattern_stats_write(pattern_stats_t *stats, FILE *file);
void anal_pattern_stats_calc(ap_stats_t *ap, pattern_stats_t *stats);
fann_type *anal_pattern_prediction_classify(fann_type *raw_data, dos_pattern_stats_t *stats_dos,u_int8_t output_prediction_vector[ATK_COUNT_ALL]);
int anal_make_decision(fann_type *input, ann_type type);
int anal_frame_prediction_classify(frame_data_t frame);

#endif /* ANALYZE_H_ */
