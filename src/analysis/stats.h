/*
 * stats.h
 *
 *  Created on: 18.2.2014
 *      Author: rdy
 */

#ifndef STATS_H_
#define STATS_H_

#include "attack.h"

typedef struct statistics{
	long num_rows;
	long num_patterns;
	long num_positive[ATK_COUNT_ALL];
	long num_negative[ATK_COUNT_ALL];
	long num_success[ATK_COUNT_ALL];
	long num_hit[ATK_COUNT_ALL];
	long num_hit_success[ATK_COUNT_ALL];
	double succes_rate[ATK_COUNT_ALL];
	double false_positive_rate[ATK_COUNT_ALL];
	double false_negative_rate[ATK_COUNT_ALL];
	double hit_rate[ATK_COUNT_ALL];
} statistics_t ;

void stats_print(statistics_t stats, ann_type type);
statistics_t stats_compute_for_patterns(const char *file_real_name, const char *file_prediction_name);
statistics_t stats_compute_for_frames(const char *file_real_name, const char *file_prediction_name);

#endif /* STATS_H_ */
