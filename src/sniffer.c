/*#define DEBUG_MANAGEMENT_FRAME*/
/*#define DEBUG_CONTROL_FRAME*/
/*#define DEBUG_DATA_FRAME*/

#define GET_LAST_BITS(x,n) ((x) & ((1<<(n))-1))
#define GET_BITS(x,m,n) GET_LAST_BITS((x)>>(m),((n)-(m)))

#define SNF_INPUT_BUFFER_SIZE 	512
#define SNF_KERNEL_BUFFER_SIZE	20000

#include <glib.h>
#include <pcap.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <net/if.h>
#include <stdint.h>
#include <pthread.h>
#include <unistd.h>
#include <assert.h>

#include "nl80211.h"
#include "database.h"
#include "common.h"
#include "attack.h"
#include "sniffer.h"
#include "debug.h"
#include "gui.h"

#include "radiotap-parser.h"

static char* 		mon_iface;
pcap_t* 			pcap_descr 						= NULL;
struct pcap_stat 	stats;
static u_int64_t	frame_counter 					= 0;
static GHashTable 	*hash_table_ap_overall 			= NULL;
static GHashTable 	*hash_table_ap_detection 		= NULL;
static gboolean		intrusion_detection				= FALSE;
static snf_ids_t	ids;

u_int64_t snf_mac_to_uint64(u_int8_t	i_addr[IEEE80211_ADDR_LEN]){
	return ((u_int64_t)i_addr[0] << 40) | ((u_int64_t)i_addr[1] << 32) |
		   ((u_int64_t)i_addr[2] << 24) | ((u_int64_t)i_addr[3] << 16) |
		   ((u_int64_t)i_addr[4] << 8)  | ((u_int64_t)i_addr[5]);
}

u_int64_t snf_bssid64_get(frame_data_t data, struct ieee80211_frame *frame){
	u_int64_t bssid = 0;

	if(data.fc_type == IEEE80211_FC0_TYPE_MGT){
		bssid = snf_mac_to_uint64(frame->i_addr3);
	}else if(data.fc_type == IEEE80211_FC0_TYPE_DATA){
		if(data.fc_from_ds == 1){
			bssid = snf_mac_to_uint64(frame->i_addr2);
		}else if(data.fc_to_ds == 1){
			bssid = snf_mac_to_uint64(frame->i_addr1);
		}
	}else if(data.fc_type == IEEE80211_FC0_TYPE_CTL){
		bssid = snf_mac_to_uint64(frame->i_addr1);
	}

	return bssid;
}


/**
 * Pomocna parsovacia funkcia, ktora parsuje radiotap hlavicku.
 * @see http://www.radiotap.org/
 *
 * @param frame
 * @param len
 * @return Dlzka hlavicky
 */
u_int16_t snf_radiotap_parse(const u_char *frame, u_int32_t len){

	struct ieee80211_radiotap_header *radiotap;
	struct ieee80211_radiotap_iterator iterator;

	radiotap = (struct ieee80211_radiotap_header *)frame;

	if (radiotap->it_version > PKTHDR_RADIOTAP_VERSION){
		fprintf(stderr,"Unsuported version of radiotap\n");
		return 0;
	}

	if (radiotap->it_len < 8 || radiotap->it_len > len){
		fprintf(stderr,"Invalid packet length\n");
		return 0;
	}

	ieee80211_radiotap_iterator_init(&iterator,radiotap,radiotap->it_len);

	while (ieee80211_radiotap_iterator_next(&iterator) >=0 ){
		switch (iterator.this_arg_index){
			case IEEE80211_RADIOTAP_CHANNEL:
				frame_data.channel = (u_int8_t)nl80211_freq_to_channel(
						(u_int32_t)((iterator.this_arg[1])*256+(iterator.this_arg[0])));
				break;
			case IEEE80211_RADIOTAP_DBM_ANTSIGNAL:
				frame_data.signal = (int8_t)*iterator.this_arg;
				break;
			case IEEE80211_RADIOTAP_DBM_ANTNOISE:
				/*packet_data.noise=*iterator.this_arg-256;*/
				break;
		}
	}

	return radiotap->it_len;
}

/**
 * Pomocna parsovacia funkcia, ktora parsuje management frame a hodnoty uklada do db.
 *
 * @param wh
 * @param fct
 * @param totlen
 */
gboolean snf_frame_management_process(struct ieee80211_frame *wh, int totlen){

	unsigned char *frame_start = (unsigned char *)(wh + 1);
	totlen -= sizeof(*wh);

	memset(frame_data.ssid,0,sizeof(char)*256);

	frame_start += 12;	 					 /* 12 = fixed parameters size */
	totlen -= 12;

	/* prechazda a parsuje Tagged parameters */
	while(totlen > 2){
		unsigned int tag_id = *frame_start++; 			 /* tag */
		unsigned int tag_length = *frame_start++; 		 /* dlzka */
		totlen -= 2;

		switch (tag_id) {
			case IEEE80211_ELEMID_SSID:

				/* skryte ssid alebo broadcast probe request */
				if(tag_length == 0 || frame_start[0] == 0)
					break;

				memcpy(frame_data.ssid,frame_start,tag_length);
				frame_data.ssid[tag_length] = 0;

				/* Pri parsovani niektorych framov vznika nejaky bordel */
				if(!g_utf8_validate(frame_data.ssid,256,NULL) == FALSE){
					return FALSE;
				}
				else if(!comm_is_string(frame_data.ssid)){
					return FALSE;
				}
				else if(strlen(frame_data.ssid) != tag_length){
					return FALSE;
				}

				if(tag_length > 256){
					memset(frame_data.ssid,0,sizeof(frame_data.ssid));
				}

				break;
			case IEEE80211_ELEMID_DSPARMS:
				frame_data.channel = frame_start[0];
				break;
			/*case IEEE80211_ELEMID_VENDOR:
				elem_vendor = parse_elem_vendor(&frame_start[-2], tag_length + 2);
				break;
			case IEEE80211_ELEMID_RSN:
				rsn = parse_rsn(frame_start, tag_length, 1);
				break;*/
			default:
				break;
		}

		/* prejdi na dalsi tag */
		frame_start += tag_length;
		totlen -= tag_length;
	}

#ifdef DEBUG_MANAGEMENT_FRAME
	printw("\n----------beacon---------\n");
	printw("ssid: %s\n", frame_data.ssid);
	printw("channel: %d\n", frame_data.channel);
	printw("-------------------------\n");
#endif


	return TRUE;
}

/**
 * Pomocna parsovacia funkcie, ktora parsuje control frame, pricom hodnoty ulozi do db.
 *
 * @param frame
 * @param frame_control
 */
void snf_frame_control_process(struct ieee80211_frame *frame,
                           struct ieee80211_frame_control *frame_control){

	struct ieee80211_frame_cts *frame_cts;
	struct ieee80211_frame_rts *frame_rts;
	struct ieee80211_frame_ack *frame_ack;

	strcpy(frame_data.addr2,"NULL");
	strcpy(frame_data.addr3,"NULL");
	strcpy(frame_data.addr4,"NULL");

    switch(frame_control->subtype << IEEE80211_FC0_SUBTYPE_SHIFT){
    case IEEE80211_FC0_SUBTYPE_CTS:
        frame_cts = (struct ieee80211_frame_cts *)(frame);
        comm_mac_to_string(frame_cts->i_ra,frame_data.addr1);
        memcpy(&frame_data.dur,frame_cts->i_dur,sizeof(u_int16_t));
        break;
	case IEEE80211_FC0_SUBTYPE_RTS:
	    frame_rts = (struct ieee80211_frame_rts *)(frame);
	    comm_mac_to_string(frame_rts->i_ra,frame_data.addr1);
	    comm_mac_to_string(frame_rts->i_ta,frame_data.addr2);
	    memcpy(&frame_data.dur,frame_rts->i_dur,sizeof(u_int16_t));
	    break;
	case IEEE80211_FC0_SUBTYPE_ACK:
	    frame_ack = (struct ieee80211_frame_ack *)(frame);
	    comm_mac_to_string(frame_ack->i_ra,frame_data.addr1);
	    memcpy(&frame_data.dur,frame_ack->i_dur,sizeof(u_int16_t));
	    break;
	default:
		break;
    }

#ifdef DEBUG_CONTROL_FRAME
    printw("Control frame\n");
#endif
}


/**
 * Pomocna parsovacia funkcia, ktora parsuje datovy frame,
 * pricom hodnoty atributov spolu s typom sifrovania ulozi do db.
 *
 * @param frame
 * @param frame_control
 */
void snf_frame_data_process(struct ieee80211_frame *frame,
                        struct ieee80211_frame_control *frame_control){

	int wep = 1;
	char mask = 0x3f;/* 6 bitov padding je nastaveny na 0 */
	int encoded = frame->i_fc[1] & IEEE80211_FC1_WEP;
	GString *key_dst;

    /* Odstrani mull data packety */
    if((frame_control->subtype << IEEE80211_FC0_SUBTYPE_SHIFT) == IEEE80211_FC0_SUBTYPE_NODATA ||
    		(frame_control->subtype << IEEE80211_FC0_SUBTYPE_SHIFT) == IEEE80211_FC0_SUBTYPE_QOS_NULL){
    	return;
    }

    /* Z dat zisti pouzite kodovanie */
    if((frame_control->subtype << IEEE80211_FC0_SUBTYPE_SHIFT) == IEEE80211_FC0_SUBTYPE_QOS){
    	wep = ((struct ieee80211_qosframe_wep *)frame)->i_wep[3] & mask;
    }else if((frame_control->subtype << IEEE80211_FC0_SUBTYPE_SHIFT) == IEEE80211_FC0_SUBTYPE_DATA){
    	wep = ((struct ieee80211_frame_wep *)frame)->i_wep[3] & mask;
    }

    if(frame_control->protected_frame == 0){/* OPEN */
    	frame_data.cipher = CIPHER_OPN;
    }else if(wep == 0){/* WEP */
    	frame_data.cipher = CIPHER_WEP;
    }else if(encoded > 0 && wep != 0){/* CCMP/TKIP */
    	frame_data.cipher = CIPHER_CCMP_TKIP;
    }

    if(frame_data.fc_to_ds == 1){
    	key_dst = g_string_ascii_up(g_string_new(frame_data.addr3));
    	/* Broadcast prefix */
    	if(g_str_has_prefix((key_dst->str),"FF:") == TRUE){
    		/* Je broadcast */
    		frame_data.bcst_prefix = 1;
    		if(g_string_equal(key_dst,g_string_new("FF:FF:FF:FF:FF:FF")) == TRUE){
    			frame_data.bcst = 1;
    		}
    	}
    }else if(frame_data.fc_from_ds == 1){
    	key_dst = g_string_ascii_up(g_string_new(frame_data.addr1));
    	/* Broadcast prefix */
    	if(g_str_has_prefix((key_dst->str),"FF:") == TRUE){
    		/* Je broadcast */
    		frame_data.bcst_prefix = 1;
    		if(g_string_equal(key_dst,g_string_new("FF:FF:FF:FF:FF:FF")) == TRUE){
    			frame_data.bcst = 1;
    		}
    	}
    }


#ifdef DEBUG_DATA_FRAME
    printw("Data frame\n");
    printw("seq: %d\n",frame_data.seq);
    printw("type: %d\n",frame_data.fc_type);
    printw("subtype: %d\n",frame_data.fc_subtype);
    printw("wep: %d\n",wep);
    printw("encoded: %d\n",encoded);
    printw("cipher: %d\n",frame_data.cipher);
#endif

}

/**
 * Pomocna funkcia spracovavajuca atributy framu spolocne pre management a
 * data framy, ktore neobsahuje control frame.
 *
 * @param frame
 */
void snf_frame_common_process(struct ieee80211_frame *frame){

	memcpy(&frame_data.seq,frame->i_seq,sizeof(u_int16_t));
    frame_data.frag = GET_LAST_BITS(frame_data.seq,4);/* 4 bit fragment number */
    frame_data.seq = GET_BITS(frame_data.seq,4,16);/* dalsich 12 bit seq number */

    /* Ak je posledny bit nastaveny na 0, ide o duration */
    /*if((frame->i_dur[1] & (1 << 0)) == 0){*/
    	memcpy(&frame_data.dur,frame->i_dur,sizeof(u_int16_t));
    	/*packet_data.dur = EXTRACT_DUR_BITS(packet_data.dur);*/
    /*}*/
}

void snf_real_time_detection(frame_data_t frame, ap_stats_t *ap_stats_gui){

	dos_pattern_stats_t pattern_dos_stats;
	u_int8_t prediction_vector[ATK_COUNT_ALL];
	int aid_prediction = 0, i = 0;
	ap_stats_t *ap_stats_detecion;
	pattern_stats_t pattern_stats;
	float *pattern_stats_array;

	/* Klasifikuje ramec */
	aid_prediction = anal_frame_prediction_classify(frame);

	/* Vrati instanciu statistik pre dane AP */
	ap_stats_detecion = anal_ap_stats_get_instance(hash_table_ap_detection,frame);

	if(aid_prediction > 0){

		/* Este neprisiel beacon frame informujuci o existujucom AP */
		if(ap_stats_detecion == NULL) return;

		anal_ap_stats_refresh(ap_stats_detecion,frame);

		if(ap_stats_detecion->len_pattern == 0) ap_stats_detecion->len_pattern = anal_pattern_lengts[aid_prediction];

		if(ap_stats_detecion->counter_total == (unsigned int)ap_stats_detecion->len_pattern){
			anal_pattern_stats_calc(ap_stats_detecion,&pattern_stats);

			pattern_stats_array = anal_pattern_stats_to_array(&pattern_stats);
			pattern_dos_stats.auth_rate = pattern_stats.auth_rate;
			pattern_dos_stats.deauth_rate = pattern_stats.deauth_rate;
			pattern_dos_stats.cts_rate = pattern_stats.cts_rate;

			anal_pattern_prediction_classify(pattern_stats_array,&pattern_dos_stats,prediction_vector);

			for (i = ATK_COUNT_ALL-1; i >= 0; --i) {
				if(prediction_vector[i] == 1){
					ap_stats_detecion->aid = i+1;
					ap_stats_gui->aid = ap_stats_detecion->aid;
					break;
				}
			}

			anal_ap_stats_reset_pattern(ap_stats_detecion,anal_pattern_lengts[aid_prediction]);
		}
	}/*else{
    	if(frame.fc_type == IEEE80211_FC0_TYPE_MGT && ((frame.fc_subtype << IEEE80211_FC0_SUBTYPE_SHIFT) == IEEE80211_FC0_SUBTYPE_BEACON)){
    		return;
		}


		sprintf(test,"type: %d, subtype: %d, ssid: %ld\n",frame.fc_type,frame.fc_subtype,frame.bssid);
		waddstr(gui_window_bottom,test);
		wrefresh(gui_window_bottom);
    }*/
}

/**
 * Zakladna parsovacie funkcia na spracovanie Wi-Fi ramca. Funkcia
 * predpoklada zariadenie v monitor mode s pouzitim radiotap hlavicky.
 *
 * @param arg
 * @param pkthdr
 * @param packet
 */
void snf_frame_process(u_char *arg, const struct pcap_pkthdr* pkthdr, const
                    u_char* packet){

    int linktype = *arg, radiotap_len = 0;
    struct ieee80211_radiotap_header *radiotap_header;
    struct ieee80211_frame *frame;
    struct ieee80211_frame_addr4 *frame_addr4;
    struct ieee80211_frame_control *frame_control;
    ap_stats_t *ap_stats_gui;

    frame_counter++;

    memset(&frame_data,0,sizeof(frame_data_t));

    /* TODO: pridat podporu pre ostatne linklayer ak bude treba */
    switch(linktype){
        case DLT_IEEE802_11_RADIO:
            radiotap_header = (struct ieee80211_radiotap_header*) packet;
            radiotap_len = snf_radiotap_parse(packet,radiotap_header->it_len);
            break;
        default:
            fprintf(stderr,"Datalink not supported\n");
            exit(EXIT_FAILURE);
            break;
    }


    frame = (struct ieee80211_frame*) (packet + radiotap_len);
    frame_control = (struct ieee80211_frame_control*) (frame->i_fc);

    frame_data.fc_type = (frame->i_fc[0] & IEEE80211_FC0_TYPE_MASK);
    /*
    packet_data.fc_subtype = (frame->i_fc[0] & IEEE80211_FC0_SUBTYPE_MASK);
*/
    frame_data.fc_subtype = frame_control->subtype;

    frame_data.len = pkthdr->len;
    frame_data.timestamp = (double)pkthdr->ts.tv_sec + ((double)pkthdr->ts.tv_usec/(double)1000000);

    frame_data.cipher = -1;

    comm_mac_to_string(frame->i_addr1,frame_data.addr1);
    comm_mac_to_string(frame->i_addr2,frame_data.addr2);
    comm_mac_to_string(frame->i_addr3,frame_data.addr3);
    strcpy(frame_data.addr4,"NULL");

    /* Frame medzi AP obsahuje addr4 */
    if(frame_control->from_ds == 1 && frame_control->to_ds == 1){
    	frame_addr4 = (struct  ieee80211_frame_addr4 *) (packet + radiotap_len);
    	comm_mac_to_string(frame_addr4->i_addr4,frame_data.addr4);
    }

    frame_data.fc_from_ds = frame_control->from_ds;
    frame_data.fc_to_ds = frame_control->to_ds;
    frame_data.fc_more_frag = frame_control->more_frag;
    frame_data.fc_order = frame_control->order;
    frame_data.fc_protected_frame = frame_control->protected_frame;
    frame_data.fc_retry = frame_control->retry;
    frame_data.bcst = 0;
    frame_data.bcst_prefix = 0;

    /* Oznacenie ramca daneho utoku */
    if(atk_attack_attributes.attack_id > 0){
    	/* Na zaklade filtra sa rozhodne o oznaceni ramca */
    	frame_data.aid = (*attack_filters[atk_attack_attributes.attack_id])(frame_data);
    }

#ifdef DEBUG_FC
    debug_frame_control(frame_control);
#endif

    switch(frame_data.fc_type){
		case IEEE80211_FC0_TYPE_CTL:
			snf_frame_control_process(frame,frame_control);
			break;
		case IEEE80211_FC0_TYPE_MGT:
			snf_frame_common_process(frame);
			if(snf_frame_management_process(frame,pkthdr->len) == FALSE) {
				goto drop_frame;
			}
			break;
		case IEEE80211_FC0_TYPE_DATA:
			snf_frame_common_process(frame);
			snf_frame_data_process(frame,frame_control);
			break;
		default:
			break;
    }

    frame_data.bssid = snf_bssid64_get(frame_data,frame);

    if(snf_save) db_packet_data_insert();

    ap_stats_gui = anal_ap_stats_get_instance(hash_table_ap_overall,frame_data);

    if(ap_stats_gui == NULL) return;

    anal_ap_stats_refresh(ap_stats_gui,frame_data);

    if(intrusion_detection) snf_real_time_detection(frame_data,ap_stats_gui);



drop_frame:
    return;
}

/**
 * Vypise napovedu pre vstupne shell parametre pre sniffer.
 */
void snf_print_help(){
	printw(SNIFFER_SHELL_HELP);
}

void snf_attack_callback(void *param){
	UNUSED(param);
}

/**
 * Vlakno, ktore cita vstup zo stdin a na zaklade zadanych prikazov
 * spusta utoky, prip. ine prikazy.
 */
void *snf_thread_input(){

	int attack_id, c;
	char lost_frames[256];

	while(1){

		c = wgetch(gui_window_main);

		switch (c) {
		case 'a': /* Spusti vsetky ulozene utoky */
			gui_menu_show(&attack_id);

			if(attack_id == 0) break;

			atk_start(attack_id,mon_iface,nl80211_get_channel(),snf_attack_callback);

			break;
		case 'h':/* Zobrazi podporovane prikazy */
			gui_popup_show("HELP",SNIFFER_HELP,40,18);
			break;
		case 's':
			pcap_stats(pcap_descr,&stats);
			sprintf(lost_frames,
					"Recieved frames: \t%d\nDropped frames: \t%d\nInterface drop: \t%d\nOveral: \t\t%f %% \n",
					stats.ps_recv,stats.ps_drop,stats.ps_ifdrop,((float)stats.ps_drop/(float)stats.ps_recv)*100);
			gui_popup_show("CAPTURE STATISTICS",lost_frames,34,10);
			break;
		case 'q':
			endwin();
			exit(EXIT_SUCCESS);
			break;
		default:
			/* Nastavi priznak utoku
			attack_id = c;*/

			/* Nastav filter pre dany utok na zaklade id */
			/*atk_start(attack_id,mon_iface);*/
			break;
		}
	}

	return (void *)1;
}

/**
 * Inicializuje db, utoky a spusti sniffer.
 *
 * @param iface Nazov zachytavecieho zariadenia. Zariadenie musi byt v monitorovacom mode. (povinne)
 * @param bssid BSSID AP pre ktore za ma zachytavat datova prevadzka. (nepovinne)
 * @param channel Wi-Fi kanal v rozmedzi 1-14. Ak nie je zadane pouzije sa frequency hopping. (nepovinne)
 */
void snf_init(char *iface, char *bssid, int channel){


	struct bpf_program fp;
	int linktype;
	char bssid_filter[256];
	char errbuf[PCAP_ERRBUF_SIZE];

	memset(errbuf,0,PCAP_ERRBUF_SIZE);

	mon_iface = iface;
	ncurses_screen_data.iface = iface;

	/* Inicializacia pripojenia k mysql */
	db_init();

	/* Inicializacia libnl pouzivaneho k frequency hopping */
	nl80211_init();

	/* Nacitanie konfiguracii utokov */
	atk_init();


	if(intrusion_detection){
		ann_network_init_all_from_file(&ids.ann);
		anal_init(&hash_table_ap_detection,&ids.ann);
	}

	if(channel > 0 && channel <= 14){
		ncurses_screen_data.channel = channel;
		nl80211_set_channel(if_nametoindex(iface),channel);
		/*printw("Starting monitoring device %s on channel %d:\n",iface,channel);*/
	}else{
		nl80211_start_channel_hopping(iface);
		/*printw("Starting monitoring device %s on all channels:\n",iface);*/
	}
/*
	if(pcap_set_buffer_size(pcap_descr, SNF_KERNEL_BUFFER_SIZE) != 0){
		printw("Error: %s\n", pcap_geterr(pcap_descr));
		pcap_close(pcap_descr);
		exit(EXIT_FAILURE);
	}
*/
	pcap_descr = pcap_open_live(iface,MAXBYTES2CAPTURE,1,4082,errbuf);

	if(pcap_descr == NULL){
		fprintf(stderr,"%s\n",errbuf);
		exit(EXIT_FAILURE);
	}

	/* ak je zadane, tak filtruj na zaklade bssid */
	if(bssid != NULL){
		ncurses_screen_data.bssid = bssid;
		sprintf(bssid_filter,"wlan addr1 %s || wlan addr2 %s || wlan addr3 %s || wlan addr4 %s",bssid,bssid,bssid,bssid);
		if(pcap_compile(pcap_descr, &fp, bssid_filter, 0, PCAP_NETMASK_UNKNOWN) == -1){
			printw("\npcap_compile() failed: %s\n",pcap_geterr(pcap_descr));
			exit(EXIT_FAILURE);
		}

		pcap_setfilter(pcap_descr,&fp);
	}

	if(pcap_can_set_rfmon(pcap_descr) == 0){
		fprintf(stderr,"Error: device %s doesn\'t support monitor mode\n",iface);
		exit(EXIT_FAILURE);
	}

	linktype = pcap_datalink(pcap_descr);

	/* len podporovane linktype */
	if(linktype != DLT_IEEE802_11_RADIO &&
			linktype != DLT_PPI &&
			linktype != DLT_PRISM_HEADER &&
			linktype != DLT_IEEE802_11){

		fprintf(stderr,"Error: %s is not an IEEE802.11 device in monitor mode\n",iface);
		exit(EXIT_FAILURE);
	}

	hash_table_ap_overall = g_hash_table_new((GHashFunc)g_int64_hash, (GEqualFunc)g_int64_equal);

	gui_init(hash_table_ap_overall);

	comm_thread_init(snf_thread_input,NULL,FALSE,NULL);

	pcap_stats(pcap_descr,&stats);

	pcap_loop(pcap_descr,-1,snf_frame_process,(u_char *)&linktype);

	pcap_close(pcap_descr);
}

/**
 * Main pre sniffer.
 *
 * @param argc
 * @param argv
 * @return
 */
int main(int argc, char *argv[]){

	int c;
	int channel = -100;
	char *bssid = NULL, *cvalue = NULL, *iface = NULL;

	strcpy(db_actual_table_name,DB_TABLE_NAME_TEST);

	while ((c = getopt (argc, argv, "b:c:i:tnhr")) != -1){

    	switch (c) {
			case 'b':
				bssid = optarg;
				break;
			case 'c':
				cvalue = optarg;
				break;
			case 'i':
				iface = optarg;
				break;
			case 'h':
				snf_print_help();
				exit(EXIT_SUCCESS);
			case 't':
				ncurses_screen_data.is_training = 1;
				strcpy(db_actual_table_name,DB_TABLE_NAME_TRAINING);
				break;
			case 'n':
				ncurses_screen_data.is_no_save = 1;
				snf_save = 0;
				break;
			case 'r':
				intrusion_detection = TRUE;
				ncurses_screen_data.is_ids_active = 1;
				break;
			case '?':
				snf_print_help();
				exit(EXIT_FAILURE);
		}
    }

    if(iface == NULL){
    	snf_print_help();
    	exit(EXIT_FAILURE);
    }

    if(cvalue != NULL){
    	channel = atoi(cvalue);
    	if(channel < 0 && channel > 14){
    		snf_print_help();
    		exit(EXIT_FAILURE);
    	}
    }

    /* Vycisti obrazovku
    if(fork() == 0) {
    	execvp("clear", argv);
    	exit(1);
    }
*/
    snf_init(iface, bssid, channel);

    return 0;
}


