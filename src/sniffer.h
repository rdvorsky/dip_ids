#ifndef SNIFFER_H_INCLUDED
#define SNIFFER_H_INCLUDED

#include "include/ieee80211.h"
#include "analysis/analyze.h"

#define SNIFFER_HELP 		"a - Run attack\n"\
							"c - Close window\n"\
							"h - Show help\n"\
							"q - Quit\n"\
							"s - Show capture statistics\n\n"

#define SNIFFER_SHELL_HELP	"Params: \n"\
							" -b - bssid (nepovinne)\n"\
							" -c - channel (nepovinne)\n"\
							" -h - help \n"\
							" -i - interface (povinne)\n"\
							" -n - no save (nepovinne)\n"\
							" -r - real-time intrusion detection\n"\
							" -t - training data (nepovinne)\n"

#define MAXBYTES2CAPTURE 	65536

typedef struct{
	ann_t 	ann;
} snf_ids_t;


/**
 * Ak je nastavene na 0, data sa neukladaju do db.
 * Default hodnota je 1.
 */
u_int8_t	snf_save 		= 1;

void snf_init(char *iface, char *bssid, int channel);

#endif
