#include <stdio.h>
#include <stdlib.h>

#include "debug.h"


/**
 * @note momentalne nepouzite
 * @param ap
 */
void debug_ap_row(ap_stats_t *ap){
	puts("-----------------------------");
	printf("%s - %s\n",ap->bssid,ap->ssid);
	puts("-----------------------------");
	printf("bssid64: %ld\n",ap->bssid64);
	printf("counter_ack: %d\n",ap->counter_ack);
	printf("counter_assoc_disassoc: %d\n",ap->counter_assoc_disassoc);
	printf("counter_auth: %d\n",ap->counter_auth);
	printf("counter_beacon: %ld\n",ap->counter_beacon);
	printf("counter_ctl: %ld\n",ap->counter_ctl);
	printf("counter_cts: %d\n",ap->counter_cts);
	printf("counter_data: %ld\n",ap->counter_data);
	printf("counter_data_opn: %d\n",ap->counter_data_opn);
	printf("counter_data_wep: %d\n",ap->counter_data_wep);
	printf("counter_data_wpa: %d\n",ap->counter_data_wpa);
	printf("counter_deauth: %d\n",ap->counter_deauth);
	printf("counter_fragmented: %d\n",ap->counter_fragmented);
	printf("counter_mgt: %ld\n",ap->counter_mgt);
	printf("counter_retry: %d\n",ap->counter_retry);
	printf("counter_rts: %d\n",ap->counter_rts);
	printf("counter_total: %ld\n",ap->counter_total);
	printf("counter_total_in: %ld\n",ap->counter_total_in);
	printf("counter_total_out: %ld\n\n",ap->counter_total_out);
	printf("total_dur: %ld\n\n",ap->size_dur);
}

/**
 * Debug pre frame control.
 *
 * @param frame_control
 */
void debug_frame_control(struct ieee80211_frame_control *frame_control){
    printf("\n--------------frame control----------------\n");
    printf("protocol %d\n",frame_control->protocol);
    printf("type %d\n",frame_control->type);
    printf("subtype %d\n",frame_control->subtype);
    printf("to_ds %d\n",frame_control->to_ds);
    printf("from_ds %d\n",frame_control->from_ds);
    printf("more_frag %d\n",frame_control->more_frag);
    printf("retry %d\n",frame_control->retry);
    printf("pwr_mgt %d\n",frame_control->pwr_mgt);
    printf("more_data %d\n",frame_control->more_data);
    printf("protected_frame %d\n",frame_control->protected_frame);
    printf("order %d\n",frame_control->order);
    printf("--------------------------------------------\n");
}

/**
 * Debug pre packet_data_t strukturu.
 *
 * @param packet
 */
void debug_packet_struct(frame_data_t packet){
	printf("\n--------------PACKET_STRUCT----------------\n");
	printf("addr1: %s\n",packet.addr1);
	printf("addr2: %s\n",packet.addr2);
	printf("addr3: %s\n",packet.addr3);
	printf("addr4: %s\n",packet.addr4);
	printf("bssid: %ld\n",packet.bssid);
	printf("fc_type: %d\n",packet.fc_type);
	printf("fc_subtype: %d\n",packet.fc_subtype);
	printf("fc_from_ds: %d\n",packet.fc_from_ds);
	printf("fc_to_ds: %d\n",packet.fc_to_ds);
	printf("fc_more_frag: %d\n",packet.fc_more_frag);
	printf("fc_retry: %d\n",packet.fc_retry);
	printf("fc_protected_frame: %d\n",packet.fc_protected_frame);
	printf("fc_order: %d\n",packet.fc_order);
	printf("frag: %d\n",packet.frag);
	printf("seq: %d\n",packet.seq);
	printf("duration: %d\n",packet.dur);
	printf("ssid: %s\n",packet.ssid);
	printf("channel: %d\n",packet.channel);
	printf("signal: %d\n",packet.signal);
	printf("size: %d\n",packet.len);
	printf("cipher: %d\n",packet.cipher);
	printf("label: %d\n",packet.aid);
	printf("--------------------------------------------\n");
}

