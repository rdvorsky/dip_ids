#ifndef COMMON_H_INCLUDED
#define COMMON_H_INCLUDED

#include <glib.h>

#include "iniparser.h"

#define UNUSED(x) (void)(x)
#define ARRAY_LENGTH(x) (sizeof(x)/sizeof(*(x)))

#define SHELL_TEXT 		"#"				/* Text zobrazeni v terminaly */
#define SHELL_COLOR 	32 				/* Farba terminalu - zelena */
#define CONFIG_FILE 	"../config.ini"

#define CIPHER_EMPTY 0
#define CIPHER_OPN 1
#define CIPHER_WEP 2
#define CIPHER_CCMP_TKIP 3

/**
 * Odkaz na spolocny konfiguracny subor.
 * Momentalne k inicializacii dochadza vo funkcii db_init_mysql().
 */
dictionary *dict_config;

/**
 * Struktura obsahujuca vsetky informacie naparsovaneho ramca, ktore sa ukladaju do db.
 */
typedef struct {
	char 		addr1[20];
	char 		addr2[20];
	char 		addr3[20];
	char 		addr4[20];
	u_int64_t 	bssid;
	u_int8_t 	fc_from_ds;
	u_int8_t 	fc_to_ds;
	u_int8_t 	fc_more_frag;
	u_int8_t 	fc_retry;
	u_int8_t 	fc_protected_frame;
	u_int8_t 	fc_order;
	u_int8_t	 frag;
	u_int16_t 	seq;
	int 		fc_type;
	int 		fc_subtype;
	char 		ssid[256];
	int 		channel;
	int8_t 		signal;
	int 		len;
	double 		timestamp;
	u_int16_t 	dur;
	/* int enc;	OPN - 0, WEP - 1, WPA - 2, WPA2 -3 */
	int 		cipher; 	 /*  0 - prazdne, 1 - WEP, 2 - TKIP, 3 - CCMP (http://www.tech-faq.com/rsn-robust-secure-network.html) */
	int 		aid;
	int			bcst;
	int			bcst_prefix;
} frame_data_t;

frame_data_t frame_data;

typedef struct {
	char 		ssid[256];
	char 		bssid[20];
	u_int64_t 	bssid64;
	u_int64_t 	counter_total;
	u_int64_t 	counter_total_in;
	u_int64_t 	counter_total_out;
	u_int64_t 	counter_beacon;
	u_int64_t 	counter_ctl;
	u_int64_t 	counter_mgt;
	u_int64_t 	counter_data;
	u_int32_t 	counter_auth;
	u_int32_t 	counter_deauth;
	u_int32_t	counter_ack;
	u_int32_t	counter_rts;
	u_int32_t	counter_cts;
	u_int32_t 	counter_fragmented;
	u_int32_t 	counter_retry;
	u_int32_t	counter_data_opn;
	u_int32_t	counter_data_wep;
	u_int32_t	counter_data_wpa;
	int32_t 	counter_assoc_disassoc;
	u_int64_t 	size_total;
	u_int64_t	size_dur;
	int			channel;
	int			len_pattern;
	GHashTable	*hash_table_fake_dst_addr;
	u_int32_t	counter_fake_dst_addr;
	u_int32_t	counter_broadcast_dst_addr;
	u_int8_t	aid;
	double		time_first;
	double		time_last;
	int			counter_aid[20];
} ap_stats_t;

typedef enum {
	ANN_TYPE_FRAME, ANN_TYPE_PATTERN
} ann_type;

void comm_thread_init(void *(*thread_fnc)(void *), void *params, gboolean joinable, void (*callback)(void *ret_value));
void comm_mac_to_string(unsigned char *mac_address, char *out_string);
int comm_is_string(char *string);
int comm_start_with(char *text, char *prefix);
#endif /* COMMON_H_INCLUDED */
