/*
 * db.h
 *
 *  Created on: 28.11.2013
 *      Author: rdy
 */

#ifndef DB_H_
#define DB_H_

#include <mysql/my_global.h>
#include <mysql/mysql.h>

#include "common.h"

/**
 * Meno mysql tabulky obsahujucej testovacie data
 */
#define DB_TABLE_NAME_TEST 			"data"

/**
 * Meno mysql tabulky obsahujucej trenovacie data
 */
#define DB_TABLE_NAME_TRAINING 		"training"

/** Pociatocna maximalna dlzka query dotazu
 * treba zvolit tak, aby nebolo mensie ako dlzka jedneho dotazu
 */
#define DB_QUERY_DEFAULT_LEN   		50000

/**
 * Pocet dotazov v jednej davke
 */
#define DB_QUERY_GROUP_SIZE	   		1000

/**
 * Aktualne pripojenie k db.
 */
MYSQL 		*db_conn;

/**
 * Meno mysql tabulky, do ktorej sa ukladaju data.
 */
char 	db_actual_table_name[64];

/*
 * Aktualna velkost skupiny dotazov, po ktorej dojde k ulozeniu naparsovanych
 * dat do db.

int		db_query_actual_group_size;
 */
void
db_packet_data_insert();

void
db_init();

MYSQL_RES
*db_query_select_all_where(const char *table_name, const char *where);

MYSQL_RES
*db_query_select_all(const char *table_name);

void
db_table_foreach(MYSQL_RES *result, void (*row_func)(MYSQL_ROW row, MYSQL_FIELD *fields, u_int32_t num_rows, u_int16_t num_fields));

frame_data_t
db_row_to_frame(MYSQL_ROW row);

long
db_data_count(const char *table_name);

#endif /* DB_H_ */
