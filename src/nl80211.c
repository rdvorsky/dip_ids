/**
 * @author Radovan Dvorsky
 * @date 10.11.2013
 *
 * Zabezpecuje zmenu kanala pre potreby sniffera.
 *
 * Zdroj: @see http://harmattan-dev.nokia.com/docs/platform-api-reference/xml/daily-docs/osso-wlan/libnl-handler_8c_source.html
 */

#include <netlink/netlink.h>
#include <netlink/msg.h>
#include <netlink/attr.h>
#include <netlink/genl/ctrl.h>
#include <netlink/genl/genl.h>
#include <netlink/genl/family.h>
#include <net/if.h>
#include <unistd.h>

#include <pthread.h>
#include <time.h>

#include "include/nl80211.h"
#include "common.h"

/** Pocet skakanych kanalov.*/
#define HOP_CHANS_COUNT 14
/** Cas zmeny kanalu pri freq. hopping v sekundach. */
#define HOP_CHANS_SLEEP 1

static struct nl_cb 		*nl_cb 				= NULL;
static struct nl_sock 		*nl_sock 			= NULL;
static struct nl_cache 		*nl_cache 			= NULL;
static struct genl_family 	*nl_80211_family 	= NULL;
static struct nl_msg 		*msg 				= NULL;

static int actual_channel = 0;

/**
 * Poradie kanalov pre frequency hopping pouzivany pri zachytavni datovej prevadzky.
 */
static int hop_chans[] = {
    1, 7, 13, 2, 8, 3, 14, 9, 4, 10, 5, 11, 6, 12
};

/**
 * Indikuje ci bezi sniffer. Nastavenim na 0 ukonci frequency hopping.
 */
unsigned int is_sniffer_running = 1;

/**
 * Prevedie cislo kanala na frekvenciu.
 *
 * @param channel Cislo kanalu.
 * @return Vrati frekvenciu na ktorej dany kanal bezi.
 */
unsigned int nl80211_channel_to_freq(int channel){

	unsigned int freq;

	if (channel < 14){
		freq = 2407 + (5 * channel);
	} else if (channel == 14) {
		freq = 2484;
	} else {
		freq = 5 * (channel + 1000);
	}
	return freq;
}

/**
 * Prevedie frekvenciu na cislo kanala.
 *
 * @param freq
 * @return Cislo kanala.
 */
unsigned int nl80211_freq_to_channel(unsigned int freq){

	if (freq < 2484){
		return (freq - 2407) / 5;
	}

	if (freq == 2484){
		return 14;
	}

	return freq / 5 - 1000;
}

static int error_handler(struct sockaddr_nl *nla, struct nlmsgerr *err, void *arg){
	int *ret = arg;

	UNUSED(nla);

	*ret = err->error;
	return NL_SKIP;
}

static int finish_handler(struct nl_msg *msg, void *arg){
	int *ret = arg;

	UNUSED(msg);

	*ret = 0;
	return NL_SKIP;
}

static int ack_handler(struct nl_msg *msg, void *arg){
	int *ret = arg;

	UNUSED(msg);

	*ret = 0;
	return NL_STOP;
}

int nl_send_recv_msgs(struct nl_msg *msg){

	nl_cb = nl_cb_alloc(NL_CB_DEFAULT);

	struct nl_cb *cb;
	int ret;

	cb = nl_cb_clone(nl_cb);

	if (!cb) {
		fprintf(stderr,"nl80211: failed to alloc netlink callback");
		exit(EXIT_FAILURE);
	}
	ret = nl_send_auto_complete(nl_sock, msg);

	if (ret < 0) {
		fprintf(stderr,"nl80211: failed to send netlink message");
		goto end_func;
	}

	ret = 1;

	if (nl_cb_err(cb, NL_CB_CUSTOM, error_handler, &ret)) {
		fprintf(stderr,"nl80211: send and recv nl_cb_err returned error");
		ret = -1;
		goto end_func;
	}
	if (nl_cb_set(cb, NL_CB_FINISH, NL_CB_CUSTOM, finish_handler, &ret)) {
		fprintf(stderr,"nl80211: send and recv set finnish handler returned error");
		ret = -1;
		goto end_func;
	}
	if (nl_cb_set(cb, NL_CB_ACK, NL_CB_CUSTOM, ack_handler, &ret)) {
		fprintf(stderr,"nl80211: send and recv set ack handler returned error");
		ret = -1;
		goto end_func;
	}

	while (ret > 0) {
		if (nl_recvmsgs(nl_sock, cb)) {
			fprintf(stderr,"nl80211: failed to receive netlink message after command");
			goto end_func;
		}
	}

	end_func:
	nl_cb_put(cb);
	return ret;
}

/**
 * Nastavenie kanalu na ktorom pracuje zadane zariadenie.
 *
 * @param iface_index Index Wi-fi rozhrania.
 * @param channel Zvoleny kanal.
 * @return 0|1
 */
int nl80211_set_channel(int iface_index, unsigned int channel){

	int ret = -1;
	msg = nlmsg_alloc();
	genlmsg_put(msg, getpid(), 0, genl_family_get_id(nl_80211_family), 0, 0, NL80211_CMD_SET_WIPHY, 0);

	unsigned int freq;

	actual_channel = channel;
	freq = nl80211_channel_to_freq(channel);

	NLA_PUT_U32(msg, NL80211_ATTR_IFINDEX, iface_index);
	NLA_PUT_U32(msg, NL80211_ATTR_WIPHY_FREQ, freq);

	ret = nl_send_recv_msgs(msg);

	if(ret){
		printf("nl80211: channel set failed\n");
	}

nla_put_failure:
		nlmsg_free(msg);
		return ret;
}

int nl80211_get_channel(){
	return actual_channel;
}

/**
 * Pomocna funkcia predstavujuca vlakno na channel hopping.
 *
 * @param iface
 */
void *nl80211_channel_changer_thread(void *iface){

	struct timespec sleep_time;
	struct timespec remaining_sleep_time;
	int iface_index = if_nametoindex(iface);
	int i = 0;

	while(is_sniffer_running){
		actual_channel = hop_chans[i];
		nl80211_set_channel(iface_index,hop_chans[i]);
		i++;
		i = i % HOP_CHANS_COUNT;
		sleep_time.tv_sec = HOP_CHANS_SLEEP;
		sleep_time.tv_nsec = 0;
		nanosleep(&sleep_time,&remaining_sleep_time);
	}

	return (void *) 0;
}

/**
 * Inicializacia libnl.
 */
void nl80211_init(){

	nl_sock = nl_socket_alloc();

	nl_cb = nl_cb_alloc(NL_CB_DEFAULT);
	if (!nl_cb) {
		fprintf(stderr,"nl80211: nl_cb_alloc() failed");
		exit(EXIT_FAILURE);
	}
	if (genl_connect(nl_sock)) {
		fprintf(stderr,"nl80211: genl_connect() failed");
		exit(EXIT_FAILURE);
	}

	genl_ctrl_alloc_cache(nl_sock,&nl_cache);
	if (!nl_cache) {
		fprintf(stderr,"nl80211: genl_ctrl_alloc_cache() failed");
		exit(EXIT_FAILURE);
	}


	nl_80211_family = genl_ctrl_search_by_name(nl_cache, "nl80211");
	if (!nl_80211_family) {
		fprintf(stderr,"nl80211: genl_ctrl_search_by_name() failed");
		exit(EXIT_FAILURE);
	}
}

/**
 * Spusti automaticky channel hopping.
 *
 * @param iface Nazov rozhrania.
 */
void nl80211_start_channel_hopping(char *iface){

	pthread_t thread;
	pthread_attr_t attr;
	int ret = 0;

	if((ret = pthread_attr_init(&attr)) != 0){
		fprintf(stderr, "pthread_attr_init() error %d\n",ret);
		exit(EXIT_FAILURE);
	}

	if((ret = pthread_attr_setdetachstate(&attr,PTHREAD_CREATE_DETACHED)) != 0){
		fprintf(stderr, "pthread_attr_setdetachstate() error %d\n",ret);
		exit(EXIT_FAILURE);
	}

	if((ret = pthread_create(&thread, &attr, nl80211_channel_changer_thread,iface)) != 0){
		fprintf(stderr, "pthread_create() error %d\n",ret);
		exit(EXIT_FAILURE);
	}
}

