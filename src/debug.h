#ifndef DEBUG_H_INCLUDED
#define DEBUG_H_INCLUDED

#include "include/ieee80211.h"
#include "common.h"

void debug_ap_row(ap_stats_t *ap);
void debug_frame_control(struct ieee80211_frame_control *frame_control);
void debug_packet_struct(frame_data_t packet);

#endif /* DEBUG_H_INCLUDED */
