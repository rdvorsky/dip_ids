/*
 * attackt.h
 *
 *  Created on: 28.11.2013
 *      Author: rdy
 */

#ifndef ATTACKT_H_
#define ATTACKT_H_


#define ATK_ATTACK_END_SLEEP					5						/* Po niektorych utokoch mozu este prist opakovane */
																		/* ramce, udava kolko s cakat po skonceni utoku nez zacne
																		 * ramce oznacovat ako neutocne */
#define ATK_COUNT_ALL 							10						/* Pocet utokov */
/*#define ATK_COUNT_ALL_WITHOUT_DOS				5*/
#define ATK_SCRIPTS_PATH 						"../attacks/"			/* Cesta do adreara obsahujuceho utoky */
#define ATK_COMMON_PARAMS_FILE ATK_SCRIPTS_PATH	"params.ini"			/* Cesta k konfiguracnemu suboru pre vsetky utoky */
#define ATK_INFO_FILE							"info.ini"				/* Meno info suboru obsahujuceho popis utoku */
#define ATK_ID_DUMMY 							0
#define ATK_ID_DEAUTH 							1
#define ATK_ID_KOREK 							2
#define ATK_ID_FRAGMENTATION 					3
#define ATK_ID_PTW 								4
#define ATK_ID_AUTH 							5
#define ATK_ID_CTS 								6
#define ATK_ID_BT 								7
#define ATK_ID_UNKNOWN							8
#define ATK_ID_HOLE196							9
#define ATK_ID_RTS								10

#include <signal.h>
#include "common.h"

extern const char *atk_attack_names[ATK_COUNT_ALL];

/**
 * Struktura obsahuje zakladne konfiguracneho informacie pre vykonavany utok.
 */
struct {
	/**
	 * ID momentalne vykonavaneho utoku utoku. Ak obsahuje nenulovu hodnotu,
	 * dochadza k aplikacii filtra a nasledneho oznacenia a ulozenia ramca do
	 * db.
	 */
	volatile sig_atomic_t attack_id;
	/**
	 * Meno utoku
	 */
	const char *name;
	/**
	 * Pocet krokov utoku
	 */
	int steps;
	/**
	 * BSSID AP na ktore sa utoci
	 */
	const char *bssid;
	/**
	 * MAC klienta na ktoreho sa utoci
	 */
	const char *target_mac;

	/**
	 * MAC adresa prveho wifi zariadenia puziteho pri hole196 utoku
	 */
	const char *hole196_src_mac;

	/**
	 * Timeout po ktorom dojde k ukonceniu utoku
	 */
	int timeout;
	/**
	 * Pauza medzi krokmi utoku
	 */
	int step_pause;

	/**
	 * Pocet spracovanych ramcov, po ktorych dojde k ulozeniu do db.
	 */
	int db_default_save_group_size;

	/**
	 * Pocet spracovanych ramcov, po ktorych dojde k ulozeniu do db.
	 */
	int db_save_group_size;

	/**
	 * Kanal na ktorom sa vykona utok
	 */
	int channel;
} atk_attack_attributes;

/**
 * Pole filtrovacich funkcii. Index pola sa musi zhodovat s ID utoku, pod
 * ktorym je utok ulozeny v slozke attack_scripts/.
 */
int (*attack_filters[ATK_COUNT_ALL+1]) (frame_data_t packet); /* +1 lebo sa nemoze pouzit utok s id == 0, nedoslo by k ozaceniu ramca */

void atk_init();
void atk_start(int attack_id, char *iface, int channel, void (*callback)(void *));
int	 atk_filter_dummy(frame_data_t packet);
int  atk_filter_unknown_dos(frame_data_t frame);
int  atk_filter_deauth(frame_data_t packet);
int  atk_filter_cts(frame_data_t packet);
int  atk_filter_rts(frame_data_t packet);
int  atk_filter_auth(frame_data_t packet);
int  atk_filter_korek(frame_data_t packet);
int  atk_filter_ptw(frame_data_t packet);
int  atk_filter_fragmentation(frame_data_t packet);
int  atk_filter_beck_tews(frame_data_t frame);
int  atk_filter_hole196(frame_data_t frame);

#endif /* ATTACKT_H_ */
