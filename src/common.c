#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <ctype.h>
#include "common.h"

void comm_thread_init(void *(*thread_fnc)(void *), void *params, gboolean joinable, void (*callback)(void *)){
	pthread_t thread;
	pthread_attr_t attr;
	int ret = 0;
	void *join_ret_value;

	if((ret = pthread_attr_init(&attr)) != 0){
		fprintf(stderr, "pthread_attr_init() error %d\n",ret);
		exit(EXIT_FAILURE);
	}

	if(joinable){
		if((ret = pthread_attr_setdetachstate(&attr,PTHREAD_CREATE_JOINABLE)) != 0){
				fprintf(stderr, "pthread_attr_setdetachstate() error %d\n",ret);
				exit(EXIT_FAILURE);
		}
	}else{
		if((ret = pthread_attr_setdetachstate(&attr,PTHREAD_CREATE_DETACHED)) != 0){
			fprintf(stderr, "pthread_attr_setdetachstate() error %d\n",ret);
			exit(EXIT_FAILURE);
		}
	}

	if((ret = pthread_create(&thread, &attr, thread_fnc,params)) != 0){
		fprintf(stderr, "pthread_create() error %d\n",ret);
		exit(EXIT_FAILURE);
	}

	if(joinable){
		pthread_join(thread,(void **)&join_ret_value);
		callback(join_ret_value);
	}


}

/**
 * Prevedie "raw" mac adresu na format: aa:bb:cc:dd:ee:ff
 *
 * @param mac_address
 * @return
 */
void comm_mac_to_string(unsigned char *mac_address, char *out_string){

	char octet_string[3];
	int i, offset = 0;

	for (i = 0; i < 6; ++i) {

		/* Ak nie je posledny, pridaj : */
		if(i != 5){
			sprintf(octet_string,"%02X:",mac_address[i]);
		}else{
			sprintf(octet_string,"%02X",mac_address[i]);
		}

		memcpy(out_string + offset,octet_string,strlen(octet_string));

		/* Dve hodnoty + : */
		offset += 3;
	}

	out_string[strlen(out_string)] = 0;
}

/**
 * Zisti ci text obsahuje zadany prefix.
 *
 * @param text
 * @param prefix
 * @return 0|1
 */
int comm_start_with(char *text, char *prefix){
	return (strstr(text, prefix) - text) == 0;
}
/**
 * Zisti ci ide o UTF-8 ratazec.
 * @see http://stackoverflow.com/questions/1031645/how-to-detect-utf-8-in-plain-c
 *
 * @param string Vstupny textovy retazec.
 * @return 0|1
 */
int comm_is_string(char *string){

	const unsigned char * bytes = (unsigned char *)string;

	if(!string)
        return 0;

    while(*bytes)
    {
        if( (
                bytes[0] == 0x09 ||
                bytes[0] == 0x0A ||
                bytes[0] == 0x0D ||
                (0x20 <= bytes[0] && bytes[0] <= 0x7E)
            )
        ) {
            bytes += 1;
            continue;
        }

        if( (
                (0xC2 <= bytes[0] && bytes[0] <= 0xDF) &&
                (0x80 <= bytes[1] && bytes[1] <= 0xBF)
            )
        ) {
            bytes += 2;
            continue;
        }

        if( (
                bytes[0] == 0xE0 &&
                (0xA0 <= bytes[1] && bytes[1] <= 0xBF) &&
                (0x80 <= bytes[2] && bytes[2] <= 0xBF)
            ) ||
            (
                ((0xE1 <= bytes[0] && bytes[0] <= 0xEC) ||
                    bytes[0] == 0xEE ||
                    bytes[0] == 0xEF) &&
                (0x80 <= bytes[1] && bytes[1] <= 0xBF) &&
                (0x80 <= bytes[2] && bytes[2] <= 0xBF)
            ) ||
            (
                bytes[0] == 0xED &&
                (0x80 <= bytes[1] && bytes[1] <= 0x9F) &&
                (0x80 <= bytes[2] && bytes[2] <= 0xBF)
            )
        ) {
            bytes += 3;
            continue;
        }

        if( (
                bytes[0] == 0xF0 &&
                (0x90 <= bytes[1] && bytes[1] <= 0xBF) &&
                (0x80 <= bytes[2] && bytes[2] <= 0xBF) &&
                (0x80 <= bytes[3] && bytes[3] <= 0xBF)
            ) ||
            (
                (0xF1 <= bytes[0] && bytes[0] <= 0xF3) &&
                (0x80 <= bytes[1] && bytes[1] <= 0xBF) &&
                (0x80 <= bytes[2] && bytes[2] <= 0xBF) &&
                (0x80 <= bytes[3] && bytes[3] <= 0xBF)
            ) ||
            (
                bytes[0] == 0xF4 &&
                (0x80 <= bytes[1] && bytes[1] <= 0x8F) &&
                (0x80 <= bytes[2] && bytes[2] <= 0xBF) &&
                (0x80 <= bytes[3] && bytes[3] <= 0xBF)
            )
        ) {
            bytes += 4;
            continue;
        }

        return 0;
    }

    return 1;

}
