/*
 * attack.c
 *
 *  Created on: 28.11.2013
 *      Author: rdy
 */

#define DISABLE_OUTPUT

#include <stdlib.h>
#include <stdio.h>
#include <sys/wait.h>
#include "attack.h"
#include "include/ieee80211.h"

#ifdef DISABLE_OUTPUT
#define printf (void)
#endif

typedef struct{
	int attack_id;
	char *iface;
} atk_attack_params;


/** Odkaz na konfiguracny suboru spolocneho pre vsetky utoky. */
static dictionary *dict_common;

/** Odkaz na konfiguracny subor spustaneho utoku. */
static dictionary *dict_cur_attack;

/**
 * Ak je spusteny background utok obsahuje 1.
 * Pouziva sa na indikaciu cakania na dokoncenie utoku, aby nedochadzalo
 * k spusteniu dalsieho utoku pokym nie je dokonceny background proces
 * predchadzajuceho.
 */
volatile sig_atomic_t bgr_proc_running = 0;

const char *atk_attack_names[ATK_COUNT_ALL] = {"Deauth Flood","KoreK ChopChop Attack","Fragmentation Attack","PTW Attack",
											   "Authentication Flood","CTS Flood","Beck-Tews Attack (WPA)","Unknown DoS","Hole196", "RTS Flood"};

/**
 * Nacita konfiguracie utokov z ini suborov.
 */
void atk_init(){

	/* Inicializacia filtrov pre oznacovanie paketov */
	attack_filters[ATK_ID_DUMMY] = atk_filter_dummy;
	attack_filters[ATK_ID_DEAUTH] = atk_filter_deauth;
	attack_filters[ATK_ID_KOREK] = atk_filter_korek;
	attack_filters[ATK_ID_FRAGMENTATION] = atk_filter_fragmentation;
	attack_filters[ATK_ID_PTW] = atk_filter_ptw;
	attack_filters[ATK_ID_AUTH] = atk_filter_auth;
	attack_filters[ATK_ID_CTS] = atk_filter_cts;
	attack_filters[ATK_ID_BT] = atk_filter_beck_tews;
	attack_filters[ATK_ID_HOLE196] = atk_filter_hole196;
	attack_filters[ATK_ID_RTS] = atk_filter_rts;
	/*attack_filters[ATK_ID_UNKNOWN] = atk_filter_unknown_dos;*/

	/* Nacitanie konfiguracie z params.ini */
	dict_common = iniparser_load(ATK_COMMON_PARAMS_FILE);

	atk_attack_attributes.bssid = iniparser_getstring(dict_common,"common:bssid",NULL);
	atk_attack_attributes.target_mac = iniparser_getstring(dict_common,"common:target_mac",NULL);
	atk_attack_attributes.hole196_src_mac = iniparser_getstring(dict_common,"common:hole196_src_mac",NULL);
	atk_attack_attributes.timeout = iniparser_getint(dict_common,"common:timeout",0);
	atk_attack_attributes.step_pause = iniparser_getint(dict_common,"common:step_pause",0);
	atk_attack_attributes.db_default_save_group_size = iniparser_getint(dict_common,"common:db_save_group_size",1000);
	atk_attack_attributes.db_save_group_size = iniparser_getint(dict_common,"common:db_save_group_size",1000);

#ifdef DEBUG_ATTACK_CONFIG
	printf("file: %s\n",ATK_COMMON_PARAMS_FILE);
	printf("bssid: %s\n",atk_attack_attributes.bssid);
	printf("target_mac: %s\n",atk_attack_attributes.target_mac);
	printf("timout: %d\n",atk_attack_attributes.timeout);
	printf("step_pause: %d\n",atk_attack_attributes.step_pause);
#endif
}

/**
 * Pomocna funkcia, ktora spusta shell skripty ulozene v adresarovej strukture utokov.
 *
 * @param script_path Cesta k shell skriptu, ktory sa ma spustit.
 * @return
 */
int atk_run_script(char *script_path){

	if(system(script_path) < 1){
		/*fprintf(stderr,"failed to run attack script: %s\n",script_path);*/
		/* TODO: pridat zistenie stavu skriptu */
		return 1;
	}

	return 1;
}

void *atk_thread_attack_run(void *params){

	char *iface = (char *)params;
	char info_file_path[256];
	char execute_script_path[256];
	char step_key[256];
	int i, id = -1, silent, background;
	int ret = 0;


	/* format: ATTACK_SCRIPTS_PATH/[attack_id]/ATTACK_INFO_FILE */
	sprintf(info_file_path,"%s%d/%s",ATK_SCRIPTS_PATH,atk_attack_attributes.attack_id,ATK_INFO_FILE);

	dict_cur_attack = iniparser_load(info_file_path);

	/* Ak hodnota nie je zadana, tak sa pouzije "common" hodnota */
	atk_attack_attributes.bssid = iniparser_getstring(dict_cur_attack,"info:bssid",atk_attack_attributes.bssid);
	atk_attack_attributes.target_mac = iniparser_getstring(dict_cur_attack,"info:target_mac",atk_attack_attributes.target_mac);
	atk_attack_attributes.timeout = iniparser_getint(dict_cur_attack,"info:timeout",atk_attack_attributes.timeout);
	atk_attack_attributes.step_pause = iniparser_getint(dict_cur_attack,"info:step_pause",atk_attack_attributes.step_pause);
	atk_attack_attributes.db_save_group_size = iniparser_getint(dict_cur_attack,"info:db_save_group_size",atk_attack_attributes.db_save_group_size);

	/* Povinne hodnoty pre  utok */
	atk_attack_attributes.name = iniparser_getstring(dict_cur_attack,"info:name","Unknown name");
	atk_attack_attributes.steps = iniparser_getint(dict_cur_attack,"info:steps",1);

	printf("~~~~~~~~~~~~~~~~~~~~~~~~~~\n");
	printf(" %s\n",atk_attack_attributes.name);
	printf("~~~~~~~~~~~~~~~~~~~~~~~~~~\n");

	for (i = 1; i <= atk_attack_attributes.steps; ++i) {

		sprintf(step_key,"%d:timeout",i);
		atk_attack_attributes.timeout = iniparser_getint(dict_cur_attack,step_key,atk_attack_attributes.timeout);
		sprintf(step_key,"%d:background",i);
		background = iniparser_getboolean(dict_cur_attack,step_key,0);

		/* Na pozadi sa vzdy spusta vzdy bez vystupu */
		if(background){
			silent = 1;
		}else{
			sprintf(step_key,"%d:silent",i);
			silent = iniparser_getboolean(dict_cur_attack,step_key,0);
		}

		sprintf(execute_script_path,
				"%s%d/%d.sh %s %s %s %d %d %d",
				ATK_SCRIPTS_PATH,
				atk_attack_attributes.attack_id,
				i,
				atk_attack_attributes.bssid,
				atk_attack_attributes.target_mac,
				iface,
				atk_attack_attributes.timeout,
				silent,
				atk_attack_attributes.channel);

#ifdef DEBUG_ATTACK_CONFIG
		printf("background: %d\n",background);
		printf("silent: %d\n",silent);
		printf("timeout: %d\n",atk_attack_attributes.timeout);
		printf("command: %s\n",execute_script_path);
#endif


		/* Spusti na pozadi */
		if(background){
			/* Maximalne jeden proces na pozadi */
			if( bgr_proc_running ){
				printf("Waiting for background process...");
				waitpid(id,NULL,0);
				printf("done\n");
				bgr_proc_running = 0;
			}

			printf("Step %d...in background\n",i);
			bgr_proc_running = 1;
			if((id = fork()) == 0){/* child vykonava skript */
				atk_run_script(execute_script_path);
				/* Ak je to posledny krok, tak nastav priznak na 0
				if(i == attack_attr.steps){
					attack_attr.attack_id = 0;
				}
				 */
				/*printf("\033[%dm\033[1m%s>\033[m\033[m ",SHELL_COLOR,SHELL_TEXT);*/

				/* Ukonci child */
				exit(EXIT_SUCCESS);

			}else if(id > 0){/* parent pokracuje v cinnosti */
				continue;
			}else{
				fprintf(stderr,"error: attack fork() failed\n");
				pthread_exit(&ret);
			}

		}else{/* spusti na popredi */
			printf("Step %d...",i);
			atk_run_script(execute_script_path);
			printf("done\n");
		}
	}

	/* Pri poslednom kroku treba pockat ak bezi background proces */
	if( bgr_proc_running ){
		printf("Background process...");
		waitpid(id,NULL,0);
		printf("done\n");
		bgr_proc_running = 0;
	}

	/* Treba pockat par sekund pre pripad, ze pridu opakovane ramce*/
	sleep(ATK_ATTACK_END_SLEEP);

	/* Treba nastavit priznak o konci utoku */
	atk_attack_attributes.attack_id = 0;

	/* Po skonceni utoku ukladaj po default pocte zaznamov */
	atk_attack_attributes.db_save_group_size = atk_attack_attributes.db_default_save_group_size;

	ret = 1;
	pthread_exit(&ret);
}

/**
 * Spusti zvoleny udok podla id.
 *
 * @param attack_id ID utoku musi zodpovedat nazvu adresara v slozke attack_scripts/.
 * @param iface Nazov rozhrania, pomocou ktoreho sa utok vykonava.
 * @param callback Funkcia, ktora sa zavola po skonceni utoku
 */
void atk_start(int attack_id, char *iface, int channel, void (*callback)(void *)){

	atk_attack_attributes.attack_id = attack_id;
	atk_attack_attributes.channel = channel;
	comm_thread_init(atk_thread_attack_run,(void *)iface,TRUE,callback);
}

/**
 * Prazdny filter. Kazdy paket bude oznaceny.
 *
 * @param packet
 * @return Vrati vzdy 1.
 */
int atk_filter_dummy(frame_data_t packet){
	UNUSED(packet);
	return ATK_ID_DUMMY;
}

int atk_filter_unknown_dos(frame_data_t frame){

	if(frame.fc_type == IEEE80211_FC0_TYPE_MGT){
		/* Beacon frame nas nezaujima */
		if(frame.fc_subtype << IEEE80211_FC0_SUBTYPE_SHIFT == IEEE80211_FC0_SUBTYPE_BEACON) return 0;

		return ATK_ID_UNKNOWN;

	}else if(frame.fc_type == IEEE80211_FC0_TYPE_CTL){
		return ATK_ID_UNKNOWN;
	}


	return 0;
}

int atk_filter_auth(frame_data_t packet){

	if(packet.fc_type == IEEE80211_FC0_TYPE_MGT){
		if(		/* Source a Dest sa strieda */
				((comm_start_with(packet.addr1,"00:00:00")) || ((strcmp(packet.addr1,atk_attack_attributes.bssid) == 0))) &&
				((comm_start_with(packet.addr2,"00:00:00")) || ((strcmp(packet.addr2,atk_attack_attributes.bssid) == 0))) &&
				(strcmp(packet.addr3,atk_attack_attributes.bssid) == 0) /* BSSID */ &&
				( /* Zaujimaju nas len niektore typy  */
					((packet.fc_subtype << IEEE80211_FC0_SUBTYPE_SHIFT) == IEEE80211_FC0_SUBTYPE_AUTH) /*||
					((packet.fc_subtype << IEEE80211_FC0_SUBTYPE_SHIFT) == IEEE80211_FC0_SUBTYPE_ASSOC_REQ) ||
					((packet.fc_subtype << IEEE80211_FC0_SUBTYPE_SHIFT) == IEEE80211_FC0_SUBTYPE_ASSOC_RESP) ||
					((packet.fc_subtype << IEEE80211_FC0_SUBTYPE_SHIFT) == IEEE80211_FC0_SUBTYPE_AUTH) ||
					((packet.fc_subtype << IEEE80211_FC0_SUBTYPE_SHIFT) == IEEE80211_FC0_SUBTYPE_DISASSOC)*/
				)
		){
			return ATK_ID_AUTH;
		}

	}

	return 0;
}

int atk_filter_deauth(frame_data_t packet){

	if(packet.fc_type == IEEE80211_FC0_TYPE_MGT){
		if(		/* Source a Dest sa strieda */
				((strcmp(packet.addr1,atk_attack_attributes.bssid) == 0) || ((strcmp(packet.addr1,atk_attack_attributes.target_mac) == 0))) &&
				((strcmp(packet.addr2,atk_attack_attributes.bssid) == 0) || ((strcmp(packet.addr2,atk_attack_attributes.target_mac) == 0))) &&
				(strcmp(packet.addr3,atk_attack_attributes.bssid) == 0) /* BSSID */ &&
				( /* Zaujimaju nas len niektore typy  */
					((packet.fc_subtype << IEEE80211_FC0_SUBTYPE_SHIFT) == IEEE80211_FC0_SUBTYPE_DEAUTH) /*||
					((packet.fc_subtype << IEEE80211_FC0_SUBTYPE_SHIFT) == IEEE80211_FC0_SUBTYPE_ASSOC_REQ) ||
					((packet.fc_subtype << IEEE80211_FC0_SUBTYPE_SHIFT) == IEEE80211_FC0_SUBTYPE_ASSOC_RESP) ||
					((packet.fc_subtype << IEEE80211_FC0_SUBTYPE_SHIFT) == IEEE80211_FC0_SUBTYPE_AUTH) ||
					((packet.fc_subtype << IEEE80211_FC0_SUBTYPE_SHIFT) == IEEE80211_FC0_SUBTYPE_DISASSOC)*/
				)
		){
			return ATK_ID_DEAUTH;
		}

	}

	return 0;
}


int atk_filter_cts(frame_data_t packet){

	if(packet.fc_type == IEEE80211_FC0_TYPE_CTL){
		if(strcmp(packet.addr1,atk_attack_attributes.bssid) == 0 &&
		 ((packet.fc_subtype << IEEE80211_FC0_SUBTYPE_SHIFT) == IEEE80211_FC0_SUBTYPE_CTS)){
			return ATK_ID_CTS;
		}

	}

	return 0;
}


int atk_filter_rts(frame_data_t packet){

	if(packet.fc_type == IEEE80211_FC0_TYPE_CTL){
		if(strcmp(packet.addr1,atk_attack_attributes.bssid) == 0 &&
		 ((packet.fc_subtype << IEEE80211_FC0_SUBTYPE_SHIFT) == IEEE80211_FC0_SUBTYPE_RTS)){
			return ATK_ID_RTS;
		}

	}

	return 0;
}

/**
 * Urci ci je dany paket relevantny pre "KoreK Attack".
 *
 * Vrati 0 alebo 1 ak nepatri/patri paket do utoku.
 *
 * KoreK Attack zachyti datovy paket, ktory sa snazi desifrovat postupnym
 * skracovanim paketu a hadanim ICV a naslednym ziskanim keystreamu.
 *
 * Vsetky pakety su posielane na addresu s prefixom ff:*, ak je hadane ICV
 * spravne tak sa vrati odpoved pre danu addresu ff:*. Takto sa postupne pokracuje
 * pre kazdy bajt paketu az kym sa nezpracuje cely paket.
 *
 * @param packet
 * @return Vrati 1|0 ak ramec patri resp. nepatri k utoku.
 */
int atk_filter_korek(frame_data_t packet){

	/* Zaujimaju nas len datove pakety */
	if(packet.fc_type == IEEE80211_FC0_TYPE_DATA){
		if(
			((strcmp(packet.addr1,atk_attack_attributes.bssid) == 0) && /* Zhoduje sa BSSID, na ktore je utok vedeny */
			(strcmp(packet.addr2,atk_attack_attributes.target_mac) == 0) && /* Source addresa je pripojeny klient*/
			comm_start_with(packet.addr3,"FF:")) /* Dest addres zacina na ff:  */
			||
			((comm_start_with(packet.addr1,"FF:")) && /* Destination addresa pri potvrdzovacom frame zacina na ff: */
			(strcmp(packet.addr2,atk_attack_attributes.bssid) == 0) && /* Zhoduje sa BSSID, na ktore je utok vedeny */
			(strcmp(packet.addr3,atk_attack_attributes.target_mac) == 0)) /* Source addresa klienta */
		){
			return ATK_ID_KOREK;
		}
	}

	return 0;
}

/**
 * Urci si sa jedna o paket patriaci PTW utoku.
 *
 * Vrati 0 alebo 1 ak nepatri/patri paket do utoku.
 *
 * ARP Replay vyzaduje associovaneho klienta, pre ktoreho sa snazi zachytit arp paket, ktory
 * nasledne posiela znova a dochadza tak k generovaniu dalsich paketov resp. novych IV. *
 * Pre funkcnost utoku je nutne najskor deauth. klienta a vynutit tak poslanie ARP paketu, nasledne
 * je paket zachyteny a opakovany.
 *
 * @param packet
 * @return Vrati 1|0 ak ramec patri resp. nepatri k utoky.
 */
int atk_filter_ptw(frame_data_t packet){

	/* Podmineky pre datove pakety  */
	if(packet.fc_type == IEEE80211_FC0_TYPE_DATA){
		if(		/* Source a Dest sa strieda */
				((strcmp(packet.addr1,atk_attack_attributes.bssid) == 0) || (comm_start_with(packet.addr1,"FF:FF:FF"))) && /* BSSID alebo broadcast */
				((strcmp(packet.addr2,atk_attack_attributes.bssid) == 0) || ((strcmp(packet.addr2,atk_attack_attributes.target_mac) == 0))) &&
				((strcmp(packet.addr3,atk_attack_attributes.target_mac) == 0) || (comm_start_with(packet.addr3,"FF:FF:FF"))) && /* Target alebo broadcast */
				( /* Zaujimaju nas len niektore typy  */
						((packet.fc_subtype << IEEE80211_FC0_SUBTYPE_SHIFT) == IEEE80211_FC0_SUBTYPE_DATA)
				)
		){
			return ATK_ID_PTW;
		}
	}else if(packet.fc_type == IEEE80211_FC0_TYPE_MGT){
		if(		/* Source a Dest sa strieda */
				((strcmp(packet.addr1,atk_attack_attributes.bssid) == 0) || ((strcmp(packet.addr1,atk_attack_attributes.target_mac) == 0))) &&
				((strcmp(packet.addr2,atk_attack_attributes.bssid) == 0) || ((strcmp(packet.addr2,atk_attack_attributes.target_mac) == 0))) &&
				(strcmp(packet.addr3,atk_attack_attributes.bssid) == 0) &&
				(
					((packet.fc_subtype << IEEE80211_FC0_SUBTYPE_SHIFT) == IEEE80211_FC0_SUBTYPE_DEAUTH)
				)
		){
			return ATK_ID_DEAUTH;
		}

	}

	return 0;
}

/**
 * Urci si sa jedna o Fragmentation Attack paket.
 *
 * Vrati 0 alebo 1 ak nepatri/patri paket do utoku.
 *
 * Zachyti datovy paket, z ktoreho vdaka znamej LLC hlavicke ziska 8B keystream. S pouzitim
 * zisteneho keystremu vytvara fragmentovane pakety (max: 16 x 4 = 64B), ktore AP desifruje a nefragmentovane
 * posle spat. Takto je mozne postupne ziskavat keystream.
 *
 * ARP paket je zasielany na dest. adresu s prefixom FF:FF:FF:FF:FF:*.
 *
 * @param packet
 * @return Vrati 1|0 ak ramec patri resp. nepatri k utoky.
 */
int atk_filter_fragmentation(frame_data_t packet){

	/* Zaujimaju nas len datove pakety */
	if(packet.fc_type == IEEE80211_FC0_TYPE_DATA){
		if(		/* Source a Dest sa strieda */
				((strcmp(packet.addr1,atk_attack_attributes.bssid) == 0) || (comm_start_with(packet.addr1,"FF:FF:FF:FF:FF"))) && /* BSSID alebo broadcast */
				((strcmp(packet.addr2,atk_attack_attributes.bssid) == 0) || ((strcmp(packet.addr2,atk_attack_attributes.target_mac) == 0))) &&
				((strcmp(packet.addr3,atk_attack_attributes.target_mac) == 0) || (comm_start_with(packet.addr3,"FF:FF:FF:FF:FF"))) /* Target alebo broadcast */
		){
			return ATK_ID_FRAGMENTATION;
		}
	}

	return 0;
}

int atk_filter_beck_tews(frame_data_t frame){

	/* Najskor sa posielaju deauth ramce na ziskanie handshake */
	if(frame.fc_type == IEEE80211_FC0_TYPE_MGT){
		if(		/* Source a Dest sa strieda */
				((strcmp(frame.addr1,atk_attack_attributes.bssid) == 0) || ((strcmp(frame.addr1,atk_attack_attributes.target_mac) == 0))) &&
				((strcmp(frame.addr2,atk_attack_attributes.bssid) == 0) || ((strcmp(frame.addr2,atk_attack_attributes.target_mac) == 0))) &&
				(strcmp(frame.addr3,atk_attack_attributes.bssid) == 0) &&
				(
						((frame.fc_subtype << IEEE80211_FC0_SUBTYPE_SHIFT) == IEEE80211_FC0_SUBTYPE_DEAUTH)
				)
		){
			return ATK_ID_DEAUTH;
		}
	}else if(frame.fc_type == IEEE80211_FC0_TYPE_DATA){
		if(		/* Source a Dest sa strieda */
				((strcmp(frame.addr1,atk_attack_attributes.bssid) == 0) || ((strcmp(frame.addr1,atk_attack_attributes.target_mac) == 0))) &&
				((strcmp(frame.addr2,atk_attack_attributes.bssid) == 0) || ((strcmp(frame.addr2,atk_attack_attributes.target_mac) == 0))) &&
				(strcmp(frame.addr3,atk_attack_attributes.bssid) == 0) &&
				(
						((frame.fc_subtype << IEEE80211_FC0_SUBTYPE_SHIFT) == IEEE80211_FC0_SUBTYPE_QOS)
				)
		){
			return ATK_ID_BT;
		}
	}

	return 0;
}

int atk_filter_hole196(frame_data_t frame){

	if(frame.fc_type == IEEE80211_FC0_TYPE_DATA){
		if(
				(comm_start_with(frame.addr1,"FF:FF:FF:FF:FF")) && /* Vzdy broadcast */
				(strcmp(frame.addr2,atk_attack_attributes.bssid) == 0) && /* Vzdy bssid */
				((strcmp(frame.addr3,atk_attack_attributes.target_mac) == 0) || (strcmp(frame.addr3,atk_attack_attributes.hole196_src_mac) == 0))
		){
			return ATK_ID_HOLE196;
		}
	}

	return 0;
}
