/*
 * db.c
 *
 *  Created on: 28.11.2013
 *      Author: rdy
 */

/*#define DISABLE_OUTPUT*/

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <glib.h>


#include "common.h"
#include "database.h"
#include "debug.h"
#include "attack.h"

/*#define DEBUG_MYSQL*/

#define DB_QUERY_SELECT_ALL			"SELECT * FROM %s "
#define DB_QUERY_INSERT 	       	"INSERT INTO %s(\
										addr1,addr2,addr3,addr4,bssid,seq,fc_type,fc_subtype,ssid,channel,size,timestamp,\
										dur,aid,cipher,sig,frag,fc_from_ds,fc_to_ds,fc_more_frag,fc_retry,fc_protected_frame,fc_order,bcst,bcst_prefix) VALUES"
#define DB_QUERY_INSERT_VALUES 		"('%s','%s','%s','%s',%ld,%d,%d,%d,'%s',%d,%d,%f,%d,%d,'%d',%d,%d,%d,%d,%d,%d,%d,%d,%d,%d),"
#define DB_QUERY_SELECT_COUNT		"SELECT COUNT(*) FROM `%s` WHERE fc_type=8"


#ifdef DISABLE_OUTPUT
#define printf (void)
#endif

static int 		mysql_query_group_counter 	= 0;
static int		mysql_alloc_size 			= DB_QUERY_DEFAULT_LEN;
static char 	*db_mysql_stmt 				= NULL;
/*pthread_t 		db_thread;
pthread_attr_t 	db_thread_attr;
pthread_cond_t	db_cond_save;
pthread_mutex_t	db_mutex_save;*/
GThreadPool		*db_thread_pool_save;

/**
 * Vypise podrobnosti o MySql chybe.
 *
 * @param conn
 */
void db_mysql_error_print(){
	printf("Error %u: %s\n", mysql_errno(db_conn), mysql_error(db_conn));
}


/**
 * Pomocna funkcia, ktora sa spusta v samostatnom vlakne a uklada
 * naparsovane data do db. Funkcia je spustana v ramci GThreadPool.
 *
 * @param mysql_stmt MySql insert dotaz.
 */
void db_thread_frame_data_insert(gpointer data, gpointer user_data){

	UNUSED(user_data);

#ifdef DEBUG_MYSQL
		printf("Inserting %d packets into database...",DB_QUERY_GROUP_SIZE);
#endif
		if(mysql_query(db_conn,data)){
#ifdef DEBUG_MYSQL
			printf("failed\n");
			db_mysql_error_print(db_conn);
			printf("\n%s\n",mysql_stmt_c);
			exit(EXIT_FAILURE);
#else
			db_mysql_error_print();
#endif
		}else{
#ifdef DEBUG_MYSQL
			printf("success\n");
#endif
		}

		g_free(data);
}

/**
 * Vyberie vsetky data z db zo zvolenej tabulky so zadanim where.
 *
 * @param table_name Nazov tabulky, z ktorej sa data vyberaju. *
 * @param query
 * @return MYSQL_RES Vrati vsetky riadky tabulky.
 */
MYSQL_RES *db_query_select_all_where(const char *table_name, const char *query){

	char *select_query = calloc(
			strlen(DB_QUERY_SELECT_ALL) + strlen(table_name) + (query == NULL ? 0 : strlen(query) + 5) + 64, sizeof(char));

	sprintf(select_query,DB_QUERY_SELECT_ALL,table_name);

	if(query != NULL){
		strcat(select_query,query);
	}

	mysql_query(db_conn,select_query);

	free(select_query);

	return mysql_store_result(db_conn);
}

/**
 * Vyberie vsetky data z db zo zvolenej tabulky.
 *
 * @param table_name Nazov tabulky, z ktorej sa data vyberaju. *
 * @return MYSQL_RES Vrati vsetky riadky tabulky.
 */
MYSQL_RES *db_query_select_all(const char *table_name){

	return db_query_select_all_where(table_name,NULL);
}

/**
 * Atributy z db vlozi do struktury.
 *
 * @param row Riadok z MySql db.
 * @return packet_data_t
 */
frame_data_t db_row_to_frame(MYSQL_ROW row){

	frame_data_t frame;

	memset(&frame,0,sizeof(frame_data_t));

	/* row[0] = ID */
	if(strcmp(row[1],"NULL") != 0){
		strcpy(frame.addr1,row[1]);
	}

	if(strcmp(row[2],"NULL") != 0){
		strcpy(frame.addr2,row[2]);
	}
	if(strcmp(row[3],"NULL") != 0){
		strcpy(frame.addr3,row[3]);
	}
	if(strcmp(row[4],"NULL") != 0){
		strcpy(frame.addr4,row[4]);
	}


	frame.bssid = strtoull(row[5],NULL,10);
	/*bssid_tmp = strtod(row[5],NULL);
	packet.bssid = (u_int64_t)bssid_tmp;*/

	frame.fc_type = atoi(row[6]);
	frame.fc_subtype = atoi(row[7]);
	frame.fc_from_ds = atoi(row[8]);
	frame.fc_to_ds = atoi(row[9]);
	frame.fc_more_frag = atoi(row[10]);
	frame.fc_retry = atoi(row[11]);
	frame.fc_protected_frame = atoi(row[12]);
	frame.fc_order = atoi(row[13]);
	frame.frag = atoi(row[14]);
	frame.seq = atoi(row[15]);
	frame.dur = atoi(row[16]);
	strcpy(frame.ssid,"Unknown");
	/*if((strlen(row[17]) > 0) && (strlen(row[17]) < 256)){
		strcpy(packet.ssid,row[17]);
	}*/

	frame.channel = atoi(row[18]);
	frame.signal = atoi(row[19]);
	frame.len = atoi(row[20]);
	if(strlen(row[21]) > 0){
		frame.cipher = atoi(row[21]);
	}

	frame.timestamp = atof(row[22]);
	frame.bcst = atoi(row[23]);
	frame.bcst_prefix = atoi(row[24]);
	frame.aid = atoi(row[25]);

	return frame;
}

/**
 * Vykona zvolenu funkciu pre kazdy riadok dat.
 *
 * @param result Data nad ktorymi sa vykonava funkcia.
 * @param row_func Odkaz na funkciu pracujucu s jendotlivymi riadkami.
 */
void db_table_foreach(
		MYSQL_RES *result,
		void (*row_func)(MYSQL_ROW row, MYSQL_FIELD *fields, u_int32_t num_rows, u_int16_t num_fields)
	){

	MYSQL_ROW row;
	MYSQL_FIELD *fields;
	unsigned int num_fields = 0, num_rows = 0;

	num_rows = mysql_num_rows(result);
	num_fields = mysql_num_fields(result);
	fields = mysql_fetch_fields(result);

	while((row = mysql_fetch_row(result))){
		row_func(row,fields,num_rows,num_fields);
	}
}

/**
 * Vlozi obsah struktury packet_data_t do databazy.
 *
 * @note Vsetky ukladacie dotazy su vykonavane v samostatnom vlakne.
 */
void db_packet_data_insert(){

	char buffer[10000] = DB_QUERY_INSERT_VALUES;
	int query_len = 0;

	/* Pri prvom dotaze alokuj pamat a inicializuj */
	if(mysql_query_group_counter == 0){

		/* Alokuje len pri prvom priechode v ramci davky dotazov */
		if(db_mysql_stmt == NULL){
			db_mysql_stmt = (char *)calloc(mysql_alloc_size,sizeof(char));
			/*free(db_mysql_stmt);*/
		}else{
			memset(db_mysql_stmt,0,mysql_alloc_size);
		}

		sprintf(db_mysql_stmt,DB_QUERY_INSERT,db_actual_table_name);
		query_len = strlen(db_mysql_stmt);
	}else{
		query_len = strlen(db_mysql_stmt);
	}

	/* Dlzka dotazu presiahla alokovane miesto */
	if(query_len*2 >= mysql_alloc_size){
		mysql_alloc_size = mysql_alloc_size*4;
		db_mysql_stmt = (char *)realloc(db_mysql_stmt,mysql_alloc_size * sizeof(char));
	}

	sprintf(db_mysql_stmt+query_len,buffer,
			frame_data.addr1,
			frame_data.addr2,
			frame_data.addr3,
			frame_data.addr4,
			frame_data.bssid,
			frame_data.seq,
			frame_data.fc_type,
			frame_data.fc_subtype,
			frame_data.ssid,
			frame_data.channel,
			frame_data.len,
			frame_data.timestamp,
			frame_data.dur,
			frame_data.aid,
			frame_data.cipher,
			frame_data.signal,
			frame_data.frag,
			frame_data.fc_from_ds,
			frame_data.fc_to_ds,
			frame_data.fc_more_frag,
			frame_data.fc_retry,
			frame_data.fc_protected_frame,
			frame_data.fc_order,
			frame_data.bcst,
			frame_data.bcst_prefix);

	mysql_query_group_counter++;

	/* Data vkladaj do db po urcenom pocte dotazov */
	if(mysql_query_group_counter >= atk_attack_attributes.db_save_group_size){

		/* Treba odstranit ciarku na konci */
		db_mysql_stmt[strlen(db_mysql_stmt) - 1] = 0;

		/* Vlakno stale uklada data do db, treba pockat
		pthread_mutex_lock(&db_mutex_save);*/

		/* Skopirovanie mysql dotazu
		db_mysql_stmt_copy = (char *)calloc(mysql_alloc_size,sizeof(char));
		strcpy(db_mysql_stmt_copy,db_mysql_stmt);
*/
		/*pthread_mutex_unlock(&db_mutex_save);*/

		g_thread_pool_push(db_thread_pool_save,g_strdup(db_mysql_stmt),NULL);

		/* Daj signal vlaknu
		pthread_cond_signal(&db_cond_save);*/



		mysql_query_group_counter = 0;
	}
}

long db_data_count(const char *table_name){

	MYSQL_RES *res;
	MYSQL_ROW row;
	char query[512];

	sprintf(query,DB_QUERY_SELECT_COUNT,table_name);

	mysql_query(db_conn,query);

	res = mysql_store_result(db_conn);

	if(mysql_num_rows(res) != 1) return 0;

	row = mysql_fetch_row(res);

	return atol(row[0]);
}
/**
 * Inicializuje pripojenie k databaze.
 *
 * @note Prihlasovacie udaje ziskava z konfiguracneho ini suboru config.ini.
 */
void db_init(){

	const char *mysql_user, *mysql_pwd, *mysql_host, *mysql_db;

	/* Nacitanie nastaveni z config suboru */
	dict_config = iniparser_load(CONFIG_FILE);
	mysql_user = iniparser_getstring(dict_config,"mysql:user","");
	mysql_pwd = iniparser_getstring(dict_config,"mysql:password","");
	mysql_host = iniparser_getstring(dict_config,"mysql:host","");
	mysql_db = iniparser_getstring(dict_config,"mysql:db","");

	/* Hodnota sa cita z konfiguracnych ini suborov, ak ju uzivatel nezada pouzije sa default hodnota
	db_query_actual_group_size = DB_QUERY_GROUP_SIZE;*/


	printf("Init Mysql client %s...",mysql_get_client_info());

	db_conn = mysql_init(NULL);

	if(db_conn == NULL){
		printf("failed\n");
		db_mysql_error_print();
		exit(EXIT_FAILURE);
	}

	printf("success\n");

	printf("Connecting to Mysql server...");

	if (mysql_real_connect(db_conn, mysql_host, mysql_user, mysql_pwd,
			mysql_db, 0, NULL, 0) == NULL) {
		printf("failed\n");
		db_mysql_error_print();
		exit(EXIT_FAILURE);
	}

	printf("success\n");

/*
	if((ret = pthread_attr_init(&db_thread_attr)) != 0){
		fprintf(stderr, "pthread_attr_init() error %d\n",ret);
		exit(EXIT_FAILURE);
	}

	if((ret = pthread_attr_setdetachstate(&db_thread_attr,PTHREAD_CREATE_DETACHED)) != 0){
		fprintf(stderr, "pthread_attr_setdetachstate() error %d\n",ret);
		exit(EXIT_FAILURE);
	}
*/
	db_thread_pool_save = g_thread_pool_new(db_thread_frame_data_insert,NULL,-1,FALSE,NULL);

/*
	if((ret = pthread_cond_init(&db_cond_save,NULL)) != 0){
		fprintf(stderr,"pthread_cond_init() error %d\n",ret);
		exit(EXIT_FAILURE);
	}

	if((ret = pthread_mutex_init(&db_mutex_save,NULL)) != 0){
		fprintf(stderr,"pthread_mutex_init() error %d\n",ret);
		exit(EXIT_FAILURE);
	}

	if((ret = pthread_create(&db_thread, &db_thread_attr, db_insert_packet_data_thread,NULL)) != 0){
		fprintf(stderr, "pthread_create() error %d\n",ret);
		exit(EXIT_FAILURE);
	}
	*/
}
