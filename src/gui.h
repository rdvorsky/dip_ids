/*
 * gui.h
 *
 *  Created on: 9.3.2014
 *      Author: rdy
 */

#ifndef GUI_H_
#define GUI_H_

#include <glib.h>
#include <sys/types.h>
#include <ncurses.h>
#include <menu.h>
#include <panel.h>

typedef struct {
	int 		channel;
	char 		*bssid;
	char 		*iface;
	u_int8_t	is_training;
	u_int8_t 	is_no_save;
	u_int8_t	is_ids_active;
	int			num_rows;
	int			num_cols;
	int			num_table_rows;
} ncurses_scr_data;

ncurses_scr_data ncurses_screen_data;

PANEL	*gui_panel_main;
PANEL	*gui_panel_bottom;
PANEL	*gui_panel_popup;

WINDOW *gui_window_main;
WINDOW *gui_window_bottom;
WINDOW *gui_window_bottom_border;
WINDOW *gui_window_popup;
WINDOW *gui_window_popup_border;
WINDOW *gui_window_menu;
WINDOW *gui_window_menu_border;

MENU *gui_menu_attacks;

void gui_init(GHashTable *hash_table_ap);
WINDOW *gui_window_create(int height, int width, int starty, int startx, gboolean center);
void gui_popup_show(char *title, char *text, int width, int height);
void gui_menu_show(int *output_index);
void gui_popup_hide();
void gui_menu_hide();
void gui_window_destroy(WINDOW *local_win);
void gui_window_bottom_refresh();
void gui_window_main_refresh();
void gui_popups_lock();
void gui_popups_unlock();

#endif /* GUI_H_ */
