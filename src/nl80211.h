/*
 * nl80211.h
 *
 *  Created on: 10.11.2013
 *      Author: rdy
 */

#ifndef NL80211_H_
#define NL80211_H_

void nl80211_init();
void nl80211_start_channel_hopping(char *iface);
int nl80211_set_channel(int iface_index, unsigned int channel);
int nl80211_get_channel();
unsigned int nl80211_channel_to_freq(int channel);
unsigned int nl80211_freq_to_channel(unsigned int freq);

#endif /* NL80211_H_ */
