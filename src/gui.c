/*
 * gui.c
 *
 *  Created on: 9.3.2014
 *      Author: rdy
 */

#define GUI_REFRESH_SLEEP 1

#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <pthread.h>

#include "gui.h"
#include "nl80211.h"
#include "common.h"
#include "attack.h"

#define GUI_WINDOW_BOTTOM_OFFSET 	10
#define GUI_FILE_NAME_LOG			"./log.txt"

static GHashTable 	*hash_table_data;
static FILE 		*file_log;
static int 			win_main_width;
static int 			win_main_height;
static gboolean 	is_window_popup_visible 		= FALSE;
static gboolean 	is_window_attacks_visible 	= FALSE;

void gui_popups_lock(){
	is_window_attacks_visible = TRUE;
	is_window_popup_visible = TRUE;
}

void gui_popups_unlock(){
	is_window_attacks_visible = FALSE;
	is_window_popup_visible = FALSE;
}

void gui_wait_input_win_attacks(int *output_index){
	int c;
	ITEM *cur_item;

	while(1){

		c = wgetch(gui_window_menu);

		switch(c){
		case KEY_DOWN:
			menu_driver(gui_menu_attacks, REQ_DOWN_ITEM);
			break;
		case KEY_UP:
			menu_driver(gui_menu_attacks, REQ_UP_ITEM);
			break;
		case 10:/* Enter */
			cur_item = current_item(gui_menu_attacks);
			*output_index = (int)cur_item->index;
			*output_index += 1;
			gui_menu_hide();
			return;
		case 'c':
			*output_index = 0;
			gui_menu_hide();
			return;
		}
	}
}

void gui_wait_input_win_popup(){

	int c;

	while(1){

		c = wgetch(gui_window_popup);

		switch(c){
		case 'c':
			gui_popup_hide();
			return;
		}
	}
}

void gui_window_create_bordered(char *title, char *text, int width, int height){

	/* POPUP */
	gui_window_popup = gui_window_create(height,width,
			ncurses_screen_data.num_rows,
			ncurses_screen_data.num_cols, TRUE);

	gui_window_popup_border = gui_window_create(height + 2,width + 2,
				ncurses_screen_data.num_rows,
				ncurses_screen_data.num_cols, TRUE);

	wattron(gui_window_popup,COLOR_PAIR(4));
	wattron(gui_window_popup,A_BOLD);
	mvwprintw(gui_window_popup,0,(width - strlen(title))/2,title);
	wattroff(gui_window_popup,A_BOLD);
	wattroff(gui_window_popup,COLOR_PAIR(4));

	mvwhline(gui_window_popup,1,0,ACS_HLINE,'_');

	mvwprintw(gui_window_popup,2,0,text);

	box(gui_window_popup_border,0,0);

	wrefresh(gui_window_popup_border);
	wrefresh(gui_window_popup);
}

void gui_menu_show(int *output_index){
	is_window_attacks_visible = TRUE;

	/* Post the menu */
	post_menu(gui_menu_attacks);

	gui_window_create_bordered("ATTACKS","",30,17);

	wrefresh(gui_window_menu);

	gui_wait_input_win_attacks(output_index);
}

void gui_menu_hide(){

	gui_popup_hide();
	unpost_menu(gui_menu_attacks);
	is_window_attacks_visible = FALSE;
	refresh();
}

void gui_popup_show(char *title, char *text, int width, int height){

	is_window_popup_visible = TRUE;

	/* Vytvor okno */
	gui_window_create_bordered(title,text,width,height);

	/* Cakaj na vstup */
	gui_wait_input_win_popup();
}


void gui_popup_hide(){

	gui_window_destroy(gui_window_popup);
	gui_window_destroy(gui_window_popup_border);

	is_window_popup_visible = FALSE;

	refresh();
}

void *gui_thread_gui_refresh(){

	while(1){
		/* AK je otvorene menu alebo popup window, neobnovuj */
		if(!is_window_popup_visible && !is_window_attacks_visible) {
			gui_window_main_refresh();
			/*attron(A_BOLD);*/
			/*mvwhline(stdscr,win_main_height,0,ACS_HLINE,ACS_HLINE);*/
			/*attroff(A_BOLD);*/
			gui_window_bottom_refresh();
		}

		sleep(GUI_REFRESH_SLEEP);
	}

	return (void *)1;
}


void gui_init_menu(){

	ITEM **my_items;
	int i;

	/* Create items */
	my_items = (ITEM **)calloc(ATK_COUNT_ALL+1, sizeof(ITEM *));
	for(i = 0; i < ATK_COUNT_ALL; ++i)
		my_items[i] = new_item(atk_attack_names[i], 0);

	/* Crate menu */
	gui_menu_attacks = new_menu((ITEM **)my_items);

	/* Create the window to be associated with the menu */
	gui_window_menu = gui_window_create(12,28,ncurses_screen_data.num_rows,ncurses_screen_data.num_cols,TRUE);

	keypad(gui_window_menu, TRUE);

	/* Set main window and sub window */
	set_menu_win(gui_menu_attacks, gui_window_menu);


	/* Set menu mark to the string " * " */
	set_menu_mark(gui_menu_attacks, "   ");

	/* Print a border around the main window and print a title */
	wrefresh(gui_window_menu);

}

void gui_init(GHashTable *hash_table_ap){

	int win_main_offset = GUI_WINDOW_BOTTOM_OFFSET;

	hash_table_data = hash_table_ap;

	if((file_log = fopen(GUI_FILE_NAME_LOG,"w+")) == NULL){
		fprintf(stderr,"ERROR: file_log fopen failed\n");
		exit(EXIT_FAILURE);
	}

	initscr();
	raw();
	keypad(stdscr,TRUE);
	noecho();
	start_color();

	init_pair(1, COLOR_WHITE, COLOR_BLACK);
	init_pair(2, COLOR_RED, COLOR_BLACK);
	init_pair(3, COLOR_GREEN, COLOR_BLACK);
	init_pair(4, COLOR_BLUE, COLOR_BLACK);
	init_pair(5, COLOR_WHITE, COLOR_BLUE);
	init_pair(6, COLOR_YELLOW, COLOR_BLACK);

	getmaxyx(stdscr,ncurses_screen_data.num_rows,ncurses_screen_data.num_cols);

	/* Hlavne okno so statistikami */
	gui_window_main = gui_window_create((ncurses_screen_data.num_rows/2) + win_main_offset,ncurses_screen_data.num_cols,0,0,FALSE);
	getmaxyx(gui_window_main,win_main_height,win_main_width);

	/*attron(A_BOLD);*/
	/*mvwhline(stdscr,win_main_height,0,ACS_HLINE,ACS_HLINE);*/
	/*attroff(A_BOLD);*/

	gui_window_bottom_border = gui_window_create(
				(ncurses_screen_data.num_rows/2) - win_main_offset + 1,
				ncurses_screen_data.num_cols,
				win_main_height,
				0,FALSE);

	box(gui_window_bottom_border,0,0);

	/* Bottom */
	gui_window_bottom = gui_window_create(
			(ncurses_screen_data.num_rows/2) - win_main_offset - 2 + 1,
			ncurses_screen_data.num_cols - 2,
			win_main_height + 1,
			1,FALSE);


	scrollok(gui_window_bottom,1);

	gui_init_menu();

	comm_thread_init(gui_thread_gui_refresh, NULL, FALSE, NULL);

	refresh();
}

WINDOW *gui_window_create(int height, int width, int starty, int startx, gboolean center){
	WINDOW *local_win;

	if(center){
		starty = (starty - height)/2;
		startx = (startx - width)/2;
	}

	local_win = newwin(height, width, starty, startx);

	return local_win;
}

void gui_window_destroy(WINDOW *local_win){

	wborder(local_win, ' ', ' ', ' ',' ',' ',' ',' ',' ');
	wrefresh(local_win);
	delwin(local_win);
}

void gui_ap_stats_print(ap_stats_t *ap){

	int offset = 3;

	if(ap->aid > 0){
		wattron(gui_window_main,COLOR_PAIR(2));
		wattron(gui_window_main,A_BOLD);
		mvwprintw(gui_window_main,offset + ncurses_screen_data.num_table_rows,0,"%d",ap->aid);
		wattroff(gui_window_main,A_BOLD);
		wattroff(gui_window_main,COLOR_PAIR(2));
	}

	mvwprintw(gui_window_main,offset + ncurses_screen_data.num_table_rows,2,"%s",ap->bssid);
	mvwprintw(gui_window_main,offset + ncurses_screen_data.num_table_rows,21,"%d",ap->channel);
	mvwprintw(gui_window_main,offset + ncurses_screen_data.num_table_rows,28,"%ld",ap->counter_data);
	mvwprintw(gui_window_main,offset + ncurses_screen_data.num_table_rows,36,"%ld",ap->counter_mgt);
	mvwprintw(gui_window_main,offset + ncurses_screen_data.num_table_rows,44,"%ld",ap->counter_ctl);
	mvwprintw(gui_window_main,offset + ncurses_screen_data.num_table_rows,52,"%d",ap->counter_auth);
	mvwprintw(gui_window_main,offset + ncurses_screen_data.num_table_rows,60,"%d",ap->counter_deauth);
	mvwprintw(gui_window_main,offset + ncurses_screen_data.num_table_rows,68,"%d",ap->counter_ack);
	mvwprintw(gui_window_main,offset + ncurses_screen_data.num_table_rows,76,"%d",ap->counter_fragmented);
	mvwprintw(gui_window_main,offset + ncurses_screen_data.num_table_rows,84,"%d",ap->counter_retry);

	ncurses_screen_data.num_table_rows++;
}

void gui_hash_table_foreach_ap(gpointer key, gpointer value, gpointer userdata){
	UNUSED(key);
	UNUSED(userdata);

	gui_ap_stats_print(value);
}

void gui_window_bottom_refresh(){

	char *buffer = NULL;
	size_t len = 0;
	ssize_t read = 0;


	while((read = getline(&buffer,&len,file_log)) > 0){
		waddstr(gui_window_bottom,buffer);
	}

	wrefresh(gui_window_bottom_border);
	wrefresh(gui_window_bottom);
}

void gui_window_main_refresh(){

	const char *msg_help = "Press 'h' for Help";
	int x_pos = 0;
	GString *title = g_string_new("");

	werase(gui_window_main);

	wattron(gui_window_main,A_BOLD);

	g_string_printf(title,"Intf: %s",ncurses_screen_data.iface);
	mvwprintw(gui_window_main,0,0,title->str);

	x_pos = title->len;
	g_string_printf(title," | CH: %2d",nl80211_get_channel());
	mvwprintw(gui_window_main,0,x_pos,title->str);

	x_pos += title->len;
	g_string_printf(title," | BSSID: %s",ncurses_screen_data.bssid == NULL ? "any" : ncurses_screen_data.bssid);
	mvwprintw(gui_window_main,0,x_pos,title->str);

	x_pos += title->len;
	g_string_printf(title," | IDS: %s", ncurses_screen_data.is_ids_active == 1 ? "yes" : "no");
	mvwprintw(gui_window_main,0,x_pos,title->str);

	x_pos += title->len;
	g_string_printf(title," | Training: %s", ncurses_screen_data.is_training == 1 ? "yes" : "no");
	mvwprintw(gui_window_main,0,x_pos,title->str);

	x_pos += title->len;
	g_string_printf(title," | Save: %s", ncurses_screen_data.is_no_save == 1 ? "no" : "yes");
	mvwprintw(gui_window_main,0,x_pos,title->str);

	wattroff(gui_window_main,A_BOLD);

	wattron(gui_window_main,COLOR_PAIR(5));
	mvwprintw(gui_window_main,0,ncurses_screen_data.num_cols - strlen(msg_help),"%s",msg_help);
	wattroff(gui_window_main,COLOR_PAIR(5));

	wattron(gui_window_main,COLOR_PAIR(6));
	wattron(gui_window_main,A_BOLD);
	mvwprintw(gui_window_main,2,0,"A");
	mvwprintw(gui_window_main,2,2,"BSSID");
	mvwprintw(gui_window_main,2,21,"CH");
	mvwprintw(gui_window_main,2,28,"DATA");
	mvwprintw(gui_window_main,2,36,"MGT");
	mvwprintw(gui_window_main,2,44,"CTL");
	mvwprintw(gui_window_main,2,52,"AUTH");
	mvwprintw(gui_window_main,2,60,"DEAUTH");
	mvwprintw(gui_window_main,2,68,"ACK");
	mvwprintw(gui_window_main,2,76,"FRAG");
	mvwprintw(gui_window_main,2,84,"RETRY");
	wattroff(gui_window_main,A_BOLD);
	wattroff(gui_window_main,COLOR_PAIR(6));

	ncurses_screen_data.num_table_rows = 0;
	g_hash_table_foreach(hash_table_data,gui_hash_table_foreach_ap,NULL);

	wrefresh(gui_window_main);
}

