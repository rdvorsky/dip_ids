all:
	cd build/ && cmake ../
	cd build/ && make
	cp build/sniffer bin/
	cp build/analyze bin/
	
rebuild: clean all

clean:
	rm -rf build/*
	rm -rf thirdparty/iniparser4/build/*
	rm -rf thirdparty/fann/build/*
	rm -rf thirdparty/generator/build/*
	rm -rf bin/output/* bin/sniffer bin/analyze bin/*.cap bin/*.xor bin/*.cap
	rm -rf thirdparty/fann/CMakeFiles/ thirdparty/fann/src/*.so.* thirdparty/fann/src/*.so \
	thirdparty/fann/src/CMakeFiles/ thirdparty/fann/src/cmake_install.cmake
	rm -rf thirdparty/fann/cmake_install.cmake thirdparty/fann/CMakeCache.txt \
	thirdparty/fann/CPack* thirdparty/fann/fann.pc thirdparty/fann/Makefile
	rm -rf thirdparty/fann/src/include/cmake_install.cmake thirdparty/fann/src/include/CMakeFiles/
